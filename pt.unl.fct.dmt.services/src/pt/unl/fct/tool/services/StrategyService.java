package pt.unl.fct.tool.services;

import java.util.Collection;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;

import dmt.DeceptionStrategy;

/**
 * @author Cristiano De Faveri
 * GoHoney Tool : Deception Modeling Editors
 *
 */
public class StrategyService implements IExternalJavaAction {
	public final String MRISK = "MRisk"; 
	public final String MCONFLICT = "MConflict"; 
	public final String MMITIGATION = "MMitigation"; 
	
	@Override
	public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
		// selections represent the deception strategies and parameters are tokens sent by the editor

		int i = 0;
		for (Map.Entry<String, Object> entry : parameters.entrySet()) {
			if (entry.getValue().equals(MRISK))
				if (selections.toArray()[i] instanceof DeceptionStrategy)
					computeMinimizeRiskStrategy((DeceptionStrategy)selections.toArray()[i]);

			if (entry.getValue().equals(MCONFLICT))
				if (selections.toArray()[i] instanceof DeceptionStrategy)
					computeMinimizeConflictsStrategy((DeceptionStrategy)selections.toArray()[i]);

			if (entry.getValue().equals(MMITIGATION))
				if (selections.toArray()[i] instanceof DeceptionStrategy)
					computeMaximizeMitigationStrategy((DeceptionStrategy)selections.toArray()[i]);
			i++;
		}
	}

	@Override
	public boolean canExecute(Collection<? extends EObject> selections) {
		return true;
	}
	
	private void computeMinimizeConflictsStrategy (DeceptionStrategy strategy) {
		// Compute Strategy that minimize conflicts with system goals and requirements
		System.out.println("Building minimize conflict strategy" + strategy);
	}
	
	private void computeMinimizeRiskStrategy (DeceptionStrategy strategy) {
		// Compute Strategy that minimize risks of being attacked	
		System.out.println("Building minimize risks strategy" + strategy);
	}
	
	private void computeMaximizeMitigationStrategy (DeceptionStrategy strategy) {
		// Compute Strategy that maximize the mitigation of attacks, threats, and vulnerabilities.
		System.out.println("Building maximize mitigation strategy" + strategy);
	}
}

