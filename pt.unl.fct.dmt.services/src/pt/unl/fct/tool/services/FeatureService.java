package pt.unl.fct.tool.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;

import dmt.DMTModel;
import dmt.DeceptionTacticMechanism;
import dmt.DmtFactory;
import dmt.EnactmentEnum;
import dmt.Feature;
import dmt.FeatureRefinement;
import dmt.NodeRefinement;
import dmt.NodesSelectionType;
import dmt.TacticFeatureRefinement;

public class FeatureService implements IExternalJavaAction {
	private DMTModel container;
	private List<NodeRefinement> nodesRefinement = new ArrayList<NodeRefinement>();
	
	public void createTacticBaseDesignPattern (EObject obj) {
		if (isValid(obj)) {
			DeceptionTacticMechanism tactic = (DeceptionTacticMechanism)obj;
			container = (DMTModel)tactic.eContainer();
			createNodeFromTactic(tactic, "D-Setup", "Abstract feature representing setup features for a tactic", true,  EnactmentEnum.OPTIONAL);
			
			Feature conf = createNodeFromTactic(tactic, "D-Configuration", "Abstract feature representing configuration features for a tactic", true, EnactmentEnum.OPTIONAL);
				createNodeFromFeature(conf, "Measurement", "Abstract feature representing measurement features", true, EnactmentEnum.OPTIONAL);
				createNodeFromFeature(conf, "Alert", "Abstract feature representing alert features", true, EnactmentEnum.OPTIONAL);
				createNodeFromFeature(conf, "Action Policy", "Abstract feature representing action policies features", true, EnactmentEnum.OPTIONAL);

			createNodeFromTactic(tactic, "D-Services", "Abstract feature representing services of a tactic.", true, EnactmentEnum.MANDATORY);				

			Feature management = createNodeFromTactic(tactic, "D-Management", "Abstract feature representing management features for a tactic.", true, EnactmentEnum.MANDATORY);
		    	createNodeFromFeature(management, "Enabling", "Enabling feature", false, EnactmentEnum.OPTIONAL);
		    	createNodeFromFeature(management, "Disabling", "Disabling feature", false, EnactmentEnum.OPTIONAL);
		    	createNodeFromFeature(management, "Tactic Control", "Tactic control feature", true, EnactmentEnum.MANDATORY);
		    	Feature activation = createNodeFromFeature(management, "Activation", "Activation feature", true, EnactmentEnum.MANDATORY);
		    		createNodeFromFeature(activation, "Configure", "Configure feature", false, EnactmentEnum.OPTIONAL);
		    		createNodeFromFeature(activation, "Initialize", "Initialize feature", false, EnactmentEnum.MANDATORY);
		    		createNodeFromFeature(activation, "Execute", "Execute feature", false, EnactmentEnum.MANDATORY);
		    	Feature deactivation = createNodeFromFeature(management, "Deactivation", "Deactivation feature", true, EnactmentEnum.MANDATORY);
		    		nodesRefinement.clear();
		    		createNodeFromFeature(deactivation, "Raw termination", "Raw termination feature", false, EnactmentEnum.OPTIONAL);
		    		createNodeFromFeature(deactivation, "Full termination", "Full termination feature", false, EnactmentEnum.OPTIONAL);
		    		createFeatureSelection("XOR between raw and full termination", NodesSelectionType.XOR);
    		
			Feature termination = createNodeFromTactic(tactic, "D-Termination", "Abstract feature representing termination features for a tactic.", true, EnactmentEnum.MANDATORY);
				createNodeFromFeature(termination, "Halt", "Halt feature", false, EnactmentEnum.MANDATORY);
				createNodeFromFeature(termination, "Clean up", "Clean up feature", true, EnactmentEnum.OPTIONAL);
				createNodeFromFeature(termination, "Reorganization", "Reorganization feature", true, EnactmentEnum.OPTIONAL);
		}	
	}
	
	private void createFeatureSelection(String description, NodesSelectionType type) {
//		if ( (nodesRefinement.isEmpty()) || (nodesRefinement.size() < 2))
//			return;
//		for (int i = 0; i < nodesRefinement.size() - 1;i++) {
//			NodeSelection nodeSelection;
//			nodeSelection = DmtFactory.eINSTANCE.createNodeSelection();
//			nodeSelection.setDescription(description);
//			nodeSelection.setSelectionsource(nodesRefinement.get(i));
//			nodeSelection.setSelectiontarget(nodesRefinement.get(i+1));
//			nodeSelection.setType(type);			
//		} 
	}

	public void createTacticAdaptiveDesignPattern (EObject obj) {
		if (isValid(obj)) {
			DeceptionTacticMechanism tactic = (DeceptionTacticMechanism)obj;
			container = (DMTModel)tactic.eContainer();
			
			Feature adaptationFeature = createNodeFromTactic(tactic, "Monitoring and Adaptation", "Abstrat feature representing monitoring and adaptation features for a tactic.", true, EnactmentEnum.MANDATORY);
			Feature monitor = createNodeFromFeature(adaptationFeature, "Monitor", "Feature representing monitoring services for a tactic.", true, EnactmentEnum.MANDATORY);
				Feature channel = createNodeFromFeature(monitor, "Channel", "Channel feature", true, EnactmentEnum.MANDATORY);
	    			nodesRefinement.clear();
	    			createNodeFromFeature(channel, "Internal", "Internal channel feature", true, EnactmentEnum.OPTIONAL);
					createNodeFromFeature(channel, "External", "External channel feature", true, EnactmentEnum.OPTIONAL);
		    		createFeatureSelection("OR between internal and external channels", NodesSelectionType.OR);
		    		
				createNodeFromFeature(monitor, "Filter", "Filter feature", true, EnactmentEnum.OPTIONAL);
				createNodeFromFeature(monitor, "Publisher", "Pubisher feature", true, EnactmentEnum.MANDATORY);
				createNodeFromFeature(monitor, "Normalization", "Normalization feature", true, EnactmentEnum.OPTIONAL);
				
			Feature analysis = createNodeFromFeature(adaptationFeature, "Analysis", "Feature representing analysis services for a tactic.", true, EnactmentEnum.MANDATORY);
				createNodeFromFeature(analysis, "Engagement", "Engagement feature", true, EnactmentEnum.MANDATORY);
				createNodeFromFeature(analysis, "Domain", "Domain feature", true, EnactmentEnum.OPTIONAL);
				createNodeFromFeature(analysis, "Event", "Event feature", true, EnactmentEnum.OPTIONAL);
				createNodeFromFeature(analysis, "Dependency", "Dependency feature", true, EnactmentEnum.MANDATORY);
			
			Feature reconfiguration = createNodeFromFeature(adaptationFeature, "Reconfiguration", "Feature representing reconfiguration services for a tactic.", true, EnactmentEnum.OPTIONAL);
				Feature dispatcher = createNodeFromFeature(reconfiguration, "Dispatcher", "Dispatcher feature", true, EnactmentEnum.OPTIONAL);
					nodesRefinement.clear();
					createNodeFromFeature(dispatcher, "Context dispatcher", "Context dispatcher feature", true, EnactmentEnum.OPTIONAL);
					createNodeFromFeature(dispatcher, "Artificial environment dispatcher", "Artificial environment dispatcher feature", true, EnactmentEnum.OPTIONAL);
					createNodeFromFeature(dispatcher, "System dispatcher", "System dispatcher feature", true, EnactmentEnum.OPTIONAL);
					createNodeFromFeature(dispatcher, "Tactic dispatcher", "Tactic dispatcher feature", true, EnactmentEnum.OPTIONAL);
					createFeatureSelection("OR between dispatchers", NodesSelectionType.OR);
					createNodeFromFeature(dispatcher, "Alert dispatcher", "Alert dispatcher feature", true, EnactmentEnum.OPTIONAL);
				Feature reconftype = createNodeFromFeature(reconfiguration, "Reconf type", "Reconfiguration type feature", true, EnactmentEnum.OPTIONAL);
				nodesRefinement.clear();
				createNodeFromFeature(reconftype, "Structural", "Structural feature", true, EnactmentEnum.OPTIONAL);
				createNodeFromFeature(reconftype, "Parametric", "Parametric feature", true, EnactmentEnum.OPTIONAL);
				createNodeFromFeature(reconftype, "Functional", "Functional feature", true, EnactmentEnum.OPTIONAL);
				createFeatureSelection("OR between dispatchers", NodesSelectionType.OR);
		}	
	}	
	private Feature createNodeFromTactic(DeceptionTacticMechanism tactic, String name, String description, boolean abstractNode, EnactmentEnum enactment) {
		Feature feature = createFeatureNode(name, description, abstractNode);
		createTacticFeatureRefinement(tactic, feature, EnactmentEnum.MANDATORY);
		return feature;
	}

	private Feature createNodeFromFeature(Feature adaptationFeature, String name, String description, boolean abstractNode,
			EnactmentEnum enactment) {
		Feature feature = createFeatureNode(name, description, abstractNode);
		createFeatureToFeatureRefinement(adaptationFeature, feature, EnactmentEnum.MANDATORY);
		return feature;
	}

	private Feature createFeatureNode(String name, String description, boolean abstractNode) {
		Feature feature;
		feature = DmtFactory.eINSTANCE.createFeature();
		feature.setName(name);
		feature.setUUID(UUID.randomUUID().toString());
		feature.setDescription(description);
		feature.setIsAbstract(abstractNode);
		return feature;
	}

	private boolean isValid(EObject obj) {
		return (null != obj) && (obj instanceof dmt.DeceptionTacticMechanism) && (obj.eContainer() instanceof dmt.DMTModel); 
	}

	private FeatureRefinement createFeatureToFeatureRefinement(Feature feature, Feature featureTarget, EnactmentEnum enactmentOption) {
		FeatureRefinement featureRefinement;
		featureRefinement = DmtFactory.eINSTANCE.createFeatureRefinement();
		
		container.getHasStrategyRelations().add(featureRefinement);
		container.getHasStrategyNodes().add(featureTarget);

		featureRefinement.setFeaturerefinementsource(feature);
		featureRefinement.setFeaturerefinementtarget(featureTarget);
		featureRefinement.setEnactmentChoice(enactmentOption);
		
		nodesRefinement.add(featureRefinement);
		return featureRefinement;
	}

	private TacticFeatureRefinement createTacticFeatureRefinement(DeceptionTacticMechanism tactic, Feature featureTarget, EnactmentEnum enactmentOption) {
		TacticFeatureRefinement tacticFeatureRefinement;
		tacticFeatureRefinement = DmtFactory.eINSTANCE.createTacticFeatureRefinement();
		
		container.getHasStrategyRelations().add(tacticFeatureRefinement);
		container.getHasStrategyNodes().add(featureTarget);

		tacticFeatureRefinement.setTacticfeaturerefinementsource(tactic);
		tacticFeatureRefinement.setTacticfeaturerefinementtarget(featureTarget);
		tacticFeatureRefinement.setEnactmentChoice(enactmentOption);
		
		nodesRefinement.add(tacticFeatureRefinement);
		
		return tacticFeatureRefinement;
	}
	
	@Override
	public void execute(Collection<? extends EObject> selections, Map<String, Object> parameters) {
		System.out.println("Executing feature external services. . . ");
		System.out.println("Objects received: \n");
		for (EObject eObject : selections) 
			System.out.println(eObject.toString());

		System.out.println("Parameters received: \n");
		for (Map.Entry<String, Object> entry : parameters.entrySet()) 
			System.out.println(entry.getKey() + " - " + entry.getValue());
		
		EObject obj = selections.iterator().next(); // get only the first object
		Object o = parameters.get("type");
		if (o.equals("Base"))
			createTacticBaseDesignPattern(obj);
		else if (o.equals("Adaptation"))
			createTacticAdaptiveDesignPattern(obj);
	}

	@Override
	public boolean canExecute(Collection<? extends EObject> selections) {
		// TODO Auto-generated method stub
		return true;
	}
}
