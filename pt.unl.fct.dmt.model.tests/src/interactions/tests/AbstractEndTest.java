/**
 * Copyright (c) 2007, 2014 THALES GLOBAL SERVICES.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *    Obeo - initial API and implementation
 *    NOVALINCS - NOVA University of Lisbon, DML integration 
 * 
 */
package interactions.tests;

import interactions.AbstractEnd;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract End</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AbstractEndTest extends TestCase {

	/**
	 * The fixture for this Abstract End test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractEnd fixture = null;

	/**
	 * Constructs a new Abstract End test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractEndTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Abstract End test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(AbstractEnd fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Abstract End test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractEnd getFixture() {
		return fixture;
	}

} //AbstractEndTest
