/**
 */
package dmt.tests;

import dmt.AgentRelation;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Agent Relation</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AgentRelationTest extends GoalRelationshipTest {

	/**
	 * Constructs a new Agent Relation test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentRelationTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Agent Relation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AgentRelation getFixture() {
		return (AgentRelation)fixture;
	}

} //AgentRelationTest
