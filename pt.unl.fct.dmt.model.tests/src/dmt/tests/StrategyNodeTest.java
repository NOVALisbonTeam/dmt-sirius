/**
 */
package dmt.tests;

import dmt.StrategyNode;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Strategy Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class StrategyNodeTest extends NodeTest {

	/**
	 * Constructs a new Strategy Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StrategyNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Strategy Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected StrategyNode getFixture() {
		return (StrategyNode)fixture;
	}

} //StrategyNodeTest
