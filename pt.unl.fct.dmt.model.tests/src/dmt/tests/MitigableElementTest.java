/**
 */
package dmt.tests;

import dmt.MitigableElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Mitigable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MitigableElementTest extends SecurityNodeTest {

	/**
	 * Constructs a new Mitigable Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MitigableElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Mitigable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected MitigableElement getFixture() {
		return (MitigableElement)fixture;
	}

} //MitigableElementTest
