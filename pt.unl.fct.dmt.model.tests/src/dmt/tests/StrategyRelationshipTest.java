/**
 */
package dmt.tests;

import dmt.StrategyRelationship;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Strategy Relationship</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class StrategyRelationshipTest extends TestCase {

	/**
	 * The fixture for this Strategy Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StrategyRelationship fixture = null;

	/**
	 * Constructs a new Strategy Relationship test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StrategyRelationshipTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Strategy Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(StrategyRelationship fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Strategy Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StrategyRelationship getFixture() {
		return fixture;
	}

} //StrategyRelationshipTest
