/**
 */
package dmt.tests;

import dmt.GoalNode;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Goal Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class GoalNodeTest extends NodeTest {

	/**
	 * Constructs a new Goal Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Goal Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected GoalNode getFixture() {
		return (GoalNode)fixture;
	}

} //GoalNodeTest
