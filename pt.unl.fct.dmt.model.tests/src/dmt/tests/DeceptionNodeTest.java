/**
 */
package dmt.tests;

import dmt.DeceptionNode;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Deception Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DeceptionNodeTest extends NodeTest {

	/**
	 * Constructs a new Deception Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Deception Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DeceptionNode getFixture() {
		return (DeceptionNode)fixture;
	}

} //DeceptionNodeTest
