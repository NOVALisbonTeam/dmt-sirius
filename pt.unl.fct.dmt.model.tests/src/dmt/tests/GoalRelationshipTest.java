/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.tests;

import dmt.GoalRelationship;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Goal Relationship</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class GoalRelationshipTest extends TestCase {

	/**
	 * The fixture for this Goal Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalRelationship fixture = null;

	/**
	 * Constructs a new Goal Relationship test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalRelationshipTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Goal Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(GoalRelationship fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Goal Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalRelationship getFixture() {
		return fixture;
	}

} //GoalRelationshipTest
