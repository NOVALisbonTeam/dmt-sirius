/**
 */
package dmt.tests;

import dmt.SecurityRelationship;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Security Relationship</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class SecurityRelationshipTest extends TestCase {

	/**
	 * The fixture for this Security Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityRelationship fixture = null;

	/**
	 * Constructs a new Security Relationship test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityRelationshipTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Security Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(SecurityRelationship fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Security Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityRelationship getFixture() {
		return fixture;
	}

} //SecurityRelationshipTest
