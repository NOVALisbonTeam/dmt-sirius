/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.tests;

import dmt.DmtFactory;
import dmt.SimulationHasArtificialElement;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Simulation Has Artificial Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SimulationHasArtificialElementTest extends DeceptionRelationshipTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SimulationHasArtificialElementTest.class);
	}

	/**
	 * Constructs a new Simulation Has Artificial Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationHasArtificialElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Simulation Has Artificial Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SimulationHasArtificialElement getFixture() {
		return (SimulationHasArtificialElement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DmtFactory.eINSTANCE.createSimulationHasArtificialElement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SimulationHasArtificialElementTest
