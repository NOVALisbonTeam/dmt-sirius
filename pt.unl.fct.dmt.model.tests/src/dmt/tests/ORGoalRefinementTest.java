/**
 */
package dmt.tests;

import dmt.DmtFactory;
import dmt.ORGoalRefinement;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>OR Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ORGoalRefinementTest extends GoalRelationshipTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ORGoalRefinementTest.class);
	}

	/**
	 * Constructs a new OR Goal Refinement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ORGoalRefinementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this OR Goal Refinement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ORGoalRefinement getFixture() {
		return (ORGoalRefinement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DmtFactory.eINSTANCE.createORGoalRefinement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ORGoalRefinementTest
