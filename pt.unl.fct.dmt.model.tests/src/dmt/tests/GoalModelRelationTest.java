/**
 */
package dmt.tests;

import dmt.GoalModelRelation;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Goal Model Relation</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class GoalModelRelationTest extends TestCase {

	/**
	 * The fixture for this Goal Model Relation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalModelRelation fixture = null;

	/**
	 * Constructs a new Goal Model Relation test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalModelRelationTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Goal Model Relation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(GoalModelRelation fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Goal Model Relation test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalModelRelation getFixture() {
		return fixture;
	}

} //GoalModelRelationTest
