/**
 */
package dmt.tests;

import dmt.FeatureConstraint;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Feature Constraint</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class FeatureConstraintTest extends StrategyRelationshipTest {

	/**
	 * Constructs a new Feature Constraint test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureConstraintTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Feature Constraint test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected FeatureConstraint getFixture() {
		return (FeatureConstraint)fixture;
	}

} //FeatureConstraintTest
