/**
 */
package dmt.tests;

import dmt.DmtFactory;
import dmt.StrategyTacticRefinement;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Strategy Tactic Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class StrategyTacticRefinementTest extends NodeRefinementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StrategyTacticRefinementTest.class);
	}

	/**
	 * Constructs a new Strategy Tactic Refinement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StrategyTacticRefinementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Strategy Tactic Refinement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected StrategyTacticRefinement getFixture() {
		return (StrategyTacticRefinement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DmtFactory.eINSTANCE.createStrategyTacticRefinement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //StrategyTacticRefinementTest
