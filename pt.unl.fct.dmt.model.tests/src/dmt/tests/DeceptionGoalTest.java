/**
 */
package dmt.tests;

import dmt.DeceptionGoal;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Deception Goal</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DeceptionGoalTest extends GoalTest {

	/**
	 * Constructs a new Deception Goal test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionGoalTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Deception Goal test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DeceptionGoal getFixture() {
		return (DeceptionGoal)fixture;
	}

} //DeceptionGoalTest
