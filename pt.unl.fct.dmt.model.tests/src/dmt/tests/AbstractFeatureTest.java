/**
 */
package dmt.tests;

import dmt.AbstractFeature;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Feature</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AbstractFeatureTest extends StrategyNodeTest {

	/**
	 * Constructs a new Abstract Feature test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractFeatureTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Abstract Feature test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AbstractFeature getFixture() {
		return (AbstractFeature)fixture;
	}

} //AbstractFeatureTest
