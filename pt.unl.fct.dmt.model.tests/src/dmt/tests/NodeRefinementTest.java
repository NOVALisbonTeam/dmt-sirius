/**
 */
package dmt.tests;

import dmt.NodeRefinement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Node Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class NodeRefinementTest extends StrategyRelationshipTest {

	/**
	 * Constructs a new Node Refinement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeRefinementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Node Refinement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected NodeRefinement getFixture() {
		return (NodeRefinement)fixture;
	}

} //NodeRefinementTest
