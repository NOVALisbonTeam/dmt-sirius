/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.tests;

import dmt.Operationable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Operationable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class OperationableTest extends GoalNodeTest {

	/**
	 * Constructs a new Operationable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationableTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Operationable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Operationable getFixture() {
		return (Operationable)fixture;
	}

} //OperationableTest
