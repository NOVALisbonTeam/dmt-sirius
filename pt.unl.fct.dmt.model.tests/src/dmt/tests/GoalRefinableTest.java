/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.tests;

import dmt.GoalRefinable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Goal Refinable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class GoalRefinableTest extends GoalNodeTest {

	/**
	 * Constructs a new Goal Refinable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalRefinableTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Goal Refinable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected GoalRefinable getFixture() {
		return (GoalRefinable)fixture;
	}

} //GoalRefinableTest
