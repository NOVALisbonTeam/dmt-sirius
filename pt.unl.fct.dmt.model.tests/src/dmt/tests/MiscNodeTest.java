/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.tests;

import dmt.MiscNode;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Misc Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MiscNodeTest extends NodeTest {

	/**
	 * Constructs a new Misc Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MiscNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Misc Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected MiscNode getFixture() {
		return (MiscNode)fixture;
	}

} //MiscNodeTest
