/**
 */
package dmt.tests;

import dmt.SecurityNode;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Security Node</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class SecurityNodeTest extends NodeTest {

	/**
	 * Constructs a new Security Node test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityNodeTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Security Node test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected SecurityNode getFixture() {
		return (SecurityNode)fixture;
	}

} //SecurityNodeTest
