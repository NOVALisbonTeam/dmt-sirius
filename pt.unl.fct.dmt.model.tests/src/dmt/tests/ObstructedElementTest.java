/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.tests;

import dmt.ObstructedElement;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Obstructed Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ObstructedElementTest extends TestCase {

	/**
	 * The fixture for this Obstructed Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObstructedElement fixture = null;

	/**
	 * Constructs a new Obstructed Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstructedElementTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Obstructed Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ObstructedElement fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Obstructed Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObstructedElement getFixture() {
		return fixture;
	}

} //ObstructedElementTest
