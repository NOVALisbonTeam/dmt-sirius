/**
 */
package dmt.tests;

import dmt.DmtFactory;
import dmt.TacticRefinement;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Tactic Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TacticRefinementTest extends NodeRefinementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TacticRefinementTest.class);
	}

	/**
	 * Constructs a new Tactic Refinement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TacticRefinementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Tactic Refinement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TacticRefinement getFixture() {
		return (TacticRefinement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DmtFactory.eINSTANCE.createTacticRefinement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TacticRefinementTest
