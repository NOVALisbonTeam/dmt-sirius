/**
 */
package dmt.tests;

import dmt.DmtFactory;
import dmt.TacticFeatureRefinement;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Tactic Feature Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class TacticFeatureRefinementTest extends NodeRefinementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TacticFeatureRefinementTest.class);
	}

	/**
	 * Constructs a new Tactic Feature Refinement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TacticFeatureRefinementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Tactic Feature Refinement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TacticFeatureRefinement getFixture() {
		return (TacticFeatureRefinement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DmtFactory.eINSTANCE.createTacticFeatureRefinement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //TacticFeatureRefinementTest
