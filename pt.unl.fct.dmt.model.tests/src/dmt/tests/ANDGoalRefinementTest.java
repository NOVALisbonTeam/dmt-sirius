/**
 */
package dmt.tests;

import dmt.ANDGoalRefinement;
import dmt.DmtFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>AND Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ANDGoalRefinementTest extends GoalRelationshipTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ANDGoalRefinementTest.class);
	}

	/**
	 * Constructs a new AND Goal Refinement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ANDGoalRefinementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this AND Goal Refinement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ANDGoalRefinement getFixture() {
		return (ANDGoalRefinement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DmtFactory.eINSTANCE.createANDGoalRefinement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ANDGoalRefinementTest
