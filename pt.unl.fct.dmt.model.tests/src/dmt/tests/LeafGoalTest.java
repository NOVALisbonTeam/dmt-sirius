/**
 */
package dmt.tests;

import dmt.LeafGoal;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Leaf Goal</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class LeafGoalTest extends GoalTest {

	/**
	 * Constructs a new Leaf Goal test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafGoalTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Leaf Goal test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected LeafGoal getFixture() {
		return (LeafGoal)fixture;
	}

} //LeafGoalTest
