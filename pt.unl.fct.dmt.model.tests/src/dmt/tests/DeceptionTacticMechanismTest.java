/**
 */
package dmt.tests;

import dmt.DeceptionTacticMechanism;
import dmt.DmtFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Deception Tactic Mechanism</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeceptionTacticMechanismTest extends AbstractFeatureTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DeceptionTacticMechanismTest.class);
	}

	/**
	 * Constructs a new Deception Tactic Mechanism test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanismTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Deception Tactic Mechanism test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DeceptionTacticMechanism getFixture() {
		return (DeceptionTacticMechanism)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DmtFactory.eINSTANCE.createDeceptionTacticMechanism());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DeceptionTacticMechanismTest
