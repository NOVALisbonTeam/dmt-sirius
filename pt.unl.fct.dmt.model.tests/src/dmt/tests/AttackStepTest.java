/**
 */
package dmt.tests;

import dmt.AttackStep;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Attack Step</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AttackStepTest extends SecurityNodeTest {

	/**
	 * Constructs a new Attack Step test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackStepTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Attack Step test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected AttackStep getFixture() {
		return (AttackStep)fixture;
	}

} //AttackStepTest
