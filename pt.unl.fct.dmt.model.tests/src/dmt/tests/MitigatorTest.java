/**
 */
package dmt.tests;

import dmt.Mitigator;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Mitigator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MitigatorTest extends TestCase {

	/**
	 * The fixture for this Mitigator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Mitigator fixture = null;

	/**
	 * Constructs a new Mitigator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MitigatorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Mitigator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Mitigator fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Mitigator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Mitigator getFixture() {
		return fixture;
	}

} //MitigatorTest
