/**
 */
package dmt.tests;

import dmt.DeceptionTechnique;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Deception Technique</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DeceptionTechniqueTest extends DeceptionNodeTest {

	/**
	 * Constructs a new Deception Technique test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTechniqueTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Deception Technique test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DeceptionTechnique getFixture() {
		return (DeceptionTechnique)fixture;
	}

} //DeceptionTechniqueTest
