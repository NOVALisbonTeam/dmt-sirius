/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.tests;

import dmt.DeceptionStrategyTacticRefinement;
import dmt.DmtFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Deception Strategy Tactic Refinement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeceptionStrategyTacticRefinementTest extends NodeRefinementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DeceptionStrategyTacticRefinementTest.class);
	}

	/**
	 * Constructs a new Deception Strategy Tactic Refinement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategyTacticRefinementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Deception Strategy Tactic Refinement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DeceptionStrategyTacticRefinement getFixture() {
		return (DeceptionStrategyTacticRefinement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(DmtFactory.eINSTANCE.createDeceptionStrategyTacticRefinement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DeceptionStrategyTacticRefinementTest
