/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.tests;

import dmt.ObstructionElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Obstruction Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ObstructionElementTest extends GoalNodeTest {

	/**
	 * Constructs a new Obstruction Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstructionElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Obstruction Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ObstructionElement getFixture() {
		return (ObstructionElement)fixture;
	}

} //ObstructionElementTest
