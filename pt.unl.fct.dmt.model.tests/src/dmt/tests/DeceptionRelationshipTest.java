/**
 */
package dmt.tests;

import dmt.DeceptionRelationship;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Deception Relationship</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DeceptionRelationshipTest extends TestCase {

	/**
	 * The fixture for this Deception Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionRelationship fixture = null;

	/**
	 * Constructs a new Deception Relationship test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionRelationshipTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Deception Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(DeceptionRelationship fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Deception Relationship test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionRelationship getFixture() {
		return fixture;
	}

} //DeceptionRelationshipTest
