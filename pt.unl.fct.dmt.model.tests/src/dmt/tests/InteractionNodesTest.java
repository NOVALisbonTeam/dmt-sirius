/**
 */
package dmt.tests;

import dmt.InteractionNodes;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Interaction Nodes</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class InteractionNodesTest extends TestCase {

	/**
	 * The fixture for this Interaction Nodes test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionNodes fixture = null;

	/**
	 * Constructs a new Interaction Nodes test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionNodesTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Interaction Nodes test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(InteractionNodes fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Interaction Nodes test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionNodes getFixture() {
		return fixture;
	}

} //InteractionNodesTest
