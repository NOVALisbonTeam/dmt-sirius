/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.tests;

import dmt.Vulnerable;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Vulnerable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class VulnerableTest extends TestCase {

	/**
	 * The fixture for this Vulnerable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Vulnerable fixture = null;

	/**
	 * Constructs a new Vulnerable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VulnerableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Vulnerable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Vulnerable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Vulnerable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Vulnerable getFixture() {
		return fixture;
	}

} //VulnerableTest
