package pt.unl.fct.dmt.utils.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import dmt.RiskLikelihoodEnum;
import dmt.RiskSeverityEnum;
import pt.unl.fct.dmt.utils.RiskComputationService;
import pt.unl.fct.dmt.utils.RiskFactorScaleEnum;

class RiskComputationServiceTest {
	static RiskComputationService rc;
	
	@BeforeAll
	public static void initialize() {
		if (rc == null)
			rc = RiskComputationService.getInstance();
	}
	
	@Test
	void testGetRiskSeverityTable() {
		assertTrue(rc.getRiskSeverityTable().size() > 0);
	}

	@Test
	void testGetRiskProbabilityTable() {
		assertTrue(rc.getRiskProbabilityTable().size() > 0);
	}

	@Test
	void testLoadRiskTable() {
		rc.loadRiskTable();
		assertTrue(rc.getRiskProbabilityTable().size() > 0 && rc.getRiskSeverityTable().size() > 0);
	}

	@Test
	void testIsRiskTableLoaded()	{
		assertTrue(rc.isRiskTableLoaded());
	}
	
	@Test
	void testgetRiskFactor() {
		dmt.RiskLikelihoodEnum likelihood;
		dmt.RiskSeverityEnum severity;
		
		likelihood = RiskLikelihoodEnum.UNLIKELY;

		severity = RiskSeverityEnum.INSIGNIFICANT;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0016);
		severity = RiskSeverityEnum.MINOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.008);
		severity = RiskSeverityEnum.MODERATE;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0316);
		severity = RiskSeverityEnum.MAJOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0380);
		severity = RiskSeverityEnum.CATASTROPHIC;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0396);
		severity = RiskSeverityEnum.DOOMSDAY;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0400);

		likelihood = RiskLikelihoodEnum.RARE;

		severity = RiskSeverityEnum.INSIGNIFICANT;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0080);
		severity = RiskSeverityEnum.MINOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0400);
		severity = RiskSeverityEnum.MODERATE;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.1580);
		severity = RiskSeverityEnum.MAJOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.1900);
		severity = RiskSeverityEnum.CATASTROPHIC;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.1980);
		severity = RiskSeverityEnum.DOOMSDAY;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.2000);		
		
		likelihood = RiskLikelihoodEnum.POSSIBLE;

		severity = RiskSeverityEnum.INSIGNIFICANT;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0316);
		severity = RiskSeverityEnum.MINOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.158);
		severity = RiskSeverityEnum.MODERATE;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.6241);
		severity = RiskSeverityEnum.MAJOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.7505);
		severity = RiskSeverityEnum.CATASTROPHIC;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.7821);
		severity = RiskSeverityEnum.DOOMSDAY;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.7900);		


		likelihood = RiskLikelihoodEnum.LIKELY;

		severity = RiskSeverityEnum.INSIGNIFICANT;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0380);
		severity = RiskSeverityEnum.MINOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.190);
		severity = RiskSeverityEnum.MODERATE;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.7505);
		severity = RiskSeverityEnum.MAJOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.9025);
		severity = RiskSeverityEnum.CATASTROPHIC;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.9405);
		severity = RiskSeverityEnum.DOOMSDAY;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.9500);		
		
		likelihood = RiskLikelihoodEnum.ALMOSTCERTAIN;

		severity = RiskSeverityEnum.INSIGNIFICANT;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.0400);
		severity = RiskSeverityEnum.MINOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.200);
		severity = RiskSeverityEnum.MODERATE;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.7900);
		severity = RiskSeverityEnum.MAJOR;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.9500);
		severity = RiskSeverityEnum.CATASTROPHIC;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 0.9900);
		severity = RiskSeverityEnum.DOOMSDAY;
		assertTrue(rc.getRiskFactor(likelihood, severity) == 1.000);		
	}
	
	@Test
	void getScaleRiskFactorLOW() {
		// LOW(0.099), SOFT(0.3),  MODERATE(0.85), SEVERE(0.97), CRITICAL(1.00);
		assertTrue(rc.getScaleRiskFactor(0.0) == RiskFactorScaleEnum.LOW);	
		assertTrue(rc.getScaleRiskFactor(0.01) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.02) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.03) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.04) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.05) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.06) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.07) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.08) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.09) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.095) == RiskFactorScaleEnum.LOW);
		assertTrue(rc.getScaleRiskFactor(0.099) == RiskFactorScaleEnum.LOW);
	}
	
	@Test
	void getScaleRiskFactorSOFT() {
		// LOW(0.099), SOFT(0.3),  MODERATE(0.85), SEVERE(0.97), CRITICAL(1.00);
		assertTrue(rc.getScaleRiskFactor(0.1) == RiskFactorScaleEnum.SOFT);	
		assertTrue(rc.getScaleRiskFactor(0.15) == RiskFactorScaleEnum.SOFT);
		assertTrue(rc.getScaleRiskFactor(0.19) == RiskFactorScaleEnum.SOFT);
		assertTrue(rc.getScaleRiskFactor(0.21) == RiskFactorScaleEnum.SOFT);
		assertTrue(rc.getScaleRiskFactor(0.25) == RiskFactorScaleEnum.SOFT);
		assertTrue(rc.getScaleRiskFactor(0.3) == RiskFactorScaleEnum.SOFT);
	}	

	@Test
	void getScaleRiskFactorMODERATE() {
		// LOW(0.099), SOFT(0.3),  MODERATE(0.85), SEVERE(0.97), CRITICAL(1.00);
		assertTrue(rc.getScaleRiskFactor(0.35) == RiskFactorScaleEnum.MODERATE);	
		assertTrue(rc.getScaleRiskFactor(0.4) == RiskFactorScaleEnum.MODERATE);
		assertTrue(rc.getScaleRiskFactor(0.45) == RiskFactorScaleEnum.MODERATE);
		assertTrue(rc.getScaleRiskFactor(0.50) == RiskFactorScaleEnum.MODERATE);
		assertTrue(rc.getScaleRiskFactor(0.55) == RiskFactorScaleEnum.MODERATE);
		assertTrue(rc.getScaleRiskFactor(0.60) == RiskFactorScaleEnum.MODERATE);
		assertTrue(rc.getScaleRiskFactor(0.65) == RiskFactorScaleEnum.MODERATE);
		assertTrue(rc.getScaleRiskFactor(0.70) == RiskFactorScaleEnum.MODERATE);
		assertTrue(rc.getScaleRiskFactor(0.75) == RiskFactorScaleEnum.MODERATE);
		assertTrue(rc.getScaleRiskFactor(0.80) == RiskFactorScaleEnum.MODERATE);
		assertTrue(rc.getScaleRiskFactor(0.85) == RiskFactorScaleEnum.MODERATE);
		assertFalse(rc.getScaleRiskFactor(0.86) == RiskFactorScaleEnum.MODERATE);
	}	

	@Test
	void getScaleRiskFactorSEVERE() {
		// LOW(0.099), SOFT(0.3),  MODERATE(0.85), SEVERE(0.97), CRITICAL(1.00);
		assertTrue(rc.getScaleRiskFactor(0.86) == RiskFactorScaleEnum.SEVERE);	
		assertTrue(rc.getScaleRiskFactor(0.90) == RiskFactorScaleEnum.SEVERE);
		assertTrue(rc.getScaleRiskFactor(0.95) == RiskFactorScaleEnum.SEVERE);
		assertTrue(rc.getScaleRiskFactor(0.96) == RiskFactorScaleEnum.SEVERE);
		assertTrue(rc.getScaleRiskFactor(0.97) == RiskFactorScaleEnum.SEVERE);
		assertFalse(rc.getScaleRiskFactor(0.98) == RiskFactorScaleEnum.SEVERE);
	}	
	
	@Test
	void getScaleRiskFactorCRITICAL() {
		// LOW(0.099), SOFT(0.3),  MODERATE(0.85), SEVERE(0.97), CRITICAL(1.00);
		assertTrue(rc.getScaleRiskFactor(0.99) == RiskFactorScaleEnum.CRITICAL);	
		assertTrue(rc.getScaleRiskFactor(1.00) == RiskFactorScaleEnum.CRITICAL);
		assertTrue(rc.getScaleRiskFactor(1.01) == RiskFactorScaleEnum.CRITICAL);
	}		
}
