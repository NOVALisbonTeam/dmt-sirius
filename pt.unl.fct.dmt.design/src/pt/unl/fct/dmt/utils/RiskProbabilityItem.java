package pt.unl.fct.dmt.utils;

public class RiskProbabilityItem  extends RiskItem {

	public RiskProbabilityItem(String name, double value) {
		super(name, value);
	}
}
