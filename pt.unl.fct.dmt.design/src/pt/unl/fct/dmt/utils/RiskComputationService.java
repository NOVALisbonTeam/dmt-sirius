package pt.unl.fct.dmt.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//import org.apache.log4j.Logger;

public class RiskComputationService {
//	final static Logger logger = Logger.getLogger(RiskComputationService.class);
	
	private static RiskComputationService riskComputationService = null;
	
	private List<String> riskProbabilityKeys = Arrays.asList("risk.probability.rare", "risk.probability.unlikely", "risk.probability.possible", "risk.probability.likely", "risk.probability.almostcertain");
	private List<String> riskSeverityKeys = Arrays.asList("risk.severity.insignificant", "risk.severity.minor", "risk.severity.moderate", "risk.severity.major", "risk.severity.catastrophic", "risk.severity.doomsday");
			
	private List<RiskProbabilityItem> riskProbabilityTable = new ArrayList<RiskProbabilityItem>();
	private List<RiskSeverityItem> riskSeverityTable = new ArrayList<RiskSeverityItem>();;
	
	private RiskComputationService() {
		loadRiskTable();
	}
	
	public static RiskComputationService getInstance() {
		System.out.println("Getting an instance of RiskComputationService...");
		if (riskComputationService == null) {
			riskComputationService = new RiskComputationService();
		}
		return riskComputationService;
	}
	
	public List<RiskSeverityItem> getRiskSeverityTable() {
		return riskSeverityTable;
	}
	
	public List<RiskProbabilityItem> getRiskProbabilityTable() {
		return riskProbabilityTable;
	}

	public void loadRiskTable() {
		System.out.println("Loading Risk Table...");
		
		FilePropertyService fileService;
		try {
			fileService = new FilePropertyService();
			for (String pKey : riskProbabilityKeys)
				riskProbabilityTable.add(new RiskProbabilityItem(pKey, Double.valueOf(fileService.getValueByKey(pKey))));
			for (String sKey : riskSeverityKeys)
				riskSeverityTable.add(new RiskSeverityItem(sKey, Double.valueOf(fileService.getValueByKey(sKey))));
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Loading Risk Table finished...");
	}
	
	public boolean isRiskTableLoaded() {
		return (riskProbabilityTable.size() > 0 && riskSeverityTable.size() > 0);
	}
	
	public double getProbabilityIndexByKey (String key) {
		for (RiskProbabilityItem rp: riskProbabilityTable)
			if (rp.getName().equals(key))
				return rp.getValue();
		return 0.0;
	}

	public double getSeverityIndexByKey (String key) {
		for (RiskSeverityItem rs: riskSeverityTable)
			if (rs.getName().equals(key))
				return rs.getValue();
		return 0.0;
	}
	
	public double getRiskFactor (dmt.RiskLikelihoodEnum likelihood, dmt.RiskSeverityEnum severity) {
		double likelihoodValue, severityValue;
		likelihoodValue = getProbabilityIndexByKey(riskProbabilityKeys.get(likelihood.ordinal()));
		severityValue = getSeverityIndexByKey(riskSeverityKeys.get(severity.ordinal()));
		return (double)Math.round(likelihoodValue * severityValue * 100000d) / 100000d;
	}

	public RiskFactorScaleEnum getScaleRiskFactor (double riskFactor) {
		return RiskFactorScaleEnum.valueOfScale	(riskFactor);
	}
	
}
