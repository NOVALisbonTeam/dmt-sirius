package pt.unl.fct.dmt.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

//import org.apache.log4j.Logger;

public class FilePropertyService {
//	final static Logger logger = Logger.getLogger(FilePropertyService.class);
	private Properties prop = new Properties();
	private static String defaultFileConfigName = "dmt.config";
	private String fileConfigName;
	private InputStream input = null;
	
	public FilePropertyService(String fileName) throws IOException {
		if ( (fileName == null) || (fileName.isEmpty()) )
			fileConfigName = defaultFileConfigName;
		else
			fileConfigName = fileName;
		openPropertyFile();
	}
	
	public FilePropertyService() throws IOException {
		this(defaultFileConfigName);
	}

	public String getValueByKey(String key) {
		return prop.getProperty(key);
	}
	
	public Set<Object> getAllKey() {
		return prop.keySet();
	}

	
	private void openPropertyFile() throws IOException {
		input = FilePropertyService.class.getResourceAsStream(fileConfigName);
		if (input == null) 
			throw new IOException ("Configuration file " + fileConfigName + "not found.");
		prop.load(input);
	}	
}	
