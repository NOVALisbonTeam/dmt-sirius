package pt.unl.fct.dmt.utils;

import java.util.Arrays;
import java.util.Optional;

public enum RiskFactorScaleEnum {
	LOW(0.099), SOFT(0.3),  MODERATE(0.85), SEVERE(0.97), CRITICAL(1.00);

	private final double value;
	
	private RiskFactorScaleEnum(double scaleFactor) {
		this.value = scaleFactor;
	}
	
	public static Optional<RiskFactorScaleEnum> valueOf(double value) {
        return Arrays.stream(values())
            .filter(rf -> rf.value == value)
            .findFirst();
    }
	
	public static RiskFactorScaleEnum valueOfScale(double value) {
		if (value > 1.00)
			return CRITICAL;
        return Arrays.stream(values())
            .filter(rf -> (double)Math.round(value * 100000d) / 100000d <= (double)Math.round(rf.value * 100000d) / 100000d)
            .findFirst().get();
    }
}
