package pt.unl.fct.dmt.services;

import java.util.Collection;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.UsageCrossReferencer;

public class EMFService {

	public Integer getUsageReferenceNumber (EObject obj) {
		System.out.println(obj.eContainmentFeature());
		return obj.eClass().getEAllReferences().size();
//		Collection<Setting> crossReferences = EcoreUtil.UsageCrossReferencer.find(obj, EcoreUtil.getRootContainer(obj));
//		return crossReferences.size(); 
	}
}
