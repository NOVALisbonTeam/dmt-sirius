package pt.unl.fct.dmt.services;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.ui.business.api.dialect.DialectUIManager;
import org.eclipse.sirius.viewpoint.DAnalysis;
import org.eclipse.sirius.viewpoint.DRepresentation;
import org.eclipse.sirius.viewpoint.DView;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public class OpenTableService {

	private static final String REPRESENTATIONS_AIRD = "representations.aird";

	public boolean openAssetTable(EObject eObject) {
		System.out.println(eObject);
		boolean result = false;
		if (eObject instanceof dmt.Asset) {
//			dmt.Asset asset = (dmt.Asset)eObject;
			IProject project = getActiveProject();
			if (project != null) {
				IFile airdFile = project.getFile(REPRESENTATIONS_AIRD);
				if (airdFile.exists()) {
					// Step 1: get the .aird file and the corresponding Sirius Session
					URI representationsFileURI = URI.createURI("/" + airdFile.getProject().getName() + "/" + airdFile.getProjectRelativePath().toOSString());			
					Session session = SessionManager.INSTANCE.getSession(representationsFileURI , new NullProgressMonitor());

					// Step 2: get the DRepresentation to open
					DAnalysis root = (DAnalysis)session.getSessionResource().getContents().get(0);
					DView dView = root.getOwnedViews().get(0);
					DRepresentation myRepresentation = dView.getOwnedRepresentationDescriptors().get(0).getRepresentation();
					
					// Step 3: open representation
					DialectUIManager.INSTANCE.openEditor(session, myRepresentation, new NullProgressMonitor());
				}
			}	
		}
		return result;
	}	
	 
	private IProject getActiveProject() {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		IProject result = null;
		if (window != null) {
			IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
			Object firstElement = selection.getFirstElement();
			if (firstElement instanceof IAdaptable) 
				result = (IProject)((IAdaptable)firstElement).getAdapter(IProject.class);
		}
		return result;
	}
}
