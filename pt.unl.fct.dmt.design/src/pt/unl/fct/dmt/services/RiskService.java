package pt.unl.fct.dmt.services;

import java.util.Arrays;
import java.util.List;

//import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;

import dmt.RiskComponent;
import dmt.RiskSeverityEnum;
import pt.unl.fct.dmt.utils.RiskComputationService;
import pt.unl.fct.dmt.utils.RiskFactorScaleEnum;

public class RiskService {
//	final static Logger logger = Logger.getLogger(RiskService.class);
	private List<String> riskLevelAsString = Arrays.asList("Low", "Soft", "Moderate", "Severe", "Critical");
	
	public RiskService() {
		System.out.println("Starting Risk Service...");
	}
	
	private boolean isTheRiskFactorScaleSought (dmt.RiskComponent risk, RiskFactorScaleEnum riskFactorSough) {
		RiskComputationService rcs = RiskComputationService.getInstance();
		return rcs.getScaleRiskFactor(rcs.getRiskFactor(risk.getRiskLikelihood(), risk.getRiskSeverity())) == riskFactorSough;
	}
	
	public boolean isLowRisk(EObject eObject) {
		System.out.println("Checking lowRisk");
		boolean result = false;
		if (eObject instanceof dmt.RiskComponent) {
			dmt.RiskComponent rc = (dmt.RiskComponent)eObject;
			result = isTheRiskFactorScaleSought (rc, RiskFactorScaleEnum.LOW);
		}
		return result;
	}	

	public boolean isSoftRisk(EObject eObject) {
		boolean result = false;
		if (eObject instanceof dmt.RiskComponent) {
			dmt.RiskComponent rc = (dmt.RiskComponent)eObject;
			result = isTheRiskFactorScaleSought (rc, RiskFactorScaleEnum.SOFT);
		}
		return result;
	}

	public boolean isModerateRisk(EObject eObject) {
		boolean result = false;
		if (eObject instanceof dmt.RiskComponent) {
			dmt.RiskComponent rc = (dmt.RiskComponent)eObject;
			result = isTheRiskFactorScaleSought (rc, RiskFactorScaleEnum.MODERATE);
		}
		return result;
	}

	public boolean isSevereRisk(EObject eObject) {
		boolean result = false;
		if (eObject instanceof dmt.RiskComponent) {
			dmt.RiskComponent rc = (dmt.RiskComponent)eObject;
			result = isTheRiskFactorScaleSought (rc, RiskFactorScaleEnum.SEVERE);
		}
		return result;
	}

	public boolean isCriticalRisk(EObject eObject) {
		boolean result = false;
		if (eObject instanceof dmt.RiskComponent) {
			dmt.RiskComponent rc = (dmt.RiskComponent)eObject;
			result = isTheRiskFactorScaleSought (rc, RiskFactorScaleEnum.CRITICAL);
		}
		return result;
	}	
	
	public boolean isNoneRiskClassification(EObject eObject) {
		boolean result = false;
		if (eObject instanceof dmt.RiskComponent) {
			dmt.RiskComponent rc = (dmt.RiskComponent)eObject;
			result = (rc.getRiskSeverity() != RiskSeverityEnum.INSIGNIFICANT) &&
					 (rc.getRiskSeverity() != RiskSeverityEnum.MINOR)         &&
					 (rc.getRiskSeverity() != RiskSeverityEnum.MODERATE)      &&
					 (rc.getRiskSeverity() != RiskSeverityEnum.MAJOR)         &&
					 (rc.getRiskSeverity() != RiskSeverityEnum.CATASTROPHIC)  &&
					 (rc.getRiskSeverity() != RiskSeverityEnum.DOOMSDAY);
		}
		return result;
	}
	
	public RiskFactorScaleEnum getRiskLevel (RiskComponent risk) {
		RiskComputationService rcs = RiskComputationService.getInstance();
		return rcs.getScaleRiskFactor(rcs.getRiskFactor(risk.getRiskLikelihood(), risk.getRiskSeverity()));
	}
	
	public String getRiskLevelAsString (RiskFactorScaleEnum riskLevel) {
		return riskLevelAsString.get(riskLevel.ordinal());
	}
}
