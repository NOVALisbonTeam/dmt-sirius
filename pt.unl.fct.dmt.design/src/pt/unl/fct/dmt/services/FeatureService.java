package pt.unl.fct.dmt.services;

import org.eclipse.emf.ecore.EObject;

import dmt.DMTModel;
import dmt.DeceptionTacticMechanism;
import dmt.DmtFactory;
import dmt.TacticRefinement;

// Esse servi�o n�o � usado. Para servir como um item de menu, precisa ser um servi�o externo exportado como um plugin.
// Consulte o projeto featureServices junto ao model

public class FeatureService {
	private DMTModel container; 
	private DeceptionTacticMechanism tacticSource;
	private DeceptionTacticMechanism tacticTarget;
	private TacticRefinement tacticRefinement;

	public void createTacticArchetype (EObject obj) {
		System.out.println("Processing createTacticArchetype...");
		if (! (obj instanceof dmt.DeceptionTacticMechanism) )
			return;
		if (! (obj.eContainer() instanceof dmt.DMTModel) )
			return;
		tacticSource = (DeceptionTacticMechanism)obj;
		container = (DMTModel)tacticSource.eContainer();
		createDSetup();
		createDConfiguration();
		createDManagement();
		createDService();
		createDTermination();
	}
	
	private void createDSetup() {	
		tacticRefinement = DmtFactory.eINSTANCE.createTacticRefinement();
		tacticTarget = DmtFactory.eINSTANCE.createDeceptionTacticMechanism();
		tacticTarget.setName("D-Setup");
		
		container.getHasStrategyRelations().add(tacticRefinement);
		container.getHasStrategyNodes().add(tacticTarget);

		tacticRefinement.setTacticrefinementsource(tacticSource);
		tacticRefinement.setTacticrefinementtarget(tacticTarget);
	}
	
	private void createDConfiguration() {	
		tacticRefinement = DmtFactory.eINSTANCE.createTacticRefinement();
		tacticTarget = DmtFactory.eINSTANCE.createDeceptionTacticMechanism();
		tacticTarget.setName("D-Configuration");
		
		container.getHasStrategyRelations().add(tacticRefinement);
		container.getHasStrategyNodes().add(tacticTarget);

		tacticRefinement.setTacticrefinementsource(tacticSource);
		tacticRefinement.setTacticrefinementtarget(tacticTarget);
	}
	
	private void createDService() {
		tacticRefinement = DmtFactory.eINSTANCE.createTacticRefinement();
		tacticTarget = DmtFactory.eINSTANCE.createDeceptionTacticMechanism();
		tacticTarget.setName("D-Service");
		
		container.getHasStrategyRelations().add(tacticRefinement);
		container.getHasStrategyNodes().add(tacticTarget);

		tacticRefinement.setTacticrefinementsource(tacticSource);
		tacticRefinement.setTacticrefinementtarget(tacticTarget);		
	}
	
	private void createDManagement() {
		tacticRefinement = DmtFactory.eINSTANCE.createTacticRefinement();
		tacticTarget = DmtFactory.eINSTANCE.createDeceptionTacticMechanism();
		tacticTarget.setName("D-Management");
		
		container.getHasStrategyRelations().add(tacticRefinement);
		container.getHasStrategyNodes().add(tacticTarget);

		tacticRefinement.setTacticrefinementsource(tacticSource);
		tacticRefinement.setTacticrefinementtarget(tacticTarget);
	}
	
	private void createDTermination() {
		tacticRefinement = DmtFactory.eINSTANCE.createTacticRefinement();
		tacticTarget = DmtFactory.eINSTANCE.createDeceptionTacticMechanism();
		tacticTarget.setName("D-Termination");
		
		container.getHasStrategyRelations().add(tacticRefinement);
		container.getHasStrategyNodes().add(tacticTarget);

		tacticRefinement.setTacticrefinementsource(tacticSource);
		tacticRefinement.setTacticrefinementtarget(tacticTarget);		
	}
	
}
