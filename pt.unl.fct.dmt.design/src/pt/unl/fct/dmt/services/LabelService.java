package pt.unl.fct.dmt.services;

//import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EObject;

import dmt.AttackConnectorEnum;
import dmt.BehaviourGoal;
import dmt.DeceptionBehaviourGoal;
import dmt.DeceptionSoftGoal;
import dmt.DeceptionTacticMechanism;
import dmt.GoalTypeEnum;
import dmt.Node;
import dmt.SoftGoal;
import dmt.SoftGoalTypeEnum;

public class LabelService {
//	final static Logger logger = Logger.getLogger(LabelService.class);

	public String getPropertyATypeLabel(EObject eObject) {
		System.out.println("Executing service getPropertyATypeLabel...");
		String result = "";
		if (eObject instanceof dmt.AProperty) {
			dmt.AProperty p = (dmt.AProperty)eObject;
			result = p.getName() + ":" + p.getAtype();
		}
		return result;
	}
	
	public String getRiskLevelLabel(EObject eObject) {
		System.out.println("Executing service getRisklevelLabel...");
//		System.out.println(eObject);
		String result = "none";
		if (eObject instanceof dmt.RiskComponent) {
			dmt.RiskComponent r = (dmt.RiskComponent)eObject;
			RiskService riskService = new RiskService();
			result = riskService.getRiskLevelAsString(riskService.getRiskLevel(r));
		}
		return result;
	}
	
	public String myTrace(EObject eObject) {
		System.out.println(eObject);
		return "ok";
	}

	public String getAttackStepConnectorLabelTree(EObject eObject) {
		System.out.println("Executing service getAttackStepConnectorLabelTree...");
		String result = "none";
		if (eObject instanceof dmt.AttackStepConnector) {
			dmt.AttackStepConnector s = (dmt.AttackStepConnector)eObject;
			if (s.getAttackStepConnectorType() == AttackConnectorEnum.OR)
				result = s.getSequence() + " - " + "(Or)" + "   " + s.getName(); 
			else
			if (s.getAttackStepConnectorType() == AttackConnectorEnum.AND)
				result = s.getSequence() + " - " + "(And)" + " " + s.getName();
			else
				result = s.getSequence() + " - " + s.getName();
		}
		return result;
	}
	
	public String getGeneralGoalLabel (Node node, GoalTypeEnum goalType) {
		String result = "";
			switch (goalType) {
			case OTHER:
				result = node.getName();
				break;
			default:
				result = toFirstLetterUpper(goalType.getName()) + "[" + node.getName() + "]";
				break;
			}	
		return result;	
	}
			
	public String getGoalLabel(EObject eObject) {
		String result = "";
		Node node;		
		if ( !(eObject instanceof Node) ) 
			return result; 
		node = (Node)eObject;
		result = node.getName();
				
		if (eObject instanceof BehaviourGoal) {
			BehaviourGoal goal = (BehaviourGoal)eObject;
			result = getGeneralGoalLabel (node, goal.getType());
		}
		return result;
	}

	public String getDeceptionGoalLabel(EObject eObject) {
		String result = "";
		Node node;
		if ( !(eObject instanceof Node) ) 
			return result; 
		node = (Node)eObject;
		result = node.getName();
				
		if (eObject instanceof DeceptionBehaviourGoal) {
			DeceptionBehaviourGoal goal = (DeceptionBehaviourGoal)eObject;
			result = getGeneralGoalLabel (node, goal.getType());
		}
		return result;
	}
	
	public String getSoftGoalLabel(EObject eObject) {
		String result = "";
		Node node;
		if (eObject instanceof Node) 
			node = (Node)eObject;
		else
			return result;
			
		if (eObject instanceof SoftGoal) {
			SoftGoal goal = (SoftGoal)eObject;
			result = getGeneralSoftGoal(node, goal.getSoftGoalType(), goal.getSoftGoalOpenType());
		}
		return result;
	}
	
	public String getDeceptionSoftGoalLabel(EObject eObject) {
		String result = "";
		Node node;
		if (eObject instanceof Node) 
			node = (Node)eObject;
		else
			return result;

		if (eObject instanceof DeceptionSoftGoal) {
			DeceptionSoftGoal goal = (DeceptionSoftGoal)eObject;
			result = getGeneralSoftGoal(node, goal.getSoftGoalType(), goal.getSoftGoalOpenType());
		}
		return result;
	}
	

	private String getGeneralSoftGoal(Node node, SoftGoalTypeEnum goalType, String openType) {
		String result = "";
//		if (null != node)
//			result = node.getName();
//		if (null != goalType)
//			result = result + goalType.toString();
//		if (null != openType)
//			result = result + openType;
		if (null == openType)
			openType = "";
		switch (goalType) {
			case OTHER: 
				if (openType.isEmpty())
					result = node.getName();
				else	
					result = toFirstLetterUpper(openType) + "[" + node.getName() + "]";
				break;
			default:
				result = toFirstLetterUpper(goalType.getName()) + "[" + node.getName() + "]";;
				break;
		}
		return result;
	}		
	
	public String getTacticMechanismName(EObject eObject) {
		String result = "";
		if (eObject instanceof DeceptionTacticMechanism) {
			DeceptionTacticMechanism tactic = (DeceptionTacticMechanism)eObject;
			if (null == tactic.getTacticReference())
				result = tactic.getName();
			else
				result = tactic.getTacticReference().getName();
		}
		return result;
	}
	
	private String toFirstLetterUpper (String s) {
		return s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase();
	}
}
