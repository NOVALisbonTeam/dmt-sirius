package pt.unl.fct.dmt.services;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecoretools.design.service.AutosizeTrigger;
import org.eclipse.sirius.business.api.session.Session;
import org.eclipse.sirius.business.api.session.SessionManager;
import org.eclipse.sirius.diagram.DNode;
import org.eclipse.sirius.diagram.DNodeList;
import org.eclipse.sirius.diagram.DSemanticDiagram;
import org.eclipse.sirius.diagram.business.internal.query.DDiagramInternalQuery;
import org.eclipse.sirius.viewpoint.DSemanticDecorator;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import dmt.Attack;
import dmt.ConcreteAsset;
import dmt.DeceptionTactic;
import dmt.DeceptionTacticMechanism;
import dmt.Goal;
import dmt.MitigableElement;
import dmt.RiskComponent;
import dmt.Threat;
import dmt.Vulnerability;

public class DesignService {
	public EObject markForAutosize(EObject any) {
		if (any != null) {
			any.eAdapters().add(AutosizeTrigger.AUTO_SIZE_MARKER);
		}
		return any;
	}
	
	public List<EObject> getSystemGoalsForDiagram(final EObject element, final DSemanticDecorator containerView) {
		Predicate<EObject> validForDeceptionDiagram = new Predicate<EObject>() {

			public boolean apply(EObject input) {
				return input instanceof Goal;
			}
		};
		return allValidSessionElements(element, validForDeceptionDiagram);
	}
	
	public List<EObject> getTacticsForDiagram(final EObject element, final DSemanticDecorator containerView) {
		Predicate<EObject> validForDeceptionDiagram = new Predicate<EObject>() {

			public boolean apply(EObject input) {
				return input instanceof DeceptionTactic;
			}
		};
		return allValidSessionElements(element, validForDeceptionDiagram);
	}
	
	public List<EObject> getSystemAssetsForDiagram(final EObject element, final DSemanticDecorator containerView) {
		Predicate<EObject> validForDeceptionDiagram = new Predicate<EObject>() {

			public boolean apply(EObject input) {
				return input instanceof ConcreteAsset;
			}
		};
		return allValidSessionElements(element, validForDeceptionDiagram);
	}

	public List<EObject> getSystemThreatsForDiagram(final EObject element, final DSemanticDecorator containerView) {
		Predicate<EObject> validForDeceptionDiagram = new Predicate<EObject>() {

			public boolean apply(EObject input) {
				return input instanceof MitigableElement;
			}
		};
		return allValidSessionElements(element, validForDeceptionDiagram);
	}

	public List<EObject> getTacticsMechanismForDiagram(final EObject element, final DSemanticDecorator containerView) {
		Predicate<EObject> validForDeceptionDiagram = new Predicate<EObject>() {

			public boolean apply(EObject input) {
				return input instanceof DeceptionTacticMechanism;
			}
		};
		return allValidSessionElements(element, validForDeceptionDiagram);
	}

	private List<EObject> allValidSessionElements(EObject cur, Predicate<EObject> validForDeceptionDiagram) {
		Session found = SessionManager.INSTANCE.getSession(cur);
		List<EObject> result = Lists.newArrayList();
		if (found != null) {
			for (Resource res : found.getSemanticResources()) {				
				if (res.getURI().isPlatformResource() || res.getURI().isPlatformPlugin()) {
					Iterators.addAll(result, Iterators.filter(res.getAllContents(), validForDeceptionDiagram));
				}
			}
		}
		return result;
	}

	public Set<Goal> getDisplayedGoals(DSemanticDiagram diagram) {
		Set<Goal> result = Sets.newLinkedHashSet();
		for (DNode node : diagram.getNodes())
			for (EObject semanticNode : node.getSemanticElements())
				if (semanticNode instanceof Goal)
					result.add((Goal) semanticNode);
//        Iterator<DNodeList> it = Iterators.filter(new DDiagramInternalQuery(diagram).getContainers().iterator(),
//                DNodeList.class);
//        
//		System.out.println(diagram.getContainers().size());
//
//        while (it.hasNext()) {
//			DNodeList dec = it.next();
//			System.out.println(dec.isVisible() + " $$$ " + dec.getTarget());
////			if (dec.getTarget() instanceof Goal)
////				System.out.println(dec.isVisible() + " $$$ " + dec.getTarget());					
//			if (dec.getTarget() instanceof Goal && dec.isVisible()) {
//
//				result.add((Goal) dec.getTarget());
//			}
//		}
		return result;
	}	
	
	public Set<ConcreteAsset> getDisplayedAssets(DSemanticDiagram diagram) {
		Set<ConcreteAsset> result = Sets.newLinkedHashSet();
		
		Iterator<DNodeList> it = Iterators.filter(new DDiagramInternalQuery(diagram).getContainers().iterator(),
				DNodeList.class);
		while (it.hasNext()) {
			DNodeList dec = it.next();
			if (dec.getTarget() instanceof ConcreteAsset && dec.isVisible()) {
				result.add((ConcreteAsset) dec.getTarget());
			}
		}
		return result;		
	}
	
	public Set<MitigableElement> getDisplayedMitigable(DSemanticDiagram diagram) {
		Set<MitigableElement> result = Sets.newLinkedHashSet();
		for (DNode node : diagram.getNodes())			
			for (EObject semanticNode : node.getSemanticElements())
				if ( (semanticNode instanceof MitigableElement) && (node.isVisible()) )
					result.add((MitigableElement) semanticNode);
	
		return result;		
	}
	
		public Boolean isThreat(EObject any) {
			return any instanceof Threat;
	}	
		
		public Boolean isAttack(EObject any) {
			return any instanceof Attack;
	}		
		
		public Boolean isVulnerability(EObject any) {
		return any instanceof Vulnerability;
	}		
		
		public Boolean isRiskComponent(EObject any) {
		return any instanceof RiskComponent;
	}		
}
