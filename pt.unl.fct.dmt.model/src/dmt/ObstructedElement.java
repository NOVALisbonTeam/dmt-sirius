/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Obstructed Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getObstructedElement()
 * @model abstract="true"
 * @generated
 */
public interface ObstructedElement extends EObject {
} // ObstructedElement
