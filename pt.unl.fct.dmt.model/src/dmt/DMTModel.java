/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DMT Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DMTModel#getHasSecurityNodes <em>Has Security Nodes</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasDeceptionNodes <em>Has Deception Nodes</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasGoalNodes <em>Has Goal Nodes</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasGoalRelations <em>Has Goal Relations</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasStrategyNodes <em>Has Strategy Nodes</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasStrategyRelations <em>Has Strategy Relations</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasDeceptionRelations <em>Has Deception Relations</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasSecurityRelations <em>Has Security Relations</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasInfluenceNodes <em>Has Influence Nodes</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasInfluenceRelations <em>Has Influence Relations</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasGoalContainers <em>Has Goal Containers</em>}</li>
 *   <li>{@link dmt.DMTModel#getHasMiscNodes <em>Has Misc Nodes</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDMTModel()
 * @model
 * @generated
 */
public interface DMTModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Has Security Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.SecurityNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Security Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Security Nodes</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasSecurityNodes()
	 * @model containment="true"
	 * @generated
	 */
	EList<SecurityNode> getHasSecurityNodes();

	/**
	 * Returns the value of the '<em><b>Has Deception Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.DeceptionNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Deception Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Deception Nodes</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasDeceptionNodes()
	 * @model containment="true"
	 * @generated
	 */
	EList<DeceptionNode> getHasDeceptionNodes();

	/**
	 * Returns the value of the '<em><b>Has Goal Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.GoalNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Goal Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Goal Nodes</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasGoalNodes()
	 * @model containment="true"
	 * @generated
	 */
	EList<GoalNode> getHasGoalNodes();

	/**
	 * Returns the value of the '<em><b>Has Goal Relations</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.GoalRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Goal Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Goal Relations</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasGoalRelations()
	 * @model containment="true"
	 * @generated
	 */
	EList<GoalRelationship> getHasGoalRelations();

	/**
	 * Returns the value of the '<em><b>Has Strategy Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.StrategyNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Strategy Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Strategy Nodes</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasStrategyNodes()
	 * @model containment="true"
	 * @generated
	 */
	EList<StrategyNode> getHasStrategyNodes();

	/**
	 * Returns the value of the '<em><b>Has Strategy Relations</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.StrategyRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Strategy Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Strategy Relations</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasStrategyRelations()
	 * @model containment="true"
	 * @generated
	 */
	EList<StrategyRelationship> getHasStrategyRelations();

	/**
	 * Returns the value of the '<em><b>Has Deception Relations</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.DeceptionRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Deception Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Deception Relations</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasDeceptionRelations()
	 * @model containment="true"
	 * @generated
	 */
	EList<DeceptionRelationship> getHasDeceptionRelations();

	/**
	 * Returns the value of the '<em><b>Has Security Relations</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.SecurityRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Security Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Security Relations</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasSecurityRelations()
	 * @model containment="true"
	 * @generated
	 */
	EList<SecurityRelationship> getHasSecurityRelations();

	/**
	 * Returns the value of the '<em><b>Has Influence Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.InfluenceNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Influence Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Influence Nodes</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasInfluenceNodes()
	 * @model containment="true"
	 * @generated
	 */
	EList<InfluenceNode> getHasInfluenceNodes();

	/**
	 * Returns the value of the '<em><b>Has Influence Relations</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.InfluenceRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Influence Relations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Influence Relations</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasInfluenceRelations()
	 * @model containment="true"
	 * @generated
	 */
	EList<InfluenceRelationship> getHasInfluenceRelations();

	/**
	 * Returns the value of the '<em><b>Has Goal Containers</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.GoalContainer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Goal Containers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Goal Containers</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasGoalContainers()
	 * @model containment="true"
	 * @generated
	 */
	EList<GoalContainer> getHasGoalContainers();

	/**
	 * Returns the value of the '<em><b>Has Misc Nodes</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.Annotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Misc Nodes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Misc Nodes</em>' containment reference list.
	 * @see dmt.DmtPackage#getDMTModel_HasMiscNodes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Annotation> getHasMiscNodes();

} // DMTModel
