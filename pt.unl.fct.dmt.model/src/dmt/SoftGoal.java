/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Soft Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.SoftGoal#getFitCriterion <em>Fit Criterion</em>}</li>
 *   <li>{@link dmt.SoftGoal#getSoftGoalType <em>Soft Goal Type</em>}</li>
 *   <li>{@link dmt.SoftGoal#getSoftGoalOpenType <em>Soft Goal Open Type</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getSoftGoal()
 * @model
 * @generated
 */
public interface SoftGoal extends Goal {
	/**
	 * Returns the value of the '<em><b>Fit Criterion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fit Criterion</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fit Criterion</em>' attribute.
	 * @see #setFitCriterion(String)
	 * @see dmt.DmtPackage#getSoftGoal_FitCriterion()
	 * @model
	 * @generated
	 */
	String getFitCriterion();

	/**
	 * Sets the value of the '{@link dmt.SoftGoal#getFitCriterion <em>Fit Criterion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fit Criterion</em>' attribute.
	 * @see #getFitCriterion()
	 * @generated
	 */
	void setFitCriterion(String value);

	/**
	 * Returns the value of the '<em><b>Soft Goal Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.SoftGoalTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Soft Goal Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Soft Goal Type</em>' attribute.
	 * @see dmt.SoftGoalTypeEnum
	 * @see #setSoftGoalType(SoftGoalTypeEnum)
	 * @see dmt.DmtPackage#getSoftGoal_SoftGoalType()
	 * @model
	 * @generated
	 */
	SoftGoalTypeEnum getSoftGoalType();

	/**
	 * Sets the value of the '{@link dmt.SoftGoal#getSoftGoalType <em>Soft Goal Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Soft Goal Type</em>' attribute.
	 * @see dmt.SoftGoalTypeEnum
	 * @see #getSoftGoalType()
	 * @generated
	 */
	void setSoftGoalType(SoftGoalTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Soft Goal Open Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Soft Goal Open Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Soft Goal Open Type</em>' attribute.
	 * @see #setSoftGoalOpenType(String)
	 * @see dmt.DmtPackage#getSoftGoal_SoftGoalOpenType()
	 * @model
	 * @generated
	 */
	String getSoftGoalOpenType();

	/**
	 * Sets the value of the '{@link dmt.SoftGoal#getSoftGoalOpenType <em>Soft Goal Open Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Soft Goal Open Type</em>' attribute.
	 * @see #getSoftGoalOpenType()
	 * @generated
	 */
	void setSoftGoalOpenType(String value);

} // SoftGoal
