/**
 */
package dmt;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Obstacle Category Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage#getObstacleCategoryEnum()
 * @model
 * @generated
 */
public enum ObstacleCategoryEnum implements Enumerator {
	/**
	 * The '<em><b>INFOUNAVAILABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INFOUNAVAILABLE_VALUE
	 * @generated
	 * @ordered
	 */
	INFOUNAVAILABLE(0, "INFOUNAVAILABLE", "INFOUNAVAILABLE"), /**
	 * The '<em><b>INFONOTINTIME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INFONOTINTIME_VALUE
	 * @generated
	 * @ordered
	 */
	INFONOTINTIME(1, "INFONOTINTIME", "INFONOTINTIME"), /**
	 * The '<em><b>WRONGBELIEF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #WRONGBELIEF_VALUE
	 * @generated
	 * @ordered
	 */
	WRONGBELIEF(2, "WRONGBELIEF", "WRONGBELIEF"), /**
	 * The '<em><b>STIMULUSRESPONSEPROBLEM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STIMULUSRESPONSEPROBLEM_VALUE
	 * @generated
	 * @ordered
	 */
	STIMULUSRESPONSEPROBLEM(3, "STIMULUSRESPONSEPROBLEM", "STIMULUSRESPONSEPROBLEM");

	/**
	 * The '<em><b>INFOUNAVAILABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INFOUNAVAILABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INFOUNAVAILABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INFOUNAVAILABLE_VALUE = 0;

	/**
	 * The '<em><b>INFONOTINTIME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INFONOTINTIME</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INFONOTINTIME
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INFONOTINTIME_VALUE = 1;

	/**
	 * The '<em><b>WRONGBELIEF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>WRONGBELIEF</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WRONGBELIEF
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int WRONGBELIEF_VALUE = 2;

	/**
	 * The '<em><b>STIMULUSRESPONSEPROBLEM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>STIMULUSRESPONSEPROBLEM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STIMULUSRESPONSEPROBLEM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STIMULUSRESPONSEPROBLEM_VALUE = 3;

	/**
	 * An array of all the '<em><b>Obstacle Category Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ObstacleCategoryEnum[] VALUES_ARRAY =
		new ObstacleCategoryEnum[] {
			INFOUNAVAILABLE,
			INFONOTINTIME,
			WRONGBELIEF,
			STIMULUSRESPONSEPROBLEM,
		};

	/**
	 * A public read-only list of all the '<em><b>Obstacle Category Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ObstacleCategoryEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Obstacle Category Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ObstacleCategoryEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ObstacleCategoryEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Obstacle Category Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ObstacleCategoryEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ObstacleCategoryEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Obstacle Category Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ObstacleCategoryEnum get(int value) {
		switch (value) {
			case INFOUNAVAILABLE_VALUE: return INFOUNAVAILABLE;
			case INFONOTINTIME_VALUE: return INFONOTINTIME;
			case WRONGBELIEF_VALUE: return WRONGBELIEF;
			case STIMULUSRESPONSEPROBLEM_VALUE: return STIMULUSRESPONSEPROBLEM;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ObstacleCategoryEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ObstacleCategoryEnum
