/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.GoalNode;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goal Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class GoalNodeImpl extends NodeImpl implements GoalNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.GOAL_NODE;
	}

} //GoalNodeImpl
