/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.Mitigator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mitigator</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class MitigatorImpl extends MinimalEObjectImpl.Container implements Mitigator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MitigatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.MITIGATOR;
	}

} //MitigatorImpl
