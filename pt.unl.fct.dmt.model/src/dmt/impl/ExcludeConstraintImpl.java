/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.ExcludeConstraint;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exclude Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExcludeConstraintImpl extends FeatureConstraintImpl implements ExcludeConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExcludeConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.EXCLUDE_CONSTRAINT;
	}

} //ExcludeConstraintImpl
