/**
 */
package dmt.impl;

import dmt.AttackTree;
import dmt.AttackVector;
import dmt.DmtPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attack Tree</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.AttackTreeImpl#getHasAttackvector <em>Has Attackvector</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttackTreeImpl extends SecurityNodeImpl implements AttackTree {
	/**
	 * The cached value of the '{@link #getHasAttackvector() <em>Has Attackvector</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAttackvector()
	 * @generated
	 * @ordered
	 */
	protected EList<AttackVector> hasAttackvector;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttackTreeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.ATTACK_TREE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttackVector> getHasAttackvector() {
		if (hasAttackvector == null) {
			hasAttackvector = new EObjectResolvingEList<AttackVector>(AttackVector.class, this, DmtPackage.ATTACK_TREE__HAS_ATTACKVECTOR);
		}
		return hasAttackvector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.ATTACK_TREE__HAS_ATTACKVECTOR:
				return getHasAttackvector();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.ATTACK_TREE__HAS_ATTACKVECTOR:
				getHasAttackvector().clear();
				getHasAttackvector().addAll((Collection<? extends AttackVector>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACK_TREE__HAS_ATTACKVECTOR:
				getHasAttackvector().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACK_TREE__HAS_ATTACKVECTOR:
				return hasAttackvector != null && !hasAttackvector.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AttackTreeImpl
