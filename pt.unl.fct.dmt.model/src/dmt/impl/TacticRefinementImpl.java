/**
 */
package dmt.impl;

import dmt.DeceptionTacticMechanism;
import dmt.DmtPackage;
import dmt.TacticRefinement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tactic Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.TacticRefinementImpl#getTacticrefinementtarget <em>Tacticrefinementtarget</em>}</li>
 *   <li>{@link dmt.impl.TacticRefinementImpl#getTacticrefinementsource <em>Tacticrefinementsource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TacticRefinementImpl extends NodeRefinementImpl implements TacticRefinement {
	/**
	 * The cached value of the '{@link #getTacticrefinementtarget() <em>Tacticrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTacticrefinementtarget()
	 * @generated
	 * @ordered
	 */
	protected DeceptionTacticMechanism tacticrefinementtarget;

	/**
	 * The cached value of the '{@link #getTacticrefinementsource() <em>Tacticrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTacticrefinementsource()
	 * @generated
	 * @ordered
	 */
	protected DeceptionTacticMechanism tacticrefinementsource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TacticRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.TACTIC_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism getTacticrefinementtarget() {
		if (tacticrefinementtarget != null && tacticrefinementtarget.eIsProxy()) {
			InternalEObject oldTacticrefinementtarget = (InternalEObject)tacticrefinementtarget;
			tacticrefinementtarget = (DeceptionTacticMechanism)eResolveProxy(oldTacticrefinementtarget);
			if (tacticrefinementtarget != oldTacticrefinementtarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTTARGET, oldTacticrefinementtarget, tacticrefinementtarget));
			}
		}
		return tacticrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism basicGetTacticrefinementtarget() {
		return tacticrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTacticrefinementtarget(DeceptionTacticMechanism newTacticrefinementtarget) {
		DeceptionTacticMechanism oldTacticrefinementtarget = tacticrefinementtarget;
		tacticrefinementtarget = newTacticrefinementtarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTTARGET, oldTacticrefinementtarget, tacticrefinementtarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism getTacticrefinementsource() {
		if (tacticrefinementsource != null && tacticrefinementsource.eIsProxy()) {
			InternalEObject oldTacticrefinementsource = (InternalEObject)tacticrefinementsource;
			tacticrefinementsource = (DeceptionTacticMechanism)eResolveProxy(oldTacticrefinementsource);
			if (tacticrefinementsource != oldTacticrefinementsource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTSOURCE, oldTacticrefinementsource, tacticrefinementsource));
			}
		}
		return tacticrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism basicGetTacticrefinementsource() {
		return tacticrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTacticrefinementsource(DeceptionTacticMechanism newTacticrefinementsource) {
		DeceptionTacticMechanism oldTacticrefinementsource = tacticrefinementsource;
		tacticrefinementsource = newTacticrefinementsource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTSOURCE, oldTacticrefinementsource, tacticrefinementsource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTTARGET:
				if (resolve) return getTacticrefinementtarget();
				return basicGetTacticrefinementtarget();
			case DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTSOURCE:
				if (resolve) return getTacticrefinementsource();
				return basicGetTacticrefinementsource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTTARGET:
				setTacticrefinementtarget((DeceptionTacticMechanism)newValue);
				return;
			case DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTSOURCE:
				setTacticrefinementsource((DeceptionTacticMechanism)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTTARGET:
				setTacticrefinementtarget((DeceptionTacticMechanism)null);
				return;
			case DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTSOURCE:
				setTacticrefinementsource((DeceptionTacticMechanism)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTTARGET:
				return tacticrefinementtarget != null;
			case DmtPackage.TACTIC_REFINEMENT__TACTICREFINEMENTSOURCE:
				return tacticrefinementsource != null;
		}
		return super.eIsSet(featureID);
	}

} //TacticRefinementImpl
