/**
 */
package dmt.impl;

import dmt.ConcreteAsset;
import dmt.DmtPackage;
import dmt.Simulation;
import dmt.SimulationTechniqueTypeEnum;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simulation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.SimulationImpl#getType <em>Type</em>}</li>
 *   <li>{@link dmt.impl.SimulationImpl#getAssetReference <em>Asset Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SimulationImpl extends DeceptionTechniqueImpl implements Simulation {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final SimulationTechniqueTypeEnum TYPE_EDEFAULT = SimulationTechniqueTypeEnum.MIMICKING;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected SimulationTechniqueTypeEnum type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAssetReference() <em>Asset Reference</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetReference()
	 * @generated
	 * @ordered
	 */
	protected EList<ConcreteAsset> assetReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimulationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.SIMULATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationTechniqueTypeEnum getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(SimulationTechniqueTypeEnum newType) {
		SimulationTechniqueTypeEnum oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SIMULATION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConcreteAsset> getAssetReference() {
		if (assetReference == null) {
			assetReference = new EObjectResolvingEList<ConcreteAsset>(ConcreteAsset.class, this, DmtPackage.SIMULATION__ASSET_REFERENCE);
		}
		return assetReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.SIMULATION__TYPE:
				return getType();
			case DmtPackage.SIMULATION__ASSET_REFERENCE:
				return getAssetReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.SIMULATION__TYPE:
				setType((SimulationTechniqueTypeEnum)newValue);
				return;
			case DmtPackage.SIMULATION__ASSET_REFERENCE:
				getAssetReference().clear();
				getAssetReference().addAll((Collection<? extends ConcreteAsset>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.SIMULATION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case DmtPackage.SIMULATION__ASSET_REFERENCE:
				getAssetReference().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.SIMULATION__TYPE:
				return type != TYPE_EDEFAULT;
			case DmtPackage.SIMULATION__ASSET_REFERENCE:
				return assetReference != null && !assetReference.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //SimulationImpl
