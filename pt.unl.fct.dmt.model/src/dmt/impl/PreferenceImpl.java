/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.Preference;
import dmt.PreferenceWeightEnum;
import dmt.QualityAttribute;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Preference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.PreferenceImpl#getPreferenceTarget <em>Preference Target</em>}</li>
 *   <li>{@link dmt.impl.PreferenceImpl#getPreferenceSource <em>Preference Source</em>}</li>
 *   <li>{@link dmt.impl.PreferenceImpl#getWeight <em>Weight</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PreferenceImpl extends InfluenceRelationshipImpl implements Preference {
	/**
	 * The cached value of the '{@link #getPreferenceTarget() <em>Preference Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreferenceTarget()
	 * @generated
	 * @ordered
	 */
	protected QualityAttribute preferenceTarget;

	/**
	 * The cached value of the '{@link #getPreferenceSource() <em>Preference Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreferenceSource()
	 * @generated
	 * @ordered
	 */
	protected QualityAttribute preferenceSource;

	/**
	 * The default value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected static final PreferenceWeightEnum WEIGHT_EDEFAULT = PreferenceWeightEnum.LOW;

	/**
	 * The cached value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected PreferenceWeightEnum weight = WEIGHT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PreferenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.PREFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualityAttribute getPreferenceTarget() {
		if (preferenceTarget != null && preferenceTarget.eIsProxy()) {
			InternalEObject oldPreferenceTarget = (InternalEObject)preferenceTarget;
			preferenceTarget = (QualityAttribute)eResolveProxy(oldPreferenceTarget);
			if (preferenceTarget != oldPreferenceTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.PREFERENCE__PREFERENCE_TARGET, oldPreferenceTarget, preferenceTarget));
			}
		}
		return preferenceTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualityAttribute basicGetPreferenceTarget() {
		return preferenceTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreferenceTarget(QualityAttribute newPreferenceTarget) {
		QualityAttribute oldPreferenceTarget = preferenceTarget;
		preferenceTarget = newPreferenceTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.PREFERENCE__PREFERENCE_TARGET, oldPreferenceTarget, preferenceTarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualityAttribute getPreferenceSource() {
		if (preferenceSource != null && preferenceSource.eIsProxy()) {
			InternalEObject oldPreferenceSource = (InternalEObject)preferenceSource;
			preferenceSource = (QualityAttribute)eResolveProxy(oldPreferenceSource);
			if (preferenceSource != oldPreferenceSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.PREFERENCE__PREFERENCE_SOURCE, oldPreferenceSource, preferenceSource));
			}
		}
		return preferenceSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualityAttribute basicGetPreferenceSource() {
		return preferenceSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreferenceSource(QualityAttribute newPreferenceSource) {
		QualityAttribute oldPreferenceSource = preferenceSource;
		preferenceSource = newPreferenceSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.PREFERENCE__PREFERENCE_SOURCE, oldPreferenceSource, preferenceSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreferenceWeightEnum getWeight() {
		return weight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWeight(PreferenceWeightEnum newWeight) {
		PreferenceWeightEnum oldWeight = weight;
		weight = newWeight == null ? WEIGHT_EDEFAULT : newWeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.PREFERENCE__WEIGHT, oldWeight, weight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.PREFERENCE__PREFERENCE_TARGET:
				if (resolve) return getPreferenceTarget();
				return basicGetPreferenceTarget();
			case DmtPackage.PREFERENCE__PREFERENCE_SOURCE:
				if (resolve) return getPreferenceSource();
				return basicGetPreferenceSource();
			case DmtPackage.PREFERENCE__WEIGHT:
				return getWeight();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.PREFERENCE__PREFERENCE_TARGET:
				setPreferenceTarget((QualityAttribute)newValue);
				return;
			case DmtPackage.PREFERENCE__PREFERENCE_SOURCE:
				setPreferenceSource((QualityAttribute)newValue);
				return;
			case DmtPackage.PREFERENCE__WEIGHT:
				setWeight((PreferenceWeightEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.PREFERENCE__PREFERENCE_TARGET:
				setPreferenceTarget((QualityAttribute)null);
				return;
			case DmtPackage.PREFERENCE__PREFERENCE_SOURCE:
				setPreferenceSource((QualityAttribute)null);
				return;
			case DmtPackage.PREFERENCE__WEIGHT:
				setWeight(WEIGHT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.PREFERENCE__PREFERENCE_TARGET:
				return preferenceTarget != null;
			case DmtPackage.PREFERENCE__PREFERENCE_SOURCE:
				return preferenceSource != null;
			case DmtPackage.PREFERENCE__WEIGHT:
				return weight != WEIGHT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (weight: ");
		result.append(weight);
		result.append(')');
		return result.toString();
	}

} //PreferenceImpl
