/**
 */
package dmt.impl;

import dmt.ANDGoalRefinement;
import dmt.Annotation;
import dmt.DmtPackage;
import dmt.Goal;
import dmt.GoalCategoryEnum;
import dmt.GoalNode;
import dmt.GoalPriorityEnum;
import dmt.GoalRefinable;
import dmt.GoalStabilityEnum;
import dmt.Node;
import dmt.ORGoalRefinement;
import dmt.ObstructionElement;
import dmt.Vulnerability;
import dmt.Vulnerable;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.GoalImpl#getVulnerability <em>Vulnerability</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getName <em>Name</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getUUID <em>UUID</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getHasAnnotations <em>Has Annotations</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getOrGoalRefined <em>Or Goal Refined</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getAndGoalRefined <em>And Goal Refined</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getGoalCategory <em>Goal Category</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getPriority <em>Priority</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getSource <em>Source</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getStability <em>Stability</em>}</li>
 *   <li>{@link dmt.impl.GoalImpl#getResolution <em>Resolution</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class GoalImpl extends ObstructedElementImpl implements Goal {
	/**
	 * The cached value of the '{@link #getVulnerability() <em>Vulnerability</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVulnerability()
	 * @generated
	 * @ordered
	 */
	protected EList<Vulnerability> vulnerability;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getUUID() <em>UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUUID()
	 * @generated
	 * @ordered
	 */
	protected static final String UUID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUUID() <em>UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUUID()
	 * @generated
	 * @ordered
	 */
	protected String uuid = UUID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHasAnnotations() <em>Has Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> hasAnnotations;

	/**
	 * The cached value of the '{@link #getOrGoalRefined() <em>Or Goal Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrGoalRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ORGoalRefinement> orGoalRefined;

	/**
	 * The cached value of the '{@link #getAndGoalRefined() <em>And Goal Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAndGoalRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ANDGoalRefinement> andGoalRefined;

	/**
	 * The default value of the '{@link #getGoalCategory() <em>Goal Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGoalCategory()
	 * @generated
	 * @ordered
	 */
	protected static final GoalCategoryEnum GOAL_CATEGORY_EDEFAULT = GoalCategoryEnum.UNDEFINED;

	/**
	 * The cached value of the '{@link #getGoalCategory() <em>Goal Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGoalCategory()
	 * @generated
	 * @ordered
	 */
	protected GoalCategoryEnum goalCategory = GOAL_CATEGORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPriority() <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected static final GoalPriorityEnum PRIORITY_EDEFAULT = GoalPriorityEnum.UNDEFINED;

	/**
	 * The cached value of the '{@link #getPriority() <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected GoalPriorityEnum priority = PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getSource() <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected String source = SOURCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStability() <em>Stability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStability()
	 * @generated
	 * @ordered
	 */
	protected static final GoalStabilityEnum STABILITY_EDEFAULT = GoalStabilityEnum.LOW;

	/**
	 * The cached value of the '{@link #getStability() <em>Stability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStability()
	 * @generated
	 * @ordered
	 */
	protected GoalStabilityEnum stability = STABILITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getResolution() <em>Resolution</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResolution()
	 * @generated
	 * @ordered
	 */
	protected EList<ObstructionElement> resolution;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.GOAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Vulnerability> getVulnerability() {
		if (vulnerability == null) {
			vulnerability = new EObjectResolvingEList<Vulnerability>(Vulnerability.class, this, DmtPackage.GOAL__VULNERABILITY);
		}
		return vulnerability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUUID() {
		return uuid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUUID(String newUUID) {
		String oldUUID = uuid;
		uuid = newUUID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL__UUID, oldUUID, uuid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getHasAnnotations() {
		if (hasAnnotations == null) {
			hasAnnotations = new EObjectResolvingEList<Annotation>(Annotation.class, this, DmtPackage.GOAL__HAS_ANNOTATIONS);
		}
		return hasAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObstructionElement> getResolution() {
		if (resolution == null) {
			resolution = new EObjectResolvingEList<ObstructionElement>(ObstructionElement.class, this, DmtPackage.GOAL__RESOLUTION);
		}
		return resolution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalCategoryEnum getGoalCategory() {
		return goalCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGoalCategory(GoalCategoryEnum newGoalCategory) {
		GoalCategoryEnum oldGoalCategory = goalCategory;
		goalCategory = newGoalCategory == null ? GOAL_CATEGORY_EDEFAULT : newGoalCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL__GOAL_CATEGORY, oldGoalCategory, goalCategory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalPriorityEnum getPriority() {
		return priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriority(GoalPriorityEnum newPriority) {
		GoalPriorityEnum oldPriority = priority;
		priority = newPriority == null ? PRIORITY_EDEFAULT : newPriority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL__PRIORITY, oldPriority, priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(String newSource) {
		String oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalStabilityEnum getStability() {
		return stability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStability(GoalStabilityEnum newStability) {
		GoalStabilityEnum oldStability = stability;
		stability = newStability == null ? STABILITY_EDEFAULT : newStability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL__STABILITY, oldStability, stability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ORGoalRefinement> getOrGoalRefined() {
		if (orGoalRefined == null) {
			orGoalRefined = new EObjectResolvingEList<ORGoalRefinement>(ORGoalRefinement.class, this, DmtPackage.GOAL__OR_GOAL_REFINED);
		}
		return orGoalRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ANDGoalRefinement> getAndGoalRefined() {
		if (andGoalRefined == null) {
			andGoalRefined = new EObjectResolvingEList<ANDGoalRefinement>(ANDGoalRefinement.class, this, DmtPackage.GOAL__AND_GOAL_REFINED);
		}
		return andGoalRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.GOAL__VULNERABILITY:
				return getVulnerability();
			case DmtPackage.GOAL__NAME:
				return getName();
			case DmtPackage.GOAL__DESCRIPTION:
				return getDescription();
			case DmtPackage.GOAL__UUID:
				return getUUID();
			case DmtPackage.GOAL__HAS_ANNOTATIONS:
				return getHasAnnotations();
			case DmtPackage.GOAL__OR_GOAL_REFINED:
				return getOrGoalRefined();
			case DmtPackage.GOAL__AND_GOAL_REFINED:
				return getAndGoalRefined();
			case DmtPackage.GOAL__GOAL_CATEGORY:
				return getGoalCategory();
			case DmtPackage.GOAL__PRIORITY:
				return getPriority();
			case DmtPackage.GOAL__SOURCE:
				return getSource();
			case DmtPackage.GOAL__STABILITY:
				return getStability();
			case DmtPackage.GOAL__RESOLUTION:
				return getResolution();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.GOAL__VULNERABILITY:
				getVulnerability().clear();
				getVulnerability().addAll((Collection<? extends Vulnerability>)newValue);
				return;
			case DmtPackage.GOAL__NAME:
				setName((String)newValue);
				return;
			case DmtPackage.GOAL__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case DmtPackage.GOAL__UUID:
				setUUID((String)newValue);
				return;
			case DmtPackage.GOAL__HAS_ANNOTATIONS:
				getHasAnnotations().clear();
				getHasAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case DmtPackage.GOAL__OR_GOAL_REFINED:
				getOrGoalRefined().clear();
				getOrGoalRefined().addAll((Collection<? extends ORGoalRefinement>)newValue);
				return;
			case DmtPackage.GOAL__AND_GOAL_REFINED:
				getAndGoalRefined().clear();
				getAndGoalRefined().addAll((Collection<? extends ANDGoalRefinement>)newValue);
				return;
			case DmtPackage.GOAL__GOAL_CATEGORY:
				setGoalCategory((GoalCategoryEnum)newValue);
				return;
			case DmtPackage.GOAL__PRIORITY:
				setPriority((GoalPriorityEnum)newValue);
				return;
			case DmtPackage.GOAL__SOURCE:
				setSource((String)newValue);
				return;
			case DmtPackage.GOAL__STABILITY:
				setStability((GoalStabilityEnum)newValue);
				return;
			case DmtPackage.GOAL__RESOLUTION:
				getResolution().clear();
				getResolution().addAll((Collection<? extends ObstructionElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.GOAL__VULNERABILITY:
				getVulnerability().clear();
				return;
			case DmtPackage.GOAL__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DmtPackage.GOAL__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case DmtPackage.GOAL__UUID:
				setUUID(UUID_EDEFAULT);
				return;
			case DmtPackage.GOAL__HAS_ANNOTATIONS:
				getHasAnnotations().clear();
				return;
			case DmtPackage.GOAL__OR_GOAL_REFINED:
				getOrGoalRefined().clear();
				return;
			case DmtPackage.GOAL__AND_GOAL_REFINED:
				getAndGoalRefined().clear();
				return;
			case DmtPackage.GOAL__GOAL_CATEGORY:
				setGoalCategory(GOAL_CATEGORY_EDEFAULT);
				return;
			case DmtPackage.GOAL__PRIORITY:
				setPriority(PRIORITY_EDEFAULT);
				return;
			case DmtPackage.GOAL__SOURCE:
				setSource(SOURCE_EDEFAULT);
				return;
			case DmtPackage.GOAL__STABILITY:
				setStability(STABILITY_EDEFAULT);
				return;
			case DmtPackage.GOAL__RESOLUTION:
				getResolution().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.GOAL__VULNERABILITY:
				return vulnerability != null && !vulnerability.isEmpty();
			case DmtPackage.GOAL__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DmtPackage.GOAL__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case DmtPackage.GOAL__UUID:
				return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
			case DmtPackage.GOAL__HAS_ANNOTATIONS:
				return hasAnnotations != null && !hasAnnotations.isEmpty();
			case DmtPackage.GOAL__OR_GOAL_REFINED:
				return orGoalRefined != null && !orGoalRefined.isEmpty();
			case DmtPackage.GOAL__AND_GOAL_REFINED:
				return andGoalRefined != null && !andGoalRefined.isEmpty();
			case DmtPackage.GOAL__GOAL_CATEGORY:
				return goalCategory != GOAL_CATEGORY_EDEFAULT;
			case DmtPackage.GOAL__PRIORITY:
				return priority != PRIORITY_EDEFAULT;
			case DmtPackage.GOAL__SOURCE:
				return SOURCE_EDEFAULT == null ? source != null : !SOURCE_EDEFAULT.equals(source);
			case DmtPackage.GOAL__STABILITY:
				return stability != STABILITY_EDEFAULT;
			case DmtPackage.GOAL__RESOLUTION:
				return resolution != null && !resolution.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Vulnerable.class) {
			switch (derivedFeatureID) {
				case DmtPackage.GOAL__VULNERABILITY: return DmtPackage.VULNERABLE__VULNERABILITY;
				default: return -1;
			}
		}
		if (baseClass == Node.class) {
			switch (derivedFeatureID) {
				case DmtPackage.GOAL__NAME: return DmtPackage.NODE__NAME;
				case DmtPackage.GOAL__DESCRIPTION: return DmtPackage.NODE__DESCRIPTION;
				case DmtPackage.GOAL__UUID: return DmtPackage.NODE__UUID;
				case DmtPackage.GOAL__HAS_ANNOTATIONS: return DmtPackage.NODE__HAS_ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == GoalNode.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == GoalRefinable.class) {
			switch (derivedFeatureID) {
				case DmtPackage.GOAL__OR_GOAL_REFINED: return DmtPackage.GOAL_REFINABLE__OR_GOAL_REFINED;
				case DmtPackage.GOAL__AND_GOAL_REFINED: return DmtPackage.GOAL_REFINABLE__AND_GOAL_REFINED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Vulnerable.class) {
			switch (baseFeatureID) {
				case DmtPackage.VULNERABLE__VULNERABILITY: return DmtPackage.GOAL__VULNERABILITY;
				default: return -1;
			}
		}
		if (baseClass == Node.class) {
			switch (baseFeatureID) {
				case DmtPackage.NODE__NAME: return DmtPackage.GOAL__NAME;
				case DmtPackage.NODE__DESCRIPTION: return DmtPackage.GOAL__DESCRIPTION;
				case DmtPackage.NODE__UUID: return DmtPackage.GOAL__UUID;
				case DmtPackage.NODE__HAS_ANNOTATIONS: return DmtPackage.GOAL__HAS_ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == GoalNode.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == GoalRefinable.class) {
			switch (baseFeatureID) {
				case DmtPackage.GOAL_REFINABLE__OR_GOAL_REFINED: return DmtPackage.GOAL__OR_GOAL_REFINED;
				case DmtPackage.GOAL_REFINABLE__AND_GOAL_REFINED: return DmtPackage.GOAL__AND_GOAL_REFINED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", UUID: ");
		result.append(uuid);
		result.append(", goalCategory: ");
		result.append(goalCategory);
		result.append(", priority: ");
		result.append(priority);
		result.append(", source: ");
		result.append(source);
		result.append(", stability: ");
		result.append(stability);
		result.append(')');
		return result.toString();
	}

} //GoalImpl
