/**
 */
package dmt.impl;

import dmt.Agent;
import dmt.AgentAssignment;
import dmt.DmtPackage;
import dmt.LeafGoal;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent Assignment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.AgentAssignmentImpl#getAgentGoalAssignment <em>Agent Goal Assignment</em>}</li>
 *   <li>{@link dmt.impl.AgentAssignmentImpl#getAgentAssignment <em>Agent Assignment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AgentAssignmentImpl extends AgentRelationImpl implements AgentAssignment {
	/**
	 * The cached value of the '{@link #getAgentGoalAssignment() <em>Agent Goal Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentGoalAssignment()
	 * @generated
	 * @ordered
	 */
	protected LeafGoal agentGoalAssignment;
	/**
	 * The cached value of the '{@link #getAgentAssignment() <em>Agent Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentAssignment()
	 * @generated
	 * @ordered
	 */
	protected Agent agentAssignment;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentAssignmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.AGENT_ASSIGNMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafGoal getAgentGoalAssignment() {
		if (agentGoalAssignment != null && agentGoalAssignment.eIsProxy()) {
			InternalEObject oldAgentGoalAssignment = (InternalEObject)agentGoalAssignment;
			agentGoalAssignment = (LeafGoal)eResolveProxy(oldAgentGoalAssignment);
			if (agentGoalAssignment != oldAgentGoalAssignment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.AGENT_ASSIGNMENT__AGENT_GOAL_ASSIGNMENT, oldAgentGoalAssignment, agentGoalAssignment));
			}
		}
		return agentGoalAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafGoal basicGetAgentGoalAssignment() {
		return agentGoalAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentGoalAssignment(LeafGoal newAgentGoalAssignment) {
		LeafGoal oldAgentGoalAssignment = agentGoalAssignment;
		agentGoalAssignment = newAgentGoalAssignment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.AGENT_ASSIGNMENT__AGENT_GOAL_ASSIGNMENT, oldAgentGoalAssignment, agentGoalAssignment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent getAgentAssignment() {
		if (agentAssignment != null && agentAssignment.eIsProxy()) {
			InternalEObject oldAgentAssignment = (InternalEObject)agentAssignment;
			agentAssignment = (Agent)eResolveProxy(oldAgentAssignment);
			if (agentAssignment != oldAgentAssignment) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.AGENT_ASSIGNMENT__AGENT_ASSIGNMENT, oldAgentAssignment, agentAssignment));
			}
		}
		return agentAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Agent basicGetAgentAssignment() {
		return agentAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentAssignment(Agent newAgentAssignment) {
		Agent oldAgentAssignment = agentAssignment;
		agentAssignment = newAgentAssignment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.AGENT_ASSIGNMENT__AGENT_ASSIGNMENT, oldAgentAssignment, agentAssignment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.AGENT_ASSIGNMENT__AGENT_GOAL_ASSIGNMENT:
				if (resolve) return getAgentGoalAssignment();
				return basicGetAgentGoalAssignment();
			case DmtPackage.AGENT_ASSIGNMENT__AGENT_ASSIGNMENT:
				if (resolve) return getAgentAssignment();
				return basicGetAgentAssignment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.AGENT_ASSIGNMENT__AGENT_GOAL_ASSIGNMENT:
				setAgentGoalAssignment((LeafGoal)newValue);
				return;
			case DmtPackage.AGENT_ASSIGNMENT__AGENT_ASSIGNMENT:
				setAgentAssignment((Agent)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.AGENT_ASSIGNMENT__AGENT_GOAL_ASSIGNMENT:
				setAgentGoalAssignment((LeafGoal)null);
				return;
			case DmtPackage.AGENT_ASSIGNMENT__AGENT_ASSIGNMENT:
				setAgentAssignment((Agent)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.AGENT_ASSIGNMENT__AGENT_GOAL_ASSIGNMENT:
				return agentGoalAssignment != null;
			case DmtPackage.AGENT_ASSIGNMENT__AGENT_ASSIGNMENT:
				return agentAssignment != null;
		}
		return super.eIsSet(featureID);
	}

} //AgentAssignmentImpl
