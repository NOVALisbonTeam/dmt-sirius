/**
 */
package dmt.impl;

import interactions.InteractionsPackage;
import dmt.*;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DmtPackageImpl extends EPackageImpl implements DmtPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dmtModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationalizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass softwareAgentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass environmentAgentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass behaviourGoalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass domainDescriptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass leafGoalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass expectationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orGoalRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass securityNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mitigableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass threatEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vulnerabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass securityControlEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass riskComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attackerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass assetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionTacticEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iVulnerabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionTechniqueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionStoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionBiasEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass artificialElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionNodesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interactionObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass linkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mitigationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dissimulationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentAssignmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass andGoalRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conflictGoalRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentMonitoringEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentControlEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionMetricEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionChannelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attackVectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attackStepConnectorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass initialAttackStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass endAttackStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attackStepEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attackTreeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass softGoalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionBehaviourGoalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionSoftGoalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass concreteAssetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodeSelectionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionTacticStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionStrategyTacticRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionTacticStrategyRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionStrategyRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulationHasArtificialElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass propertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass annotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionGoalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionRiskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionStrategyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionTacticMechanismEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass strategyNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass strategyRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass abstractFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass excludeConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass benefitConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass avoidConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nodeRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tacticRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass featureRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass strategyTacticRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tacticFeatureRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mitigatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass securityRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass obstructionElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass orObstructionRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass andObstructionRefinementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass obstacleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass influenceNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass qualityAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass influenceRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass influenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass preferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deceptionTacticEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass environmentEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass obstructedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vulnerableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalRefinableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass beliefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum goalCategoryEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum domainPropertyCategoryEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum goalPriorityEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum obstacleCategoryEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum preferenceWeightEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum influenceWeightEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum influencePolarityEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum deceptionGoalTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum softGoalTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum nodesSelectionTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum channelTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mitigationLevelEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assetTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum attackTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum attackerProfileEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum riskLikelihoodEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum riskSeverityEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum securityControlIncidentEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum threatCategoryEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum vulnerabilityTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum securityControlNatureEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum biasTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum simulationTechniqueTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum metricTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dissimulationTechniqueTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum artificialElementTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum goalTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum goalStabilityEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum environmentAgentTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum attackOriginEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum attackConnectorEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum storyTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum deceptionRiskTypeEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum bindingStateEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum enactmentEnumEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see dmt.DmtPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DmtPackageImpl() {
		super(eNS_URI, DmtFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DmtPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DmtPackage init() {
		if (isInited) return (DmtPackage)EPackage.Registry.INSTANCE.getEPackage(DmtPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDmtPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DmtPackageImpl theDmtPackage = registeredDmtPackage instanceof DmtPackageImpl ? (DmtPackageImpl)registeredDmtPackage : new DmtPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		InteractionsPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDmtPackage.createPackageContents();

		// Initialize created meta-data
		theDmtPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDmtPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DmtPackage.eNS_URI, theDmtPackage);
		return theDmtPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDMTModel() {
		return dmtModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasSecurityNodes() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasDeceptionNodes() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasGoalNodes() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasGoalRelations() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasStrategyNodes() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasStrategyRelations() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasDeceptionRelations() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasSecurityRelations() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasInfluenceNodes() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasInfluenceRelations() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasGoalContainers() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDMTModel_HasMiscNodes() {
		return (EReference)dmtModelEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoal() {
		return goalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoal_Resolution() {
		return (EReference)goalEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoalRelationship() {
		return goalRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoalRelationship_Status() {
		return (EAttribute)goalRelationshipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoalRelationship_Tactic() {
		return (EAttribute)goalRelationshipEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoalRelationship_SysRef() {
		return (EAttribute)goalRelationshipEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoalRelationship_AltName() {
		return (EAttribute)goalRelationshipEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoal_GoalCategory() {
		return (EAttribute)goalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoal_Priority() {
		return (EAttribute)goalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoal_Source() {
		return (EAttribute)goalEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoal_Stability() {
		return (EAttribute)goalEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgent() {
		return agentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgent_Def() {
		return (EAttribute)agentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgent_Load() {
		return (EAttribute)agentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgent_Subagent() {
		return (EReference)agentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgent_PerformsOperation() {
		return (EReference)agentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationalization() {
		return operationalizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationalization_ReqPre() {
		return (EAttribute)operationalizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationalization_ReqTrig() {
		return (EAttribute)operationalizationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationalization_ReqPost() {
		return (EAttribute)operationalizationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationalization_OperationalizeGoal() {
		return (EReference)operationalizationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationalization_Operationalization() {
		return (EReference)operationalizationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoftwareAgent() {
		return softwareAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareAgent_ResponsibilitySoftware() {
		return (EReference)softwareAgentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnvironmentAgent() {
		return environmentAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnvironmentAgent_Type() {
		return (EAttribute)environmentAgentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnvironmentAgent_ResponsibilityEnvironment() {
		return (EReference)environmentAgentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBehaviourGoal() {
		return behaviourGoalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBehaviourGoal_Type() {
		return (EAttribute)behaviourGoalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBehaviourGoal_FormalSpec() {
		return (EAttribute)behaviourGoalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDomainDescription() {
		return domainDescriptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDomainDescription_DomainCategory() {
		return (EAttribute)domainDescriptionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLeafGoal() {
		return leafGoalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExpectation() {
		return expectationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequirement() {
		return requirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperation() {
		return operationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getORGoalRefinement() {
		return orGoalRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getORGoalRefinement_OrGoalRefine() {
		return (EReference)orGoalRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSecurityNode() {
		return securityNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMitigableElement() {
		return mitigableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttack() {
		return attackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttack_Type() {
		return (EAttribute)attackEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttack_InstantiatesThreat() {
		return (EReference)attackEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttack_ExploitsVulnerability() {
		return (EReference)attackEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttack_ContributesToAttack() {
		return (EReference)attackEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttack_LeadsRisk() {
		return (EReference)attackEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttack_Origin() {
		return (EAttribute)attackEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttack_RealizedByAttackVector() {
		return (EReference)attackEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getThreat() {
		return threatEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThreat_Category() {
		return (EAttribute)threatEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThreat_Scenario() {
		return (EAttribute)threatEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getThreat_ContributesToThreat() {
		return (EReference)threatEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getThreat_Consequence() {
		return (EAttribute)threatEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVulnerability() {
		return vulnerabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVulnerability_WindowOccurrence() {
		return (EAttribute)vulnerabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVulnerability_VulnerabilityType() {
		return (EAttribute)vulnerabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSecurityControl() {
		return securityControlEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSecurityControl_Pros() {
		return (EAttribute)securityControlEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSecurityControl_Cons() {
		return (EAttribute)securityControlEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSecurityControl_Nature() {
		return (EAttribute)securityControlEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSecurityControl_Incident() {
		return (EAttribute)securityControlEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecurityControl_Implements() {
		return (EReference)securityControlEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRiskComponent() {
		return riskComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRiskComponent_RiskSeverity() {
		return (EAttribute)riskComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRiskComponent_RiskLikelihood() {
		return (EAttribute)riskComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRiskComponent_HarmsAsset() {
		return (EReference)riskComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttacker() {
		return attackerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttacker_Profile() {
		return (EAttribute)attackerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttacker_LaunchesAttack() {
		return (EReference)attackerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAsset() {
		return assetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAsset_AssetType() {
		return (EAttribute)assetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAsset_Location() {
		return (EAttribute)assetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAsset_HasProperty() {
		return (EReference)assetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAsset_Documentation() {
		return (EAttribute)assetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAsset_UID() {
		return (EAttribute)assetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionNode() {
		return deceptionNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionTactic() {
		return deceptionTacticEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTactic_SubTactic() {
		return (EReference)deceptionTacticEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTactic_ApplyDissimulation() {
		return (EReference)deceptionTacticEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTactic_DescribedByDeceptionStory() {
		return (EReference)deceptionTacticEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTactic_MeasuredBy() {
		return (EReference)deceptionTacticEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTactic_MonitoredBy() {
		return (EReference)deceptionTacticEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTactic_LeadDeceptionRisk() {
		return (EReference)deceptionTacticEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTactic_ApplySimulation() {
		return (EReference)deceptionTacticEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionTactic_IsAbstract() {
		return (EAttribute)deceptionTacticEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTactic_Qualityattribute() {
		return (EReference)deceptionTacticEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIVulnerability() {
		return iVulnerabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVulnerability_WindowOccurence() {
		return (EAttribute)iVulnerabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIVulnerability_VulnerabilityType() {
		return (EAttribute)iVulnerabilityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionTechnique() {
		return deceptionTechniqueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionTechnique_FormalSpec() {
		return (EAttribute)deceptionTechniqueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionTechnique_InformalSpec() {
		return (EAttribute)deceptionTechniqueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionTechnique_ExpectedOutcome() {
		return (EAttribute)deceptionTechniqueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionStory() {
		return deceptionStoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionStory_ExternalIdentifier() {
		return (EAttribute)deceptionStoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionStory_ExploitBias() {
		return (EReference)deceptionStoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionStory_StoryType() {
		return (EAttribute)deceptionStoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionStory_PreCondition() {
		return (EAttribute)deceptionStoryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionStory_PosCondition() {
		return (EAttribute)deceptionStoryEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionStory_Interaction() {
		return (EReference)deceptionStoryEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionBias() {
		return deceptionBiasEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionBias_Type() {
		return (EAttribute)deceptionBiasEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getArtificialElement() {
		return artificialElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getArtificialElement_ContainVulnerability() {
		return (EReference)artificialElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getArtificialElement_FakeType() {
		return (EAttribute)artificialElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionNodes() {
		return interactionNodesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInteractionObject() {
		return interactionObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInteractionObject_ObjectName() {
		return (EAttribute)interactionObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInteractionObject_ClassName() {
		return (EAttribute)interactionObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLink() {
		return linkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLink_IntegerId() {
		return (EAttribute)linkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLink_Message() {
		return (EAttribute)linkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLink_ToObject() {
		return (EReference)linkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLink_FromObject() {
		return (EReference)linkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMitigation() {
		return mitigationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMitigation_MitigationLevel() {
		return (EAttribute)mitigationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMitigation_MitigationTarget() {
		return (EReference)mitigationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMitigation_MitigationSource() {
		return (EReference)mitigationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimulation() {
		return simulationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Type() {
		return (EAttribute)simulationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_AssetReference() {
		return (EReference)simulationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDissimulation() {
		return dissimulationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDissimulation_Type() {
		return (EAttribute)dissimulationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDissimulation_HideRealAsset() {
		return (EReference)dissimulationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNode() {
		return nodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNode_Name() {
		return (EAttribute)nodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNode_Description() {
		return (EAttribute)nodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNode_UUID() {
		return (EAttribute)nodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNode_HasAnnotations() {
		return (EReference)nodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgentAssignment() {
		return agentAssignmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentAssignment_AgentGoalAssignment() {
		return (EReference)agentAssignmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentAssignment_AgentAssignment() {
		return (EReference)agentAssignmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getANDGoalRefinement() {
		return andGoalRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getANDGoalRefinement_AndGoalRefine() {
		return (EReference)andGoalRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConflictGoalRefinement() {
		return conflictGoalRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConflictGoalRefinement_ConflictGoalSource() {
		return (EReference)conflictGoalRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConflictGoalRefinement_ConflictGoalTarget() {
		return (EReference)conflictGoalRefinementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgentMonitoring() {
		return agentMonitoringEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentMonitoring_AgentMonitor() {
		return (EReference)agentMonitoringEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgentMonitoring_ObjectAttribute() {
		return (EAttribute)agentMonitoringEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgentControl() {
		return agentControlEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAgentControl_ObjectAttribute() {
		return (EAttribute)agentControlEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentControl_AgentControl() {
		return (EReference)agentControlEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgentRelation() {
		return agentRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionMetric() {
		return deceptionMetricEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionMetric_MetricType() {
		return (EAttribute)deceptionMetricEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionMetric_Unit() {
		return (EAttribute)deceptionMetricEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionChannel() {
		return deceptionChannelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionChannel_Type() {
		return (EAttribute)deceptionChannelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttackVector() {
		return attackVectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttackVector_Example() {
		return (EAttribute)attackVectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttackVector_Resources() {
		return (EAttribute)attackVectorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttackVector_RealizesAttack() {
		return (EReference)attackVectorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttackVector_Initialattackstep() {
		return (EReference)attackVectorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttackVector_Endattackstep() {
		return (EReference)attackVectorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAProperty() {
		return aPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAProperty_Atype() {
		return (EAttribute)aPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttackStepConnector() {
		return attackStepConnectorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttackStepConnector_Subattackstep() {
		return (EReference)attackStepConnectorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttackStepConnector_StepDescription() {
		return (EAttribute)attackStepConnectorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttackStepConnector_AttackStepConnectorType() {
		return (EAttribute)attackStepConnectorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAttackStepConnector_Sequence() {
		return (EAttribute)attackStepConnectorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInitialAttackStep() {
		return initialAttackStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInitialAttackStep_HasAttackSteps() {
		return (EReference)initialAttackStepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInitialAttackStep_PreCondition() {
		return (EAttribute)initialAttackStepEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEndAttackStep() {
		return endAttackStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEndAttackStep_ExpectedResult() {
		return (EAttribute)endAttackStepEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttackStep() {
		return attackStepEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttackTree() {
		return attackTreeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttackTree_HasAttackvector() {
		return (EReference)attackTreeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoalNode() {
		return goalNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoftGoal() {
		return softGoalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftGoal_FitCriterion() {
		return (EAttribute)softGoalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftGoal_SoftGoalType() {
		return (EAttribute)softGoalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftGoal_SoftGoalOpenType() {
		return (EAttribute)softGoalEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionBehaviourGoal() {
		return deceptionBehaviourGoalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionBehaviourGoal_FormalSpec() {
		return (EAttribute)deceptionBehaviourGoalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionBehaviourGoal_Type() {
		return (EAttribute)deceptionBehaviourGoalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionSoftGoal() {
		return deceptionSoftGoalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionSoftGoal_FitCriterion() {
		return (EAttribute)deceptionSoftGoalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionSoftGoal_SoftGoalType() {
		return (EAttribute)deceptionSoftGoalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionSoftGoal_SoftGoalOpenType() {
		return (EAttribute)deceptionSoftGoalEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConcreteAsset() {
		return concreteAssetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConcreteAsset_BusinessImportance() {
		return (EAttribute)concreteAssetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationable() {
		return operationableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationable_DomPost() {
		return (EAttribute)operationableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationable_DomPre() {
		return (EAttribute)operationableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationable_FormalDomPost() {
		return (EAttribute)operationableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationable_FormalDomPre() {
		return (EAttribute)operationableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoalContainer() {
		return goalContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoalContainer_Goal() {
		return (EReference)goalContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNodeSelection() {
		return nodeSelectionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNodeSelection_Selectionsource() {
		return (EReference)nodeSelectionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNodeSelection_Selectiontarget() {
		return (EReference)nodeSelectionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNodeSelection_Type() {
		return (EAttribute)nodeSelectionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionTacticStrategy() {
		return deceptionTacticStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTacticStrategy_Deceptiontacticmechanism() {
		return (EReference)deceptionTacticStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionStrategyTacticRefinement() {
		return deceptionStrategyTacticRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionStrategyTacticRefinement_Strategytacticrefinementsource() {
		return (EReference)deceptionStrategyTacticRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionStrategyTacticRefinement_Strategytacticrefinementtarget() {
		return (EReference)deceptionStrategyTacticRefinementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionTacticStrategyRefinement() {
		return deceptionTacticStrategyRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementsource() {
		return (EReference)deceptionTacticStrategyRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementtarget() {
		return (EReference)deceptionTacticStrategyRefinementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionStrategyRefinement() {
		return deceptionStrategyRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionStrategyRefinement_Deceptionstrategyrefinementsource() {
		return (EReference)deceptionStrategyRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionStrategyRefinement_Deceptionstrategyrefinementtarget() {
		return (EReference)deceptionStrategyRefinementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimulationHasArtificialElement() {
		return simulationHasArtificialElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationHasArtificialElement_ShowFalseAsset() {
		return (EReference)simulationHasArtificialElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationHasArtificialElement_LowerFakes() {
		return (EAttribute)simulationHasArtificialElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulationHasArtificialElement_UpperFakes() {
		return (EAttribute)simulationHasArtificialElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulationHasArtificialElement_SimulationType() {
		return (EReference)simulationHasArtificialElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProperty() {
		return propertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProperty_Value() {
		return (EAttribute)propertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAnnotation() {
		return annotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAnnotation_Text() {
		return (EAttribute)annotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionGoal() {
		return deceptionGoalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionGoal_DeceptionGoalType() {
		return (EAttribute)deceptionGoalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionRisk() {
		return deceptionRiskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionRisk_DeceptionRiskType() {
		return (EAttribute)deceptionRiskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionStrategy() {
		return deceptionStrategyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionStrategy_IsRoot() {
		return (EAttribute)deceptionStrategyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionStrategy_ExecutionParameter() {
		return (EAttribute)deceptionStrategyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionTacticMechanism() {
		return deceptionTacticMechanismEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionTacticMechanism_EnablingCondition() {
		return (EAttribute)deceptionTacticMechanismEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeceptionTacticMechanism_TacticReference() {
		return (EReference)deceptionTacticMechanismEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStrategyNode() {
		return strategyNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStrategyNode_Property() {
		return (EReference)strategyNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeature() {
		return featureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStrategyRelationship() {
		return strategyRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStrategyRelationship_Description() {
		return (EAttribute)strategyRelationshipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureConstraint() {
		return featureConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureConstraint_Source() {
		return (EReference)featureConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureConstraint_Target() {
		return (EReference)featureConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAbstractFeature() {
		return abstractFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractFeature_BindingState() {
		return (EAttribute)abstractFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAbstractFeature_IsAbstract() {
		return (EAttribute)abstractFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequiredConstraint() {
		return requiredConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExcludeConstraint() {
		return excludeConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBenefitConstraint() {
		return benefitConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAvoidConstraint() {
		return avoidConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNodeRefinement() {
		return nodeRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNodeRefinement_EnactmentChoice() {
		return (EAttribute)nodeRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTacticRefinement() {
		return tacticRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTacticRefinement_Tacticrefinementtarget() {
		return (EReference)tacticRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTacticRefinement_Tacticrefinementsource() {
		return (EReference)tacticRefinementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFeatureRefinement() {
		return featureRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureRefinement_Featurerefinementtarget() {
		return (EReference)featureRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFeatureRefinement_Featurerefinementsource() {
		return (EReference)featureRefinementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStrategyTacticRefinement() {
		return strategyTacticRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStrategyTacticRefinement_Strategytacticrefinementtarget() {
		return (EReference)strategyTacticRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getStrategyTacticRefinement_Strategytacticrefinementsource() {
		return (EReference)strategyTacticRefinementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTacticFeatureRefinement() {
		return tacticFeatureRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTacticFeatureRefinement_Tacticfeaturerefinementtarget() {
		return (EReference)tacticFeatureRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTacticFeatureRefinement_Tacticfeaturerefinementsource() {
		return (EReference)tacticFeatureRefinementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionRelationship() {
		return deceptionRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeceptionRelationship_AltName() {
		return (EAttribute)deceptionRelationshipEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMitigator() {
		return mitigatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSecurityRelationship() {
		return securityRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObstructionElement() {
		return obstructionElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObstructionElement_Obstruct() {
		return (EReference)obstructionElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObstructionElement_ORObstructionRefined() {
		return (EReference)obstructionElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObstructionElement_ANDObstructionRefined() {
		return (EReference)obstructionElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getORObstructionRefinement() {
		return orObstructionRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getORObstructionRefinement_ORObstructionRefine() {
		return (EReference)orObstructionRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getANDObstructionRefinement() {
		return andObstructionRefinementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getANDObstructionRefinement_ANDObstructionRefine() {
		return (EReference)andObstructionRefinementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObstacle() {
		return obstacleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObstacle_ObstacleCategory() {
		return (EAttribute)obstacleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObstacle_FormalDef() {
		return (EAttribute)obstacleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getObstacle_Likelihood() {
		return (EAttribute)obstacleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInfluenceNode() {
		return influenceNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQualityAttribute() {
		return qualityAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInfluenceRelationship() {
		return influenceRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInfluence() {
		return influenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInfluence_Weight() {
		return (EAttribute)influenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInfluence_Polarity() {
		return (EAttribute)influenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInfluence_InfluenceSource() {
		return (EReference)influenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInfluence_Influencetarget() {
		return (EReference)influenceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPreference() {
		return preferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPreference_PreferenceTarget() {
		return (EReference)preferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPreference_PreferenceSource() {
		return (EReference)preferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPreference_Weight() {
		return (EAttribute)preferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEvent() {
		return eventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeceptionTacticEvent() {
		return deceptionTacticEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnvironmentEvent() {
		return environmentEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObstructedElement() {
		return obstructedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVulnerable() {
		return vulnerableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVulnerable_Vulnerability() {
		return (EReference)vulnerableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoalRefinable() {
		return goalRefinableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoalRefinable_OrGoalRefined() {
		return (EReference)goalRefinableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGoalRefinable_AndGoalRefined() {
		return (EReference)goalRefinableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBelief() {
		return beliefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getGoalCategoryEnum() {
		return goalCategoryEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDomainPropertyCategoryEnum() {
		return domainPropertyCategoryEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getGoalPriorityEnum() {
		return goalPriorityEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getObstacleCategoryEnum() {
		return obstacleCategoryEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPreferenceWeightEnum() {
		return preferenceWeightEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getInfluenceWeightEnum() {
		return influenceWeightEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getInfluencePolarityEnum() {
		return influencePolarityEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDeceptionGoalTypeEnum() {
		return deceptionGoalTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSoftGoalTypeEnum() {
		return softGoalTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getNodesSelectionType() {
		return nodesSelectionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getChannelTypeEnum() {
		return channelTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMitigationLevelEnum() {
		return mitigationLevelEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssetTypeEnum() {
		return assetTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAttackTypeEnum() {
		return attackTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAttackerProfileEnum() {
		return attackerProfileEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRiskLikelihoodEnum() {
		return riskLikelihoodEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRiskSeverityEnum() {
		return riskSeverityEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSecurityControlIncidentEnum() {
		return securityControlIncidentEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getThreatCategoryEnum() {
		return threatCategoryEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getVulnerabilityTypeEnum() {
		return vulnerabilityTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSecurityControlNatureEnum() {
		return securityControlNatureEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBiasTypeEnum() {
		return biasTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSimulationTechniqueTypeEnum() {
		return simulationTechniqueTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMetricTypeEnum() {
		return metricTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDissimulationTechniqueTypeEnum() {
		return dissimulationTechniqueTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getArtificialElementTypeEnum() {
		return artificialElementTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getGoalTypeEnum() {
		return goalTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getGoalStabilityEnum() {
		return goalStabilityEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEnvironmentAgentTypeEnum() {
		return environmentAgentTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAttackOriginEnum() {
		return attackOriginEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAttackConnectorEnum() {
		return attackConnectorEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStoryTypeEnum() {
		return storyTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDeceptionRiskTypeEnum() {
		return deceptionRiskTypeEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBindingStateEnum() {
		return bindingStateEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEnactmentEnum() {
		return enactmentEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DmtFactory getDmtFactory() {
		return (DmtFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		dmtModelEClass = createEClass(DMT_MODEL);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_SECURITY_NODES);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_DECEPTION_NODES);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_GOAL_NODES);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_GOAL_RELATIONS);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_STRATEGY_NODES);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_STRATEGY_RELATIONS);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_DECEPTION_RELATIONS);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_SECURITY_RELATIONS);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_INFLUENCE_NODES);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_INFLUENCE_RELATIONS);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_GOAL_CONTAINERS);
		createEReference(dmtModelEClass, DMT_MODEL__HAS_MISC_NODES);

		goalEClass = createEClass(GOAL);
		createEAttribute(goalEClass, GOAL__GOAL_CATEGORY);
		createEAttribute(goalEClass, GOAL__PRIORITY);
		createEAttribute(goalEClass, GOAL__SOURCE);
		createEAttribute(goalEClass, GOAL__STABILITY);
		createEReference(goalEClass, GOAL__RESOLUTION);

		goalRelationshipEClass = createEClass(GOAL_RELATIONSHIP);
		createEAttribute(goalRelationshipEClass, GOAL_RELATIONSHIP__STATUS);
		createEAttribute(goalRelationshipEClass, GOAL_RELATIONSHIP__TACTIC);
		createEAttribute(goalRelationshipEClass, GOAL_RELATIONSHIP__SYS_REF);
		createEAttribute(goalRelationshipEClass, GOAL_RELATIONSHIP__ALT_NAME);

		agentEClass = createEClass(AGENT);
		createEAttribute(agentEClass, AGENT__DEF);
		createEAttribute(agentEClass, AGENT__LOAD);
		createEReference(agentEClass, AGENT__SUBAGENT);
		createEReference(agentEClass, AGENT__PERFORMS_OPERATION);

		operationalizationEClass = createEClass(OPERATIONALIZATION);
		createEAttribute(operationalizationEClass, OPERATIONALIZATION__REQ_PRE);
		createEAttribute(operationalizationEClass, OPERATIONALIZATION__REQ_TRIG);
		createEAttribute(operationalizationEClass, OPERATIONALIZATION__REQ_POST);
		createEReference(operationalizationEClass, OPERATIONALIZATION__OPERATIONALIZE_GOAL);
		createEReference(operationalizationEClass, OPERATIONALIZATION__OPERATIONALIZATION);

		softwareAgentEClass = createEClass(SOFTWARE_AGENT);
		createEReference(softwareAgentEClass, SOFTWARE_AGENT__RESPONSIBILITY_SOFTWARE);

		environmentAgentEClass = createEClass(ENVIRONMENT_AGENT);
		createEAttribute(environmentAgentEClass, ENVIRONMENT_AGENT__TYPE);
		createEReference(environmentAgentEClass, ENVIRONMENT_AGENT__RESPONSIBILITY_ENVIRONMENT);

		behaviourGoalEClass = createEClass(BEHAVIOUR_GOAL);
		createEAttribute(behaviourGoalEClass, BEHAVIOUR_GOAL__TYPE);
		createEAttribute(behaviourGoalEClass, BEHAVIOUR_GOAL__FORMAL_SPEC);

		domainDescriptionEClass = createEClass(DOMAIN_DESCRIPTION);
		createEAttribute(domainDescriptionEClass, DOMAIN_DESCRIPTION__DOMAIN_CATEGORY);

		leafGoalEClass = createEClass(LEAF_GOAL);

		expectationEClass = createEClass(EXPECTATION);

		requirementEClass = createEClass(REQUIREMENT);

		operationEClass = createEClass(OPERATION);

		orGoalRefinementEClass = createEClass(OR_GOAL_REFINEMENT);
		createEReference(orGoalRefinementEClass, OR_GOAL_REFINEMENT__OR_GOAL_REFINE);

		securityNodeEClass = createEClass(SECURITY_NODE);

		mitigableElementEClass = createEClass(MITIGABLE_ELEMENT);

		attackEClass = createEClass(ATTACK);
		createEAttribute(attackEClass, ATTACK__TYPE);
		createEReference(attackEClass, ATTACK__INSTANTIATES_THREAT);
		createEReference(attackEClass, ATTACK__EXPLOITS_VULNERABILITY);
		createEReference(attackEClass, ATTACK__CONTRIBUTES_TO_ATTACK);
		createEReference(attackEClass, ATTACK__LEADS_RISK);
		createEAttribute(attackEClass, ATTACK__ORIGIN);
		createEReference(attackEClass, ATTACK__REALIZED_BY_ATTACK_VECTOR);

		threatEClass = createEClass(THREAT);
		createEAttribute(threatEClass, THREAT__CATEGORY);
		createEAttribute(threatEClass, THREAT__SCENARIO);
		createEReference(threatEClass, THREAT__CONTRIBUTES_TO_THREAT);
		createEAttribute(threatEClass, THREAT__CONSEQUENCE);

		vulnerabilityEClass = createEClass(VULNERABILITY);
		createEAttribute(vulnerabilityEClass, VULNERABILITY__WINDOW_OCCURRENCE);
		createEAttribute(vulnerabilityEClass, VULNERABILITY__VULNERABILITY_TYPE);

		securityControlEClass = createEClass(SECURITY_CONTROL);
		createEAttribute(securityControlEClass, SECURITY_CONTROL__PROS);
		createEAttribute(securityControlEClass, SECURITY_CONTROL__CONS);
		createEAttribute(securityControlEClass, SECURITY_CONTROL__NATURE);
		createEAttribute(securityControlEClass, SECURITY_CONTROL__INCIDENT);
		createEReference(securityControlEClass, SECURITY_CONTROL__IMPLEMENTS);

		riskComponentEClass = createEClass(RISK_COMPONENT);
		createEAttribute(riskComponentEClass, RISK_COMPONENT__RISK_SEVERITY);
		createEAttribute(riskComponentEClass, RISK_COMPONENT__RISK_LIKELIHOOD);
		createEReference(riskComponentEClass, RISK_COMPONENT__HARMS_ASSET);

		attackerEClass = createEClass(ATTACKER);
		createEAttribute(attackerEClass, ATTACKER__PROFILE);
		createEReference(attackerEClass, ATTACKER__LAUNCHES_ATTACK);

		assetEClass = createEClass(ASSET);
		createEAttribute(assetEClass, ASSET__ASSET_TYPE);
		createEAttribute(assetEClass, ASSET__LOCATION);
		createEReference(assetEClass, ASSET__HAS_PROPERTY);
		createEAttribute(assetEClass, ASSET__DOCUMENTATION);
		createEAttribute(assetEClass, ASSET__UID);

		deceptionNodeEClass = createEClass(DECEPTION_NODE);

		deceptionTacticEClass = createEClass(DECEPTION_TACTIC);
		createEReference(deceptionTacticEClass, DECEPTION_TACTIC__SUB_TACTIC);
		createEReference(deceptionTacticEClass, DECEPTION_TACTIC__APPLY_DISSIMULATION);
		createEReference(deceptionTacticEClass, DECEPTION_TACTIC__DESCRIBED_BY_DECEPTION_STORY);
		createEReference(deceptionTacticEClass, DECEPTION_TACTIC__MEASURED_BY);
		createEReference(deceptionTacticEClass, DECEPTION_TACTIC__MONITORED_BY);
		createEReference(deceptionTacticEClass, DECEPTION_TACTIC__LEAD_DECEPTION_RISK);
		createEReference(deceptionTacticEClass, DECEPTION_TACTIC__APPLY_SIMULATION);
		createEAttribute(deceptionTacticEClass, DECEPTION_TACTIC__IS_ABSTRACT);
		createEReference(deceptionTacticEClass, DECEPTION_TACTIC__QUALITYATTRIBUTE);

		iVulnerabilityEClass = createEClass(IVULNERABILITY);
		createEAttribute(iVulnerabilityEClass, IVULNERABILITY__WINDOW_OCCURENCE);
		createEAttribute(iVulnerabilityEClass, IVULNERABILITY__VULNERABILITY_TYPE);

		deceptionTechniqueEClass = createEClass(DECEPTION_TECHNIQUE);
		createEAttribute(deceptionTechniqueEClass, DECEPTION_TECHNIQUE__FORMAL_SPEC);
		createEAttribute(deceptionTechniqueEClass, DECEPTION_TECHNIQUE__INFORMAL_SPEC);
		createEAttribute(deceptionTechniqueEClass, DECEPTION_TECHNIQUE__EXPECTED_OUTCOME);

		deceptionStoryEClass = createEClass(DECEPTION_STORY);
		createEAttribute(deceptionStoryEClass, DECEPTION_STORY__EXTERNAL_IDENTIFIER);
		createEReference(deceptionStoryEClass, DECEPTION_STORY__EXPLOIT_BIAS);
		createEAttribute(deceptionStoryEClass, DECEPTION_STORY__STORY_TYPE);
		createEAttribute(deceptionStoryEClass, DECEPTION_STORY__PRE_CONDITION);
		createEAttribute(deceptionStoryEClass, DECEPTION_STORY__POS_CONDITION);
		createEReference(deceptionStoryEClass, DECEPTION_STORY__INTERACTION);

		deceptionBiasEClass = createEClass(DECEPTION_BIAS);
		createEAttribute(deceptionBiasEClass, DECEPTION_BIAS__TYPE);

		artificialElementEClass = createEClass(ARTIFICIAL_ELEMENT);
		createEReference(artificialElementEClass, ARTIFICIAL_ELEMENT__CONTAIN_VULNERABILITY);
		createEAttribute(artificialElementEClass, ARTIFICIAL_ELEMENT__FAKE_TYPE);

		mitigationEClass = createEClass(MITIGATION);
		createEAttribute(mitigationEClass, MITIGATION__MITIGATION_LEVEL);
		createEReference(mitigationEClass, MITIGATION__MITIGATION_TARGET);
		createEReference(mitigationEClass, MITIGATION__MITIGATION_SOURCE);

		interactionNodesEClass = createEClass(INTERACTION_NODES);

		interactionObjectEClass = createEClass(INTERACTION_OBJECT);
		createEAttribute(interactionObjectEClass, INTERACTION_OBJECT__OBJECT_NAME);
		createEAttribute(interactionObjectEClass, INTERACTION_OBJECT__CLASS_NAME);

		linkEClass = createEClass(LINK);
		createEAttribute(linkEClass, LINK__INTEGER_ID);
		createEAttribute(linkEClass, LINK__MESSAGE);
		createEReference(linkEClass, LINK__TO_OBJECT);
		createEReference(linkEClass, LINK__FROM_OBJECT);

		simulationEClass = createEClass(SIMULATION);
		createEAttribute(simulationEClass, SIMULATION__TYPE);
		createEReference(simulationEClass, SIMULATION__ASSET_REFERENCE);

		dissimulationEClass = createEClass(DISSIMULATION);
		createEAttribute(dissimulationEClass, DISSIMULATION__TYPE);
		createEReference(dissimulationEClass, DISSIMULATION__HIDE_REAL_ASSET);

		nodeEClass = createEClass(NODE);
		createEAttribute(nodeEClass, NODE__NAME);
		createEAttribute(nodeEClass, NODE__DESCRIPTION);
		createEAttribute(nodeEClass, NODE__UUID);
		createEReference(nodeEClass, NODE__HAS_ANNOTATIONS);

		agentAssignmentEClass = createEClass(AGENT_ASSIGNMENT);
		createEReference(agentAssignmentEClass, AGENT_ASSIGNMENT__AGENT_GOAL_ASSIGNMENT);
		createEReference(agentAssignmentEClass, AGENT_ASSIGNMENT__AGENT_ASSIGNMENT);

		andGoalRefinementEClass = createEClass(AND_GOAL_REFINEMENT);
		createEReference(andGoalRefinementEClass, AND_GOAL_REFINEMENT__AND_GOAL_REFINE);

		conflictGoalRefinementEClass = createEClass(CONFLICT_GOAL_REFINEMENT);
		createEReference(conflictGoalRefinementEClass, CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_SOURCE);
		createEReference(conflictGoalRefinementEClass, CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_TARGET);

		agentMonitoringEClass = createEClass(AGENT_MONITORING);
		createEReference(agentMonitoringEClass, AGENT_MONITORING__AGENT_MONITOR);
		createEAttribute(agentMonitoringEClass, AGENT_MONITORING__OBJECT_ATTRIBUTE);

		agentControlEClass = createEClass(AGENT_CONTROL);
		createEAttribute(agentControlEClass, AGENT_CONTROL__OBJECT_ATTRIBUTE);
		createEReference(agentControlEClass, AGENT_CONTROL__AGENT_CONTROL);

		agentRelationEClass = createEClass(AGENT_RELATION);

		deceptionMetricEClass = createEClass(DECEPTION_METRIC);
		createEAttribute(deceptionMetricEClass, DECEPTION_METRIC__METRIC_TYPE);
		createEAttribute(deceptionMetricEClass, DECEPTION_METRIC__UNIT);

		deceptionChannelEClass = createEClass(DECEPTION_CHANNEL);
		createEAttribute(deceptionChannelEClass, DECEPTION_CHANNEL__TYPE);

		attackVectorEClass = createEClass(ATTACK_VECTOR);
		createEAttribute(attackVectorEClass, ATTACK_VECTOR__EXAMPLE);
		createEAttribute(attackVectorEClass, ATTACK_VECTOR__RESOURCES);
		createEReference(attackVectorEClass, ATTACK_VECTOR__REALIZES_ATTACK);
		createEReference(attackVectorEClass, ATTACK_VECTOR__INITIALATTACKSTEP);
		createEReference(attackVectorEClass, ATTACK_VECTOR__ENDATTACKSTEP);

		aPropertyEClass = createEClass(APROPERTY);
		createEAttribute(aPropertyEClass, APROPERTY__ATYPE);

		attackStepConnectorEClass = createEClass(ATTACK_STEP_CONNECTOR);
		createEReference(attackStepConnectorEClass, ATTACK_STEP_CONNECTOR__SUBATTACKSTEP);
		createEAttribute(attackStepConnectorEClass, ATTACK_STEP_CONNECTOR__STEP_DESCRIPTION);
		createEAttribute(attackStepConnectorEClass, ATTACK_STEP_CONNECTOR__ATTACK_STEP_CONNECTOR_TYPE);
		createEAttribute(attackStepConnectorEClass, ATTACK_STEP_CONNECTOR__SEQUENCE);

		initialAttackStepEClass = createEClass(INITIAL_ATTACK_STEP);
		createEReference(initialAttackStepEClass, INITIAL_ATTACK_STEP__HAS_ATTACK_STEPS);
		createEAttribute(initialAttackStepEClass, INITIAL_ATTACK_STEP__PRE_CONDITION);

		endAttackStepEClass = createEClass(END_ATTACK_STEP);
		createEAttribute(endAttackStepEClass, END_ATTACK_STEP__EXPECTED_RESULT);

		attackStepEClass = createEClass(ATTACK_STEP);

		attackTreeEClass = createEClass(ATTACK_TREE);
		createEReference(attackTreeEClass, ATTACK_TREE__HAS_ATTACKVECTOR);

		goalNodeEClass = createEClass(GOAL_NODE);

		softGoalEClass = createEClass(SOFT_GOAL);
		createEAttribute(softGoalEClass, SOFT_GOAL__FIT_CRITERION);
		createEAttribute(softGoalEClass, SOFT_GOAL__SOFT_GOAL_TYPE);
		createEAttribute(softGoalEClass, SOFT_GOAL__SOFT_GOAL_OPEN_TYPE);

		deceptionBehaviourGoalEClass = createEClass(DECEPTION_BEHAVIOUR_GOAL);
		createEAttribute(deceptionBehaviourGoalEClass, DECEPTION_BEHAVIOUR_GOAL__FORMAL_SPEC);
		createEAttribute(deceptionBehaviourGoalEClass, DECEPTION_BEHAVIOUR_GOAL__TYPE);

		deceptionRiskEClass = createEClass(DECEPTION_RISK);
		createEAttribute(deceptionRiskEClass, DECEPTION_RISK__DECEPTION_RISK_TYPE);

		deceptionStrategyEClass = createEClass(DECEPTION_STRATEGY);
		createEAttribute(deceptionStrategyEClass, DECEPTION_STRATEGY__IS_ROOT);
		createEAttribute(deceptionStrategyEClass, DECEPTION_STRATEGY__EXECUTION_PARAMETER);

		deceptionTacticMechanismEClass = createEClass(DECEPTION_TACTIC_MECHANISM);
		createEAttribute(deceptionTacticMechanismEClass, DECEPTION_TACTIC_MECHANISM__ENABLING_CONDITION);
		createEReference(deceptionTacticMechanismEClass, DECEPTION_TACTIC_MECHANISM__TACTIC_REFERENCE);

		strategyNodeEClass = createEClass(STRATEGY_NODE);
		createEReference(strategyNodeEClass, STRATEGY_NODE__PROPERTY);

		featureEClass = createEClass(FEATURE);

		strategyRelationshipEClass = createEClass(STRATEGY_RELATIONSHIP);
		createEAttribute(strategyRelationshipEClass, STRATEGY_RELATIONSHIP__DESCRIPTION);

		featureConstraintEClass = createEClass(FEATURE_CONSTRAINT);
		createEReference(featureConstraintEClass, FEATURE_CONSTRAINT__SOURCE);
		createEReference(featureConstraintEClass, FEATURE_CONSTRAINT__TARGET);

		abstractFeatureEClass = createEClass(ABSTRACT_FEATURE);
		createEAttribute(abstractFeatureEClass, ABSTRACT_FEATURE__BINDING_STATE);
		createEAttribute(abstractFeatureEClass, ABSTRACT_FEATURE__IS_ABSTRACT);

		requiredConstraintEClass = createEClass(REQUIRED_CONSTRAINT);

		excludeConstraintEClass = createEClass(EXCLUDE_CONSTRAINT);

		benefitConstraintEClass = createEClass(BENEFIT_CONSTRAINT);

		avoidConstraintEClass = createEClass(AVOID_CONSTRAINT);

		nodeRefinementEClass = createEClass(NODE_REFINEMENT);
		createEAttribute(nodeRefinementEClass, NODE_REFINEMENT__ENACTMENT_CHOICE);

		tacticRefinementEClass = createEClass(TACTIC_REFINEMENT);
		createEReference(tacticRefinementEClass, TACTIC_REFINEMENT__TACTICREFINEMENTTARGET);
		createEReference(tacticRefinementEClass, TACTIC_REFINEMENT__TACTICREFINEMENTSOURCE);

		featureRefinementEClass = createEClass(FEATURE_REFINEMENT);
		createEReference(featureRefinementEClass, FEATURE_REFINEMENT__FEATUREREFINEMENTTARGET);
		createEReference(featureRefinementEClass, FEATURE_REFINEMENT__FEATUREREFINEMENTSOURCE);

		strategyTacticRefinementEClass = createEClass(STRATEGY_TACTIC_REFINEMENT);
		createEReference(strategyTacticRefinementEClass, STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET);
		createEReference(strategyTacticRefinementEClass, STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE);

		tacticFeatureRefinementEClass = createEClass(TACTIC_FEATURE_REFINEMENT);
		createEReference(tacticFeatureRefinementEClass, TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTTARGET);
		createEReference(tacticFeatureRefinementEClass, TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTSOURCE);

		deceptionRelationshipEClass = createEClass(DECEPTION_RELATIONSHIP);
		createEAttribute(deceptionRelationshipEClass, DECEPTION_RELATIONSHIP__ALT_NAME);

		mitigatorEClass = createEClass(MITIGATOR);

		securityRelationshipEClass = createEClass(SECURITY_RELATIONSHIP);

		obstructionElementEClass = createEClass(OBSTRUCTION_ELEMENT);
		createEReference(obstructionElementEClass, OBSTRUCTION_ELEMENT__OBSTRUCT);
		createEReference(obstructionElementEClass, OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED);
		createEReference(obstructionElementEClass, OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED);

		orObstructionRefinementEClass = createEClass(OR_OBSTRUCTION_REFINEMENT);
		createEReference(orObstructionRefinementEClass, OR_OBSTRUCTION_REFINEMENT__OR_OBSTRUCTION_REFINE);

		andObstructionRefinementEClass = createEClass(AND_OBSTRUCTION_REFINEMENT);
		createEReference(andObstructionRefinementEClass, AND_OBSTRUCTION_REFINEMENT__AND_OBSTRUCTION_REFINE);

		obstacleEClass = createEClass(OBSTACLE);
		createEAttribute(obstacleEClass, OBSTACLE__OBSTACLE_CATEGORY);
		createEAttribute(obstacleEClass, OBSTACLE__FORMAL_DEF);
		createEAttribute(obstacleEClass, OBSTACLE__LIKELIHOOD);

		influenceNodeEClass = createEClass(INFLUENCE_NODE);

		qualityAttributeEClass = createEClass(QUALITY_ATTRIBUTE);

		influenceRelationshipEClass = createEClass(INFLUENCE_RELATIONSHIP);

		influenceEClass = createEClass(INFLUENCE);
		createEAttribute(influenceEClass, INFLUENCE__WEIGHT);
		createEAttribute(influenceEClass, INFLUENCE__POLARITY);
		createEReference(influenceEClass, INFLUENCE__INFLUENCE_SOURCE);
		createEReference(influenceEClass, INFLUENCE__INFLUENCETARGET);

		preferenceEClass = createEClass(PREFERENCE);
		createEReference(preferenceEClass, PREFERENCE__PREFERENCE_TARGET);
		createEReference(preferenceEClass, PREFERENCE__PREFERENCE_SOURCE);
		createEAttribute(preferenceEClass, PREFERENCE__WEIGHT);

		eventEClass = createEClass(EVENT);

		deceptionTacticEventEClass = createEClass(DECEPTION_TACTIC_EVENT);

		environmentEventEClass = createEClass(ENVIRONMENT_EVENT);

		obstructedElementEClass = createEClass(OBSTRUCTED_ELEMENT);

		vulnerableEClass = createEClass(VULNERABLE);
		createEReference(vulnerableEClass, VULNERABLE__VULNERABILITY);

		goalRefinableEClass = createEClass(GOAL_REFINABLE);
		createEReference(goalRefinableEClass, GOAL_REFINABLE__OR_GOAL_REFINED);
		createEReference(goalRefinableEClass, GOAL_REFINABLE__AND_GOAL_REFINED);

		beliefEClass = createEClass(BELIEF);

		deceptionGoalEClass = createEClass(DECEPTION_GOAL);
		createEAttribute(deceptionGoalEClass, DECEPTION_GOAL__DECEPTION_GOAL_TYPE);

		deceptionSoftGoalEClass = createEClass(DECEPTION_SOFT_GOAL);
		createEAttribute(deceptionSoftGoalEClass, DECEPTION_SOFT_GOAL__FIT_CRITERION);
		createEAttribute(deceptionSoftGoalEClass, DECEPTION_SOFT_GOAL__SOFT_GOAL_TYPE);
		createEAttribute(deceptionSoftGoalEClass, DECEPTION_SOFT_GOAL__SOFT_GOAL_OPEN_TYPE);

		concreteAssetEClass = createEClass(CONCRETE_ASSET);
		createEAttribute(concreteAssetEClass, CONCRETE_ASSET__BUSINESS_IMPORTANCE);

		operationableEClass = createEClass(OPERATIONABLE);
		createEAttribute(operationableEClass, OPERATIONABLE__DOM_POST);
		createEAttribute(operationableEClass, OPERATIONABLE__DOM_PRE);
		createEAttribute(operationableEClass, OPERATIONABLE__FORMAL_DOM_POST);
		createEAttribute(operationableEClass, OPERATIONABLE__FORMAL_DOM_PRE);

		goalContainerEClass = createEClass(GOAL_CONTAINER);
		createEReference(goalContainerEClass, GOAL_CONTAINER__GOAL);

		nodeSelectionEClass = createEClass(NODE_SELECTION);
		createEReference(nodeSelectionEClass, NODE_SELECTION__SELECTIONSOURCE);
		createEReference(nodeSelectionEClass, NODE_SELECTION__SELECTIONTARGET);
		createEAttribute(nodeSelectionEClass, NODE_SELECTION__TYPE);

		deceptionTacticStrategyEClass = createEClass(DECEPTION_TACTIC_STRATEGY);
		createEReference(deceptionTacticStrategyEClass, DECEPTION_TACTIC_STRATEGY__DECEPTIONTACTICMECHANISM);

		deceptionStrategyTacticRefinementEClass = createEClass(DECEPTION_STRATEGY_TACTIC_REFINEMENT);
		createEReference(deceptionStrategyTacticRefinementEClass, DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE);
		createEReference(deceptionStrategyTacticRefinementEClass, DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET);

		deceptionTacticStrategyRefinementEClass = createEClass(DECEPTION_TACTIC_STRATEGY_REFINEMENT);
		createEReference(deceptionTacticStrategyRefinementEClass, DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTSOURCE);
		createEReference(deceptionTacticStrategyRefinementEClass, DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTTARGET);

		deceptionStrategyRefinementEClass = createEClass(DECEPTION_STRATEGY_REFINEMENT);
		createEReference(deceptionStrategyRefinementEClass, DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTSOURCE);
		createEReference(deceptionStrategyRefinementEClass, DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTTARGET);

		simulationHasArtificialElementEClass = createEClass(SIMULATION_HAS_ARTIFICIAL_ELEMENT);
		createEReference(simulationHasArtificialElementEClass, SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET);
		createEAttribute(simulationHasArtificialElementEClass, SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES);
		createEAttribute(simulationHasArtificialElementEClass, SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES);
		createEReference(simulationHasArtificialElementEClass, SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE);

		propertyEClass = createEClass(PROPERTY);
		createEAttribute(propertyEClass, PROPERTY__VALUE);

		annotationEClass = createEClass(ANNOTATION);
		createEAttribute(annotationEClass, ANNOTATION__TEXT);

		// Create enums
		goalCategoryEnumEEnum = createEEnum(GOAL_CATEGORY_ENUM);
		domainPropertyCategoryEnumEEnum = createEEnum(DOMAIN_PROPERTY_CATEGORY_ENUM);
		goalPriorityEnumEEnum = createEEnum(GOAL_PRIORITY_ENUM);
		mitigationLevelEnumEEnum = createEEnum(MITIGATION_LEVEL_ENUM);
		assetTypeEnumEEnum = createEEnum(ASSET_TYPE_ENUM);
		attackTypeEnumEEnum = createEEnum(ATTACK_TYPE_ENUM);
		attackerProfileEnumEEnum = createEEnum(ATTACKER_PROFILE_ENUM);
		riskLikelihoodEnumEEnum = createEEnum(RISK_LIKELIHOOD_ENUM);
		riskSeverityEnumEEnum = createEEnum(RISK_SEVERITY_ENUM);
		securityControlIncidentEnumEEnum = createEEnum(SECURITY_CONTROL_INCIDENT_ENUM);
		threatCategoryEnumEEnum = createEEnum(THREAT_CATEGORY_ENUM);
		vulnerabilityTypeEnumEEnum = createEEnum(VULNERABILITY_TYPE_ENUM);
		securityControlNatureEnumEEnum = createEEnum(SECURITY_CONTROL_NATURE_ENUM);
		biasTypeEnumEEnum = createEEnum(BIAS_TYPE_ENUM);
		simulationTechniqueTypeEnumEEnum = createEEnum(SIMULATION_TECHNIQUE_TYPE_ENUM);
		metricTypeEnumEEnum = createEEnum(METRIC_TYPE_ENUM);
		dissimulationTechniqueTypeEnumEEnum = createEEnum(DISSIMULATION_TECHNIQUE_TYPE_ENUM);
		goalTypeEnumEEnum = createEEnum(GOAL_TYPE_ENUM);
		goalStabilityEnumEEnum = createEEnum(GOAL_STABILITY_ENUM);
		environmentAgentTypeEnumEEnum = createEEnum(ENVIRONMENT_AGENT_TYPE_ENUM);
		attackOriginEnumEEnum = createEEnum(ATTACK_ORIGIN_ENUM);
		attackConnectorEnumEEnum = createEEnum(ATTACK_CONNECTOR_ENUM);
		storyTypeEnumEEnum = createEEnum(STORY_TYPE_ENUM);
		deceptionRiskTypeEnumEEnum = createEEnum(DECEPTION_RISK_TYPE_ENUM);
		bindingStateEnumEEnum = createEEnum(BINDING_STATE_ENUM);
		enactmentEnumEEnum = createEEnum(ENACTMENT_ENUM);
		artificialElementTypeEnumEEnum = createEEnum(ARTIFICIAL_ELEMENT_TYPE_ENUM);
		obstacleCategoryEnumEEnum = createEEnum(OBSTACLE_CATEGORY_ENUM);
		preferenceWeightEnumEEnum = createEEnum(PREFERENCE_WEIGHT_ENUM);
		influenceWeightEnumEEnum = createEEnum(INFLUENCE_WEIGHT_ENUM);
		influencePolarityEnumEEnum = createEEnum(INFLUENCE_POLARITY_ENUM);
		deceptionGoalTypeEnumEEnum = createEEnum(DECEPTION_GOAL_TYPE_ENUM);
		softGoalTypeEnumEEnum = createEEnum(SOFT_GOAL_TYPE_ENUM);
		nodesSelectionTypeEEnum = createEEnum(NODES_SELECTION_TYPE);
		channelTypeEnumEEnum = createEEnum(CHANNEL_TYPE_ENUM);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);
		InteractionsPackage theInteractionsPackage = (InteractionsPackage)EPackage.Registry.INSTANCE.getEPackage(InteractionsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		goalEClass.getESuperTypes().add(this.getObstructedElement());
		goalEClass.getESuperTypes().add(this.getVulnerable());
		goalEClass.getESuperTypes().add(this.getGoalRefinable());
		agentEClass.getESuperTypes().add(this.getGoalNode());
		operationalizationEClass.getESuperTypes().add(this.getGoalRelationship());
		softwareAgentEClass.getESuperTypes().add(this.getAgent());
		environmentAgentEClass.getESuperTypes().add(this.getAgent());
		behaviourGoalEClass.getESuperTypes().add(this.getGoal());
		domainDescriptionEClass.getESuperTypes().add(this.getObstructedElement());
		domainDescriptionEClass.getESuperTypes().add(this.getVulnerable());
		domainDescriptionEClass.getESuperTypes().add(this.getGoalRefinable());
		leafGoalEClass.getESuperTypes().add(this.getGoal());
		expectationEClass.getESuperTypes().add(this.getLeafGoal());
		requirementEClass.getESuperTypes().add(this.getLeafGoal());
		operationEClass.getESuperTypes().add(this.getVulnerable());
		operationEClass.getESuperTypes().add(this.getOperationable());
		orGoalRefinementEClass.getESuperTypes().add(this.getGoalRelationship());
		securityNodeEClass.getESuperTypes().add(this.getNode());
		mitigableElementEClass.getESuperTypes().add(this.getSecurityNode());
		attackEClass.getESuperTypes().add(this.getMitigableElement());
		threatEClass.getESuperTypes().add(this.getMitigableElement());
		threatEClass.getESuperTypes().add(this.getObstructionElement());
		vulnerabilityEClass.getESuperTypes().add(this.getMitigableElement());
		securityControlEClass.getESuperTypes().add(this.getSecurityNode());
		securityControlEClass.getESuperTypes().add(this.getMitigator());
		riskComponentEClass.getESuperTypes().add(this.getMitigableElement());
		attackerEClass.getESuperTypes().add(this.getSecurityNode());
		assetEClass.getESuperTypes().add(this.getSecurityNode());
		deceptionNodeEClass.getESuperTypes().add(this.getNode());
		deceptionTacticEClass.getESuperTypes().add(this.getDeceptionNode());
		deceptionTacticEClass.getESuperTypes().add(this.getMitigator());
		deceptionTacticEClass.getESuperTypes().add(this.getOperationable());
		iVulnerabilityEClass.getESuperTypes().add(this.getDeceptionNode());
		deceptionTechniqueEClass.getESuperTypes().add(this.getDeceptionNode());
		deceptionStoryEClass.getESuperTypes().add(this.getDeceptionNode());
		deceptionBiasEClass.getESuperTypes().add(this.getDeceptionNode());
		artificialElementEClass.getESuperTypes().add(this.getDeceptionNode());
		artificialElementEClass.getESuperTypes().add(this.getAsset());
		mitigationEClass.getESuperTypes().add(this.getSecurityRelationship());
		interactionObjectEClass.getESuperTypes().add(this.getInteractionNodes());
		linkEClass.getESuperTypes().add(this.getInteractionNodes());
		simulationEClass.getESuperTypes().add(this.getDeceptionTechnique());
		dissimulationEClass.getESuperTypes().add(this.getDeceptionTechnique());
		agentAssignmentEClass.getESuperTypes().add(this.getAgentRelation());
		andGoalRefinementEClass.getESuperTypes().add(this.getGoalRelationship());
		conflictGoalRefinementEClass.getESuperTypes().add(this.getGoalRelationship());
		agentMonitoringEClass.getESuperTypes().add(this.getAgentRelation());
		agentControlEClass.getESuperTypes().add(this.getAgentRelation());
		agentRelationEClass.getESuperTypes().add(this.getGoalRelationship());
		deceptionMetricEClass.getESuperTypes().add(this.getDeceptionNode());
		deceptionChannelEClass.getESuperTypes().add(this.getDeceptionNode());
		attackVectorEClass.getESuperTypes().add(this.getSecurityNode());
		aPropertyEClass.getESuperTypes().add(this.getSecurityNode());
		attackStepConnectorEClass.getESuperTypes().add(this.getAttackStep());
		initialAttackStepEClass.getESuperTypes().add(this.getAttackStep());
		endAttackStepEClass.getESuperTypes().add(this.getAttackStep());
		attackStepEClass.getESuperTypes().add(this.getSecurityNode());
		attackTreeEClass.getESuperTypes().add(this.getSecurityNode());
		goalNodeEClass.getESuperTypes().add(this.getNode());
		softGoalEClass.getESuperTypes().add(this.getGoal());
		deceptionBehaviourGoalEClass.getESuperTypes().add(this.getDeceptionGoal());
		deceptionRiskEClass.getESuperTypes().add(this.getRiskComponent());
		deceptionRiskEClass.getESuperTypes().add(this.getDeceptionNode());
		deceptionStrategyEClass.getESuperTypes().add(this.getAbstractFeature());
		deceptionTacticMechanismEClass.getESuperTypes().add(this.getAbstractFeature());
		strategyNodeEClass.getESuperTypes().add(this.getNode());
		featureEClass.getESuperTypes().add(this.getAbstractFeature());
		featureConstraintEClass.getESuperTypes().add(this.getStrategyRelationship());
		abstractFeatureEClass.getESuperTypes().add(this.getStrategyNode());
		requiredConstraintEClass.getESuperTypes().add(this.getFeatureConstraint());
		excludeConstraintEClass.getESuperTypes().add(this.getFeatureConstraint());
		benefitConstraintEClass.getESuperTypes().add(this.getFeatureConstraint());
		avoidConstraintEClass.getESuperTypes().add(this.getFeatureConstraint());
		nodeRefinementEClass.getESuperTypes().add(this.getStrategyRelationship());
		tacticRefinementEClass.getESuperTypes().add(this.getNodeRefinement());
		featureRefinementEClass.getESuperTypes().add(this.getNodeRefinement());
		strategyTacticRefinementEClass.getESuperTypes().add(this.getNodeRefinement());
		tacticFeatureRefinementEClass.getESuperTypes().add(this.getNodeRefinement());
		obstructionElementEClass.getESuperTypes().add(this.getGoalNode());
		orObstructionRefinementEClass.getESuperTypes().add(this.getGoalRelationship());
		andObstructionRefinementEClass.getESuperTypes().add(this.getGoalRelationship());
		obstacleEClass.getESuperTypes().add(this.getObstructionElement());
		influenceNodeEClass.getESuperTypes().add(this.getNode());
		qualityAttributeEClass.getESuperTypes().add(this.getInfluenceNode());
		influenceEClass.getESuperTypes().add(this.getInfluenceRelationship());
		preferenceEClass.getESuperTypes().add(this.getInfluenceRelationship());
		eventEClass.getESuperTypes().add(this.getInfluenceNode());
		deceptionTacticEventEClass.getESuperTypes().add(this.getEvent());
		environmentEventEClass.getESuperTypes().add(this.getEvent());
		goalRefinableEClass.getESuperTypes().add(this.getGoalNode());
		beliefEClass.getESuperTypes().add(this.getGoalRefinable());
		deceptionGoalEClass.getESuperTypes().add(this.getGoal());
		deceptionGoalEClass.getESuperTypes().add(this.getDeceptionNode());
		deceptionSoftGoalEClass.getESuperTypes().add(this.getDeceptionGoal());
		concreteAssetEClass.getESuperTypes().add(this.getAsset());
		operationableEClass.getESuperTypes().add(this.getGoalNode());
		nodeSelectionEClass.getESuperTypes().add(this.getStrategyRelationship());
		deceptionTacticStrategyEClass.getESuperTypes().add(this.getAbstractFeature());
		deceptionStrategyTacticRefinementEClass.getESuperTypes().add(this.getNodeRefinement());
		deceptionTacticStrategyRefinementEClass.getESuperTypes().add(this.getNodeRefinement());
		deceptionStrategyRefinementEClass.getESuperTypes().add(this.getNodeRefinement());
		simulationHasArtificialElementEClass.getESuperTypes().add(this.getDeceptionRelationship());
		propertyEClass.getESuperTypes().add(this.getDeceptionNode());

		// Initialize classes, features, and operations; add parameters
		initEClass(dmtModelEClass, DMTModel.class, "DMTModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDMTModel_HasSecurityNodes(), this.getSecurityNode(), null, "hasSecurityNodes", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasDeceptionNodes(), this.getDeceptionNode(), null, "hasDeceptionNodes", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasGoalNodes(), this.getGoalNode(), null, "hasGoalNodes", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasGoalRelations(), this.getGoalRelationship(), null, "hasGoalRelations", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasStrategyNodes(), this.getStrategyNode(), null, "hasStrategyNodes", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasStrategyRelations(), this.getStrategyRelationship(), null, "hasStrategyRelations", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasDeceptionRelations(), this.getDeceptionRelationship(), null, "hasDeceptionRelations", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasSecurityRelations(), this.getSecurityRelationship(), null, "hasSecurityRelations", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasInfluenceNodes(), this.getInfluenceNode(), null, "hasInfluenceNodes", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasInfluenceRelations(), this.getInfluenceRelationship(), null, "hasInfluenceRelations", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasGoalContainers(), this.getGoalContainer(), null, "hasGoalContainers", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDMTModel_HasMiscNodes(), this.getAnnotation(), null, "hasMiscNodes", null, 0, -1, DMTModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(goalEClass, Goal.class, "Goal", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGoal_GoalCategory(), this.getGoalCategoryEnum(), "goalCategory", "UNDEFINED", 0, 1, Goal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGoal_Priority(), this.getGoalPriorityEnum(), "priority", null, 0, 1, Goal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGoal_Source(), ecorePackage.getEString(), "source", null, 0, 1, Goal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGoal_Stability(), this.getGoalStabilityEnum(), "stability", "LOW", 0, 1, Goal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGoal_Resolution(), this.getObstructionElement(), null, "resolution", null, 0, -1, Goal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(goalRelationshipEClass, GoalRelationship.class, "GoalRelationship", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGoalRelationship_Status(), ecorePackage.getEString(), "status", null, 0, 1, GoalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGoalRelationship_Tactic(), ecorePackage.getEString(), "tactic", null, 0, 1, GoalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGoalRelationship_SysRef(), ecorePackage.getEString(), "sysRef", null, 0, 1, GoalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGoalRelationship_AltName(), ecorePackage.getEString(), "altName", null, 0, 1, GoalRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(agentEClass, Agent.class, "Agent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAgent_Def(), theEcorePackage.getEString(), "def", null, 0, 1, Agent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgent_Load(), ecorePackage.getEString(), "load", null, 0, 1, Agent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAgent_Subagent(), this.getAgent(), null, "subagent", null, 0, -1, Agent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAgent_PerformsOperation(), this.getOperationable(), null, "performsOperation", null, 0, -1, Agent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationalizationEClass, Operationalization.class, "Operationalization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOperationalization_ReqPre(), theEcorePackage.getEString(), "reqPre", null, 0, 1, Operationalization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationalization_ReqTrig(), theEcorePackage.getEString(), "reqTrig", null, 0, 1, Operationalization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationalization_ReqPost(), theEcorePackage.getEString(), "reqPost", null, 0, 1, Operationalization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationalization_OperationalizeGoal(), this.getLeafGoal(), null, "operationalizeGoal", null, 1, 1, Operationalization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getOperationalization_Operationalization(), this.getOperationable(), null, "operationalization", null, 1, 1, Operationalization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(softwareAgentEClass, SoftwareAgent.class, "SoftwareAgent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSoftwareAgent_ResponsibilitySoftware(), this.getRequirement(), null, "responsibilitySoftware", null, 1, -1, SoftwareAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(environmentAgentEClass, EnvironmentAgent.class, "EnvironmentAgent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEnvironmentAgent_Type(), this.getEnvironmentAgentTypeEnum(), "type", null, 0, 1, EnvironmentAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnvironmentAgent_ResponsibilityEnvironment(), this.getExpectation(), null, "responsibilityEnvironment", null, 1, -1, EnvironmentAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(behaviourGoalEClass, BehaviourGoal.class, "BehaviourGoal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBehaviourGoal_Type(), this.getGoalTypeEnum(), "type", null, 0, 1, BehaviourGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBehaviourGoal_FormalSpec(), ecorePackage.getEString(), "formalSpec", null, 0, 1, BehaviourGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(domainDescriptionEClass, DomainDescription.class, "DomainDescription", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDomainDescription_DomainCategory(), this.getDomainPropertyCategoryEnum(), "domainCategory", "DOMAINVAR", 0, 1, DomainDescription.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(leafGoalEClass, LeafGoal.class, "LeafGoal", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(expectationEClass, Expectation.class, "Expectation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(requirementEClass, Requirement.class, "Requirement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(operationEClass, Operation.class, "Operation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(orGoalRefinementEClass, ORGoalRefinement.class, "ORGoalRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getORGoalRefinement_OrGoalRefine(), this.getGoalRefinable(), null, "orGoalRefine", null, 1, 1, ORGoalRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(securityNodeEClass, SecurityNode.class, "SecurityNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mitigableElementEClass, MitigableElement.class, "MitigableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(attackEClass, Attack.class, "Attack", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAttack_Type(), this.getAttackTypeEnum(), "type", "ACTIVE", 0, 1, Attack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttack_InstantiatesThreat(), this.getThreat(), null, "instantiatesThreat", null, 0, -1, Attack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttack_ExploitsVulnerability(), this.getVulnerability(), null, "exploitsVulnerability", null, 0, -1, Attack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttack_ContributesToAttack(), this.getAttack(), null, "contributesToAttack", null, 0, -1, Attack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttack_LeadsRisk(), this.getRiskComponent(), null, "leadsRisk", null, 0, -1, Attack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAttack_Origin(), this.getAttackOriginEnum(), "origin", null, 0, 1, Attack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttack_RealizedByAttackVector(), this.getAttackVector(), this.getAttackVector_RealizesAttack(), "realizedByAttackVector", null, 0, -1, Attack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(threatEClass, Threat.class, "Threat", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getThreat_Category(), this.getThreatCategoryEnum(), "category", "INTENTIONAL", 0, 1, Threat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThreat_Scenario(), ecorePackage.getEString(), "scenario", null, 0, 1, Threat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getThreat_ContributesToThreat(), this.getThreat(), null, "contributesToThreat", null, 0, -1, Threat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getThreat_Consequence(), ecorePackage.getEString(), "consequence", null, 0, 1, Threat.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vulnerabilityEClass, Vulnerability.class, "Vulnerability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVulnerability_WindowOccurrence(), ecorePackage.getEString(), "windowOccurrence", null, 0, 1, Vulnerability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVulnerability_VulnerabilityType(), this.getVulnerabilityTypeEnum(), "vulnerabilityType", "TECHNICAL", 0, 1, Vulnerability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(securityControlEClass, SecurityControl.class, "SecurityControl", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSecurityControl_Pros(), ecorePackage.getEString(), "pros", null, 0, 1, SecurityControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSecurityControl_Cons(), ecorePackage.getEString(), "cons", null, 0, 1, SecurityControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSecurityControl_Nature(), this.getSecurityControlNatureEnum(), "nature", "TECHNICAL", 0, 1, SecurityControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSecurityControl_Incident(), this.getSecurityControlIncidentEnum(), "incident", null, 0, 1, SecurityControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSecurityControl_Implements(), this.getRequirement(), null, "implements", null, 0, 1, SecurityControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(riskComponentEClass, RiskComponent.class, "RiskComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRiskComponent_RiskSeverity(), this.getRiskSeverityEnum(), "riskSeverity", "INSIGNIFICANT", 0, 1, RiskComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRiskComponent_RiskLikelihood(), this.getRiskLikelihoodEnum(), "riskLikelihood", null, 0, 1, RiskComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRiskComponent_HarmsAsset(), this.getConcreteAsset(), null, "harmsAsset", null, 0, -1, RiskComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attackerEClass, Attacker.class, "Attacker", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAttacker_Profile(), this.getAttackerProfileEnum(), "profile", "SCRIPTKIDDIE", 0, 1, Attacker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttacker_LaunchesAttack(), this.getAttack(), null, "launchesAttack", null, 0, -1, Attacker.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(assetEClass, Asset.class, "Asset", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAsset_AssetType(), this.getAssetTypeEnum(), "assetType", "DATA", 0, 1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAsset_Location(), ecorePackage.getEString(), "location", null, 0, 1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAsset_HasProperty(), this.getAProperty(), null, "hasProperty", null, 0, -1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAsset_Documentation(), ecorePackage.getEString(), "documentation", null, 0, 1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAsset_UID(), ecorePackage.getEString(), "UID", null, 0, 1, Asset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionNodeEClass, DeceptionNode.class, "DeceptionNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(deceptionTacticEClass, DeceptionTactic.class, "DeceptionTactic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeceptionTactic_SubTactic(), this.getDeceptionTactic(), null, "subTactic", null, 0, -1, DeceptionTactic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionTactic_ApplyDissimulation(), this.getDissimulation(), null, "applyDissimulation", null, 0, -1, DeceptionTactic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionTactic_DescribedByDeceptionStory(), this.getDeceptionStory(), null, "describedByDeceptionStory", null, 0, -1, DeceptionTactic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionTactic_MeasuredBy(), this.getDeceptionMetric(), null, "measuredBy", null, 0, -1, DeceptionTactic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionTactic_MonitoredBy(), this.getDeceptionChannel(), null, "monitoredBy", null, 0, -1, DeceptionTactic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionTactic_LeadDeceptionRisk(), this.getDeceptionRisk(), null, "leadDeceptionRisk", null, 0, -1, DeceptionTactic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionTactic_ApplySimulation(), this.getSimulation(), null, "applySimulation", null, 0, -1, DeceptionTactic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionTactic_IsAbstract(), ecorePackage.getEBoolean(), "isAbstract", null, 0, 1, DeceptionTactic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionTactic_Qualityattribute(), this.getQualityAttribute(), null, "qualityattribute", null, 0, -1, DeceptionTactic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(iVulnerabilityEClass, IVulnerability.class, "IVulnerability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIVulnerability_WindowOccurence(), ecorePackage.getEString(), "windowOccurence", null, 0, 1, IVulnerability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getIVulnerability_VulnerabilityType(), this.getVulnerabilityTypeEnum(), "vulnerabilityType", "TECHNICAL", 0, 1, IVulnerability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionTechniqueEClass, DeceptionTechnique.class, "DeceptionTechnique", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionTechnique_FormalSpec(), ecorePackage.getEString(), "formalSpec", null, 0, 1, DeceptionTechnique.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionTechnique_InformalSpec(), ecorePackage.getEString(), "InformalSpec", null, 0, 1, DeceptionTechnique.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionTechnique_ExpectedOutcome(), ecorePackage.getEString(), "expectedOutcome", null, 0, 1, DeceptionTechnique.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionStoryEClass, DeceptionStory.class, "DeceptionStory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionStory_ExternalIdentifier(), ecorePackage.getEString(), "externalIdentifier", null, 0, 1, DeceptionStory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionStory_ExploitBias(), this.getDeceptionBias(), null, "exploitBias", null, 0, -1, DeceptionStory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionStory_StoryType(), this.getStoryTypeEnum(), "storyType", null, 0, 1, DeceptionStory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionStory_PreCondition(), ecorePackage.getEString(), "preCondition", null, 0, 1, DeceptionStory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionStory_PosCondition(), ecorePackage.getEString(), "posCondition", null, 0, 1, DeceptionStory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionStory_Interaction(), theInteractionsPackage.getInteraction(), null, "interaction", null, 0, 1, DeceptionStory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionBiasEClass, DeceptionBias.class, "DeceptionBias", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionBias_Type(), this.getBiasTypeEnum(), "type", null, 0, 1, DeceptionBias.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(artificialElementEClass, ArtificialElement.class, "ArtificialElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getArtificialElement_ContainVulnerability(), this.getIVulnerability(), null, "containVulnerability", null, 0, -1, ArtificialElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getArtificialElement_FakeType(), this.getArtificialElementTypeEnum(), "fakeType", "FAKEDATA", 0, 1, ArtificialElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mitigationEClass, Mitigation.class, "Mitigation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMitigation_MitigationLevel(), this.getMitigationLevelEnum(), "mitigationLevel", "M", 0, 1, Mitigation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMitigation_MitigationTarget(), this.getMitigableElement(), null, "mitigationTarget", null, 1, 1, Mitigation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMitigation_MitigationSource(), this.getMitigator(), null, "mitigationSource", null, 1, 1, Mitigation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interactionNodesEClass, InteractionNodes.class, "InteractionNodes", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(interactionObjectEClass, InteractionObject.class, "InteractionObject", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInteractionObject_ObjectName(), ecorePackage.getEString(), "objectName", null, 0, 1, InteractionObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInteractionObject_ClassName(), ecorePackage.getEString(), "className", null, 0, 1, InteractionObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(linkEClass, Link.class, "Link", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLink_IntegerId(), ecorePackage.getEInt(), "integerId", null, 1, 1, Link.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLink_Message(), ecorePackage.getEString(), "message", null, 0, 1, Link.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLink_ToObject(), this.getInteractionObject(), null, "toObject", null, 0, 1, Link.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLink_FromObject(), this.getInteractionObject(), null, "fromObject", null, 0, 1, Link.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simulationEClass, Simulation.class, "Simulation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSimulation_Type(), this.getSimulationTechniqueTypeEnum(), "type", null, 0, 1, Simulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_AssetReference(), this.getConcreteAsset(), null, "assetReference", null, 0, -1, Simulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dissimulationEClass, Dissimulation.class, "Dissimulation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDissimulation_Type(), this.getDissimulationTechniqueTypeEnum(), "type", "MASKING", 0, 1, Dissimulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDissimulation_HideRealAsset(), this.getConcreteAsset(), null, "hideRealAsset", null, 1, -1, Dissimulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nodeEClass, Node.class, "Node", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNode_Name(), ecorePackage.getEString(), "name", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNode_Description(), ecorePackage.getEString(), "description", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNode_UUID(), theEcorePackage.getEString(), "UUID", null, 0, 1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNode_HasAnnotations(), this.getAnnotation(), null, "hasAnnotations", null, 0, -1, Node.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(agentAssignmentEClass, AgentAssignment.class, "AgentAssignment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAgentAssignment_AgentGoalAssignment(), this.getLeafGoal(), null, "agentGoalAssignment", null, 1, 1, AgentAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAgentAssignment_AgentAssignment(), this.getAgent(), null, "agentAssignment", null, 1, 1, AgentAssignment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(andGoalRefinementEClass, ANDGoalRefinement.class, "ANDGoalRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getANDGoalRefinement_AndGoalRefine(), this.getGoalRefinable(), null, "andGoalRefine", null, 1, 1, ANDGoalRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conflictGoalRefinementEClass, ConflictGoalRefinement.class, "ConflictGoalRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConflictGoalRefinement_ConflictGoalSource(), this.getGoal(), null, "conflictGoalSource", null, 1, 1, ConflictGoalRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getConflictGoalRefinement_ConflictGoalTarget(), this.getGoal(), null, "conflictGoalTarget", null, 1, 1, ConflictGoalRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(agentMonitoringEClass, AgentMonitoring.class, "AgentMonitoring", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAgentMonitoring_AgentMonitor(), this.getInteractionObject(), null, "agentMonitor", null, 1, -1, AgentMonitoring.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAgentMonitoring_ObjectAttribute(), ecorePackage.getEString(), "objectAttribute", null, 0, 1, AgentMonitoring.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(agentControlEClass, AgentControl.class, "AgentControl", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAgentControl_ObjectAttribute(), ecorePackage.getEString(), "objectAttribute", null, 0, 1, AgentControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAgentControl_AgentControl(), this.getInteractionObject(), null, "agentControl", null, 1, -1, AgentControl.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(agentRelationEClass, AgentRelation.class, "AgentRelation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(deceptionMetricEClass, DeceptionMetric.class, "DeceptionMetric", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionMetric_MetricType(), this.getMetricTypeEnum(), "metricType", null, 0, 1, DeceptionMetric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionMetric_Unit(), ecorePackage.getEString(), "unit", null, 0, 1, DeceptionMetric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionChannelEClass, DeceptionChannel.class, "DeceptionChannel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionChannel_Type(), this.getChannelTypeEnum(), "type", null, 0, 1, DeceptionChannel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attackVectorEClass, AttackVector.class, "AttackVector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAttackVector_Example(), ecorePackage.getEString(), "example", null, 0, 1, AttackVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAttackVector_Resources(), ecorePackage.getEString(), "resources", null, 0, 1, AttackVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttackVector_RealizesAttack(), this.getAttack(), this.getAttack_RealizedByAttackVector(), "realizesAttack", null, 0, 1, AttackVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttackVector_Initialattackstep(), this.getInitialAttackStep(), null, "initialattackstep", null, 0, 1, AttackVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttackVector_Endattackstep(), this.getEndAttackStep(), null, "endattackstep", null, 0, 1, AttackVector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aPropertyEClass, AProperty.class, "AProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAProperty_Atype(), ecorePackage.getEString(), "atype", null, 0, 1, AProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attackStepConnectorEClass, AttackStepConnector.class, "AttackStepConnector", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttackStepConnector_Subattackstep(), this.getAttackStepConnector(), null, "subattackstep", null, 0, -1, AttackStepConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAttackStepConnector_StepDescription(), ecorePackage.getEString(), "stepDescription", null, 0, 1, AttackStepConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAttackStepConnector_AttackStepConnectorType(), this.getAttackConnectorEnum(), "attackStepConnectorType", "OR", 0, 1, AttackStepConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAttackStepConnector_Sequence(), ecorePackage.getEInt(), "sequence", null, 0, 1, AttackStepConnector.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(initialAttackStepEClass, InitialAttackStep.class, "InitialAttackStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInitialAttackStep_HasAttackSteps(), this.getAttackStepConnector(), null, "hasAttackSteps", null, 0, -1, InitialAttackStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInitialAttackStep_PreCondition(), ecorePackage.getEString(), "preCondition", null, 0, 1, InitialAttackStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(endAttackStepEClass, EndAttackStep.class, "EndAttackStep", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEndAttackStep_ExpectedResult(), ecorePackage.getEString(), "expectedResult", null, 0, 1, EndAttackStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attackStepEClass, AttackStep.class, "AttackStep", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(attackTreeEClass, AttackTree.class, "AttackTree", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttackTree_HasAttackvector(), this.getAttackVector(), null, "hasAttackvector", null, 0, -1, AttackTree.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(goalNodeEClass, GoalNode.class, "GoalNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(softGoalEClass, SoftGoal.class, "SoftGoal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSoftGoal_FitCriterion(), ecorePackage.getEString(), "fitCriterion", null, 0, 1, SoftGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSoftGoal_SoftGoalType(), this.getSoftGoalTypeEnum(), "softGoalType", null, 0, 1, SoftGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSoftGoal_SoftGoalOpenType(), theEcorePackage.getEString(), "softGoalOpenType", null, 0, 1, SoftGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionBehaviourGoalEClass, DeceptionBehaviourGoal.class, "DeceptionBehaviourGoal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionBehaviourGoal_FormalSpec(), ecorePackage.getEString(), "formalSpec", null, 0, 1, DeceptionBehaviourGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionBehaviourGoal_Type(), this.getGoalTypeEnum(), "type", null, 0, 1, DeceptionBehaviourGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionRiskEClass, DeceptionRisk.class, "DeceptionRisk", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionRisk_DeceptionRiskType(), this.getDeceptionRiskTypeEnum(), "deceptionRiskType", null, 0, 1, DeceptionRisk.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionStrategyEClass, DeceptionStrategy.class, "DeceptionStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionStrategy_IsRoot(), ecorePackage.getEBoolean(), "isRoot", null, 0, 1, DeceptionStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionStrategy_ExecutionParameter(), theEcorePackage.getEString(), "executionParameter", null, 0, 1, DeceptionStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionTacticMechanismEClass, DeceptionTacticMechanism.class, "DeceptionTacticMechanism", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionTacticMechanism_EnablingCondition(), ecorePackage.getEString(), "enablingCondition", null, 0, 1, DeceptionTacticMechanism.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionTacticMechanism_TacticReference(), this.getDeceptionTactic(), null, "tacticReference", null, 0, 1, DeceptionTacticMechanism.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(strategyNodeEClass, StrategyNode.class, "StrategyNode", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStrategyNode_Property(), this.getProperty(), null, "property", null, 0, -1, StrategyNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureEClass, Feature.class, "Feature", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(strategyRelationshipEClass, StrategyRelationship.class, "StrategyRelationship", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStrategyRelationship_Description(), theEcorePackage.getEString(), "description", null, 0, 1, StrategyRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureConstraintEClass, FeatureConstraint.class, "FeatureConstraint", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureConstraint_Source(), this.getAbstractFeature(), null, "source", null, 1, 1, FeatureConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureConstraint_Target(), this.getAbstractFeature(), null, "target", null, 1, 1, FeatureConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(abstractFeatureEClass, AbstractFeature.class, "AbstractFeature", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAbstractFeature_BindingState(), this.getBindingStateEnum(), "bindingState", null, 0, 1, AbstractFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAbstractFeature_IsAbstract(), ecorePackage.getEBoolean(), "isAbstract", "false", 0, 1, AbstractFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(requiredConstraintEClass, RequiredConstraint.class, "RequiredConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(excludeConstraintEClass, ExcludeConstraint.class, "ExcludeConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(benefitConstraintEClass, BenefitConstraint.class, "BenefitConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(avoidConstraintEClass, AvoidConstraint.class, "AvoidConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(nodeRefinementEClass, NodeRefinement.class, "NodeRefinement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNodeRefinement_EnactmentChoice(), this.getEnactmentEnum(), "enactmentChoice", null, 0, 1, NodeRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tacticRefinementEClass, TacticRefinement.class, "TacticRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTacticRefinement_Tacticrefinementtarget(), this.getDeceptionTacticMechanism(), null, "tacticrefinementtarget", null, 1, 1, TacticRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTacticRefinement_Tacticrefinementsource(), this.getDeceptionTacticMechanism(), null, "tacticrefinementsource", null, 1, 1, TacticRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(featureRefinementEClass, FeatureRefinement.class, "FeatureRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFeatureRefinement_Featurerefinementtarget(), this.getFeature(), null, "featurerefinementtarget", null, 1, 1, FeatureRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFeatureRefinement_Featurerefinementsource(), this.getFeature(), null, "featurerefinementsource", null, 1, 1, FeatureRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(strategyTacticRefinementEClass, StrategyTacticRefinement.class, "StrategyTacticRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getStrategyTacticRefinement_Strategytacticrefinementtarget(), this.getDeceptionTacticMechanism(), null, "strategytacticrefinementtarget", null, 1, 1, StrategyTacticRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getStrategyTacticRefinement_Strategytacticrefinementsource(), this.getDeceptionStrategy(), null, "strategytacticrefinementsource", null, 1, 1, StrategyTacticRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tacticFeatureRefinementEClass, TacticFeatureRefinement.class, "TacticFeatureRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTacticFeatureRefinement_Tacticfeaturerefinementtarget(), this.getFeature(), null, "tacticfeaturerefinementtarget", null, 1, 1, TacticFeatureRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTacticFeatureRefinement_Tacticfeaturerefinementsource(), this.getDeceptionTacticMechanism(), null, "tacticfeaturerefinementsource", null, 1, 1, TacticFeatureRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionRelationshipEClass, DeceptionRelationship.class, "DeceptionRelationship", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionRelationship_AltName(), ecorePackage.getEString(), "altName", null, 0, 1, DeceptionRelationship.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mitigatorEClass, Mitigator.class, "Mitigator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(securityRelationshipEClass, SecurityRelationship.class, "SecurityRelationship", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(obstructionElementEClass, ObstructionElement.class, "ObstructionElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObstructionElement_Obstruct(), this.getObstructedElement(), null, "obstruct", null, 0, -1, ObstructionElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObstructionElement_ORObstructionRefined(), this.getORObstructionRefinement(), null, "ORObstructionRefined", null, 0, -1, ObstructionElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObstructionElement_ANDObstructionRefined(), this.getANDObstructionRefinement(), null, "ANDObstructionRefined", null, 0, -1, ObstructionElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(orObstructionRefinementEClass, ORObstructionRefinement.class, "ORObstructionRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getORObstructionRefinement_ORObstructionRefine(), this.getObstructionElement(), null, "ORObstructionRefine", null, 1, 1, ORObstructionRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(andObstructionRefinementEClass, ANDObstructionRefinement.class, "ANDObstructionRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getANDObstructionRefinement_ANDObstructionRefine(), this.getObstructionElement(), null, "ANDObstructionRefine", null, 1, 1, ANDObstructionRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(obstacleEClass, Obstacle.class, "Obstacle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getObstacle_ObstacleCategory(), this.getObstacleCategoryEnum(), "obstacleCategory", null, 0, 1, Obstacle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getObstacle_FormalDef(), theEcorePackage.getEString(), "formalDef", null, 0, 1, Obstacle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getObstacle_Likelihood(), this.getRiskLikelihoodEnum(), "likelihood", null, 0, 1, Obstacle.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(influenceNodeEClass, InfluenceNode.class, "InfluenceNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(qualityAttributeEClass, QualityAttribute.class, "QualityAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(influenceRelationshipEClass, InfluenceRelationship.class, "InfluenceRelationship", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(influenceEClass, Influence.class, "Influence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInfluence_Weight(), this.getInfluenceWeightEnum(), "weight", null, 0, 1, Influence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInfluence_Polarity(), this.getInfluencePolarityEnum(), "polarity", null, 0, 1, Influence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInfluence_InfluenceSource(), this.getEvent(), null, "influenceSource", null, 1, 1, Influence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getInfluence_Influencetarget(), this.getInfluenceNode(), null, "influencetarget", null, 1, 1, Influence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(preferenceEClass, Preference.class, "Preference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPreference_PreferenceTarget(), this.getQualityAttribute(), null, "preferenceTarget", null, 1, 1, Preference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPreference_PreferenceSource(), this.getQualityAttribute(), null, "preferenceSource", null, 1, 1, Preference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPreference_Weight(), this.getPreferenceWeightEnum(), "weight", null, 0, 1, Preference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventEClass, Event.class, "Event", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(deceptionTacticEventEClass, DeceptionTacticEvent.class, "DeceptionTacticEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(environmentEventEClass, EnvironmentEvent.class, "EnvironmentEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(obstructedElementEClass, ObstructedElement.class, "ObstructedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(vulnerableEClass, Vulnerable.class, "Vulnerable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVulnerable_Vulnerability(), this.getVulnerability(), null, "vulnerability", null, 0, -1, Vulnerable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(goalRefinableEClass, GoalRefinable.class, "GoalRefinable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGoalRefinable_OrGoalRefined(), this.getORGoalRefinement(), null, "orGoalRefined", null, 0, -1, GoalRefinable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getGoalRefinable_AndGoalRefined(), this.getANDGoalRefinement(), null, "andGoalRefined", null, 0, -1, GoalRefinable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(beliefEClass, Belief.class, "Belief", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(deceptionGoalEClass, DeceptionGoal.class, "DeceptionGoal", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionGoal_DeceptionGoalType(), this.getDeceptionGoalTypeEnum(), "DeceptionGoalType", null, 0, 1, DeceptionGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionSoftGoalEClass, DeceptionSoftGoal.class, "DeceptionSoftGoal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeceptionSoftGoal_FitCriterion(), ecorePackage.getEString(), "fitCriterion", null, 0, 1, DeceptionSoftGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionSoftGoal_SoftGoalType(), this.getSoftGoalTypeEnum(), "softGoalType", null, 0, 1, DeceptionSoftGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeceptionSoftGoal_SoftGoalOpenType(), theEcorePackage.getEString(), "softGoalOpenType", null, 0, 1, DeceptionSoftGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(concreteAssetEClass, ConcreteAsset.class, "ConcreteAsset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConcreteAsset_BusinessImportance(), ecorePackage.getEInt(), "businessImportance", null, 0, 1, ConcreteAsset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationableEClass, Operationable.class, "Operationable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOperationable_DomPost(), ecorePackage.getEString(), "domPost", null, 0, 1, Operationable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationable_DomPre(), ecorePackage.getEString(), "domPre", null, 0, 1, Operationable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationable_FormalDomPost(), ecorePackage.getEString(), "formalDomPost", null, 0, 1, Operationable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperationable_FormalDomPre(), ecorePackage.getEString(), "formalDomPre", null, 0, 1, Operationable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(goalContainerEClass, GoalContainer.class, "GoalContainer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGoalContainer_Goal(), this.getGoal(), null, "goal", null, 0, -1, GoalContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nodeSelectionEClass, NodeSelection.class, "NodeSelection", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNodeSelection_Selectionsource(), this.getNodeRefinement(), null, "selectionsource", null, 1, 1, NodeSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNodeSelection_Selectiontarget(), this.getNodeRefinement(), null, "selectiontarget", null, 1, 1, NodeSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNodeSelection_Type(), this.getNodesSelectionType(), "type", null, 0, 1, NodeSelection.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionTacticStrategyEClass, DeceptionTacticStrategy.class, "DeceptionTacticStrategy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeceptionTacticStrategy_Deceptiontacticmechanism(), this.getDeceptionTacticMechanism(), null, "deceptiontacticmechanism", null, 1, 1, DeceptionTacticStrategy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionStrategyTacticRefinementEClass, DeceptionStrategyTacticRefinement.class, "DeceptionStrategyTacticRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeceptionStrategyTacticRefinement_Strategytacticrefinementsource(), this.getDeceptionStrategy(), null, "strategytacticrefinementsource", null, 1, 1, DeceptionStrategyTacticRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionStrategyTacticRefinement_Strategytacticrefinementtarget(), this.getDeceptionTacticStrategy(), null, "strategytacticrefinementtarget", null, 1, 1, DeceptionStrategyTacticRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionTacticStrategyRefinementEClass, DeceptionTacticStrategyRefinement.class, "DeceptionTacticStrategyRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementsource(), this.getDeceptionTacticStrategy(), null, "tacticstrategytacticrefinementsource", null, 1, 1, DeceptionTacticStrategyRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementtarget(), this.getDeceptionTacticStrategy(), null, "tacticstrategytacticrefinementtarget", null, 1, 1, DeceptionTacticStrategyRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deceptionStrategyRefinementEClass, DeceptionStrategyRefinement.class, "DeceptionStrategyRefinement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeceptionStrategyRefinement_Deceptionstrategyrefinementsource(), this.getDeceptionStrategy(), null, "deceptionstrategyrefinementsource", null, 1, 1, DeceptionStrategyRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeceptionStrategyRefinement_Deceptionstrategyrefinementtarget(), this.getDeceptionStrategy(), null, "deceptionstrategyrefinementtarget", null, 1, 1, DeceptionStrategyRefinement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simulationHasArtificialElementEClass, SimulationHasArtificialElement.class, "SimulationHasArtificialElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSimulationHasArtificialElement_ShowFalseAsset(), this.getArtificialElement(), null, "showFalseAsset", null, 1, 1, SimulationHasArtificialElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationHasArtificialElement_LowerFakes(), theEcorePackage.getEString(), "lowerFakes", null, 1, 1, SimulationHasArtificialElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulationHasArtificialElement_UpperFakes(), theEcorePackage.getEString(), "upperFakes", null, 1, 1, SimulationHasArtificialElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulationHasArtificialElement_SimulationType(), this.getSimulation(), null, "simulationType", null, 1, 1, SimulationHasArtificialElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(propertyEClass, Property.class, "Property", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProperty_Value(), theEcorePackage.getEString(), "value", null, 0, 1, Property.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(annotationEClass, Annotation.class, "Annotation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAnnotation_Text(), theEcorePackage.getEString(), "text", null, 0, 1, Annotation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(goalCategoryEnumEEnum, GoalCategoryEnum.class, "GoalCategoryEnum");
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.UNDEFINED);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.ACCURACY);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.ADAPTABILITY);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.CAPACITY);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.CONSISTENCY);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.EFFICIENCY);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.INFORMATION);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.PERFORMANCE);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.PRIVACY);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.ROBUSTNESS);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.SAFETY);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.SATISFACTION);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.SECURITY);
		addEEnumLiteral(goalCategoryEnumEEnum, GoalCategoryEnum.USABILITY);

		initEEnum(domainPropertyCategoryEnumEEnum, DomainPropertyCategoryEnum.class, "DomainPropertyCategoryEnum");
		addEEnumLiteral(domainPropertyCategoryEnumEEnum, DomainPropertyCategoryEnum.DOMAINVAR);
		addEEnumLiteral(domainPropertyCategoryEnumEEnum, DomainPropertyCategoryEnum.HYPOTHESIS);

		initEEnum(goalPriorityEnumEEnum, GoalPriorityEnum.class, "GoalPriorityEnum");
		addEEnumLiteral(goalPriorityEnumEEnum, GoalPriorityEnum.UNDEFINED);
		addEEnumLiteral(goalPriorityEnumEEnum, GoalPriorityEnum.LOW);
		addEEnumLiteral(goalPriorityEnumEEnum, GoalPriorityEnum.MEDIUM);
		addEEnumLiteral(goalPriorityEnumEEnum, GoalPriorityEnum.HIGH);

		initEEnum(mitigationLevelEnumEEnum, MitigationLevelEnum.class, "MitigationLevelEnum");
		addEEnumLiteral(mitigationLevelEnumEEnum, MitigationLevelEnum.LOW_MITIGATION);
		addEEnumLiteral(mitigationLevelEnumEEnum, MitigationLevelEnum.NORMAL_MITIGATION);
		addEEnumLiteral(mitigationLevelEnumEEnum, MitigationLevelEnum.HIGH_MITIGATION);

		initEEnum(assetTypeEnumEEnum, AssetTypeEnum.class, "AssetTypeEnum");
		addEEnumLiteral(assetTypeEnumEEnum, AssetTypeEnum.SOFTWARE);
		addEEnumLiteral(assetTypeEnumEEnum, AssetTypeEnum.DATA);
		addEEnumLiteral(assetTypeEnumEEnum, AssetTypeEnum.HARDWARE);
		addEEnumLiteral(assetTypeEnumEEnum, AssetTypeEnum.FACILITY);

		initEEnum(attackTypeEnumEEnum, AttackTypeEnum.class, "AttackTypeEnum");
		addEEnumLiteral(attackTypeEnumEEnum, AttackTypeEnum.PASSIVE);
		addEEnumLiteral(attackTypeEnumEEnum, AttackTypeEnum.ACTIVE);

		initEEnum(attackerProfileEnumEEnum, AttackerProfileEnum.class, "AttackerProfileEnum");
		addEEnumLiteral(attackerProfileEnumEEnum, AttackerProfileEnum.SCRIPTKIDDIE);
		addEEnumLiteral(attackerProfileEnumEEnum, AttackerProfileEnum.SKILLED);

		initEEnum(riskLikelihoodEnumEEnum, RiskLikelihoodEnum.class, "RiskLikelihoodEnum");
		addEEnumLiteral(riskLikelihoodEnumEEnum, RiskLikelihoodEnum.VERYLOW);
		addEEnumLiteral(riskLikelihoodEnumEEnum, RiskLikelihoodEnum.LOW);
		addEEnumLiteral(riskLikelihoodEnumEEnum, RiskLikelihoodEnum.MODERATE);
		addEEnumLiteral(riskLikelihoodEnumEEnum, RiskLikelihoodEnum.HIGH);
		addEEnumLiteral(riskLikelihoodEnumEEnum, RiskLikelihoodEnum.VERYHIGH);

		initEEnum(riskSeverityEnumEEnum, RiskSeverityEnum.class, "RiskSeverityEnum");
		addEEnumLiteral(riskSeverityEnumEEnum, RiskSeverityEnum.INSIGNIFICANT);
		addEEnumLiteral(riskSeverityEnumEEnum, RiskSeverityEnum.MINOR);
		addEEnumLiteral(riskSeverityEnumEEnum, RiskSeverityEnum.MODERATE);
		addEEnumLiteral(riskSeverityEnumEEnum, RiskSeverityEnum.MAJOR);
		addEEnumLiteral(riskSeverityEnumEEnum, RiskSeverityEnum.CATASTROPHIC);
		addEEnumLiteral(riskSeverityEnumEEnum, RiskSeverityEnum.DOOMSDAY);

		initEEnum(securityControlIncidentEnumEEnum, SecurityControlIncidentEnum.class, "SecurityControlIncidentEnum");
		addEEnumLiteral(securityControlIncidentEnumEEnum, SecurityControlIncidentEnum.PREVENTIVE);
		addEEnumLiteral(securityControlIncidentEnumEEnum, SecurityControlIncidentEnum.DETECTIVE);
		addEEnumLiteral(securityControlIncidentEnumEEnum, SecurityControlIncidentEnum.CORRECTIVE);

		initEEnum(threatCategoryEnumEEnum, ThreatCategoryEnum.class, "ThreatCategoryEnum");
		addEEnumLiteral(threatCategoryEnumEEnum, ThreatCategoryEnum.INTENTIONAL);
		addEEnumLiteral(threatCategoryEnumEEnum, ThreatCategoryEnum.ACIDENTAL);
		addEEnumLiteral(threatCategoryEnumEEnum, ThreatCategoryEnum.ENVIRONMENT);
		addEEnumLiteral(threatCategoryEnumEEnum, ThreatCategoryEnum.NEGLIGENCE);

		initEEnum(vulnerabilityTypeEnumEEnum, VulnerabilityTypeEnum.class, "VulnerabilityTypeEnum");
		addEEnumLiteral(vulnerabilityTypeEnumEEnum, VulnerabilityTypeEnum.SOCIAL);
		addEEnumLiteral(vulnerabilityTypeEnumEEnum, VulnerabilityTypeEnum.TECHNICAL);
		addEEnumLiteral(vulnerabilityTypeEnumEEnum, VulnerabilityTypeEnum.PROCEDURAL);
		addEEnumLiteral(vulnerabilityTypeEnumEEnum, VulnerabilityTypeEnum.PHYSICAL);
		addEEnumLiteral(vulnerabilityTypeEnumEEnum, VulnerabilityTypeEnum.NATURAL);

		initEEnum(securityControlNatureEnumEEnum, SecurityControlNatureEnum.class, "SecurityControlNatureEnum");
		addEEnumLiteral(securityControlNatureEnumEEnum, SecurityControlNatureEnum.ADMINISTRATIVE);
		addEEnumLiteral(securityControlNatureEnumEEnum, SecurityControlNatureEnum.TECHNICAL);
		addEEnumLiteral(securityControlNatureEnumEEnum, SecurityControlNatureEnum.PHYSICAL);

		initEEnum(biasTypeEnumEEnum, BiasTypeEnum.class, "BiasTypeEnum");
		addEEnumLiteral(biasTypeEnumEEnum, BiasTypeEnum.COGNITIVE);
		addEEnumLiteral(biasTypeEnumEEnum, BiasTypeEnum.CULTURAL);
		addEEnumLiteral(biasTypeEnumEEnum, BiasTypeEnum.ORGANIZATIONAL);
		addEEnumLiteral(biasTypeEnumEEnum, BiasTypeEnum.PERSONAL);

		initEEnum(simulationTechniqueTypeEnumEEnum, SimulationTechniqueTypeEnum.class, "SimulationTechniqueTypeEnum");
		addEEnumLiteral(simulationTechniqueTypeEnumEEnum, SimulationTechniqueTypeEnum.MIMICKING);
		addEEnumLiteral(simulationTechniqueTypeEnumEEnum, SimulationTechniqueTypeEnum.INVENTING);
		addEEnumLiteral(simulationTechniqueTypeEnumEEnum, SimulationTechniqueTypeEnum.DECOYING);

		initEEnum(metricTypeEnumEEnum, MetricTypeEnum.class, "MetricTypeEnum");
		addEEnumLiteral(metricTypeEnumEEnum, MetricTypeEnum.QUALITATIVE);
		addEEnumLiteral(metricTypeEnumEEnum, MetricTypeEnum.QUANTITATIVE);

		initEEnum(dissimulationTechniqueTypeEnumEEnum, DissimulationTechniqueTypeEnum.class, "DissimulationTechniqueTypeEnum");
		addEEnumLiteral(dissimulationTechniqueTypeEnumEEnum, DissimulationTechniqueTypeEnum.MASKING);
		addEEnumLiteral(dissimulationTechniqueTypeEnumEEnum, DissimulationTechniqueTypeEnum.REPACKAGING);
		addEEnumLiteral(dissimulationTechniqueTypeEnumEEnum, DissimulationTechniqueTypeEnum.DAZZLING);

		initEEnum(goalTypeEnumEEnum, GoalTypeEnum.class, "GoalTypeEnum");
		addEEnumLiteral(goalTypeEnumEEnum, GoalTypeEnum.ACHIEVE);
		addEEnumLiteral(goalTypeEnumEEnum, GoalTypeEnum.MAINTAIN);
		addEEnumLiteral(goalTypeEnumEEnum, GoalTypeEnum.AVOID);
		addEEnumLiteral(goalTypeEnumEEnum, GoalTypeEnum.CEASE);
		addEEnumLiteral(goalTypeEnumEEnum, GoalTypeEnum.OTHER);

		initEEnum(goalStabilityEnumEEnum, GoalStabilityEnum.class, "GoalStabilityEnum");
		addEEnumLiteral(goalStabilityEnumEEnum, GoalStabilityEnum.LOW);
		addEEnumLiteral(goalStabilityEnumEEnum, GoalStabilityEnum.MODERATE);
		addEEnumLiteral(goalStabilityEnumEEnum, GoalStabilityEnum.HIGH);

		initEEnum(environmentAgentTypeEnumEEnum, EnvironmentAgentTypeEnum.class, "EnvironmentAgentTypeEnum");
		addEEnumLiteral(environmentAgentTypeEnumEEnum, EnvironmentAgentTypeEnum.SOFTWARE);
		addEEnumLiteral(environmentAgentTypeEnumEEnum, EnvironmentAgentTypeEnum.DEVICE);
		addEEnumLiteral(environmentAgentTypeEnumEEnum, EnvironmentAgentTypeEnum.HUMAN);

		initEEnum(attackOriginEnumEEnum, AttackOriginEnum.class, "AttackOriginEnum");
		addEEnumLiteral(attackOriginEnumEEnum, AttackOriginEnum.INSIDER);
		addEEnumLiteral(attackOriginEnumEEnum, AttackOriginEnum.OUTSIDER);

		initEEnum(attackConnectorEnumEEnum, AttackConnectorEnum.class, "AttackConnectorEnum");
		addEEnumLiteral(attackConnectorEnumEEnum, AttackConnectorEnum.OR);
		addEEnumLiteral(attackConnectorEnumEEnum, AttackConnectorEnum.AND);
		addEEnumLiteral(attackConnectorEnumEEnum, AttackConnectorEnum.NONE);

		initEEnum(storyTypeEnumEEnum, StoryTypeEnum.class, "StoryTypeEnum");
		addEEnumLiteral(storyTypeEnumEEnum, StoryTypeEnum.ENGAGEMENT);
		addEEnumLiteral(storyTypeEnumEEnum, StoryTypeEnum.OPERATION);
		addEEnumLiteral(storyTypeEnumEEnum, StoryTypeEnum.TERMINATION);
		addEEnumLiteral(storyTypeEnumEEnum, StoryTypeEnum.SETUP);
		addEEnumLiteral(storyTypeEnumEEnum, StoryTypeEnum.OTHER);

		initEEnum(deceptionRiskTypeEnumEEnum, DeceptionRiskTypeEnum.class, "DeceptionRiskTypeEnum");
		addEEnumLiteral(deceptionRiskTypeEnumEEnum, DeceptionRiskTypeEnum.OBSCURITY);
		addEEnumLiteral(deceptionRiskTypeEnumEEnum, DeceptionRiskTypeEnum.LEGITIMATEUSE);
		addEEnumLiteral(deceptionRiskTypeEnumEEnum, DeceptionRiskTypeEnum.OTHER);

		initEEnum(bindingStateEnumEEnum, BindingStateEnum.class, "BindingStateEnum");
		addEEnumLiteral(bindingStateEnumEEnum, BindingStateEnum.ACTIVE);
		addEEnumLiteral(bindingStateEnumEEnum, BindingStateEnum.DISACTIVATED);
		addEEnumLiteral(bindingStateEnumEEnum, BindingStateEnum.DISABLED);

		initEEnum(enactmentEnumEEnum, EnactmentEnum.class, "EnactmentEnum");
		addEEnumLiteral(enactmentEnumEEnum, EnactmentEnum.OPTIONAL);
		addEEnumLiteral(enactmentEnumEEnum, EnactmentEnum.MANDATORY);

		initEEnum(artificialElementTypeEnumEEnum, ArtificialElementTypeEnum.class, "ArtificialElementTypeEnum");
		addEEnumLiteral(artificialElementTypeEnumEEnum, ArtificialElementTypeEnum.FAKEDATA);
		addEEnumLiteral(artificialElementTypeEnumEEnum, ArtificialElementTypeEnum.FAKECODE);
		addEEnumLiteral(artificialElementTypeEnumEEnum, ArtificialElementTypeEnum.FAKESERVICE);
		addEEnumLiteral(artificialElementTypeEnumEEnum, ArtificialElementTypeEnum.FAKEHARDWARE);
		addEEnumLiteral(artificialElementTypeEnumEEnum, ArtificialElementTypeEnum.FAKEFACILITY);
		addEEnumLiteral(artificialElementTypeEnumEEnum, ArtificialElementTypeEnum.FAKEEVENT);
		addEEnumLiteral(artificialElementTypeEnumEEnum, ArtificialElementTypeEnum.FAKEDELAY);
		addEEnumLiteral(artificialElementTypeEnumEEnum, ArtificialElementTypeEnum.FAKEOTHER);

		initEEnum(obstacleCategoryEnumEEnum, ObstacleCategoryEnum.class, "ObstacleCategoryEnum");
		addEEnumLiteral(obstacleCategoryEnumEEnum, ObstacleCategoryEnum.INFOUNAVAILABLE);
		addEEnumLiteral(obstacleCategoryEnumEEnum, ObstacleCategoryEnum.INFONOTINTIME);
		addEEnumLiteral(obstacleCategoryEnumEEnum, ObstacleCategoryEnum.WRONGBELIEF);
		addEEnumLiteral(obstacleCategoryEnumEEnum, ObstacleCategoryEnum.STIMULUSRESPONSEPROBLEM);

		initEEnum(preferenceWeightEnumEEnum, PreferenceWeightEnum.class, "PreferenceWeightEnum");
		addEEnumLiteral(preferenceWeightEnumEEnum, PreferenceWeightEnum.LOW);
		addEEnumLiteral(preferenceWeightEnumEEnum, PreferenceWeightEnum.NEUTRAL);
		addEEnumLiteral(preferenceWeightEnumEEnum, PreferenceWeightEnum.HIGH);

		initEEnum(influenceWeightEnumEEnum, InfluenceWeightEnum.class, "InfluenceWeightEnum");
		addEEnumLiteral(influenceWeightEnumEEnum, InfluenceWeightEnum.WEAK);
		addEEnumLiteral(influenceWeightEnumEEnum, InfluenceWeightEnum.NEUTRAL);
		addEEnumLiteral(influenceWeightEnumEEnum, InfluenceWeightEnum.STRONG);

		initEEnum(influencePolarityEnumEEnum, InfluencePolarityEnum.class, "InfluencePolarityEnum");
		addEEnumLiteral(influencePolarityEnumEEnum, InfluencePolarityEnum.POSITIVE);
		addEEnumLiteral(influencePolarityEnumEEnum, InfluencePolarityEnum.NEGATIVE);

		initEEnum(deceptionGoalTypeEnumEEnum, DeceptionGoalTypeEnum.class, "DeceptionGoalTypeEnum");
		addEEnumLiteral(deceptionGoalTypeEnumEEnum, DeceptionGoalTypeEnum.UNNOTICED);
		addEEnumLiteral(deceptionGoalTypeEnumEEnum, DeceptionGoalTypeEnum.BENIGN);
		addEEnumLiteral(deceptionGoalTypeEnumEEnum, DeceptionGoalTypeEnum.DESIRABLE);
		addEEnumLiteral(deceptionGoalTypeEnumEEnum, DeceptionGoalTypeEnum.UNAPPEALING);
		addEEnumLiteral(deceptionGoalTypeEnumEEnum, DeceptionGoalTypeEnum.DANGEROUS);
		addEEnumLiteral(deceptionGoalTypeEnumEEnum, DeceptionGoalTypeEnum.UNDEFINED);

		initEEnum(softGoalTypeEnumEEnum, SoftGoalTypeEnum.class, "SoftGoalTypeEnum");
		addEEnumLiteral(softGoalTypeEnumEEnum, SoftGoalTypeEnum.INCREASE);
		addEEnumLiteral(softGoalTypeEnumEEnum, SoftGoalTypeEnum.DECREASE);
		addEEnumLiteral(softGoalTypeEnumEEnum, SoftGoalTypeEnum.IMPROVE);
		addEEnumLiteral(softGoalTypeEnumEEnum, SoftGoalTypeEnum.MITIGATE);
		addEEnumLiteral(softGoalTypeEnumEEnum, SoftGoalTypeEnum.MAXIMIZE);
		addEEnumLiteral(softGoalTypeEnumEEnum, SoftGoalTypeEnum.MINIMIZE);
		addEEnumLiteral(softGoalTypeEnumEEnum, SoftGoalTypeEnum.REDUCE);
		addEEnumLiteral(softGoalTypeEnumEEnum, SoftGoalTypeEnum.OTHER);

		initEEnum(nodesSelectionTypeEEnum, NodesSelectionType.class, "NodesSelectionType");
		addEEnumLiteral(nodesSelectionTypeEEnum, NodesSelectionType.OR);
		addEEnumLiteral(nodesSelectionTypeEEnum, NodesSelectionType.XOR);

		initEEnum(channelTypeEnumEEnum, ChannelTypeEnum.class, "ChannelTypeEnum");
		addEEnumLiteral(channelTypeEnumEEnum, ChannelTypeEnum.PERMANENT);
		addEEnumLiteral(channelTypeEnumEEnum, ChannelTypeEnum.VOLATILE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.obeo.fr/dsl/dnc/archetype
		createArchetypeAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.obeo.fr/dsl/dnc/archetype</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createArchetypeAnnotations() {
		String source = "http://www.obeo.fr/dsl/dnc/archetype";
		addAnnotation
		  (orGoalRefinementEClass,
		   source,
		   new String[] {
			   "archetype", "Role"
		   });
		addAnnotation
		  (nodeEClass,
		   source,
		   new String[] {
			   "archetype", "Role"
		   });
	}

} //DmtPackageImpl
