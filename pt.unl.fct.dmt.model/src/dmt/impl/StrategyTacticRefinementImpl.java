/**
 */
package dmt.impl;

import dmt.DeceptionStrategy;
import dmt.DeceptionTacticMechanism;
import dmt.DmtPackage;
import dmt.StrategyTacticRefinement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Strategy Tactic Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.StrategyTacticRefinementImpl#getStrategytacticrefinementtarget <em>Strategytacticrefinementtarget</em>}</li>
 *   <li>{@link dmt.impl.StrategyTacticRefinementImpl#getStrategytacticrefinementsource <em>Strategytacticrefinementsource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StrategyTacticRefinementImpl extends NodeRefinementImpl implements StrategyTacticRefinement {
	/**
	 * The cached value of the '{@link #getStrategytacticrefinementtarget() <em>Strategytacticrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategytacticrefinementtarget()
	 * @generated
	 * @ordered
	 */
	protected DeceptionTacticMechanism strategytacticrefinementtarget;

	/**
	 * The cached value of the '{@link #getStrategytacticrefinementsource() <em>Strategytacticrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategytacticrefinementsource()
	 * @generated
	 * @ordered
	 */
	protected DeceptionStrategy strategytacticrefinementsource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StrategyTacticRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.STRATEGY_TACTIC_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism getStrategytacticrefinementtarget() {
		if (strategytacticrefinementtarget != null && strategytacticrefinementtarget.eIsProxy()) {
			InternalEObject oldStrategytacticrefinementtarget = (InternalEObject)strategytacticrefinementtarget;
			strategytacticrefinementtarget = (DeceptionTacticMechanism)eResolveProxy(oldStrategytacticrefinementtarget);
			if (strategytacticrefinementtarget != oldStrategytacticrefinementtarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET, oldStrategytacticrefinementtarget, strategytacticrefinementtarget));
			}
		}
		return strategytacticrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism basicGetStrategytacticrefinementtarget() {
		return strategytacticrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrategytacticrefinementtarget(DeceptionTacticMechanism newStrategytacticrefinementtarget) {
		DeceptionTacticMechanism oldStrategytacticrefinementtarget = strategytacticrefinementtarget;
		strategytacticrefinementtarget = newStrategytacticrefinementtarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET, oldStrategytacticrefinementtarget, strategytacticrefinementtarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategy getStrategytacticrefinementsource() {
		if (strategytacticrefinementsource != null && strategytacticrefinementsource.eIsProxy()) {
			InternalEObject oldStrategytacticrefinementsource = (InternalEObject)strategytacticrefinementsource;
			strategytacticrefinementsource = (DeceptionStrategy)eResolveProxy(oldStrategytacticrefinementsource);
			if (strategytacticrefinementsource != oldStrategytacticrefinementsource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE, oldStrategytacticrefinementsource, strategytacticrefinementsource));
			}
		}
		return strategytacticrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategy basicGetStrategytacticrefinementsource() {
		return strategytacticrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrategytacticrefinementsource(DeceptionStrategy newStrategytacticrefinementsource) {
		DeceptionStrategy oldStrategytacticrefinementsource = strategytacticrefinementsource;
		strategytacticrefinementsource = newStrategytacticrefinementsource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE, oldStrategytacticrefinementsource, strategytacticrefinementsource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET:
				if (resolve) return getStrategytacticrefinementtarget();
				return basicGetStrategytacticrefinementtarget();
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE:
				if (resolve) return getStrategytacticrefinementsource();
				return basicGetStrategytacticrefinementsource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET:
				setStrategytacticrefinementtarget((DeceptionTacticMechanism)newValue);
				return;
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE:
				setStrategytacticrefinementsource((DeceptionStrategy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET:
				setStrategytacticrefinementtarget((DeceptionTacticMechanism)null);
				return;
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE:
				setStrategytacticrefinementsource((DeceptionStrategy)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET:
				return strategytacticrefinementtarget != null;
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE:
				return strategytacticrefinementsource != null;
		}
		return super.eIsSet(featureID);
	}

} //StrategyTacticRefinementImpl
