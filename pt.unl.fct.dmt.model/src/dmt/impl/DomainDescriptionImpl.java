/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.ANDGoalRefinement;
import dmt.Annotation;
import dmt.DmtPackage;
import dmt.DomainDescription;
import dmt.DomainPropertyCategoryEnum;
import dmt.GoalNode;
import dmt.GoalRefinable;
import dmt.Node;
import dmt.ORGoalRefinement;
import dmt.Vulnerability;
import dmt.Vulnerable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Domain Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DomainDescriptionImpl#getVulnerability <em>Vulnerability</em>}</li>
 *   <li>{@link dmt.impl.DomainDescriptionImpl#getName <em>Name</em>}</li>
 *   <li>{@link dmt.impl.DomainDescriptionImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link dmt.impl.DomainDescriptionImpl#getUUID <em>UUID</em>}</li>
 *   <li>{@link dmt.impl.DomainDescriptionImpl#getHasAnnotations <em>Has Annotations</em>}</li>
 *   <li>{@link dmt.impl.DomainDescriptionImpl#getOrGoalRefined <em>Or Goal Refined</em>}</li>
 *   <li>{@link dmt.impl.DomainDescriptionImpl#getAndGoalRefined <em>And Goal Refined</em>}</li>
 *   <li>{@link dmt.impl.DomainDescriptionImpl#getDomainCategory <em>Domain Category</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DomainDescriptionImpl extends ObstructedElementImpl implements DomainDescription {
	/**
	 * The cached value of the '{@link #getVulnerability() <em>Vulnerability</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVulnerability()
	 * @generated
	 * @ordered
	 */
	protected EList<Vulnerability> vulnerability;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getUUID() <em>UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUUID()
	 * @generated
	 * @ordered
	 */
	protected static final String UUID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUUID() <em>UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUUID()
	 * @generated
	 * @ordered
	 */
	protected String uuid = UUID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHasAnnotations() <em>Has Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> hasAnnotations;

	/**
	 * The cached value of the '{@link #getOrGoalRefined() <em>Or Goal Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrGoalRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ORGoalRefinement> orGoalRefined;

	/**
	 * The cached value of the '{@link #getAndGoalRefined() <em>And Goal Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAndGoalRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ANDGoalRefinement> andGoalRefined;

	/**
	 * The default value of the '{@link #getDomainCategory() <em>Domain Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainCategory()
	 * @generated
	 * @ordered
	 */
	protected static final DomainPropertyCategoryEnum DOMAIN_CATEGORY_EDEFAULT = DomainPropertyCategoryEnum.DOMAINVAR;

	/**
	 * The cached value of the '{@link #getDomainCategory() <em>Domain Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainCategory()
	 * @generated
	 * @ordered
	 */
	protected DomainPropertyCategoryEnum domainCategory = DOMAIN_CATEGORY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DomainDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DOMAIN_DESCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Vulnerability> getVulnerability() {
		if (vulnerability == null) {
			vulnerability = new EObjectResolvingEList<Vulnerability>(Vulnerability.class, this, DmtPackage.DOMAIN_DESCRIPTION__VULNERABILITY);
		}
		return vulnerability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DOMAIN_DESCRIPTION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DOMAIN_DESCRIPTION__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUUID() {
		return uuid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUUID(String newUUID) {
		String oldUUID = uuid;
		uuid = newUUID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DOMAIN_DESCRIPTION__UUID, oldUUID, uuid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getHasAnnotations() {
		if (hasAnnotations == null) {
			hasAnnotations = new EObjectResolvingEList<Annotation>(Annotation.class, this, DmtPackage.DOMAIN_DESCRIPTION__HAS_ANNOTATIONS);
		}
		return hasAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ORGoalRefinement> getOrGoalRefined() {
		if (orGoalRefined == null) {
			orGoalRefined = new EObjectResolvingEList<ORGoalRefinement>(ORGoalRefinement.class, this, DmtPackage.DOMAIN_DESCRIPTION__OR_GOAL_REFINED);
		}
		return orGoalRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ANDGoalRefinement> getAndGoalRefined() {
		if (andGoalRefined == null) {
			andGoalRefined = new EObjectResolvingEList<ANDGoalRefinement>(ANDGoalRefinement.class, this, DmtPackage.DOMAIN_DESCRIPTION__AND_GOAL_REFINED);
		}
		return andGoalRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainPropertyCategoryEnum getDomainCategory() {
		return domainCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomainCategory(DomainPropertyCategoryEnum newDomainCategory) {
		DomainPropertyCategoryEnum oldDomainCategory = domainCategory;
		domainCategory = newDomainCategory == null ? DOMAIN_CATEGORY_EDEFAULT : newDomainCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DOMAIN_DESCRIPTION__DOMAIN_CATEGORY, oldDomainCategory, domainCategory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DOMAIN_DESCRIPTION__VULNERABILITY:
				return getVulnerability();
			case DmtPackage.DOMAIN_DESCRIPTION__NAME:
				return getName();
			case DmtPackage.DOMAIN_DESCRIPTION__DESCRIPTION:
				return getDescription();
			case DmtPackage.DOMAIN_DESCRIPTION__UUID:
				return getUUID();
			case DmtPackage.DOMAIN_DESCRIPTION__HAS_ANNOTATIONS:
				return getHasAnnotations();
			case DmtPackage.DOMAIN_DESCRIPTION__OR_GOAL_REFINED:
				return getOrGoalRefined();
			case DmtPackage.DOMAIN_DESCRIPTION__AND_GOAL_REFINED:
				return getAndGoalRefined();
			case DmtPackage.DOMAIN_DESCRIPTION__DOMAIN_CATEGORY:
				return getDomainCategory();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DOMAIN_DESCRIPTION__VULNERABILITY:
				getVulnerability().clear();
				getVulnerability().addAll((Collection<? extends Vulnerability>)newValue);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__NAME:
				setName((String)newValue);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__UUID:
				setUUID((String)newValue);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__HAS_ANNOTATIONS:
				getHasAnnotations().clear();
				getHasAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__OR_GOAL_REFINED:
				getOrGoalRefined().clear();
				getOrGoalRefined().addAll((Collection<? extends ORGoalRefinement>)newValue);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__AND_GOAL_REFINED:
				getAndGoalRefined().clear();
				getAndGoalRefined().addAll((Collection<? extends ANDGoalRefinement>)newValue);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__DOMAIN_CATEGORY:
				setDomainCategory((DomainPropertyCategoryEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DOMAIN_DESCRIPTION__VULNERABILITY:
				getVulnerability().clear();
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__UUID:
				setUUID(UUID_EDEFAULT);
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__HAS_ANNOTATIONS:
				getHasAnnotations().clear();
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__OR_GOAL_REFINED:
				getOrGoalRefined().clear();
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__AND_GOAL_REFINED:
				getAndGoalRefined().clear();
				return;
			case DmtPackage.DOMAIN_DESCRIPTION__DOMAIN_CATEGORY:
				setDomainCategory(DOMAIN_CATEGORY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DOMAIN_DESCRIPTION__VULNERABILITY:
				return vulnerability != null && !vulnerability.isEmpty();
			case DmtPackage.DOMAIN_DESCRIPTION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DmtPackage.DOMAIN_DESCRIPTION__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case DmtPackage.DOMAIN_DESCRIPTION__UUID:
				return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
			case DmtPackage.DOMAIN_DESCRIPTION__HAS_ANNOTATIONS:
				return hasAnnotations != null && !hasAnnotations.isEmpty();
			case DmtPackage.DOMAIN_DESCRIPTION__OR_GOAL_REFINED:
				return orGoalRefined != null && !orGoalRefined.isEmpty();
			case DmtPackage.DOMAIN_DESCRIPTION__AND_GOAL_REFINED:
				return andGoalRefined != null && !andGoalRefined.isEmpty();
			case DmtPackage.DOMAIN_DESCRIPTION__DOMAIN_CATEGORY:
				return domainCategory != DOMAIN_CATEGORY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Vulnerable.class) {
			switch (derivedFeatureID) {
				case DmtPackage.DOMAIN_DESCRIPTION__VULNERABILITY: return DmtPackage.VULNERABLE__VULNERABILITY;
				default: return -1;
			}
		}
		if (baseClass == Node.class) {
			switch (derivedFeatureID) {
				case DmtPackage.DOMAIN_DESCRIPTION__NAME: return DmtPackage.NODE__NAME;
				case DmtPackage.DOMAIN_DESCRIPTION__DESCRIPTION: return DmtPackage.NODE__DESCRIPTION;
				case DmtPackage.DOMAIN_DESCRIPTION__UUID: return DmtPackage.NODE__UUID;
				case DmtPackage.DOMAIN_DESCRIPTION__HAS_ANNOTATIONS: return DmtPackage.NODE__HAS_ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == GoalNode.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == GoalRefinable.class) {
			switch (derivedFeatureID) {
				case DmtPackage.DOMAIN_DESCRIPTION__OR_GOAL_REFINED: return DmtPackage.GOAL_REFINABLE__OR_GOAL_REFINED;
				case DmtPackage.DOMAIN_DESCRIPTION__AND_GOAL_REFINED: return DmtPackage.GOAL_REFINABLE__AND_GOAL_REFINED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Vulnerable.class) {
			switch (baseFeatureID) {
				case DmtPackage.VULNERABLE__VULNERABILITY: return DmtPackage.DOMAIN_DESCRIPTION__VULNERABILITY;
				default: return -1;
			}
		}
		if (baseClass == Node.class) {
			switch (baseFeatureID) {
				case DmtPackage.NODE__NAME: return DmtPackage.DOMAIN_DESCRIPTION__NAME;
				case DmtPackage.NODE__DESCRIPTION: return DmtPackage.DOMAIN_DESCRIPTION__DESCRIPTION;
				case DmtPackage.NODE__UUID: return DmtPackage.DOMAIN_DESCRIPTION__UUID;
				case DmtPackage.NODE__HAS_ANNOTATIONS: return DmtPackage.DOMAIN_DESCRIPTION__HAS_ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == GoalNode.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == GoalRefinable.class) {
			switch (baseFeatureID) {
				case DmtPackage.GOAL_REFINABLE__OR_GOAL_REFINED: return DmtPackage.DOMAIN_DESCRIPTION__OR_GOAL_REFINED;
				case DmtPackage.GOAL_REFINABLE__AND_GOAL_REFINED: return DmtPackage.DOMAIN_DESCRIPTION__AND_GOAL_REFINED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", UUID: ");
		result.append(uuid);
		result.append(", domainCategory: ");
		result.append(domainCategory);
		result.append(')');
		return result.toString();
	}

} //DomainDescriptionImpl
