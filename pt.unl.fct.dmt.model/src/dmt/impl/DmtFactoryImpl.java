/**
 */
package dmt.impl;

import dmt.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DmtFactoryImpl extends EFactoryImpl implements DmtFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DmtFactory init() {
		try {
			DmtFactory theDmtFactory = (DmtFactory)EPackage.Registry.INSTANCE.getEFactory(DmtPackage.eNS_URI);
			if (theDmtFactory != null) {
				return theDmtFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DmtFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DmtFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DmtPackage.DMT_MODEL: return createDMTModel();
			case DmtPackage.OPERATIONALIZATION: return createOperationalization();
			case DmtPackage.SOFTWARE_AGENT: return createSoftwareAgent();
			case DmtPackage.ENVIRONMENT_AGENT: return createEnvironmentAgent();
			case DmtPackage.BEHAVIOUR_GOAL: return createBehaviourGoal();
			case DmtPackage.DOMAIN_DESCRIPTION: return createDomainDescription();
			case DmtPackage.EXPECTATION: return createExpectation();
			case DmtPackage.REQUIREMENT: return createRequirement();
			case DmtPackage.OPERATION: return createOperation();
			case DmtPackage.OR_GOAL_REFINEMENT: return createORGoalRefinement();
			case DmtPackage.ATTACK: return createAttack();
			case DmtPackage.THREAT: return createThreat();
			case DmtPackage.VULNERABILITY: return createVulnerability();
			case DmtPackage.SECURITY_CONTROL: return createSecurityControl();
			case DmtPackage.RISK_COMPONENT: return createRiskComponent();
			case DmtPackage.ATTACKER: return createAttacker();
			case DmtPackage.DECEPTION_TACTIC: return createDeceptionTactic();
			case DmtPackage.IVULNERABILITY: return createIVulnerability();
			case DmtPackage.DECEPTION_STORY: return createDeceptionStory();
			case DmtPackage.DECEPTION_BIAS: return createDeceptionBias();
			case DmtPackage.ARTIFICIAL_ELEMENT: return createArtificialElement();
			case DmtPackage.MITIGATION: return createMitigation();
			case DmtPackage.INTERACTION_OBJECT: return createInteractionObject();
			case DmtPackage.LINK: return createLink();
			case DmtPackage.SIMULATION: return createSimulation();
			case DmtPackage.DISSIMULATION: return createDissimulation();
			case DmtPackage.AGENT_ASSIGNMENT: return createAgentAssignment();
			case DmtPackage.AND_GOAL_REFINEMENT: return createANDGoalRefinement();
			case DmtPackage.CONFLICT_GOAL_REFINEMENT: return createConflictGoalRefinement();
			case DmtPackage.AGENT_MONITORING: return createAgentMonitoring();
			case DmtPackage.AGENT_CONTROL: return createAgentControl();
			case DmtPackage.DECEPTION_METRIC: return createDeceptionMetric();
			case DmtPackage.DECEPTION_CHANNEL: return createDeceptionChannel();
			case DmtPackage.ATTACK_VECTOR: return createAttackVector();
			case DmtPackage.APROPERTY: return createAProperty();
			case DmtPackage.ATTACK_STEP_CONNECTOR: return createAttackStepConnector();
			case DmtPackage.INITIAL_ATTACK_STEP: return createInitialAttackStep();
			case DmtPackage.END_ATTACK_STEP: return createEndAttackStep();
			case DmtPackage.ATTACK_TREE: return createAttackTree();
			case DmtPackage.SOFT_GOAL: return createSoftGoal();
			case DmtPackage.DECEPTION_BEHAVIOUR_GOAL: return createDeceptionBehaviourGoal();
			case DmtPackage.DECEPTION_RISK: return createDeceptionRisk();
			case DmtPackage.DECEPTION_STRATEGY: return createDeceptionStrategy();
			case DmtPackage.DECEPTION_TACTIC_MECHANISM: return createDeceptionTacticMechanism();
			case DmtPackage.FEATURE: return createFeature();
			case DmtPackage.REQUIRED_CONSTRAINT: return createRequiredConstraint();
			case DmtPackage.EXCLUDE_CONSTRAINT: return createExcludeConstraint();
			case DmtPackage.BENEFIT_CONSTRAINT: return createBenefitConstraint();
			case DmtPackage.AVOID_CONSTRAINT: return createAvoidConstraint();
			case DmtPackage.TACTIC_REFINEMENT: return createTacticRefinement();
			case DmtPackage.FEATURE_REFINEMENT: return createFeatureRefinement();
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT: return createStrategyTacticRefinement();
			case DmtPackage.TACTIC_FEATURE_REFINEMENT: return createTacticFeatureRefinement();
			case DmtPackage.OR_OBSTRUCTION_REFINEMENT: return createORObstructionRefinement();
			case DmtPackage.AND_OBSTRUCTION_REFINEMENT: return createANDObstructionRefinement();
			case DmtPackage.OBSTACLE: return createObstacle();
			case DmtPackage.INFLUENCE_NODE: return createInfluenceNode();
			case DmtPackage.QUALITY_ATTRIBUTE: return createQualityAttribute();
			case DmtPackage.INFLUENCE_RELATIONSHIP: return createInfluenceRelationship();
			case DmtPackage.INFLUENCE: return createInfluence();
			case DmtPackage.PREFERENCE: return createPreference();
			case DmtPackage.EVENT: return createEvent();
			case DmtPackage.DECEPTION_TACTIC_EVENT: return createDeceptionTacticEvent();
			case DmtPackage.ENVIRONMENT_EVENT: return createEnvironmentEvent();
			case DmtPackage.BELIEF: return createBelief();
			case DmtPackage.DECEPTION_SOFT_GOAL: return createDeceptionSoftGoal();
			case DmtPackage.CONCRETE_ASSET: return createConcreteAsset();
			case DmtPackage.GOAL_CONTAINER: return createGoalContainer();
			case DmtPackage.NODE_SELECTION: return createNodeSelection();
			case DmtPackage.DECEPTION_TACTIC_STRATEGY: return createDeceptionTacticStrategy();
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT: return createDeceptionStrategyTacticRefinement();
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT: return createDeceptionTacticStrategyRefinement();
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT: return createDeceptionStrategyRefinement();
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT: return createSimulationHasArtificialElement();
			case DmtPackage.PROPERTY: return createProperty();
			case DmtPackage.ANNOTATION: return createAnnotation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case DmtPackage.GOAL_CATEGORY_ENUM:
				return createGoalCategoryEnumFromString(eDataType, initialValue);
			case DmtPackage.DOMAIN_PROPERTY_CATEGORY_ENUM:
				return createDomainPropertyCategoryEnumFromString(eDataType, initialValue);
			case DmtPackage.GOAL_PRIORITY_ENUM:
				return createGoalPriorityEnumFromString(eDataType, initialValue);
			case DmtPackage.MITIGATION_LEVEL_ENUM:
				return createMitigationLevelEnumFromString(eDataType, initialValue);
			case DmtPackage.ASSET_TYPE_ENUM:
				return createAssetTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.ATTACK_TYPE_ENUM:
				return createAttackTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.ATTACKER_PROFILE_ENUM:
				return createAttackerProfileEnumFromString(eDataType, initialValue);
			case DmtPackage.RISK_LIKELIHOOD_ENUM:
				return createRiskLikelihoodEnumFromString(eDataType, initialValue);
			case DmtPackage.RISK_SEVERITY_ENUM:
				return createRiskSeverityEnumFromString(eDataType, initialValue);
			case DmtPackage.SECURITY_CONTROL_INCIDENT_ENUM:
				return createSecurityControlIncidentEnumFromString(eDataType, initialValue);
			case DmtPackage.THREAT_CATEGORY_ENUM:
				return createThreatCategoryEnumFromString(eDataType, initialValue);
			case DmtPackage.VULNERABILITY_TYPE_ENUM:
				return createVulnerabilityTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.SECURITY_CONTROL_NATURE_ENUM:
				return createSecurityControlNatureEnumFromString(eDataType, initialValue);
			case DmtPackage.BIAS_TYPE_ENUM:
				return createBiasTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.SIMULATION_TECHNIQUE_TYPE_ENUM:
				return createSimulationTechniqueTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.METRIC_TYPE_ENUM:
				return createMetricTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.DISSIMULATION_TECHNIQUE_TYPE_ENUM:
				return createDissimulationTechniqueTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.GOAL_TYPE_ENUM:
				return createGoalTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.GOAL_STABILITY_ENUM:
				return createGoalStabilityEnumFromString(eDataType, initialValue);
			case DmtPackage.ENVIRONMENT_AGENT_TYPE_ENUM:
				return createEnvironmentAgentTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.ATTACK_ORIGIN_ENUM:
				return createAttackOriginEnumFromString(eDataType, initialValue);
			case DmtPackage.ATTACK_CONNECTOR_ENUM:
				return createAttackConnectorEnumFromString(eDataType, initialValue);
			case DmtPackage.STORY_TYPE_ENUM:
				return createStoryTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.DECEPTION_RISK_TYPE_ENUM:
				return createDeceptionRiskTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.BINDING_STATE_ENUM:
				return createBindingStateEnumFromString(eDataType, initialValue);
			case DmtPackage.ENACTMENT_ENUM:
				return createEnactmentEnumFromString(eDataType, initialValue);
			case DmtPackage.ARTIFICIAL_ELEMENT_TYPE_ENUM:
				return createArtificialElementTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.OBSTACLE_CATEGORY_ENUM:
				return createObstacleCategoryEnumFromString(eDataType, initialValue);
			case DmtPackage.PREFERENCE_WEIGHT_ENUM:
				return createPreferenceWeightEnumFromString(eDataType, initialValue);
			case DmtPackage.INFLUENCE_WEIGHT_ENUM:
				return createInfluenceWeightEnumFromString(eDataType, initialValue);
			case DmtPackage.INFLUENCE_POLARITY_ENUM:
				return createInfluencePolarityEnumFromString(eDataType, initialValue);
			case DmtPackage.DECEPTION_GOAL_TYPE_ENUM:
				return createDeceptionGoalTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.SOFT_GOAL_TYPE_ENUM:
				return createSoftGoalTypeEnumFromString(eDataType, initialValue);
			case DmtPackage.NODES_SELECTION_TYPE:
				return createNodesSelectionTypeFromString(eDataType, initialValue);
			case DmtPackage.CHANNEL_TYPE_ENUM:
				return createChannelTypeEnumFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case DmtPackage.GOAL_CATEGORY_ENUM:
				return convertGoalCategoryEnumToString(eDataType, instanceValue);
			case DmtPackage.DOMAIN_PROPERTY_CATEGORY_ENUM:
				return convertDomainPropertyCategoryEnumToString(eDataType, instanceValue);
			case DmtPackage.GOAL_PRIORITY_ENUM:
				return convertGoalPriorityEnumToString(eDataType, instanceValue);
			case DmtPackage.MITIGATION_LEVEL_ENUM:
				return convertMitigationLevelEnumToString(eDataType, instanceValue);
			case DmtPackage.ASSET_TYPE_ENUM:
				return convertAssetTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.ATTACK_TYPE_ENUM:
				return convertAttackTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.ATTACKER_PROFILE_ENUM:
				return convertAttackerProfileEnumToString(eDataType, instanceValue);
			case DmtPackage.RISK_LIKELIHOOD_ENUM:
				return convertRiskLikelihoodEnumToString(eDataType, instanceValue);
			case DmtPackage.RISK_SEVERITY_ENUM:
				return convertRiskSeverityEnumToString(eDataType, instanceValue);
			case DmtPackage.SECURITY_CONTROL_INCIDENT_ENUM:
				return convertSecurityControlIncidentEnumToString(eDataType, instanceValue);
			case DmtPackage.THREAT_CATEGORY_ENUM:
				return convertThreatCategoryEnumToString(eDataType, instanceValue);
			case DmtPackage.VULNERABILITY_TYPE_ENUM:
				return convertVulnerabilityTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.SECURITY_CONTROL_NATURE_ENUM:
				return convertSecurityControlNatureEnumToString(eDataType, instanceValue);
			case DmtPackage.BIAS_TYPE_ENUM:
				return convertBiasTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.SIMULATION_TECHNIQUE_TYPE_ENUM:
				return convertSimulationTechniqueTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.METRIC_TYPE_ENUM:
				return convertMetricTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.DISSIMULATION_TECHNIQUE_TYPE_ENUM:
				return convertDissimulationTechniqueTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.GOAL_TYPE_ENUM:
				return convertGoalTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.GOAL_STABILITY_ENUM:
				return convertGoalStabilityEnumToString(eDataType, instanceValue);
			case DmtPackage.ENVIRONMENT_AGENT_TYPE_ENUM:
				return convertEnvironmentAgentTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.ATTACK_ORIGIN_ENUM:
				return convertAttackOriginEnumToString(eDataType, instanceValue);
			case DmtPackage.ATTACK_CONNECTOR_ENUM:
				return convertAttackConnectorEnumToString(eDataType, instanceValue);
			case DmtPackage.STORY_TYPE_ENUM:
				return convertStoryTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.DECEPTION_RISK_TYPE_ENUM:
				return convertDeceptionRiskTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.BINDING_STATE_ENUM:
				return convertBindingStateEnumToString(eDataType, instanceValue);
			case DmtPackage.ENACTMENT_ENUM:
				return convertEnactmentEnumToString(eDataType, instanceValue);
			case DmtPackage.ARTIFICIAL_ELEMENT_TYPE_ENUM:
				return convertArtificialElementTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.OBSTACLE_CATEGORY_ENUM:
				return convertObstacleCategoryEnumToString(eDataType, instanceValue);
			case DmtPackage.PREFERENCE_WEIGHT_ENUM:
				return convertPreferenceWeightEnumToString(eDataType, instanceValue);
			case DmtPackage.INFLUENCE_WEIGHT_ENUM:
				return convertInfluenceWeightEnumToString(eDataType, instanceValue);
			case DmtPackage.INFLUENCE_POLARITY_ENUM:
				return convertInfluencePolarityEnumToString(eDataType, instanceValue);
			case DmtPackage.DECEPTION_GOAL_TYPE_ENUM:
				return convertDeceptionGoalTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.SOFT_GOAL_TYPE_ENUM:
				return convertSoftGoalTypeEnumToString(eDataType, instanceValue);
			case DmtPackage.NODES_SELECTION_TYPE:
				return convertNodesSelectionTypeToString(eDataType, instanceValue);
			case DmtPackage.CHANNEL_TYPE_ENUM:
				return convertChannelTypeEnumToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DMTModel createDMTModel() {
		DMTModelImpl dmtModel = new DMTModelImpl();
		return dmtModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operationalization createOperationalization() {
		OperationalizationImpl operationalization = new OperationalizationImpl();
		return operationalization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareAgent createSoftwareAgent() {
		SoftwareAgentImpl softwareAgent = new SoftwareAgentImpl();
		return softwareAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentAgent createEnvironmentAgent() {
		EnvironmentAgentImpl environmentAgent = new EnvironmentAgentImpl();
		return environmentAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BehaviourGoal createBehaviourGoal() {
		BehaviourGoalImpl behaviourGoal = new BehaviourGoalImpl();
		return behaviourGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainDescription createDomainDescription() {
		DomainDescriptionImpl domainDescription = new DomainDescriptionImpl();
		return domainDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Expectation createExpectation() {
		ExpectationImpl expectation = new ExpectationImpl();
		return expectation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Requirement createRequirement() {
		RequirementImpl requirement = new RequirementImpl();
		return requirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation createOperation() {
		OperationImpl operation = new OperationImpl();
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ORGoalRefinement createORGoalRefinement() {
		ORGoalRefinementImpl orGoalRefinement = new ORGoalRefinementImpl();
		return orGoalRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attack createAttack() {
		AttackImpl attack = new AttackImpl();
		return attack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Threat createThreat() {
		ThreatImpl threat = new ThreatImpl();
		return threat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vulnerability createVulnerability() {
		VulnerabilityImpl vulnerability = new VulnerabilityImpl();
		return vulnerability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityControl createSecurityControl() {
		SecurityControlImpl securityControl = new SecurityControlImpl();
		return securityControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskComponent createRiskComponent() {
		RiskComponentImpl riskComponent = new RiskComponentImpl();
		return riskComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attacker createAttacker() {
		AttackerImpl attacker = new AttackerImpl();
		return attacker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTactic createDeceptionTactic() {
		DeceptionTacticImpl deceptionTactic = new DeceptionTacticImpl();
		return deceptionTactic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IVulnerability createIVulnerability() {
		IVulnerabilityImpl iVulnerability = new IVulnerabilityImpl();
		return iVulnerability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStory createDeceptionStory() {
		DeceptionStoryImpl deceptionStory = new DeceptionStoryImpl();
		return deceptionStory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionBias createDeceptionBias() {
		DeceptionBiasImpl deceptionBias = new DeceptionBiasImpl();
		return deceptionBias;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArtificialElement createArtificialElement() {
		ArtificialElementImpl artificialElement = new ArtificialElementImpl();
		return artificialElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionObject createInteractionObject() {
		InteractionObjectImpl interactionObject = new InteractionObjectImpl();
		return interactionObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Link createLink() {
		LinkImpl link = new LinkImpl();
		return link;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mitigation createMitigation() {
		MitigationImpl mitigation = new MitigationImpl();
		return mitigation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation createSimulation() {
		SimulationImpl simulation = new SimulationImpl();
		return simulation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dissimulation createDissimulation() {
		DissimulationImpl dissimulation = new DissimulationImpl();
		return dissimulation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentAssignment createAgentAssignment() {
		AgentAssignmentImpl agentAssignment = new AgentAssignmentImpl();
		return agentAssignment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ANDGoalRefinement createANDGoalRefinement() {
		ANDGoalRefinementImpl andGoalRefinement = new ANDGoalRefinementImpl();
		return andGoalRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConflictGoalRefinement createConflictGoalRefinement() {
		ConflictGoalRefinementImpl conflictGoalRefinement = new ConflictGoalRefinementImpl();
		return conflictGoalRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentMonitoring createAgentMonitoring() {
		AgentMonitoringImpl agentMonitoring = new AgentMonitoringImpl();
		return agentMonitoring;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentControl createAgentControl() {
		AgentControlImpl agentControl = new AgentControlImpl();
		return agentControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionMetric createDeceptionMetric() {
		DeceptionMetricImpl deceptionMetric = new DeceptionMetricImpl();
		return deceptionMetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionChannel createDeceptionChannel() {
		DeceptionChannelImpl deceptionChannel = new DeceptionChannelImpl();
		return deceptionChannel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackVector createAttackVector() {
		AttackVectorImpl attackVector = new AttackVectorImpl();
		return attackVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AProperty createAProperty() {
		APropertyImpl aProperty = new APropertyImpl();
		return aProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackStepConnector createAttackStepConnector() {
		AttackStepConnectorImpl attackStepConnector = new AttackStepConnectorImpl();
		return attackStepConnector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialAttackStep createInitialAttackStep() {
		InitialAttackStepImpl initialAttackStep = new InitialAttackStepImpl();
		return initialAttackStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndAttackStep createEndAttackStep() {
		EndAttackStepImpl endAttackStep = new EndAttackStepImpl();
		return endAttackStep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackTree createAttackTree() {
		AttackTreeImpl attackTree = new AttackTreeImpl();
		return attackTree;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftGoal createSoftGoal() {
		SoftGoalImpl softGoal = new SoftGoalImpl();
		return softGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionBehaviourGoal createDeceptionBehaviourGoal() {
		DeceptionBehaviourGoalImpl deceptionBehaviourGoal = new DeceptionBehaviourGoalImpl();
		return deceptionBehaviourGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionSoftGoal createDeceptionSoftGoal() {
		DeceptionSoftGoalImpl deceptionSoftGoal = new DeceptionSoftGoalImpl();
		return deceptionSoftGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConcreteAsset createConcreteAsset() {
		ConcreteAssetImpl concreteAsset = new ConcreteAssetImpl();
		return concreteAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalContainer createGoalContainer() {
		GoalContainerImpl goalContainer = new GoalContainerImpl();
		return goalContainer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeSelection createNodeSelection() {
		NodeSelectionImpl nodeSelection = new NodeSelectionImpl();
		return nodeSelection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticStrategy createDeceptionTacticStrategy() {
		DeceptionTacticStrategyImpl deceptionTacticStrategy = new DeceptionTacticStrategyImpl();
		return deceptionTacticStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategyTacticRefinement createDeceptionStrategyTacticRefinement() {
		DeceptionStrategyTacticRefinementImpl deceptionStrategyTacticRefinement = new DeceptionStrategyTacticRefinementImpl();
		return deceptionStrategyTacticRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticStrategyRefinement createDeceptionTacticStrategyRefinement() {
		DeceptionTacticStrategyRefinementImpl deceptionTacticStrategyRefinement = new DeceptionTacticStrategyRefinementImpl();
		return deceptionTacticStrategyRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategyRefinement createDeceptionStrategyRefinement() {
		DeceptionStrategyRefinementImpl deceptionStrategyRefinement = new DeceptionStrategyRefinementImpl();
		return deceptionStrategyRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationHasArtificialElement createSimulationHasArtificialElement() {
		SimulationHasArtificialElementImpl simulationHasArtificialElement = new SimulationHasArtificialElementImpl();
		return simulationHasArtificialElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Annotation createAnnotation() {
		AnnotationImpl annotation = new AnnotationImpl();
		return annotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionRisk createDeceptionRisk() {
		DeceptionRiskImpl deceptionRisk = new DeceptionRiskImpl();
		return deceptionRisk;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategy createDeceptionStrategy() {
		DeceptionStrategyImpl deceptionStrategy = new DeceptionStrategyImpl();
		return deceptionStrategy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism createDeceptionTacticMechanism() {
		DeceptionTacticMechanismImpl deceptionTacticMechanism = new DeceptionTacticMechanismImpl();
		return deceptionTacticMechanism;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature createFeature() {
		FeatureImpl feature = new FeatureImpl();
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredConstraint createRequiredConstraint() {
		RequiredConstraintImpl requiredConstraint = new RequiredConstraintImpl();
		return requiredConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExcludeConstraint createExcludeConstraint() {
		ExcludeConstraintImpl excludeConstraint = new ExcludeConstraintImpl();
		return excludeConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BenefitConstraint createBenefitConstraint() {
		BenefitConstraintImpl benefitConstraint = new BenefitConstraintImpl();
		return benefitConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AvoidConstraint createAvoidConstraint() {
		AvoidConstraintImpl avoidConstraint = new AvoidConstraintImpl();
		return avoidConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TacticRefinement createTacticRefinement() {
		TacticRefinementImpl tacticRefinement = new TacticRefinementImpl();
		return tacticRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureRefinement createFeatureRefinement() {
		FeatureRefinementImpl featureRefinement = new FeatureRefinementImpl();
		return featureRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StrategyTacticRefinement createStrategyTacticRefinement() {
		StrategyTacticRefinementImpl strategyTacticRefinement = new StrategyTacticRefinementImpl();
		return strategyTacticRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TacticFeatureRefinement createTacticFeatureRefinement() {
		TacticFeatureRefinementImpl tacticFeatureRefinement = new TacticFeatureRefinementImpl();
		return tacticFeatureRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ORObstructionRefinement createORObstructionRefinement() {
		ORObstructionRefinementImpl orObstructionRefinement = new ORObstructionRefinementImpl();
		return orObstructionRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ANDObstructionRefinement createANDObstructionRefinement() {
		ANDObstructionRefinementImpl andObstructionRefinement = new ANDObstructionRefinementImpl();
		return andObstructionRefinement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Obstacle createObstacle() {
		ObstacleImpl obstacle = new ObstacleImpl();
		return obstacle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfluenceNode createInfluenceNode() {
		InfluenceNodeImpl influenceNode = new InfluenceNodeImpl();
		return influenceNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QualityAttribute createQualityAttribute() {
		QualityAttributeImpl qualityAttribute = new QualityAttributeImpl();
		return qualityAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfluenceRelationship createInfluenceRelationship() {
		InfluenceRelationshipImpl influenceRelationship = new InfluenceRelationshipImpl();
		return influenceRelationship;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Influence createInfluence() {
		InfluenceImpl influence = new InfluenceImpl();
		return influence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Preference createPreference() {
		PreferenceImpl preference = new PreferenceImpl();
		return preference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event createEvent() {
		EventImpl event = new EventImpl();
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticEvent createDeceptionTacticEvent() {
		DeceptionTacticEventImpl deceptionTacticEvent = new DeceptionTacticEventImpl();
		return deceptionTacticEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentEvent createEnvironmentEvent() {
		EnvironmentEventImpl environmentEvent = new EnvironmentEventImpl();
		return environmentEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Belief createBelief() {
		BeliefImpl belief = new BeliefImpl();
		return belief;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalCategoryEnum createGoalCategoryEnumFromString(EDataType eDataType, String initialValue) {
		GoalCategoryEnum result = GoalCategoryEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGoalCategoryEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DomainPropertyCategoryEnum createDomainPropertyCategoryEnumFromString(EDataType eDataType, String initialValue) {
		DomainPropertyCategoryEnum result = DomainPropertyCategoryEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDomainPropertyCategoryEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalPriorityEnum createGoalPriorityEnumFromString(EDataType eDataType, String initialValue) {
		GoalPriorityEnum result = GoalPriorityEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGoalPriorityEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstacleCategoryEnum createObstacleCategoryEnumFromString(EDataType eDataType, String initialValue) {
		ObstacleCategoryEnum result = ObstacleCategoryEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertObstacleCategoryEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PreferenceWeightEnum createPreferenceWeightEnumFromString(EDataType eDataType, String initialValue) {
		PreferenceWeightEnum result = PreferenceWeightEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPreferenceWeightEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfluenceWeightEnum createInfluenceWeightEnumFromString(EDataType eDataType, String initialValue) {
		InfluenceWeightEnum result = InfluenceWeightEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInfluenceWeightEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfluencePolarityEnum createInfluencePolarityEnumFromString(EDataType eDataType, String initialValue) {
		InfluencePolarityEnum result = InfluencePolarityEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInfluencePolarityEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionGoalTypeEnum createDeceptionGoalTypeEnumFromString(EDataType eDataType, String initialValue) {
		DeceptionGoalTypeEnum result = DeceptionGoalTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDeceptionGoalTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftGoalTypeEnum createSoftGoalTypeEnumFromString(EDataType eDataType, String initialValue) {
		SoftGoalTypeEnum result = SoftGoalTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSoftGoalTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodesSelectionType createNodesSelectionTypeFromString(EDataType eDataType, String initialValue) {
		NodesSelectionType result = NodesSelectionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNodesSelectionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChannelTypeEnum createChannelTypeEnumFromString(EDataType eDataType, String initialValue) {
		ChannelTypeEnum result = ChannelTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertChannelTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MitigationLevelEnum createMitigationLevelEnumFromString(EDataType eDataType, String initialValue) {
		MitigationLevelEnum result = MitigationLevelEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMitigationLevelEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetTypeEnum createAssetTypeEnumFromString(EDataType eDataType, String initialValue) {
		AssetTypeEnum result = AssetTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssetTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackTypeEnum createAttackTypeEnumFromString(EDataType eDataType, String initialValue) {
		AttackTypeEnum result = AttackTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAttackTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackerProfileEnum createAttackerProfileEnumFromString(EDataType eDataType, String initialValue) {
		AttackerProfileEnum result = AttackerProfileEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAttackerProfileEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskLikelihoodEnum createRiskLikelihoodEnumFromString(EDataType eDataType, String initialValue) {
		RiskLikelihoodEnum result = RiskLikelihoodEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRiskLikelihoodEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskSeverityEnum createRiskSeverityEnumFromString(EDataType eDataType, String initialValue) {
		RiskSeverityEnum result = RiskSeverityEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRiskSeverityEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityControlIncidentEnum createSecurityControlIncidentEnumFromString(EDataType eDataType, String initialValue) {
		SecurityControlIncidentEnum result = SecurityControlIncidentEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSecurityControlIncidentEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThreatCategoryEnum createThreatCategoryEnumFromString(EDataType eDataType, String initialValue) {
		ThreatCategoryEnum result = ThreatCategoryEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertThreatCategoryEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VulnerabilityTypeEnum createVulnerabilityTypeEnumFromString(EDataType eDataType, String initialValue) {
		VulnerabilityTypeEnum result = VulnerabilityTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertVulnerabilityTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityControlNatureEnum createSecurityControlNatureEnumFromString(EDataType eDataType, String initialValue) {
		SecurityControlNatureEnum result = SecurityControlNatureEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSecurityControlNatureEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BiasTypeEnum createBiasTypeEnumFromString(EDataType eDataType, String initialValue) {
		BiasTypeEnum result = BiasTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBiasTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationTechniqueTypeEnum createSimulationTechniqueTypeEnumFromString(EDataType eDataType, String initialValue) {
		SimulationTechniqueTypeEnum result = SimulationTechniqueTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSimulationTechniqueTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricTypeEnum createMetricTypeEnumFromString(EDataType eDataType, String initialValue) {
		MetricTypeEnum result = MetricTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMetricTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DissimulationTechniqueTypeEnum createDissimulationTechniqueTypeEnumFromString(EDataType eDataType, String initialValue) {
		DissimulationTechniqueTypeEnum result = DissimulationTechniqueTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDissimulationTechniqueTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArtificialElementTypeEnum createArtificialElementTypeEnumFromString(EDataType eDataType, String initialValue) {
		ArtificialElementTypeEnum result = ArtificialElementTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertArtificialElementTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalTypeEnum createGoalTypeEnumFromString(EDataType eDataType, String initialValue) {
		GoalTypeEnum result = GoalTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGoalTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalStabilityEnum createGoalStabilityEnumFromString(EDataType eDataType, String initialValue) {
		GoalStabilityEnum result = GoalStabilityEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGoalStabilityEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentAgentTypeEnum createEnvironmentAgentTypeEnumFromString(EDataType eDataType, String initialValue) {
		EnvironmentAgentTypeEnum result = EnvironmentAgentTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEnvironmentAgentTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackOriginEnum createAttackOriginEnumFromString(EDataType eDataType, String initialValue) {
		AttackOriginEnum result = AttackOriginEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAttackOriginEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackConnectorEnum createAttackConnectorEnumFromString(EDataType eDataType, String initialValue) {
		AttackConnectorEnum result = AttackConnectorEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAttackConnectorEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StoryTypeEnum createStoryTypeEnumFromString(EDataType eDataType, String initialValue) {
		StoryTypeEnum result = StoryTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStoryTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionRiskTypeEnum createDeceptionRiskTypeEnumFromString(EDataType eDataType, String initialValue) {
		DeceptionRiskTypeEnum result = DeceptionRiskTypeEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDeceptionRiskTypeEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BindingStateEnum createBindingStateEnumFromString(EDataType eDataType, String initialValue) {
		BindingStateEnum result = BindingStateEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBindingStateEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnactmentEnum createEnactmentEnumFromString(EDataType eDataType, String initialValue) {
		EnactmentEnum result = EnactmentEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEnactmentEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DmtPackage getDmtPackage() {
		return (DmtPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DmtPackage getPackage() {
		return DmtPackage.eINSTANCE;
	}

} //DmtFactoryImpl
