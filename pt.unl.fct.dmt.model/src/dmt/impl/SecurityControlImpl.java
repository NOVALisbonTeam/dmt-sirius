/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.Requirement;
import dmt.SecurityControl;
import dmt.SecurityControlIncidentEnum;
import dmt.SecurityControlNatureEnum;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Security Control</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.SecurityControlImpl#getPros <em>Pros</em>}</li>
 *   <li>{@link dmt.impl.SecurityControlImpl#getCons <em>Cons</em>}</li>
 *   <li>{@link dmt.impl.SecurityControlImpl#getNature <em>Nature</em>}</li>
 *   <li>{@link dmt.impl.SecurityControlImpl#getIncident <em>Incident</em>}</li>
 *   <li>{@link dmt.impl.SecurityControlImpl#getImplements <em>Implements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityControlImpl extends SecurityNodeImpl implements SecurityControl {
	/**
	 * The default value of the '{@link #getPros() <em>Pros</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPros()
	 * @generated
	 * @ordered
	 */
	protected static final String PROS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPros() <em>Pros</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPros()
	 * @generated
	 * @ordered
	 */
	protected String pros = PROS_EDEFAULT;

	/**
	 * The default value of the '{@link #getCons() <em>Cons</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCons()
	 * @generated
	 * @ordered
	 */
	protected static final String CONS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCons() <em>Cons</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCons()
	 * @generated
	 * @ordered
	 */
	protected String cons = CONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNature() <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNature()
	 * @generated
	 * @ordered
	 */
	protected static final SecurityControlNatureEnum NATURE_EDEFAULT = SecurityControlNatureEnum.TECHNICAL;

	/**
	 * The cached value of the '{@link #getNature() <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNature()
	 * @generated
	 * @ordered
	 */
	protected SecurityControlNatureEnum nature = NATURE_EDEFAULT;

	/**
	 * The default value of the '{@link #getIncident() <em>Incident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncident()
	 * @generated
	 * @ordered
	 */
	protected static final SecurityControlIncidentEnum INCIDENT_EDEFAULT = SecurityControlIncidentEnum.PREVENTIVE;

	/**
	 * The cached value of the '{@link #getIncident() <em>Incident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncident()
	 * @generated
	 * @ordered
	 */
	protected SecurityControlIncidentEnum incident = INCIDENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getImplements() <em>Implements</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImplements()
	 * @generated
	 * @ordered
	 */
	protected Requirement implements_;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityControlImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.SECURITY_CONTROL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPros() {
		return pros;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPros(String newPros) {
		String oldPros = pros;
		pros = newPros;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SECURITY_CONTROL__PROS, oldPros, pros));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCons() {
		return cons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCons(String newCons) {
		String oldCons = cons;
		cons = newCons;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SECURITY_CONTROL__CONS, oldCons, cons));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityControlNatureEnum getNature() {
		return nature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNature(SecurityControlNatureEnum newNature) {
		SecurityControlNatureEnum oldNature = nature;
		nature = newNature == null ? NATURE_EDEFAULT : newNature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SECURITY_CONTROL__NATURE, oldNature, nature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityControlIncidentEnum getIncident() {
		return incident;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIncident(SecurityControlIncidentEnum newIncident) {
		SecurityControlIncidentEnum oldIncident = incident;
		incident = newIncident == null ? INCIDENT_EDEFAULT : newIncident;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SECURITY_CONTROL__INCIDENT, oldIncident, incident));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Requirement getImplements() {
		if (implements_ != null && implements_.eIsProxy()) {
			InternalEObject oldImplements = (InternalEObject)implements_;
			implements_ = (Requirement)eResolveProxy(oldImplements);
			if (implements_ != oldImplements) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.SECURITY_CONTROL__IMPLEMENTS, oldImplements, implements_));
			}
		}
		return implements_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Requirement basicGetImplements() {
		return implements_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImplements(Requirement newImplements) {
		Requirement oldImplements = implements_;
		implements_ = newImplements;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SECURITY_CONTROL__IMPLEMENTS, oldImplements, implements_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.SECURITY_CONTROL__PROS:
				return getPros();
			case DmtPackage.SECURITY_CONTROL__CONS:
				return getCons();
			case DmtPackage.SECURITY_CONTROL__NATURE:
				return getNature();
			case DmtPackage.SECURITY_CONTROL__INCIDENT:
				return getIncident();
			case DmtPackage.SECURITY_CONTROL__IMPLEMENTS:
				if (resolve) return getImplements();
				return basicGetImplements();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.SECURITY_CONTROL__PROS:
				setPros((String)newValue);
				return;
			case DmtPackage.SECURITY_CONTROL__CONS:
				setCons((String)newValue);
				return;
			case DmtPackage.SECURITY_CONTROL__NATURE:
				setNature((SecurityControlNatureEnum)newValue);
				return;
			case DmtPackage.SECURITY_CONTROL__INCIDENT:
				setIncident((SecurityControlIncidentEnum)newValue);
				return;
			case DmtPackage.SECURITY_CONTROL__IMPLEMENTS:
				setImplements((Requirement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.SECURITY_CONTROL__PROS:
				setPros(PROS_EDEFAULT);
				return;
			case DmtPackage.SECURITY_CONTROL__CONS:
				setCons(CONS_EDEFAULT);
				return;
			case DmtPackage.SECURITY_CONTROL__NATURE:
				setNature(NATURE_EDEFAULT);
				return;
			case DmtPackage.SECURITY_CONTROL__INCIDENT:
				setIncident(INCIDENT_EDEFAULT);
				return;
			case DmtPackage.SECURITY_CONTROL__IMPLEMENTS:
				setImplements((Requirement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.SECURITY_CONTROL__PROS:
				return PROS_EDEFAULT == null ? pros != null : !PROS_EDEFAULT.equals(pros);
			case DmtPackage.SECURITY_CONTROL__CONS:
				return CONS_EDEFAULT == null ? cons != null : !CONS_EDEFAULT.equals(cons);
			case DmtPackage.SECURITY_CONTROL__NATURE:
				return nature != NATURE_EDEFAULT;
			case DmtPackage.SECURITY_CONTROL__INCIDENT:
				return incident != INCIDENT_EDEFAULT;
			case DmtPackage.SECURITY_CONTROL__IMPLEMENTS:
				return implements_ != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (pros: ");
		result.append(pros);
		result.append(", cons: ");
		result.append(cons);
		result.append(", nature: ");
		result.append(nature);
		result.append(", incident: ");
		result.append(incident);
		result.append(')');
		return result.toString();
	}

} //SecurityControlImpl
