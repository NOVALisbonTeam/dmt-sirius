/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.ANDObstructionRefinement;
import dmt.DmtPackage;
import dmt.ORObstructionRefinement;
import dmt.ObstructedElement;
import dmt.ObstructionElement;
import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Obstruction Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ObstructionElementImpl#getObstruct <em>Obstruct</em>}</li>
 *   <li>{@link dmt.impl.ObstructionElementImpl#getORObstructionRefined <em>OR Obstruction Refined</em>}</li>
 *   <li>{@link dmt.impl.ObstructionElementImpl#getANDObstructionRefined <em>AND Obstruction Refined</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ObstructionElementImpl extends GoalNodeImpl implements ObstructionElement {
	/**
	 * The cached value of the '{@link #getObstruct() <em>Obstruct</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObstruct()
	 * @generated
	 * @ordered
	 */
	protected EList<ObstructedElement> obstruct;

	/**
	 * The cached value of the '{@link #getORObstructionRefined() <em>OR Obstruction Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getORObstructionRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ORObstructionRefinement> orObstructionRefined;
	/**
	 * The cached value of the '{@link #getANDObstructionRefined() <em>AND Obstruction Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getANDObstructionRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ANDObstructionRefinement> andObstructionRefined;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObstructionElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.OBSTRUCTION_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObstructedElement> getObstruct() {
		if (obstruct == null) {
			obstruct = new EObjectResolvingEList<ObstructedElement>(ObstructedElement.class, this, DmtPackage.OBSTRUCTION_ELEMENT__OBSTRUCT);
		}
		return obstruct;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ORObstructionRefinement> getORObstructionRefined() {
		if (orObstructionRefined == null) {
			orObstructionRefined = new EObjectResolvingEList<ORObstructionRefinement>(ORObstructionRefinement.class, this, DmtPackage.OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED);
		}
		return orObstructionRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ANDObstructionRefinement> getANDObstructionRefined() {
		if (andObstructionRefined == null) {
			andObstructionRefined = new EObjectResolvingEList<ANDObstructionRefinement>(ANDObstructionRefinement.class, this, DmtPackage.OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED);
		}
		return andObstructionRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.OBSTRUCTION_ELEMENT__OBSTRUCT:
				return getObstruct();
			case DmtPackage.OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED:
				return getORObstructionRefined();
			case DmtPackage.OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED:
				return getANDObstructionRefined();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.OBSTRUCTION_ELEMENT__OBSTRUCT:
				getObstruct().clear();
				getObstruct().addAll((Collection<? extends ObstructedElement>)newValue);
				return;
			case DmtPackage.OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED:
				getORObstructionRefined().clear();
				getORObstructionRefined().addAll((Collection<? extends ORObstructionRefinement>)newValue);
				return;
			case DmtPackage.OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED:
				getANDObstructionRefined().clear();
				getANDObstructionRefined().addAll((Collection<? extends ANDObstructionRefinement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.OBSTRUCTION_ELEMENT__OBSTRUCT:
				getObstruct().clear();
				return;
			case DmtPackage.OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED:
				getORObstructionRefined().clear();
				return;
			case DmtPackage.OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED:
				getANDObstructionRefined().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.OBSTRUCTION_ELEMENT__OBSTRUCT:
				return obstruct != null && !obstruct.isEmpty();
			case DmtPackage.OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED:
				return orObstructionRefined != null && !orObstructionRefined.isEmpty();
			case DmtPackage.OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED:
				return andObstructionRefined != null && !andObstructionRefined.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ObstructionElementImpl
