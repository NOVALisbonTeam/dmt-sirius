/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.EnactmentEnum;
import dmt.NodeRefinement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.NodeRefinementImpl#getEnactmentChoice <em>Enactment Choice</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class NodeRefinementImpl extends StrategyRelationshipImpl implements NodeRefinement {
	/**
	 * The default value of the '{@link #getEnactmentChoice() <em>Enactment Choice</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnactmentChoice()
	 * @generated
	 * @ordered
	 */
	protected static final EnactmentEnum ENACTMENT_CHOICE_EDEFAULT = EnactmentEnum.OPTIONAL;

	/**
	 * The cached value of the '{@link #getEnactmentChoice() <em>Enactment Choice</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnactmentChoice()
	 * @generated
	 * @ordered
	 */
	protected EnactmentEnum enactmentChoice = ENACTMENT_CHOICE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodeRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.NODE_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnactmentEnum getEnactmentChoice() {
		return enactmentChoice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnactmentChoice(EnactmentEnum newEnactmentChoice) {
		EnactmentEnum oldEnactmentChoice = enactmentChoice;
		enactmentChoice = newEnactmentChoice == null ? ENACTMENT_CHOICE_EDEFAULT : newEnactmentChoice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.NODE_REFINEMENT__ENACTMENT_CHOICE, oldEnactmentChoice, enactmentChoice));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.NODE_REFINEMENT__ENACTMENT_CHOICE:
				return getEnactmentChoice();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.NODE_REFINEMENT__ENACTMENT_CHOICE:
				setEnactmentChoice((EnactmentEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.NODE_REFINEMENT__ENACTMENT_CHOICE:
				setEnactmentChoice(ENACTMENT_CHOICE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.NODE_REFINEMENT__ENACTMENT_CHOICE:
				return enactmentChoice != ENACTMENT_CHOICE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (enactmentChoice: ");
		result.append(enactmentChoice);
		result.append(')');
		return result.toString();
	}

} //NodeRefinementImpl
