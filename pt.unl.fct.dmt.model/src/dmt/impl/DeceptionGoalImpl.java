/**
 */
package dmt.impl;

import dmt.DeceptionGoal;
import dmt.DeceptionGoalTypeEnum;
import dmt.DmtPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Goal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionGoalImpl#getDeceptionGoalType <em>Deception Goal Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DeceptionGoalImpl extends GoalImpl implements DeceptionGoal {
	/**
	 * The default value of the '{@link #getDeceptionGoalType() <em>Deception Goal Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeceptionGoalType()
	 * @generated
	 * @ordered
	 */
	protected static final DeceptionGoalTypeEnum DECEPTION_GOAL_TYPE_EDEFAULT = DeceptionGoalTypeEnum.UNNOTICED;

	/**
	 * The cached value of the '{@link #getDeceptionGoalType() <em>Deception Goal Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeceptionGoalType()
	 * @generated
	 * @ordered
	 */
	protected DeceptionGoalTypeEnum deceptionGoalType = DECEPTION_GOAL_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionGoalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_GOAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionGoalTypeEnum getDeceptionGoalType() {
		return deceptionGoalType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeceptionGoalType(DeceptionGoalTypeEnum newDeceptionGoalType) {
		DeceptionGoalTypeEnum oldDeceptionGoalType = deceptionGoalType;
		deceptionGoalType = newDeceptionGoalType == null ? DECEPTION_GOAL_TYPE_EDEFAULT : newDeceptionGoalType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_GOAL__DECEPTION_GOAL_TYPE, oldDeceptionGoalType, deceptionGoalType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_GOAL__DECEPTION_GOAL_TYPE:
				return getDeceptionGoalType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_GOAL__DECEPTION_GOAL_TYPE:
				setDeceptionGoalType((DeceptionGoalTypeEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_GOAL__DECEPTION_GOAL_TYPE:
				setDeceptionGoalType(DECEPTION_GOAL_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_GOAL__DECEPTION_GOAL_TYPE:
				return deceptionGoalType != DECEPTION_GOAL_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (DeceptionGoalType: ");
		result.append(deceptionGoalType);
		result.append(')');
		return result.toString();
	}

} //DeceptionGoalImpl
