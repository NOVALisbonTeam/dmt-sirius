/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.ArtificialElement;
import dmt.DmtPackage;
import dmt.Simulation;
import dmt.SimulationHasArtificialElement;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simulation Has Artificial Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.SimulationHasArtificialElementImpl#getShowFalseAsset <em>Show False Asset</em>}</li>
 *   <li>{@link dmt.impl.SimulationHasArtificialElementImpl#getLowerFakes <em>Lower Fakes</em>}</li>
 *   <li>{@link dmt.impl.SimulationHasArtificialElementImpl#getUpperFakes <em>Upper Fakes</em>}</li>
 *   <li>{@link dmt.impl.SimulationHasArtificialElementImpl#getSimulationType <em>Simulation Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SimulationHasArtificialElementImpl extends DeceptionRelationshipImpl implements SimulationHasArtificialElement {
	/**
	 * The cached value of the '{@link #getShowFalseAsset() <em>Show False Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShowFalseAsset()
	 * @generated
	 * @ordered
	 */
	protected ArtificialElement showFalseAsset;

	/**
	 * The default value of the '{@link #getLowerFakes() <em>Lower Fakes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerFakes()
	 * @generated
	 * @ordered
	 */
	protected static final String LOWER_FAKES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLowerFakes() <em>Lower Fakes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLowerFakes()
	 * @generated
	 * @ordered
	 */
	protected String lowerFakes = LOWER_FAKES_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpperFakes() <em>Upper Fakes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperFakes()
	 * @generated
	 * @ordered
	 */
	protected static final String UPPER_FAKES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUpperFakes() <em>Upper Fakes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpperFakes()
	 * @generated
	 * @ordered
	 */
	protected String upperFakes = UPPER_FAKES_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSimulationType() <em>Simulation Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimulationType()
	 * @generated
	 * @ordered
	 */
	protected Simulation simulationType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SimulationHasArtificialElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.SIMULATION_HAS_ARTIFICIAL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArtificialElement getShowFalseAsset() {
		if (showFalseAsset != null && showFalseAsset.eIsProxy()) {
			InternalEObject oldShowFalseAsset = (InternalEObject)showFalseAsset;
			showFalseAsset = (ArtificialElement)eResolveProxy(oldShowFalseAsset);
			if (showFalseAsset != oldShowFalseAsset) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET, oldShowFalseAsset, showFalseAsset));
			}
		}
		return showFalseAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArtificialElement basicGetShowFalseAsset() {
		return showFalseAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShowFalseAsset(ArtificialElement newShowFalseAsset) {
		ArtificialElement oldShowFalseAsset = showFalseAsset;
		showFalseAsset = newShowFalseAsset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET, oldShowFalseAsset, showFalseAsset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLowerFakes() {
		return lowerFakes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLowerFakes(String newLowerFakes) {
		String oldLowerFakes = lowerFakes;
		lowerFakes = newLowerFakes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES, oldLowerFakes, lowerFakes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUpperFakes() {
		return upperFakes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpperFakes(String newUpperFakes) {
		String oldUpperFakes = upperFakes;
		upperFakes = newUpperFakes;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES, oldUpperFakes, upperFakes));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation getSimulationType() {
		if (simulationType != null && simulationType.eIsProxy()) {
			InternalEObject oldSimulationType = (InternalEObject)simulationType;
			simulationType = (Simulation)eResolveProxy(oldSimulationType);
			if (simulationType != oldSimulationType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE, oldSimulationType, simulationType));
			}
		}
		return simulationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation basicGetSimulationType() {
		return simulationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimulationType(Simulation newSimulationType) {
		Simulation oldSimulationType = simulationType;
		simulationType = newSimulationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE, oldSimulationType, simulationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET:
				if (resolve) return getShowFalseAsset();
				return basicGetShowFalseAsset();
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES:
				return getLowerFakes();
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES:
				return getUpperFakes();
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE:
				if (resolve) return getSimulationType();
				return basicGetSimulationType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET:
				setShowFalseAsset((ArtificialElement)newValue);
				return;
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES:
				setLowerFakes((String)newValue);
				return;
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES:
				setUpperFakes((String)newValue);
				return;
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE:
				setSimulationType((Simulation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET:
				setShowFalseAsset((ArtificialElement)null);
				return;
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES:
				setLowerFakes(LOWER_FAKES_EDEFAULT);
				return;
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES:
				setUpperFakes(UPPER_FAKES_EDEFAULT);
				return;
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE:
				setSimulationType((Simulation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET:
				return showFalseAsset != null;
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES:
				return LOWER_FAKES_EDEFAULT == null ? lowerFakes != null : !LOWER_FAKES_EDEFAULT.equals(lowerFakes);
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES:
				return UPPER_FAKES_EDEFAULT == null ? upperFakes != null : !UPPER_FAKES_EDEFAULT.equals(upperFakes);
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE:
				return simulationType != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (lowerFakes: ");
		result.append(lowerFakes);
		result.append(", upperFakes: ");
		result.append(upperFakes);
		result.append(')');
		return result.toString();
	}

} //SimulationHasArtificialElementImpl
