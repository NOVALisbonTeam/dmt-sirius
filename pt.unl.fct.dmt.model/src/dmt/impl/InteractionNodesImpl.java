/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.InteractionNodes;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interaction Nodes</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class InteractionNodesImpl extends MinimalEObjectImpl.Container implements InteractionNodes {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InteractionNodesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.INTERACTION_NODES;
	}

} //InteractionNodesImpl
