/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.Requirement;
import dmt.SoftwareAgent;

import java.util.Collection;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Software Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.SoftwareAgentImpl#getResponsibilitySoftware <em>Responsibility Software</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SoftwareAgentImpl extends AgentImpl implements SoftwareAgent {
	/**
	 * The cached value of the '{@link #getResponsibilitySoftware() <em>Responsibility Software</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibilitySoftware()
	 * @generated
	 * @ordered
	 */
	protected EList<Requirement> responsibilitySoftware;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoftwareAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.SOFTWARE_AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Requirement> getResponsibilitySoftware() {
		if (responsibilitySoftware == null) {
			responsibilitySoftware = new EObjectResolvingEList<Requirement>(Requirement.class, this, DmtPackage.SOFTWARE_AGENT__RESPONSIBILITY_SOFTWARE);
		}
		return responsibilitySoftware;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.SOFTWARE_AGENT__RESPONSIBILITY_SOFTWARE:
				return getResponsibilitySoftware();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.SOFTWARE_AGENT__RESPONSIBILITY_SOFTWARE:
				getResponsibilitySoftware().clear();
				getResponsibilitySoftware().addAll((Collection<? extends Requirement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.SOFTWARE_AGENT__RESPONSIBILITY_SOFTWARE:
				getResponsibilitySoftware().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.SOFTWARE_AGENT__RESPONSIBILITY_SOFTWARE:
				return responsibilitySoftware != null && !responsibilitySoftware.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SoftwareAgentImpl
