/**
 */
package dmt.impl;

import dmt.Attack;
import dmt.Attacker;
import dmt.AttackerProfileEnum;
import dmt.DmtPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attacker</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.AttackerImpl#getProfile <em>Profile</em>}</li>
 *   <li>{@link dmt.impl.AttackerImpl#getLaunchesAttack <em>Launches Attack</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttackerImpl extends SecurityNodeImpl implements Attacker {
	/**
	 * The default value of the '{@link #getProfile() <em>Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfile()
	 * @generated
	 * @ordered
	 */
	protected static final AttackerProfileEnum PROFILE_EDEFAULT = AttackerProfileEnum.SCRIPTKIDDIE;

	/**
	 * The cached value of the '{@link #getProfile() <em>Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProfile()
	 * @generated
	 * @ordered
	 */
	protected AttackerProfileEnum profile = PROFILE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLaunchesAttack() <em>Launches Attack</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLaunchesAttack()
	 * @generated
	 * @ordered
	 */
	protected EList<Attack> launchesAttack;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttackerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.ATTACKER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackerProfileEnum getProfile() {
		return profile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProfile(AttackerProfileEnum newProfile) {
		AttackerProfileEnum oldProfile = profile;
		profile = newProfile == null ? PROFILE_EDEFAULT : newProfile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACKER__PROFILE, oldProfile, profile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attack> getLaunchesAttack() {
		if (launchesAttack == null) {
			launchesAttack = new EObjectResolvingEList<Attack>(Attack.class, this, DmtPackage.ATTACKER__LAUNCHES_ATTACK);
		}
		return launchesAttack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.ATTACKER__PROFILE:
				return getProfile();
			case DmtPackage.ATTACKER__LAUNCHES_ATTACK:
				return getLaunchesAttack();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.ATTACKER__PROFILE:
				setProfile((AttackerProfileEnum)newValue);
				return;
			case DmtPackage.ATTACKER__LAUNCHES_ATTACK:
				getLaunchesAttack().clear();
				getLaunchesAttack().addAll((Collection<? extends Attack>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACKER__PROFILE:
				setProfile(PROFILE_EDEFAULT);
				return;
			case DmtPackage.ATTACKER__LAUNCHES_ATTACK:
				getLaunchesAttack().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACKER__PROFILE:
				return profile != PROFILE_EDEFAULT;
			case DmtPackage.ATTACKER__LAUNCHES_ATTACK:
				return launchesAttack != null && !launchesAttack.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (profile: ");
		result.append(profile);
		result.append(')');
		return result.toString();
	}

} //AttackerImpl
