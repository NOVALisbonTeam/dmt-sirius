/**
 */
package dmt.impl;

import dmt.ANDObstructionRefinement;
import dmt.DmtPackage;
import dmt.GoalNode;
import dmt.ORObstructionRefinement;
import dmt.ObstructedElement;
import dmt.ObstructionElement;
import dmt.Threat;
import dmt.ThreatCategoryEnum;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Threat</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ThreatImpl#getObstruct <em>Obstruct</em>}</li>
 *   <li>{@link dmt.impl.ThreatImpl#getORObstructionRefined <em>OR Obstruction Refined</em>}</li>
 *   <li>{@link dmt.impl.ThreatImpl#getANDObstructionRefined <em>AND Obstruction Refined</em>}</li>
 *   <li>{@link dmt.impl.ThreatImpl#getCategory <em>Category</em>}</li>
 *   <li>{@link dmt.impl.ThreatImpl#getScenario <em>Scenario</em>}</li>
 *   <li>{@link dmt.impl.ThreatImpl#getContributesToThreat <em>Contributes To Threat</em>}</li>
 *   <li>{@link dmt.impl.ThreatImpl#getConsequence <em>Consequence</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ThreatImpl extends MitigableElementImpl implements Threat {
	/**
	 * The cached value of the '{@link #getObstruct() <em>Obstruct</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObstruct()
	 * @generated
	 * @ordered
	 */
	protected EList<ObstructedElement> obstruct;

	/**
	 * The cached value of the '{@link #getORObstructionRefined() <em>OR Obstruction Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getORObstructionRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ORObstructionRefinement> orObstructionRefined;

	/**
	 * The cached value of the '{@link #getANDObstructionRefined() <em>AND Obstruction Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getANDObstructionRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ANDObstructionRefinement> andObstructionRefined;

	/**
	 * The default value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected static final ThreatCategoryEnum CATEGORY_EDEFAULT = ThreatCategoryEnum.INTENTIONAL;

	/**
	 * The cached value of the '{@link #getCategory() <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCategory()
	 * @generated
	 * @ordered
	 */
	protected ThreatCategoryEnum category = CATEGORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getScenario() <em>Scenario</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenario()
	 * @generated
	 * @ordered
	 */
	protected static final String SCENARIO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getScenario() <em>Scenario</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScenario()
	 * @generated
	 * @ordered
	 */
	protected String scenario = SCENARIO_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContributesToThreat() <em>Contributes To Threat</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContributesToThreat()
	 * @generated
	 * @ordered
	 */
	protected EList<Threat> contributesToThreat;

	/**
	 * The default value of the '{@link #getConsequence() <em>Consequence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsequence()
	 * @generated
	 * @ordered
	 */
	protected static final String CONSEQUENCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConsequence() <em>Consequence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConsequence()
	 * @generated
	 * @ordered
	 */
	protected String consequence = CONSEQUENCE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ThreatImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.THREAT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ObstructedElement> getObstruct() {
		if (obstruct == null) {
			obstruct = new EObjectResolvingEList<ObstructedElement>(ObstructedElement.class, this, DmtPackage.THREAT__OBSTRUCT);
		}
		return obstruct;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ORObstructionRefinement> getORObstructionRefined() {
		if (orObstructionRefined == null) {
			orObstructionRefined = new EObjectResolvingEList<ORObstructionRefinement>(ORObstructionRefinement.class, this, DmtPackage.THREAT__OR_OBSTRUCTION_REFINED);
		}
		return orObstructionRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ANDObstructionRefinement> getANDObstructionRefined() {
		if (andObstructionRefined == null) {
			andObstructionRefined = new EObjectResolvingEList<ANDObstructionRefinement>(ANDObstructionRefinement.class, this, DmtPackage.THREAT__AND_OBSTRUCTION_REFINED);
		}
		return andObstructionRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ThreatCategoryEnum getCategory() {
		return category;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCategory(ThreatCategoryEnum newCategory) {
		ThreatCategoryEnum oldCategory = category;
		category = newCategory == null ? CATEGORY_EDEFAULT : newCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.THREAT__CATEGORY, oldCategory, category));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScenario() {
		return scenario;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScenario(String newScenario) {
		String oldScenario = scenario;
		scenario = newScenario;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.THREAT__SCENARIO, oldScenario, scenario));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Threat> getContributesToThreat() {
		if (contributesToThreat == null) {
			contributesToThreat = new EObjectResolvingEList<Threat>(Threat.class, this, DmtPackage.THREAT__CONTRIBUTES_TO_THREAT);
		}
		return contributesToThreat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConsequence() {
		return consequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConsequence(String newConsequence) {
		String oldConsequence = consequence;
		consequence = newConsequence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.THREAT__CONSEQUENCE, oldConsequence, consequence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.THREAT__OBSTRUCT:
				return getObstruct();
			case DmtPackage.THREAT__OR_OBSTRUCTION_REFINED:
				return getORObstructionRefined();
			case DmtPackage.THREAT__AND_OBSTRUCTION_REFINED:
				return getANDObstructionRefined();
			case DmtPackage.THREAT__CATEGORY:
				return getCategory();
			case DmtPackage.THREAT__SCENARIO:
				return getScenario();
			case DmtPackage.THREAT__CONTRIBUTES_TO_THREAT:
				return getContributesToThreat();
			case DmtPackage.THREAT__CONSEQUENCE:
				return getConsequence();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.THREAT__OBSTRUCT:
				getObstruct().clear();
				getObstruct().addAll((Collection<? extends ObstructedElement>)newValue);
				return;
			case DmtPackage.THREAT__OR_OBSTRUCTION_REFINED:
				getORObstructionRefined().clear();
				getORObstructionRefined().addAll((Collection<? extends ORObstructionRefinement>)newValue);
				return;
			case DmtPackage.THREAT__AND_OBSTRUCTION_REFINED:
				getANDObstructionRefined().clear();
				getANDObstructionRefined().addAll((Collection<? extends ANDObstructionRefinement>)newValue);
				return;
			case DmtPackage.THREAT__CATEGORY:
				setCategory((ThreatCategoryEnum)newValue);
				return;
			case DmtPackage.THREAT__SCENARIO:
				setScenario((String)newValue);
				return;
			case DmtPackage.THREAT__CONTRIBUTES_TO_THREAT:
				getContributesToThreat().clear();
				getContributesToThreat().addAll((Collection<? extends Threat>)newValue);
				return;
			case DmtPackage.THREAT__CONSEQUENCE:
				setConsequence((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.THREAT__OBSTRUCT:
				getObstruct().clear();
				return;
			case DmtPackage.THREAT__OR_OBSTRUCTION_REFINED:
				getORObstructionRefined().clear();
				return;
			case DmtPackage.THREAT__AND_OBSTRUCTION_REFINED:
				getANDObstructionRefined().clear();
				return;
			case DmtPackage.THREAT__CATEGORY:
				setCategory(CATEGORY_EDEFAULT);
				return;
			case DmtPackage.THREAT__SCENARIO:
				setScenario(SCENARIO_EDEFAULT);
				return;
			case DmtPackage.THREAT__CONTRIBUTES_TO_THREAT:
				getContributesToThreat().clear();
				return;
			case DmtPackage.THREAT__CONSEQUENCE:
				setConsequence(CONSEQUENCE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.THREAT__OBSTRUCT:
				return obstruct != null && !obstruct.isEmpty();
			case DmtPackage.THREAT__OR_OBSTRUCTION_REFINED:
				return orObstructionRefined != null && !orObstructionRefined.isEmpty();
			case DmtPackage.THREAT__AND_OBSTRUCTION_REFINED:
				return andObstructionRefined != null && !andObstructionRefined.isEmpty();
			case DmtPackage.THREAT__CATEGORY:
				return category != CATEGORY_EDEFAULT;
			case DmtPackage.THREAT__SCENARIO:
				return SCENARIO_EDEFAULT == null ? scenario != null : !SCENARIO_EDEFAULT.equals(scenario);
			case DmtPackage.THREAT__CONTRIBUTES_TO_THREAT:
				return contributesToThreat != null && !contributesToThreat.isEmpty();
			case DmtPackage.THREAT__CONSEQUENCE:
				return CONSEQUENCE_EDEFAULT == null ? consequence != null : !CONSEQUENCE_EDEFAULT.equals(consequence);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == GoalNode.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ObstructionElement.class) {
			switch (derivedFeatureID) {
				case DmtPackage.THREAT__OBSTRUCT: return DmtPackage.OBSTRUCTION_ELEMENT__OBSTRUCT;
				case DmtPackage.THREAT__OR_OBSTRUCTION_REFINED: return DmtPackage.OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED;
				case DmtPackage.THREAT__AND_OBSTRUCTION_REFINED: return DmtPackage.OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == GoalNode.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == ObstructionElement.class) {
			switch (baseFeatureID) {
				case DmtPackage.OBSTRUCTION_ELEMENT__OBSTRUCT: return DmtPackage.THREAT__OBSTRUCT;
				case DmtPackage.OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED: return DmtPackage.THREAT__OR_OBSTRUCTION_REFINED;
				case DmtPackage.OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED: return DmtPackage.THREAT__AND_OBSTRUCTION_REFINED;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (category: ");
		result.append(category);
		result.append(", scenario: ");
		result.append(scenario);
		result.append(", consequence: ");
		result.append(consequence);
		result.append(')');
		return result.toString();
	}

} //ThreatImpl
