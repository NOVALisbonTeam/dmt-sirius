/**
 */
package dmt.impl;

import dmt.BehaviourGoal;
import dmt.DmtPackage;
import dmt.GoalTypeEnum;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Behaviour Goal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.BehaviourGoalImpl#getType <em>Type</em>}</li>
 *   <li>{@link dmt.impl.BehaviourGoalImpl#getFormalSpec <em>Formal Spec</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BehaviourGoalImpl extends GoalImpl implements BehaviourGoal {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final GoalTypeEnum TYPE_EDEFAULT = GoalTypeEnum.ACHIEVE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected GoalTypeEnum type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalSpec() <em>Formal Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalSpec()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAL_SPEC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormalSpec() <em>Formal Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalSpec()
	 * @generated
	 * @ordered
	 */
	protected String formalSpec = FORMAL_SPEC_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BehaviourGoalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.BEHAVIOUR_GOAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalTypeEnum getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(GoalTypeEnum newType) {
		GoalTypeEnum oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.BEHAVIOUR_GOAL__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalSpec() {
		return formalSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalSpec(String newFormalSpec) {
		String oldFormalSpec = formalSpec;
		formalSpec = newFormalSpec;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.BEHAVIOUR_GOAL__FORMAL_SPEC, oldFormalSpec, formalSpec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.BEHAVIOUR_GOAL__TYPE:
				return getType();
			case DmtPackage.BEHAVIOUR_GOAL__FORMAL_SPEC:
				return getFormalSpec();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.BEHAVIOUR_GOAL__TYPE:
				setType((GoalTypeEnum)newValue);
				return;
			case DmtPackage.BEHAVIOUR_GOAL__FORMAL_SPEC:
				setFormalSpec((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.BEHAVIOUR_GOAL__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case DmtPackage.BEHAVIOUR_GOAL__FORMAL_SPEC:
				setFormalSpec(FORMAL_SPEC_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.BEHAVIOUR_GOAL__TYPE:
				return type != TYPE_EDEFAULT;
			case DmtPackage.BEHAVIOUR_GOAL__FORMAL_SPEC:
				return FORMAL_SPEC_EDEFAULT == null ? formalSpec != null : !FORMAL_SPEC_EDEFAULT.equals(formalSpec);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", formalSpec: ");
		result.append(formalSpec);
		result.append(')');
		return result.toString();
	}

} //BehaviourGoalImpl
