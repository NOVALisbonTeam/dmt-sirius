/**
 */
package dmt.impl;

import dmt.Attack;
import dmt.AttackOriginEnum;
import dmt.AttackTypeEnum;
import dmt.AttackVector;
import dmt.DmtPackage;
import dmt.RiskComponent;
import dmt.Threat;
import dmt.Vulnerability;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attack</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.AttackImpl#getType <em>Type</em>}</li>
 *   <li>{@link dmt.impl.AttackImpl#getInstantiatesThreat <em>Instantiates Threat</em>}</li>
 *   <li>{@link dmt.impl.AttackImpl#getExploitsVulnerability <em>Exploits Vulnerability</em>}</li>
 *   <li>{@link dmt.impl.AttackImpl#getContributesToAttack <em>Contributes To Attack</em>}</li>
 *   <li>{@link dmt.impl.AttackImpl#getLeadsRisk <em>Leads Risk</em>}</li>
 *   <li>{@link dmt.impl.AttackImpl#getOrigin <em>Origin</em>}</li>
 *   <li>{@link dmt.impl.AttackImpl#getRealizedByAttackVector <em>Realized By Attack Vector</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttackImpl extends MitigableElementImpl implements Attack {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final AttackTypeEnum TYPE_EDEFAULT = AttackTypeEnum.ACTIVE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected AttackTypeEnum type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInstantiatesThreat() <em>Instantiates Threat</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstantiatesThreat()
	 * @generated
	 * @ordered
	 */
	protected EList<Threat> instantiatesThreat;

	/**
	 * The cached value of the '{@link #getExploitsVulnerability() <em>Exploits Vulnerability</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExploitsVulnerability()
	 * @generated
	 * @ordered
	 */
	protected EList<Vulnerability> exploitsVulnerability;

	/**
	 * The cached value of the '{@link #getContributesToAttack() <em>Contributes To Attack</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContributesToAttack()
	 * @generated
	 * @ordered
	 */
	protected EList<Attack> contributesToAttack;

	/**
	 * The cached value of the '{@link #getLeadsRisk() <em>Leads Risk</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeadsRisk()
	 * @generated
	 * @ordered
	 */
	protected EList<RiskComponent> leadsRisk;

	/**
	 * The default value of the '{@link #getOrigin() <em>Origin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrigin()
	 * @generated
	 * @ordered
	 */
	protected static final AttackOriginEnum ORIGIN_EDEFAULT = AttackOriginEnum.INSIDER;

	/**
	 * The cached value of the '{@link #getOrigin() <em>Origin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrigin()
	 * @generated
	 * @ordered
	 */
	protected AttackOriginEnum origin = ORIGIN_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRealizedByAttackVector() <em>Realized By Attack Vector</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealizedByAttackVector()
	 * @generated
	 * @ordered
	 */
	protected EList<AttackVector> realizedByAttackVector;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttackImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.ATTACK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackTypeEnum getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(AttackTypeEnum newType) {
		AttackTypeEnum oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Threat> getInstantiatesThreat() {
		if (instantiatesThreat == null) {
			instantiatesThreat = new EObjectResolvingEList<Threat>(Threat.class, this, DmtPackage.ATTACK__INSTANTIATES_THREAT);
		}
		return instantiatesThreat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Vulnerability> getExploitsVulnerability() {
		if (exploitsVulnerability == null) {
			exploitsVulnerability = new EObjectResolvingEList<Vulnerability>(Vulnerability.class, this, DmtPackage.ATTACK__EXPLOITS_VULNERABILITY);
		}
		return exploitsVulnerability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Attack> getContributesToAttack() {
		if (contributesToAttack == null) {
			contributesToAttack = new EObjectResolvingEList<Attack>(Attack.class, this, DmtPackage.ATTACK__CONTRIBUTES_TO_ATTACK);
		}
		return contributesToAttack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RiskComponent> getLeadsRisk() {
		if (leadsRisk == null) {
			leadsRisk = new EObjectResolvingEList<RiskComponent>(RiskComponent.class, this, DmtPackage.ATTACK__LEADS_RISK);
		}
		return leadsRisk;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackOriginEnum getOrigin() {
		return origin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrigin(AttackOriginEnum newOrigin) {
		AttackOriginEnum oldOrigin = origin;
		origin = newOrigin == null ? ORIGIN_EDEFAULT : newOrigin;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK__ORIGIN, oldOrigin, origin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttackVector> getRealizedByAttackVector() {
		if (realizedByAttackVector == null) {
			realizedByAttackVector = new EObjectWithInverseResolvingEList<AttackVector>(AttackVector.class, this, DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR, DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK);
		}
		return realizedByAttackVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRealizedByAttackVector()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR:
				return ((InternalEList<?>)getRealizedByAttackVector()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.ATTACK__TYPE:
				return getType();
			case DmtPackage.ATTACK__INSTANTIATES_THREAT:
				return getInstantiatesThreat();
			case DmtPackage.ATTACK__EXPLOITS_VULNERABILITY:
				return getExploitsVulnerability();
			case DmtPackage.ATTACK__CONTRIBUTES_TO_ATTACK:
				return getContributesToAttack();
			case DmtPackage.ATTACK__LEADS_RISK:
				return getLeadsRisk();
			case DmtPackage.ATTACK__ORIGIN:
				return getOrigin();
			case DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR:
				return getRealizedByAttackVector();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.ATTACK__TYPE:
				setType((AttackTypeEnum)newValue);
				return;
			case DmtPackage.ATTACK__INSTANTIATES_THREAT:
				getInstantiatesThreat().clear();
				getInstantiatesThreat().addAll((Collection<? extends Threat>)newValue);
				return;
			case DmtPackage.ATTACK__EXPLOITS_VULNERABILITY:
				getExploitsVulnerability().clear();
				getExploitsVulnerability().addAll((Collection<? extends Vulnerability>)newValue);
				return;
			case DmtPackage.ATTACK__CONTRIBUTES_TO_ATTACK:
				getContributesToAttack().clear();
				getContributesToAttack().addAll((Collection<? extends Attack>)newValue);
				return;
			case DmtPackage.ATTACK__LEADS_RISK:
				getLeadsRisk().clear();
				getLeadsRisk().addAll((Collection<? extends RiskComponent>)newValue);
				return;
			case DmtPackage.ATTACK__ORIGIN:
				setOrigin((AttackOriginEnum)newValue);
				return;
			case DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR:
				getRealizedByAttackVector().clear();
				getRealizedByAttackVector().addAll((Collection<? extends AttackVector>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACK__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case DmtPackage.ATTACK__INSTANTIATES_THREAT:
				getInstantiatesThreat().clear();
				return;
			case DmtPackage.ATTACK__EXPLOITS_VULNERABILITY:
				getExploitsVulnerability().clear();
				return;
			case DmtPackage.ATTACK__CONTRIBUTES_TO_ATTACK:
				getContributesToAttack().clear();
				return;
			case DmtPackage.ATTACK__LEADS_RISK:
				getLeadsRisk().clear();
				return;
			case DmtPackage.ATTACK__ORIGIN:
				setOrigin(ORIGIN_EDEFAULT);
				return;
			case DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR:
				getRealizedByAttackVector().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACK__TYPE:
				return type != TYPE_EDEFAULT;
			case DmtPackage.ATTACK__INSTANTIATES_THREAT:
				return instantiatesThreat != null && !instantiatesThreat.isEmpty();
			case DmtPackage.ATTACK__EXPLOITS_VULNERABILITY:
				return exploitsVulnerability != null && !exploitsVulnerability.isEmpty();
			case DmtPackage.ATTACK__CONTRIBUTES_TO_ATTACK:
				return contributesToAttack != null && !contributesToAttack.isEmpty();
			case DmtPackage.ATTACK__LEADS_RISK:
				return leadsRisk != null && !leadsRisk.isEmpty();
			case DmtPackage.ATTACK__ORIGIN:
				return origin != ORIGIN_EDEFAULT;
			case DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR:
				return realizedByAttackVector != null && !realizedByAttackVector.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", origin: ");
		result.append(origin);
		result.append(')');
		return result.toString();
	}

} //AttackImpl
