/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.SecurityNode;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Security Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class SecurityNodeImpl extends NodeImpl implements SecurityNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.SECURITY_NODE;
	}

} //SecurityNodeImpl
