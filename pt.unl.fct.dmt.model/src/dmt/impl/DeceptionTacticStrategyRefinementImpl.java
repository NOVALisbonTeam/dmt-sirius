/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DeceptionTacticStrategy;
import dmt.DeceptionTacticStrategyRefinement;
import dmt.DmtPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Tactic Strategy Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionTacticStrategyRefinementImpl#getTacticstrategytacticrefinementsource <em>Tacticstrategytacticrefinementsource</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticStrategyRefinementImpl#getTacticstrategytacticrefinementtarget <em>Tacticstrategytacticrefinementtarget</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionTacticStrategyRefinementImpl extends NodeRefinementImpl implements DeceptionTacticStrategyRefinement {
	/**
	 * The cached value of the '{@link #getTacticstrategytacticrefinementsource() <em>Tacticstrategytacticrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTacticstrategytacticrefinementsource()
	 * @generated
	 * @ordered
	 */
	protected DeceptionTacticStrategy tacticstrategytacticrefinementsource;

	/**
	 * The cached value of the '{@link #getTacticstrategytacticrefinementtarget() <em>Tacticstrategytacticrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTacticstrategytacticrefinementtarget()
	 * @generated
	 * @ordered
	 */
	protected DeceptionTacticStrategy tacticstrategytacticrefinementtarget;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionTacticStrategyRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_TACTIC_STRATEGY_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticStrategy getTacticstrategytacticrefinementsource() {
		if (tacticstrategytacticrefinementsource != null && tacticstrategytacticrefinementsource.eIsProxy()) {
			InternalEObject oldTacticstrategytacticrefinementsource = (InternalEObject)tacticstrategytacticrefinementsource;
			tacticstrategytacticrefinementsource = (DeceptionTacticStrategy)eResolveProxy(oldTacticstrategytacticrefinementsource);
			if (tacticstrategytacticrefinementsource != oldTacticstrategytacticrefinementsource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTSOURCE, oldTacticstrategytacticrefinementsource, tacticstrategytacticrefinementsource));
			}
		}
		return tacticstrategytacticrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticStrategy basicGetTacticstrategytacticrefinementsource() {
		return tacticstrategytacticrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTacticstrategytacticrefinementsource(DeceptionTacticStrategy newTacticstrategytacticrefinementsource) {
		DeceptionTacticStrategy oldTacticstrategytacticrefinementsource = tacticstrategytacticrefinementsource;
		tacticstrategytacticrefinementsource = newTacticstrategytacticrefinementsource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTSOURCE, oldTacticstrategytacticrefinementsource, tacticstrategytacticrefinementsource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticStrategy getTacticstrategytacticrefinementtarget() {
		if (tacticstrategytacticrefinementtarget != null && tacticstrategytacticrefinementtarget.eIsProxy()) {
			InternalEObject oldTacticstrategytacticrefinementtarget = (InternalEObject)tacticstrategytacticrefinementtarget;
			tacticstrategytacticrefinementtarget = (DeceptionTacticStrategy)eResolveProxy(oldTacticstrategytacticrefinementtarget);
			if (tacticstrategytacticrefinementtarget != oldTacticstrategytacticrefinementtarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTTARGET, oldTacticstrategytacticrefinementtarget, tacticstrategytacticrefinementtarget));
			}
		}
		return tacticstrategytacticrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticStrategy basicGetTacticstrategytacticrefinementtarget() {
		return tacticstrategytacticrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTacticstrategytacticrefinementtarget(DeceptionTacticStrategy newTacticstrategytacticrefinementtarget) {
		DeceptionTacticStrategy oldTacticstrategytacticrefinementtarget = tacticstrategytacticrefinementtarget;
		tacticstrategytacticrefinementtarget = newTacticstrategytacticrefinementtarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTTARGET, oldTacticstrategytacticrefinementtarget, tacticstrategytacticrefinementtarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTSOURCE:
				if (resolve) return getTacticstrategytacticrefinementsource();
				return basicGetTacticstrategytacticrefinementsource();
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTTARGET:
				if (resolve) return getTacticstrategytacticrefinementtarget();
				return basicGetTacticstrategytacticrefinementtarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTSOURCE:
				setTacticstrategytacticrefinementsource((DeceptionTacticStrategy)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTTARGET:
				setTacticstrategytacticrefinementtarget((DeceptionTacticStrategy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTSOURCE:
				setTacticstrategytacticrefinementsource((DeceptionTacticStrategy)null);
				return;
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTTARGET:
				setTacticstrategytacticrefinementtarget((DeceptionTacticStrategy)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTSOURCE:
				return tacticstrategytacticrefinementsource != null;
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTTARGET:
				return tacticstrategytacticrefinementtarget != null;
		}
		return super.eIsSet(featureID);
	}

} //DeceptionTacticStrategyRefinementImpl
