/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.MitigableElement;
import dmt.Mitigation;
import dmt.MitigationLevelEnum;
import dmt.Mitigator;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mitigation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.MitigationImpl#getMitigationLevel <em>Mitigation Level</em>}</li>
 *   <li>{@link dmt.impl.MitigationImpl#getMitigationTarget <em>Mitigation Target</em>}</li>
 *   <li>{@link dmt.impl.MitigationImpl#getMitigationSource <em>Mitigation Source</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MitigationImpl extends SecurityRelationshipImpl implements Mitigation {
	/**
	 * The default value of the '{@link #getMitigationLevel() <em>Mitigation Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMitigationLevel()
	 * @generated
	 * @ordered
	 */
	protected static final MitigationLevelEnum MITIGATION_LEVEL_EDEFAULT = MitigationLevelEnum.NORMAL_MITIGATION;

	/**
	 * The cached value of the '{@link #getMitigationLevel() <em>Mitigation Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMitigationLevel()
	 * @generated
	 * @ordered
	 */
	protected MitigationLevelEnum mitigationLevel = MITIGATION_LEVEL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getMitigationTarget() <em>Mitigation Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMitigationTarget()
	 * @generated
	 * @ordered
	 */
	protected MitigableElement mitigationTarget;

	/**
	 * The cached value of the '{@link #getMitigationSource() <em>Mitigation Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMitigationSource()
	 * @generated
	 * @ordered
	 */
	protected Mitigator mitigationSource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MitigationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.MITIGATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MitigationLevelEnum getMitigationLevel() {
		return mitigationLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMitigationLevel(MitigationLevelEnum newMitigationLevel) {
		MitigationLevelEnum oldMitigationLevel = mitigationLevel;
		mitigationLevel = newMitigationLevel == null ? MITIGATION_LEVEL_EDEFAULT : newMitigationLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.MITIGATION__MITIGATION_LEVEL, oldMitigationLevel, mitigationLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MitigableElement getMitigationTarget() {
		if (mitigationTarget != null && mitigationTarget.eIsProxy()) {
			InternalEObject oldMitigationTarget = (InternalEObject)mitigationTarget;
			mitigationTarget = (MitigableElement)eResolveProxy(oldMitigationTarget);
			if (mitigationTarget != oldMitigationTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.MITIGATION__MITIGATION_TARGET, oldMitigationTarget, mitigationTarget));
			}
		}
		return mitigationTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MitigableElement basicGetMitigationTarget() {
		return mitigationTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMitigationTarget(MitigableElement newMitigationTarget) {
		MitigableElement oldMitigationTarget = mitigationTarget;
		mitigationTarget = newMitigationTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.MITIGATION__MITIGATION_TARGET, oldMitigationTarget, mitigationTarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mitigator getMitigationSource() {
		if (mitigationSource != null && mitigationSource.eIsProxy()) {
			InternalEObject oldMitigationSource = (InternalEObject)mitigationSource;
			mitigationSource = (Mitigator)eResolveProxy(oldMitigationSource);
			if (mitigationSource != oldMitigationSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.MITIGATION__MITIGATION_SOURCE, oldMitigationSource, mitigationSource));
			}
		}
		return mitigationSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Mitigator basicGetMitigationSource() {
		return mitigationSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMitigationSource(Mitigator newMitigationSource) {
		Mitigator oldMitigationSource = mitigationSource;
		mitigationSource = newMitigationSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.MITIGATION__MITIGATION_SOURCE, oldMitigationSource, mitigationSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.MITIGATION__MITIGATION_LEVEL:
				return getMitigationLevel();
			case DmtPackage.MITIGATION__MITIGATION_TARGET:
				if (resolve) return getMitigationTarget();
				return basicGetMitigationTarget();
			case DmtPackage.MITIGATION__MITIGATION_SOURCE:
				if (resolve) return getMitigationSource();
				return basicGetMitigationSource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.MITIGATION__MITIGATION_LEVEL:
				setMitigationLevel((MitigationLevelEnum)newValue);
				return;
			case DmtPackage.MITIGATION__MITIGATION_TARGET:
				setMitigationTarget((MitigableElement)newValue);
				return;
			case DmtPackage.MITIGATION__MITIGATION_SOURCE:
				setMitigationSource((Mitigator)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.MITIGATION__MITIGATION_LEVEL:
				setMitigationLevel(MITIGATION_LEVEL_EDEFAULT);
				return;
			case DmtPackage.MITIGATION__MITIGATION_TARGET:
				setMitigationTarget((MitigableElement)null);
				return;
			case DmtPackage.MITIGATION__MITIGATION_SOURCE:
				setMitigationSource((Mitigator)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.MITIGATION__MITIGATION_LEVEL:
				return mitigationLevel != MITIGATION_LEVEL_EDEFAULT;
			case DmtPackage.MITIGATION__MITIGATION_TARGET:
				return mitigationTarget != null;
			case DmtPackage.MITIGATION__MITIGATION_SOURCE:
				return mitigationSource != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (mitigationLevel: ");
		result.append(mitigationLevel);
		result.append(')');
		return result.toString();
	}

} //MitigationImpl
