/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.Obstacle;
import dmt.ObstacleCategoryEnum;
import dmt.RiskLikelihoodEnum;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Obstacle</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ObstacleImpl#getObstacleCategory <em>Obstacle Category</em>}</li>
 *   <li>{@link dmt.impl.ObstacleImpl#getFormalDef <em>Formal Def</em>}</li>
 *   <li>{@link dmt.impl.ObstacleImpl#getLikelihood <em>Likelihood</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObstacleImpl extends ObstructionElementImpl implements Obstacle {
	/**
	 * The default value of the '{@link #getObstacleCategory() <em>Obstacle Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObstacleCategory()
	 * @generated
	 * @ordered
	 */
	protected static final ObstacleCategoryEnum OBSTACLE_CATEGORY_EDEFAULT = ObstacleCategoryEnum.INFOUNAVAILABLE;

	/**
	 * The cached value of the '{@link #getObstacleCategory() <em>Obstacle Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObstacleCategory()
	 * @generated
	 * @ordered
	 */
	protected ObstacleCategoryEnum obstacleCategory = OBSTACLE_CATEGORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalDef() <em>Formal Def</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDef()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAL_DEF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormalDef() <em>Formal Def</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDef()
	 * @generated
	 * @ordered
	 */
	protected String formalDef = FORMAL_DEF_EDEFAULT;

	/**
	 * The default value of the '{@link #getLikelihood() <em>Likelihood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLikelihood()
	 * @generated
	 * @ordered
	 */
	protected static final RiskLikelihoodEnum LIKELIHOOD_EDEFAULT = RiskLikelihoodEnum.VERYLOW;

	/**
	 * The cached value of the '{@link #getLikelihood() <em>Likelihood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLikelihood()
	 * @generated
	 * @ordered
	 */
	protected RiskLikelihoodEnum likelihood = LIKELIHOOD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObstacleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.OBSTACLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstacleCategoryEnum getObstacleCategory() {
		return obstacleCategory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObstacleCategory(ObstacleCategoryEnum newObstacleCategory) {
		ObstacleCategoryEnum oldObstacleCategory = obstacleCategory;
		obstacleCategory = newObstacleCategory == null ? OBSTACLE_CATEGORY_EDEFAULT : newObstacleCategory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OBSTACLE__OBSTACLE_CATEGORY, oldObstacleCategory, obstacleCategory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalDef() {
		return formalDef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDef(String newFormalDef) {
		String oldFormalDef = formalDef;
		formalDef = newFormalDef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OBSTACLE__FORMAL_DEF, oldFormalDef, formalDef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskLikelihoodEnum getLikelihood() {
		return likelihood;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLikelihood(RiskLikelihoodEnum newLikelihood) {
		RiskLikelihoodEnum oldLikelihood = likelihood;
		likelihood = newLikelihood == null ? LIKELIHOOD_EDEFAULT : newLikelihood;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OBSTACLE__LIKELIHOOD, oldLikelihood, likelihood));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.OBSTACLE__OBSTACLE_CATEGORY:
				return getObstacleCategory();
			case DmtPackage.OBSTACLE__FORMAL_DEF:
				return getFormalDef();
			case DmtPackage.OBSTACLE__LIKELIHOOD:
				return getLikelihood();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.OBSTACLE__OBSTACLE_CATEGORY:
				setObstacleCategory((ObstacleCategoryEnum)newValue);
				return;
			case DmtPackage.OBSTACLE__FORMAL_DEF:
				setFormalDef((String)newValue);
				return;
			case DmtPackage.OBSTACLE__LIKELIHOOD:
				setLikelihood((RiskLikelihoodEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.OBSTACLE__OBSTACLE_CATEGORY:
				setObstacleCategory(OBSTACLE_CATEGORY_EDEFAULT);
				return;
			case DmtPackage.OBSTACLE__FORMAL_DEF:
				setFormalDef(FORMAL_DEF_EDEFAULT);
				return;
			case DmtPackage.OBSTACLE__LIKELIHOOD:
				setLikelihood(LIKELIHOOD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.OBSTACLE__OBSTACLE_CATEGORY:
				return obstacleCategory != OBSTACLE_CATEGORY_EDEFAULT;
			case DmtPackage.OBSTACLE__FORMAL_DEF:
				return FORMAL_DEF_EDEFAULT == null ? formalDef != null : !FORMAL_DEF_EDEFAULT.equals(formalDef);
			case DmtPackage.OBSTACLE__LIKELIHOOD:
				return likelihood != LIKELIHOOD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (obstacleCategory: ");
		result.append(obstacleCategory);
		result.append(", formalDef: ");
		result.append(formalDef);
		result.append(", likelihood: ");
		result.append(likelihood);
		result.append(')');
		return result.toString();
	}

} //ObstacleImpl
