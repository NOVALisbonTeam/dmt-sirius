/**
 */
package dmt.impl;

import dmt.AgentControl;
import dmt.DmtPackage;
import dmt.InteractionObject;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent Control</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.AgentControlImpl#getObjectAttribute <em>Object Attribute</em>}</li>
 *   <li>{@link dmt.impl.AgentControlImpl#getAgentControl <em>Agent Control</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AgentControlImpl extends AgentRelationImpl implements AgentControl {
	/**
	 * The default value of the '{@link #getObjectAttribute() <em>Object Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final String OBJECT_ATTRIBUTE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getObjectAttribute() <em>Object Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectAttribute()
	 * @generated
	 * @ordered
	 */
	protected String objectAttribute = OBJECT_ATTRIBUTE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAgentControl() <em>Agent Control</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentControl()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionObject> agentControl;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentControlImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.AGENT_CONTROL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getObjectAttribute() {
		return objectAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectAttribute(String newObjectAttribute) {
		String oldObjectAttribute = objectAttribute;
		objectAttribute = newObjectAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.AGENT_CONTROL__OBJECT_ATTRIBUTE, oldObjectAttribute, objectAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionObject> getAgentControl() {
		if (agentControl == null) {
			agentControl = new EObjectResolvingEList<InteractionObject>(InteractionObject.class, this, DmtPackage.AGENT_CONTROL__AGENT_CONTROL);
		}
		return agentControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.AGENT_CONTROL__OBJECT_ATTRIBUTE:
				return getObjectAttribute();
			case DmtPackage.AGENT_CONTROL__AGENT_CONTROL:
				return getAgentControl();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.AGENT_CONTROL__OBJECT_ATTRIBUTE:
				setObjectAttribute((String)newValue);
				return;
			case DmtPackage.AGENT_CONTROL__AGENT_CONTROL:
				getAgentControl().clear();
				getAgentControl().addAll((Collection<? extends InteractionObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.AGENT_CONTROL__OBJECT_ATTRIBUTE:
				setObjectAttribute(OBJECT_ATTRIBUTE_EDEFAULT);
				return;
			case DmtPackage.AGENT_CONTROL__AGENT_CONTROL:
				getAgentControl().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.AGENT_CONTROL__OBJECT_ATTRIBUTE:
				return OBJECT_ATTRIBUTE_EDEFAULT == null ? objectAttribute != null : !OBJECT_ATTRIBUTE_EDEFAULT.equals(objectAttribute);
			case DmtPackage.AGENT_CONTROL__AGENT_CONTROL:
				return agentControl != null && !agentControl.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (objectAttribute: ");
		result.append(objectAttribute);
		result.append(')');
		return result.toString();
	}

} //AgentControlImpl
