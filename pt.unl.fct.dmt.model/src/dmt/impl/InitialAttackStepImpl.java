/**
 */
package dmt.impl;

import dmt.AttackStepConnector;
import dmt.DmtPackage;
import dmt.InitialAttackStep;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Initial Attack Step</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.InitialAttackStepImpl#getHasAttackSteps <em>Has Attack Steps</em>}</li>
 *   <li>{@link dmt.impl.InitialAttackStepImpl#getPreCondition <em>Pre Condition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InitialAttackStepImpl extends AttackStepImpl implements InitialAttackStep {
	/**
	 * The cached value of the '{@link #getHasAttackSteps() <em>Has Attack Steps</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAttackSteps()
	 * @generated
	 * @ordered
	 */
	protected EList<AttackStepConnector> hasAttackSteps;

	/**
	 * The default value of the '{@link #getPreCondition() <em>Pre Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String PRE_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPreCondition() <em>Pre Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreCondition()
	 * @generated
	 * @ordered
	 */
	protected String preCondition = PRE_CONDITION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InitialAttackStepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.INITIAL_ATTACK_STEP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttackStepConnector> getHasAttackSteps() {
		if (hasAttackSteps == null) {
			hasAttackSteps = new EObjectContainmentEList<AttackStepConnector>(AttackStepConnector.class, this, DmtPackage.INITIAL_ATTACK_STEP__HAS_ATTACK_STEPS);
		}
		return hasAttackSteps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPreCondition() {
		return preCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreCondition(String newPreCondition) {
		String oldPreCondition = preCondition;
		preCondition = newPreCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.INITIAL_ATTACK_STEP__PRE_CONDITION, oldPreCondition, preCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DmtPackage.INITIAL_ATTACK_STEP__HAS_ATTACK_STEPS:
				return ((InternalEList<?>)getHasAttackSteps()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.INITIAL_ATTACK_STEP__HAS_ATTACK_STEPS:
				return getHasAttackSteps();
			case DmtPackage.INITIAL_ATTACK_STEP__PRE_CONDITION:
				return getPreCondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.INITIAL_ATTACK_STEP__HAS_ATTACK_STEPS:
				getHasAttackSteps().clear();
				getHasAttackSteps().addAll((Collection<? extends AttackStepConnector>)newValue);
				return;
			case DmtPackage.INITIAL_ATTACK_STEP__PRE_CONDITION:
				setPreCondition((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.INITIAL_ATTACK_STEP__HAS_ATTACK_STEPS:
				getHasAttackSteps().clear();
				return;
			case DmtPackage.INITIAL_ATTACK_STEP__PRE_CONDITION:
				setPreCondition(PRE_CONDITION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.INITIAL_ATTACK_STEP__HAS_ATTACK_STEPS:
				return hasAttackSteps != null && !hasAttackSteps.isEmpty();
			case DmtPackage.INITIAL_ATTACK_STEP__PRE_CONDITION:
				return PRE_CONDITION_EDEFAULT == null ? preCondition != null : !PRE_CONDITION_EDEFAULT.equals(preCondition);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (preCondition: ");
		result.append(preCondition);
		result.append(')');
		return result.toString();
	}

} //InitialAttackStepImpl
