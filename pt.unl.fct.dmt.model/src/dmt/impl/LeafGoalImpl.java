/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.LeafGoal;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Leaf Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class LeafGoalImpl extends GoalImpl implements LeafGoal {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LeafGoalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.LEAF_GOAL;
	}

} //LeafGoalImpl
