/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.ConcreteAsset;
import dmt.DmtPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concrete Asset</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ConcreteAssetImpl#getBusinessImportance <em>Business Importance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConcreteAssetImpl extends AssetImpl implements ConcreteAsset {
	/**
	 * The default value of the '{@link #getBusinessImportance() <em>Business Importance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusinessImportance()
	 * @generated
	 * @ordered
	 */
	protected static final int BUSINESS_IMPORTANCE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBusinessImportance() <em>Business Importance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBusinessImportance()
	 * @generated
	 * @ordered
	 */
	protected int businessImportance = BUSINESS_IMPORTANCE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConcreteAssetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.CONCRETE_ASSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBusinessImportance() {
		return businessImportance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBusinessImportance(int newBusinessImportance) {
		int oldBusinessImportance = businessImportance;
		businessImportance = newBusinessImportance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.CONCRETE_ASSET__BUSINESS_IMPORTANCE, oldBusinessImportance, businessImportance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.CONCRETE_ASSET__BUSINESS_IMPORTANCE:
				return getBusinessImportance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.CONCRETE_ASSET__BUSINESS_IMPORTANCE:
				setBusinessImportance((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.CONCRETE_ASSET__BUSINESS_IMPORTANCE:
				setBusinessImportance(BUSINESS_IMPORTANCE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.CONCRETE_ASSET__BUSINESS_IMPORTANCE:
				return businessImportance != BUSINESS_IMPORTANCE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (businessImportance: ");
		result.append(businessImportance);
		result.append(')');
		return result.toString();
	}

} //ConcreteAssetImpl
