/**
 */
package dmt.impl;

import dmt.BenefitConstraint;
import dmt.DmtPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Benefit Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class BenefitConstraintImpl extends FeatureConstraintImpl implements BenefitConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BenefitConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.BENEFIT_CONSTRAINT;
	}

} //BenefitConstraintImpl
