/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.ConflictGoalRefinement;
import dmt.DmtPackage;
import dmt.Goal;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conflict Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ConflictGoalRefinementImpl#getConflictGoalSource <em>Conflict Goal Source</em>}</li>
 *   <li>{@link dmt.impl.ConflictGoalRefinementImpl#getConflictGoalTarget <em>Conflict Goal Target</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConflictGoalRefinementImpl extends GoalRelationshipImpl implements ConflictGoalRefinement {
	/**
	 * The cached value of the '{@link #getConflictGoalSource() <em>Conflict Goal Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConflictGoalSource()
	 * @generated
	 * @ordered
	 */
	protected Goal conflictGoalSource;

	/**
	 * The cached value of the '{@link #getConflictGoalTarget() <em>Conflict Goal Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConflictGoalTarget()
	 * @generated
	 * @ordered
	 */
	protected Goal conflictGoalTarget;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConflictGoalRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.CONFLICT_GOAL_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Goal getConflictGoalSource() {
		if (conflictGoalSource != null && conflictGoalSource.eIsProxy()) {
			InternalEObject oldConflictGoalSource = (InternalEObject)conflictGoalSource;
			conflictGoalSource = (Goal)eResolveProxy(oldConflictGoalSource);
			if (conflictGoalSource != oldConflictGoalSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_SOURCE, oldConflictGoalSource, conflictGoalSource));
			}
		}
		return conflictGoalSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Goal basicGetConflictGoalSource() {
		return conflictGoalSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConflictGoalSource(Goal newConflictGoalSource) {
		Goal oldConflictGoalSource = conflictGoalSource;
		conflictGoalSource = newConflictGoalSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_SOURCE, oldConflictGoalSource, conflictGoalSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Goal getConflictGoalTarget() {
		if (conflictGoalTarget != null && conflictGoalTarget.eIsProxy()) {
			InternalEObject oldConflictGoalTarget = (InternalEObject)conflictGoalTarget;
			conflictGoalTarget = (Goal)eResolveProxy(oldConflictGoalTarget);
			if (conflictGoalTarget != oldConflictGoalTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_TARGET, oldConflictGoalTarget, conflictGoalTarget));
			}
		}
		return conflictGoalTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Goal basicGetConflictGoalTarget() {
		return conflictGoalTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConflictGoalTarget(Goal newConflictGoalTarget) {
		Goal oldConflictGoalTarget = conflictGoalTarget;
		conflictGoalTarget = newConflictGoalTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_TARGET, oldConflictGoalTarget, conflictGoalTarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_SOURCE:
				if (resolve) return getConflictGoalSource();
				return basicGetConflictGoalSource();
			case DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_TARGET:
				if (resolve) return getConflictGoalTarget();
				return basicGetConflictGoalTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_SOURCE:
				setConflictGoalSource((Goal)newValue);
				return;
			case DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_TARGET:
				setConflictGoalTarget((Goal)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_SOURCE:
				setConflictGoalSource((Goal)null);
				return;
			case DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_TARGET:
				setConflictGoalTarget((Goal)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_SOURCE:
				return conflictGoalSource != null;
			case DmtPackage.CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_TARGET:
				return conflictGoalTarget != null;
		}
		return super.eIsSet(featureID);
	}

} //ConflictGoalRefinementImpl
