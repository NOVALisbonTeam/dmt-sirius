/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.Feature;
import dmt.FeatureRefinement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.FeatureRefinementImpl#getFeaturerefinementtarget <em>Featurerefinementtarget</em>}</li>
 *   <li>{@link dmt.impl.FeatureRefinementImpl#getFeaturerefinementsource <em>Featurerefinementsource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureRefinementImpl extends NodeRefinementImpl implements FeatureRefinement {
	/**
	 * The cached value of the '{@link #getFeaturerefinementtarget() <em>Featurerefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeaturerefinementtarget()
	 * @generated
	 * @ordered
	 */
	protected Feature featurerefinementtarget;

	/**
	 * The cached value of the '{@link #getFeaturerefinementsource() <em>Featurerefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeaturerefinementsource()
	 * @generated
	 * @ordered
	 */
	protected Feature featurerefinementsource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.FEATURE_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature getFeaturerefinementtarget() {
		if (featurerefinementtarget != null && featurerefinementtarget.eIsProxy()) {
			InternalEObject oldFeaturerefinementtarget = (InternalEObject)featurerefinementtarget;
			featurerefinementtarget = (Feature)eResolveProxy(oldFeaturerefinementtarget);
			if (featurerefinementtarget != oldFeaturerefinementtarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTTARGET, oldFeaturerefinementtarget, featurerefinementtarget));
			}
		}
		return featurerefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature basicGetFeaturerefinementtarget() {
		return featurerefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeaturerefinementtarget(Feature newFeaturerefinementtarget) {
		Feature oldFeaturerefinementtarget = featurerefinementtarget;
		featurerefinementtarget = newFeaturerefinementtarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTTARGET, oldFeaturerefinementtarget, featurerefinementtarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature getFeaturerefinementsource() {
		if (featurerefinementsource != null && featurerefinementsource.eIsProxy()) {
			InternalEObject oldFeaturerefinementsource = (InternalEObject)featurerefinementsource;
			featurerefinementsource = (Feature)eResolveProxy(oldFeaturerefinementsource);
			if (featurerefinementsource != oldFeaturerefinementsource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTSOURCE, oldFeaturerefinementsource, featurerefinementsource));
			}
		}
		return featurerefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature basicGetFeaturerefinementsource() {
		return featurerefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeaturerefinementsource(Feature newFeaturerefinementsource) {
		Feature oldFeaturerefinementsource = featurerefinementsource;
		featurerefinementsource = newFeaturerefinementsource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTSOURCE, oldFeaturerefinementsource, featurerefinementsource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTTARGET:
				if (resolve) return getFeaturerefinementtarget();
				return basicGetFeaturerefinementtarget();
			case DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTSOURCE:
				if (resolve) return getFeaturerefinementsource();
				return basicGetFeaturerefinementsource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTTARGET:
				setFeaturerefinementtarget((Feature)newValue);
				return;
			case DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTSOURCE:
				setFeaturerefinementsource((Feature)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTTARGET:
				setFeaturerefinementtarget((Feature)null);
				return;
			case DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTSOURCE:
				setFeaturerefinementsource((Feature)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTTARGET:
				return featurerefinementtarget != null;
			case DmtPackage.FEATURE_REFINEMENT__FEATUREREFINEMENTSOURCE:
				return featurerefinementsource != null;
		}
		return super.eIsSet(featureID);
	}

} //FeatureRefinementImpl
