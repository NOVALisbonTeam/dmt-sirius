/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.MitigableElement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Mitigable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class MitigableElementImpl extends SecurityNodeImpl implements MitigableElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MitigableElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.MITIGABLE_ELEMENT;
	}

} //MitigableElementImpl
