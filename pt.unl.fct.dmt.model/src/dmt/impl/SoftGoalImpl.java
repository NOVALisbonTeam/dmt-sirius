/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.SoftGoal;

import dmt.SoftGoalTypeEnum;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Soft Goal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.SoftGoalImpl#getFitCriterion <em>Fit Criterion</em>}</li>
 *   <li>{@link dmt.impl.SoftGoalImpl#getSoftGoalType <em>Soft Goal Type</em>}</li>
 *   <li>{@link dmt.impl.SoftGoalImpl#getSoftGoalOpenType <em>Soft Goal Open Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SoftGoalImpl extends GoalImpl implements SoftGoal {
	/**
	 * The default value of the '{@link #getFitCriterion() <em>Fit Criterion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFitCriterion()
	 * @generated
	 * @ordered
	 */
	protected static final String FIT_CRITERION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFitCriterion() <em>Fit Criterion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFitCriterion()
	 * @generated
	 * @ordered
	 */
	protected String fitCriterion = FIT_CRITERION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSoftGoalType() <em>Soft Goal Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoftGoalType()
	 * @generated
	 * @ordered
	 */
	protected static final SoftGoalTypeEnum SOFT_GOAL_TYPE_EDEFAULT = SoftGoalTypeEnum.INCREASE;

	/**
	 * The cached value of the '{@link #getSoftGoalType() <em>Soft Goal Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoftGoalType()
	 * @generated
	 * @ordered
	 */
	protected SoftGoalTypeEnum softGoalType = SOFT_GOAL_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSoftGoalOpenType() <em>Soft Goal Open Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoftGoalOpenType()
	 * @generated
	 * @ordered
	 */
	protected static final String SOFT_GOAL_OPEN_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSoftGoalOpenType() <em>Soft Goal Open Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSoftGoalOpenType()
	 * @generated
	 * @ordered
	 */
	protected String softGoalOpenType = SOFT_GOAL_OPEN_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoftGoalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.SOFT_GOAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFitCriterion() {
		return fitCriterion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFitCriterion(String newFitCriterion) {
		String oldFitCriterion = fitCriterion;
		fitCriterion = newFitCriterion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SOFT_GOAL__FIT_CRITERION, oldFitCriterion, fitCriterion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftGoalTypeEnum getSoftGoalType() {
		return softGoalType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoftGoalType(SoftGoalTypeEnum newSoftGoalType) {
		SoftGoalTypeEnum oldSoftGoalType = softGoalType;
		softGoalType = newSoftGoalType == null ? SOFT_GOAL_TYPE_EDEFAULT : newSoftGoalType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SOFT_GOAL__SOFT_GOAL_TYPE, oldSoftGoalType, softGoalType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSoftGoalOpenType() {
		return softGoalOpenType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoftGoalOpenType(String newSoftGoalOpenType) {
		String oldSoftGoalOpenType = softGoalOpenType;
		softGoalOpenType = newSoftGoalOpenType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.SOFT_GOAL__SOFT_GOAL_OPEN_TYPE, oldSoftGoalOpenType, softGoalOpenType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.SOFT_GOAL__FIT_CRITERION:
				return getFitCriterion();
			case DmtPackage.SOFT_GOAL__SOFT_GOAL_TYPE:
				return getSoftGoalType();
			case DmtPackage.SOFT_GOAL__SOFT_GOAL_OPEN_TYPE:
				return getSoftGoalOpenType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.SOFT_GOAL__FIT_CRITERION:
				setFitCriterion((String)newValue);
				return;
			case DmtPackage.SOFT_GOAL__SOFT_GOAL_TYPE:
				setSoftGoalType((SoftGoalTypeEnum)newValue);
				return;
			case DmtPackage.SOFT_GOAL__SOFT_GOAL_OPEN_TYPE:
				setSoftGoalOpenType((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.SOFT_GOAL__FIT_CRITERION:
				setFitCriterion(FIT_CRITERION_EDEFAULT);
				return;
			case DmtPackage.SOFT_GOAL__SOFT_GOAL_TYPE:
				setSoftGoalType(SOFT_GOAL_TYPE_EDEFAULT);
				return;
			case DmtPackage.SOFT_GOAL__SOFT_GOAL_OPEN_TYPE:
				setSoftGoalOpenType(SOFT_GOAL_OPEN_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.SOFT_GOAL__FIT_CRITERION:
				return FIT_CRITERION_EDEFAULT == null ? fitCriterion != null : !FIT_CRITERION_EDEFAULT.equals(fitCriterion);
			case DmtPackage.SOFT_GOAL__SOFT_GOAL_TYPE:
				return softGoalType != SOFT_GOAL_TYPE_EDEFAULT;
			case DmtPackage.SOFT_GOAL__SOFT_GOAL_OPEN_TYPE:
				return SOFT_GOAL_OPEN_TYPE_EDEFAULT == null ? softGoalOpenType != null : !SOFT_GOAL_OPEN_TYPE_EDEFAULT.equals(softGoalOpenType);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (fitCriterion: ");
		result.append(fitCriterion);
		result.append(", softGoalType: ");
		result.append(softGoalType);
		result.append(", softGoalOpenType: ");
		result.append(softGoalOpenType);
		result.append(')');
		return result.toString();
	}

} //SoftGoalImpl
