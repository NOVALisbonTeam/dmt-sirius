/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.ObstructedElement;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Obstructed Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ObstructedElementImpl extends MinimalEObjectImpl.Container implements ObstructedElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObstructedElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.OBSTRUCTED_ELEMENT;
	}

} //ObstructedElementImpl
