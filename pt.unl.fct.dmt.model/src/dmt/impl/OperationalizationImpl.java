/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.LeafGoal;
import dmt.Operationable;
import dmt.Operationalization;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operationalization</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.OperationalizationImpl#getReqPre <em>Req Pre</em>}</li>
 *   <li>{@link dmt.impl.OperationalizationImpl#getReqTrig <em>Req Trig</em>}</li>
 *   <li>{@link dmt.impl.OperationalizationImpl#getReqPost <em>Req Post</em>}</li>
 *   <li>{@link dmt.impl.OperationalizationImpl#getOperationalizeGoal <em>Operationalize Goal</em>}</li>
 *   <li>{@link dmt.impl.OperationalizationImpl#getOperationalization <em>Operationalization</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationalizationImpl extends GoalRelationshipImpl implements Operationalization {
	/**
	 * The default value of the '{@link #getReqPre() <em>Req Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqPre()
	 * @generated
	 * @ordered
	 */
	protected static final String REQ_PRE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReqPre() <em>Req Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqPre()
	 * @generated
	 * @ordered
	 */
	protected String reqPre = REQ_PRE_EDEFAULT;

	/**
	 * The default value of the '{@link #getReqTrig() <em>Req Trig</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqTrig()
	 * @generated
	 * @ordered
	 */
	protected static final String REQ_TRIG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReqTrig() <em>Req Trig</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqTrig()
	 * @generated
	 * @ordered
	 */
	protected String reqTrig = REQ_TRIG_EDEFAULT;

	/**
	 * The default value of the '{@link #getReqPost() <em>Req Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqPost()
	 * @generated
	 * @ordered
	 */
	protected static final String REQ_POST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReqPost() <em>Req Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReqPost()
	 * @generated
	 * @ordered
	 */
	protected String reqPost = REQ_POST_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOperationalizeGoal() <em>Operationalize Goal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationalizeGoal()
	 * @generated
	 * @ordered
	 */
	protected LeafGoal operationalizeGoal;

	/**
	 * The cached value of the '{@link #getOperationalization() <em>Operationalization</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationalization()
	 * @generated
	 * @ordered
	 */
	protected Operationable operationalization;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationalizationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.OPERATIONALIZATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReqPre() {
		return reqPre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReqPre(String newReqPre) {
		String oldReqPre = reqPre;
		reqPre = newReqPre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATIONALIZATION__REQ_PRE, oldReqPre, reqPre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReqTrig() {
		return reqTrig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReqTrig(String newReqTrig) {
		String oldReqTrig = reqTrig;
		reqTrig = newReqTrig;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATIONALIZATION__REQ_TRIG, oldReqTrig, reqTrig));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReqPost() {
		return reqPost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReqPost(String newReqPost) {
		String oldReqPost = reqPost;
		reqPost = newReqPost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATIONALIZATION__REQ_POST, oldReqPost, reqPost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafGoal getOperationalizeGoal() {
		if (operationalizeGoal != null && operationalizeGoal.eIsProxy()) {
			InternalEObject oldOperationalizeGoal = (InternalEObject)operationalizeGoal;
			operationalizeGoal = (LeafGoal)eResolveProxy(oldOperationalizeGoal);
			if (operationalizeGoal != oldOperationalizeGoal) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.OPERATIONALIZATION__OPERATIONALIZE_GOAL, oldOperationalizeGoal, operationalizeGoal));
			}
		}
		return operationalizeGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LeafGoal basicGetOperationalizeGoal() {
		return operationalizeGoal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationalizeGoal(LeafGoal newOperationalizeGoal) {
		LeafGoal oldOperationalizeGoal = operationalizeGoal;
		operationalizeGoal = newOperationalizeGoal;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATIONALIZATION__OPERATIONALIZE_GOAL, oldOperationalizeGoal, operationalizeGoal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operationable getOperationalization() {
		if (operationalization != null && operationalization.eIsProxy()) {
			InternalEObject oldOperationalization = (InternalEObject)operationalization;
			operationalization = (Operationable)eResolveProxy(oldOperationalization);
			if (operationalization != oldOperationalization) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.OPERATIONALIZATION__OPERATIONALIZATION, oldOperationalization, operationalization));
			}
		}
		return operationalization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operationable basicGetOperationalization() {
		return operationalization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationalization(Operationable newOperationalization) {
		Operationable oldOperationalization = operationalization;
		operationalization = newOperationalization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATIONALIZATION__OPERATIONALIZATION, oldOperationalization, operationalization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.OPERATIONALIZATION__REQ_PRE:
				return getReqPre();
			case DmtPackage.OPERATIONALIZATION__REQ_TRIG:
				return getReqTrig();
			case DmtPackage.OPERATIONALIZATION__REQ_POST:
				return getReqPost();
			case DmtPackage.OPERATIONALIZATION__OPERATIONALIZE_GOAL:
				if (resolve) return getOperationalizeGoal();
				return basicGetOperationalizeGoal();
			case DmtPackage.OPERATIONALIZATION__OPERATIONALIZATION:
				if (resolve) return getOperationalization();
				return basicGetOperationalization();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.OPERATIONALIZATION__REQ_PRE:
				setReqPre((String)newValue);
				return;
			case DmtPackage.OPERATIONALIZATION__REQ_TRIG:
				setReqTrig((String)newValue);
				return;
			case DmtPackage.OPERATIONALIZATION__REQ_POST:
				setReqPost((String)newValue);
				return;
			case DmtPackage.OPERATIONALIZATION__OPERATIONALIZE_GOAL:
				setOperationalizeGoal((LeafGoal)newValue);
				return;
			case DmtPackage.OPERATIONALIZATION__OPERATIONALIZATION:
				setOperationalization((Operationable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.OPERATIONALIZATION__REQ_PRE:
				setReqPre(REQ_PRE_EDEFAULT);
				return;
			case DmtPackage.OPERATIONALIZATION__REQ_TRIG:
				setReqTrig(REQ_TRIG_EDEFAULT);
				return;
			case DmtPackage.OPERATIONALIZATION__REQ_POST:
				setReqPost(REQ_POST_EDEFAULT);
				return;
			case DmtPackage.OPERATIONALIZATION__OPERATIONALIZE_GOAL:
				setOperationalizeGoal((LeafGoal)null);
				return;
			case DmtPackage.OPERATIONALIZATION__OPERATIONALIZATION:
				setOperationalization((Operationable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.OPERATIONALIZATION__REQ_PRE:
				return REQ_PRE_EDEFAULT == null ? reqPre != null : !REQ_PRE_EDEFAULT.equals(reqPre);
			case DmtPackage.OPERATIONALIZATION__REQ_TRIG:
				return REQ_TRIG_EDEFAULT == null ? reqTrig != null : !REQ_TRIG_EDEFAULT.equals(reqTrig);
			case DmtPackage.OPERATIONALIZATION__REQ_POST:
				return REQ_POST_EDEFAULT == null ? reqPost != null : !REQ_POST_EDEFAULT.equals(reqPost);
			case DmtPackage.OPERATIONALIZATION__OPERATIONALIZE_GOAL:
				return operationalizeGoal != null;
			case DmtPackage.OPERATIONALIZATION__OPERATIONALIZATION:
				return operationalization != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (reqPre: ");
		result.append(reqPre);
		result.append(", reqTrig: ");
		result.append(reqTrig);
		result.append(", reqPost: ");
		result.append(reqPost);
		result.append(')');
		return result.toString();
	}

} //OperationalizationImpl
