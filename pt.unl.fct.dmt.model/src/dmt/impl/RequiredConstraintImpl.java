/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.RequiredConstraint;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Required Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequiredConstraintImpl extends FeatureConstraintImpl implements RequiredConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequiredConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.REQUIRED_CONSTRAINT;
	}

} //RequiredConstraintImpl
