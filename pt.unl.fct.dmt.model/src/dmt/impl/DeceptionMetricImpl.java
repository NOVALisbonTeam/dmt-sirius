/**
 */
package dmt.impl;

import dmt.DeceptionMetric;
import dmt.DmtPackage;
import dmt.MetricTypeEnum;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Metric</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionMetricImpl#getMetricType <em>Metric Type</em>}</li>
 *   <li>{@link dmt.impl.DeceptionMetricImpl#getUnit <em>Unit</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionMetricImpl extends DeceptionNodeImpl implements DeceptionMetric {
	/**
	 * The default value of the '{@link #getMetricType() <em>Metric Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetricType()
	 * @generated
	 * @ordered
	 */
	protected static final MetricTypeEnum METRIC_TYPE_EDEFAULT = MetricTypeEnum.QUALITATIVE;

	/**
	 * The cached value of the '{@link #getMetricType() <em>Metric Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMetricType()
	 * @generated
	 * @ordered
	 */
	protected MetricTypeEnum metricType = METRIC_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnit() <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnit()
	 * @generated
	 * @ordered
	 */
	protected String unit = UNIT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionMetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_METRIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricTypeEnum getMetricType() {
		return metricType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricType(MetricTypeEnum newMetricType) {
		MetricTypeEnum oldMetricType = metricType;
		metricType = newMetricType == null ? METRIC_TYPE_EDEFAULT : newMetricType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_METRIC__METRIC_TYPE, oldMetricType, metricType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnit(String newUnit) {
		String oldUnit = unit;
		unit = newUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_METRIC__UNIT, oldUnit, unit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_METRIC__METRIC_TYPE:
				return getMetricType();
			case DmtPackage.DECEPTION_METRIC__UNIT:
				return getUnit();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_METRIC__METRIC_TYPE:
				setMetricType((MetricTypeEnum)newValue);
				return;
			case DmtPackage.DECEPTION_METRIC__UNIT:
				setUnit((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_METRIC__METRIC_TYPE:
				setMetricType(METRIC_TYPE_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_METRIC__UNIT:
				setUnit(UNIT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_METRIC__METRIC_TYPE:
				return metricType != METRIC_TYPE_EDEFAULT;
			case DmtPackage.DECEPTION_METRIC__UNIT:
				return UNIT_EDEFAULT == null ? unit != null : !UNIT_EDEFAULT.equals(unit);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (metricType: ");
		result.append(metricType);
		result.append(", unit: ");
		result.append(unit);
		result.append(')');
		return result.toString();
	}

} //DeceptionMetricImpl
