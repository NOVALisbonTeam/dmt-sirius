/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.ORObstructionRefinement;
import dmt.ObstructionElement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OR Obstruction Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ORObstructionRefinementImpl#getORObstructionRefine <em>OR Obstruction Refine</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ORObstructionRefinementImpl extends GoalRelationshipImpl implements ORObstructionRefinement {
	/**
	 * The cached value of the '{@link #getORObstructionRefine() <em>OR Obstruction Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getORObstructionRefine()
	 * @generated
	 * @ordered
	 */
	protected ObstructionElement orObstructionRefine;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ORObstructionRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.OR_OBSTRUCTION_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstructionElement getORObstructionRefine() {
		if (orObstructionRefine != null && orObstructionRefine.eIsProxy()) {
			InternalEObject oldORObstructionRefine = (InternalEObject)orObstructionRefine;
			orObstructionRefine = (ObstructionElement)eResolveProxy(oldORObstructionRefine);
			if (orObstructionRefine != oldORObstructionRefine) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.OR_OBSTRUCTION_REFINEMENT__OR_OBSTRUCTION_REFINE, oldORObstructionRefine, orObstructionRefine));
			}
		}
		return orObstructionRefine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstructionElement basicGetORObstructionRefine() {
		return orObstructionRefine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setORObstructionRefine(ObstructionElement newORObstructionRefine) {
		ObstructionElement oldORObstructionRefine = orObstructionRefine;
		orObstructionRefine = newORObstructionRefine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OR_OBSTRUCTION_REFINEMENT__OR_OBSTRUCTION_REFINE, oldORObstructionRefine, orObstructionRefine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.OR_OBSTRUCTION_REFINEMENT__OR_OBSTRUCTION_REFINE:
				if (resolve) return getORObstructionRefine();
				return basicGetORObstructionRefine();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.OR_OBSTRUCTION_REFINEMENT__OR_OBSTRUCTION_REFINE:
				setORObstructionRefine((ObstructionElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.OR_OBSTRUCTION_REFINEMENT__OR_OBSTRUCTION_REFINE:
				setORObstructionRefine((ObstructionElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.OR_OBSTRUCTION_REFINEMENT__OR_OBSTRUCTION_REFINE:
				return orObstructionRefine != null;
		}
		return super.eIsSet(featureID);
	}

} //ORObstructionRefinementImpl
