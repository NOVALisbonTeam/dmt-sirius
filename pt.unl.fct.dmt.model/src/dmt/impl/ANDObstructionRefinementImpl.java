/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.ANDObstructionRefinement;
import dmt.DmtPackage;
import dmt.ObstructionElement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AND Obstruction Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ANDObstructionRefinementImpl#getANDObstructionRefine <em>AND Obstruction Refine</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ANDObstructionRefinementImpl extends GoalRelationshipImpl implements ANDObstructionRefinement {
	/**
	 * The cached value of the '{@link #getANDObstructionRefine() <em>AND Obstruction Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getANDObstructionRefine()
	 * @generated
	 * @ordered
	 */
	protected ObstructionElement andObstructionRefine;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ANDObstructionRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.AND_OBSTRUCTION_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstructionElement getANDObstructionRefine() {
		if (andObstructionRefine != null && andObstructionRefine.eIsProxy()) {
			InternalEObject oldANDObstructionRefine = (InternalEObject)andObstructionRefine;
			andObstructionRefine = (ObstructionElement)eResolveProxy(oldANDObstructionRefine);
			if (andObstructionRefine != oldANDObstructionRefine) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.AND_OBSTRUCTION_REFINEMENT__AND_OBSTRUCTION_REFINE, oldANDObstructionRefine, andObstructionRefine));
			}
		}
		return andObstructionRefine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObstructionElement basicGetANDObstructionRefine() {
		return andObstructionRefine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setANDObstructionRefine(ObstructionElement newANDObstructionRefine) {
		ObstructionElement oldANDObstructionRefine = andObstructionRefine;
		andObstructionRefine = newANDObstructionRefine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.AND_OBSTRUCTION_REFINEMENT__AND_OBSTRUCTION_REFINE, oldANDObstructionRefine, andObstructionRefine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.AND_OBSTRUCTION_REFINEMENT__AND_OBSTRUCTION_REFINE:
				if (resolve) return getANDObstructionRefine();
				return basicGetANDObstructionRefine();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.AND_OBSTRUCTION_REFINEMENT__AND_OBSTRUCTION_REFINE:
				setANDObstructionRefine((ObstructionElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.AND_OBSTRUCTION_REFINEMENT__AND_OBSTRUCTION_REFINE:
				setANDObstructionRefine((ObstructionElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.AND_OBSTRUCTION_REFINEMENT__AND_OBSTRUCTION_REFINE:
				return andObstructionRefine != null;
		}
		return super.eIsSet(featureID);
	}

} //ANDObstructionRefinementImpl
