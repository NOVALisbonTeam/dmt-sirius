/**
 */
package dmt.impl;

import dmt.Attack;
import dmt.AttackVector;
import dmt.DmtPackage;
import dmt.EndAttackStep;
import dmt.InitialAttackStep;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attack Vector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.AttackVectorImpl#getExample <em>Example</em>}</li>
 *   <li>{@link dmt.impl.AttackVectorImpl#getResources <em>Resources</em>}</li>
 *   <li>{@link dmt.impl.AttackVectorImpl#getRealizesAttack <em>Realizes Attack</em>}</li>
 *   <li>{@link dmt.impl.AttackVectorImpl#getInitialattackstep <em>Initialattackstep</em>}</li>
 *   <li>{@link dmt.impl.AttackVectorImpl#getEndattackstep <em>Endattackstep</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttackVectorImpl extends SecurityNodeImpl implements AttackVector {
	/**
	 * The default value of the '{@link #getExample() <em>Example</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExample()
	 * @generated
	 * @ordered
	 */
	protected static final String EXAMPLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExample() <em>Example</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExample()
	 * @generated
	 * @ordered
	 */
	protected String example = EXAMPLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getResources() <em>Resources</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResources()
	 * @generated
	 * @ordered
	 */
	protected static final String RESOURCES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResources() <em>Resources</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResources()
	 * @generated
	 * @ordered
	 */
	protected String resources = RESOURCES_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRealizesAttack() <em>Realizes Attack</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRealizesAttack()
	 * @generated
	 * @ordered
	 */
	protected Attack realizesAttack;

	/**
	 * The cached value of the '{@link #getInitialattackstep() <em>Initialattackstep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialattackstep()
	 * @generated
	 * @ordered
	 */
	protected InitialAttackStep initialattackstep;

	/**
	 * The cached value of the '{@link #getEndattackstep() <em>Endattackstep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEndattackstep()
	 * @generated
	 * @ordered
	 */
	protected EndAttackStep endattackstep;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttackVectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.ATTACK_VECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExample() {
		return example;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExample(String newExample) {
		String oldExample = example;
		example = newExample;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_VECTOR__EXAMPLE, oldExample, example));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getResources() {
		return resources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResources(String newResources) {
		String oldResources = resources;
		resources = newResources;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_VECTOR__RESOURCES, oldResources, resources));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attack getRealizesAttack() {
		if (realizesAttack != null && realizesAttack.eIsProxy()) {
			InternalEObject oldRealizesAttack = (InternalEObject)realizesAttack;
			realizesAttack = (Attack)eResolveProxy(oldRealizesAttack);
			if (realizesAttack != oldRealizesAttack) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK, oldRealizesAttack, realizesAttack));
			}
		}
		return realizesAttack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attack basicGetRealizesAttack() {
		return realizesAttack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRealizesAttack(Attack newRealizesAttack, NotificationChain msgs) {
		Attack oldRealizesAttack = realizesAttack;
		realizesAttack = newRealizesAttack;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK, oldRealizesAttack, newRealizesAttack);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRealizesAttack(Attack newRealizesAttack) {
		if (newRealizesAttack != realizesAttack) {
			NotificationChain msgs = null;
			if (realizesAttack != null)
				msgs = ((InternalEObject)realizesAttack).eInverseRemove(this, DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR, Attack.class, msgs);
			if (newRealizesAttack != null)
				msgs = ((InternalEObject)newRealizesAttack).eInverseAdd(this, DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR, Attack.class, msgs);
			msgs = basicSetRealizesAttack(newRealizesAttack, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK, newRealizesAttack, newRealizesAttack));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialAttackStep getInitialattackstep() {
		return initialattackstep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitialattackstep(InitialAttackStep newInitialattackstep, NotificationChain msgs) {
		InitialAttackStep oldInitialattackstep = initialattackstep;
		initialattackstep = newInitialattackstep;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_VECTOR__INITIALATTACKSTEP, oldInitialattackstep, newInitialattackstep);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialattackstep(InitialAttackStep newInitialattackstep) {
		if (newInitialattackstep != initialattackstep) {
			NotificationChain msgs = null;
			if (initialattackstep != null)
				msgs = ((InternalEObject)initialattackstep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DmtPackage.ATTACK_VECTOR__INITIALATTACKSTEP, null, msgs);
			if (newInitialattackstep != null)
				msgs = ((InternalEObject)newInitialattackstep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DmtPackage.ATTACK_VECTOR__INITIALATTACKSTEP, null, msgs);
			msgs = basicSetInitialattackstep(newInitialattackstep, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_VECTOR__INITIALATTACKSTEP, newInitialattackstep, newInitialattackstep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EndAttackStep getEndattackstep() {
		return endattackstep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEndattackstep(EndAttackStep newEndattackstep, NotificationChain msgs) {
		EndAttackStep oldEndattackstep = endattackstep;
		endattackstep = newEndattackstep;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_VECTOR__ENDATTACKSTEP, oldEndattackstep, newEndattackstep);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndattackstep(EndAttackStep newEndattackstep) {
		if (newEndattackstep != endattackstep) {
			NotificationChain msgs = null;
			if (endattackstep != null)
				msgs = ((InternalEObject)endattackstep).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DmtPackage.ATTACK_VECTOR__ENDATTACKSTEP, null, msgs);
			if (newEndattackstep != null)
				msgs = ((InternalEObject)newEndattackstep).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DmtPackage.ATTACK_VECTOR__ENDATTACKSTEP, null, msgs);
			msgs = basicSetEndattackstep(newEndattackstep, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_VECTOR__ENDATTACKSTEP, newEndattackstep, newEndattackstep));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK:
				if (realizesAttack != null)
					msgs = ((InternalEObject)realizesAttack).eInverseRemove(this, DmtPackage.ATTACK__REALIZED_BY_ATTACK_VECTOR, Attack.class, msgs);
				return basicSetRealizesAttack((Attack)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK:
				return basicSetRealizesAttack(null, msgs);
			case DmtPackage.ATTACK_VECTOR__INITIALATTACKSTEP:
				return basicSetInitialattackstep(null, msgs);
			case DmtPackage.ATTACK_VECTOR__ENDATTACKSTEP:
				return basicSetEndattackstep(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.ATTACK_VECTOR__EXAMPLE:
				return getExample();
			case DmtPackage.ATTACK_VECTOR__RESOURCES:
				return getResources();
			case DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK:
				if (resolve) return getRealizesAttack();
				return basicGetRealizesAttack();
			case DmtPackage.ATTACK_VECTOR__INITIALATTACKSTEP:
				return getInitialattackstep();
			case DmtPackage.ATTACK_VECTOR__ENDATTACKSTEP:
				return getEndattackstep();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.ATTACK_VECTOR__EXAMPLE:
				setExample((String)newValue);
				return;
			case DmtPackage.ATTACK_VECTOR__RESOURCES:
				setResources((String)newValue);
				return;
			case DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK:
				setRealizesAttack((Attack)newValue);
				return;
			case DmtPackage.ATTACK_VECTOR__INITIALATTACKSTEP:
				setInitialattackstep((InitialAttackStep)newValue);
				return;
			case DmtPackage.ATTACK_VECTOR__ENDATTACKSTEP:
				setEndattackstep((EndAttackStep)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACK_VECTOR__EXAMPLE:
				setExample(EXAMPLE_EDEFAULT);
				return;
			case DmtPackage.ATTACK_VECTOR__RESOURCES:
				setResources(RESOURCES_EDEFAULT);
				return;
			case DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK:
				setRealizesAttack((Attack)null);
				return;
			case DmtPackage.ATTACK_VECTOR__INITIALATTACKSTEP:
				setInitialattackstep((InitialAttackStep)null);
				return;
			case DmtPackage.ATTACK_VECTOR__ENDATTACKSTEP:
				setEndattackstep((EndAttackStep)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACK_VECTOR__EXAMPLE:
				return EXAMPLE_EDEFAULT == null ? example != null : !EXAMPLE_EDEFAULT.equals(example);
			case DmtPackage.ATTACK_VECTOR__RESOURCES:
				return RESOURCES_EDEFAULT == null ? resources != null : !RESOURCES_EDEFAULT.equals(resources);
			case DmtPackage.ATTACK_VECTOR__REALIZES_ATTACK:
				return realizesAttack != null;
			case DmtPackage.ATTACK_VECTOR__INITIALATTACKSTEP:
				return initialattackstep != null;
			case DmtPackage.ATTACK_VECTOR__ENDATTACKSTEP:
				return endattackstep != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (example: ");
		result.append(example);
		result.append(", resources: ");
		result.append(resources);
		result.append(')');
		return result.toString();
	}

} //AttackVectorImpl
