/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DeceptionStrategy;
import dmt.DeceptionStrategyTacticRefinement;
import dmt.DeceptionTacticStrategy;
import dmt.DmtPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Strategy Tactic Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionStrategyTacticRefinementImpl#getStrategytacticrefinementsource <em>Strategytacticrefinementsource</em>}</li>
 *   <li>{@link dmt.impl.DeceptionStrategyTacticRefinementImpl#getStrategytacticrefinementtarget <em>Strategytacticrefinementtarget</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionStrategyTacticRefinementImpl extends NodeRefinementImpl implements DeceptionStrategyTacticRefinement {
	/**
	 * The cached value of the '{@link #getStrategytacticrefinementsource() <em>Strategytacticrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategytacticrefinementsource()
	 * @generated
	 * @ordered
	 */
	protected DeceptionStrategy strategytacticrefinementsource;

	/**
	 * The cached value of the '{@link #getStrategytacticrefinementtarget() <em>Strategytacticrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStrategytacticrefinementtarget()
	 * @generated
	 * @ordered
	 */
	protected DeceptionTacticStrategy strategytacticrefinementtarget;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionStrategyTacticRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_STRATEGY_TACTIC_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategy getStrategytacticrefinementsource() {
		if (strategytacticrefinementsource != null && strategytacticrefinementsource.eIsProxy()) {
			InternalEObject oldStrategytacticrefinementsource = (InternalEObject)strategytacticrefinementsource;
			strategytacticrefinementsource = (DeceptionStrategy)eResolveProxy(oldStrategytacticrefinementsource);
			if (strategytacticrefinementsource != oldStrategytacticrefinementsource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE, oldStrategytacticrefinementsource, strategytacticrefinementsource));
			}
		}
		return strategytacticrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategy basicGetStrategytacticrefinementsource() {
		return strategytacticrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrategytacticrefinementsource(DeceptionStrategy newStrategytacticrefinementsource) {
		DeceptionStrategy oldStrategytacticrefinementsource = strategytacticrefinementsource;
		strategytacticrefinementsource = newStrategytacticrefinementsource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE, oldStrategytacticrefinementsource, strategytacticrefinementsource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticStrategy getStrategytacticrefinementtarget() {
		if (strategytacticrefinementtarget != null && strategytacticrefinementtarget.eIsProxy()) {
			InternalEObject oldStrategytacticrefinementtarget = (InternalEObject)strategytacticrefinementtarget;
			strategytacticrefinementtarget = (DeceptionTacticStrategy)eResolveProxy(oldStrategytacticrefinementtarget);
			if (strategytacticrefinementtarget != oldStrategytacticrefinementtarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET, oldStrategytacticrefinementtarget, strategytacticrefinementtarget));
			}
		}
		return strategytacticrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticStrategy basicGetStrategytacticrefinementtarget() {
		return strategytacticrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStrategytacticrefinementtarget(DeceptionTacticStrategy newStrategytacticrefinementtarget) {
		DeceptionTacticStrategy oldStrategytacticrefinementtarget = strategytacticrefinementtarget;
		strategytacticrefinementtarget = newStrategytacticrefinementtarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET, oldStrategytacticrefinementtarget, strategytacticrefinementtarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE:
				if (resolve) return getStrategytacticrefinementsource();
				return basicGetStrategytacticrefinementsource();
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET:
				if (resolve) return getStrategytacticrefinementtarget();
				return basicGetStrategytacticrefinementtarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE:
				setStrategytacticrefinementsource((DeceptionStrategy)newValue);
				return;
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET:
				setStrategytacticrefinementtarget((DeceptionTacticStrategy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE:
				setStrategytacticrefinementsource((DeceptionStrategy)null);
				return;
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET:
				setStrategytacticrefinementtarget((DeceptionTacticStrategy)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE:
				return strategytacticrefinementsource != null;
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET:
				return strategytacticrefinementtarget != null;
		}
		return super.eIsSet(featureID);
	}

} //DeceptionStrategyTacticRefinementImpl
