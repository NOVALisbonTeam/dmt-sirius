/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DeceptionTacticMechanism;
import dmt.DeceptionTacticStrategy;
import dmt.DmtPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Tactic Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionTacticStrategyImpl#getDeceptiontacticmechanism <em>Deceptiontacticmechanism</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionTacticStrategyImpl extends AbstractFeatureImpl implements DeceptionTacticStrategy {
	/**
	 * The cached value of the '{@link #getDeceptiontacticmechanism() <em>Deceptiontacticmechanism</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeceptiontacticmechanism()
	 * @generated
	 * @ordered
	 */
	protected DeceptionTacticMechanism deceptiontacticmechanism;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionTacticStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_TACTIC_STRATEGY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism getDeceptiontacticmechanism() {
		if (deceptiontacticmechanism != null && deceptiontacticmechanism.eIsProxy()) {
			InternalEObject oldDeceptiontacticmechanism = (InternalEObject)deceptiontacticmechanism;
			deceptiontacticmechanism = (DeceptionTacticMechanism)eResolveProxy(oldDeceptiontacticmechanism);
			if (deceptiontacticmechanism != oldDeceptiontacticmechanism) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.DECEPTION_TACTIC_STRATEGY__DECEPTIONTACTICMECHANISM, oldDeceptiontacticmechanism, deceptiontacticmechanism));
			}
		}
		return deceptiontacticmechanism;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism basicGetDeceptiontacticmechanism() {
		return deceptiontacticmechanism;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeceptiontacticmechanism(DeceptionTacticMechanism newDeceptiontacticmechanism) {
		DeceptionTacticMechanism oldDeceptiontacticmechanism = deceptiontacticmechanism;
		deceptiontacticmechanism = newDeceptiontacticmechanism;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC_STRATEGY__DECEPTIONTACTICMECHANISM, oldDeceptiontacticmechanism, deceptiontacticmechanism));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_STRATEGY__DECEPTIONTACTICMECHANISM:
				if (resolve) return getDeceptiontacticmechanism();
				return basicGetDeceptiontacticmechanism();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_STRATEGY__DECEPTIONTACTICMECHANISM:
				setDeceptiontacticmechanism((DeceptionTacticMechanism)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_STRATEGY__DECEPTIONTACTICMECHANISM:
				setDeceptiontacticmechanism((DeceptionTacticMechanism)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_STRATEGY__DECEPTIONTACTICMECHANISM:
				return deceptiontacticmechanism != null;
		}
		return super.eIsSet(featureID);
	}

} //DeceptionTacticStrategyImpl
