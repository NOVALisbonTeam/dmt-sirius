/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.InfluenceRelationship;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Influence Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class InfluenceRelationshipImpl extends MinimalEObjectImpl.Container implements InfluenceRelationship {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InfluenceRelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.INFLUENCE_RELATIONSHIP;
	}

} //InfluenceRelationshipImpl
