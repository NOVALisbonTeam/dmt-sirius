/**
 */
package dmt.impl;

import dmt.AProperty;
import dmt.ArtificialElement;
import dmt.ArtificialElementTypeEnum;
import dmt.Asset;
import dmt.AssetTypeEnum;
import dmt.DmtPackage;
import dmt.IVulnerability;
import dmt.SecurityNode;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Artificial Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ArtificialElementImpl#getAssetType <em>Asset Type</em>}</li>
 *   <li>{@link dmt.impl.ArtificialElementImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link dmt.impl.ArtificialElementImpl#getHasProperty <em>Has Property</em>}</li>
 *   <li>{@link dmt.impl.ArtificialElementImpl#getDocumentation <em>Documentation</em>}</li>
 *   <li>{@link dmt.impl.ArtificialElementImpl#getUID <em>UID</em>}</li>
 *   <li>{@link dmt.impl.ArtificialElementImpl#getContainVulnerability <em>Contain Vulnerability</em>}</li>
 *   <li>{@link dmt.impl.ArtificialElementImpl#getFakeType <em>Fake Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArtificialElementImpl extends DeceptionNodeImpl implements ArtificialElement {
	/**
	 * The default value of the '{@link #getAssetType() <em>Asset Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetType()
	 * @generated
	 * @ordered
	 */
	protected static final AssetTypeEnum ASSET_TYPE_EDEFAULT = AssetTypeEnum.DATA;

	/**
	 * The cached value of the '{@link #getAssetType() <em>Asset Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssetType()
	 * @generated
	 * @ordered
	 */
	protected AssetTypeEnum assetType = ASSET_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected static final String LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected String location = LOCATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHasProperty() <em>Has Property</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasProperty()
	 * @generated
	 * @ordered
	 */
	protected EList<AProperty> hasProperty;

	/**
	 * The default value of the '{@link #getDocumentation() <em>Documentation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected static final String DOCUMENTATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDocumentation() <em>Documentation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDocumentation()
	 * @generated
	 * @ordered
	 */
	protected String documentation = DOCUMENTATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getUID() <em>UID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUID()
	 * @generated
	 * @ordered
	 */
	protected static final String UID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUID() <em>UID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUID()
	 * @generated
	 * @ordered
	 */
	protected String uid = UID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContainVulnerability() <em>Contain Vulnerability</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainVulnerability()
	 * @generated
	 * @ordered
	 */
	protected EList<IVulnerability> containVulnerability;

	/**
	 * The default value of the '{@link #getFakeType() <em>Fake Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFakeType()
	 * @generated
	 * @ordered
	 */
	protected static final ArtificialElementTypeEnum FAKE_TYPE_EDEFAULT = ArtificialElementTypeEnum.FAKEDATA;

	/**
	 * The cached value of the '{@link #getFakeType() <em>Fake Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFakeType()
	 * @generated
	 * @ordered
	 */
	protected ArtificialElementTypeEnum fakeType = FAKE_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArtificialElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.ARTIFICIAL_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssetTypeEnum getAssetType() {
		return assetType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssetType(AssetTypeEnum newAssetType) {
		AssetTypeEnum oldAssetType = assetType;
		assetType = newAssetType == null ? ASSET_TYPE_EDEFAULT : newAssetType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ARTIFICIAL_ELEMENT__ASSET_TYPE, oldAssetType, assetType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(String newLocation) {
		String oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ARTIFICIAL_ELEMENT__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AProperty> getHasProperty() {
		if (hasProperty == null) {
			hasProperty = new EObjectContainmentEList<AProperty>(AProperty.class, this, DmtPackage.ARTIFICIAL_ELEMENT__HAS_PROPERTY);
		}
		return hasProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDocumentation() {
		return documentation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDocumentation(String newDocumentation) {
		String oldDocumentation = documentation;
		documentation = newDocumentation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ARTIFICIAL_ELEMENT__DOCUMENTATION, oldDocumentation, documentation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUID() {
		return uid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUID(String newUID) {
		String oldUID = uid;
		uid = newUID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ARTIFICIAL_ELEMENT__UID, oldUID, uid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IVulnerability> getContainVulnerability() {
		if (containVulnerability == null) {
			containVulnerability = new EObjectResolvingEList<IVulnerability>(IVulnerability.class, this, DmtPackage.ARTIFICIAL_ELEMENT__CONTAIN_VULNERABILITY);
		}
		return containVulnerability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArtificialElementTypeEnum getFakeType() {
		return fakeType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFakeType(ArtificialElementTypeEnum newFakeType) {
		ArtificialElementTypeEnum oldFakeType = fakeType;
		fakeType = newFakeType == null ? FAKE_TYPE_EDEFAULT : newFakeType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ARTIFICIAL_ELEMENT__FAKE_TYPE, oldFakeType, fakeType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DmtPackage.ARTIFICIAL_ELEMENT__HAS_PROPERTY:
				return ((InternalEList<?>)getHasProperty()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.ARTIFICIAL_ELEMENT__ASSET_TYPE:
				return getAssetType();
			case DmtPackage.ARTIFICIAL_ELEMENT__LOCATION:
				return getLocation();
			case DmtPackage.ARTIFICIAL_ELEMENT__HAS_PROPERTY:
				return getHasProperty();
			case DmtPackage.ARTIFICIAL_ELEMENT__DOCUMENTATION:
				return getDocumentation();
			case DmtPackage.ARTIFICIAL_ELEMENT__UID:
				return getUID();
			case DmtPackage.ARTIFICIAL_ELEMENT__CONTAIN_VULNERABILITY:
				return getContainVulnerability();
			case DmtPackage.ARTIFICIAL_ELEMENT__FAKE_TYPE:
				return getFakeType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.ARTIFICIAL_ELEMENT__ASSET_TYPE:
				setAssetType((AssetTypeEnum)newValue);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__LOCATION:
				setLocation((String)newValue);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__HAS_PROPERTY:
				getHasProperty().clear();
				getHasProperty().addAll((Collection<? extends AProperty>)newValue);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__DOCUMENTATION:
				setDocumentation((String)newValue);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__UID:
				setUID((String)newValue);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__CONTAIN_VULNERABILITY:
				getContainVulnerability().clear();
				getContainVulnerability().addAll((Collection<? extends IVulnerability>)newValue);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__FAKE_TYPE:
				setFakeType((ArtificialElementTypeEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.ARTIFICIAL_ELEMENT__ASSET_TYPE:
				setAssetType(ASSET_TYPE_EDEFAULT);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__LOCATION:
				setLocation(LOCATION_EDEFAULT);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__HAS_PROPERTY:
				getHasProperty().clear();
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__DOCUMENTATION:
				setDocumentation(DOCUMENTATION_EDEFAULT);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__UID:
				setUID(UID_EDEFAULT);
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__CONTAIN_VULNERABILITY:
				getContainVulnerability().clear();
				return;
			case DmtPackage.ARTIFICIAL_ELEMENT__FAKE_TYPE:
				setFakeType(FAKE_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.ARTIFICIAL_ELEMENT__ASSET_TYPE:
				return assetType != ASSET_TYPE_EDEFAULT;
			case DmtPackage.ARTIFICIAL_ELEMENT__LOCATION:
				return LOCATION_EDEFAULT == null ? location != null : !LOCATION_EDEFAULT.equals(location);
			case DmtPackage.ARTIFICIAL_ELEMENT__HAS_PROPERTY:
				return hasProperty != null && !hasProperty.isEmpty();
			case DmtPackage.ARTIFICIAL_ELEMENT__DOCUMENTATION:
				return DOCUMENTATION_EDEFAULT == null ? documentation != null : !DOCUMENTATION_EDEFAULT.equals(documentation);
			case DmtPackage.ARTIFICIAL_ELEMENT__UID:
				return UID_EDEFAULT == null ? uid != null : !UID_EDEFAULT.equals(uid);
			case DmtPackage.ARTIFICIAL_ELEMENT__CONTAIN_VULNERABILITY:
				return containVulnerability != null && !containVulnerability.isEmpty();
			case DmtPackage.ARTIFICIAL_ELEMENT__FAKE_TYPE:
				return fakeType != FAKE_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == SecurityNode.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Asset.class) {
			switch (derivedFeatureID) {
				case DmtPackage.ARTIFICIAL_ELEMENT__ASSET_TYPE: return DmtPackage.ASSET__ASSET_TYPE;
				case DmtPackage.ARTIFICIAL_ELEMENT__LOCATION: return DmtPackage.ASSET__LOCATION;
				case DmtPackage.ARTIFICIAL_ELEMENT__HAS_PROPERTY: return DmtPackage.ASSET__HAS_PROPERTY;
				case DmtPackage.ARTIFICIAL_ELEMENT__DOCUMENTATION: return DmtPackage.ASSET__DOCUMENTATION;
				case DmtPackage.ARTIFICIAL_ELEMENT__UID: return DmtPackage.ASSET__UID;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == SecurityNode.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Asset.class) {
			switch (baseFeatureID) {
				case DmtPackage.ASSET__ASSET_TYPE: return DmtPackage.ARTIFICIAL_ELEMENT__ASSET_TYPE;
				case DmtPackage.ASSET__LOCATION: return DmtPackage.ARTIFICIAL_ELEMENT__LOCATION;
				case DmtPackage.ASSET__HAS_PROPERTY: return DmtPackage.ARTIFICIAL_ELEMENT__HAS_PROPERTY;
				case DmtPackage.ASSET__DOCUMENTATION: return DmtPackage.ARTIFICIAL_ELEMENT__DOCUMENTATION;
				case DmtPackage.ASSET__UID: return DmtPackage.ARTIFICIAL_ELEMENT__UID;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (assetType: ");
		result.append(assetType);
		result.append(", location: ");
		result.append(location);
		result.append(", documentation: ");
		result.append(documentation);
		result.append(", UID: ");
		result.append(uid);
		result.append(", fakeType: ");
		result.append(fakeType);
		result.append(')');
		return result.toString();
	}

} //ArtificialElementImpl
