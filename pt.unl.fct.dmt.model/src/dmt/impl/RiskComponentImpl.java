/**
 */
package dmt.impl;

import dmt.ConcreteAsset;
import dmt.DmtPackage;
import dmt.RiskComponent;
import dmt.RiskLikelihoodEnum;
import dmt.RiskSeverityEnum;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Risk Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.RiskComponentImpl#getRiskSeverity <em>Risk Severity</em>}</li>
 *   <li>{@link dmt.impl.RiskComponentImpl#getRiskLikelihood <em>Risk Likelihood</em>}</li>
 *   <li>{@link dmt.impl.RiskComponentImpl#getHarmsAsset <em>Harms Asset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RiskComponentImpl extends MitigableElementImpl implements RiskComponent {
	/**
	 * The default value of the '{@link #getRiskSeverity() <em>Risk Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRiskSeverity()
	 * @generated
	 * @ordered
	 */
	protected static final RiskSeverityEnum RISK_SEVERITY_EDEFAULT = RiskSeverityEnum.INSIGNIFICANT;

	/**
	 * The cached value of the '{@link #getRiskSeverity() <em>Risk Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRiskSeverity()
	 * @generated
	 * @ordered
	 */
	protected RiskSeverityEnum riskSeverity = RISK_SEVERITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getRiskLikelihood() <em>Risk Likelihood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRiskLikelihood()
	 * @generated
	 * @ordered
	 */
	protected static final RiskLikelihoodEnum RISK_LIKELIHOOD_EDEFAULT = RiskLikelihoodEnum.VERYLOW;

	/**
	 * The cached value of the '{@link #getRiskLikelihood() <em>Risk Likelihood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRiskLikelihood()
	 * @generated
	 * @ordered
	 */
	protected RiskLikelihoodEnum riskLikelihood = RISK_LIKELIHOOD_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHarmsAsset() <em>Harms Asset</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHarmsAsset()
	 * @generated
	 * @ordered
	 */
	protected EList<ConcreteAsset> harmsAsset;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RiskComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.RISK_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskSeverityEnum getRiskSeverity() {
		return riskSeverity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRiskSeverity(RiskSeverityEnum newRiskSeverity) {
		RiskSeverityEnum oldRiskSeverity = riskSeverity;
		riskSeverity = newRiskSeverity == null ? RISK_SEVERITY_EDEFAULT : newRiskSeverity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.RISK_COMPONENT__RISK_SEVERITY, oldRiskSeverity, riskSeverity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RiskLikelihoodEnum getRiskLikelihood() {
		return riskLikelihood;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRiskLikelihood(RiskLikelihoodEnum newRiskLikelihood) {
		RiskLikelihoodEnum oldRiskLikelihood = riskLikelihood;
		riskLikelihood = newRiskLikelihood == null ? RISK_LIKELIHOOD_EDEFAULT : newRiskLikelihood;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.RISK_COMPONENT__RISK_LIKELIHOOD, oldRiskLikelihood, riskLikelihood));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConcreteAsset> getHarmsAsset() {
		if (harmsAsset == null) {
			harmsAsset = new EObjectResolvingEList<ConcreteAsset>(ConcreteAsset.class, this, DmtPackage.RISK_COMPONENT__HARMS_ASSET);
		}
		return harmsAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.RISK_COMPONENT__RISK_SEVERITY:
				return getRiskSeverity();
			case DmtPackage.RISK_COMPONENT__RISK_LIKELIHOOD:
				return getRiskLikelihood();
			case DmtPackage.RISK_COMPONENT__HARMS_ASSET:
				return getHarmsAsset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.RISK_COMPONENT__RISK_SEVERITY:
				setRiskSeverity((RiskSeverityEnum)newValue);
				return;
			case DmtPackage.RISK_COMPONENT__RISK_LIKELIHOOD:
				setRiskLikelihood((RiskLikelihoodEnum)newValue);
				return;
			case DmtPackage.RISK_COMPONENT__HARMS_ASSET:
				getHarmsAsset().clear();
				getHarmsAsset().addAll((Collection<? extends ConcreteAsset>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.RISK_COMPONENT__RISK_SEVERITY:
				setRiskSeverity(RISK_SEVERITY_EDEFAULT);
				return;
			case DmtPackage.RISK_COMPONENT__RISK_LIKELIHOOD:
				setRiskLikelihood(RISK_LIKELIHOOD_EDEFAULT);
				return;
			case DmtPackage.RISK_COMPONENT__HARMS_ASSET:
				getHarmsAsset().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.RISK_COMPONENT__RISK_SEVERITY:
				return riskSeverity != RISK_SEVERITY_EDEFAULT;
			case DmtPackage.RISK_COMPONENT__RISK_LIKELIHOOD:
				return riskLikelihood != RISK_LIKELIHOOD_EDEFAULT;
			case DmtPackage.RISK_COMPONENT__HARMS_ASSET:
				return harmsAsset != null && !harmsAsset.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (riskSeverity: ");
		result.append(riskSeverity);
		result.append(", riskLikelihood: ");
		result.append(riskLikelihood);
		result.append(')');
		return result.toString();
	}

} //RiskComponentImpl
