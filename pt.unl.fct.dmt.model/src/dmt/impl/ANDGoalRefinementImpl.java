/**
 */
package dmt.impl;

import dmt.ANDGoalRefinement;
import dmt.DmtPackage;
import dmt.GoalRefinable;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AND Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ANDGoalRefinementImpl#getAndGoalRefine <em>And Goal Refine</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ANDGoalRefinementImpl extends GoalRelationshipImpl implements ANDGoalRefinement {
	/**
	 * The cached value of the '{@link #getAndGoalRefine() <em>And Goal Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAndGoalRefine()
	 * @generated
	 * @ordered
	 */
	protected GoalRefinable andGoalRefine;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ANDGoalRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.AND_GOAL_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalRefinable getAndGoalRefine() {
		if (andGoalRefine != null && andGoalRefine.eIsProxy()) {
			InternalEObject oldAndGoalRefine = (InternalEObject)andGoalRefine;
			andGoalRefine = (GoalRefinable)eResolveProxy(oldAndGoalRefine);
			if (andGoalRefine != oldAndGoalRefine) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.AND_GOAL_REFINEMENT__AND_GOAL_REFINE, oldAndGoalRefine, andGoalRefine));
			}
		}
		return andGoalRefine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalRefinable basicGetAndGoalRefine() {
		return andGoalRefine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAndGoalRefine(GoalRefinable newAndGoalRefine) {
		GoalRefinable oldAndGoalRefine = andGoalRefine;
		andGoalRefine = newAndGoalRefine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.AND_GOAL_REFINEMENT__AND_GOAL_REFINE, oldAndGoalRefine, andGoalRefine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.AND_GOAL_REFINEMENT__AND_GOAL_REFINE:
				if (resolve) return getAndGoalRefine();
				return basicGetAndGoalRefine();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.AND_GOAL_REFINEMENT__AND_GOAL_REFINE:
				setAndGoalRefine((GoalRefinable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.AND_GOAL_REFINEMENT__AND_GOAL_REFINE:
				setAndGoalRefine((GoalRefinable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.AND_GOAL_REFINEMENT__AND_GOAL_REFINE:
				return andGoalRefine != null;
		}
		return super.eIsSet(featureID);
	}

} //ANDGoalRefinementImpl
