/**
 */
package dmt.impl;

import dmt.AttackStep;
import dmt.DmtPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attack Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AttackStepImpl extends SecurityNodeImpl implements AttackStep {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttackStepImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.ATTACK_STEP;
	}

} //AttackStepImpl
