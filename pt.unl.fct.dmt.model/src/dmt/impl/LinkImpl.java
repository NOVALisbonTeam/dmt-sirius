/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.InteractionObject;
import dmt.Link;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.LinkImpl#getIntegerId <em>Integer Id</em>}</li>
 *   <li>{@link dmt.impl.LinkImpl#getMessage <em>Message</em>}</li>
 *   <li>{@link dmt.impl.LinkImpl#getToObject <em>To Object</em>}</li>
 *   <li>{@link dmt.impl.LinkImpl#getFromObject <em>From Object</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LinkImpl extends InteractionNodesImpl implements Link {
	/**
	 * The default value of the '{@link #getIntegerId() <em>Integer Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerId()
	 * @generated
	 * @ordered
	 */
	protected static final int INTEGER_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getIntegerId() <em>Integer Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerId()
	 * @generated
	 * @ordered
	 */
	protected int integerId = INTEGER_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessage() <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected static final String MESSAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMessage() <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage()
	 * @generated
	 * @ordered
	 */
	protected String message = MESSAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getToObject() <em>To Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToObject()
	 * @generated
	 * @ordered
	 */
	protected InteractionObject toObject;

	/**
	 * The cached value of the '{@link #getFromObject() <em>From Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromObject()
	 * @generated
	 * @ordered
	 */
	protected InteractionObject fromObject;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getIntegerId() {
		return integerId;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntegerId(int newIntegerId) {
		int oldIntegerId = integerId;
		integerId = newIntegerId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.LINK__INTEGER_ID, oldIntegerId, integerId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessage(String newMessage) {
		String oldMessage = message;
		message = newMessage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.LINK__MESSAGE, oldMessage, message));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionObject getToObject() {
		if (toObject != null && toObject.eIsProxy()) {
			InternalEObject oldToObject = (InternalEObject)toObject;
			toObject = (InteractionObject)eResolveProxy(oldToObject);
			if (toObject != oldToObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.LINK__TO_OBJECT, oldToObject, toObject));
			}
		}
		return toObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionObject basicGetToObject() {
		return toObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToObject(InteractionObject newToObject) {
		InteractionObject oldToObject = toObject;
		toObject = newToObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.LINK__TO_OBJECT, oldToObject, toObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionObject getFromObject() {
		if (fromObject != null && fromObject.eIsProxy()) {
			InternalEObject oldFromObject = (InternalEObject)fromObject;
			fromObject = (InteractionObject)eResolveProxy(oldFromObject);
			if (fromObject != oldFromObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.LINK__FROM_OBJECT, oldFromObject, fromObject));
			}
		}
		return fromObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InteractionObject basicGetFromObject() {
		return fromObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromObject(InteractionObject newFromObject) {
		InteractionObject oldFromObject = fromObject;
		fromObject = newFromObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.LINK__FROM_OBJECT, oldFromObject, fromObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.LINK__INTEGER_ID:
				return getIntegerId();
			case DmtPackage.LINK__MESSAGE:
				return getMessage();
			case DmtPackage.LINK__TO_OBJECT:
				if (resolve) return getToObject();
				return basicGetToObject();
			case DmtPackage.LINK__FROM_OBJECT:
				if (resolve) return getFromObject();
				return basicGetFromObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.LINK__INTEGER_ID:
				setIntegerId((Integer)newValue);
				return;
			case DmtPackage.LINK__MESSAGE:
				setMessage((String)newValue);
				return;
			case DmtPackage.LINK__TO_OBJECT:
				setToObject((InteractionObject)newValue);
				return;
			case DmtPackage.LINK__FROM_OBJECT:
				setFromObject((InteractionObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.LINK__INTEGER_ID:
				setIntegerId(INTEGER_ID_EDEFAULT);
				return;
			case DmtPackage.LINK__MESSAGE:
				setMessage(MESSAGE_EDEFAULT);
				return;
			case DmtPackage.LINK__TO_OBJECT:
				setToObject((InteractionObject)null);
				return;
			case DmtPackage.LINK__FROM_OBJECT:
				setFromObject((InteractionObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.LINK__INTEGER_ID:
				return integerId != INTEGER_ID_EDEFAULT;
			case DmtPackage.LINK__MESSAGE:
				return MESSAGE_EDEFAULT == null ? message != null : !MESSAGE_EDEFAULT.equals(message);
			case DmtPackage.LINK__TO_OBJECT:
				return toObject != null;
			case DmtPackage.LINK__FROM_OBJECT:
				return fromObject != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (integerId: ");
		result.append(integerId);
		result.append(", message: ");
		result.append(message);
		result.append(')');
		return result.toString();
	}

} //LinkImpl
