/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.Event;
import dmt.Influence;
import dmt.InfluenceNode;
import dmt.InfluencePolarityEnum;
import dmt.InfluenceWeightEnum;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Influence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.InfluenceImpl#getWeight <em>Weight</em>}</li>
 *   <li>{@link dmt.impl.InfluenceImpl#getPolarity <em>Polarity</em>}</li>
 *   <li>{@link dmt.impl.InfluenceImpl#getInfluenceSource <em>Influence Source</em>}</li>
 *   <li>{@link dmt.impl.InfluenceImpl#getInfluencetarget <em>Influencetarget</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InfluenceImpl extends InfluenceRelationshipImpl implements Influence {
	/**
	 * The default value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected static final InfluenceWeightEnum WEIGHT_EDEFAULT = InfluenceWeightEnum.WEAK;

	/**
	 * The cached value of the '{@link #getWeight() <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWeight()
	 * @generated
	 * @ordered
	 */
	protected InfluenceWeightEnum weight = WEIGHT_EDEFAULT;

	/**
	 * The default value of the '{@link #getPolarity() <em>Polarity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolarity()
	 * @generated
	 * @ordered
	 */
	protected static final InfluencePolarityEnum POLARITY_EDEFAULT = InfluencePolarityEnum.POSITIVE;

	/**
	 * The cached value of the '{@link #getPolarity() <em>Polarity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolarity()
	 * @generated
	 * @ordered
	 */
	protected InfluencePolarityEnum polarity = POLARITY_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInfluenceSource() <em>Influence Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInfluenceSource()
	 * @generated
	 * @ordered
	 */
	protected Event influenceSource;

	/**
	 * The cached value of the '{@link #getInfluencetarget() <em>Influencetarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInfluencetarget()
	 * @generated
	 * @ordered
	 */
	protected InfluenceNode influencetarget;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InfluenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.INFLUENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfluenceWeightEnum getWeight() {
		return weight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWeight(InfluenceWeightEnum newWeight) {
		InfluenceWeightEnum oldWeight = weight;
		weight = newWeight == null ? WEIGHT_EDEFAULT : newWeight;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.INFLUENCE__WEIGHT, oldWeight, weight));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfluencePolarityEnum getPolarity() {
		return polarity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolarity(InfluencePolarityEnum newPolarity) {
		InfluencePolarityEnum oldPolarity = polarity;
		polarity = newPolarity == null ? POLARITY_EDEFAULT : newPolarity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.INFLUENCE__POLARITY, oldPolarity, polarity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event getInfluenceSource() {
		if (influenceSource != null && influenceSource.eIsProxy()) {
			InternalEObject oldInfluenceSource = (InternalEObject)influenceSource;
			influenceSource = (Event)eResolveProxy(oldInfluenceSource);
			if (influenceSource != oldInfluenceSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.INFLUENCE__INFLUENCE_SOURCE, oldInfluenceSource, influenceSource));
			}
		}
		return influenceSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event basicGetInfluenceSource() {
		return influenceSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInfluenceSource(Event newInfluenceSource) {
		Event oldInfluenceSource = influenceSource;
		influenceSource = newInfluenceSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.INFLUENCE__INFLUENCE_SOURCE, oldInfluenceSource, influenceSource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfluenceNode getInfluencetarget() {
		if (influencetarget != null && influencetarget.eIsProxy()) {
			InternalEObject oldInfluencetarget = (InternalEObject)influencetarget;
			influencetarget = (InfluenceNode)eResolveProxy(oldInfluencetarget);
			if (influencetarget != oldInfluencetarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.INFLUENCE__INFLUENCETARGET, oldInfluencetarget, influencetarget));
			}
		}
		return influencetarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InfluenceNode basicGetInfluencetarget() {
		return influencetarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInfluencetarget(InfluenceNode newInfluencetarget) {
		InfluenceNode oldInfluencetarget = influencetarget;
		influencetarget = newInfluencetarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.INFLUENCE__INFLUENCETARGET, oldInfluencetarget, influencetarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.INFLUENCE__WEIGHT:
				return getWeight();
			case DmtPackage.INFLUENCE__POLARITY:
				return getPolarity();
			case DmtPackage.INFLUENCE__INFLUENCE_SOURCE:
				if (resolve) return getInfluenceSource();
				return basicGetInfluenceSource();
			case DmtPackage.INFLUENCE__INFLUENCETARGET:
				if (resolve) return getInfluencetarget();
				return basicGetInfluencetarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.INFLUENCE__WEIGHT:
				setWeight((InfluenceWeightEnum)newValue);
				return;
			case DmtPackage.INFLUENCE__POLARITY:
				setPolarity((InfluencePolarityEnum)newValue);
				return;
			case DmtPackage.INFLUENCE__INFLUENCE_SOURCE:
				setInfluenceSource((Event)newValue);
				return;
			case DmtPackage.INFLUENCE__INFLUENCETARGET:
				setInfluencetarget((InfluenceNode)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.INFLUENCE__WEIGHT:
				setWeight(WEIGHT_EDEFAULT);
				return;
			case DmtPackage.INFLUENCE__POLARITY:
				setPolarity(POLARITY_EDEFAULT);
				return;
			case DmtPackage.INFLUENCE__INFLUENCE_SOURCE:
				setInfluenceSource((Event)null);
				return;
			case DmtPackage.INFLUENCE__INFLUENCETARGET:
				setInfluencetarget((InfluenceNode)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.INFLUENCE__WEIGHT:
				return weight != WEIGHT_EDEFAULT;
			case DmtPackage.INFLUENCE__POLARITY:
				return polarity != POLARITY_EDEFAULT;
			case DmtPackage.INFLUENCE__INFLUENCE_SOURCE:
				return influenceSource != null;
			case DmtPackage.INFLUENCE__INFLUENCETARGET:
				return influencetarget != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (weight: ");
		result.append(weight);
		result.append(", polarity: ");
		result.append(polarity);
		result.append(')');
		return result.toString();
	}

} //InfluenceImpl
