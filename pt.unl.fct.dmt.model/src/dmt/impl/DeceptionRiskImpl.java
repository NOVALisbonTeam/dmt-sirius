/**
 */
package dmt.impl;

import dmt.DeceptionRisk;
import dmt.DeceptionRiskTypeEnum;
import dmt.DmtPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Risk</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionRiskImpl#getDeceptionRiskType <em>Deception Risk Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionRiskImpl extends RiskComponentImpl implements DeceptionRisk {
	/**
	 * The default value of the '{@link #getDeceptionRiskType() <em>Deception Risk Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeceptionRiskType()
	 * @generated
	 * @ordered
	 */
	protected static final DeceptionRiskTypeEnum DECEPTION_RISK_TYPE_EDEFAULT = DeceptionRiskTypeEnum.OBSCURITY;

	/**
	 * The cached value of the '{@link #getDeceptionRiskType() <em>Deception Risk Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeceptionRiskType()
	 * @generated
	 * @ordered
	 */
	protected DeceptionRiskTypeEnum deceptionRiskType = DECEPTION_RISK_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionRiskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_RISK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionRiskTypeEnum getDeceptionRiskType() {
		return deceptionRiskType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeceptionRiskType(DeceptionRiskTypeEnum newDeceptionRiskType) {
		DeceptionRiskTypeEnum oldDeceptionRiskType = deceptionRiskType;
		deceptionRiskType = newDeceptionRiskType == null ? DECEPTION_RISK_TYPE_EDEFAULT : newDeceptionRiskType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_RISK__DECEPTION_RISK_TYPE, oldDeceptionRiskType, deceptionRiskType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_RISK__DECEPTION_RISK_TYPE:
				return getDeceptionRiskType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_RISK__DECEPTION_RISK_TYPE:
				setDeceptionRiskType((DeceptionRiskTypeEnum)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_RISK__DECEPTION_RISK_TYPE:
				setDeceptionRiskType(DECEPTION_RISK_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_RISK__DECEPTION_RISK_TYPE:
				return deceptionRiskType != DECEPTION_RISK_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (deceptionRiskType: ");
		result.append(deceptionRiskType);
		result.append(')');
		return result.toString();
	}

} //DeceptionRiskImpl
