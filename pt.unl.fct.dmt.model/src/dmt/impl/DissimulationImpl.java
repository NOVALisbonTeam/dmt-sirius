/**
 */
package dmt.impl;

import dmt.ConcreteAsset;
import dmt.Dissimulation;
import dmt.DissimulationTechniqueTypeEnum;
import dmt.DmtPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dissimulation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DissimulationImpl#getType <em>Type</em>}</li>
 *   <li>{@link dmt.impl.DissimulationImpl#getHideRealAsset <em>Hide Real Asset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DissimulationImpl extends DeceptionTechniqueImpl implements Dissimulation {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final DissimulationTechniqueTypeEnum TYPE_EDEFAULT = DissimulationTechniqueTypeEnum.MASKING;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected DissimulationTechniqueTypeEnum type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHideRealAsset() <em>Hide Real Asset</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHideRealAsset()
	 * @generated
	 * @ordered
	 */
	protected EList<ConcreteAsset> hideRealAsset;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DissimulationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DISSIMULATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DissimulationTechniqueTypeEnum getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(DissimulationTechniqueTypeEnum newType) {
		DissimulationTechniqueTypeEnum oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DISSIMULATION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConcreteAsset> getHideRealAsset() {
		if (hideRealAsset == null) {
			hideRealAsset = new EObjectResolvingEList<ConcreteAsset>(ConcreteAsset.class, this, DmtPackage.DISSIMULATION__HIDE_REAL_ASSET);
		}
		return hideRealAsset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DISSIMULATION__TYPE:
				return getType();
			case DmtPackage.DISSIMULATION__HIDE_REAL_ASSET:
				return getHideRealAsset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DISSIMULATION__TYPE:
				setType((DissimulationTechniqueTypeEnum)newValue);
				return;
			case DmtPackage.DISSIMULATION__HIDE_REAL_ASSET:
				getHideRealAsset().clear();
				getHideRealAsset().addAll((Collection<? extends ConcreteAsset>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DISSIMULATION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case DmtPackage.DISSIMULATION__HIDE_REAL_ASSET:
				getHideRealAsset().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DISSIMULATION__TYPE:
				return type != TYPE_EDEFAULT;
			case DmtPackage.DISSIMULATION__HIDE_REAL_ASSET:
				return hideRealAsset != null && !hideRealAsset.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //DissimulationImpl
