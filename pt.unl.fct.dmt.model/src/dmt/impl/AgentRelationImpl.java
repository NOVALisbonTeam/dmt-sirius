/**
 */
package dmt.impl;

import dmt.AgentRelation;
import dmt.DmtPackage;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AgentRelationImpl extends GoalRelationshipImpl implements AgentRelation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentRelationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.AGENT_RELATION;
	}

} //AgentRelationImpl
