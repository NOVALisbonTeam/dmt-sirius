/**
 */
package dmt.impl;

import dmt.Annotation;
import dmt.DMTModel;
import dmt.DeceptionNode;
import dmt.DeceptionRelationship;
import dmt.DmtPackage;
import dmt.GoalContainer;
import dmt.GoalNode;
import dmt.GoalRelationship;
import dmt.InfluenceNode;
import dmt.InfluenceRelationship;
import dmt.SecurityNode;
import dmt.SecurityRelationship;
import dmt.StrategyNode;

import dmt.StrategyRelationship;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DMT Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasSecurityNodes <em>Has Security Nodes</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasDeceptionNodes <em>Has Deception Nodes</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasGoalNodes <em>Has Goal Nodes</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasGoalRelations <em>Has Goal Relations</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasStrategyNodes <em>Has Strategy Nodes</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasStrategyRelations <em>Has Strategy Relations</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasDeceptionRelations <em>Has Deception Relations</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasSecurityRelations <em>Has Security Relations</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasInfluenceNodes <em>Has Influence Nodes</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasInfluenceRelations <em>Has Influence Relations</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasGoalContainers <em>Has Goal Containers</em>}</li>
 *   <li>{@link dmt.impl.DMTModelImpl#getHasMiscNodes <em>Has Misc Nodes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DMTModelImpl extends MinimalEObjectImpl.Container implements DMTModel {
	/**
	 * The cached value of the '{@link #getHasSecurityNodes() <em>Has Security Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasSecurityNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<SecurityNode> hasSecurityNodes;

	/**
	 * The cached value of the '{@link #getHasDeceptionNodes() <em>Has Deception Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasDeceptionNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<DeceptionNode> hasDeceptionNodes;

	/**
	 * The cached value of the '{@link #getHasGoalNodes() <em>Has Goal Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasGoalNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<GoalNode> hasGoalNodes;

	/**
	 * The cached value of the '{@link #getHasGoalRelations() <em>Has Goal Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasGoalRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<GoalRelationship> hasGoalRelations;

	/**
	 * The cached value of the '{@link #getHasStrategyNodes() <em>Has Strategy Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasStrategyNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<StrategyNode> hasStrategyNodes;

	/**
	 * The cached value of the '{@link #getHasStrategyRelations() <em>Has Strategy Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasStrategyRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<StrategyRelationship> hasStrategyRelations;

	/**
	 * The cached value of the '{@link #getHasDeceptionRelations() <em>Has Deception Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasDeceptionRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<DeceptionRelationship> hasDeceptionRelations;

	/**
	 * The cached value of the '{@link #getHasSecurityRelations() <em>Has Security Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasSecurityRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<SecurityRelationship> hasSecurityRelations;

	/**
	 * The cached value of the '{@link #getHasInfluenceNodes() <em>Has Influence Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasInfluenceNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<InfluenceNode> hasInfluenceNodes;

	/**
	 * The cached value of the '{@link #getHasInfluenceRelations() <em>Has Influence Relations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasInfluenceRelations()
	 * @generated
	 * @ordered
	 */
	protected EList<InfluenceRelationship> hasInfluenceRelations;

	/**
	 * The cached value of the '{@link #getHasGoalContainers() <em>Has Goal Containers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasGoalContainers()
	 * @generated
	 * @ordered
	 */
	protected EList<GoalContainer> hasGoalContainers;

	/**
	 * The cached value of the '{@link #getHasMiscNodes() <em>Has Misc Nodes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasMiscNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> hasMiscNodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DMTModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DMT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SecurityNode> getHasSecurityNodes() {
		if (hasSecurityNodes == null) {
			hasSecurityNodes = new EObjectContainmentEList<SecurityNode>(SecurityNode.class, this, DmtPackage.DMT_MODEL__HAS_SECURITY_NODES);
		}
		return hasSecurityNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeceptionNode> getHasDeceptionNodes() {
		if (hasDeceptionNodes == null) {
			hasDeceptionNodes = new EObjectContainmentEList<DeceptionNode>(DeceptionNode.class, this, DmtPackage.DMT_MODEL__HAS_DECEPTION_NODES);
		}
		return hasDeceptionNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GoalNode> getHasGoalNodes() {
		if (hasGoalNodes == null) {
			hasGoalNodes = new EObjectContainmentEList<GoalNode>(GoalNode.class, this, DmtPackage.DMT_MODEL__HAS_GOAL_NODES);
		}
		return hasGoalNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GoalRelationship> getHasGoalRelations() {
		if (hasGoalRelations == null) {
			hasGoalRelations = new EObjectContainmentEList<GoalRelationship>(GoalRelationship.class, this, DmtPackage.DMT_MODEL__HAS_GOAL_RELATIONS);
		}
		return hasGoalRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StrategyNode> getHasStrategyNodes() {
		if (hasStrategyNodes == null) {
			hasStrategyNodes = new EObjectContainmentEList<StrategyNode>(StrategyNode.class, this, DmtPackage.DMT_MODEL__HAS_STRATEGY_NODES);
		}
		return hasStrategyNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StrategyRelationship> getHasStrategyRelations() {
		if (hasStrategyRelations == null) {
			hasStrategyRelations = new EObjectContainmentEList<StrategyRelationship>(StrategyRelationship.class, this, DmtPackage.DMT_MODEL__HAS_STRATEGY_RELATIONS);
		}
		return hasStrategyRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeceptionRelationship> getHasDeceptionRelations() {
		if (hasDeceptionRelations == null) {
			hasDeceptionRelations = new EObjectContainmentEList<DeceptionRelationship>(DeceptionRelationship.class, this, DmtPackage.DMT_MODEL__HAS_DECEPTION_RELATIONS);
		}
		return hasDeceptionRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SecurityRelationship> getHasSecurityRelations() {
		if (hasSecurityRelations == null) {
			hasSecurityRelations = new EObjectContainmentEList<SecurityRelationship>(SecurityRelationship.class, this, DmtPackage.DMT_MODEL__HAS_SECURITY_RELATIONS);
		}
		return hasSecurityRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InfluenceNode> getHasInfluenceNodes() {
		if (hasInfluenceNodes == null) {
			hasInfluenceNodes = new EObjectContainmentEList<InfluenceNode>(InfluenceNode.class, this, DmtPackage.DMT_MODEL__HAS_INFLUENCE_NODES);
		}
		return hasInfluenceNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InfluenceRelationship> getHasInfluenceRelations() {
		if (hasInfluenceRelations == null) {
			hasInfluenceRelations = new EObjectContainmentEList<InfluenceRelationship>(InfluenceRelationship.class, this, DmtPackage.DMT_MODEL__HAS_INFLUENCE_RELATIONS);
		}
		return hasInfluenceRelations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<GoalContainer> getHasGoalContainers() {
		if (hasGoalContainers == null) {
			hasGoalContainers = new EObjectContainmentEList<GoalContainer>(GoalContainer.class, this, DmtPackage.DMT_MODEL__HAS_GOAL_CONTAINERS);
		}
		return hasGoalContainers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getHasMiscNodes() {
		if (hasMiscNodes == null) {
			hasMiscNodes = new EObjectContainmentEList<Annotation>(Annotation.class, this, DmtPackage.DMT_MODEL__HAS_MISC_NODES);
		}
		return hasMiscNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DmtPackage.DMT_MODEL__HAS_SECURITY_NODES:
				return ((InternalEList<?>)getHasSecurityNodes()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_NODES:
				return ((InternalEList<?>)getHasDeceptionNodes()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_GOAL_NODES:
				return ((InternalEList<?>)getHasGoalNodes()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_GOAL_RELATIONS:
				return ((InternalEList<?>)getHasGoalRelations()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_NODES:
				return ((InternalEList<?>)getHasStrategyNodes()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_RELATIONS:
				return ((InternalEList<?>)getHasStrategyRelations()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_RELATIONS:
				return ((InternalEList<?>)getHasDeceptionRelations()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_SECURITY_RELATIONS:
				return ((InternalEList<?>)getHasSecurityRelations()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_NODES:
				return ((InternalEList<?>)getHasInfluenceNodes()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_RELATIONS:
				return ((InternalEList<?>)getHasInfluenceRelations()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_GOAL_CONTAINERS:
				return ((InternalEList<?>)getHasGoalContainers()).basicRemove(otherEnd, msgs);
			case DmtPackage.DMT_MODEL__HAS_MISC_NODES:
				return ((InternalEList<?>)getHasMiscNodes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DMT_MODEL__HAS_SECURITY_NODES:
				return getHasSecurityNodes();
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_NODES:
				return getHasDeceptionNodes();
			case DmtPackage.DMT_MODEL__HAS_GOAL_NODES:
				return getHasGoalNodes();
			case DmtPackage.DMT_MODEL__HAS_GOAL_RELATIONS:
				return getHasGoalRelations();
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_NODES:
				return getHasStrategyNodes();
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_RELATIONS:
				return getHasStrategyRelations();
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_RELATIONS:
				return getHasDeceptionRelations();
			case DmtPackage.DMT_MODEL__HAS_SECURITY_RELATIONS:
				return getHasSecurityRelations();
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_NODES:
				return getHasInfluenceNodes();
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_RELATIONS:
				return getHasInfluenceRelations();
			case DmtPackage.DMT_MODEL__HAS_GOAL_CONTAINERS:
				return getHasGoalContainers();
			case DmtPackage.DMT_MODEL__HAS_MISC_NODES:
				return getHasMiscNodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DMT_MODEL__HAS_SECURITY_NODES:
				getHasSecurityNodes().clear();
				getHasSecurityNodes().addAll((Collection<? extends SecurityNode>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_NODES:
				getHasDeceptionNodes().clear();
				getHasDeceptionNodes().addAll((Collection<? extends DeceptionNode>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_GOAL_NODES:
				getHasGoalNodes().clear();
				getHasGoalNodes().addAll((Collection<? extends GoalNode>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_GOAL_RELATIONS:
				getHasGoalRelations().clear();
				getHasGoalRelations().addAll((Collection<? extends GoalRelationship>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_NODES:
				getHasStrategyNodes().clear();
				getHasStrategyNodes().addAll((Collection<? extends StrategyNode>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_RELATIONS:
				getHasStrategyRelations().clear();
				getHasStrategyRelations().addAll((Collection<? extends StrategyRelationship>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_RELATIONS:
				getHasDeceptionRelations().clear();
				getHasDeceptionRelations().addAll((Collection<? extends DeceptionRelationship>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_SECURITY_RELATIONS:
				getHasSecurityRelations().clear();
				getHasSecurityRelations().addAll((Collection<? extends SecurityRelationship>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_NODES:
				getHasInfluenceNodes().clear();
				getHasInfluenceNodes().addAll((Collection<? extends InfluenceNode>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_RELATIONS:
				getHasInfluenceRelations().clear();
				getHasInfluenceRelations().addAll((Collection<? extends InfluenceRelationship>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_GOAL_CONTAINERS:
				getHasGoalContainers().clear();
				getHasGoalContainers().addAll((Collection<? extends GoalContainer>)newValue);
				return;
			case DmtPackage.DMT_MODEL__HAS_MISC_NODES:
				getHasMiscNodes().clear();
				getHasMiscNodes().addAll((Collection<? extends Annotation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DMT_MODEL__HAS_SECURITY_NODES:
				getHasSecurityNodes().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_NODES:
				getHasDeceptionNodes().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_GOAL_NODES:
				getHasGoalNodes().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_GOAL_RELATIONS:
				getHasGoalRelations().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_NODES:
				getHasStrategyNodes().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_RELATIONS:
				getHasStrategyRelations().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_RELATIONS:
				getHasDeceptionRelations().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_SECURITY_RELATIONS:
				getHasSecurityRelations().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_NODES:
				getHasInfluenceNodes().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_RELATIONS:
				getHasInfluenceRelations().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_GOAL_CONTAINERS:
				getHasGoalContainers().clear();
				return;
			case DmtPackage.DMT_MODEL__HAS_MISC_NODES:
				getHasMiscNodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DMT_MODEL__HAS_SECURITY_NODES:
				return hasSecurityNodes != null && !hasSecurityNodes.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_NODES:
				return hasDeceptionNodes != null && !hasDeceptionNodes.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_GOAL_NODES:
				return hasGoalNodes != null && !hasGoalNodes.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_GOAL_RELATIONS:
				return hasGoalRelations != null && !hasGoalRelations.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_NODES:
				return hasStrategyNodes != null && !hasStrategyNodes.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_RELATIONS:
				return hasStrategyRelations != null && !hasStrategyRelations.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_RELATIONS:
				return hasDeceptionRelations != null && !hasDeceptionRelations.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_SECURITY_RELATIONS:
				return hasSecurityRelations != null && !hasSecurityRelations.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_NODES:
				return hasInfluenceNodes != null && !hasInfluenceNodes.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_RELATIONS:
				return hasInfluenceRelations != null && !hasInfluenceRelations.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_GOAL_CONTAINERS:
				return hasGoalContainers != null && !hasGoalContainers.isEmpty();
			case DmtPackage.DMT_MODEL__HAS_MISC_NODES:
				return hasMiscNodes != null && !hasMiscNodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DMTModelImpl
