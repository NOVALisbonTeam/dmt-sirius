/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.Operationable;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operationable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.OperationableImpl#getDomPost <em>Dom Post</em>}</li>
 *   <li>{@link dmt.impl.OperationableImpl#getDomPre <em>Dom Pre</em>}</li>
 *   <li>{@link dmt.impl.OperationableImpl#getFormalDomPost <em>Formal Dom Post</em>}</li>
 *   <li>{@link dmt.impl.OperationableImpl#getFormalDomPre <em>Formal Dom Pre</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class OperationableImpl extends GoalNodeImpl implements Operationable {
	/**
	 * The default value of the '{@link #getDomPost() <em>Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPost()
	 * @generated
	 * @ordered
	 */
	protected static final String DOM_POST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomPost() <em>Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPost()
	 * @generated
	 * @ordered
	 */
	protected String domPost = DOM_POST_EDEFAULT;

	/**
	 * The default value of the '{@link #getDomPre() <em>Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPre()
	 * @generated
	 * @ordered
	 */
	protected static final String DOM_PRE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomPre() <em>Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPre()
	 * @generated
	 * @ordered
	 */
	protected String domPre = DOM_PRE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalDomPost() <em>Formal Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPost()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAL_DOM_POST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormalDomPost() <em>Formal Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPost()
	 * @generated
	 * @ordered
	 */
	protected String formalDomPost = FORMAL_DOM_POST_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalDomPre() <em>Formal Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPre()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAL_DOM_PRE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormalDomPre() <em>Formal Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPre()
	 * @generated
	 * @ordered
	 */
	protected String formalDomPre = FORMAL_DOM_PRE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.OPERATIONABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomPost() {
		return domPost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomPost(String newDomPost) {
		String oldDomPost = domPost;
		domPost = newDomPost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATIONABLE__DOM_POST, oldDomPost, domPost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomPre() {
		return domPre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomPre(String newDomPre) {
		String oldDomPre = domPre;
		domPre = newDomPre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATIONABLE__DOM_PRE, oldDomPre, domPre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalDomPost() {
		return formalDomPost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDomPost(String newFormalDomPost) {
		String oldFormalDomPost = formalDomPost;
		formalDomPost = newFormalDomPost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATIONABLE__FORMAL_DOM_POST, oldFormalDomPost, formalDomPost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalDomPre() {
		return formalDomPre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDomPre(String newFormalDomPre) {
		String oldFormalDomPre = formalDomPre;
		formalDomPre = newFormalDomPre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATIONABLE__FORMAL_DOM_PRE, oldFormalDomPre, formalDomPre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.OPERATIONABLE__DOM_POST:
				return getDomPost();
			case DmtPackage.OPERATIONABLE__DOM_PRE:
				return getDomPre();
			case DmtPackage.OPERATIONABLE__FORMAL_DOM_POST:
				return getFormalDomPost();
			case DmtPackage.OPERATIONABLE__FORMAL_DOM_PRE:
				return getFormalDomPre();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.OPERATIONABLE__DOM_POST:
				setDomPost((String)newValue);
				return;
			case DmtPackage.OPERATIONABLE__DOM_PRE:
				setDomPre((String)newValue);
				return;
			case DmtPackage.OPERATIONABLE__FORMAL_DOM_POST:
				setFormalDomPost((String)newValue);
				return;
			case DmtPackage.OPERATIONABLE__FORMAL_DOM_PRE:
				setFormalDomPre((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.OPERATIONABLE__DOM_POST:
				setDomPost(DOM_POST_EDEFAULT);
				return;
			case DmtPackage.OPERATIONABLE__DOM_PRE:
				setDomPre(DOM_PRE_EDEFAULT);
				return;
			case DmtPackage.OPERATIONABLE__FORMAL_DOM_POST:
				setFormalDomPost(FORMAL_DOM_POST_EDEFAULT);
				return;
			case DmtPackage.OPERATIONABLE__FORMAL_DOM_PRE:
				setFormalDomPre(FORMAL_DOM_PRE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.OPERATIONABLE__DOM_POST:
				return DOM_POST_EDEFAULT == null ? domPost != null : !DOM_POST_EDEFAULT.equals(domPost);
			case DmtPackage.OPERATIONABLE__DOM_PRE:
				return DOM_PRE_EDEFAULT == null ? domPre != null : !DOM_PRE_EDEFAULT.equals(domPre);
			case DmtPackage.OPERATIONABLE__FORMAL_DOM_POST:
				return FORMAL_DOM_POST_EDEFAULT == null ? formalDomPost != null : !FORMAL_DOM_POST_EDEFAULT.equals(formalDomPost);
			case DmtPackage.OPERATIONABLE__FORMAL_DOM_PRE:
				return FORMAL_DOM_PRE_EDEFAULT == null ? formalDomPre != null : !FORMAL_DOM_PRE_EDEFAULT.equals(formalDomPre);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (domPost: ");
		result.append(domPost);
		result.append(", domPre: ");
		result.append(domPre);
		result.append(", formalDomPost: ");
		result.append(formalDomPost);
		result.append(", formalDomPre: ");
		result.append(formalDomPre);
		result.append(')');
		return result.toString();
	}

} //OperationableImpl
