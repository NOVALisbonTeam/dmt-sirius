/**
 */
package dmt.impl;

import dmt.DeceptionTacticMechanism;
import dmt.DmtPackage;
import dmt.Feature;
import dmt.TacticFeatureRefinement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tactic Feature Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.TacticFeatureRefinementImpl#getTacticfeaturerefinementtarget <em>Tacticfeaturerefinementtarget</em>}</li>
 *   <li>{@link dmt.impl.TacticFeatureRefinementImpl#getTacticfeaturerefinementsource <em>Tacticfeaturerefinementsource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TacticFeatureRefinementImpl extends NodeRefinementImpl implements TacticFeatureRefinement {
	/**
	 * The cached value of the '{@link #getTacticfeaturerefinementtarget() <em>Tacticfeaturerefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTacticfeaturerefinementtarget()
	 * @generated
	 * @ordered
	 */
	protected Feature tacticfeaturerefinementtarget;

	/**
	 * The cached value of the '{@link #getTacticfeaturerefinementsource() <em>Tacticfeaturerefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTacticfeaturerefinementsource()
	 * @generated
	 * @ordered
	 */
	protected DeceptionTacticMechanism tacticfeaturerefinementsource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TacticFeatureRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.TACTIC_FEATURE_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature getTacticfeaturerefinementtarget() {
		if (tacticfeaturerefinementtarget != null && tacticfeaturerefinementtarget.eIsProxy()) {
			InternalEObject oldTacticfeaturerefinementtarget = (InternalEObject)tacticfeaturerefinementtarget;
			tacticfeaturerefinementtarget = (Feature)eResolveProxy(oldTacticfeaturerefinementtarget);
			if (tacticfeaturerefinementtarget != oldTacticfeaturerefinementtarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTTARGET, oldTacticfeaturerefinementtarget, tacticfeaturerefinementtarget));
			}
		}
		return tacticfeaturerefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature basicGetTacticfeaturerefinementtarget() {
		return tacticfeaturerefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTacticfeaturerefinementtarget(Feature newTacticfeaturerefinementtarget) {
		Feature oldTacticfeaturerefinementtarget = tacticfeaturerefinementtarget;
		tacticfeaturerefinementtarget = newTacticfeaturerefinementtarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTTARGET, oldTacticfeaturerefinementtarget, tacticfeaturerefinementtarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism getTacticfeaturerefinementsource() {
		if (tacticfeaturerefinementsource != null && tacticfeaturerefinementsource.eIsProxy()) {
			InternalEObject oldTacticfeaturerefinementsource = (InternalEObject)tacticfeaturerefinementsource;
			tacticfeaturerefinementsource = (DeceptionTacticMechanism)eResolveProxy(oldTacticfeaturerefinementsource);
			if (tacticfeaturerefinementsource != oldTacticfeaturerefinementsource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTSOURCE, oldTacticfeaturerefinementsource, tacticfeaturerefinementsource));
			}
		}
		return tacticfeaturerefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticMechanism basicGetTacticfeaturerefinementsource() {
		return tacticfeaturerefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTacticfeaturerefinementsource(DeceptionTacticMechanism newTacticfeaturerefinementsource) {
		DeceptionTacticMechanism oldTacticfeaturerefinementsource = tacticfeaturerefinementsource;
		tacticfeaturerefinementsource = newTacticfeaturerefinementsource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTSOURCE, oldTacticfeaturerefinementsource, tacticfeaturerefinementsource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTTARGET:
				if (resolve) return getTacticfeaturerefinementtarget();
				return basicGetTacticfeaturerefinementtarget();
			case DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTSOURCE:
				if (resolve) return getTacticfeaturerefinementsource();
				return basicGetTacticfeaturerefinementsource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTTARGET:
				setTacticfeaturerefinementtarget((Feature)newValue);
				return;
			case DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTSOURCE:
				setTacticfeaturerefinementsource((DeceptionTacticMechanism)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTTARGET:
				setTacticfeaturerefinementtarget((Feature)null);
				return;
			case DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTSOURCE:
				setTacticfeaturerefinementsource((DeceptionTacticMechanism)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTTARGET:
				return tacticfeaturerefinementtarget != null;
			case DmtPackage.TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTSOURCE:
				return tacticfeaturerefinementsource != null;
		}
		return super.eIsSet(featureID);
	}

} //TacticFeatureRefinementImpl
