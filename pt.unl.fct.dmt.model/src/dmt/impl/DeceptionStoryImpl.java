/**
 */
package dmt.impl;

import dmt.DeceptionBias;
import dmt.DeceptionStory;
import dmt.DmtPackage;
import dmt.StoryTypeEnum;

import interactions.Interaction;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Story</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionStoryImpl#getExternalIdentifier <em>External Identifier</em>}</li>
 *   <li>{@link dmt.impl.DeceptionStoryImpl#getExploitBias <em>Exploit Bias</em>}</li>
 *   <li>{@link dmt.impl.DeceptionStoryImpl#getStoryType <em>Story Type</em>}</li>
 *   <li>{@link dmt.impl.DeceptionStoryImpl#getPreCondition <em>Pre Condition</em>}</li>
 *   <li>{@link dmt.impl.DeceptionStoryImpl#getPosCondition <em>Pos Condition</em>}</li>
 *   <li>{@link dmt.impl.DeceptionStoryImpl#getInteraction <em>Interaction</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionStoryImpl extends DeceptionNodeImpl implements DeceptionStory {
	/**
	 * The default value of the '{@link #getExternalIdentifier() <em>External Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalIdentifier()
	 * @generated
	 * @ordered
	 */
	protected static final String EXTERNAL_IDENTIFIER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExternalIdentifier() <em>External Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExternalIdentifier()
	 * @generated
	 * @ordered
	 */
	protected String externalIdentifier = EXTERNAL_IDENTIFIER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExploitBias() <em>Exploit Bias</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExploitBias()
	 * @generated
	 * @ordered
	 */
	protected EList<DeceptionBias> exploitBias;

	/**
	 * The default value of the '{@link #getStoryType() <em>Story Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStoryType()
	 * @generated
	 * @ordered
	 */
	protected static final StoryTypeEnum STORY_TYPE_EDEFAULT = StoryTypeEnum.ENGAGEMENT;

	/**
	 * The cached value of the '{@link #getStoryType() <em>Story Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStoryType()
	 * @generated
	 * @ordered
	 */
	protected StoryTypeEnum storyType = STORY_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPreCondition() <em>Pre Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String PRE_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPreCondition() <em>Pre Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreCondition()
	 * @generated
	 * @ordered
	 */
	protected String preCondition = PRE_CONDITION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPosCondition() <em>Pos Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String POS_CONDITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPosCondition() <em>Pos Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPosCondition()
	 * @generated
	 * @ordered
	 */
	protected String posCondition = POS_CONDITION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getInteraction() <em>Interaction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInteraction()
	 * @generated
	 * @ordered
	 */
	protected Interaction interaction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionStoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_STORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExternalIdentifier() {
		return externalIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternalIdentifier(String newExternalIdentifier) {
		String oldExternalIdentifier = externalIdentifier;
		externalIdentifier = newExternalIdentifier;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STORY__EXTERNAL_IDENTIFIER, oldExternalIdentifier, externalIdentifier));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeceptionBias> getExploitBias() {
		if (exploitBias == null) {
			exploitBias = new EObjectResolvingEList<DeceptionBias>(DeceptionBias.class, this, DmtPackage.DECEPTION_STORY__EXPLOIT_BIAS);
		}
		return exploitBias;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StoryTypeEnum getStoryType() {
		return storyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStoryType(StoryTypeEnum newStoryType) {
		StoryTypeEnum oldStoryType = storyType;
		storyType = newStoryType == null ? STORY_TYPE_EDEFAULT : newStoryType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STORY__STORY_TYPE, oldStoryType, storyType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPreCondition() {
		return preCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreCondition(String newPreCondition) {
		String oldPreCondition = preCondition;
		preCondition = newPreCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STORY__PRE_CONDITION, oldPreCondition, preCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPosCondition() {
		return posCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPosCondition(String newPosCondition) {
		String oldPosCondition = posCondition;
		posCondition = newPosCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STORY__POS_CONDITION, oldPosCondition, posCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interaction getInteraction() {
		if (interaction != null && interaction.eIsProxy()) {
			InternalEObject oldInteraction = (InternalEObject)interaction;
			interaction = (Interaction)eResolveProxy(oldInteraction);
			if (interaction != oldInteraction) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.DECEPTION_STORY__INTERACTION, oldInteraction, interaction));
			}
		}
		return interaction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interaction basicGetInteraction() {
		return interaction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInteraction(Interaction newInteraction) {
		Interaction oldInteraction = interaction;
		interaction = newInteraction;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STORY__INTERACTION, oldInteraction, interaction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STORY__EXTERNAL_IDENTIFIER:
				return getExternalIdentifier();
			case DmtPackage.DECEPTION_STORY__EXPLOIT_BIAS:
				return getExploitBias();
			case DmtPackage.DECEPTION_STORY__STORY_TYPE:
				return getStoryType();
			case DmtPackage.DECEPTION_STORY__PRE_CONDITION:
				return getPreCondition();
			case DmtPackage.DECEPTION_STORY__POS_CONDITION:
				return getPosCondition();
			case DmtPackage.DECEPTION_STORY__INTERACTION:
				if (resolve) return getInteraction();
				return basicGetInteraction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STORY__EXTERNAL_IDENTIFIER:
				setExternalIdentifier((String)newValue);
				return;
			case DmtPackage.DECEPTION_STORY__EXPLOIT_BIAS:
				getExploitBias().clear();
				getExploitBias().addAll((Collection<? extends DeceptionBias>)newValue);
				return;
			case DmtPackage.DECEPTION_STORY__STORY_TYPE:
				setStoryType((StoryTypeEnum)newValue);
				return;
			case DmtPackage.DECEPTION_STORY__PRE_CONDITION:
				setPreCondition((String)newValue);
				return;
			case DmtPackage.DECEPTION_STORY__POS_CONDITION:
				setPosCondition((String)newValue);
				return;
			case DmtPackage.DECEPTION_STORY__INTERACTION:
				setInteraction((Interaction)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STORY__EXTERNAL_IDENTIFIER:
				setExternalIdentifier(EXTERNAL_IDENTIFIER_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_STORY__EXPLOIT_BIAS:
				getExploitBias().clear();
				return;
			case DmtPackage.DECEPTION_STORY__STORY_TYPE:
				setStoryType(STORY_TYPE_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_STORY__PRE_CONDITION:
				setPreCondition(PRE_CONDITION_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_STORY__POS_CONDITION:
				setPosCondition(POS_CONDITION_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_STORY__INTERACTION:
				setInteraction((Interaction)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STORY__EXTERNAL_IDENTIFIER:
				return EXTERNAL_IDENTIFIER_EDEFAULT == null ? externalIdentifier != null : !EXTERNAL_IDENTIFIER_EDEFAULT.equals(externalIdentifier);
			case DmtPackage.DECEPTION_STORY__EXPLOIT_BIAS:
				return exploitBias != null && !exploitBias.isEmpty();
			case DmtPackage.DECEPTION_STORY__STORY_TYPE:
				return storyType != STORY_TYPE_EDEFAULT;
			case DmtPackage.DECEPTION_STORY__PRE_CONDITION:
				return PRE_CONDITION_EDEFAULT == null ? preCondition != null : !PRE_CONDITION_EDEFAULT.equals(preCondition);
			case DmtPackage.DECEPTION_STORY__POS_CONDITION:
				return POS_CONDITION_EDEFAULT == null ? posCondition != null : !POS_CONDITION_EDEFAULT.equals(posCondition);
			case DmtPackage.DECEPTION_STORY__INTERACTION:
				return interaction != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (externalIdentifier: ");
		result.append(externalIdentifier);
		result.append(", storyType: ");
		result.append(storyType);
		result.append(", preCondition: ");
		result.append(preCondition);
		result.append(", posCondition: ");
		result.append(posCondition);
		result.append(')');
		return result.toString();
	}

} //DeceptionStoryImpl
