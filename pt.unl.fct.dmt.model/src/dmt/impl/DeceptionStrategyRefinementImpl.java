/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DeceptionStrategy;
import dmt.DeceptionStrategyRefinement;
import dmt.DmtPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Strategy Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionStrategyRefinementImpl#getDeceptionstrategyrefinementsource <em>Deceptionstrategyrefinementsource</em>}</li>
 *   <li>{@link dmt.impl.DeceptionStrategyRefinementImpl#getDeceptionstrategyrefinementtarget <em>Deceptionstrategyrefinementtarget</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionStrategyRefinementImpl extends NodeRefinementImpl implements DeceptionStrategyRefinement {
	/**
	 * The cached value of the '{@link #getDeceptionstrategyrefinementsource() <em>Deceptionstrategyrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeceptionstrategyrefinementsource()
	 * @generated
	 * @ordered
	 */
	protected DeceptionStrategy deceptionstrategyrefinementsource;

	/**
	 * The cached value of the '{@link #getDeceptionstrategyrefinementtarget() <em>Deceptionstrategyrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeceptionstrategyrefinementtarget()
	 * @generated
	 * @ordered
	 */
	protected DeceptionStrategy deceptionstrategyrefinementtarget;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionStrategyRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_STRATEGY_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategy getDeceptionstrategyrefinementsource() {
		if (deceptionstrategyrefinementsource != null && deceptionstrategyrefinementsource.eIsProxy()) {
			InternalEObject oldDeceptionstrategyrefinementsource = (InternalEObject)deceptionstrategyrefinementsource;
			deceptionstrategyrefinementsource = (DeceptionStrategy)eResolveProxy(oldDeceptionstrategyrefinementsource);
			if (deceptionstrategyrefinementsource != oldDeceptionstrategyrefinementsource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTSOURCE, oldDeceptionstrategyrefinementsource, deceptionstrategyrefinementsource));
			}
		}
		return deceptionstrategyrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategy basicGetDeceptionstrategyrefinementsource() {
		return deceptionstrategyrefinementsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeceptionstrategyrefinementsource(DeceptionStrategy newDeceptionstrategyrefinementsource) {
		DeceptionStrategy oldDeceptionstrategyrefinementsource = deceptionstrategyrefinementsource;
		deceptionstrategyrefinementsource = newDeceptionstrategyrefinementsource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTSOURCE, oldDeceptionstrategyrefinementsource, deceptionstrategyrefinementsource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategy getDeceptionstrategyrefinementtarget() {
		if (deceptionstrategyrefinementtarget != null && deceptionstrategyrefinementtarget.eIsProxy()) {
			InternalEObject oldDeceptionstrategyrefinementtarget = (InternalEObject)deceptionstrategyrefinementtarget;
			deceptionstrategyrefinementtarget = (DeceptionStrategy)eResolveProxy(oldDeceptionstrategyrefinementtarget);
			if (deceptionstrategyrefinementtarget != oldDeceptionstrategyrefinementtarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTTARGET, oldDeceptionstrategyrefinementtarget, deceptionstrategyrefinementtarget));
			}
		}
		return deceptionstrategyrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionStrategy basicGetDeceptionstrategyrefinementtarget() {
		return deceptionstrategyrefinementtarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeceptionstrategyrefinementtarget(DeceptionStrategy newDeceptionstrategyrefinementtarget) {
		DeceptionStrategy oldDeceptionstrategyrefinementtarget = deceptionstrategyrefinementtarget;
		deceptionstrategyrefinementtarget = newDeceptionstrategyrefinementtarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTTARGET, oldDeceptionstrategyrefinementtarget, deceptionstrategyrefinementtarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTSOURCE:
				if (resolve) return getDeceptionstrategyrefinementsource();
				return basicGetDeceptionstrategyrefinementsource();
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTTARGET:
				if (resolve) return getDeceptionstrategyrefinementtarget();
				return basicGetDeceptionstrategyrefinementtarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTSOURCE:
				setDeceptionstrategyrefinementsource((DeceptionStrategy)newValue);
				return;
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTTARGET:
				setDeceptionstrategyrefinementtarget((DeceptionStrategy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTSOURCE:
				setDeceptionstrategyrefinementsource((DeceptionStrategy)null);
				return;
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTTARGET:
				setDeceptionstrategyrefinementtarget((DeceptionStrategy)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTSOURCE:
				return deceptionstrategyrefinementsource != null;
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTTARGET:
				return deceptionstrategyrefinementtarget != null;
		}
		return super.eIsSet(featureID);
	}

} //DeceptionStrategyRefinementImpl
