/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.SecurityRelationship;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Security Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class SecurityRelationshipImpl extends MinimalEObjectImpl.Container implements SecurityRelationship {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityRelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.SECURITY_RELATIONSHIP;
	}

} //SecurityRelationshipImpl
