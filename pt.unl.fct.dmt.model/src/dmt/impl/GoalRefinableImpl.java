/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.ANDGoalRefinement;
import dmt.DmtPackage;
import dmt.GoalRefinable;
import dmt.ORGoalRefinement;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goal Refinable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.GoalRefinableImpl#getOrGoalRefined <em>Or Goal Refined</em>}</li>
 *   <li>{@link dmt.impl.GoalRefinableImpl#getAndGoalRefined <em>And Goal Refined</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class GoalRefinableImpl extends GoalNodeImpl implements GoalRefinable {
	/**
	 * The cached value of the '{@link #getOrGoalRefined() <em>Or Goal Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrGoalRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ORGoalRefinement> orGoalRefined;

	/**
	 * The cached value of the '{@link #getAndGoalRefined() <em>And Goal Refined</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAndGoalRefined()
	 * @generated
	 * @ordered
	 */
	protected EList<ANDGoalRefinement> andGoalRefined;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalRefinableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.GOAL_REFINABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ORGoalRefinement> getOrGoalRefined() {
		if (orGoalRefined == null) {
			orGoalRefined = new EObjectResolvingEList<ORGoalRefinement>(ORGoalRefinement.class, this, DmtPackage.GOAL_REFINABLE__OR_GOAL_REFINED);
		}
		return orGoalRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ANDGoalRefinement> getAndGoalRefined() {
		if (andGoalRefined == null) {
			andGoalRefined = new EObjectResolvingEList<ANDGoalRefinement>(ANDGoalRefinement.class, this, DmtPackage.GOAL_REFINABLE__AND_GOAL_REFINED);
		}
		return andGoalRefined;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.GOAL_REFINABLE__OR_GOAL_REFINED:
				return getOrGoalRefined();
			case DmtPackage.GOAL_REFINABLE__AND_GOAL_REFINED:
				return getAndGoalRefined();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.GOAL_REFINABLE__OR_GOAL_REFINED:
				getOrGoalRefined().clear();
				getOrGoalRefined().addAll((Collection<? extends ORGoalRefinement>)newValue);
				return;
			case DmtPackage.GOAL_REFINABLE__AND_GOAL_REFINED:
				getAndGoalRefined().clear();
				getAndGoalRefined().addAll((Collection<? extends ANDGoalRefinement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.GOAL_REFINABLE__OR_GOAL_REFINED:
				getOrGoalRefined().clear();
				return;
			case DmtPackage.GOAL_REFINABLE__AND_GOAL_REFINED:
				getAndGoalRefined().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.GOAL_REFINABLE__OR_GOAL_REFINED:
				return orGoalRefined != null && !orGoalRefined.isEmpty();
			case DmtPackage.GOAL_REFINABLE__AND_GOAL_REFINED:
				return andGoalRefined != null && !andGoalRefined.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //GoalRefinableImpl
