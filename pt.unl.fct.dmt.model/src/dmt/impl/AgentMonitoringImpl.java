/**
 */
package dmt.impl;

import dmt.AgentMonitoring;
import dmt.DmtPackage;
import dmt.InteractionObject;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent Monitoring</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.AgentMonitoringImpl#getAgentMonitor <em>Agent Monitor</em>}</li>
 *   <li>{@link dmt.impl.AgentMonitoringImpl#getObjectAttribute <em>Object Attribute</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AgentMonitoringImpl extends AgentRelationImpl implements AgentMonitoring {
	/**
	 * The cached value of the '{@link #getAgentMonitor() <em>Agent Monitor</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgentMonitor()
	 * @generated
	 * @ordered
	 */
	protected EList<InteractionObject> agentMonitor;

	/**
	 * The default value of the '{@link #getObjectAttribute() <em>Object Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final String OBJECT_ATTRIBUTE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getObjectAttribute() <em>Object Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectAttribute()
	 * @generated
	 * @ordered
	 */
	protected String objectAttribute = OBJECT_ATTRIBUTE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentMonitoringImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.AGENT_MONITORING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InteractionObject> getAgentMonitor() {
		if (agentMonitor == null) {
			agentMonitor = new EObjectResolvingEList<InteractionObject>(InteractionObject.class, this, DmtPackage.AGENT_MONITORING__AGENT_MONITOR);
		}
		return agentMonitor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getObjectAttribute() {
		return objectAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectAttribute(String newObjectAttribute) {
		String oldObjectAttribute = objectAttribute;
		objectAttribute = newObjectAttribute;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.AGENT_MONITORING__OBJECT_ATTRIBUTE, oldObjectAttribute, objectAttribute));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.AGENT_MONITORING__AGENT_MONITOR:
				return getAgentMonitor();
			case DmtPackage.AGENT_MONITORING__OBJECT_ATTRIBUTE:
				return getObjectAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.AGENT_MONITORING__AGENT_MONITOR:
				getAgentMonitor().clear();
				getAgentMonitor().addAll((Collection<? extends InteractionObject>)newValue);
				return;
			case DmtPackage.AGENT_MONITORING__OBJECT_ATTRIBUTE:
				setObjectAttribute((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.AGENT_MONITORING__AGENT_MONITOR:
				getAgentMonitor().clear();
				return;
			case DmtPackage.AGENT_MONITORING__OBJECT_ATTRIBUTE:
				setObjectAttribute(OBJECT_ATTRIBUTE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.AGENT_MONITORING__AGENT_MONITOR:
				return agentMonitor != null && !agentMonitor.isEmpty();
			case DmtPackage.AGENT_MONITORING__OBJECT_ATTRIBUTE:
				return OBJECT_ATTRIBUTE_EDEFAULT == null ? objectAttribute != null : !OBJECT_ATTRIBUTE_EDEFAULT.equals(objectAttribute);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (objectAttribute: ");
		result.append(objectAttribute);
		result.append(')');
		return result.toString();
	}

} //AgentMonitoringImpl
