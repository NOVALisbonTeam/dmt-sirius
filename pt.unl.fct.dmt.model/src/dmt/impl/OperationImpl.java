/**
 */
package dmt.impl;

import dmt.Annotation;
import dmt.DmtPackage;
import dmt.GoalNode;
import dmt.Node;
import dmt.Operation;

import dmt.Operationable;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.OperationImpl#getName <em>Name</em>}</li>
 *   <li>{@link dmt.impl.OperationImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link dmt.impl.OperationImpl#getUUID <em>UUID</em>}</li>
 *   <li>{@link dmt.impl.OperationImpl#getHasAnnotations <em>Has Annotations</em>}</li>
 *   <li>{@link dmt.impl.OperationImpl#getDomPost <em>Dom Post</em>}</li>
 *   <li>{@link dmt.impl.OperationImpl#getDomPre <em>Dom Pre</em>}</li>
 *   <li>{@link dmt.impl.OperationImpl#getFormalDomPost <em>Formal Dom Post</em>}</li>
 *   <li>{@link dmt.impl.OperationImpl#getFormalDomPre <em>Formal Dom Pre</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OperationImpl extends VulnerableImpl implements Operation {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getUUID() <em>UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUUID()
	 * @generated
	 * @ordered
	 */
	protected static final String UUID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUUID() <em>UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUUID()
	 * @generated
	 * @ordered
	 */
	protected String uuid = UUID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHasAnnotations() <em>Has Annotations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> hasAnnotations;

	/**
	 * The default value of the '{@link #getDomPost() <em>Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPost()
	 * @generated
	 * @ordered
	 */
	protected static final String DOM_POST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomPost() <em>Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPost()
	 * @generated
	 * @ordered
	 */
	protected String domPost = DOM_POST_EDEFAULT;

	/**
	 * The default value of the '{@link #getDomPre() <em>Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPre()
	 * @generated
	 * @ordered
	 */
	protected static final String DOM_PRE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomPre() <em>Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPre()
	 * @generated
	 * @ordered
	 */
	protected String domPre = DOM_PRE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalDomPost() <em>Formal Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPost()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAL_DOM_POST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormalDomPost() <em>Formal Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPost()
	 * @generated
	 * @ordered
	 */
	protected String formalDomPost = FORMAL_DOM_POST_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalDomPre() <em>Formal Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPre()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAL_DOM_PRE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormalDomPre() <em>Formal Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPre()
	 * @generated
	 * @ordered
	 */
	protected String formalDomPre = FORMAL_DOM_PRE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATION__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUUID() {
		return uuid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUUID(String newUUID) {
		String oldUUID = uuid;
		uuid = newUUID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATION__UUID, oldUUID, uuid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getHasAnnotations() {
		if (hasAnnotations == null) {
			hasAnnotations = new EObjectResolvingEList<Annotation>(Annotation.class, this, DmtPackage.OPERATION__HAS_ANNOTATIONS);
		}
		return hasAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomPost() {
		return domPost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomPost(String newDomPost) {
		String oldDomPost = domPost;
		domPost = newDomPost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATION__DOM_POST, oldDomPost, domPost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalDomPost() {
		return formalDomPost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDomPost(String newFormalDomPost) {
		String oldFormalDomPost = formalDomPost;
		formalDomPost = newFormalDomPost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATION__FORMAL_DOM_POST, oldFormalDomPost, formalDomPost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalDomPre() {
		return formalDomPre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDomPre(String newFormalDomPre) {
		String oldFormalDomPre = formalDomPre;
		formalDomPre = newFormalDomPre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATION__FORMAL_DOM_PRE, oldFormalDomPre, formalDomPre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomPre() {
		return domPre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomPre(String newDomPre) {
		String oldDomPre = domPre;
		domPre = newDomPre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OPERATION__DOM_PRE, oldDomPre, domPre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.OPERATION__NAME:
				return getName();
			case DmtPackage.OPERATION__DESCRIPTION:
				return getDescription();
			case DmtPackage.OPERATION__UUID:
				return getUUID();
			case DmtPackage.OPERATION__HAS_ANNOTATIONS:
				return getHasAnnotations();
			case DmtPackage.OPERATION__DOM_POST:
				return getDomPost();
			case DmtPackage.OPERATION__DOM_PRE:
				return getDomPre();
			case DmtPackage.OPERATION__FORMAL_DOM_POST:
				return getFormalDomPost();
			case DmtPackage.OPERATION__FORMAL_DOM_PRE:
				return getFormalDomPre();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.OPERATION__NAME:
				setName((String)newValue);
				return;
			case DmtPackage.OPERATION__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case DmtPackage.OPERATION__UUID:
				setUUID((String)newValue);
				return;
			case DmtPackage.OPERATION__HAS_ANNOTATIONS:
				getHasAnnotations().clear();
				getHasAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case DmtPackage.OPERATION__DOM_POST:
				setDomPost((String)newValue);
				return;
			case DmtPackage.OPERATION__DOM_PRE:
				setDomPre((String)newValue);
				return;
			case DmtPackage.OPERATION__FORMAL_DOM_POST:
				setFormalDomPost((String)newValue);
				return;
			case DmtPackage.OPERATION__FORMAL_DOM_PRE:
				setFormalDomPre((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.OPERATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DmtPackage.OPERATION__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case DmtPackage.OPERATION__UUID:
				setUUID(UUID_EDEFAULT);
				return;
			case DmtPackage.OPERATION__HAS_ANNOTATIONS:
				getHasAnnotations().clear();
				return;
			case DmtPackage.OPERATION__DOM_POST:
				setDomPost(DOM_POST_EDEFAULT);
				return;
			case DmtPackage.OPERATION__DOM_PRE:
				setDomPre(DOM_PRE_EDEFAULT);
				return;
			case DmtPackage.OPERATION__FORMAL_DOM_POST:
				setFormalDomPost(FORMAL_DOM_POST_EDEFAULT);
				return;
			case DmtPackage.OPERATION__FORMAL_DOM_PRE:
				setFormalDomPre(FORMAL_DOM_PRE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.OPERATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DmtPackage.OPERATION__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case DmtPackage.OPERATION__UUID:
				return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
			case DmtPackage.OPERATION__HAS_ANNOTATIONS:
				return hasAnnotations != null && !hasAnnotations.isEmpty();
			case DmtPackage.OPERATION__DOM_POST:
				return DOM_POST_EDEFAULT == null ? domPost != null : !DOM_POST_EDEFAULT.equals(domPost);
			case DmtPackage.OPERATION__DOM_PRE:
				return DOM_PRE_EDEFAULT == null ? domPre != null : !DOM_PRE_EDEFAULT.equals(domPre);
			case DmtPackage.OPERATION__FORMAL_DOM_POST:
				return FORMAL_DOM_POST_EDEFAULT == null ? formalDomPost != null : !FORMAL_DOM_POST_EDEFAULT.equals(formalDomPost);
			case DmtPackage.OPERATION__FORMAL_DOM_PRE:
				return FORMAL_DOM_PRE_EDEFAULT == null ? formalDomPre != null : !FORMAL_DOM_PRE_EDEFAULT.equals(formalDomPre);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Node.class) {
			switch (derivedFeatureID) {
				case DmtPackage.OPERATION__NAME: return DmtPackage.NODE__NAME;
				case DmtPackage.OPERATION__DESCRIPTION: return DmtPackage.NODE__DESCRIPTION;
				case DmtPackage.OPERATION__UUID: return DmtPackage.NODE__UUID;
				case DmtPackage.OPERATION__HAS_ANNOTATIONS: return DmtPackage.NODE__HAS_ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == GoalNode.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Operationable.class) {
			switch (derivedFeatureID) {
				case DmtPackage.OPERATION__DOM_POST: return DmtPackage.OPERATIONABLE__DOM_POST;
				case DmtPackage.OPERATION__DOM_PRE: return DmtPackage.OPERATIONABLE__DOM_PRE;
				case DmtPackage.OPERATION__FORMAL_DOM_POST: return DmtPackage.OPERATIONABLE__FORMAL_DOM_POST;
				case DmtPackage.OPERATION__FORMAL_DOM_PRE: return DmtPackage.OPERATIONABLE__FORMAL_DOM_PRE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Node.class) {
			switch (baseFeatureID) {
				case DmtPackage.NODE__NAME: return DmtPackage.OPERATION__NAME;
				case DmtPackage.NODE__DESCRIPTION: return DmtPackage.OPERATION__DESCRIPTION;
				case DmtPackage.NODE__UUID: return DmtPackage.OPERATION__UUID;
				case DmtPackage.NODE__HAS_ANNOTATIONS: return DmtPackage.OPERATION__HAS_ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == GoalNode.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Operationable.class) {
			switch (baseFeatureID) {
				case DmtPackage.OPERATIONABLE__DOM_POST: return DmtPackage.OPERATION__DOM_POST;
				case DmtPackage.OPERATIONABLE__DOM_PRE: return DmtPackage.OPERATION__DOM_PRE;
				case DmtPackage.OPERATIONABLE__FORMAL_DOM_POST: return DmtPackage.OPERATION__FORMAL_DOM_POST;
				case DmtPackage.OPERATIONABLE__FORMAL_DOM_PRE: return DmtPackage.OPERATION__FORMAL_DOM_PRE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", UUID: ");
		result.append(uuid);
		result.append(", domPost: ");
		result.append(domPost);
		result.append(", domPre: ");
		result.append(domPre);
		result.append(", formalDomPost: ");
		result.append(formalDomPost);
		result.append(", formalDomPre: ");
		result.append(formalDomPre);
		result.append(')');
		return result.toString();
	}

} //OperationImpl
