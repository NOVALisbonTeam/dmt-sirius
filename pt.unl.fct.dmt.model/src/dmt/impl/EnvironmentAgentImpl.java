/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.EnvironmentAgent;
import dmt.EnvironmentAgentTypeEnum;

import dmt.Expectation;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Environment Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.EnvironmentAgentImpl#getType <em>Type</em>}</li>
 *   <li>{@link dmt.impl.EnvironmentAgentImpl#getResponsibilityEnvironment <em>Responsibility Environment</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnvironmentAgentImpl extends AgentImpl implements EnvironmentAgent {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final EnvironmentAgentTypeEnum TYPE_EDEFAULT = EnvironmentAgentTypeEnum.SOFTWARE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected EnvironmentAgentTypeEnum type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getResponsibilityEnvironment() <em>Responsibility Environment</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibilityEnvironment()
	 * @generated
	 * @ordered
	 */
	protected EList<Expectation> responsibilityEnvironment;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnvironmentAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.ENVIRONMENT_AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentAgentTypeEnum getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(EnvironmentAgentTypeEnum newType) {
		EnvironmentAgentTypeEnum oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ENVIRONMENT_AGENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Expectation> getResponsibilityEnvironment() {
		if (responsibilityEnvironment == null) {
			responsibilityEnvironment = new EObjectResolvingEList<Expectation>(Expectation.class, this, DmtPackage.ENVIRONMENT_AGENT__RESPONSIBILITY_ENVIRONMENT);
		}
		return responsibilityEnvironment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.ENVIRONMENT_AGENT__TYPE:
				return getType();
			case DmtPackage.ENVIRONMENT_AGENT__RESPONSIBILITY_ENVIRONMENT:
				return getResponsibilityEnvironment();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.ENVIRONMENT_AGENT__TYPE:
				setType((EnvironmentAgentTypeEnum)newValue);
				return;
			case DmtPackage.ENVIRONMENT_AGENT__RESPONSIBILITY_ENVIRONMENT:
				getResponsibilityEnvironment().clear();
				getResponsibilityEnvironment().addAll((Collection<? extends Expectation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.ENVIRONMENT_AGENT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case DmtPackage.ENVIRONMENT_AGENT__RESPONSIBILITY_ENVIRONMENT:
				getResponsibilityEnvironment().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.ENVIRONMENT_AGENT__TYPE:
				return type != TYPE_EDEFAULT;
			case DmtPackage.ENVIRONMENT_AGENT__RESPONSIBILITY_ENVIRONMENT:
				return responsibilityEnvironment != null && !responsibilityEnvironment.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //EnvironmentAgentImpl
