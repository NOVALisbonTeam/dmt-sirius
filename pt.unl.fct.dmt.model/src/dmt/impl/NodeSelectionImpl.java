/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.NodeRefinement;
import dmt.NodeSelection;
import dmt.NodesSelectionType;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Node Selection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.NodeSelectionImpl#getSelectionsource <em>Selectionsource</em>}</li>
 *   <li>{@link dmt.impl.NodeSelectionImpl#getSelectiontarget <em>Selectiontarget</em>}</li>
 *   <li>{@link dmt.impl.NodeSelectionImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NodeSelectionImpl extends StrategyRelationshipImpl implements NodeSelection {
	/**
	 * The cached value of the '{@link #getSelectionsource() <em>Selectionsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectionsource()
	 * @generated
	 * @ordered
	 */
	protected NodeRefinement selectionsource;

	/**
	 * The cached value of the '{@link #getSelectiontarget() <em>Selectiontarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectiontarget()
	 * @generated
	 * @ordered
	 */
	protected NodeRefinement selectiontarget;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final NodesSelectionType TYPE_EDEFAULT = NodesSelectionType.OR;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected NodesSelectionType type = TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NodeSelectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.NODE_SELECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeRefinement getSelectionsource() {
		if (selectionsource != null && selectionsource.eIsProxy()) {
			InternalEObject oldSelectionsource = (InternalEObject)selectionsource;
			selectionsource = (NodeRefinement)eResolveProxy(oldSelectionsource);
			if (selectionsource != oldSelectionsource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.NODE_SELECTION__SELECTIONSOURCE, oldSelectionsource, selectionsource));
			}
		}
		return selectionsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeRefinement basicGetSelectionsource() {
		return selectionsource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectionsource(NodeRefinement newSelectionsource) {
		NodeRefinement oldSelectionsource = selectionsource;
		selectionsource = newSelectionsource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.NODE_SELECTION__SELECTIONSOURCE, oldSelectionsource, selectionsource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeRefinement getSelectiontarget() {
		if (selectiontarget != null && selectiontarget.eIsProxy()) {
			InternalEObject oldSelectiontarget = (InternalEObject)selectiontarget;
			selectiontarget = (NodeRefinement)eResolveProxy(oldSelectiontarget);
			if (selectiontarget != oldSelectiontarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.NODE_SELECTION__SELECTIONTARGET, oldSelectiontarget, selectiontarget));
			}
		}
		return selectiontarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodeRefinement basicGetSelectiontarget() {
		return selectiontarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectiontarget(NodeRefinement newSelectiontarget) {
		NodeRefinement oldSelectiontarget = selectiontarget;
		selectiontarget = newSelectiontarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.NODE_SELECTION__SELECTIONTARGET, oldSelectiontarget, selectiontarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NodesSelectionType getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(NodesSelectionType newType) {
		NodesSelectionType oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.NODE_SELECTION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.NODE_SELECTION__SELECTIONSOURCE:
				if (resolve) return getSelectionsource();
				return basicGetSelectionsource();
			case DmtPackage.NODE_SELECTION__SELECTIONTARGET:
				if (resolve) return getSelectiontarget();
				return basicGetSelectiontarget();
			case DmtPackage.NODE_SELECTION__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.NODE_SELECTION__SELECTIONSOURCE:
				setSelectionsource((NodeRefinement)newValue);
				return;
			case DmtPackage.NODE_SELECTION__SELECTIONTARGET:
				setSelectiontarget((NodeRefinement)newValue);
				return;
			case DmtPackage.NODE_SELECTION__TYPE:
				setType((NodesSelectionType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.NODE_SELECTION__SELECTIONSOURCE:
				setSelectionsource((NodeRefinement)null);
				return;
			case DmtPackage.NODE_SELECTION__SELECTIONTARGET:
				setSelectiontarget((NodeRefinement)null);
				return;
			case DmtPackage.NODE_SELECTION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.NODE_SELECTION__SELECTIONSOURCE:
				return selectionsource != null;
			case DmtPackage.NODE_SELECTION__SELECTIONTARGET:
				return selectiontarget != null;
			case DmtPackage.NODE_SELECTION__TYPE:
				return type != TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //NodeSelectionImpl
