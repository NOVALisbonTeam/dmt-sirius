/**
 */
package dmt.impl;

import dmt.Agent;
import dmt.DmtPackage;
import dmt.Operationable;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.AgentImpl#getDef <em>Def</em>}</li>
 *   <li>{@link dmt.impl.AgentImpl#getLoad <em>Load</em>}</li>
 *   <li>{@link dmt.impl.AgentImpl#getSubagent <em>Subagent</em>}</li>
 *   <li>{@link dmt.impl.AgentImpl#getPerformsOperation <em>Performs Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AgentImpl extends GoalNodeImpl implements Agent {
	/**
	 * The default value of the '{@link #getDef() <em>Def</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDef()
	 * @generated
	 * @ordered
	 */
	protected static final String DEF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDef() <em>Def</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDef()
	 * @generated
	 * @ordered
	 */
	protected String def = DEF_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoad() <em>Load</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoad()
	 * @generated
	 * @ordered
	 */
	protected static final String LOAD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoad() <em>Load</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoad()
	 * @generated
	 * @ordered
	 */
	protected String load = LOAD_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubagent() <em>Subagent</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubagent()
	 * @generated
	 * @ordered
	 */
	protected EList<Agent> subagent;

	/**
	 * The cached value of the '{@link #getPerformsOperation() <em>Performs Operation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPerformsOperation()
	 * @generated
	 * @ordered
	 */
	protected EList<Operationable> performsOperation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDef() {
		return def;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDef(String newDef) {
		String oldDef = def;
		def = newDef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.AGENT__DEF, oldDef, def));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLoad() {
		return load;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoad(String newLoad) {
		String oldLoad = load;
		load = newLoad;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.AGENT__LOAD, oldLoad, load));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Agent> getSubagent() {
		if (subagent == null) {
			subagent = new EObjectContainmentEList<Agent>(Agent.class, this, DmtPackage.AGENT__SUBAGENT);
		}
		return subagent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operationable> getPerformsOperation() {
		if (performsOperation == null) {
			performsOperation = new EObjectResolvingEList<Operationable>(Operationable.class, this, DmtPackage.AGENT__PERFORMS_OPERATION);
		}
		return performsOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DmtPackage.AGENT__SUBAGENT:
				return ((InternalEList<?>)getSubagent()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.AGENT__DEF:
				return getDef();
			case DmtPackage.AGENT__LOAD:
				return getLoad();
			case DmtPackage.AGENT__SUBAGENT:
				return getSubagent();
			case DmtPackage.AGENT__PERFORMS_OPERATION:
				return getPerformsOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.AGENT__DEF:
				setDef((String)newValue);
				return;
			case DmtPackage.AGENT__LOAD:
				setLoad((String)newValue);
				return;
			case DmtPackage.AGENT__SUBAGENT:
				getSubagent().clear();
				getSubagent().addAll((Collection<? extends Agent>)newValue);
				return;
			case DmtPackage.AGENT__PERFORMS_OPERATION:
				getPerformsOperation().clear();
				getPerformsOperation().addAll((Collection<? extends Operationable>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.AGENT__DEF:
				setDef(DEF_EDEFAULT);
				return;
			case DmtPackage.AGENT__LOAD:
				setLoad(LOAD_EDEFAULT);
				return;
			case DmtPackage.AGENT__SUBAGENT:
				getSubagent().clear();
				return;
			case DmtPackage.AGENT__PERFORMS_OPERATION:
				getPerformsOperation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.AGENT__DEF:
				return DEF_EDEFAULT == null ? def != null : !DEF_EDEFAULT.equals(def);
			case DmtPackage.AGENT__LOAD:
				return LOAD_EDEFAULT == null ? load != null : !LOAD_EDEFAULT.equals(load);
			case DmtPackage.AGENT__SUBAGENT:
				return subagent != null && !subagent.isEmpty();
			case DmtPackage.AGENT__PERFORMS_OPERATION:
				return performsOperation != null && !performsOperation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (def: ");
		result.append(def);
		result.append(", load: ");
		result.append(load);
		result.append(')');
		return result.toString();
	}

} //AgentImpl
