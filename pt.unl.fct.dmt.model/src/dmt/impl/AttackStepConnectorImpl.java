/**
 */
package dmt.impl;

import dmt.AttackConnectorEnum;
import dmt.AttackStepConnector;
import dmt.DmtPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attack Step Connector</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.AttackStepConnectorImpl#getSubattackstep <em>Subattackstep</em>}</li>
 *   <li>{@link dmt.impl.AttackStepConnectorImpl#getStepDescription <em>Step Description</em>}</li>
 *   <li>{@link dmt.impl.AttackStepConnectorImpl#getAttackStepConnectorType <em>Attack Step Connector Type</em>}</li>
 *   <li>{@link dmt.impl.AttackStepConnectorImpl#getSequence <em>Sequence</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttackStepConnectorImpl extends AttackStepImpl implements AttackStepConnector {
	/**
	 * The cached value of the '{@link #getSubattackstep() <em>Subattackstep</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubattackstep()
	 * @generated
	 * @ordered
	 */
	protected EList<AttackStepConnector> subattackstep;

	/**
	 * The default value of the '{@link #getStepDescription() <em>Step Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String STEP_DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStepDescription() <em>Step Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStepDescription()
	 * @generated
	 * @ordered
	 */
	protected String stepDescription = STEP_DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getAttackStepConnectorType() <em>Attack Step Connector Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttackStepConnectorType()
	 * @generated
	 * @ordered
	 */
	protected static final AttackConnectorEnum ATTACK_STEP_CONNECTOR_TYPE_EDEFAULT = AttackConnectorEnum.OR;

	/**
	 * The cached value of the '{@link #getAttackStepConnectorType() <em>Attack Step Connector Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttackStepConnectorType()
	 * @generated
	 * @ordered
	 */
	protected AttackConnectorEnum attackStepConnectorType = ATTACK_STEP_CONNECTOR_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSequence() <em>Sequence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequence()
	 * @generated
	 * @ordered
	 */
	protected static final int SEQUENCE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSequence() <em>Sequence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequence()
	 * @generated
	 * @ordered
	 */
	protected int sequence = SEQUENCE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttackStepConnectorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.ATTACK_STEP_CONNECTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AttackStepConnector> getSubattackstep() {
		if (subattackstep == null) {
			subattackstep = new EObjectResolvingEList<AttackStepConnector>(AttackStepConnector.class, this, DmtPackage.ATTACK_STEP_CONNECTOR__SUBATTACKSTEP);
		}
		return subattackstep;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStepDescription() {
		return stepDescription;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStepDescription(String newStepDescription) {
		String oldStepDescription = stepDescription;
		stepDescription = newStepDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_STEP_CONNECTOR__STEP_DESCRIPTION, oldStepDescription, stepDescription));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttackConnectorEnum getAttackStepConnectorType() {
		return attackStepConnectorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttackStepConnectorType(AttackConnectorEnum newAttackStepConnectorType) {
		AttackConnectorEnum oldAttackStepConnectorType = attackStepConnectorType;
		attackStepConnectorType = newAttackStepConnectorType == null ? ATTACK_STEP_CONNECTOR_TYPE_EDEFAULT : newAttackStepConnectorType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_STEP_CONNECTOR__ATTACK_STEP_CONNECTOR_TYPE, oldAttackStepConnectorType, attackStepConnectorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSequence() {
		return sequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSequence(int newSequence) {
		int oldSequence = sequence;
		sequence = newSequence;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.ATTACK_STEP_CONNECTOR__SEQUENCE, oldSequence, sequence));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.ATTACK_STEP_CONNECTOR__SUBATTACKSTEP:
				return getSubattackstep();
			case DmtPackage.ATTACK_STEP_CONNECTOR__STEP_DESCRIPTION:
				return getStepDescription();
			case DmtPackage.ATTACK_STEP_CONNECTOR__ATTACK_STEP_CONNECTOR_TYPE:
				return getAttackStepConnectorType();
			case DmtPackage.ATTACK_STEP_CONNECTOR__SEQUENCE:
				return getSequence();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.ATTACK_STEP_CONNECTOR__SUBATTACKSTEP:
				getSubattackstep().clear();
				getSubattackstep().addAll((Collection<? extends AttackStepConnector>)newValue);
				return;
			case DmtPackage.ATTACK_STEP_CONNECTOR__STEP_DESCRIPTION:
				setStepDescription((String)newValue);
				return;
			case DmtPackage.ATTACK_STEP_CONNECTOR__ATTACK_STEP_CONNECTOR_TYPE:
				setAttackStepConnectorType((AttackConnectorEnum)newValue);
				return;
			case DmtPackage.ATTACK_STEP_CONNECTOR__SEQUENCE:
				setSequence((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACK_STEP_CONNECTOR__SUBATTACKSTEP:
				getSubattackstep().clear();
				return;
			case DmtPackage.ATTACK_STEP_CONNECTOR__STEP_DESCRIPTION:
				setStepDescription(STEP_DESCRIPTION_EDEFAULT);
				return;
			case DmtPackage.ATTACK_STEP_CONNECTOR__ATTACK_STEP_CONNECTOR_TYPE:
				setAttackStepConnectorType(ATTACK_STEP_CONNECTOR_TYPE_EDEFAULT);
				return;
			case DmtPackage.ATTACK_STEP_CONNECTOR__SEQUENCE:
				setSequence(SEQUENCE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.ATTACK_STEP_CONNECTOR__SUBATTACKSTEP:
				return subattackstep != null && !subattackstep.isEmpty();
			case DmtPackage.ATTACK_STEP_CONNECTOR__STEP_DESCRIPTION:
				return STEP_DESCRIPTION_EDEFAULT == null ? stepDescription != null : !STEP_DESCRIPTION_EDEFAULT.equals(stepDescription);
			case DmtPackage.ATTACK_STEP_CONNECTOR__ATTACK_STEP_CONNECTOR_TYPE:
				return attackStepConnectorType != ATTACK_STEP_CONNECTOR_TYPE_EDEFAULT;
			case DmtPackage.ATTACK_STEP_CONNECTOR__SEQUENCE:
				return sequence != SEQUENCE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (stepDescription: ");
		result.append(stepDescription);
		result.append(", attackStepConnectorType: ");
		result.append(attackStepConnectorType);
		result.append(", sequence: ");
		result.append(sequence);
		result.append(')');
		return result.toString();
	}

} //AttackStepConnectorImpl
