/**
 */
package dmt.impl;

import dmt.DeceptionTactic;
import dmt.DeceptionTacticMechanism;
import dmt.DmtPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Tactic Mechanism</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionTacticMechanismImpl#getEnablingCondition <em>Enabling Condition</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticMechanismImpl#getTacticReference <em>Tactic Reference</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionTacticMechanismImpl extends AbstractFeatureImpl implements DeceptionTacticMechanism {
	/**
	 * The default value of the '{@link #getEnablingCondition() <em>Enabling Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnablingCondition()
	 * @generated
	 * @ordered
	 */
	protected static final String ENABLING_CONDITION_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getEnablingCondition() <em>Enabling Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnablingCondition()
	 * @generated
	 * @ordered
	 */
	protected String enablingCondition = ENABLING_CONDITION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTacticReference() <em>Tactic Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTacticReference()
	 * @generated
	 * @ordered
	 */
	protected DeceptionTactic tacticReference;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionTacticMechanismImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_TACTIC_MECHANISM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEnablingCondition() {
		return enablingCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnablingCondition(String newEnablingCondition) {
		String oldEnablingCondition = enablingCondition;
		enablingCondition = newEnablingCondition;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC_MECHANISM__ENABLING_CONDITION, oldEnablingCondition, enablingCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTactic getTacticReference() {
		if (tacticReference != null && tacticReference.eIsProxy()) {
			InternalEObject oldTacticReference = (InternalEObject)tacticReference;
			tacticReference = (DeceptionTactic)eResolveProxy(oldTacticReference);
			if (tacticReference != oldTacticReference) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.DECEPTION_TACTIC_MECHANISM__TACTIC_REFERENCE, oldTacticReference, tacticReference));
			}
		}
		return tacticReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTactic basicGetTacticReference() {
		return tacticReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTacticReference(DeceptionTactic newTacticReference) {
		DeceptionTactic oldTacticReference = tacticReference;
		tacticReference = newTacticReference;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC_MECHANISM__TACTIC_REFERENCE, oldTacticReference, tacticReference));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_MECHANISM__ENABLING_CONDITION:
				return getEnablingCondition();
			case DmtPackage.DECEPTION_TACTIC_MECHANISM__TACTIC_REFERENCE:
				if (resolve) return getTacticReference();
				return basicGetTacticReference();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_MECHANISM__ENABLING_CONDITION:
				setEnablingCondition((String)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC_MECHANISM__TACTIC_REFERENCE:
				setTacticReference((DeceptionTactic)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_MECHANISM__ENABLING_CONDITION:
				setEnablingCondition(ENABLING_CONDITION_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_TACTIC_MECHANISM__TACTIC_REFERENCE:
				setTacticReference((DeceptionTactic)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC_MECHANISM__ENABLING_CONDITION:
				return ENABLING_CONDITION_EDEFAULT == null ? enablingCondition != null : !ENABLING_CONDITION_EDEFAULT.equals(enablingCondition);
			case DmtPackage.DECEPTION_TACTIC_MECHANISM__TACTIC_REFERENCE:
				return tacticReference != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (enablingCondition: ");
		result.append(enablingCondition);
		result.append(')');
		return result.toString();
	}

} //DeceptionTacticMechanismImpl
