/**
 */
package dmt.impl;

import dmt.AvoidConstraint;
import dmt.DmtPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Avoid Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AvoidConstraintImpl extends FeatureConstraintImpl implements AvoidConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AvoidConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.AVOID_CONSTRAINT;
	}

} //AvoidConstraintImpl
