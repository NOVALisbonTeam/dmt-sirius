/**
 */
package dmt.impl;

import dmt.DeceptionChannel;
import dmt.DeceptionMetric;
import dmt.DeceptionRisk;
import dmt.DeceptionStory;
import dmt.DeceptionTactic;
import dmt.Dissimulation;
import dmt.DmtPackage;
import dmt.GoalNode;
import dmt.Mitigator;
import dmt.Operationable;
import dmt.QualityAttribute;
import dmt.Simulation;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Tactic</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getDomPost <em>Dom Post</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getDomPre <em>Dom Pre</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getFormalDomPost <em>Formal Dom Post</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getFormalDomPre <em>Formal Dom Pre</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getSubTactic <em>Sub Tactic</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getApplyDissimulation <em>Apply Dissimulation</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getDescribedByDeceptionStory <em>Described By Deception Story</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getMeasuredBy <em>Measured By</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getMonitoredBy <em>Monitored By</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getLeadDeceptionRisk <em>Lead Deception Risk</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getApplySimulation <em>Apply Simulation</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#isIsAbstract <em>Is Abstract</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTacticImpl#getQualityattribute <em>Qualityattribute</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionTacticImpl extends DeceptionNodeImpl implements DeceptionTactic {
	/**
	 * The default value of the '{@link #getDomPost() <em>Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPost()
	 * @generated
	 * @ordered
	 */
	protected static final String DOM_POST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomPost() <em>Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPost()
	 * @generated
	 * @ordered
	 */
	protected String domPost = DOM_POST_EDEFAULT;

	/**
	 * The default value of the '{@link #getDomPre() <em>Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPre()
	 * @generated
	 * @ordered
	 */
	protected static final String DOM_PRE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomPre() <em>Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomPre()
	 * @generated
	 * @ordered
	 */
	protected String domPre = DOM_PRE_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalDomPost() <em>Formal Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPost()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAL_DOM_POST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormalDomPost() <em>Formal Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPost()
	 * @generated
	 * @ordered
	 */
	protected String formalDomPost = FORMAL_DOM_POST_EDEFAULT;

	/**
	 * The default value of the '{@link #getFormalDomPre() <em>Formal Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPre()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAL_DOM_PRE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFormalDomPre() <em>Formal Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalDomPre()
	 * @generated
	 * @ordered
	 */
	protected String formalDomPre = FORMAL_DOM_PRE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSubTactic() <em>Sub Tactic</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubTactic()
	 * @generated
	 * @ordered
	 */
	protected EList<DeceptionTactic> subTactic;

	/**
	 * The cached value of the '{@link #getApplyDissimulation() <em>Apply Dissimulation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplyDissimulation()
	 * @generated
	 * @ordered
	 */
	protected EList<Dissimulation> applyDissimulation;

	/**
	 * The cached value of the '{@link #getDescribedByDeceptionStory() <em>Described By Deception Story</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescribedByDeceptionStory()
	 * @generated
	 * @ordered
	 */
	protected EList<DeceptionStory> describedByDeceptionStory;

	/**
	 * The cached value of the '{@link #getMeasuredBy() <em>Measured By</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasuredBy()
	 * @generated
	 * @ordered
	 */
	protected EList<DeceptionMetric> measuredBy;

	/**
	 * The cached value of the '{@link #getMonitoredBy() <em>Monitored By</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMonitoredBy()
	 * @generated
	 * @ordered
	 */
	protected EList<DeceptionChannel> monitoredBy;

	/**
	 * The cached value of the '{@link #getLeadDeceptionRisk() <em>Lead Deception Risk</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeadDeceptionRisk()
	 * @generated
	 * @ordered
	 */
	protected EList<DeceptionRisk> leadDeceptionRisk;

	/**
	 * The cached value of the '{@link #getApplySimulation() <em>Apply Simulation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplySimulation()
	 * @generated
	 * @ordered
	 */
	protected EList<Simulation> applySimulation;

	/**
	 * The default value of the '{@link #isIsAbstract() <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsAbstract() <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean isAbstract = IS_ABSTRACT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getQualityattribute() <em>Qualityattribute</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQualityattribute()
	 * @generated
	 * @ordered
	 */
	protected EList<QualityAttribute> qualityattribute;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionTacticImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_TACTIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomPost() {
		return domPost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomPost(String newDomPost) {
		String oldDomPost = domPost;
		domPost = newDomPost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC__DOM_POST, oldDomPost, domPost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalDomPost() {
		return formalDomPost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDomPost(String newFormalDomPost) {
		String oldFormalDomPost = formalDomPost;
		formalDomPost = newFormalDomPost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_POST, oldFormalDomPost, formalDomPost));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalDomPre() {
		return formalDomPre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalDomPre(String newFormalDomPre) {
		String oldFormalDomPre = formalDomPre;
		formalDomPre = newFormalDomPre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_PRE, oldFormalDomPre, formalDomPre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomPre() {
		return domPre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomPre(String newDomPre) {
		String oldDomPre = domPre;
		domPre = newDomPre;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC__DOM_PRE, oldDomPre, domPre));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeceptionTactic> getSubTactic() {
		if (subTactic == null) {
			subTactic = new EObjectResolvingEList<DeceptionTactic>(DeceptionTactic.class, this, DmtPackage.DECEPTION_TACTIC__SUB_TACTIC);
		}
		return subTactic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Dissimulation> getApplyDissimulation() {
		if (applyDissimulation == null) {
			applyDissimulation = new EObjectResolvingEList<Dissimulation>(Dissimulation.class, this, DmtPackage.DECEPTION_TACTIC__APPLY_DISSIMULATION);
		}
		return applyDissimulation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeceptionStory> getDescribedByDeceptionStory() {
		if (describedByDeceptionStory == null) {
			describedByDeceptionStory = new EObjectResolvingEList<DeceptionStory>(DeceptionStory.class, this, DmtPackage.DECEPTION_TACTIC__DESCRIBED_BY_DECEPTION_STORY);
		}
		return describedByDeceptionStory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeceptionMetric> getMeasuredBy() {
		if (measuredBy == null) {
			measuredBy = new EObjectResolvingEList<DeceptionMetric>(DeceptionMetric.class, this, DmtPackage.DECEPTION_TACTIC__MEASURED_BY);
		}
		return measuredBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeceptionChannel> getMonitoredBy() {
		if (monitoredBy == null) {
			monitoredBy = new EObjectResolvingEList<DeceptionChannel>(DeceptionChannel.class, this, DmtPackage.DECEPTION_TACTIC__MONITORED_BY);
		}
		return monitoredBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DeceptionRisk> getLeadDeceptionRisk() {
		if (leadDeceptionRisk == null) {
			leadDeceptionRisk = new EObjectResolvingEList<DeceptionRisk>(DeceptionRisk.class, this, DmtPackage.DECEPTION_TACTIC__LEAD_DECEPTION_RISK);
		}
		return leadDeceptionRisk;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Simulation> getApplySimulation() {
		if (applySimulation == null) {
			applySimulation = new EObjectResolvingEList<Simulation>(Simulation.class, this, DmtPackage.DECEPTION_TACTIC__APPLY_SIMULATION);
		}
		return applySimulation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsAbstract() {
		return isAbstract;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsAbstract(boolean newIsAbstract) {
		boolean oldIsAbstract = isAbstract;
		isAbstract = newIsAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TACTIC__IS_ABSTRACT, oldIsAbstract, isAbstract));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<QualityAttribute> getQualityattribute() {
		if (qualityattribute == null) {
			qualityattribute = new EObjectResolvingEList<QualityAttribute>(QualityAttribute.class, this, DmtPackage.DECEPTION_TACTIC__QUALITYATTRIBUTE);
		}
		return qualityattribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC__DOM_POST:
				return getDomPost();
			case DmtPackage.DECEPTION_TACTIC__DOM_PRE:
				return getDomPre();
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_POST:
				return getFormalDomPost();
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_PRE:
				return getFormalDomPre();
			case DmtPackage.DECEPTION_TACTIC__SUB_TACTIC:
				return getSubTactic();
			case DmtPackage.DECEPTION_TACTIC__APPLY_DISSIMULATION:
				return getApplyDissimulation();
			case DmtPackage.DECEPTION_TACTIC__DESCRIBED_BY_DECEPTION_STORY:
				return getDescribedByDeceptionStory();
			case DmtPackage.DECEPTION_TACTIC__MEASURED_BY:
				return getMeasuredBy();
			case DmtPackage.DECEPTION_TACTIC__MONITORED_BY:
				return getMonitoredBy();
			case DmtPackage.DECEPTION_TACTIC__LEAD_DECEPTION_RISK:
				return getLeadDeceptionRisk();
			case DmtPackage.DECEPTION_TACTIC__APPLY_SIMULATION:
				return getApplySimulation();
			case DmtPackage.DECEPTION_TACTIC__IS_ABSTRACT:
				return isIsAbstract();
			case DmtPackage.DECEPTION_TACTIC__QUALITYATTRIBUTE:
				return getQualityattribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC__DOM_POST:
				setDomPost((String)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__DOM_PRE:
				setDomPre((String)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_POST:
				setFormalDomPost((String)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_PRE:
				setFormalDomPre((String)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__SUB_TACTIC:
				getSubTactic().clear();
				getSubTactic().addAll((Collection<? extends DeceptionTactic>)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__APPLY_DISSIMULATION:
				getApplyDissimulation().clear();
				getApplyDissimulation().addAll((Collection<? extends Dissimulation>)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__DESCRIBED_BY_DECEPTION_STORY:
				getDescribedByDeceptionStory().clear();
				getDescribedByDeceptionStory().addAll((Collection<? extends DeceptionStory>)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__MEASURED_BY:
				getMeasuredBy().clear();
				getMeasuredBy().addAll((Collection<? extends DeceptionMetric>)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__MONITORED_BY:
				getMonitoredBy().clear();
				getMonitoredBy().addAll((Collection<? extends DeceptionChannel>)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__LEAD_DECEPTION_RISK:
				getLeadDeceptionRisk().clear();
				getLeadDeceptionRisk().addAll((Collection<? extends DeceptionRisk>)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__APPLY_SIMULATION:
				getApplySimulation().clear();
				getApplySimulation().addAll((Collection<? extends Simulation>)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__IS_ABSTRACT:
				setIsAbstract((Boolean)newValue);
				return;
			case DmtPackage.DECEPTION_TACTIC__QUALITYATTRIBUTE:
				getQualityattribute().clear();
				getQualityattribute().addAll((Collection<? extends QualityAttribute>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC__DOM_POST:
				setDomPost(DOM_POST_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_TACTIC__DOM_PRE:
				setDomPre(DOM_PRE_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_POST:
				setFormalDomPost(FORMAL_DOM_POST_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_PRE:
				setFormalDomPre(FORMAL_DOM_PRE_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_TACTIC__SUB_TACTIC:
				getSubTactic().clear();
				return;
			case DmtPackage.DECEPTION_TACTIC__APPLY_DISSIMULATION:
				getApplyDissimulation().clear();
				return;
			case DmtPackage.DECEPTION_TACTIC__DESCRIBED_BY_DECEPTION_STORY:
				getDescribedByDeceptionStory().clear();
				return;
			case DmtPackage.DECEPTION_TACTIC__MEASURED_BY:
				getMeasuredBy().clear();
				return;
			case DmtPackage.DECEPTION_TACTIC__MONITORED_BY:
				getMonitoredBy().clear();
				return;
			case DmtPackage.DECEPTION_TACTIC__LEAD_DECEPTION_RISK:
				getLeadDeceptionRisk().clear();
				return;
			case DmtPackage.DECEPTION_TACTIC__APPLY_SIMULATION:
				getApplySimulation().clear();
				return;
			case DmtPackage.DECEPTION_TACTIC__IS_ABSTRACT:
				setIsAbstract(IS_ABSTRACT_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_TACTIC__QUALITYATTRIBUTE:
				getQualityattribute().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TACTIC__DOM_POST:
				return DOM_POST_EDEFAULT == null ? domPost != null : !DOM_POST_EDEFAULT.equals(domPost);
			case DmtPackage.DECEPTION_TACTIC__DOM_PRE:
				return DOM_PRE_EDEFAULT == null ? domPre != null : !DOM_PRE_EDEFAULT.equals(domPre);
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_POST:
				return FORMAL_DOM_POST_EDEFAULT == null ? formalDomPost != null : !FORMAL_DOM_POST_EDEFAULT.equals(formalDomPost);
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_PRE:
				return FORMAL_DOM_PRE_EDEFAULT == null ? formalDomPre != null : !FORMAL_DOM_PRE_EDEFAULT.equals(formalDomPre);
			case DmtPackage.DECEPTION_TACTIC__SUB_TACTIC:
				return subTactic != null && !subTactic.isEmpty();
			case DmtPackage.DECEPTION_TACTIC__APPLY_DISSIMULATION:
				return applyDissimulation != null && !applyDissimulation.isEmpty();
			case DmtPackage.DECEPTION_TACTIC__DESCRIBED_BY_DECEPTION_STORY:
				return describedByDeceptionStory != null && !describedByDeceptionStory.isEmpty();
			case DmtPackage.DECEPTION_TACTIC__MEASURED_BY:
				return measuredBy != null && !measuredBy.isEmpty();
			case DmtPackage.DECEPTION_TACTIC__MONITORED_BY:
				return monitoredBy != null && !monitoredBy.isEmpty();
			case DmtPackage.DECEPTION_TACTIC__LEAD_DECEPTION_RISK:
				return leadDeceptionRisk != null && !leadDeceptionRisk.isEmpty();
			case DmtPackage.DECEPTION_TACTIC__APPLY_SIMULATION:
				return applySimulation != null && !applySimulation.isEmpty();
			case DmtPackage.DECEPTION_TACTIC__IS_ABSTRACT:
				return isAbstract != IS_ABSTRACT_EDEFAULT;
			case DmtPackage.DECEPTION_TACTIC__QUALITYATTRIBUTE:
				return qualityattribute != null && !qualityattribute.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Mitigator.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == GoalNode.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Operationable.class) {
			switch (derivedFeatureID) {
				case DmtPackage.DECEPTION_TACTIC__DOM_POST: return DmtPackage.OPERATIONABLE__DOM_POST;
				case DmtPackage.DECEPTION_TACTIC__DOM_PRE: return DmtPackage.OPERATIONABLE__DOM_PRE;
				case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_POST: return DmtPackage.OPERATIONABLE__FORMAL_DOM_POST;
				case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_PRE: return DmtPackage.OPERATIONABLE__FORMAL_DOM_PRE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Mitigator.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == GoalNode.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		if (baseClass == Operationable.class) {
			switch (baseFeatureID) {
				case DmtPackage.OPERATIONABLE__DOM_POST: return DmtPackage.DECEPTION_TACTIC__DOM_POST;
				case DmtPackage.OPERATIONABLE__DOM_PRE: return DmtPackage.DECEPTION_TACTIC__DOM_PRE;
				case DmtPackage.OPERATIONABLE__FORMAL_DOM_POST: return DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_POST;
				case DmtPackage.OPERATIONABLE__FORMAL_DOM_PRE: return DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_PRE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (domPost: ");
		result.append(domPost);
		result.append(", domPre: ");
		result.append(domPre);
		result.append(", formalDomPost: ");
		result.append(formalDomPost);
		result.append(", formalDomPre: ");
		result.append(formalDomPre);
		result.append(", isAbstract: ");
		result.append(isAbstract);
		result.append(')');
		return result.toString();
	}

} //DeceptionTacticImpl
