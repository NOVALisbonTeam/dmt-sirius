/**
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.GoalRefinable;
import dmt.ORGoalRefinement;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>OR Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.ORGoalRefinementImpl#getOrGoalRefine <em>Or Goal Refine</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ORGoalRefinementImpl extends GoalRelationshipImpl implements ORGoalRefinement {
	/**
	 * The cached value of the '{@link #getOrGoalRefine() <em>Or Goal Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrGoalRefine()
	 * @generated
	 * @ordered
	 */
	protected GoalRefinable orGoalRefine;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ORGoalRefinementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.OR_GOAL_REFINEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalRefinable getOrGoalRefine() {
		if (orGoalRefine != null && orGoalRefine.eIsProxy()) {
			InternalEObject oldOrGoalRefine = (InternalEObject)orGoalRefine;
			orGoalRefine = (GoalRefinable)eResolveProxy(oldOrGoalRefine);
			if (orGoalRefine != oldOrGoalRefine) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DmtPackage.OR_GOAL_REFINEMENT__OR_GOAL_REFINE, oldOrGoalRefine, orGoalRefine));
			}
		}
		return orGoalRefine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GoalRefinable basicGetOrGoalRefine() {
		return orGoalRefine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrGoalRefine(GoalRefinable newOrGoalRefine) {
		GoalRefinable oldOrGoalRefine = orGoalRefine;
		orGoalRefine = newOrGoalRefine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.OR_GOAL_REFINEMENT__OR_GOAL_REFINE, oldOrGoalRefine, orGoalRefine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.OR_GOAL_REFINEMENT__OR_GOAL_REFINE:
				if (resolve) return getOrGoalRefine();
				return basicGetOrGoalRefine();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.OR_GOAL_REFINEMENT__OR_GOAL_REFINE:
				setOrGoalRefine((GoalRefinable)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.OR_GOAL_REFINEMENT__OR_GOAL_REFINE:
				setOrGoalRefine((GoalRefinable)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.OR_GOAL_REFINEMENT__OR_GOAL_REFINE:
				return orGoalRefine != null;
		}
		return super.eIsSet(featureID);
	}

} //ORGoalRefinementImpl
