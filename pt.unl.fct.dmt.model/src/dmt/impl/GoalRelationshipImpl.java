/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.GoalRelationship;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goal Relationship</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.GoalRelationshipImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link dmt.impl.GoalRelationshipImpl#getTactic <em>Tactic</em>}</li>
 *   <li>{@link dmt.impl.GoalRelationshipImpl#getSysRef <em>Sys Ref</em>}</li>
 *   <li>{@link dmt.impl.GoalRelationshipImpl#getAltName <em>Alt Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class GoalRelationshipImpl extends MinimalEObjectImpl.Container implements GoalRelationship {
	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final String STATUS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected String status = STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTactic() <em>Tactic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTactic()
	 * @generated
	 * @ordered
	 */
	protected static final String TACTIC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTactic() <em>Tactic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTactic()
	 * @generated
	 * @ordered
	 */
	protected String tactic = TACTIC_EDEFAULT;

	/**
	 * The default value of the '{@link #getSysRef() <em>Sys Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSysRef()
	 * @generated
	 * @ordered
	 */
	protected static final String SYS_REF_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSysRef() <em>Sys Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSysRef()
	 * @generated
	 * @ordered
	 */
	protected String sysRef = SYS_REF_EDEFAULT;

	/**
	 * The default value of the '{@link #getAltName() <em>Alt Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAltName()
	 * @generated
	 * @ordered
	 */
	protected static final String ALT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAltName() <em>Alt Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAltName()
	 * @generated
	 * @ordered
	 */
	protected String altName = ALT_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalRelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.GOAL_RELATIONSHIP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(String newStatus) {
		String oldStatus = status;
		status = newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL_RELATIONSHIP__STATUS, oldStatus, status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTactic() {
		return tactic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTactic(String newTactic) {
		String oldTactic = tactic;
		tactic = newTactic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL_RELATIONSHIP__TACTIC, oldTactic, tactic));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSysRef() {
		return sysRef;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSysRef(String newSysRef) {
		String oldSysRef = sysRef;
		sysRef = newSysRef;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL_RELATIONSHIP__SYS_REF, oldSysRef, sysRef));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAltName() {
		return altName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAltName(String newAltName) {
		String oldAltName = altName;
		altName = newAltName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.GOAL_RELATIONSHIP__ALT_NAME, oldAltName, altName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.GOAL_RELATIONSHIP__STATUS:
				return getStatus();
			case DmtPackage.GOAL_RELATIONSHIP__TACTIC:
				return getTactic();
			case DmtPackage.GOAL_RELATIONSHIP__SYS_REF:
				return getSysRef();
			case DmtPackage.GOAL_RELATIONSHIP__ALT_NAME:
				return getAltName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.GOAL_RELATIONSHIP__STATUS:
				setStatus((String)newValue);
				return;
			case DmtPackage.GOAL_RELATIONSHIP__TACTIC:
				setTactic((String)newValue);
				return;
			case DmtPackage.GOAL_RELATIONSHIP__SYS_REF:
				setSysRef((String)newValue);
				return;
			case DmtPackage.GOAL_RELATIONSHIP__ALT_NAME:
				setAltName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.GOAL_RELATIONSHIP__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
			case DmtPackage.GOAL_RELATIONSHIP__TACTIC:
				setTactic(TACTIC_EDEFAULT);
				return;
			case DmtPackage.GOAL_RELATIONSHIP__SYS_REF:
				setSysRef(SYS_REF_EDEFAULT);
				return;
			case DmtPackage.GOAL_RELATIONSHIP__ALT_NAME:
				setAltName(ALT_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.GOAL_RELATIONSHIP__STATUS:
				return STATUS_EDEFAULT == null ? status != null : !STATUS_EDEFAULT.equals(status);
			case DmtPackage.GOAL_RELATIONSHIP__TACTIC:
				return TACTIC_EDEFAULT == null ? tactic != null : !TACTIC_EDEFAULT.equals(tactic);
			case DmtPackage.GOAL_RELATIONSHIP__SYS_REF:
				return SYS_REF_EDEFAULT == null ? sysRef != null : !SYS_REF_EDEFAULT.equals(sysRef);
			case DmtPackage.GOAL_RELATIONSHIP__ALT_NAME:
				return ALT_NAME_EDEFAULT == null ? altName != null : !ALT_NAME_EDEFAULT.equals(altName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (status: ");
		result.append(status);
		result.append(", tactic: ");
		result.append(tactic);
		result.append(", sysRef: ");
		result.append(sysRef);
		result.append(", altName: ");
		result.append(altName);
		result.append(')');
		return result.toString();
	}

} //GoalRelationshipImpl
