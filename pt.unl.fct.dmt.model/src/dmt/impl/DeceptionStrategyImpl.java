/**
 */
package dmt.impl;

import dmt.DeceptionStrategy;
import dmt.DmtPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionStrategyImpl#isIsRoot <em>Is Root</em>}</li>
 *   <li>{@link dmt.impl.DeceptionStrategyImpl#getExecutionParameter <em>Execution Parameter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeceptionStrategyImpl extends AbstractFeatureImpl implements DeceptionStrategy {
	/**
	 * The default value of the '{@link #isIsRoot() <em>Is Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsRoot()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_ROOT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isIsRoot() <em>Is Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isIsRoot()
	 * @generated
	 * @ordered
	 */
	protected boolean isRoot = IS_ROOT_EDEFAULT;

	/**
	 * The default value of the '{@link #getExecutionParameter() <em>Execution Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionParameter()
	 * @generated
	 * @ordered
	 */
	protected static final String EXECUTION_PARAMETER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecutionParameter() <em>Execution Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionParameter()
	 * @generated
	 * @ordered
	 */
	protected String executionParameter = EXECUTION_PARAMETER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_STRATEGY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsRoot() {
		return isRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsRoot(boolean newIsRoot) {
		boolean oldIsRoot = isRoot;
		isRoot = newIsRoot;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STRATEGY__IS_ROOT, oldIsRoot, isRoot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExecutionParameter() {
		return executionParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionParameter(String newExecutionParameter) {
		String oldExecutionParameter = executionParameter;
		executionParameter = newExecutionParameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_STRATEGY__EXECUTION_PARAMETER, oldExecutionParameter, executionParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY__IS_ROOT:
				return isIsRoot();
			case DmtPackage.DECEPTION_STRATEGY__EXECUTION_PARAMETER:
				return getExecutionParameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY__IS_ROOT:
				setIsRoot((Boolean)newValue);
				return;
			case DmtPackage.DECEPTION_STRATEGY__EXECUTION_PARAMETER:
				setExecutionParameter((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY__IS_ROOT:
				setIsRoot(IS_ROOT_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_STRATEGY__EXECUTION_PARAMETER:
				setExecutionParameter(EXECUTION_PARAMETER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_STRATEGY__IS_ROOT:
				return isRoot != IS_ROOT_EDEFAULT;
			case DmtPackage.DECEPTION_STRATEGY__EXECUTION_PARAMETER:
				return EXECUTION_PARAMETER_EDEFAULT == null ? executionParameter != null : !EXECUTION_PARAMETER_EDEFAULT.equals(executionParameter);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (isRoot: ");
		result.append(isRoot);
		result.append(", executionParameter: ");
		result.append(executionParameter);
		result.append(')');
		return result.toString();
	}

} //DeceptionStrategyImpl
