/**
 */
package dmt.impl;

import dmt.DeceptionTechnique;
import dmt.DmtPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Technique</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link dmt.impl.DeceptionTechniqueImpl#getFormalSpec <em>Formal Spec</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTechniqueImpl#getInformalSpec <em>Informal Spec</em>}</li>
 *   <li>{@link dmt.impl.DeceptionTechniqueImpl#getExpectedOutcome <em>Expected Outcome</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DeceptionTechniqueImpl extends DeceptionNodeImpl implements DeceptionTechnique {
	/**
	 * The default value of the '{@link #getFormalSpec() <em>Formal Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalSpec()
	 * @generated
	 * @ordered
	 */
	protected static final String FORMAL_SPEC_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getFormalSpec() <em>Formal Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormalSpec()
	 * @generated
	 * @ordered
	 */
	protected String formalSpec = FORMAL_SPEC_EDEFAULT;
	/**
	 * The default value of the '{@link #getInformalSpec() <em>Informal Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInformalSpec()
	 * @generated
	 * @ordered
	 */
	protected static final String INFORMAL_SPEC_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getInformalSpec() <em>Informal Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInformalSpec()
	 * @generated
	 * @ordered
	 */
	protected String informalSpec = INFORMAL_SPEC_EDEFAULT;
	/**
	 * The default value of the '{@link #getExpectedOutcome() <em>Expected Outcome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedOutcome()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPECTED_OUTCOME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getExpectedOutcome() <em>Expected Outcome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedOutcome()
	 * @generated
	 * @ordered
	 */
	protected String expectedOutcome = EXPECTED_OUTCOME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionTechniqueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_TECHNIQUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormalSpec() {
		return formalSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormalSpec(String newFormalSpec) {
		String oldFormalSpec = formalSpec;
		formalSpec = newFormalSpec;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TECHNIQUE__FORMAL_SPEC, oldFormalSpec, formalSpec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInformalSpec() {
		return informalSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInformalSpec(String newInformalSpec) {
		String oldInformalSpec = informalSpec;
		informalSpec = newInformalSpec;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TECHNIQUE__INFORMAL_SPEC, oldInformalSpec, informalSpec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExpectedOutcome() {
		return expectedOutcome;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpectedOutcome(String newExpectedOutcome) {
		String oldExpectedOutcome = expectedOutcome;
		expectedOutcome = newExpectedOutcome;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DmtPackage.DECEPTION_TECHNIQUE__EXPECTED_OUTCOME, oldExpectedOutcome, expectedOutcome));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TECHNIQUE__FORMAL_SPEC:
				return getFormalSpec();
			case DmtPackage.DECEPTION_TECHNIQUE__INFORMAL_SPEC:
				return getInformalSpec();
			case DmtPackage.DECEPTION_TECHNIQUE__EXPECTED_OUTCOME:
				return getExpectedOutcome();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TECHNIQUE__FORMAL_SPEC:
				setFormalSpec((String)newValue);
				return;
			case DmtPackage.DECEPTION_TECHNIQUE__INFORMAL_SPEC:
				setInformalSpec((String)newValue);
				return;
			case DmtPackage.DECEPTION_TECHNIQUE__EXPECTED_OUTCOME:
				setExpectedOutcome((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TECHNIQUE__FORMAL_SPEC:
				setFormalSpec(FORMAL_SPEC_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_TECHNIQUE__INFORMAL_SPEC:
				setInformalSpec(INFORMAL_SPEC_EDEFAULT);
				return;
			case DmtPackage.DECEPTION_TECHNIQUE__EXPECTED_OUTCOME:
				setExpectedOutcome(EXPECTED_OUTCOME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DmtPackage.DECEPTION_TECHNIQUE__FORMAL_SPEC:
				return FORMAL_SPEC_EDEFAULT == null ? formalSpec != null : !FORMAL_SPEC_EDEFAULT.equals(formalSpec);
			case DmtPackage.DECEPTION_TECHNIQUE__INFORMAL_SPEC:
				return INFORMAL_SPEC_EDEFAULT == null ? informalSpec != null : !INFORMAL_SPEC_EDEFAULT.equals(informalSpec);
			case DmtPackage.DECEPTION_TECHNIQUE__EXPECTED_OUTCOME:
				return EXPECTED_OUTCOME_EDEFAULT == null ? expectedOutcome != null : !EXPECTED_OUTCOME_EDEFAULT.equals(expectedOutcome);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (formalSpec: ");
		result.append(formalSpec);
		result.append(", InformalSpec: ");
		result.append(informalSpec);
		result.append(", expectedOutcome: ");
		result.append(expectedOutcome);
		result.append(')');
		return result.toString();
	}

} //DeceptionTechniqueImpl
