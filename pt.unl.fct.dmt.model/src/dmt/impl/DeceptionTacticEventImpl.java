/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DeceptionTacticEvent;
import dmt.DmtPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Tactic Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DeceptionTacticEventImpl extends EventImpl implements DeceptionTacticEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionTacticEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_TACTIC_EVENT;
	}

} //DeceptionTacticEventImpl
