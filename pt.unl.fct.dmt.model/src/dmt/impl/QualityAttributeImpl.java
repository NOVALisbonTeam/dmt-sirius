/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.impl;

import dmt.DmtPackage;
import dmt.QualityAttribute;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Quality Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class QualityAttributeImpl extends InfluenceNodeImpl implements QualityAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QualityAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.QUALITY_ATTRIBUTE;
	}

} //QualityAttributeImpl
