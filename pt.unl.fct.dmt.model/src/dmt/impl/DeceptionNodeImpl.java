/**
 */
package dmt.impl;

import dmt.DeceptionNode;
import dmt.DmtPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deception Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DeceptionNodeImpl extends NodeImpl implements DeceptionNode {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeceptionNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DmtPackage.Literals.DECEPTION_NODE;
	}

} //DeceptionNodeImpl
