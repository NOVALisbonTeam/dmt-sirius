/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simulation Has Artificial Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.SimulationHasArtificialElement#getShowFalseAsset <em>Show False Asset</em>}</li>
 *   <li>{@link dmt.SimulationHasArtificialElement#getLowerFakes <em>Lower Fakes</em>}</li>
 *   <li>{@link dmt.SimulationHasArtificialElement#getUpperFakes <em>Upper Fakes</em>}</li>
 *   <li>{@link dmt.SimulationHasArtificialElement#getSimulationType <em>Simulation Type</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getSimulationHasArtificialElement()
 * @model
 * @generated
 */
public interface SimulationHasArtificialElement extends DeceptionRelationship {
	/**
	 * Returns the value of the '<em><b>Show False Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Show False Asset</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Show False Asset</em>' reference.
	 * @see #setShowFalseAsset(ArtificialElement)
	 * @see dmt.DmtPackage#getSimulationHasArtificialElement_ShowFalseAsset()
	 * @model required="true"
	 * @generated
	 */
	ArtificialElement getShowFalseAsset();

	/**
	 * Sets the value of the '{@link dmt.SimulationHasArtificialElement#getShowFalseAsset <em>Show False Asset</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Show False Asset</em>' reference.
	 * @see #getShowFalseAsset()
	 * @generated
	 */
	void setShowFalseAsset(ArtificialElement value);

	/**
	 * Returns the value of the '<em><b>Lower Fakes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower Fakes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower Fakes</em>' attribute.
	 * @see #setLowerFakes(String)
	 * @see dmt.DmtPackage#getSimulationHasArtificialElement_LowerFakes()
	 * @model required="true"
	 * @generated
	 */
	String getLowerFakes();

	/**
	 * Sets the value of the '{@link dmt.SimulationHasArtificialElement#getLowerFakes <em>Lower Fakes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Fakes</em>' attribute.
	 * @see #getLowerFakes()
	 * @generated
	 */
	void setLowerFakes(String value);

	/**
	 * Returns the value of the '<em><b>Upper Fakes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper Fakes</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Fakes</em>' attribute.
	 * @see #setUpperFakes(String)
	 * @see dmt.DmtPackage#getSimulationHasArtificialElement_UpperFakes()
	 * @model required="true"
	 * @generated
	 */
	String getUpperFakes();

	/**
	 * Sets the value of the '{@link dmt.SimulationHasArtificialElement#getUpperFakes <em>Upper Fakes</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Fakes</em>' attribute.
	 * @see #getUpperFakes()
	 * @generated
	 */
	void setUpperFakes(String value);

	/**
	 * Returns the value of the '<em><b>Simulation Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulation Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulation Type</em>' reference.
	 * @see #setSimulationType(Simulation)
	 * @see dmt.DmtPackage#getSimulationHasArtificialElement_SimulationType()
	 * @model required="true"
	 * @generated
	 */
	Simulation getSimulationType();

	/**
	 * Sets the value of the '{@link dmt.SimulationHasArtificialElement#getSimulationType <em>Simulation Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulation Type</em>' reference.
	 * @see #getSimulationType()
	 * @generated
	 */
	void setSimulationType(Simulation value);

} // SimulationHasArtificialElement
