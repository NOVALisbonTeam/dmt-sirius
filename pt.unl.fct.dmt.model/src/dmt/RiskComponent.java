/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Risk Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * https://en.wikipedia.org/wiki/IT_risk_management##Risk_assessment
 * 
 * Risk is the possibility of losing something of value. Values (such as physical health, social status, emotional well-being, or financial wealth) can be gained or lost when taking risk resulting from a given action or inaction, foreseen or unforeseen (planned or not planned). Risk can also be defined as the intentional interaction with uncertainty.[1] Uncertainty is a potential, unpredictable, and uncontrollable outcome; risk is a consequence of action taken in spite of uncertainty.
 * 
 * Risk perception is the subjective judgment people make about the severity and probability of a risk, and may vary person to person. Any human endeavour carries some risk, but some are much riskier than others.[2]
 * 
 * Because risk is strictly tied to uncertainty, decision theory should be applied to manage risk as a science, i.e. rationally making choices under uncertainty.
 * 
 * Generally speaking, risk is the product of likelihood times impact (Risk = Likelihood * Impact).[4]
 * 
 * The measure of an IT risk can be determined as a product of threat, vulnerability and asset values:[5]
 * 
 * {\displaystyle Risk=Threat*Vulnerability*Asset} {\displaystyle Risk=Threat*Vulnerability*Asset}
 * A more current Risk management framework for IT Risk would be the TIK framework:
 * 
 * {\displaystyle Risk=((Vulnerability*Threat)/CounterMeasure)*AssetValueatRisk} {\displaystyle Risk=((Vulnerability*Threat)/CounterMeasure)*AssetValueatRisk}[6]
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.RiskComponent#getRiskSeverity <em>Risk Severity</em>}</li>
 *   <li>{@link dmt.RiskComponent#getRiskLikelihood <em>Risk Likelihood</em>}</li>
 *   <li>{@link dmt.RiskComponent#getHarmsAsset <em>Harms Asset</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getRiskComponent()
 * @model
 * @generated
 */
public interface RiskComponent extends MitigableElement {
	/**
	 * Returns the value of the '<em><b>Risk Severity</b></em>' attribute.
	 * The default value is <code>"INSIGNIFICANT"</code>.
	 * The literals are from the enumeration {@link dmt.RiskSeverityEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Risk Severity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Risk Severity</em>' attribute.
	 * @see dmt.RiskSeverityEnum
	 * @see #setRiskSeverity(RiskSeverityEnum)
	 * @see dmt.DmtPackage#getRiskComponent_RiskSeverity()
	 * @model default="INSIGNIFICANT"
	 * @generated
	 */
	RiskSeverityEnum getRiskSeverity();

	/**
	 * Sets the value of the '{@link dmt.RiskComponent#getRiskSeverity <em>Risk Severity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Risk Severity</em>' attribute.
	 * @see dmt.RiskSeverityEnum
	 * @see #getRiskSeverity()
	 * @generated
	 */
	void setRiskSeverity(RiskSeverityEnum value);

	/**
	 * Returns the value of the '<em><b>Risk Likelihood</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.RiskLikelihoodEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Risk Likelihood</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Risk Likelihood</em>' attribute.
	 * @see dmt.RiskLikelihoodEnum
	 * @see #setRiskLikelihood(RiskLikelihoodEnum)
	 * @see dmt.DmtPackage#getRiskComponent_RiskLikelihood()
	 * @model
	 * @generated
	 */
	RiskLikelihoodEnum getRiskLikelihood();

	/**
	 * Sets the value of the '{@link dmt.RiskComponent#getRiskLikelihood <em>Risk Likelihood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Risk Likelihood</em>' attribute.
	 * @see dmt.RiskLikelihoodEnum
	 * @see #getRiskLikelihood()
	 * @generated
	 */
	void setRiskLikelihood(RiskLikelihoodEnum value);

	/**
	 * Returns the value of the '<em><b>Harms Asset</b></em>' reference list.
	 * The list contents are of type {@link dmt.ConcreteAsset}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Harms Asset</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Harms Asset</em>' reference list.
	 * @see dmt.DmtPackage#getRiskComponent_HarmsAsset()
	 * @model
	 * @generated
	 */
	EList<ConcreteAsset> getHarmsAsset();

} // RiskComponent
