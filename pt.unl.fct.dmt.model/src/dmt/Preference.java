/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Preference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Preference#getPreferenceTarget <em>Preference Target</em>}</li>
 *   <li>{@link dmt.Preference#getPreferenceSource <em>Preference Source</em>}</li>
 *   <li>{@link dmt.Preference#getWeight <em>Weight</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getPreference()
 * @model
 * @generated
 */
public interface Preference extends InfluenceRelationship {
	/**
	 * Returns the value of the '<em><b>Preference Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preference Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preference Target</em>' reference.
	 * @see #setPreferenceTarget(QualityAttribute)
	 * @see dmt.DmtPackage#getPreference_PreferenceTarget()
	 * @model required="true"
	 * @generated
	 */
	QualityAttribute getPreferenceTarget();

	/**
	 * Sets the value of the '{@link dmt.Preference#getPreferenceTarget <em>Preference Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preference Target</em>' reference.
	 * @see #getPreferenceTarget()
	 * @generated
	 */
	void setPreferenceTarget(QualityAttribute value);

	/**
	 * Returns the value of the '<em><b>Preference Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preference Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preference Source</em>' reference.
	 * @see #setPreferenceSource(QualityAttribute)
	 * @see dmt.DmtPackage#getPreference_PreferenceSource()
	 * @model required="true"
	 * @generated
	 */
	QualityAttribute getPreferenceSource();

	/**
	 * Sets the value of the '{@link dmt.Preference#getPreferenceSource <em>Preference Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preference Source</em>' reference.
	 * @see #getPreferenceSource()
	 * @generated
	 */
	void setPreferenceSource(QualityAttribute value);

	/**
	 * Returns the value of the '<em><b>Weight</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.PreferenceWeightEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Weight</em>' attribute.
	 * @see dmt.PreferenceWeightEnum
	 * @see #setWeight(PreferenceWeightEnum)
	 * @see dmt.DmtPackage#getPreference_Weight()
	 * @model
	 * @generated
	 */
	PreferenceWeightEnum getWeight();

	/**
	 * Sets the value of the '{@link dmt.Preference#getWeight <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Weight</em>' attribute.
	 * @see dmt.PreferenceWeightEnum
	 * @see #getWeight()
	 * @generated
	 */
	void setWeight(PreferenceWeightEnum value);

} // Preference
