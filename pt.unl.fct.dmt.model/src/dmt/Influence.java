/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Influence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Influence#getWeight <em>Weight</em>}</li>
 *   <li>{@link dmt.Influence#getPolarity <em>Polarity</em>}</li>
 *   <li>{@link dmt.Influence#getInfluenceSource <em>Influence Source</em>}</li>
 *   <li>{@link dmt.Influence#getInfluencetarget <em>Influencetarget</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getInfluence()
 * @model
 * @generated
 */
public interface Influence extends InfluenceRelationship {
	/**
	 * Returns the value of the '<em><b>Weight</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.InfluenceWeightEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Weight</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Weight</em>' attribute.
	 * @see dmt.InfluenceWeightEnum
	 * @see #setWeight(InfluenceWeightEnum)
	 * @see dmt.DmtPackage#getInfluence_Weight()
	 * @model
	 * @generated
	 */
	InfluenceWeightEnum getWeight();

	/**
	 * Sets the value of the '{@link dmt.Influence#getWeight <em>Weight</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Weight</em>' attribute.
	 * @see dmt.InfluenceWeightEnum
	 * @see #getWeight()
	 * @generated
	 */
	void setWeight(InfluenceWeightEnum value);

	/**
	 * Returns the value of the '<em><b>Polarity</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.InfluencePolarityEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polarity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polarity</em>' attribute.
	 * @see dmt.InfluencePolarityEnum
	 * @see #setPolarity(InfluencePolarityEnum)
	 * @see dmt.DmtPackage#getInfluence_Polarity()
	 * @model
	 * @generated
	 */
	InfluencePolarityEnum getPolarity();

	/**
	 * Sets the value of the '{@link dmt.Influence#getPolarity <em>Polarity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polarity</em>' attribute.
	 * @see dmt.InfluencePolarityEnum
	 * @see #getPolarity()
	 * @generated
	 */
	void setPolarity(InfluencePolarityEnum value);

	/**
	 * Returns the value of the '<em><b>Influence Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Influence Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Influence Source</em>' reference.
	 * @see #setInfluenceSource(Event)
	 * @see dmt.DmtPackage#getInfluence_InfluenceSource()
	 * @model required="true"
	 * @generated
	 */
	Event getInfluenceSource();

	/**
	 * Sets the value of the '{@link dmt.Influence#getInfluenceSource <em>Influence Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Influence Source</em>' reference.
	 * @see #getInfluenceSource()
	 * @generated
	 */
	void setInfluenceSource(Event value);

	/**
	 * Returns the value of the '<em><b>Influencetarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Influencetarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Influencetarget</em>' reference.
	 * @see #setInfluencetarget(InfluenceNode)
	 * @see dmt.DmtPackage#getInfluence_Influencetarget()
	 * @model required="true"
	 * @generated
	 */
	InfluenceNode getInfluencetarget();

	/**
	 * Sets the value of the '{@link dmt.Influence#getInfluencetarget <em>Influencetarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Influencetarget</em>' reference.
	 * @see #getInfluencetarget()
	 * @generated
	 */
	void setInfluencetarget(InfluenceNode value);

} // Influence
