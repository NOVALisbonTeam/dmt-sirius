/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Mitigates</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionMitigates#getMitigationLevel <em>Mitigation Level</em>}</li>
 *   <li>{@link dmt.DeceptionMitigates#getTargetsMitigableNode <em>Targets Mitigable Node</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionMitigates()
 * @model
 * @generated
 */
public interface DeceptionMitigates extends DeceptionNode {
	/**
	 * Returns the value of the '<em><b>Mitigation Level</b></em>' attribute.
	 * The default value is <code>"M-"</code>.
	 * The literals are from the enumeration {@link dmt.MitigationLevelEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mitigation Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mitigation Level</em>' attribute.
	 * @see dmt.MitigationLevelEnum
	 * @see #setMitigationLevel(MitigationLevelEnum)
	 * @see dmt.DmtPackage#getDeceptionMitigates_MitigationLevel()
	 * @model default="M-"
	 * @generated
	 */
	MitigationLevelEnum getMitigationLevel();

	/**
	 * Sets the value of the '{@link dmt.DeceptionMitigates#getMitigationLevel <em>Mitigation Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mitigation Level</em>' attribute.
	 * @see dmt.MitigationLevelEnum
	 * @see #getMitigationLevel()
	 * @generated
	 */
	void setMitigationLevel(MitigationLevelEnum value);

	/**
	 * Returns the value of the '<em><b>Targets Mitigable Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Targets Mitigable Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Targets Mitigable Node</em>' reference.
	 * @see #setTargetsMitigableNode(MitigableElement)
	 * @see dmt.DmtPackage#getDeceptionMitigates_TargetsMitigableNode()
	 * @model
	 * @generated
	 */
	MitigableElement getTargetsMitigableNode();

	/**
	 * Sets the value of the '{@link dmt.DeceptionMitigates#getTargetsMitigableNode <em>Targets Mitigable Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Targets Mitigable Node</em>' reference.
	 * @see #getTargetsMitigableNode()
	 * @generated
	 */
	void setTargetsMitigableNode(MitigableElement value);

} // DeceptionMitigates
