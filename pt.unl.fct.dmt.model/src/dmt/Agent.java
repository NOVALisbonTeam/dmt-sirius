/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Agent#getDef <em>Def</em>}</li>
 *   <li>{@link dmt.Agent#getLoad <em>Load</em>}</li>
 *   <li>{@link dmt.Agent#getSubagent <em>Subagent</em>}</li>
 *   <li>{@link dmt.Agent#getPerformsOperation <em>Performs Operation</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAgent()
 * @model abstract="true"
 * @generated
 */
public interface Agent extends GoalNode {
	/**
	 * Returns the value of the '<em><b>Def</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Def</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Def</em>' attribute.
	 * @see #setDef(String)
	 * @see dmt.DmtPackage#getAgent_Def()
	 * @model
	 * @generated
	 */
	String getDef();

	/**
	 * Sets the value of the '{@link dmt.Agent#getDef <em>Def</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Def</em>' attribute.
	 * @see #getDef()
	 * @generated
	 */
	void setDef(String value);

	/**
	 * Returns the value of the '<em><b>Load</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Load</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Load</em>' attribute.
	 * @see #setLoad(String)
	 * @see dmt.DmtPackage#getAgent_Load()
	 * @model
	 * @generated
	 */
	String getLoad();

	/**
	 * Sets the value of the '{@link dmt.Agent#getLoad <em>Load</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load</em>' attribute.
	 * @see #getLoad()
	 * @generated
	 */
	void setLoad(String value);

	/**
	 * Returns the value of the '<em><b>Subagent</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.Agent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subagent</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subagent</em>' containment reference list.
	 * @see dmt.DmtPackage#getAgent_Subagent()
	 * @model containment="true"
	 * @generated
	 */
	EList<Agent> getSubagent();

	/**
	 * Returns the value of the '<em><b>Performs Operation</b></em>' reference list.
	 * The list contents are of type {@link dmt.Operationable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Performs Operation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Performs Operation</em>' reference list.
	 * @see dmt.DmtPackage#getAgent_PerformsOperation()
	 * @model
	 * @generated
	 */
	EList<Operationable> getPerformsOperation();

} // Agent
