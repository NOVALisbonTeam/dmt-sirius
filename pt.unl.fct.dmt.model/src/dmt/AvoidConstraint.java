/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Avoid Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getAvoidConstraint()
 * @model
 * @generated
 */
public interface AvoidConstraint extends FeatureConstraint {
} // AvoidConstraint
