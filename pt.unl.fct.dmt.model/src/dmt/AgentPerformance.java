/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent Performance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.AgentPerformance#getAgentPerforms <em>Agent Performs</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAgentPerformance()
 * @model
 * @generated
 */
public interface AgentPerformance extends AgentRelation {
	/**
	 * Returns the value of the '<em><b>Agent Performs</b></em>' reference list.
	 * The list contents are of type {@link dmt.Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Agent Performs</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Performs</em>' reference list.
	 * @see dmt.DmtPackage#getAgentPerformance_AgentPerforms()
	 * @model required="true"
	 * @generated
	 */
	EList<Operation> getAgentPerforms();

} // AgentPerformance
