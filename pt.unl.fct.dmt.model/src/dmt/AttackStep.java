/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attack Step</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getAttackStep()
 * @model abstract="true"
 * @generated
 */
public interface AttackStep extends SecurityNode {
} // AttackStep
