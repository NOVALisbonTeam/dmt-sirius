/**
 */
package dmt.util;

import dmt.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage
 * @generated
 */
public class DmtAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DmtPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DmtAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DmtPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DmtSwitch<Adapter> modelSwitch =
		new DmtSwitch<Adapter>() {
			@Override
			public Adapter caseDMTModel(DMTModel object) {
				return createDMTModelAdapter();
			}
			@Override
			public Adapter caseGoal(Goal object) {
				return createGoalAdapter();
			}
			@Override
			public Adapter caseGoalRelationship(GoalRelationship object) {
				return createGoalRelationshipAdapter();
			}
			@Override
			public Adapter caseAgent(Agent object) {
				return createAgentAdapter();
			}
			@Override
			public Adapter caseOperationalization(Operationalization object) {
				return createOperationalizationAdapter();
			}
			@Override
			public Adapter caseSoftwareAgent(SoftwareAgent object) {
				return createSoftwareAgentAdapter();
			}
			@Override
			public Adapter caseEnvironmentAgent(EnvironmentAgent object) {
				return createEnvironmentAgentAdapter();
			}
			@Override
			public Adapter caseBehaviourGoal(BehaviourGoal object) {
				return createBehaviourGoalAdapter();
			}
			@Override
			public Adapter caseDomainDescription(DomainDescription object) {
				return createDomainDescriptionAdapter();
			}
			@Override
			public Adapter caseLeafGoal(LeafGoal object) {
				return createLeafGoalAdapter();
			}
			@Override
			public Adapter caseExpectation(Expectation object) {
				return createExpectationAdapter();
			}
			@Override
			public Adapter caseRequirement(Requirement object) {
				return createRequirementAdapter();
			}
			@Override
			public Adapter caseOperation(Operation object) {
				return createOperationAdapter();
			}
			@Override
			public Adapter caseORGoalRefinement(ORGoalRefinement object) {
				return createORGoalRefinementAdapter();
			}
			@Override
			public Adapter caseSecurityNode(SecurityNode object) {
				return createSecurityNodeAdapter();
			}
			@Override
			public Adapter caseMitigableElement(MitigableElement object) {
				return createMitigableElementAdapter();
			}
			@Override
			public Adapter caseAttack(Attack object) {
				return createAttackAdapter();
			}
			@Override
			public Adapter caseThreat(Threat object) {
				return createThreatAdapter();
			}
			@Override
			public Adapter caseVulnerability(Vulnerability object) {
				return createVulnerabilityAdapter();
			}
			@Override
			public Adapter caseSecurityControl(SecurityControl object) {
				return createSecurityControlAdapter();
			}
			@Override
			public Adapter caseRiskComponent(RiskComponent object) {
				return createRiskComponentAdapter();
			}
			@Override
			public Adapter caseAttacker(Attacker object) {
				return createAttackerAdapter();
			}
			@Override
			public Adapter caseAsset(Asset object) {
				return createAssetAdapter();
			}
			@Override
			public Adapter caseDeceptionNode(DeceptionNode object) {
				return createDeceptionNodeAdapter();
			}
			@Override
			public Adapter caseDeceptionTactic(DeceptionTactic object) {
				return createDeceptionTacticAdapter();
			}
			@Override
			public Adapter caseIVulnerability(IVulnerability object) {
				return createIVulnerabilityAdapter();
			}
			@Override
			public Adapter caseDeceptionTechnique(DeceptionTechnique object) {
				return createDeceptionTechniqueAdapter();
			}
			@Override
			public Adapter caseDeceptionStory(DeceptionStory object) {
				return createDeceptionStoryAdapter();
			}
			@Override
			public Adapter caseDeceptionBias(DeceptionBias object) {
				return createDeceptionBiasAdapter();
			}
			@Override
			public Adapter caseArtificialElement(ArtificialElement object) {
				return createArtificialElementAdapter();
			}
			@Override
			public Adapter caseMitigation(Mitigation object) {
				return createMitigationAdapter();
			}
			@Override
			public Adapter caseInteractionNodes(InteractionNodes object) {
				return createInteractionNodesAdapter();
			}
			@Override
			public Adapter caseInteractionObject(InteractionObject object) {
				return createInteractionObjectAdapter();
			}
			@Override
			public Adapter caseLink(Link object) {
				return createLinkAdapter();
			}
			@Override
			public Adapter caseSimulation(Simulation object) {
				return createSimulationAdapter();
			}
			@Override
			public Adapter caseDissimulation(Dissimulation object) {
				return createDissimulationAdapter();
			}
			@Override
			public Adapter caseNode(Node object) {
				return createNodeAdapter();
			}
			@Override
			public Adapter caseAgentAssignment(AgentAssignment object) {
				return createAgentAssignmentAdapter();
			}
			@Override
			public Adapter caseANDGoalRefinement(ANDGoalRefinement object) {
				return createANDGoalRefinementAdapter();
			}
			@Override
			public Adapter caseConflictGoalRefinement(ConflictGoalRefinement object) {
				return createConflictGoalRefinementAdapter();
			}
			@Override
			public Adapter caseAgentMonitoring(AgentMonitoring object) {
				return createAgentMonitoringAdapter();
			}
			@Override
			public Adapter caseAgentControl(AgentControl object) {
				return createAgentControlAdapter();
			}
			@Override
			public Adapter caseAgentRelation(AgentRelation object) {
				return createAgentRelationAdapter();
			}
			@Override
			public Adapter caseDeceptionMetric(DeceptionMetric object) {
				return createDeceptionMetricAdapter();
			}
			@Override
			public Adapter caseDeceptionChannel(DeceptionChannel object) {
				return createDeceptionChannelAdapter();
			}
			@Override
			public Adapter caseAttackVector(AttackVector object) {
				return createAttackVectorAdapter();
			}
			@Override
			public Adapter caseAProperty(AProperty object) {
				return createAPropertyAdapter();
			}
			@Override
			public Adapter caseAttackStepConnector(AttackStepConnector object) {
				return createAttackStepConnectorAdapter();
			}
			@Override
			public Adapter caseInitialAttackStep(InitialAttackStep object) {
				return createInitialAttackStepAdapter();
			}
			@Override
			public Adapter caseEndAttackStep(EndAttackStep object) {
				return createEndAttackStepAdapter();
			}
			@Override
			public Adapter caseAttackStep(AttackStep object) {
				return createAttackStepAdapter();
			}
			@Override
			public Adapter caseAttackTree(AttackTree object) {
				return createAttackTreeAdapter();
			}
			@Override
			public Adapter caseGoalNode(GoalNode object) {
				return createGoalNodeAdapter();
			}
			@Override
			public Adapter caseSoftGoal(SoftGoal object) {
				return createSoftGoalAdapter();
			}
			@Override
			public Adapter caseDeceptionBehaviourGoal(DeceptionBehaviourGoal object) {
				return createDeceptionBehaviourGoalAdapter();
			}
			@Override
			public Adapter caseDeceptionRisk(DeceptionRisk object) {
				return createDeceptionRiskAdapter();
			}
			@Override
			public Adapter caseDeceptionStrategy(DeceptionStrategy object) {
				return createDeceptionStrategyAdapter();
			}
			@Override
			public Adapter caseDeceptionTacticMechanism(DeceptionTacticMechanism object) {
				return createDeceptionTacticMechanismAdapter();
			}
			@Override
			public Adapter caseStrategyNode(StrategyNode object) {
				return createStrategyNodeAdapter();
			}
			@Override
			public Adapter caseFeature(Feature object) {
				return createFeatureAdapter();
			}
			@Override
			public Adapter caseStrategyRelationship(StrategyRelationship object) {
				return createStrategyRelationshipAdapter();
			}
			@Override
			public Adapter caseFeatureConstraint(FeatureConstraint object) {
				return createFeatureConstraintAdapter();
			}
			@Override
			public Adapter caseAbstractFeature(AbstractFeature object) {
				return createAbstractFeatureAdapter();
			}
			@Override
			public Adapter caseRequiredConstraint(RequiredConstraint object) {
				return createRequiredConstraintAdapter();
			}
			@Override
			public Adapter caseExcludeConstraint(ExcludeConstraint object) {
				return createExcludeConstraintAdapter();
			}
			@Override
			public Adapter caseBenefitConstraint(BenefitConstraint object) {
				return createBenefitConstraintAdapter();
			}
			@Override
			public Adapter caseAvoidConstraint(AvoidConstraint object) {
				return createAvoidConstraintAdapter();
			}
			@Override
			public Adapter caseNodeRefinement(NodeRefinement object) {
				return createNodeRefinementAdapter();
			}
			@Override
			public Adapter caseTacticRefinement(TacticRefinement object) {
				return createTacticRefinementAdapter();
			}
			@Override
			public Adapter caseFeatureRefinement(FeatureRefinement object) {
				return createFeatureRefinementAdapter();
			}
			@Override
			public Adapter caseStrategyTacticRefinement(StrategyTacticRefinement object) {
				return createStrategyTacticRefinementAdapter();
			}
			@Override
			public Adapter caseTacticFeatureRefinement(TacticFeatureRefinement object) {
				return createTacticFeatureRefinementAdapter();
			}
			@Override
			public Adapter caseDeceptionRelationship(DeceptionRelationship object) {
				return createDeceptionRelationshipAdapter();
			}
			@Override
			public Adapter caseMitigator(Mitigator object) {
				return createMitigatorAdapter();
			}
			@Override
			public Adapter caseSecurityRelationship(SecurityRelationship object) {
				return createSecurityRelationshipAdapter();
			}
			@Override
			public Adapter caseObstructionElement(ObstructionElement object) {
				return createObstructionElementAdapter();
			}
			@Override
			public Adapter caseORObstructionRefinement(ORObstructionRefinement object) {
				return createORObstructionRefinementAdapter();
			}
			@Override
			public Adapter caseANDObstructionRefinement(ANDObstructionRefinement object) {
				return createANDObstructionRefinementAdapter();
			}
			@Override
			public Adapter caseObstacle(Obstacle object) {
				return createObstacleAdapter();
			}
			@Override
			public Adapter caseInfluenceNode(InfluenceNode object) {
				return createInfluenceNodeAdapter();
			}
			@Override
			public Adapter caseQualityAttribute(QualityAttribute object) {
				return createQualityAttributeAdapter();
			}
			@Override
			public Adapter caseInfluenceRelationship(InfluenceRelationship object) {
				return createInfluenceRelationshipAdapter();
			}
			@Override
			public Adapter caseInfluence(Influence object) {
				return createInfluenceAdapter();
			}
			@Override
			public Adapter casePreference(Preference object) {
				return createPreferenceAdapter();
			}
			@Override
			public Adapter caseEvent(Event object) {
				return createEventAdapter();
			}
			@Override
			public Adapter caseDeceptionTacticEvent(DeceptionTacticEvent object) {
				return createDeceptionTacticEventAdapter();
			}
			@Override
			public Adapter caseEnvironmentEvent(EnvironmentEvent object) {
				return createEnvironmentEventAdapter();
			}
			@Override
			public Adapter caseObstructedElement(ObstructedElement object) {
				return createObstructedElementAdapter();
			}
			@Override
			public Adapter caseVulnerable(Vulnerable object) {
				return createVulnerableAdapter();
			}
			@Override
			public Adapter caseGoalRefinable(GoalRefinable object) {
				return createGoalRefinableAdapter();
			}
			@Override
			public Adapter caseBelief(Belief object) {
				return createBeliefAdapter();
			}
			@Override
			public Adapter caseDeceptionGoal(DeceptionGoal object) {
				return createDeceptionGoalAdapter();
			}
			@Override
			public Adapter caseDeceptionSoftGoal(DeceptionSoftGoal object) {
				return createDeceptionSoftGoalAdapter();
			}
			@Override
			public Adapter caseConcreteAsset(ConcreteAsset object) {
				return createConcreteAssetAdapter();
			}
			@Override
			public Adapter caseOperationable(Operationable object) {
				return createOperationableAdapter();
			}
			@Override
			public Adapter caseGoalContainer(GoalContainer object) {
				return createGoalContainerAdapter();
			}
			@Override
			public Adapter caseNodeSelection(NodeSelection object) {
				return createNodeSelectionAdapter();
			}
			@Override
			public Adapter caseDeceptionTacticStrategy(DeceptionTacticStrategy object) {
				return createDeceptionTacticStrategyAdapter();
			}
			@Override
			public Adapter caseDeceptionStrategyTacticRefinement(DeceptionStrategyTacticRefinement object) {
				return createDeceptionStrategyTacticRefinementAdapter();
			}
			@Override
			public Adapter caseDeceptionTacticStrategyRefinement(DeceptionTacticStrategyRefinement object) {
				return createDeceptionTacticStrategyRefinementAdapter();
			}
			@Override
			public Adapter caseDeceptionStrategyRefinement(DeceptionStrategyRefinement object) {
				return createDeceptionStrategyRefinementAdapter();
			}
			@Override
			public Adapter caseSimulationHasArtificialElement(SimulationHasArtificialElement object) {
				return createSimulationHasArtificialElementAdapter();
			}
			@Override
			public Adapter caseProperty(Property object) {
				return createPropertyAdapter();
			}
			@Override
			public Adapter caseAnnotation(Annotation object) {
				return createAnnotationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link dmt.DMTModel <em>DMT Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DMTModel
	 * @generated
	 */
	public Adapter createDMTModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Goal <em>Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Goal
	 * @generated
	 */
	public Adapter createGoalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.GoalRelationship <em>Goal Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.GoalRelationship
	 * @generated
	 */
	public Adapter createGoalRelationshipAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Agent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Agent
	 * @generated
	 */
	public Adapter createAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Operationalization <em>Operationalization</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Operationalization
	 * @generated
	 */
	public Adapter createOperationalizationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.SoftwareAgent <em>Software Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.SoftwareAgent
	 * @generated
	 */
	public Adapter createSoftwareAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.EnvironmentAgent <em>Environment Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.EnvironmentAgent
	 * @generated
	 */
	public Adapter createEnvironmentAgentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.BehaviourGoal <em>Behaviour Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.BehaviourGoal
	 * @generated
	 */
	public Adapter createBehaviourGoalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DomainDescription <em>Domain Description</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DomainDescription
	 * @generated
	 */
	public Adapter createDomainDescriptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.LeafGoal <em>Leaf Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.LeafGoal
	 * @generated
	 */
	public Adapter createLeafGoalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Expectation <em>Expectation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Expectation
	 * @generated
	 */
	public Adapter createExpectationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Requirement <em>Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Requirement
	 * @generated
	 */
	public Adapter createRequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Operation
	 * @generated
	 */
	public Adapter createOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ORGoalRefinement <em>OR Goal Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ORGoalRefinement
	 * @generated
	 */
	public Adapter createORGoalRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.SecurityNode <em>Security Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.SecurityNode
	 * @generated
	 */
	public Adapter createSecurityNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.MitigableElement <em>Mitigable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.MitigableElement
	 * @generated
	 */
	public Adapter createMitigableElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Attack <em>Attack</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Attack
	 * @generated
	 */
	public Adapter createAttackAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Threat <em>Threat</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Threat
	 * @generated
	 */
	public Adapter createThreatAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Vulnerability <em>Vulnerability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Vulnerability
	 * @generated
	 */
	public Adapter createVulnerabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.SecurityControl <em>Security Control</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.SecurityControl
	 * @generated
	 */
	public Adapter createSecurityControlAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.RiskComponent <em>Risk Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.RiskComponent
	 * @generated
	 */
	public Adapter createRiskComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Attacker <em>Attacker</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Attacker
	 * @generated
	 */
	public Adapter createAttackerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Asset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Asset
	 * @generated
	 */
	public Adapter createAssetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionNode <em>Deception Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionNode
	 * @generated
	 */
	public Adapter createDeceptionNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionTactic <em>Deception Tactic</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionTactic
	 * @generated
	 */
	public Adapter createDeceptionTacticAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.IVulnerability <em>IVulnerability</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.IVulnerability
	 * @generated
	 */
	public Adapter createIVulnerabilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionTechnique <em>Deception Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionTechnique
	 * @generated
	 */
	public Adapter createDeceptionTechniqueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionStory <em>Deception Story</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionStory
	 * @generated
	 */
	public Adapter createDeceptionStoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionBias <em>Deception Bias</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionBias
	 * @generated
	 */
	public Adapter createDeceptionBiasAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ArtificialElement <em>Artificial Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ArtificialElement
	 * @generated
	 */
	public Adapter createArtificialElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.InteractionNodes <em>Interaction Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.InteractionNodes
	 * @generated
	 */
	public Adapter createInteractionNodesAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.InteractionObject <em>Interaction Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.InteractionObject
	 * @generated
	 */
	public Adapter createInteractionObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Link <em>Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Link
	 * @generated
	 */
	public Adapter createLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Mitigation <em>Mitigation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Mitigation
	 * @generated
	 */
	public Adapter createMitigationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Simulation <em>Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Simulation
	 * @generated
	 */
	public Adapter createSimulationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Dissimulation <em>Dissimulation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Dissimulation
	 * @generated
	 */
	public Adapter createDissimulationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Node
	 * @generated
	 */
	public Adapter createNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AgentAssignment <em>Agent Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AgentAssignment
	 * @generated
	 */
	public Adapter createAgentAssignmentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ANDGoalRefinement <em>AND Goal Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ANDGoalRefinement
	 * @generated
	 */
	public Adapter createANDGoalRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ConflictGoalRefinement <em>Conflict Goal Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ConflictGoalRefinement
	 * @generated
	 */
	public Adapter createConflictGoalRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AgentMonitoring <em>Agent Monitoring</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AgentMonitoring
	 * @generated
	 */
	public Adapter createAgentMonitoringAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AgentControl <em>Agent Control</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AgentControl
	 * @generated
	 */
	public Adapter createAgentControlAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AgentRelation <em>Agent Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AgentRelation
	 * @generated
	 */
	public Adapter createAgentRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionMetric <em>Deception Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionMetric
	 * @generated
	 */
	public Adapter createDeceptionMetricAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionChannel <em>Deception Channel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionChannel
	 * @generated
	 */
	public Adapter createDeceptionChannelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AttackVector <em>Attack Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AttackVector
	 * @generated
	 */
	public Adapter createAttackVectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AProperty <em>AProperty</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AProperty
	 * @generated
	 */
	public Adapter createAPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AttackStepConnector <em>Attack Step Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AttackStepConnector
	 * @generated
	 */
	public Adapter createAttackStepConnectorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.InitialAttackStep <em>Initial Attack Step</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.InitialAttackStep
	 * @generated
	 */
	public Adapter createInitialAttackStepAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.EndAttackStep <em>End Attack Step</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.EndAttackStep
	 * @generated
	 */
	public Adapter createEndAttackStepAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AttackStep <em>Attack Step</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AttackStep
	 * @generated
	 */
	public Adapter createAttackStepAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AttackTree <em>Attack Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AttackTree
	 * @generated
	 */
	public Adapter createAttackTreeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.GoalNode <em>Goal Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.GoalNode
	 * @generated
	 */
	public Adapter createGoalNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.SoftGoal <em>Soft Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.SoftGoal
	 * @generated
	 */
	public Adapter createSoftGoalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionBehaviourGoal <em>Deception Behaviour Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionBehaviourGoal
	 * @generated
	 */
	public Adapter createDeceptionBehaviourGoalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionSoftGoal <em>Deception Soft Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionSoftGoal
	 * @generated
	 */
	public Adapter createDeceptionSoftGoalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ConcreteAsset <em>Concrete Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ConcreteAsset
	 * @generated
	 */
	public Adapter createConcreteAssetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Operationable <em>Operationable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Operationable
	 * @generated
	 */
	public Adapter createOperationableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.GoalContainer <em>Goal Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.GoalContainer
	 * @generated
	 */
	public Adapter createGoalContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.NodeSelection <em>Node Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.NodeSelection
	 * @generated
	 */
	public Adapter createNodeSelectionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionTacticStrategy <em>Deception Tactic Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionTacticStrategy
	 * @generated
	 */
	public Adapter createDeceptionTacticStrategyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionStrategyTacticRefinement <em>Deception Strategy Tactic Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionStrategyTacticRefinement
	 * @generated
	 */
	public Adapter createDeceptionStrategyTacticRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionTacticStrategyRefinement <em>Deception Tactic Strategy Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionTacticStrategyRefinement
	 * @generated
	 */
	public Adapter createDeceptionTacticStrategyRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionStrategyRefinement <em>Deception Strategy Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionStrategyRefinement
	 * @generated
	 */
	public Adapter createDeceptionStrategyRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.SimulationHasArtificialElement <em>Simulation Has Artificial Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.SimulationHasArtificialElement
	 * @generated
	 */
	public Adapter createSimulationHasArtificialElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Property
	 * @generated
	 */
	public Adapter createPropertyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Annotation
	 * @generated
	 */
	public Adapter createAnnotationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionGoal <em>Deception Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionGoal
	 * @generated
	 */
	public Adapter createDeceptionGoalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionRisk <em>Deception Risk</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionRisk
	 * @generated
	 */
	public Adapter createDeceptionRiskAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionStrategy <em>Deception Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionStrategy
	 * @generated
	 */
	public Adapter createDeceptionStrategyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionTacticMechanism <em>Deception Tactic Mechanism</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionTacticMechanism
	 * @generated
	 */
	public Adapter createDeceptionTacticMechanismAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.StrategyNode <em>Strategy Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.StrategyNode
	 * @generated
	 */
	public Adapter createStrategyNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Feature
	 * @generated
	 */
	public Adapter createFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.StrategyRelationship <em>Strategy Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.StrategyRelationship
	 * @generated
	 */
	public Adapter createStrategyRelationshipAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.FeatureConstraint <em>Feature Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.FeatureConstraint
	 * @generated
	 */
	public Adapter createFeatureConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AbstractFeature <em>Abstract Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AbstractFeature
	 * @generated
	 */
	public Adapter createAbstractFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.RequiredConstraint <em>Required Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.RequiredConstraint
	 * @generated
	 */
	public Adapter createRequiredConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ExcludeConstraint <em>Exclude Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ExcludeConstraint
	 * @generated
	 */
	public Adapter createExcludeConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.BenefitConstraint <em>Benefit Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.BenefitConstraint
	 * @generated
	 */
	public Adapter createBenefitConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.AvoidConstraint <em>Avoid Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.AvoidConstraint
	 * @generated
	 */
	public Adapter createAvoidConstraintAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.NodeRefinement <em>Node Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.NodeRefinement
	 * @generated
	 */
	public Adapter createNodeRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.TacticRefinement <em>Tactic Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.TacticRefinement
	 * @generated
	 */
	public Adapter createTacticRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.FeatureRefinement <em>Feature Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.FeatureRefinement
	 * @generated
	 */
	public Adapter createFeatureRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.StrategyTacticRefinement <em>Strategy Tactic Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.StrategyTacticRefinement
	 * @generated
	 */
	public Adapter createStrategyTacticRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.TacticFeatureRefinement <em>Tactic Feature Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.TacticFeatureRefinement
	 * @generated
	 */
	public Adapter createTacticFeatureRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionRelationship <em>Deception Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionRelationship
	 * @generated
	 */
	public Adapter createDeceptionRelationshipAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Mitigator <em>Mitigator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Mitigator
	 * @generated
	 */
	public Adapter createMitigatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.SecurityRelationship <em>Security Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.SecurityRelationship
	 * @generated
	 */
	public Adapter createSecurityRelationshipAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ObstructionElement <em>Obstruction Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ObstructionElement
	 * @generated
	 */
	public Adapter createObstructionElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ORObstructionRefinement <em>OR Obstruction Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ORObstructionRefinement
	 * @generated
	 */
	public Adapter createORObstructionRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ANDObstructionRefinement <em>AND Obstruction Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ANDObstructionRefinement
	 * @generated
	 */
	public Adapter createANDObstructionRefinementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Obstacle <em>Obstacle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Obstacle
	 * @generated
	 */
	public Adapter createObstacleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.InfluenceNode <em>Influence Node</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.InfluenceNode
	 * @generated
	 */
	public Adapter createInfluenceNodeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.QualityAttribute <em>Quality Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.QualityAttribute
	 * @generated
	 */
	public Adapter createQualityAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.InfluenceRelationship <em>Influence Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.InfluenceRelationship
	 * @generated
	 */
	public Adapter createInfluenceRelationshipAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Influence <em>Influence</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Influence
	 * @generated
	 */
	public Adapter createInfluenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Preference <em>Preference</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Preference
	 * @generated
	 */
	public Adapter createPreferenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Event
	 * @generated
	 */
	public Adapter createEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.DeceptionTacticEvent <em>Deception Tactic Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.DeceptionTacticEvent
	 * @generated
	 */
	public Adapter createDeceptionTacticEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.EnvironmentEvent <em>Environment Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.EnvironmentEvent
	 * @generated
	 */
	public Adapter createEnvironmentEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.ObstructedElement <em>Obstructed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.ObstructedElement
	 * @generated
	 */
	public Adapter createObstructedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Vulnerable <em>Vulnerable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Vulnerable
	 * @generated
	 */
	public Adapter createVulnerableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.GoalRefinable <em>Goal Refinable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.GoalRefinable
	 * @generated
	 */
	public Adapter createGoalRefinableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link dmt.Belief <em>Belief</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see dmt.Belief
	 * @generated
	 */
	public Adapter createBeliefAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DmtAdapterFactory
