/**
 */
package dmt.util;

import dmt.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage
 * @generated
 */
public class DmtSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DmtPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DmtSwitch() {
		if (modelPackage == null) {
			modelPackage = DmtPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DmtPackage.DMT_MODEL: {
				DMTModel dmtModel = (DMTModel)theEObject;
				T result = caseDMTModel(dmtModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.GOAL: {
				Goal goal = (Goal)theEObject;
				T result = caseGoal(goal);
				if (result == null) result = caseObstructedElement(goal);
				if (result == null) result = caseVulnerable(goal);
				if (result == null) result = caseGoalRefinable(goal);
				if (result == null) result = caseGoalNode(goal);
				if (result == null) result = caseNode(goal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.GOAL_RELATIONSHIP: {
				GoalRelationship goalRelationship = (GoalRelationship)theEObject;
				T result = caseGoalRelationship(goalRelationship);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.AGENT: {
				Agent agent = (Agent)theEObject;
				T result = caseAgent(agent);
				if (result == null) result = caseGoalNode(agent);
				if (result == null) result = caseNode(agent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.OPERATIONALIZATION: {
				Operationalization operationalization = (Operationalization)theEObject;
				T result = caseOperationalization(operationalization);
				if (result == null) result = caseGoalRelationship(operationalization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.SOFTWARE_AGENT: {
				SoftwareAgent softwareAgent = (SoftwareAgent)theEObject;
				T result = caseSoftwareAgent(softwareAgent);
				if (result == null) result = caseAgent(softwareAgent);
				if (result == null) result = caseGoalNode(softwareAgent);
				if (result == null) result = caseNode(softwareAgent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ENVIRONMENT_AGENT: {
				EnvironmentAgent environmentAgent = (EnvironmentAgent)theEObject;
				T result = caseEnvironmentAgent(environmentAgent);
				if (result == null) result = caseAgent(environmentAgent);
				if (result == null) result = caseGoalNode(environmentAgent);
				if (result == null) result = caseNode(environmentAgent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.BEHAVIOUR_GOAL: {
				BehaviourGoal behaviourGoal = (BehaviourGoal)theEObject;
				T result = caseBehaviourGoal(behaviourGoal);
				if (result == null) result = caseGoal(behaviourGoal);
				if (result == null) result = caseObstructedElement(behaviourGoal);
				if (result == null) result = caseVulnerable(behaviourGoal);
				if (result == null) result = caseGoalRefinable(behaviourGoal);
				if (result == null) result = caseGoalNode(behaviourGoal);
				if (result == null) result = caseNode(behaviourGoal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DOMAIN_DESCRIPTION: {
				DomainDescription domainDescription = (DomainDescription)theEObject;
				T result = caseDomainDescription(domainDescription);
				if (result == null) result = caseObstructedElement(domainDescription);
				if (result == null) result = caseVulnerable(domainDescription);
				if (result == null) result = caseGoalRefinable(domainDescription);
				if (result == null) result = caseGoalNode(domainDescription);
				if (result == null) result = caseNode(domainDescription);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.LEAF_GOAL: {
				LeafGoal leafGoal = (LeafGoal)theEObject;
				T result = caseLeafGoal(leafGoal);
				if (result == null) result = caseGoal(leafGoal);
				if (result == null) result = caseObstructedElement(leafGoal);
				if (result == null) result = caseVulnerable(leafGoal);
				if (result == null) result = caseGoalRefinable(leafGoal);
				if (result == null) result = caseGoalNode(leafGoal);
				if (result == null) result = caseNode(leafGoal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.EXPECTATION: {
				Expectation expectation = (Expectation)theEObject;
				T result = caseExpectation(expectation);
				if (result == null) result = caseLeafGoal(expectation);
				if (result == null) result = caseGoal(expectation);
				if (result == null) result = caseObstructedElement(expectation);
				if (result == null) result = caseVulnerable(expectation);
				if (result == null) result = caseGoalRefinable(expectation);
				if (result == null) result = caseGoalNode(expectation);
				if (result == null) result = caseNode(expectation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.REQUIREMENT: {
				Requirement requirement = (Requirement)theEObject;
				T result = caseRequirement(requirement);
				if (result == null) result = caseLeafGoal(requirement);
				if (result == null) result = caseGoal(requirement);
				if (result == null) result = caseObstructedElement(requirement);
				if (result == null) result = caseVulnerable(requirement);
				if (result == null) result = caseGoalRefinable(requirement);
				if (result == null) result = caseGoalNode(requirement);
				if (result == null) result = caseNode(requirement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.OPERATION: {
				Operation operation = (Operation)theEObject;
				T result = caseOperation(operation);
				if (result == null) result = caseVulnerable(operation);
				if (result == null) result = caseOperationable(operation);
				if (result == null) result = caseGoalNode(operation);
				if (result == null) result = caseNode(operation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.OR_GOAL_REFINEMENT: {
				ORGoalRefinement orGoalRefinement = (ORGoalRefinement)theEObject;
				T result = caseORGoalRefinement(orGoalRefinement);
				if (result == null) result = caseGoalRelationship(orGoalRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.SECURITY_NODE: {
				SecurityNode securityNode = (SecurityNode)theEObject;
				T result = caseSecurityNode(securityNode);
				if (result == null) result = caseNode(securityNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.MITIGABLE_ELEMENT: {
				MitigableElement mitigableElement = (MitigableElement)theEObject;
				T result = caseMitigableElement(mitigableElement);
				if (result == null) result = caseSecurityNode(mitigableElement);
				if (result == null) result = caseNode(mitigableElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ATTACK: {
				Attack attack = (Attack)theEObject;
				T result = caseAttack(attack);
				if (result == null) result = caseMitigableElement(attack);
				if (result == null) result = caseSecurityNode(attack);
				if (result == null) result = caseNode(attack);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.THREAT: {
				Threat threat = (Threat)theEObject;
				T result = caseThreat(threat);
				if (result == null) result = caseMitigableElement(threat);
				if (result == null) result = caseObstructionElement(threat);
				if (result == null) result = caseSecurityNode(threat);
				if (result == null) result = caseGoalNode(threat);
				if (result == null) result = caseNode(threat);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.VULNERABILITY: {
				Vulnerability vulnerability = (Vulnerability)theEObject;
				T result = caseVulnerability(vulnerability);
				if (result == null) result = caseMitigableElement(vulnerability);
				if (result == null) result = caseSecurityNode(vulnerability);
				if (result == null) result = caseNode(vulnerability);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.SECURITY_CONTROL: {
				SecurityControl securityControl = (SecurityControl)theEObject;
				T result = caseSecurityControl(securityControl);
				if (result == null) result = caseSecurityNode(securityControl);
				if (result == null) result = caseMitigator(securityControl);
				if (result == null) result = caseNode(securityControl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.RISK_COMPONENT: {
				RiskComponent riskComponent = (RiskComponent)theEObject;
				T result = caseRiskComponent(riskComponent);
				if (result == null) result = caseMitigableElement(riskComponent);
				if (result == null) result = caseSecurityNode(riskComponent);
				if (result == null) result = caseNode(riskComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ATTACKER: {
				Attacker attacker = (Attacker)theEObject;
				T result = caseAttacker(attacker);
				if (result == null) result = caseSecurityNode(attacker);
				if (result == null) result = caseNode(attacker);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ASSET: {
				Asset asset = (Asset)theEObject;
				T result = caseAsset(asset);
				if (result == null) result = caseSecurityNode(asset);
				if (result == null) result = caseNode(asset);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_NODE: {
				DeceptionNode deceptionNode = (DeceptionNode)theEObject;
				T result = caseDeceptionNode(deceptionNode);
				if (result == null) result = caseNode(deceptionNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_TACTIC: {
				DeceptionTactic deceptionTactic = (DeceptionTactic)theEObject;
				T result = caseDeceptionTactic(deceptionTactic);
				if (result == null) result = caseDeceptionNode(deceptionTactic);
				if (result == null) result = caseMitigator(deceptionTactic);
				if (result == null) result = caseOperationable(deceptionTactic);
				if (result == null) result = caseGoalNode(deceptionTactic);
				if (result == null) result = caseNode(deceptionTactic);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.IVULNERABILITY: {
				IVulnerability iVulnerability = (IVulnerability)theEObject;
				T result = caseIVulnerability(iVulnerability);
				if (result == null) result = caseDeceptionNode(iVulnerability);
				if (result == null) result = caseNode(iVulnerability);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_TECHNIQUE: {
				DeceptionTechnique deceptionTechnique = (DeceptionTechnique)theEObject;
				T result = caseDeceptionTechnique(deceptionTechnique);
				if (result == null) result = caseDeceptionNode(deceptionTechnique);
				if (result == null) result = caseNode(deceptionTechnique);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_STORY: {
				DeceptionStory deceptionStory = (DeceptionStory)theEObject;
				T result = caseDeceptionStory(deceptionStory);
				if (result == null) result = caseDeceptionNode(deceptionStory);
				if (result == null) result = caseNode(deceptionStory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_BIAS: {
				DeceptionBias deceptionBias = (DeceptionBias)theEObject;
				T result = caseDeceptionBias(deceptionBias);
				if (result == null) result = caseDeceptionNode(deceptionBias);
				if (result == null) result = caseNode(deceptionBias);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ARTIFICIAL_ELEMENT: {
				ArtificialElement artificialElement = (ArtificialElement)theEObject;
				T result = caseArtificialElement(artificialElement);
				if (result == null) result = caseDeceptionNode(artificialElement);
				if (result == null) result = caseAsset(artificialElement);
				if (result == null) result = caseSecurityNode(artificialElement);
				if (result == null) result = caseNode(artificialElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.MITIGATION: {
				Mitigation mitigation = (Mitigation)theEObject;
				T result = caseMitigation(mitigation);
				if (result == null) result = caseSecurityRelationship(mitigation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.INTERACTION_NODES: {
				InteractionNodes interactionNodes = (InteractionNodes)theEObject;
				T result = caseInteractionNodes(interactionNodes);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.INTERACTION_OBJECT: {
				InteractionObject interactionObject = (InteractionObject)theEObject;
				T result = caseInteractionObject(interactionObject);
				if (result == null) result = caseInteractionNodes(interactionObject);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.LINK: {
				Link link = (Link)theEObject;
				T result = caseLink(link);
				if (result == null) result = caseInteractionNodes(link);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.SIMULATION: {
				Simulation simulation = (Simulation)theEObject;
				T result = caseSimulation(simulation);
				if (result == null) result = caseDeceptionTechnique(simulation);
				if (result == null) result = caseDeceptionNode(simulation);
				if (result == null) result = caseNode(simulation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DISSIMULATION: {
				Dissimulation dissimulation = (Dissimulation)theEObject;
				T result = caseDissimulation(dissimulation);
				if (result == null) result = caseDeceptionTechnique(dissimulation);
				if (result == null) result = caseDeceptionNode(dissimulation);
				if (result == null) result = caseNode(dissimulation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.NODE: {
				Node node = (Node)theEObject;
				T result = caseNode(node);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.AGENT_ASSIGNMENT: {
				AgentAssignment agentAssignment = (AgentAssignment)theEObject;
				T result = caseAgentAssignment(agentAssignment);
				if (result == null) result = caseAgentRelation(agentAssignment);
				if (result == null) result = caseGoalRelationship(agentAssignment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.AND_GOAL_REFINEMENT: {
				ANDGoalRefinement andGoalRefinement = (ANDGoalRefinement)theEObject;
				T result = caseANDGoalRefinement(andGoalRefinement);
				if (result == null) result = caseGoalRelationship(andGoalRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.CONFLICT_GOAL_REFINEMENT: {
				ConflictGoalRefinement conflictGoalRefinement = (ConflictGoalRefinement)theEObject;
				T result = caseConflictGoalRefinement(conflictGoalRefinement);
				if (result == null) result = caseGoalRelationship(conflictGoalRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.AGENT_MONITORING: {
				AgentMonitoring agentMonitoring = (AgentMonitoring)theEObject;
				T result = caseAgentMonitoring(agentMonitoring);
				if (result == null) result = caseAgentRelation(agentMonitoring);
				if (result == null) result = caseGoalRelationship(agentMonitoring);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.AGENT_CONTROL: {
				AgentControl agentControl = (AgentControl)theEObject;
				T result = caseAgentControl(agentControl);
				if (result == null) result = caseAgentRelation(agentControl);
				if (result == null) result = caseGoalRelationship(agentControl);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.AGENT_RELATION: {
				AgentRelation agentRelation = (AgentRelation)theEObject;
				T result = caseAgentRelation(agentRelation);
				if (result == null) result = caseGoalRelationship(agentRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_METRIC: {
				DeceptionMetric deceptionMetric = (DeceptionMetric)theEObject;
				T result = caseDeceptionMetric(deceptionMetric);
				if (result == null) result = caseDeceptionNode(deceptionMetric);
				if (result == null) result = caseNode(deceptionMetric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_CHANNEL: {
				DeceptionChannel deceptionChannel = (DeceptionChannel)theEObject;
				T result = caseDeceptionChannel(deceptionChannel);
				if (result == null) result = caseDeceptionNode(deceptionChannel);
				if (result == null) result = caseNode(deceptionChannel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ATTACK_VECTOR: {
				AttackVector attackVector = (AttackVector)theEObject;
				T result = caseAttackVector(attackVector);
				if (result == null) result = caseSecurityNode(attackVector);
				if (result == null) result = caseNode(attackVector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.APROPERTY: {
				AProperty aProperty = (AProperty)theEObject;
				T result = caseAProperty(aProperty);
				if (result == null) result = caseSecurityNode(aProperty);
				if (result == null) result = caseNode(aProperty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ATTACK_STEP_CONNECTOR: {
				AttackStepConnector attackStepConnector = (AttackStepConnector)theEObject;
				T result = caseAttackStepConnector(attackStepConnector);
				if (result == null) result = caseAttackStep(attackStepConnector);
				if (result == null) result = caseSecurityNode(attackStepConnector);
				if (result == null) result = caseNode(attackStepConnector);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.INITIAL_ATTACK_STEP: {
				InitialAttackStep initialAttackStep = (InitialAttackStep)theEObject;
				T result = caseInitialAttackStep(initialAttackStep);
				if (result == null) result = caseAttackStep(initialAttackStep);
				if (result == null) result = caseSecurityNode(initialAttackStep);
				if (result == null) result = caseNode(initialAttackStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.END_ATTACK_STEP: {
				EndAttackStep endAttackStep = (EndAttackStep)theEObject;
				T result = caseEndAttackStep(endAttackStep);
				if (result == null) result = caseAttackStep(endAttackStep);
				if (result == null) result = caseSecurityNode(endAttackStep);
				if (result == null) result = caseNode(endAttackStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ATTACK_STEP: {
				AttackStep attackStep = (AttackStep)theEObject;
				T result = caseAttackStep(attackStep);
				if (result == null) result = caseSecurityNode(attackStep);
				if (result == null) result = caseNode(attackStep);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ATTACK_TREE: {
				AttackTree attackTree = (AttackTree)theEObject;
				T result = caseAttackTree(attackTree);
				if (result == null) result = caseSecurityNode(attackTree);
				if (result == null) result = caseNode(attackTree);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.GOAL_NODE: {
				GoalNode goalNode = (GoalNode)theEObject;
				T result = caseGoalNode(goalNode);
				if (result == null) result = caseNode(goalNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.SOFT_GOAL: {
				SoftGoal softGoal = (SoftGoal)theEObject;
				T result = caseSoftGoal(softGoal);
				if (result == null) result = caseGoal(softGoal);
				if (result == null) result = caseObstructedElement(softGoal);
				if (result == null) result = caseVulnerable(softGoal);
				if (result == null) result = caseGoalRefinable(softGoal);
				if (result == null) result = caseGoalNode(softGoal);
				if (result == null) result = caseNode(softGoal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_BEHAVIOUR_GOAL: {
				DeceptionBehaviourGoal deceptionBehaviourGoal = (DeceptionBehaviourGoal)theEObject;
				T result = caseDeceptionBehaviourGoal(deceptionBehaviourGoal);
				if (result == null) result = caseDeceptionGoal(deceptionBehaviourGoal);
				if (result == null) result = caseGoal(deceptionBehaviourGoal);
				if (result == null) result = caseDeceptionNode(deceptionBehaviourGoal);
				if (result == null) result = caseObstructedElement(deceptionBehaviourGoal);
				if (result == null) result = caseVulnerable(deceptionBehaviourGoal);
				if (result == null) result = caseGoalRefinable(deceptionBehaviourGoal);
				if (result == null) result = caseGoalNode(deceptionBehaviourGoal);
				if (result == null) result = caseNode(deceptionBehaviourGoal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_RISK: {
				DeceptionRisk deceptionRisk = (DeceptionRisk)theEObject;
				T result = caseDeceptionRisk(deceptionRisk);
				if (result == null) result = caseRiskComponent(deceptionRisk);
				if (result == null) result = caseDeceptionNode(deceptionRisk);
				if (result == null) result = caseMitigableElement(deceptionRisk);
				if (result == null) result = caseSecurityNode(deceptionRisk);
				if (result == null) result = caseNode(deceptionRisk);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_STRATEGY: {
				DeceptionStrategy deceptionStrategy = (DeceptionStrategy)theEObject;
				T result = caseDeceptionStrategy(deceptionStrategy);
				if (result == null) result = caseAbstractFeature(deceptionStrategy);
				if (result == null) result = caseStrategyNode(deceptionStrategy);
				if (result == null) result = caseNode(deceptionStrategy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_TACTIC_MECHANISM: {
				DeceptionTacticMechanism deceptionTacticMechanism = (DeceptionTacticMechanism)theEObject;
				T result = caseDeceptionTacticMechanism(deceptionTacticMechanism);
				if (result == null) result = caseAbstractFeature(deceptionTacticMechanism);
				if (result == null) result = caseStrategyNode(deceptionTacticMechanism);
				if (result == null) result = caseNode(deceptionTacticMechanism);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.STRATEGY_NODE: {
				StrategyNode strategyNode = (StrategyNode)theEObject;
				T result = caseStrategyNode(strategyNode);
				if (result == null) result = caseNode(strategyNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.FEATURE: {
				Feature feature = (Feature)theEObject;
				T result = caseFeature(feature);
				if (result == null) result = caseAbstractFeature(feature);
				if (result == null) result = caseStrategyNode(feature);
				if (result == null) result = caseNode(feature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.STRATEGY_RELATIONSHIP: {
				StrategyRelationship strategyRelationship = (StrategyRelationship)theEObject;
				T result = caseStrategyRelationship(strategyRelationship);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.FEATURE_CONSTRAINT: {
				FeatureConstraint featureConstraint = (FeatureConstraint)theEObject;
				T result = caseFeatureConstraint(featureConstraint);
				if (result == null) result = caseStrategyRelationship(featureConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ABSTRACT_FEATURE: {
				AbstractFeature abstractFeature = (AbstractFeature)theEObject;
				T result = caseAbstractFeature(abstractFeature);
				if (result == null) result = caseStrategyNode(abstractFeature);
				if (result == null) result = caseNode(abstractFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.REQUIRED_CONSTRAINT: {
				RequiredConstraint requiredConstraint = (RequiredConstraint)theEObject;
				T result = caseRequiredConstraint(requiredConstraint);
				if (result == null) result = caseFeatureConstraint(requiredConstraint);
				if (result == null) result = caseStrategyRelationship(requiredConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.EXCLUDE_CONSTRAINT: {
				ExcludeConstraint excludeConstraint = (ExcludeConstraint)theEObject;
				T result = caseExcludeConstraint(excludeConstraint);
				if (result == null) result = caseFeatureConstraint(excludeConstraint);
				if (result == null) result = caseStrategyRelationship(excludeConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.BENEFIT_CONSTRAINT: {
				BenefitConstraint benefitConstraint = (BenefitConstraint)theEObject;
				T result = caseBenefitConstraint(benefitConstraint);
				if (result == null) result = caseFeatureConstraint(benefitConstraint);
				if (result == null) result = caseStrategyRelationship(benefitConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.AVOID_CONSTRAINT: {
				AvoidConstraint avoidConstraint = (AvoidConstraint)theEObject;
				T result = caseAvoidConstraint(avoidConstraint);
				if (result == null) result = caseFeatureConstraint(avoidConstraint);
				if (result == null) result = caseStrategyRelationship(avoidConstraint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.NODE_REFINEMENT: {
				NodeRefinement nodeRefinement = (NodeRefinement)theEObject;
				T result = caseNodeRefinement(nodeRefinement);
				if (result == null) result = caseStrategyRelationship(nodeRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.TACTIC_REFINEMENT: {
				TacticRefinement tacticRefinement = (TacticRefinement)theEObject;
				T result = caseTacticRefinement(tacticRefinement);
				if (result == null) result = caseNodeRefinement(tacticRefinement);
				if (result == null) result = caseStrategyRelationship(tacticRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.FEATURE_REFINEMENT: {
				FeatureRefinement featureRefinement = (FeatureRefinement)theEObject;
				T result = caseFeatureRefinement(featureRefinement);
				if (result == null) result = caseNodeRefinement(featureRefinement);
				if (result == null) result = caseStrategyRelationship(featureRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.STRATEGY_TACTIC_REFINEMENT: {
				StrategyTacticRefinement strategyTacticRefinement = (StrategyTacticRefinement)theEObject;
				T result = caseStrategyTacticRefinement(strategyTacticRefinement);
				if (result == null) result = caseNodeRefinement(strategyTacticRefinement);
				if (result == null) result = caseStrategyRelationship(strategyTacticRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.TACTIC_FEATURE_REFINEMENT: {
				TacticFeatureRefinement tacticFeatureRefinement = (TacticFeatureRefinement)theEObject;
				T result = caseTacticFeatureRefinement(tacticFeatureRefinement);
				if (result == null) result = caseNodeRefinement(tacticFeatureRefinement);
				if (result == null) result = caseStrategyRelationship(tacticFeatureRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_RELATIONSHIP: {
				DeceptionRelationship deceptionRelationship = (DeceptionRelationship)theEObject;
				T result = caseDeceptionRelationship(deceptionRelationship);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.MITIGATOR: {
				Mitigator mitigator = (Mitigator)theEObject;
				T result = caseMitigator(mitigator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.SECURITY_RELATIONSHIP: {
				SecurityRelationship securityRelationship = (SecurityRelationship)theEObject;
				T result = caseSecurityRelationship(securityRelationship);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.OBSTRUCTION_ELEMENT: {
				ObstructionElement obstructionElement = (ObstructionElement)theEObject;
				T result = caseObstructionElement(obstructionElement);
				if (result == null) result = caseGoalNode(obstructionElement);
				if (result == null) result = caseNode(obstructionElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.OR_OBSTRUCTION_REFINEMENT: {
				ORObstructionRefinement orObstructionRefinement = (ORObstructionRefinement)theEObject;
				T result = caseORObstructionRefinement(orObstructionRefinement);
				if (result == null) result = caseGoalRelationship(orObstructionRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.AND_OBSTRUCTION_REFINEMENT: {
				ANDObstructionRefinement andObstructionRefinement = (ANDObstructionRefinement)theEObject;
				T result = caseANDObstructionRefinement(andObstructionRefinement);
				if (result == null) result = caseGoalRelationship(andObstructionRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.OBSTACLE: {
				Obstacle obstacle = (Obstacle)theEObject;
				T result = caseObstacle(obstacle);
				if (result == null) result = caseObstructionElement(obstacle);
				if (result == null) result = caseGoalNode(obstacle);
				if (result == null) result = caseNode(obstacle);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.INFLUENCE_NODE: {
				InfluenceNode influenceNode = (InfluenceNode)theEObject;
				T result = caseInfluenceNode(influenceNode);
				if (result == null) result = caseNode(influenceNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.QUALITY_ATTRIBUTE: {
				QualityAttribute qualityAttribute = (QualityAttribute)theEObject;
				T result = caseQualityAttribute(qualityAttribute);
				if (result == null) result = caseInfluenceNode(qualityAttribute);
				if (result == null) result = caseNode(qualityAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.INFLUENCE_RELATIONSHIP: {
				InfluenceRelationship influenceRelationship = (InfluenceRelationship)theEObject;
				T result = caseInfluenceRelationship(influenceRelationship);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.INFLUENCE: {
				Influence influence = (Influence)theEObject;
				T result = caseInfluence(influence);
				if (result == null) result = caseInfluenceRelationship(influence);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.PREFERENCE: {
				Preference preference = (Preference)theEObject;
				T result = casePreference(preference);
				if (result == null) result = caseInfluenceRelationship(preference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.EVENT: {
				Event event = (Event)theEObject;
				T result = caseEvent(event);
				if (result == null) result = caseInfluenceNode(event);
				if (result == null) result = caseNode(event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_TACTIC_EVENT: {
				DeceptionTacticEvent deceptionTacticEvent = (DeceptionTacticEvent)theEObject;
				T result = caseDeceptionTacticEvent(deceptionTacticEvent);
				if (result == null) result = caseEvent(deceptionTacticEvent);
				if (result == null) result = caseInfluenceNode(deceptionTacticEvent);
				if (result == null) result = caseNode(deceptionTacticEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ENVIRONMENT_EVENT: {
				EnvironmentEvent environmentEvent = (EnvironmentEvent)theEObject;
				T result = caseEnvironmentEvent(environmentEvent);
				if (result == null) result = caseEvent(environmentEvent);
				if (result == null) result = caseInfluenceNode(environmentEvent);
				if (result == null) result = caseNode(environmentEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.OBSTRUCTED_ELEMENT: {
				ObstructedElement obstructedElement = (ObstructedElement)theEObject;
				T result = caseObstructedElement(obstructedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.VULNERABLE: {
				Vulnerable vulnerable = (Vulnerable)theEObject;
				T result = caseVulnerable(vulnerable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.GOAL_REFINABLE: {
				GoalRefinable goalRefinable = (GoalRefinable)theEObject;
				T result = caseGoalRefinable(goalRefinable);
				if (result == null) result = caseGoalNode(goalRefinable);
				if (result == null) result = caseNode(goalRefinable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.BELIEF: {
				Belief belief = (Belief)theEObject;
				T result = caseBelief(belief);
				if (result == null) result = caseGoalRefinable(belief);
				if (result == null) result = caseGoalNode(belief);
				if (result == null) result = caseNode(belief);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_GOAL: {
				DeceptionGoal deceptionGoal = (DeceptionGoal)theEObject;
				T result = caseDeceptionGoal(deceptionGoal);
				if (result == null) result = caseGoal(deceptionGoal);
				if (result == null) result = caseDeceptionNode(deceptionGoal);
				if (result == null) result = caseObstructedElement(deceptionGoal);
				if (result == null) result = caseVulnerable(deceptionGoal);
				if (result == null) result = caseGoalRefinable(deceptionGoal);
				if (result == null) result = caseGoalNode(deceptionGoal);
				if (result == null) result = caseNode(deceptionGoal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_SOFT_GOAL: {
				DeceptionSoftGoal deceptionSoftGoal = (DeceptionSoftGoal)theEObject;
				T result = caseDeceptionSoftGoal(deceptionSoftGoal);
				if (result == null) result = caseDeceptionGoal(deceptionSoftGoal);
				if (result == null) result = caseGoal(deceptionSoftGoal);
				if (result == null) result = caseDeceptionNode(deceptionSoftGoal);
				if (result == null) result = caseObstructedElement(deceptionSoftGoal);
				if (result == null) result = caseVulnerable(deceptionSoftGoal);
				if (result == null) result = caseGoalRefinable(deceptionSoftGoal);
				if (result == null) result = caseGoalNode(deceptionSoftGoal);
				if (result == null) result = caseNode(deceptionSoftGoal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.CONCRETE_ASSET: {
				ConcreteAsset concreteAsset = (ConcreteAsset)theEObject;
				T result = caseConcreteAsset(concreteAsset);
				if (result == null) result = caseAsset(concreteAsset);
				if (result == null) result = caseSecurityNode(concreteAsset);
				if (result == null) result = caseNode(concreteAsset);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.OPERATIONABLE: {
				Operationable operationable = (Operationable)theEObject;
				T result = caseOperationable(operationable);
				if (result == null) result = caseGoalNode(operationable);
				if (result == null) result = caseNode(operationable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.GOAL_CONTAINER: {
				GoalContainer goalContainer = (GoalContainer)theEObject;
				T result = caseGoalContainer(goalContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.NODE_SELECTION: {
				NodeSelection nodeSelection = (NodeSelection)theEObject;
				T result = caseNodeSelection(nodeSelection);
				if (result == null) result = caseStrategyRelationship(nodeSelection);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_TACTIC_STRATEGY: {
				DeceptionTacticStrategy deceptionTacticStrategy = (DeceptionTacticStrategy)theEObject;
				T result = caseDeceptionTacticStrategy(deceptionTacticStrategy);
				if (result == null) result = caseAbstractFeature(deceptionTacticStrategy);
				if (result == null) result = caseStrategyNode(deceptionTacticStrategy);
				if (result == null) result = caseNode(deceptionTacticStrategy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_STRATEGY_TACTIC_REFINEMENT: {
				DeceptionStrategyTacticRefinement deceptionStrategyTacticRefinement = (DeceptionStrategyTacticRefinement)theEObject;
				T result = caseDeceptionStrategyTacticRefinement(deceptionStrategyTacticRefinement);
				if (result == null) result = caseNodeRefinement(deceptionStrategyTacticRefinement);
				if (result == null) result = caseStrategyRelationship(deceptionStrategyTacticRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_TACTIC_STRATEGY_REFINEMENT: {
				DeceptionTacticStrategyRefinement deceptionTacticStrategyRefinement = (DeceptionTacticStrategyRefinement)theEObject;
				T result = caseDeceptionTacticStrategyRefinement(deceptionTacticStrategyRefinement);
				if (result == null) result = caseNodeRefinement(deceptionTacticStrategyRefinement);
				if (result == null) result = caseStrategyRelationship(deceptionTacticStrategyRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.DECEPTION_STRATEGY_REFINEMENT: {
				DeceptionStrategyRefinement deceptionStrategyRefinement = (DeceptionStrategyRefinement)theEObject;
				T result = caseDeceptionStrategyRefinement(deceptionStrategyRefinement);
				if (result == null) result = caseNodeRefinement(deceptionStrategyRefinement);
				if (result == null) result = caseStrategyRelationship(deceptionStrategyRefinement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT: {
				SimulationHasArtificialElement simulationHasArtificialElement = (SimulationHasArtificialElement)theEObject;
				T result = caseSimulationHasArtificialElement(simulationHasArtificialElement);
				if (result == null) result = caseDeceptionRelationship(simulationHasArtificialElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.PROPERTY: {
				Property property = (Property)theEObject;
				T result = caseProperty(property);
				if (result == null) result = caseDeceptionNode(property);
				if (result == null) result = caseNode(property);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DmtPackage.ANNOTATION: {
				Annotation annotation = (Annotation)theEObject;
				T result = caseAnnotation(annotation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>DMT Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>DMT Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDMTModel(DMTModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoal(Goal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal Relationship</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoalRelationship(GoalRelationship object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAgent(Agent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operationalization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operationalization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationalization(Operationalization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Software Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Software Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftwareAgent(SoftwareAgent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Environment Agent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Environment Agent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnvironmentAgent(EnvironmentAgent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Behaviour Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Behaviour Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBehaviourGoal(BehaviourGoal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Domain Description</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Domain Description</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDomainDescription(DomainDescription object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Leaf Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Leaf Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLeafGoal(LeafGoal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Expectation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Expectation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExpectation(Expectation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequirement(Requirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperation(Operation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OR Goal Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OR Goal Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseORGoalRefinement(ORGoalRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Security Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Security Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSecurityNode(SecurityNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mitigable Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mitigable Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMitigableElement(MitigableElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attack</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attack</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttack(Attack object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Threat</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Threat</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseThreat(Threat object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vulnerability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vulnerability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVulnerability(Vulnerability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Security Control</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Security Control</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSecurityControl(SecurityControl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Risk Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Risk Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRiskComponent(RiskComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attacker</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attacker</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttacker(Attacker object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Asset</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Asset</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAsset(Asset object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionNode(DeceptionNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Tactic</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Tactic</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionTactic(DeceptionTactic object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>IVulnerability</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>IVulnerability</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIVulnerability(IVulnerability object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Technique</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Technique</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionTechnique(DeceptionTechnique object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Story</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Story</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionStory(DeceptionStory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Bias</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Bias</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionBias(DeceptionBias object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Artificial Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Artificial Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArtificialElement(ArtificialElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interaction Nodes</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interaction Nodes</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInteractionNodes(InteractionNodes object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interaction Object</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interaction Object</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInteractionObject(InteractionObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLink(Link object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mitigation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mitigation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMitigation(Mitigation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simulation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimulation(Simulation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Dissimulation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Dissimulation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDissimulation(Dissimulation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNode(Node object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Agent Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Agent Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAgentAssignment(AgentAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AND Goal Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AND Goal Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseANDGoalRefinement(ANDGoalRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Conflict Goal Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Conflict Goal Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConflictGoalRefinement(ConflictGoalRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Agent Monitoring</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Agent Monitoring</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAgentMonitoring(AgentMonitoring object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Agent Control</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Agent Control</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAgentControl(AgentControl object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Agent Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Agent Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAgentRelation(AgentRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Metric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Metric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionMetric(DeceptionMetric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Channel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Channel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionChannel(DeceptionChannel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attack Vector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attack Vector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttackVector(AttackVector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AProperty</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AProperty</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAProperty(AProperty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attack Step Connector</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attack Step Connector</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttackStepConnector(AttackStepConnector object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Initial Attack Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Initial Attack Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInitialAttackStep(InitialAttackStep object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>End Attack Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>End Attack Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEndAttackStep(EndAttackStep object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attack Step</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attack Step</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttackStep(AttackStep object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attack Tree</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attack Tree</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttackTree(AttackTree object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoalNode(GoalNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Soft Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Soft Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftGoal(SoftGoal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Behaviour Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Behaviour Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionBehaviourGoal(DeceptionBehaviourGoal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Soft Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Soft Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionSoftGoal(DeceptionSoftGoal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Concrete Asset</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concrete Asset</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConcreteAsset(ConcreteAsset object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operationable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operationable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationable(Operationable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoalContainer(GoalContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Selection</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Selection</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodeSelection(NodeSelection object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Tactic Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Tactic Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionTacticStrategy(DeceptionTacticStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Strategy Tactic Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Strategy Tactic Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionStrategyTacticRefinement(DeceptionStrategyTacticRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Tactic Strategy Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Tactic Strategy Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionTacticStrategyRefinement(DeceptionTacticStrategyRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Strategy Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Strategy Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionStrategyRefinement(DeceptionStrategyRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simulation Has Artificial Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simulation Has Artificial Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimulationHasArtificialElement(SimulationHasArtificialElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Property</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProperty(Property object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Annotation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAnnotation(Annotation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionGoal(DeceptionGoal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Risk</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Risk</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionRisk(DeceptionRisk object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Strategy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionStrategy(DeceptionStrategy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Tactic Mechanism</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Tactic Mechanism</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionTacticMechanism(DeceptionTacticMechanism object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Strategy Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Strategy Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStrategyNode(StrategyNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeature(Feature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Strategy Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Strategy Relationship</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStrategyRelationship(StrategyRelationship object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureConstraint(FeatureConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractFeature(AbstractFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Required Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Required Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequiredConstraint(RequiredConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exclude Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exclude Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExcludeConstraint(ExcludeConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Benefit Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Benefit Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBenefitConstraint(BenefitConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Avoid Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Avoid Constraint</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAvoidConstraint(AvoidConstraint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Node Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Node Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNodeRefinement(NodeRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tactic Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tactic Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTacticRefinement(TacticRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeatureRefinement(FeatureRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Strategy Tactic Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Strategy Tactic Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStrategyTacticRefinement(StrategyTacticRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tactic Feature Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tactic Feature Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTacticFeatureRefinement(TacticFeatureRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Relationship</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionRelationship(DeceptionRelationship object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Mitigator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Mitigator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMitigator(Mitigator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Security Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Security Relationship</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSecurityRelationship(SecurityRelationship object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Obstruction Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Obstruction Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObstructionElement(ObstructionElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>OR Obstruction Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>OR Obstruction Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseORObstructionRefinement(ORObstructionRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AND Obstruction Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AND Obstruction Refinement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseANDObstructionRefinement(ANDObstructionRefinement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Obstacle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Obstacle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObstacle(Obstacle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Influence Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Influence Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInfluenceNode(InfluenceNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quality Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quality Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQualityAttribute(QualityAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Influence Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Influence Relationship</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInfluenceRelationship(InfluenceRelationship object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Influence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Influence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInfluence(Influence object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Preference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Preference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePreference(Preference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvent(Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deception Tactic Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deception Tactic Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeceptionTacticEvent(DeceptionTacticEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Environment Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Environment Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnvironmentEvent(EnvironmentEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Obstructed Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Obstructed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObstructedElement(ObstructedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vulnerable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vulnerable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVulnerable(Vulnerable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal Refinable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal Refinable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoalRefinable(GoalRefinable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Belief</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Belief</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBelief(Belief object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DmtSwitch
