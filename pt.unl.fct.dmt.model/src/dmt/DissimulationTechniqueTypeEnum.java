/**
 */
package dmt;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Dissimulation Technique Type Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage#getDissimulationTechniqueTypeEnum()
 * @model
 * @generated
 */
public enum DissimulationTechniqueTypeEnum implements Enumerator {
	/**
	 * The '<em><b>MASKING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MASKING_VALUE
	 * @generated
	 * @ordered
	 */
	MASKING(0, "MASKING", "MASKING"),

	/**
	 * The '<em><b>REPACKAGING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REPACKAGING_VALUE
	 * @generated
	 * @ordered
	 */
	REPACKAGING(1, "REPACKAGING", "REPACKAGING"),

	/**
	 * The '<em><b>DAZZLING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DAZZLING_VALUE
	 * @generated
	 * @ordered
	 */
	DAZZLING(2, "DAZZLING", "DAZZLING");

	/**
	 * The '<em><b>MASKING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MASKING</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MASKING
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MASKING_VALUE = 0;

	/**
	 * The '<em><b>REPACKAGING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>REPACKAGING</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REPACKAGING
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REPACKAGING_VALUE = 1;

	/**
	 * The '<em><b>DAZZLING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DAZZLING</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DAZZLING
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DAZZLING_VALUE = 2;

	/**
	 * An array of all the '<em><b>Dissimulation Technique Type Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DissimulationTechniqueTypeEnum[] VALUES_ARRAY =
		new DissimulationTechniqueTypeEnum[] {
			MASKING,
			REPACKAGING,
			DAZZLING,
		};

	/**
	 * A public read-only list of all the '<em><b>Dissimulation Technique Type Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DissimulationTechniqueTypeEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Dissimulation Technique Type Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DissimulationTechniqueTypeEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DissimulationTechniqueTypeEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Dissimulation Technique Type Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DissimulationTechniqueTypeEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DissimulationTechniqueTypeEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Dissimulation Technique Type Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DissimulationTechniqueTypeEnum get(int value) {
		switch (value) {
			case MASKING_VALUE: return MASKING;
			case REPACKAGING_VALUE: return REPACKAGING;
			case DAZZLING_VALUE: return DAZZLING;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DissimulationTechniqueTypeEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DissimulationTechniqueTypeEnum
