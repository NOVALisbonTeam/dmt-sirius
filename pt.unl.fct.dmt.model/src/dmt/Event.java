/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getEvent()
 * @model
 * @generated
 */
public interface Event extends InfluenceNode {
} // Event
