/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Strategy Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.StrategyNode#getProperty <em>Property</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getStrategyNode()
 * @model abstract="true"
 * @generated
 */
public interface StrategyNode extends Node {

	/**
	 * Returns the value of the '<em><b>Property</b></em>' reference list.
	 * The list contents are of type {@link dmt.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' reference list.
	 * @see dmt.DmtPackage#getStrategyNode_Property()
	 * @model
	 * @generated
	 */
	EList<Property> getProperty();
} // StrategyNode
