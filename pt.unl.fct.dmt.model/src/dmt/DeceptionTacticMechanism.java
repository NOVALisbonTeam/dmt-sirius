/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Tactic Mechanism</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionTacticMechanism#getEnablingCondition <em>Enabling Condition</em>}</li>
 *   <li>{@link dmt.DeceptionTacticMechanism#getTacticReference <em>Tactic Reference</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionTacticMechanism()
 * @model
 * @generated
 */
public interface DeceptionTacticMechanism extends AbstractFeature {

	/**
	 * Returns the value of the '<em><b>Enabling Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enabling Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enabling Condition</em>' attribute.
	 * @see #setEnablingCondition(String)
	 * @see dmt.DmtPackage#getDeceptionTacticMechanism_EnablingCondition()
	 * @model
	 * @generated
	 */
	String getEnablingCondition();

	/**
	 * Sets the value of the '{@link dmt.DeceptionTacticMechanism#getEnablingCondition <em>Enabling Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enabling Condition</em>' attribute.
	 * @see #getEnablingCondition()
	 * @generated
	 */
	void setEnablingCondition(String value);

	/**
	 * Returns the value of the '<em><b>Tactic Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tactic Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tactic Reference</em>' reference.
	 * @see #setTacticReference(DeceptionTactic)
	 * @see dmt.DmtPackage#getDeceptionTacticMechanism_TacticReference()
	 * @model
	 * @generated
	 */
	DeceptionTactic getTacticReference();

	/**
	 * Sets the value of the '{@link dmt.DeceptionTacticMechanism#getTacticReference <em>Tactic Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tactic Reference</em>' reference.
	 * @see #getTacticReference()
	 * @generated
	 */
	void setTacticReference(DeceptionTactic value);
} // DeceptionTacticMechanism
