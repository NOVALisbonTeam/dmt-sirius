/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Artificial Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ArtificialElement#getContainVulnerability <em>Contain Vulnerability</em>}</li>
 *   <li>{@link dmt.ArtificialElement#getFakeType <em>Fake Type</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getArtificialElement()
 * @model
 * @generated
 */
public interface ArtificialElement extends DeceptionNode, Asset {
	/**
	 * Returns the value of the '<em><b>Contain Vulnerability</b></em>' reference list.
	 * The list contents are of type {@link dmt.IVulnerability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contain Vulnerability</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contain Vulnerability</em>' reference list.
	 * @see dmt.DmtPackage#getArtificialElement_ContainVulnerability()
	 * @model
	 * @generated
	 */
	EList<IVulnerability> getContainVulnerability();

	/**
	 * Returns the value of the '<em><b>Fake Type</b></em>' attribute.
	 * The default value is <code>"FAKEDATA"</code>.
	 * The literals are from the enumeration {@link dmt.ArtificialElementTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fake Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fake Type</em>' attribute.
	 * @see dmt.ArtificialElementTypeEnum
	 * @see #setFakeType(ArtificialElementTypeEnum)
	 * @see dmt.DmtPackage#getArtificialElement_FakeType()
	 * @model default="FAKEDATA"
	 * @generated
	 */
	ArtificialElementTypeEnum getFakeType();

	/**
	 * Sets the value of the '{@link dmt.ArtificialElement#getFakeType <em>Fake Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fake Type</em>' attribute.
	 * @see dmt.ArtificialElementTypeEnum
	 * @see #getFakeType()
	 * @generated
	 */
	void setFakeType(ArtificialElementTypeEnum value);

} // ArtificialElement
