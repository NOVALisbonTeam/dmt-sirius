/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Threat</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * A threat is a possible danger that might exploit a vulnerability to breach security and therefore cause possible harm.
 * ISO 27005 defines threat as:[2] A potential cause of an incident, that may result in harm of systems and organization
 * 
 * 
 * Threats can be classified according to their type and origin:[2]
 * 
 * Types of threats:
 * 
 * Physical damage: fire, water, pollution
 * Natural events: climatic, seismic, volcanic
 * Loss of essential services: electrical power, air conditioning, telecommunication
 * Compromise of information: eavesdropping, theft of media, retrieval of discarded materials
 * Technical failures: equipment, software, capacity saturation,
 * Compromise of functions: error in use, abuse of rights, denial of actions
 * 
 * Note that a threat type can have multiple origins.
 * 
 * Deliberate: aiming at information asset 
 * spying 
 * illegal processing of data
 * 
 * Accidental:  
 * equipment failure
 * software failure
 * 
 * Environmental
 * natural event
 * loss of power supply
 * 
 * Negligence: Known but neglected factors, compromising the network safety and sustainability
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Threat#getCategory <em>Category</em>}</li>
 *   <li>{@link dmt.Threat#getScenario <em>Scenario</em>}</li>
 *   <li>{@link dmt.Threat#getContributesToThreat <em>Contributes To Threat</em>}</li>
 *   <li>{@link dmt.Threat#getConsequence <em>Consequence</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getThreat()
 * @model
 * @generated
 */
public interface Threat extends MitigableElement, ObstructionElement {
	/**
	 * Returns the value of the '<em><b>Category</b></em>' attribute.
	 * The default value is <code>"INTENTIONAL"</code>.
	 * The literals are from the enumeration {@link dmt.ThreatCategoryEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Category</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Category</em>' attribute.
	 * @see dmt.ThreatCategoryEnum
	 * @see #setCategory(ThreatCategoryEnum)
	 * @see dmt.DmtPackage#getThreat_Category()
	 * @model default="INTENTIONAL"
	 * @generated
	 */
	ThreatCategoryEnum getCategory();

	/**
	 * Sets the value of the '{@link dmt.Threat#getCategory <em>Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Category</em>' attribute.
	 * @see dmt.ThreatCategoryEnum
	 * @see #getCategory()
	 * @generated
	 */
	void setCategory(ThreatCategoryEnum value);

	/**
	 * Returns the value of the '<em><b>Scenario</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scenario</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scenario</em>' attribute.
	 * @see #setScenario(String)
	 * @see dmt.DmtPackage#getThreat_Scenario()
	 * @model
	 * @generated
	 */
	String getScenario();

	/**
	 * Sets the value of the '{@link dmt.Threat#getScenario <em>Scenario</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scenario</em>' attribute.
	 * @see #getScenario()
	 * @generated
	 */
	void setScenario(String value);

	/**
	 * Returns the value of the '<em><b>Contributes To Threat</b></em>' reference list.
	 * The list contents are of type {@link dmt.Threat}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contributes To Threat</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contributes To Threat</em>' reference list.
	 * @see dmt.DmtPackage#getThreat_ContributesToThreat()
	 * @model
	 * @generated
	 */
	EList<Threat> getContributesToThreat();

	/**
	 * Returns the value of the '<em><b>Consequence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Consequence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consequence</em>' attribute.
	 * @see #setConsequence(String)
	 * @see dmt.DmtPackage#getThreat_Consequence()
	 * @model
	 * @generated
	 */
	String getConsequence();

	/**
	 * Sets the value of the '{@link dmt.Threat#getConsequence <em>Consequence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Consequence</em>' attribute.
	 * @see #getConsequence()
	 * @generated
	 */
	void setConsequence(String value);

} // Threat
