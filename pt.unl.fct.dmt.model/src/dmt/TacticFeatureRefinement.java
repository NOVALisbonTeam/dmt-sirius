/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tactic Feature Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.TacticFeatureRefinement#getTacticfeaturerefinementtarget <em>Tacticfeaturerefinementtarget</em>}</li>
 *   <li>{@link dmt.TacticFeatureRefinement#getTacticfeaturerefinementsource <em>Tacticfeaturerefinementsource</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getTacticFeatureRefinement()
 * @model
 * @generated
 */
public interface TacticFeatureRefinement extends NodeRefinement {
	/**
	 * Returns the value of the '<em><b>Tacticfeaturerefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tacticfeaturerefinementtarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tacticfeaturerefinementtarget</em>' reference.
	 * @see #setTacticfeaturerefinementtarget(Feature)
	 * @see dmt.DmtPackage#getTacticFeatureRefinement_Tacticfeaturerefinementtarget()
	 * @model required="true"
	 * @generated
	 */
	Feature getTacticfeaturerefinementtarget();

	/**
	 * Sets the value of the '{@link dmt.TacticFeatureRefinement#getTacticfeaturerefinementtarget <em>Tacticfeaturerefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tacticfeaturerefinementtarget</em>' reference.
	 * @see #getTacticfeaturerefinementtarget()
	 * @generated
	 */
	void setTacticfeaturerefinementtarget(Feature value);

	/**
	 * Returns the value of the '<em><b>Tacticfeaturerefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tacticfeaturerefinementsource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tacticfeaturerefinementsource</em>' reference.
	 * @see #setTacticfeaturerefinementsource(DeceptionTacticMechanism)
	 * @see dmt.DmtPackage#getTacticFeatureRefinement_Tacticfeaturerefinementsource()
	 * @model required="true"
	 * @generated
	 */
	DeceptionTacticMechanism getTacticfeaturerefinementsource();

	/**
	 * Sets the value of the '{@link dmt.TacticFeatureRefinement#getTacticfeaturerefinementsource <em>Tacticfeaturerefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tacticfeaturerefinementsource</em>' reference.
	 * @see #getTacticfeaturerefinementsource()
	 * @generated
	 */
	void setTacticfeaturerefinementsource(DeceptionTacticMechanism value);

} // TacticFeatureRefinement
