/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mitigates</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Mitigates#getMitigationLevel <em>Mitigation Level</em>}</li>
 *   <li>{@link dmt.Mitigates#getMitigablenode <em>Mitigablenode</em>}</li>
 *   <li>{@link dmt.Mitigates#getSecuritycontrol <em>Securitycontrol</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getMitigates()
 * @model
 * @generated
 */
public interface Mitigates extends SecurityNode {
	/**
	 * Returns the value of the '<em><b>Mitigation Level</b></em>' attribute.
	 * The default value is <code>"M"</code>.
	 * The literals are from the enumeration {@link dmt.MitigationLevelEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mitigation Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mitigation Level</em>' attribute.
	 * @see dmt.MitigationLevelEnum
	 * @see #setMitigationLevel(MitigationLevelEnum)
	 * @see dmt.DmtPackage#getMitigates_MitigationLevel()
	 * @model default="M"
	 * @generated
	 */
	MitigationLevelEnum getMitigationLevel();

	/**
	 * Sets the value of the '{@link dmt.Mitigates#getMitigationLevel <em>Mitigation Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mitigation Level</em>' attribute.
	 * @see dmt.MitigationLevelEnum
	 * @see #getMitigationLevel()
	 * @generated
	 */
	void setMitigationLevel(MitigationLevelEnum value);

	/**
	 * Returns the value of the '<em><b>Mitigablenode</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mitigablenode</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mitigablenode</em>' reference.
	 * @see #setMitigablenode(MitigableElement)
	 * @see dmt.DmtPackage#getMitigates_Mitigablenode()
	 * @model
	 * @generated
	 */
	MitigableElement getMitigablenode();

	/**
	 * Sets the value of the '{@link dmt.Mitigates#getMitigablenode <em>Mitigablenode</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mitigablenode</em>' reference.
	 * @see #getMitigablenode()
	 * @generated
	 */
	void setMitigablenode(MitigableElement value);

	/**
	 * Returns the value of the '<em><b>Securitycontrol</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Securitycontrol</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Securitycontrol</em>' reference.
	 * @see #setSecuritycontrol(SecurityControl)
	 * @see dmt.DmtPackage#getMitigates_Securitycontrol()
	 * @model
	 * @generated
	 */
	SecurityControl getSecuritycontrol();

	/**
	 * Sets the value of the '{@link dmt.Mitigates#getSecuritycontrol <em>Securitycontrol</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Securitycontrol</em>' reference.
	 * @see #getSecuritycontrol()
	 * @generated
	 */
	void setSecuritycontrol(SecurityControl value);

} // Mitigates
