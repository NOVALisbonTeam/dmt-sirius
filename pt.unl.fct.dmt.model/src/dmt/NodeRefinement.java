/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.NodeRefinement#getEnactmentChoice <em>Enactment Choice</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getNodeRefinement()
 * @model abstract="true"
 * @generated
 */
public interface NodeRefinement extends StrategyRelationship {
	/**
	 * Returns the value of the '<em><b>Enactment Choice</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.EnactmentEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enactment Choice</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enactment Choice</em>' attribute.
	 * @see dmt.EnactmentEnum
	 * @see #setEnactmentChoice(EnactmentEnum)
	 * @see dmt.DmtPackage#getNodeRefinement_EnactmentChoice()
	 * @model
	 * @generated
	 */
	EnactmentEnum getEnactmentChoice();

	/**
	 * Sets the value of the '{@link dmt.NodeRefinement#getEnactmentChoice <em>Enactment Choice</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Enactment Choice</em>' attribute.
	 * @see dmt.EnactmentEnum
	 * @see #getEnactmentChoice()
	 * @generated
	 */
	void setEnactmentChoice(EnactmentEnum value);

} // NodeRefinement
