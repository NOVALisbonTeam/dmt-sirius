/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.AbstractFeature#getBindingState <em>Binding State</em>}</li>
 *   <li>{@link dmt.AbstractFeature#isIsAbstract <em>Is Abstract</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAbstractFeature()
 * @model abstract="true"
 * @generated
 */
public interface AbstractFeature extends StrategyNode {
	/**
	 * Returns the value of the '<em><b>Binding State</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.BindingStateEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binding State</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binding State</em>' attribute.
	 * @see dmt.BindingStateEnum
	 * @see #setBindingState(BindingStateEnum)
	 * @see dmt.DmtPackage#getAbstractFeature_BindingState()
	 * @model
	 * @generated
	 */
	BindingStateEnum getBindingState();

	/**
	 * Sets the value of the '{@link dmt.AbstractFeature#getBindingState <em>Binding State</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binding State</em>' attribute.
	 * @see dmt.BindingStateEnum
	 * @see #getBindingState()
	 * @generated
	 */
	void setBindingState(BindingStateEnum value);

	/**
	 * Returns the value of the '<em><b>Is Abstract</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Abstract</em>' attribute.
	 * @see #setIsAbstract(boolean)
	 * @see dmt.DmtPackage#getAbstractFeature_IsAbstract()
	 * @model default="false"
	 * @generated
	 */
	boolean isIsAbstract();

	/**
	 * Sets the value of the '{@link dmt.AbstractFeature#isIsAbstract <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Abstract</em>' attribute.
	 * @see #isIsAbstract()
	 * @generated
	 */
	void setIsAbstract(boolean value);

} // AbstractFeature
