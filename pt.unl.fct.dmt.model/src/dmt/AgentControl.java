/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent Control</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.AgentControl#getObjectAttribute <em>Object Attribute</em>}</li>
 *   <li>{@link dmt.AgentControl#getAgentControl <em>Agent Control</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAgentControl()
 * @model
 * @generated
 */
public interface AgentControl extends AgentRelation {
	/**
	 * Returns the value of the '<em><b>Object Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Attribute</em>' attribute.
	 * @see #setObjectAttribute(String)
	 * @see dmt.DmtPackage#getAgentControl_ObjectAttribute()
	 * @model
	 * @generated
	 */
	String getObjectAttribute();

	/**
	 * Sets the value of the '{@link dmt.AgentControl#getObjectAttribute <em>Object Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Attribute</em>' attribute.
	 * @see #getObjectAttribute()
	 * @generated
	 */
	void setObjectAttribute(String value);

	/**
	 * Returns the value of the '<em><b>Agent Control</b></em>' reference list.
	 * The list contents are of type {@link dmt.InteractionObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Agent Control</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Control</em>' reference list.
	 * @see dmt.DmtPackage#getAgentControl_AgentControl()
	 * @model required="true"
	 * @generated
	 */
	EList<InteractionObject> getAgentControl();

} // AgentControl
