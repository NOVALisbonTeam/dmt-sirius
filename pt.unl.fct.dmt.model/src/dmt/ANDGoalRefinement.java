/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AND Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ANDGoalRefinement#getAndGoalRefine <em>And Goal Refine</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getANDGoalRefinement()
 * @model
 * @generated
 */
public interface ANDGoalRefinement extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>And Goal Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Goal Refine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Goal Refine</em>' reference.
	 * @see #setAndGoalRefine(GoalRefinable)
	 * @see dmt.DmtPackage#getANDGoalRefinement_AndGoalRefine()
	 * @model required="true"
	 * @generated
	 */
	GoalRefinable getAndGoalRefine();

	/**
	 * Sets the value of the '{@link dmt.ANDGoalRefinement#getAndGoalRefine <em>And Goal Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>And Goal Refine</em>' reference.
	 * @see #getAndGoalRefine()
	 * @generated
	 */
	void setAndGoalRefine(GoalRefinable value);

} // ANDGoalRefinement
