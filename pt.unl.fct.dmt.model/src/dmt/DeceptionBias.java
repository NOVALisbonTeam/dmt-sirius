/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Bias</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionBias#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionBias()
 * @model
 * @generated
 */
public interface DeceptionBias extends DeceptionNode {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.BiasTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dmt.BiasTypeEnum
	 * @see #setType(BiasTypeEnum)
	 * @see dmt.DmtPackage#getDeceptionBias_Type()
	 * @model
	 * @generated
	 */
	BiasTypeEnum getType();

	/**
	 * Sets the value of the '{@link dmt.DeceptionBias#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dmt.BiasTypeEnum
	 * @see #getType()
	 * @generated
	 */
	void setType(BiasTypeEnum value);

} // DeceptionBias
