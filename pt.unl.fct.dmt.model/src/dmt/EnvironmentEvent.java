/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getEnvironmentEvent()
 * @model
 * @generated
 */
public interface EnvironmentEvent extends Event {
} // EnvironmentEvent
