/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getDeceptionNode()
 * @model abstract="true"
 * @generated
 */
public interface DeceptionNode extends Node {
} // DeceptionNode
