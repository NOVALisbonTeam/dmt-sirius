/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent Monitoring</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.AgentMonitoring#getAgentMonitor <em>Agent Monitor</em>}</li>
 *   <li>{@link dmt.AgentMonitoring#getObjectAttribute <em>Object Attribute</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAgentMonitoring()
 * @model
 * @generated
 */
public interface AgentMonitoring extends AgentRelation {
	/**
	 * Returns the value of the '<em><b>Agent Monitor</b></em>' reference list.
	 * The list contents are of type {@link dmt.InteractionObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Agent Monitor</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Monitor</em>' reference list.
	 * @see dmt.DmtPackage#getAgentMonitoring_AgentMonitor()
	 * @model required="true"
	 * @generated
	 */
	EList<InteractionObject> getAgentMonitor();

	/**
	 * Returns the value of the '<em><b>Object Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Attribute</em>' attribute.
	 * @see #setObjectAttribute(String)
	 * @see dmt.DmtPackage#getAgentMonitoring_ObjectAttribute()
	 * @model
	 * @generated
	 */
	String getObjectAttribute();

	/**
	 * Sets the value of the '{@link dmt.AgentMonitoring#getObjectAttribute <em>Object Attribute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Attribute</em>' attribute.
	 * @see #getObjectAttribute()
	 * @generated
	 */
	void setObjectAttribute(String value);

} // AgentMonitoring
