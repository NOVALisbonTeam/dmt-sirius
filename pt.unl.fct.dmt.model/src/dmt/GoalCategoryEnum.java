/**
 */
package dmt;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Goal Category Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage#getGoalCategoryEnum()
 * @model
 * @generated
 */
public enum GoalCategoryEnum implements Enumerator {
	/**
	 * The '<em><b>UNDEFINED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNDEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	UNDEFINED(7, "UNDEFINED", "UNDEFINED"),

	/**
	 * The '<em><b>ACCURACY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACCURACY_VALUE
	 * @generated
	 * @ordered
	 */
	ACCURACY(1, "ACCURACY", "ACCURACY"),

	/**
	 * The '<em><b>ADAPTABILITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADAPTABILITY_VALUE
	 * @generated
	 * @ordered
	 */
	ADAPTABILITY(2, "ADAPTABILITY", "ADAPTABILITY"),

	/**
	 * The '<em><b>CAPACITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CAPACITY_VALUE
	 * @generated
	 * @ordered
	 */
	CAPACITY(3, "CAPACITY", "CAPACITY"),

	/**
	 * The '<em><b>CONSISTENCY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSISTENCY_VALUE
	 * @generated
	 * @ordered
	 */
	CONSISTENCY(4, "CONSISTENCY", "CONSISTENCY"),

	/**
	 * The '<em><b>EFFICIENCY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EFFICIENCY_VALUE
	 * @generated
	 * @ordered
	 */
	EFFICIENCY(5, "EFFICIENCY", "EFFICIENCY"),

	/**
	 * The '<em><b>INFORMATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INFORMATION_VALUE
	 * @generated
	 * @ordered
	 */
	INFORMATION(6, "INFORMATION", "INFORMATION"),

	/**
	 * The '<em><b>PERFORMANCE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PERFORMANCE_VALUE
	 * @generated
	 * @ordered
	 */
	PERFORMANCE(7, "PERFORMANCE", "PERFORMANCE"),

	/**
	 * The '<em><b>PRIVACY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PRIVACY_VALUE
	 * @generated
	 * @ordered
	 */
	PRIVACY(8, "PRIVACY", "PRIVACY"),

	/**
	 * The '<em><b>ROBUSTNESS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ROBUSTNESS_VALUE
	 * @generated
	 * @ordered
	 */
	ROBUSTNESS(9, "ROBUSTNESS", "ROBUSTNESS"),

	/**
	 * The '<em><b>SAFETY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAFETY_VALUE
	 * @generated
	 * @ordered
	 */
	SAFETY(10, "SAFETY", "SAFETY"),

	/**
	 * The '<em><b>SATISFACTION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SATISFACTION_VALUE
	 * @generated
	 * @ordered
	 */
	SATISFACTION(11, "SATISFACTION", "SATISFACTION"),

	/**
	 * The '<em><b>SECURITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SECURITY_VALUE
	 * @generated
	 * @ordered
	 */
	SECURITY(12, "SECURITY", "SECURITY"),

	/**
	 * The '<em><b>USABILITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #USABILITY_VALUE
	 * @generated
	 * @ordered
	 */
	USABILITY(13, "USABILITY", "USABILITY");

	/**
	 * The '<em><b>UNDEFINED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNDEFINED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNDEFINED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNDEFINED_VALUE = 7;

	/**
	 * The '<em><b>ACCURACY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ACCURACY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACCURACY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ACCURACY_VALUE = 1;

	/**
	 * The '<em><b>ADAPTABILITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ADAPTABILITY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADAPTABILITY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ADAPTABILITY_VALUE = 2;

	/**
	 * The '<em><b>CAPACITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CAPACITY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CAPACITY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CAPACITY_VALUE = 3;

	/**
	 * The '<em><b>CONSISTENCY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CONSISTENCY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSISTENCY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CONSISTENCY_VALUE = 4;

	/**
	 * The '<em><b>EFFICIENCY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EFFICIENCY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EFFICIENCY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EFFICIENCY_VALUE = 5;

	/**
	 * The '<em><b>INFORMATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INFORMATION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INFORMATION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INFORMATION_VALUE = 6;

	/**
	 * The '<em><b>PERFORMANCE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PERFORMANCE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PERFORMANCE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PERFORMANCE_VALUE = 7;

	/**
	 * The '<em><b>PRIVACY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PRIVACY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PRIVACY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PRIVACY_VALUE = 8;

	/**
	 * The '<em><b>ROBUSTNESS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ROBUSTNESS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ROBUSTNESS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ROBUSTNESS_VALUE = 9;

	/**
	 * The '<em><b>SAFETY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SAFETY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAFETY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAFETY_VALUE = 10;

	/**
	 * The '<em><b>SATISFACTION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SATISFACTION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SATISFACTION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SATISFACTION_VALUE = 11;

	/**
	 * The '<em><b>SECURITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SECURITY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SECURITY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SECURITY_VALUE = 12;

	/**
	 * The '<em><b>USABILITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>USABILITY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #USABILITY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int USABILITY_VALUE = 13;

	/**
	 * An array of all the '<em><b>Goal Category Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final GoalCategoryEnum[] VALUES_ARRAY =
		new GoalCategoryEnum[] {
			UNDEFINED,
			ACCURACY,
			ADAPTABILITY,
			CAPACITY,
			CONSISTENCY,
			EFFICIENCY,
			INFORMATION,
			PERFORMANCE,
			PRIVACY,
			ROBUSTNESS,
			SAFETY,
			SATISFACTION,
			SECURITY,
			USABILITY,
		};

	/**
	 * A public read-only list of all the '<em><b>Goal Category Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<GoalCategoryEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Goal Category Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GoalCategoryEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			GoalCategoryEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Goal Category Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GoalCategoryEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			GoalCategoryEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Goal Category Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GoalCategoryEnum get(int value) {
		switch (value) {
			case UNDEFINED_VALUE: return UNDEFINED;
			case ACCURACY_VALUE: return ACCURACY;
			case ADAPTABILITY_VALUE: return ADAPTABILITY;
			case CAPACITY_VALUE: return CAPACITY;
			case CONSISTENCY_VALUE: return CONSISTENCY;
			case EFFICIENCY_VALUE: return EFFICIENCY;
			case INFORMATION_VALUE: return INFORMATION;
			case PRIVACY_VALUE: return PRIVACY;
			case ROBUSTNESS_VALUE: return ROBUSTNESS;
			case SAFETY_VALUE: return SAFETY;
			case SATISFACTION_VALUE: return SATISFACTION;
			case SECURITY_VALUE: return SECURITY;
			case USABILITY_VALUE: return USABILITY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private GoalCategoryEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //GoalCategoryEnum
