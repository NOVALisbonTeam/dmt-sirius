/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Misc Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getMiscNode()
 * @model abstract="true"
 * @generated
 */
public interface MiscNode extends Node {
} // MiscNode
