/**
 */
package dmt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal Model Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.GoalModelRelation#getStatus <em>Status</em>}</li>
 *   <li>{@link dmt.GoalModelRelation#getTactic <em>Tactic</em>}</li>
 *   <li>{@link dmt.GoalModelRelation#getSysRef <em>Sys Ref</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getGoalModelRelation()
 * @model abstract="true"
 * @generated
 */
public interface GoalModelRelation extends EObject {
	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see #setStatus(String)
	 * @see dmt.DmtPackage#getGoalModelRelation_Status()
	 * @model
	 * @generated
	 */
	String getStatus();

	/**
	 * Sets the value of the '{@link dmt.GoalModelRelation#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(String value);

	/**
	 * Returns the value of the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tactic</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tactic</em>' attribute.
	 * @see #setTactic(String)
	 * @see dmt.DmtPackage#getGoalModelRelation_Tactic()
	 * @model
	 * @generated
	 */
	String getTactic();

	/**
	 * Sets the value of the '{@link dmt.GoalModelRelation#getTactic <em>Tactic</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tactic</em>' attribute.
	 * @see #getTactic()
	 * @generated
	 */
	void setTactic(String value);

	/**
	 * Returns the value of the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sys Ref</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sys Ref</em>' attribute.
	 * @see #setSysRef(String)
	 * @see dmt.DmtPackage#getGoalModelRelation_SysRef()
	 * @model
	 * @generated
	 */
	String getSysRef();

	/**
	 * Sets the value of the '{@link dmt.GoalModelRelation#getSysRef <em>Sys Ref</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sys Ref</em>' attribute.
	 * @see #getSysRef()
	 * @generated
	 */
	void setSysRef(String value);

} // GoalModelRelation
