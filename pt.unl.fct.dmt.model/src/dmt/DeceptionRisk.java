/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Risk</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionRisk#getDeceptionRiskType <em>Deception Risk Type</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionRisk()
 * @model
 * @generated
 */
public interface DeceptionRisk extends RiskComponent, DeceptionNode {
	/**
	 * Returns the value of the '<em><b>Deception Risk Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.DeceptionRiskTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deception Risk Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deception Risk Type</em>' attribute.
	 * @see dmt.DeceptionRiskTypeEnum
	 * @see #setDeceptionRiskType(DeceptionRiskTypeEnum)
	 * @see dmt.DmtPackage#getDeceptionRisk_DeceptionRiskType()
	 * @model
	 * @generated
	 */
	DeceptionRiskTypeEnum getDeceptionRiskType();

	/**
	 * Sets the value of the '{@link dmt.DeceptionRisk#getDeceptionRiskType <em>Deception Risk Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deception Risk Type</em>' attribute.
	 * @see dmt.DeceptionRiskTypeEnum
	 * @see #getDeceptionRiskType()
	 * @generated
	 */
	void setDeceptionRiskType(DeceptionRiskTypeEnum value);

} // DeceptionRisk
