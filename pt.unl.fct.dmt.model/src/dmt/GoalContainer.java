/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.GoalContainer#getGoal <em>Goal</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getGoalContainer()
 * @model
 * @generated
 */
public interface GoalContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Goal</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.Goal}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goal</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goal</em>' containment reference list.
	 * @see dmt.DmtPackage#getGoalContainer_Goal()
	 * @model containment="true"
	 * @generated
	 */
	EList<Goal> getGoal();

} // GoalContainer
