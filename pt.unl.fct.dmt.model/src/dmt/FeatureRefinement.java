/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.FeatureRefinement#getFeaturerefinementtarget <em>Featurerefinementtarget</em>}</li>
 *   <li>{@link dmt.FeatureRefinement#getFeaturerefinementsource <em>Featurerefinementsource</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getFeatureRefinement()
 * @model
 * @generated
 */
public interface FeatureRefinement extends NodeRefinement {
	/**
	 * Returns the value of the '<em><b>Featurerefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Featurerefinementtarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Featurerefinementtarget</em>' reference.
	 * @see #setFeaturerefinementtarget(Feature)
	 * @see dmt.DmtPackage#getFeatureRefinement_Featurerefinementtarget()
	 * @model required="true"
	 * @generated
	 */
	Feature getFeaturerefinementtarget();

	/**
	 * Sets the value of the '{@link dmt.FeatureRefinement#getFeaturerefinementtarget <em>Featurerefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Featurerefinementtarget</em>' reference.
	 * @see #getFeaturerefinementtarget()
	 * @generated
	 */
	void setFeaturerefinementtarget(Feature value);

	/**
	 * Returns the value of the '<em><b>Featurerefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Featurerefinementsource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Featurerefinementsource</em>' reference.
	 * @see #setFeaturerefinementsource(Feature)
	 * @see dmt.DmtPackage#getFeatureRefinement_Featurerefinementsource()
	 * @model required="true"
	 * @generated
	 */
	Feature getFeaturerefinementsource();

	/**
	 * Sets the value of the '{@link dmt.FeatureRefinement#getFeaturerefinementsource <em>Featurerefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Featurerefinementsource</em>' reference.
	 * @see #getFeaturerefinementsource()
	 * @generated
	 */
	void setFeaturerefinementsource(Feature value);

} // FeatureRefinement
