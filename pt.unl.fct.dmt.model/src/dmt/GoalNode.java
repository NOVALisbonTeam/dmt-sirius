/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getGoalNode()
 * @model abstract="true"
 * @generated
 */
public interface GoalNode extends Node {
} // GoalNode
