/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Selection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.NodeSelection#getSelectionsource <em>Selectionsource</em>}</li>
 *   <li>{@link dmt.NodeSelection#getSelectiontarget <em>Selectiontarget</em>}</li>
 *   <li>{@link dmt.NodeSelection#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getNodeSelection()
 * @model
 * @generated
 */
public interface NodeSelection extends StrategyRelationship {
	/**
	 * Returns the value of the '<em><b>Selectionsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selectionsource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selectionsource</em>' reference.
	 * @see #setSelectionsource(NodeRefinement)
	 * @see dmt.DmtPackage#getNodeSelection_Selectionsource()
	 * @model required="true"
	 * @generated
	 */
	NodeRefinement getSelectionsource();

	/**
	 * Sets the value of the '{@link dmt.NodeSelection#getSelectionsource <em>Selectionsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selectionsource</em>' reference.
	 * @see #getSelectionsource()
	 * @generated
	 */
	void setSelectionsource(NodeRefinement value);

	/**
	 * Returns the value of the '<em><b>Selectiontarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selectiontarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selectiontarget</em>' reference.
	 * @see #setSelectiontarget(NodeRefinement)
	 * @see dmt.DmtPackage#getNodeSelection_Selectiontarget()
	 * @model required="true"
	 * @generated
	 */
	NodeRefinement getSelectiontarget();

	/**
	 * Sets the value of the '{@link dmt.NodeSelection#getSelectiontarget <em>Selectiontarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selectiontarget</em>' reference.
	 * @see #getSelectiontarget()
	 * @generated
	 */
	void setSelectiontarget(NodeRefinement value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.NodesSelectionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dmt.NodesSelectionType
	 * @see #setType(NodesSelectionType)
	 * @see dmt.DmtPackage#getNodeSelection_Type()
	 * @model
	 * @generated
	 */
	NodesSelectionType getType();

	/**
	 * Sets the value of the '{@link dmt.NodeSelection#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dmt.NodesSelectionType
	 * @see #getType()
	 * @generated
	 */
	void setType(NodesSelectionType value);

} // NodeSelection
