/**
 */
package dmt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Security Control</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Security controls are safeguards or countermeasures to avoid, detect, counteract, or minimize security risks to physical property, information, computer systems, or other assets.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.SecurityControl#getPros <em>Pros</em>}</li>
 *   <li>{@link dmt.SecurityControl#getCons <em>Cons</em>}</li>
 *   <li>{@link dmt.SecurityControl#getNature <em>Nature</em>}</li>
 *   <li>{@link dmt.SecurityControl#getIncident <em>Incident</em>}</li>
 *   <li>{@link dmt.SecurityControl#getImplements <em>Implements</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getSecurityControl()
 * @model
 * @generated
 */
public interface SecurityControl extends SecurityNode, Mitigator {
	/**
	 * Returns the value of the '<em><b>Pros</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pros</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pros</em>' attribute.
	 * @see #setPros(String)
	 * @see dmt.DmtPackage#getSecurityControl_Pros()
	 * @model
	 * @generated
	 */
	String getPros();

	/**
	 * Sets the value of the '{@link dmt.SecurityControl#getPros <em>Pros</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pros</em>' attribute.
	 * @see #getPros()
	 * @generated
	 */
	void setPros(String value);

	/**
	 * Returns the value of the '<em><b>Cons</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cons</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cons</em>' attribute.
	 * @see #setCons(String)
	 * @see dmt.DmtPackage#getSecurityControl_Cons()
	 * @model
	 * @generated
	 */
	String getCons();

	/**
	 * Sets the value of the '{@link dmt.SecurityControl#getCons <em>Cons</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cons</em>' attribute.
	 * @see #getCons()
	 * @generated
	 */
	void setCons(String value);

	/**
	 * Returns the value of the '<em><b>Nature</b></em>' attribute.
	 * The default value is <code>"TECHNICAL"</code>.
	 * The literals are from the enumeration {@link dmt.SecurityControlNatureEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nature</em>' attribute.
	 * @see dmt.SecurityControlNatureEnum
	 * @see #setNature(SecurityControlNatureEnum)
	 * @see dmt.DmtPackage#getSecurityControl_Nature()
	 * @model default="TECHNICAL"
	 * @generated
	 */
	SecurityControlNatureEnum getNature();

	/**
	 * Sets the value of the '{@link dmt.SecurityControl#getNature <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nature</em>' attribute.
	 * @see dmt.SecurityControlNatureEnum
	 * @see #getNature()
	 * @generated
	 */
	void setNature(SecurityControlNatureEnum value);

	/**
	 * Returns the value of the '<em><b>Incident</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.SecurityControlIncidentEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Incident</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Incident</em>' attribute.
	 * @see dmt.SecurityControlIncidentEnum
	 * @see #setIncident(SecurityControlIncidentEnum)
	 * @see dmt.DmtPackage#getSecurityControl_Incident()
	 * @model
	 * @generated
	 */
	SecurityControlIncidentEnum getIncident();

	/**
	 * Sets the value of the '{@link dmt.SecurityControl#getIncident <em>Incident</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Incident</em>' attribute.
	 * @see dmt.SecurityControlIncidentEnum
	 * @see #getIncident()
	 * @generated
	 */
	void setIncident(SecurityControlIncidentEnum value);

	/**
	 * Returns the value of the '<em><b>Implements</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Implements</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Implements</em>' reference.
	 * @see #setImplements(Requirement)
	 * @see dmt.DmtPackage#getSecurityControl_Implements()
	 * @model
	 * @generated
	 */
	Requirement getImplements();

	/**
	 * Sets the value of the '{@link dmt.SecurityControl#getImplements <em>Implements</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Implements</em>' reference.
	 * @see #getImplements()
	 * @generated
	 */
	void setImplements(Requirement value);

} // SecurityControl
