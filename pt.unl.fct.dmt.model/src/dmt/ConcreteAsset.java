/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concrete Asset</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ConcreteAsset#getBusinessImportance <em>Business Importance</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getConcreteAsset()
 * @model
 * @generated
 */
public interface ConcreteAsset extends Asset {
	/**
	 * Returns the value of the '<em><b>Business Importance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Business Importance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Business Importance</em>' attribute.
	 * @see #setBusinessImportance(int)
	 * @see dmt.DmtPackage#getConcreteAsset_BusinessImportance()
	 * @model
	 * @generated
	 */
	int getBusinessImportance();

	/**
	 * Sets the value of the '{@link dmt.ConcreteAsset#getBusinessImportance <em>Business Importance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Business Importance</em>' attribute.
	 * @see #getBusinessImportance()
	 * @generated
	 */
	void setBusinessImportance(int value);

} // ConcreteAsset
