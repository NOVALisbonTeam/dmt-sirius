/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Deception Goal Type Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage#getDeceptionGoalTypeEnum()
 * @model
 * @generated
 */
public enum DeceptionGoalTypeEnum implements Enumerator {
	/**
	 * The '<em><b>UNNOTICED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNNOTICED_VALUE
	 * @generated
	 * @ordered
	 */
	UNNOTICED(0, "UNNOTICED", "UNNOTICED"),

	/**
	 * The '<em><b>BENIGN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BENIGN_VALUE
	 * @generated
	 * @ordered
	 */
	BENIGN(1, "BENIGN", "BENIGN"),

	/**
	 * The '<em><b>DESIRABLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DESIRABLE_VALUE
	 * @generated
	 * @ordered
	 */
	DESIRABLE(2, "DESIRABLE", "DESIRABLE"),

	/**
	 * The '<em><b>UNAPPEALING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNAPPEALING_VALUE
	 * @generated
	 * @ordered
	 */
	UNAPPEALING(3, "UNAPPEALING", "UNAPPEALING"),

	/**
	 * The '<em><b>DANGEROUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DANGEROUS_VALUE
	 * @generated
	 * @ordered
	 */
	DANGEROUS(4, "DANGEROUS", "DANGEROUS"), /**
	 * The '<em><b>UNDEFINED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNDEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	UNDEFINED(5, "UNDEFINED", "UNDEFINED");

	/**
	 * The '<em><b>UNNOTICED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNNOTICED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNNOTICED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNNOTICED_VALUE = 0;

	/**
	 * The '<em><b>BENIGN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BENIGN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BENIGN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BENIGN_VALUE = 1;

	/**
	 * The '<em><b>DESIRABLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DESIRABLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DESIRABLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DESIRABLE_VALUE = 2;

	/**
	 * The '<em><b>UNAPPEALING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNAPPEALING</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNAPPEALING
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNAPPEALING_VALUE = 3;

	/**
	 * The '<em><b>DANGEROUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DANGEROUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DANGEROUS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DANGEROUS_VALUE = 4;

	/**
	 * The '<em><b>UNDEFINED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UNDEFINED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNDEFINED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UNDEFINED_VALUE = 5;

	/**
	 * An array of all the '<em><b>Deception Goal Type Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DeceptionGoalTypeEnum[] VALUES_ARRAY =
		new DeceptionGoalTypeEnum[] {
			UNNOTICED,
			BENIGN,
			DESIRABLE,
			UNAPPEALING,
			DANGEROUS,
			UNDEFINED,
		};

	/**
	 * A public read-only list of all the '<em><b>Deception Goal Type Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DeceptionGoalTypeEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Deception Goal Type Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DeceptionGoalTypeEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DeceptionGoalTypeEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Deception Goal Type Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DeceptionGoalTypeEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DeceptionGoalTypeEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Deception Goal Type Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DeceptionGoalTypeEnum get(int value) {
		switch (value) {
			case UNNOTICED_VALUE: return UNNOTICED;
			case BENIGN_VALUE: return BENIGN;
			case DESIRABLE_VALUE: return DESIRABLE;
			case UNAPPEALING_VALUE: return UNAPPEALING;
			case DANGEROUS_VALUE: return DANGEROUS;
			case UNDEFINED_VALUE: return UNDEFINED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DeceptionGoalTypeEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //DeceptionGoalTypeEnum
