/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal Refinable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.GoalRefinable#getOrGoalRefined <em>Or Goal Refined</em>}</li>
 *   <li>{@link dmt.GoalRefinable#getAndGoalRefined <em>And Goal Refined</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getGoalRefinable()
 * @model abstract="true"
 * @generated
 */
public interface GoalRefinable extends GoalNode {
	/**
	 * Returns the value of the '<em><b>Or Goal Refined</b></em>' reference list.
	 * The list contents are of type {@link dmt.ORGoalRefinement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Goal Refined</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Goal Refined</em>' reference list.
	 * @see dmt.DmtPackage#getGoalRefinable_OrGoalRefined()
	 * @model
	 * @generated
	 */
	EList<ORGoalRefinement> getOrGoalRefined();

	/**
	 * Returns the value of the '<em><b>And Goal Refined</b></em>' reference list.
	 * The list contents are of type {@link dmt.ANDGoalRefinement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Goal Refined</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Goal Refined</em>' reference list.
	 * @see dmt.DmtPackage#getGoalRefinable_AndGoalRefined()
	 * @model
	 * @generated
	 */
	EList<ANDGoalRefinement> getAndGoalRefined();

} // GoalRefinable
