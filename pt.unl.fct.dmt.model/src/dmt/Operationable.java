/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operationable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Operationable#getDomPost <em>Dom Post</em>}</li>
 *   <li>{@link dmt.Operationable#getDomPre <em>Dom Pre</em>}</li>
 *   <li>{@link dmt.Operationable#getFormalDomPost <em>Formal Dom Post</em>}</li>
 *   <li>{@link dmt.Operationable#getFormalDomPre <em>Formal Dom Pre</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getOperationable()
 * @model abstract="true"
 * @generated
 */
public interface Operationable extends GoalNode {
	/**
	 * Returns the value of the '<em><b>Dom Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dom Post</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dom Post</em>' attribute.
	 * @see #setDomPost(String)
	 * @see dmt.DmtPackage#getOperationable_DomPost()
	 * @model
	 * @generated
	 */
	String getDomPost();

	/**
	 * Sets the value of the '{@link dmt.Operationable#getDomPost <em>Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dom Post</em>' attribute.
	 * @see #getDomPost()
	 * @generated
	 */
	void setDomPost(String value);

	/**
	 * Returns the value of the '<em><b>Dom Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dom Pre</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dom Pre</em>' attribute.
	 * @see #setDomPre(String)
	 * @see dmt.DmtPackage#getOperationable_DomPre()
	 * @model
	 * @generated
	 */
	String getDomPre();

	/**
	 * Sets the value of the '{@link dmt.Operationable#getDomPre <em>Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dom Pre</em>' attribute.
	 * @see #getDomPre()
	 * @generated
	 */
	void setDomPre(String value);

	/**
	 * Returns the value of the '<em><b>Formal Dom Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Dom Post</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Dom Post</em>' attribute.
	 * @see #setFormalDomPost(String)
	 * @see dmt.DmtPackage#getOperationable_FormalDomPost()
	 * @model
	 * @generated
	 */
	String getFormalDomPost();

	/**
	 * Sets the value of the '{@link dmt.Operationable#getFormalDomPost <em>Formal Dom Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Dom Post</em>' attribute.
	 * @see #getFormalDomPost()
	 * @generated
	 */
	void setFormalDomPost(String value);

	/**
	 * Returns the value of the '<em><b>Formal Dom Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Dom Pre</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Dom Pre</em>' attribute.
	 * @see #setFormalDomPre(String)
	 * @see dmt.DmtPackage#getOperationable_FormalDomPre()
	 * @model
	 * @generated
	 */
	String getFormalDomPre();

	/**
	 * Sets the value of the '{@link dmt.Operationable#getFormalDomPre <em>Formal Dom Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Dom Pre</em>' attribute.
	 * @see #getFormalDomPre()
	 * @generated
	 */
	void setFormalDomPre(String value);

} // Operationable
