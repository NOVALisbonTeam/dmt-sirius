/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Strategy Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionStrategyRefinement#getDeceptionstrategyrefinementsource <em>Deceptionstrategyrefinementsource</em>}</li>
 *   <li>{@link dmt.DeceptionStrategyRefinement#getDeceptionstrategyrefinementtarget <em>Deceptionstrategyrefinementtarget</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionStrategyRefinement()
 * @model
 * @generated
 */
public interface DeceptionStrategyRefinement extends NodeRefinement {
	/**
	 * Returns the value of the '<em><b>Deceptionstrategyrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deceptionstrategyrefinementsource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deceptionstrategyrefinementsource</em>' reference.
	 * @see #setDeceptionstrategyrefinementsource(DeceptionStrategy)
	 * @see dmt.DmtPackage#getDeceptionStrategyRefinement_Deceptionstrategyrefinementsource()
	 * @model required="true"
	 * @generated
	 */
	DeceptionStrategy getDeceptionstrategyrefinementsource();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStrategyRefinement#getDeceptionstrategyrefinementsource <em>Deceptionstrategyrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deceptionstrategyrefinementsource</em>' reference.
	 * @see #getDeceptionstrategyrefinementsource()
	 * @generated
	 */
	void setDeceptionstrategyrefinementsource(DeceptionStrategy value);

	/**
	 * Returns the value of the '<em><b>Deceptionstrategyrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deceptionstrategyrefinementtarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deceptionstrategyrefinementtarget</em>' reference.
	 * @see #setDeceptionstrategyrefinementtarget(DeceptionStrategy)
	 * @see dmt.DmtPackage#getDeceptionStrategyRefinement_Deceptionstrategyrefinementtarget()
	 * @model required="true"
	 * @generated
	 */
	DeceptionStrategy getDeceptionstrategyrefinementtarget();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStrategyRefinement#getDeceptionstrategyrefinementtarget <em>Deceptionstrategyrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deceptionstrategyrefinementtarget</em>' reference.
	 * @see #getDeceptionstrategyrefinementtarget()
	 * @generated
	 */
	void setDeceptionstrategyrefinementtarget(DeceptionStrategy value);

} // DeceptionStrategyRefinement
