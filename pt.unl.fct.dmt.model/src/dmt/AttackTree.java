/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attack Tree</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.AttackTree#getHasAttackvector <em>Has Attackvector</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAttackTree()
 * @model
 * @generated
 */
public interface AttackTree extends SecurityNode {
	/**
	 * Returns the value of the '<em><b>Has Attackvector</b></em>' reference list.
	 * The list contents are of type {@link dmt.AttackVector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Attackvector</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Attackvector</em>' reference list.
	 * @see dmt.DmtPackage#getAttackTree_HasAttackvector()
	 * @model
	 * @generated
	 */
	EList<AttackVector> getHasAttackvector();

} // AttackTree
