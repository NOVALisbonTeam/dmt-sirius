/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Obstacle</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Obstacle#getObstacleCategory <em>Obstacle Category</em>}</li>
 *   <li>{@link dmt.Obstacle#getFormalDef <em>Formal Def</em>}</li>
 *   <li>{@link dmt.Obstacle#getLikelihood <em>Likelihood</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getObstacle()
 * @model
 * @generated
 */
public interface Obstacle extends ObstructionElement {
	/**
	 * Returns the value of the '<em><b>Obstacle Category</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.ObstacleCategoryEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Obstacle Category</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Obstacle Category</em>' attribute.
	 * @see dmt.ObstacleCategoryEnum
	 * @see #setObstacleCategory(ObstacleCategoryEnum)
	 * @see dmt.DmtPackage#getObstacle_ObstacleCategory()
	 * @model
	 * @generated
	 */
	ObstacleCategoryEnum getObstacleCategory();

	/**
	 * Sets the value of the '{@link dmt.Obstacle#getObstacleCategory <em>Obstacle Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Obstacle Category</em>' attribute.
	 * @see dmt.ObstacleCategoryEnum
	 * @see #getObstacleCategory()
	 * @generated
	 */
	void setObstacleCategory(ObstacleCategoryEnum value);

	/**
	 * Returns the value of the '<em><b>Formal Def</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Def</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Def</em>' attribute.
	 * @see #setFormalDef(String)
	 * @see dmt.DmtPackage#getObstacle_FormalDef()
	 * @model
	 * @generated
	 */
	String getFormalDef();

	/**
	 * Sets the value of the '{@link dmt.Obstacle#getFormalDef <em>Formal Def</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Def</em>' attribute.
	 * @see #getFormalDef()
	 * @generated
	 */
	void setFormalDef(String value);

	/**
	 * Returns the value of the '<em><b>Likelihood</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.RiskLikelihoodEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Likelihood</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Likelihood</em>' attribute.
	 * @see dmt.RiskLikelihoodEnum
	 * @see #setLikelihood(RiskLikelihoodEnum)
	 * @see dmt.DmtPackage#getObstacle_Likelihood()
	 * @model
	 * @generated
	 */
	RiskLikelihoodEnum getLikelihood();

	/**
	 * Sets the value of the '{@link dmt.Obstacle#getLikelihood <em>Likelihood</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Likelihood</em>' attribute.
	 * @see dmt.RiskLikelihoodEnum
	 * @see #getLikelihood()
	 * @generated
	 */
	void setLikelihood(RiskLikelihoodEnum value);

} // Obstacle
