/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simulation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Simulation#getType <em>Type</em>}</li>
 *   <li>{@link dmt.Simulation#getAssetReference <em>Asset Reference</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getSimulation()
 * @model
 * @generated
 */
public interface Simulation extends DeceptionTechnique {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.SimulationTechniqueTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dmt.SimulationTechniqueTypeEnum
	 * @see #setType(SimulationTechniqueTypeEnum)
	 * @see dmt.DmtPackage#getSimulation_Type()
	 * @model
	 * @generated
	 */
	SimulationTechniqueTypeEnum getType();

	/**
	 * Sets the value of the '{@link dmt.Simulation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dmt.SimulationTechniqueTypeEnum
	 * @see #getType()
	 * @generated
	 */
	void setType(SimulationTechniqueTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Asset Reference</b></em>' reference list.
	 * The list contents are of type {@link dmt.ConcreteAsset}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Asset Reference</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Asset Reference</em>' reference list.
	 * @see dmt.DmtPackage#getSimulation_AssetReference()
	 * @model
	 * @generated
	 */
	EList<ConcreteAsset> getAssetReference();

} // Simulation
