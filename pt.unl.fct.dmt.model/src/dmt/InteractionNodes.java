/**
 */
package dmt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interaction Nodes</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getInteractionNodes()
 * @model abstract="true"
 * @generated
 */
public interface InteractionNodes extends EObject {
} // InteractionNodes
