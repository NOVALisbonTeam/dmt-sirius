/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attack Step Connector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.AttackStepConnector#getSubattackstep <em>Subattackstep</em>}</li>
 *   <li>{@link dmt.AttackStepConnector#getStepDescription <em>Step Description</em>}</li>
 *   <li>{@link dmt.AttackStepConnector#getAttackStepConnectorType <em>Attack Step Connector Type</em>}</li>
 *   <li>{@link dmt.AttackStepConnector#getSequence <em>Sequence</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAttackStepConnector()
 * @model
 * @generated
 */
public interface AttackStepConnector extends AttackStep {
	/**
	 * Returns the value of the '<em><b>Subattackstep</b></em>' reference list.
	 * The list contents are of type {@link dmt.AttackStepConnector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subattackstep</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subattackstep</em>' reference list.
	 * @see dmt.DmtPackage#getAttackStepConnector_Subattackstep()
	 * @model
	 * @generated
	 */
	EList<AttackStepConnector> getSubattackstep();

	/**
	 * Returns the value of the '<em><b>Step Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Step Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Step Description</em>' attribute.
	 * @see #setStepDescription(String)
	 * @see dmt.DmtPackage#getAttackStepConnector_StepDescription()
	 * @model unique="false" ordered="false"
	 * @generated
	 */
	String getStepDescription();

	/**
	 * Sets the value of the '{@link dmt.AttackStepConnector#getStepDescription <em>Step Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Step Description</em>' attribute.
	 * @see #getStepDescription()
	 * @generated
	 */
	void setStepDescription(String value);

	/**
	 * Returns the value of the '<em><b>Attack Step Connector Type</b></em>' attribute.
	 * The default value is <code>"OR"</code>.
	 * The literals are from the enumeration {@link dmt.AttackConnectorEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attack Step Connector Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attack Step Connector Type</em>' attribute.
	 * @see dmt.AttackConnectorEnum
	 * @see #setAttackStepConnectorType(AttackConnectorEnum)
	 * @see dmt.DmtPackage#getAttackStepConnector_AttackStepConnectorType()
	 * @model default="OR" unique="false" ordered="false"
	 * @generated
	 */
	AttackConnectorEnum getAttackStepConnectorType();

	/**
	 * Sets the value of the '{@link dmt.AttackStepConnector#getAttackStepConnectorType <em>Attack Step Connector Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attack Step Connector Type</em>' attribute.
	 * @see dmt.AttackConnectorEnum
	 * @see #getAttackStepConnectorType()
	 * @generated
	 */
	void setAttackStepConnectorType(AttackConnectorEnum value);

	/**
	 * Returns the value of the '<em><b>Sequence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sequence</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequence</em>' attribute.
	 * @see #setSequence(int)
	 * @see dmt.DmtPackage#getAttackStepConnector_Sequence()
	 * @model
	 * @generated
	 */
	int getSequence();

	/**
	 * Sets the value of the '{@link dmt.AttackStepConnector#getSequence <em>Sequence</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sequence</em>' attribute.
	 * @see #getSequence()
	 * @generated
	 */
	void setSequence(int value);

} // AttackStepConnector
