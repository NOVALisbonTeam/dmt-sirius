/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Soft Goal Type Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage#getSoftGoalTypeEnum()
 * @model
 * @generated
 */
public enum SoftGoalTypeEnum implements Enumerator {
	/**
	 * The '<em><b>INCREASE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INCREASE_VALUE
	 * @generated
	 * @ordered
	 */
	INCREASE(0, "INCREASE", "INCREASE"),

	/**
	 * The '<em><b>DECREASE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DECREASE_VALUE
	 * @generated
	 * @ordered
	 */
	DECREASE(1, "DECREASE", "DECREASE"),

	/**
	 * The '<em><b>IMPROVE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPROVE_VALUE
	 * @generated
	 * @ordered
	 */
	IMPROVE(2, "IMPROVE", "IMPROVE"),

	/**
	 * The '<em><b>MITIGATE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MITIGATE_VALUE
	 * @generated
	 * @ordered
	 */
	MITIGATE(3, "MITIGATE", "MITIGATE"), /**
	 * The '<em><b>MAXIMIZE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MAXIMIZE_VALUE
	 * @generated
	 * @ordered
	 */
	MAXIMIZE(4, "MAXIMIZE", "MAXIMIZE"), /**
	 * The '<em><b>MINIMIZE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINIMIZE_VALUE
	 * @generated
	 * @ordered
	 */
	MINIMIZE(5, "MINIMIZE", "MINIMIZE"), /**
	 * The '<em><b>REDUCE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REDUCE_VALUE
	 * @generated
	 * @ordered
	 */
	REDUCE(6, "REDUCE", "REDUCE"), /**
	 * The '<em><b>OTHER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(7, "OTHER", "OTHER");

	/**
	 * The '<em><b>INCREASE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INCREASE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INCREASE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INCREASE_VALUE = 0;

	/**
	 * The '<em><b>DECREASE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DECREASE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DECREASE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DECREASE_VALUE = 1;

	/**
	 * The '<em><b>IMPROVE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IMPROVE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IMPROVE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IMPROVE_VALUE = 2;

	/**
	 * The '<em><b>MITIGATE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MITIGATE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MITIGATE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MITIGATE_VALUE = 3;

	/**
	 * The '<em><b>MAXIMIZE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MAXIMIZE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MAXIMIZE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MAXIMIZE_VALUE = 4;

	/**
	 * The '<em><b>MINIMIZE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MINIMIZE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINIMIZE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MINIMIZE_VALUE = 5;

	/**
	 * The '<em><b>REDUCE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>REDUCE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REDUCE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REDUCE_VALUE = 6;

	/**
	 * The '<em><b>OTHER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>OTHER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 7;

	/**
	 * An array of all the '<em><b>Soft Goal Type Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SoftGoalTypeEnum[] VALUES_ARRAY =
		new SoftGoalTypeEnum[] {
			INCREASE,
			DECREASE,
			IMPROVE,
			MITIGATE,
			MAXIMIZE,
			MINIMIZE,
			REDUCE,
			OTHER,
		};

	/**
	 * A public read-only list of all the '<em><b>Soft Goal Type Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SoftGoalTypeEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Soft Goal Type Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SoftGoalTypeEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SoftGoalTypeEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Soft Goal Type Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SoftGoalTypeEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SoftGoalTypeEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Soft Goal Type Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SoftGoalTypeEnum get(int value) {
		switch (value) {
			case INCREASE_VALUE: return INCREASE;
			case DECREASE_VALUE: return DECREASE;
			case IMPROVE_VALUE: return IMPROVE;
			case MITIGATE_VALUE: return MITIGATE;
			case MAXIMIZE_VALUE: return MAXIMIZE;
			case MINIMIZE_VALUE: return MINIMIZE;
			case REDUCE_VALUE: return REDUCE;
			case OTHER_VALUE: return OTHER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SoftGoalTypeEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SoftGoalTypeEnum
