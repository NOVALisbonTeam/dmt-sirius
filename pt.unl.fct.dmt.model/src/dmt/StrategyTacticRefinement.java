/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Strategy Tactic Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.StrategyTacticRefinement#getStrategytacticrefinementtarget <em>Strategytacticrefinementtarget</em>}</li>
 *   <li>{@link dmt.StrategyTacticRefinement#getStrategytacticrefinementsource <em>Strategytacticrefinementsource</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getStrategyTacticRefinement()
 * @model
 * @generated
 */
public interface StrategyTacticRefinement extends NodeRefinement {
	/**
	 * Returns the value of the '<em><b>Strategytacticrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strategytacticrefinementtarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strategytacticrefinementtarget</em>' reference.
	 * @see #setStrategytacticrefinementtarget(DeceptionTacticMechanism)
	 * @see dmt.DmtPackage#getStrategyTacticRefinement_Strategytacticrefinementtarget()
	 * @model required="true"
	 * @generated
	 */
	DeceptionTacticMechanism getStrategytacticrefinementtarget();

	/**
	 * Sets the value of the '{@link dmt.StrategyTacticRefinement#getStrategytacticrefinementtarget <em>Strategytacticrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strategytacticrefinementtarget</em>' reference.
	 * @see #getStrategytacticrefinementtarget()
	 * @generated
	 */
	void setStrategytacticrefinementtarget(DeceptionTacticMechanism value);

	/**
	 * Returns the value of the '<em><b>Strategytacticrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strategytacticrefinementsource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strategytacticrefinementsource</em>' reference.
	 * @see #setStrategytacticrefinementsource(DeceptionStrategy)
	 * @see dmt.DmtPackage#getStrategyTacticRefinement_Strategytacticrefinementsource()
	 * @model required="true"
	 * @generated
	 */
	DeceptionStrategy getStrategytacticrefinementsource();

	/**
	 * Sets the value of the '{@link dmt.StrategyTacticRefinement#getStrategytacticrefinementsource <em>Strategytacticrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strategytacticrefinementsource</em>' reference.
	 * @see #getStrategytacticrefinementsource()
	 * @generated
	 */
	void setStrategytacticrefinementsource(DeceptionStrategy value);

} // StrategyTacticRefinement
