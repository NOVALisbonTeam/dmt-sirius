/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Influence Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getInfluenceNode()
 * @model
 * @generated
 */
public interface InfluenceNode extends Node {
} // InfluenceNode
