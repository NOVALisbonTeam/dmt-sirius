/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Tactic Strategy Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionTacticStrategyRefinement#getTacticstrategytacticrefinementsource <em>Tacticstrategytacticrefinementsource</em>}</li>
 *   <li>{@link dmt.DeceptionTacticStrategyRefinement#getTacticstrategytacticrefinementtarget <em>Tacticstrategytacticrefinementtarget</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionTacticStrategyRefinement()
 * @model
 * @generated
 */
public interface DeceptionTacticStrategyRefinement extends NodeRefinement {
	/**
	 * Returns the value of the '<em><b>Tacticstrategytacticrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tacticstrategytacticrefinementsource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tacticstrategytacticrefinementsource</em>' reference.
	 * @see #setTacticstrategytacticrefinementsource(DeceptionTacticStrategy)
	 * @see dmt.DmtPackage#getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementsource()
	 * @model required="true"
	 * @generated
	 */
	DeceptionTacticStrategy getTacticstrategytacticrefinementsource();

	/**
	 * Sets the value of the '{@link dmt.DeceptionTacticStrategyRefinement#getTacticstrategytacticrefinementsource <em>Tacticstrategytacticrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tacticstrategytacticrefinementsource</em>' reference.
	 * @see #getTacticstrategytacticrefinementsource()
	 * @generated
	 */
	void setTacticstrategytacticrefinementsource(DeceptionTacticStrategy value);

	/**
	 * Returns the value of the '<em><b>Tacticstrategytacticrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tacticstrategytacticrefinementtarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tacticstrategytacticrefinementtarget</em>' reference.
	 * @see #setTacticstrategytacticrefinementtarget(DeceptionTacticStrategy)
	 * @see dmt.DmtPackage#getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementtarget()
	 * @model required="true"
	 * @generated
	 */
	DeceptionTacticStrategy getTacticstrategytacticrefinementtarget();

	/**
	 * Sets the value of the '{@link dmt.DeceptionTacticStrategyRefinement#getTacticstrategytacticrefinementtarget <em>Tacticstrategytacticrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tacticstrategytacticrefinementtarget</em>' reference.
	 * @see #getTacticstrategytacticrefinementtarget()
	 * @generated
	 */
	void setTacticstrategytacticrefinementtarget(DeceptionTacticStrategy value);

} // DeceptionTacticStrategyRefinement
