/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exclude Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getExcludeConstraint()
 * @model
 * @generated
 */
public interface ExcludeConstraint extends FeatureConstraint {
} // ExcludeConstraint
