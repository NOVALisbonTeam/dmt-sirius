/**
 */
package dmt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mitigator</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getMitigator()
 * @model abstract="true"
 * @generated
 */
public interface Mitigator extends EObject {
} // Mitigator
