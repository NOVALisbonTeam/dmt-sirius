/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.FeatureConstraint#getSource <em>Source</em>}</li>
 *   <li>{@link dmt.FeatureConstraint#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getFeatureConstraint()
 * @model abstract="true"
 * @generated
 */
public interface FeatureConstraint extends StrategyRelationship {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(AbstractFeature)
	 * @see dmt.DmtPackage#getFeatureConstraint_Source()
	 * @model required="true"
	 * @generated
	 */
	AbstractFeature getSource();

	/**
	 * Sets the value of the '{@link dmt.FeatureConstraint#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(AbstractFeature value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(AbstractFeature)
	 * @see dmt.DmtPackage#getFeatureConstraint_Target()
	 * @model required="true"
	 * @generated
	 */
	AbstractFeature getTarget();

	/**
	 * Sets the value of the '{@link dmt.FeatureConstraint#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(AbstractFeature value);

} // FeatureConstraint
