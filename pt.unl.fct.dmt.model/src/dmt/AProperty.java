/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AProperty</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.AProperty#getAtype <em>Atype</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAProperty()
 * @model
 * @generated
 */
public interface AProperty extends SecurityNode {
	/**
	 * Returns the value of the '<em><b>Atype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Atype</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Atype</em>' attribute.
	 * @see #setAtype(String)
	 * @see dmt.DmtPackage#getAProperty_Atype()
	 * @model
	 * @generated
	 */
	String getAtype();

	/**
	 * Sets the value of the '{@link dmt.AProperty#getAtype <em>Atype</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Atype</em>' attribute.
	 * @see #getAtype()
	 * @generated
	 */
	void setAtype(String value);

} // AProperty
