/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AND Conflict Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ANDConflictGoalRefinement#getAndConflictGoalRefine <em>And Conflict Goal Refine</em>}</li>
 *   <li>{@link dmt.ANDConflictGoalRefinement#getAltName <em>Alt Name</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getANDConflictGoalRefinement()
 * @model
 * @generated
 */
public interface ANDConflictGoalRefinement extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>And Conflict Goal Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>And Conflict Goal Refine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>And Conflict Goal Refine</em>' reference.
	 * @see #setAndConflictGoalRefine(Goal)
	 * @see dmt.DmtPackage#getANDConflictGoalRefinement_AndConflictGoalRefine()
	 * @model required="true"
	 * @generated
	 */
	Goal getAndConflictGoalRefine();

	/**
	 * Sets the value of the '{@link dmt.ANDConflictGoalRefinement#getAndConflictGoalRefine <em>And Conflict Goal Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>And Conflict Goal Refine</em>' reference.
	 * @see #getAndConflictGoalRefine()
	 * @generated
	 */
	void setAndConflictGoalRefine(Goal value);

	/**
	 * Returns the value of the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alt Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alt Name</em>' attribute.
	 * @see #setAltName(String)
	 * @see dmt.DmtPackage#getANDConflictGoalRefinement_AltName()
	 * @model
	 * @generated
	 */
	String getAltName();

	/**
	 * Sets the value of the '{@link dmt.ANDConflictGoalRefinement#getAltName <em>Alt Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alt Name</em>' attribute.
	 * @see #getAltName()
	 * @generated
	 */
	void setAltName(String value);

} // ANDConflictGoalRefinement
