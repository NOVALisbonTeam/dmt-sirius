/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Link#getIntegerId <em>Integer Id</em>}</li>
 *   <li>{@link dmt.Link#getMessage <em>Message</em>}</li>
 *   <li>{@link dmt.Link#getToObject <em>To Object</em>}</li>
 *   <li>{@link dmt.Link#getFromObject <em>From Object</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getLink()
 * @model
 * @generated
 */
public interface Link extends InteractionNodes {
	/**
	 * Returns the value of the '<em><b>Integer Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Integer Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Integer Id</em>' attribute.
	 * @see #setIntegerId(int)
	 * @see dmt.DmtPackage#getLink_IntegerId()
	 * @model id="true" required="true"
	 * @generated
	 */
	int getIntegerId();

	/**
	 * Sets the value of the '{@link dmt.Link#getIntegerId <em>Integer Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Integer Id</em>' attribute.
	 * @see #getIntegerId()
	 * @generated
	 */
	void setIntegerId(int value);

	/**
	 * Returns the value of the '<em><b>Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message</em>' attribute.
	 * @see #setMessage(String)
	 * @see dmt.DmtPackage#getLink_Message()
	 * @model
	 * @generated
	 */
	String getMessage();

	/**
	 * Sets the value of the '{@link dmt.Link#getMessage <em>Message</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message</em>' attribute.
	 * @see #getMessage()
	 * @generated
	 */
	void setMessage(String value);

	/**
	 * Returns the value of the '<em><b>To Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Object</em>' reference.
	 * @see #setToObject(InteractionObject)
	 * @see dmt.DmtPackage#getLink_ToObject()
	 * @model
	 * @generated
	 */
	InteractionObject getToObject();

	/**
	 * Sets the value of the '{@link dmt.Link#getToObject <em>To Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Object</em>' reference.
	 * @see #getToObject()
	 * @generated
	 */
	void setToObject(InteractionObject value);

	/**
	 * Returns the value of the '<em><b>From Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Object</em>' reference.
	 * @see #setFromObject(InteractionObject)
	 * @see dmt.DmtPackage#getLink_FromObject()
	 * @model
	 * @generated
	 */
	InteractionObject getFromObject();

	/**
	 * Sets the value of the '{@link dmt.Link#getFromObject <em>From Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Object</em>' reference.
	 * @see #getFromObject()
	 * @generated
	 */
	void setFromObject(InteractionObject value);

} // Link
