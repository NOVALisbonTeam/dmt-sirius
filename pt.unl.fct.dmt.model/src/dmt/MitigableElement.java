/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mitigable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getMitigableElement()
 * @model abstract="true"
 * @generated
 */
public interface MitigableElement extends SecurityNode {
} // MitigableElement
