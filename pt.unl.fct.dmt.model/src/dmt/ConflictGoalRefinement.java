/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conflict Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ConflictGoalRefinement#getConflictGoalSource <em>Conflict Goal Source</em>}</li>
 *   <li>{@link dmt.ConflictGoalRefinement#getConflictGoalTarget <em>Conflict Goal Target</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getConflictGoalRefinement()
 * @model
 * @generated
 */
public interface ConflictGoalRefinement extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>Conflict Goal Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conflict Goal Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conflict Goal Source</em>' reference.
	 * @see #setConflictGoalSource(Goal)
	 * @see dmt.DmtPackage#getConflictGoalRefinement_ConflictGoalSource()
	 * @model required="true"
	 * @generated
	 */
	Goal getConflictGoalSource();

	/**
	 * Sets the value of the '{@link dmt.ConflictGoalRefinement#getConflictGoalSource <em>Conflict Goal Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conflict Goal Source</em>' reference.
	 * @see #getConflictGoalSource()
	 * @generated
	 */
	void setConflictGoalSource(Goal value);

	/**
	 * Returns the value of the '<em><b>Conflict Goal Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conflict Goal Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conflict Goal Target</em>' reference.
	 * @see #setConflictGoalTarget(Goal)
	 * @see dmt.DmtPackage#getConflictGoalRefinement_ConflictGoalTarget()
	 * @model required="true"
	 * @generated
	 */
	Goal getConflictGoalTarget();

	/**
	 * Sets the value of the '{@link dmt.ConflictGoalRefinement#getConflictGoalTarget <em>Conflict Goal Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conflict Goal Target</em>' reference.
	 * @see #getConflictGoalTarget()
	 * @generated
	 */
	void setConflictGoalTarget(Goal value);

} // ConflictGoalRefinement
