/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Software Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.SoftwareAgent#getResponsibilitySoftware <em>Responsibility Software</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getSoftwareAgent()
 * @model
 * @generated
 */
public interface SoftwareAgent extends Agent {

	/**
	 * Returns the value of the '<em><b>Responsibility Software</b></em>' reference list.
	 * The list contents are of type {@link dmt.Requirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsibility Software</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsibility Software</em>' reference list.
	 * @see dmt.DmtPackage#getSoftwareAgent_ResponsibilitySoftware()
	 * @model required="true"
	 * @generated
	 */
	EList<Requirement> getResponsibilitySoftware();
} // SoftwareAgent
