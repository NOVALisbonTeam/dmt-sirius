/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Strategy Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.StrategyRefinement#getStrategyrefinementsource <em>Strategyrefinementsource</em>}</li>
 *   <li>{@link dmt.StrategyRefinement#getStrategyrefinementtarget <em>Strategyrefinementtarget</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getStrategyRefinement()
 * @model
 * @generated
 */
public interface StrategyRefinement extends NodeRefinement {
	/**
	 * Returns the value of the '<em><b>Strategyrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strategyrefinementsource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strategyrefinementsource</em>' reference.
	 * @see #setStrategyrefinementsource(DeceptionStrategy)
	 * @see dmt.DmtPackage#getStrategyRefinement_Strategyrefinementsource()
	 * @model required="true"
	 * @generated
	 */
	DeceptionStrategy getStrategyrefinementsource();

	/**
	 * Sets the value of the '{@link dmt.StrategyRefinement#getStrategyrefinementsource <em>Strategyrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strategyrefinementsource</em>' reference.
	 * @see #getStrategyrefinementsource()
	 * @generated
	 */
	void setStrategyrefinementsource(DeceptionStrategy value);

	/**
	 * Returns the value of the '<em><b>Strategyrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strategyrefinementtarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strategyrefinementtarget</em>' reference.
	 * @see #setStrategyrefinementtarget(DeceptionStrategy)
	 * @see dmt.DmtPackage#getStrategyRefinement_Strategyrefinementtarget()
	 * @model required="true"
	 * @generated
	 */
	DeceptionStrategy getStrategyrefinementtarget();

	/**
	 * Sets the value of the '{@link dmt.StrategyRefinement#getStrategyrefinementtarget <em>Strategyrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strategyrefinementtarget</em>' reference.
	 * @see #getStrategyrefinementtarget()
	 * @generated
	 */
	void setStrategyrefinementtarget(DeceptionStrategy value);

} // StrategyRefinement
