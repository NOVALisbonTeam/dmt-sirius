/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attack Vector</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * The 'route' by which an attack was carried out. SQLi is typically carried out using a browser client to the web application. The web application is the attack vector (possibly also the Internet, the client application, etc.; it depends on your focus).
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.AttackVector#getExample <em>Example</em>}</li>
 *   <li>{@link dmt.AttackVector#getResources <em>Resources</em>}</li>
 *   <li>{@link dmt.AttackVector#getRealizesAttack <em>Realizes Attack</em>}</li>
 *   <li>{@link dmt.AttackVector#getInitialattackstep <em>Initialattackstep</em>}</li>
 *   <li>{@link dmt.AttackVector#getEndattackstep <em>Endattackstep</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAttackVector()
 * @model
 * @generated
 */
public interface AttackVector extends SecurityNode {
	/**
	 * Returns the value of the '<em><b>Example</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * An example of how the vector operates
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Example</em>' attribute.
	 * @see #setExample(String)
	 * @see dmt.DmtPackage#getAttackVector_Example()
	 * @model
	 * @generated
	 */
	String getExample();

	/**
	 * Sets the value of the '{@link dmt.AttackVector#getExample <em>Example</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Example</em>' attribute.
	 * @see #getExample()
	 * @generated
	 */
	void setExample(String value);

	/**
	 * Returns the value of the '<em><b>Resources</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * An exploit, a tool/code used to carry on an attack
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Resources</em>' attribute.
	 * @see #setResources(String)
	 * @see dmt.DmtPackage#getAttackVector_Resources()
	 * @model
	 * @generated
	 */
	String getResources();

	/**
	 * Sets the value of the '{@link dmt.AttackVector#getResources <em>Resources</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resources</em>' attribute.
	 * @see #getResources()
	 * @generated
	 */
	void setResources(String value);

	/**
	 * Returns the value of the '<em><b>Realizes Attack</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link dmt.Attack#getRealizedByAttackVector <em>Realized By Attack Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Realizes Attack</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Realizes Attack</em>' reference.
	 * @see #setRealizesAttack(Attack)
	 * @see dmt.DmtPackage#getAttackVector_RealizesAttack()
	 * @see dmt.Attack#getRealizedByAttackVector
	 * @model opposite="realizedByAttackVector"
	 * @generated
	 */
	Attack getRealizesAttack();

	/**
	 * Sets the value of the '{@link dmt.AttackVector#getRealizesAttack <em>Realizes Attack</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Realizes Attack</em>' reference.
	 * @see #getRealizesAttack()
	 * @generated
	 */
	void setRealizesAttack(Attack value);

	/**
	 * Returns the value of the '<em><b>Initialattackstep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialattackstep</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialattackstep</em>' containment reference.
	 * @see #setInitialattackstep(InitialAttackStep)
	 * @see dmt.DmtPackage#getAttackVector_Initialattackstep()
	 * @model containment="true"
	 * @generated
	 */
	InitialAttackStep getInitialattackstep();

	/**
	 * Sets the value of the '{@link dmt.AttackVector#getInitialattackstep <em>Initialattackstep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initialattackstep</em>' containment reference.
	 * @see #getInitialattackstep()
	 * @generated
	 */
	void setInitialattackstep(InitialAttackStep value);

	/**
	 * Returns the value of the '<em><b>Endattackstep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Endattackstep</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Endattackstep</em>' containment reference.
	 * @see #setEndattackstep(EndAttackStep)
	 * @see dmt.DmtPackage#getAttackVector_Endattackstep()
	 * @model containment="true"
	 * @generated
	 */
	EndAttackStep getEndattackstep();

	/**
	 * Sets the value of the '{@link dmt.AttackVector#getEndattackstep <em>Endattackstep</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Endattackstep</em>' containment reference.
	 * @see #getEndattackstep()
	 * @generated
	 */
	void setEndattackstep(EndAttackStep value);

} // AttackVector
