/**
 */
package dmt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getAgentRelation()
 * @model abstract="true"
 * @generated
 */
public interface AgentRelation extends GoalRelationship {

} // AgentRelation
