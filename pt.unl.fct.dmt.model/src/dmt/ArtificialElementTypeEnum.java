/**
 */
package dmt;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Artificial Element Type Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage#getArtificialElementTypeEnum()
 * @model
 * @generated
 */
public enum ArtificialElementTypeEnum implements Enumerator {
	/**
	 * The '<em><b>FAKEDATA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FAKEDATA_VALUE
	 * @generated
	 * @ordered
	 */
	FAKEDATA(0, "FAKEDATA", "FAKEDATA"), /**
	 * The '<em><b>FAKECODE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FAKECODE_VALUE
	 * @generated
	 * @ordered
	 */
	FAKECODE(1, "FAKECODE", "FAKECODE"), /**
	 * The '<em><b>FAKESERVICE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FAKESERVICE_VALUE
	 * @generated
	 * @ordered
	 */
	FAKESERVICE(2, "FAKESERVICE", "FAKESERVICE"), /**
	 * The '<em><b>FAKEHARDWARE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FAKEHARDWARE_VALUE
	 * @generated
	 * @ordered
	 */
	FAKEHARDWARE(3, "FAKEHARDWARE", "FAKEHARDWARE"), /**
	 * The '<em><b>FAKEFACILITY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FAKEFACILITY_VALUE
	 * @generated
	 * @ordered
	 */
	FAKEFACILITY(4, "FAKEFACILITY", "FAKEFACILITY"), /**
	 * The '<em><b>FAKEEVENT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FAKEEVENT_VALUE
	 * @generated
	 * @ordered
	 */
	FAKEEVENT(5, "FAKEEVENT", "FAKEEVENT"), /**
	 * The '<em><b>FAKEDELAY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FAKEDELAY_VALUE
	 * @generated
	 * @ordered
	 */
	FAKEDELAY(6, "FAKEDELAY", "FAKEDELAY"), /**
	 * The '<em><b>FAKEOTHER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FAKEOTHER_VALUE
	 * @generated
	 * @ordered
	 */
	FAKEOTHER(7, "FAKEOTHER", "FAKEOTHER");

	/**
	 * The '<em><b>FAKEDATA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FAKEDATA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FAKEDATA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAKEDATA_VALUE = 0;

	/**
	 * The '<em><b>FAKECODE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FAKECODE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FAKECODE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAKECODE_VALUE = 1;

	/**
	 * The '<em><b>FAKESERVICE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FAKESERVICE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FAKESERVICE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAKESERVICE_VALUE = 2;

	/**
	 * The '<em><b>FAKEHARDWARE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FAKEHARDWARE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FAKEHARDWARE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAKEHARDWARE_VALUE = 3;

	/**
	 * The '<em><b>FAKEFACILITY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FAKEFACILITY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FAKEFACILITY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAKEFACILITY_VALUE = 4;

	/**
	 * The '<em><b>FAKEEVENT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FAKEEVENT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FAKEEVENT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAKEEVENT_VALUE = 5;

	/**
	 * The '<em><b>FAKEDELAY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FAKEDELAY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FAKEDELAY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAKEDELAY_VALUE = 6;

	/**
	 * The '<em><b>FAKEOTHER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FAKEOTHER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FAKEOTHER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FAKEOTHER_VALUE = 7;

	/**
	 * An array of all the '<em><b>Artificial Element Type Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ArtificialElementTypeEnum[] VALUES_ARRAY =
		new ArtificialElementTypeEnum[] {
			FAKEDATA,
			FAKECODE,
			FAKESERVICE,
			FAKEHARDWARE,
			FAKEFACILITY,
			FAKEEVENT,
			FAKEDELAY,
			FAKEOTHER,
		};

	/**
	 * A public read-only list of all the '<em><b>Artificial Element Type Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ArtificialElementTypeEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Artificial Element Type Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ArtificialElementTypeEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ArtificialElementTypeEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Artificial Element Type Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ArtificialElementTypeEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ArtificialElementTypeEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Artificial Element Type Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ArtificialElementTypeEnum get(int value) {
		switch (value) {
			case FAKEDATA_VALUE: return FAKEDATA;
			case FAKECODE_VALUE: return FAKECODE;
			case FAKESERVICE_VALUE: return FAKESERVICE;
			case FAKEHARDWARE_VALUE: return FAKEHARDWARE;
			case FAKEFACILITY_VALUE: return FAKEFACILITY;
			case FAKEEVENT_VALUE: return FAKEEVENT;
			case FAKEDELAY_VALUE: return FAKEDELAY;
			case FAKEOTHER_VALUE: return FAKEOTHER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ArtificialElementTypeEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ArtificialElementTypeEnum
