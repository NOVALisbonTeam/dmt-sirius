/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Obstruction Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ObstructionElement#getObstruct <em>Obstruct</em>}</li>
 *   <li>{@link dmt.ObstructionElement#getORObstructionRefined <em>OR Obstruction Refined</em>}</li>
 *   <li>{@link dmt.ObstructionElement#getANDObstructionRefined <em>AND Obstruction Refined</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getObstructionElement()
 * @model abstract="true"
 * @generated
 */
public interface ObstructionElement extends GoalNode {

	/**
	 * Returns the value of the '<em><b>Obstruct</b></em>' reference list.
	 * The list contents are of type {@link dmt.ObstructedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Obstruct</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Obstruct</em>' reference list.
	 * @see dmt.DmtPackage#getObstructionElement_Obstruct()
	 * @model
	 * @generated
	 */
	EList<ObstructedElement> getObstruct();

	/**
	 * Returns the value of the '<em><b>OR Obstruction Refined</b></em>' reference list.
	 * The list contents are of type {@link dmt.ORObstructionRefinement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>OR Obstruction Refined</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OR Obstruction Refined</em>' reference list.
	 * @see dmt.DmtPackage#getObstructionElement_ORObstructionRefined()
	 * @model
	 * @generated
	 */
	EList<ORObstructionRefinement> getORObstructionRefined();

	/**
	 * Returns the value of the '<em><b>AND Obstruction Refined</b></em>' reference list.
	 * The list contents are of type {@link dmt.ANDObstructionRefinement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AND Obstruction Refined</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AND Obstruction Refined</em>' reference list.
	 * @see dmt.DmtPackage#getObstructionElement_ANDObstructionRefined()
	 * @model
	 * @generated
	 */
	EList<ANDObstructionRefinement> getANDObstructionRefined();

	/**
	 * Returns the value of the '<em><b>AND Obstruction Refined</b></em>' reference list.
	 * The list contents are of type {@link dmt.ANDObstructionRefinement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AND Obstruction Refined</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AND Obstruction Refined</em>' reference list.
	 * @see dmt.DmtPackage#getObstructionElement_ANDObstructionRefined()
	 * @model
	 * @generated
	 */
	

} // ObstructionElement
