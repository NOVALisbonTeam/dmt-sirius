/**
 */
package dmt;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Mitigation Level Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage#getMitigationLevelEnum()
 * @model
 * @generated
 */
public enum MitigationLevelEnum implements Enumerator {
	/**
	 * The '<em><b>LOW MITIGATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LOW_MITIGATION_VALUE
	 * @generated
	 * @ordered
	 */
	LOW_MITIGATION(0, "LOW_MITIGATION", "M-"),

	/**
	 * The '<em><b>NORMAL MITIGATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NORMAL_MITIGATION_VALUE
	 * @generated
	 * @ordered
	 */
	NORMAL_MITIGATION(1, "NORMAL_MITIGATION", "M"),

	/**
	 * The '<em><b>HIGH MITIGATION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HIGH_MITIGATION_VALUE
	 * @generated
	 * @ordered
	 */
	HIGH_MITIGATION(2, "HIGH_MITIGATION", "M+");

	/**
	 * The '<em><b>LOW MITIGATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LOW MITIGATION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LOW_MITIGATION
	 * @model literal="M-"
	 * @generated
	 * @ordered
	 */
	public static final int LOW_MITIGATION_VALUE = 0;

	/**
	 * The '<em><b>NORMAL MITIGATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NORMAL MITIGATION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NORMAL_MITIGATION
	 * @model literal="M"
	 * @generated
	 * @ordered
	 */
	public static final int NORMAL_MITIGATION_VALUE = 1;

	/**
	 * The '<em><b>HIGH MITIGATION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>HIGH MITIGATION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HIGH_MITIGATION
	 * @model literal="M+"
	 * @generated
	 * @ordered
	 */
	public static final int HIGH_MITIGATION_VALUE = 2;

	/**
	 * An array of all the '<em><b>Mitigation Level Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MitigationLevelEnum[] VALUES_ARRAY =
		new MitigationLevelEnum[] {
			LOW_MITIGATION,
			NORMAL_MITIGATION,
			HIGH_MITIGATION,
		};

	/**
	 * A public read-only list of all the '<em><b>Mitigation Level Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MitigationLevelEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Mitigation Level Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MitigationLevelEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MitigationLevelEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Mitigation Level Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MitigationLevelEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MitigationLevelEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Mitigation Level Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static MitigationLevelEnum get(int value) {
		switch (value) {
			case LOW_MITIGATION_VALUE: return LOW_MITIGATION;
			case NORMAL_MITIGATION_VALUE: return NORMAL_MITIGATION;
			case HIGH_MITIGATION_VALUE: return HIGH_MITIGATION;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MitigationLevelEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //MitigationLevelEnum
