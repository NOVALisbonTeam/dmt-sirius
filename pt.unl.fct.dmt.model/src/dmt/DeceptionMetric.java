/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionMetric#getMetricType <em>Metric Type</em>}</li>
 *   <li>{@link dmt.DeceptionMetric#getUnit <em>Unit</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionMetric()
 * @model
 * @generated
 */
public interface DeceptionMetric extends DeceptionNode {
	/**
	 * Returns the value of the '<em><b>Metric Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.MetricTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Type</em>' attribute.
	 * @see dmt.MetricTypeEnum
	 * @see #setMetricType(MetricTypeEnum)
	 * @see dmt.DmtPackage#getDeceptionMetric_MetricType()
	 * @model
	 * @generated
	 */
	MetricTypeEnum getMetricType();

	/**
	 * Sets the value of the '{@link dmt.DeceptionMetric#getMetricType <em>Metric Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Type</em>' attribute.
	 * @see dmt.MetricTypeEnum
	 * @see #getMetricType()
	 * @generated
	 */
	void setMetricType(MetricTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' attribute.
	 * @see #setUnit(String)
	 * @see dmt.DmtPackage#getDeceptionMetric_Unit()
	 * @model
	 * @generated
	 */
	String getUnit();

	/**
	 * Sets the value of the '{@link dmt.DeceptionMetric#getUnit <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' attribute.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(String value);

} // DeceptionMetric
