/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Expectation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getExpectation()
 * @model
 * @generated
 */
public interface Expectation extends LeafGoal {
} // Expectation
