/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Join Operationalization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.JoinOperationalization#getOperationalizeGoal <em>Operationalize Goal</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getJoinOperationalization()
 * @model
 * @generated
 */
public interface JoinOperationalization extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>Operationalize Goal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operationalize Goal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operationalize Goal</em>' reference.
	 * @see #setOperationalizeGoal(LeafGoal)
	 * @see dmt.DmtPackage#getJoinOperationalization_OperationalizeGoal()
	 * @model required="true"
	 * @generated
	 */
	LeafGoal getOperationalizeGoal();

	/**
	 * Sets the value of the '{@link dmt.JoinOperationalization#getOperationalizeGoal <em>Operationalize Goal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operationalize Goal</em>' reference.
	 * @see #getOperationalizeGoal()
	 * @generated
	 */
	void setOperationalizeGoal(LeafGoal value);

} // JoinOperationalization
