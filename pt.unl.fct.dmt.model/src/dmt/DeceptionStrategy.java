/**
 */
package dmt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionStrategy#isIsRoot <em>Is Root</em>}</li>
 *   <li>{@link dmt.DeceptionStrategy#getExecutionParameter <em>Execution Parameter</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionStrategy()
 * @model
 * @generated
 */
public interface DeceptionStrategy extends AbstractFeature {
	/**
	 * Returns the value of the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Root</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Root</em>' attribute.
	 * @see #setIsRoot(boolean)
	 * @see dmt.DmtPackage#getDeceptionStrategy_IsRoot()
	 * @model
	 * @generated
	 */
	boolean isIsRoot();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStrategy#isIsRoot <em>Is Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Root</em>' attribute.
	 * @see #isIsRoot()
	 * @generated
	 */
	void setIsRoot(boolean value);

	/**
	 * Returns the value of the '<em><b>Execution Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Parameter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Parameter</em>' attribute.
	 * @see #setExecutionParameter(String)
	 * @see dmt.DmtPackage#getDeceptionStrategy_ExecutionParameter()
	 * @model
	 * @generated
	 */
	String getExecutionParameter();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStrategy#getExecutionParameter <em>Execution Parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Parameter</em>' attribute.
	 * @see #getExecutionParameter()
	 * @generated
	 */
	void setExecutionParameter(String value);

} // DeceptionStrategy
