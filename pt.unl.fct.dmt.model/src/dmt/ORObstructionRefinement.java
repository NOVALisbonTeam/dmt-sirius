/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OR Obstruction Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ORObstructionRefinement#getORObstructionRefine <em>OR Obstruction Refine</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getORObstructionRefinement()
 * @model
 * @generated
 */
public interface ORObstructionRefinement extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>OR Obstruction Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>OR Obstruction Refine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>OR Obstruction Refine</em>' reference.
	 * @see #setORObstructionRefine(ObstructionElement)
	 * @see dmt.DmtPackage#getORObstructionRefinement_ORObstructionRefine()
	 * @model required="true"
	 * @generated
	 */
	ObstructionElement getORObstructionRefine();

	/**
	 * Sets the value of the '{@link dmt.ORObstructionRefinement#getORObstructionRefine <em>OR Obstruction Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>OR Obstruction Refine</em>' reference.
	 * @see #getORObstructionRefine()
	 * @generated
	 */
	void setORObstructionRefine(ObstructionElement value);

} // ORObstructionRefinement
