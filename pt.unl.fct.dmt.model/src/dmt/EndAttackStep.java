/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>End Attack Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.EndAttackStep#getExpectedResult <em>Expected Result</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getEndAttackStep()
 * @model
 * @generated
 */
public interface EndAttackStep extends AttackStep {
	/**
	 * Returns the value of the '<em><b>Expected Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expected Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expected Result</em>' attribute.
	 * @see #setExpectedResult(String)
	 * @see dmt.DmtPackage#getEndAttackStep_ExpectedResult()
	 * @model
	 * @generated
	 */
	String getExpectedResult();

	/**
	 * Sets the value of the '{@link dmt.EndAttackStep#getExpectedResult <em>Expected Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expected Result</em>' attribute.
	 * @see #getExpectedResult()
	 * @generated
	 */
	void setExpectedResult(String value);

} // EndAttackStep
