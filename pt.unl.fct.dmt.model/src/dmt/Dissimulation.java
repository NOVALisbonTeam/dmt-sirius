/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dissimulation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Dissimulation#getType <em>Type</em>}</li>
 *   <li>{@link dmt.Dissimulation#getHideRealAsset <em>Hide Real Asset</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDissimulation()
 * @model
 * @generated
 */
public interface Dissimulation extends DeceptionTechnique {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"MASKING"</code>.
	 * The literals are from the enumeration {@link dmt.DissimulationTechniqueTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dmt.DissimulationTechniqueTypeEnum
	 * @see #setType(DissimulationTechniqueTypeEnum)
	 * @see dmt.DmtPackage#getDissimulation_Type()
	 * @model default="MASKING"
	 * @generated
	 */
	DissimulationTechniqueTypeEnum getType();

	/**
	 * Sets the value of the '{@link dmt.Dissimulation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dmt.DissimulationTechniqueTypeEnum
	 * @see #getType()
	 * @generated
	 */
	void setType(DissimulationTechniqueTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Hide Real Asset</b></em>' reference list.
	 * The list contents are of type {@link dmt.ConcreteAsset}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hide Real Asset</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hide Real Asset</em>' reference list.
	 * @see dmt.DmtPackage#getDissimulation_HideRealAsset()
	 * @model required="true"
	 * @generated
	 */
	EList<ConcreteAsset> getHideRealAsset();

} // Dissimulation
