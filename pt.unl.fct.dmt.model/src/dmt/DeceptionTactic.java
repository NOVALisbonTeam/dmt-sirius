/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Tactic</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionTactic#getSubTactic <em>Sub Tactic</em>}</li>
 *   <li>{@link dmt.DeceptionTactic#getApplyDissimulation <em>Apply Dissimulation</em>}</li>
 *   <li>{@link dmt.DeceptionTactic#getDescribedByDeceptionStory <em>Described By Deception Story</em>}</li>
 *   <li>{@link dmt.DeceptionTactic#getMeasuredBy <em>Measured By</em>}</li>
 *   <li>{@link dmt.DeceptionTactic#getMonitoredBy <em>Monitored By</em>}</li>
 *   <li>{@link dmt.DeceptionTactic#getLeadDeceptionRisk <em>Lead Deception Risk</em>}</li>
 *   <li>{@link dmt.DeceptionTactic#getApplySimulation <em>Apply Simulation</em>}</li>
 *   <li>{@link dmt.DeceptionTactic#isIsAbstract <em>Is Abstract</em>}</li>
 *   <li>{@link dmt.DeceptionTactic#getQualityattribute <em>Qualityattribute</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionTactic()
 * @model
 * @generated
 */
public interface DeceptionTactic extends DeceptionNode, Mitigator, Operationable {
	/**
	 * Returns the value of the '<em><b>Sub Tactic</b></em>' reference list.
	 * The list contents are of type {@link dmt.DeceptionTactic}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Tactic</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Tactic</em>' reference list.
	 * @see dmt.DmtPackage#getDeceptionTactic_SubTactic()
	 * @model
	 * @generated
	 */
	EList<DeceptionTactic> getSubTactic();

	/**
	 * Returns the value of the '<em><b>Apply Dissimulation</b></em>' reference list.
	 * The list contents are of type {@link dmt.Dissimulation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Apply Dissimulation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Apply Dissimulation</em>' reference list.
	 * @see dmt.DmtPackage#getDeceptionTactic_ApplyDissimulation()
	 * @model
	 * @generated
	 */
	EList<Dissimulation> getApplyDissimulation();

	/**
	 * Returns the value of the '<em><b>Described By Deception Story</b></em>' reference list.
	 * The list contents are of type {@link dmt.DeceptionStory}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Described By Deception Story</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Described By Deception Story</em>' reference list.
	 * @see dmt.DmtPackage#getDeceptionTactic_DescribedByDeceptionStory()
	 * @model
	 * @generated
	 */
	EList<DeceptionStory> getDescribedByDeceptionStory();

	/**
	 * Returns the value of the '<em><b>Measured By</b></em>' reference list.
	 * The list contents are of type {@link dmt.DeceptionMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measured By</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measured By</em>' reference list.
	 * @see dmt.DmtPackage#getDeceptionTactic_MeasuredBy()
	 * @model
	 * @generated
	 */
	EList<DeceptionMetric> getMeasuredBy();

	/**
	 * Returns the value of the '<em><b>Monitored By</b></em>' reference list.
	 * The list contents are of type {@link dmt.DeceptionChannel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Monitored By</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Monitored By</em>' reference list.
	 * @see dmt.DmtPackage#getDeceptionTactic_MonitoredBy()
	 * @model
	 * @generated
	 */
	EList<DeceptionChannel> getMonitoredBy();

	/**
	 * Returns the value of the '<em><b>Lead Deception Risk</b></em>' reference list.
	 * The list contents are of type {@link dmt.DeceptionRisk}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lead Deception Risk</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lead Deception Risk</em>' reference list.
	 * @see dmt.DmtPackage#getDeceptionTactic_LeadDeceptionRisk()
	 * @model
	 * @generated
	 */
	EList<DeceptionRisk> getLeadDeceptionRisk();

	/**
	 * Returns the value of the '<em><b>Apply Simulation</b></em>' reference list.
	 * The list contents are of type {@link dmt.Simulation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Apply Simulation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Apply Simulation</em>' reference list.
	 * @see dmt.DmtPackage#getDeceptionTactic_ApplySimulation()
	 * @model
	 * @generated
	 */
	EList<Simulation> getApplySimulation();

	/**
	 * Returns the value of the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Abstract</em>' attribute.
	 * @see #setIsAbstract(boolean)
	 * @see dmt.DmtPackage#getDeceptionTactic_IsAbstract()
	 * @model
	 * @generated
	 */
	boolean isIsAbstract();

	/**
	 * Sets the value of the '{@link dmt.DeceptionTactic#isIsAbstract <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Abstract</em>' attribute.
	 * @see #isIsAbstract()
	 * @generated
	 */
	void setIsAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Qualityattribute</b></em>' reference list.
	 * The list contents are of type {@link dmt.QualityAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Qualityattribute</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Qualityattribute</em>' reference list.
	 * @see dmt.DmtPackage#getDeceptionTactic_Qualityattribute()
	 * @model
	 * @generated
	 */
	EList<QualityAttribute> getQualityattribute();

} // DeceptionTactic
