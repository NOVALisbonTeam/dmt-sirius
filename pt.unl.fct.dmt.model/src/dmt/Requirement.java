/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getRequirement()
 * @model
 * @generated
 */
public interface Requirement extends LeafGoal {
} // Requirement
