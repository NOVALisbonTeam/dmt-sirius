/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Influence Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getInfluenceRelationship()
 * @model
 * @generated
 */
public interface InfluenceRelationship extends EObject {
} // InfluenceRelationship
