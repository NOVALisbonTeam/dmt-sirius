/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vulnerable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Vulnerable#getVulnerability <em>Vulnerability</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getVulnerable()
 * @model abstract="true"
 * @generated
 */
public interface Vulnerable extends EObject {
	/**
	 * Returns the value of the '<em><b>Vulnerability</b></em>' reference list.
	 * The list contents are of type {@link dmt.Vulnerability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vulnerability</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vulnerability</em>' reference list.
	 * @see dmt.DmtPackage#getVulnerable_Vulnerability()
	 * @model
	 * @generated
	 */
	EList<Vulnerability> getVulnerability();

} // Vulnerable
