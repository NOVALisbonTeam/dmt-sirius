/**
 */
package dmt;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Security Control Incident Enum</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage#getSecurityControlIncidentEnum()
 * @model
 * @generated
 */
public enum SecurityControlIncidentEnum implements Enumerator {
	/**
	 * The '<em><b>PREVENTIVE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PREVENTIVE_VALUE
	 * @generated
	 * @ordered
	 */
	PREVENTIVE(0, "PREVENTIVE", "PREVENTIVE"),

	/**
	 * The '<em><b>DETECTIVE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DETECTIVE_VALUE
	 * @generated
	 * @ordered
	 */
	DETECTIVE(1, "DETECTIVE", "DETECTIVE"),

	/**
	 * The '<em><b>CORRECTIVE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CORRECTIVE_VALUE
	 * @generated
	 * @ordered
	 */
	CORRECTIVE(2, "CORRECTIVE", "CORRECTIVE");

	/**
	 * The '<em><b>PREVENTIVE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PREVENTIVE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PREVENTIVE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PREVENTIVE_VALUE = 0;

	/**
	 * The '<em><b>DETECTIVE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DETECTIVE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DETECTIVE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DETECTIVE_VALUE = 1;

	/**
	 * The '<em><b>CORRECTIVE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CORRECTIVE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CORRECTIVE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CORRECTIVE_VALUE = 2;

	/**
	 * An array of all the '<em><b>Security Control Incident Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final SecurityControlIncidentEnum[] VALUES_ARRAY =
		new SecurityControlIncidentEnum[] {
			PREVENTIVE,
			DETECTIVE,
			CORRECTIVE,
		};

	/**
	 * A public read-only list of all the '<em><b>Security Control Incident Enum</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<SecurityControlIncidentEnum> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Security Control Incident Enum</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SecurityControlIncidentEnum get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SecurityControlIncidentEnum result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Security Control Incident Enum</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SecurityControlIncidentEnum getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			SecurityControlIncidentEnum result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Security Control Incident Enum</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static SecurityControlIncidentEnum get(int value) {
		switch (value) {
			case PREVENTIVE_VALUE: return PREVENTIVE;
			case DETECTIVE_VALUE: return DETECTIVE;
			case CORRECTIVE_VALUE: return CORRECTIVE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private SecurityControlIncidentEnum(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //SecurityControlIncidentEnum
