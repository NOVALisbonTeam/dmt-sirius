/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getDeceptionRequirement()
 * @model
 * @generated
 */
public interface DeceptionRequirement extends Requirement, DeceptionGoal {
} // DeceptionRequirement
