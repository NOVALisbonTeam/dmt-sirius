/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DomainProperty#getDomainCategory <em>Domain Category</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDomainProperty()
 * @model
 * @generated
 */
public interface DomainProperty extends ObstructedElement {
	/**
	 * Returns the value of the '<em><b>Domain Category</b></em>' attribute.
	 * The default value is <code>"DOMAINVAR"</code>.
	 * The literals are from the enumeration {@link dmt.DomainPropertyCategoryEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Category</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Category</em>' attribute.
	 * @see dmt.DomainPropertyCategoryEnum
	 * @see #setDomainCategory(DomainPropertyCategoryEnum)
	 * @see dmt.DmtPackage#getDomainProperty_DomainCategory()
	 * @model default="DOMAINVAR"
	 * @generated
	 */
	DomainPropertyCategoryEnum getDomainCategory();

	/**
	 * Sets the value of the '{@link dmt.DomainProperty#getDomainCategory <em>Domain Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain Category</em>' attribute.
	 * @see dmt.DomainPropertyCategoryEnum
	 * @see #getDomainCategory()
	 * @generated
	 */
	void setDomainCategory(DomainPropertyCategoryEnum value);

} // DomainProperty
