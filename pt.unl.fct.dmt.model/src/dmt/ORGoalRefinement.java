/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OR Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ORGoalRefinement#getOrGoalRefine <em>Or Goal Refine</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getORGoalRefinement()
 * @model annotation="http://www.obeo.fr/dsl/dnc/archetype archetype='Role'"
 * @generated
 */
public interface ORGoalRefinement extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>Or Goal Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Goal Refine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Goal Refine</em>' reference.
	 * @see #setOrGoalRefine(GoalRefinable)
	 * @see dmt.DmtPackage#getORGoalRefinement_OrGoalRefine()
	 * @model required="true"
	 * @generated
	 */
	GoalRefinable getOrGoalRefine();

	/**
	 * Sets the value of the '{@link dmt.ORGoalRefinement#getOrGoalRefine <em>Or Goal Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Goal Refine</em>' reference.
	 * @see #getOrGoalRefine()
	 * @generated
	 */
	void setOrGoalRefine(GoalRefinable value);

} // ORGoalRefinement
