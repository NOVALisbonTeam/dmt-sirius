/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attack</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * https://en.wikipedia.org/wiki/Cyberattack
 * 
 * Any attempt to expose, alter, disable, destroy, steal or gain unauthorized access to or make unauthorized use of an Asset.
 * An attack represents an instanciation (realization) of a threat.
 * 
 * A cyberattack can be employed by nation-states, individuals, groups, society or organizations. A cyberattack may originate from an anonymous source.
 * 
 * 
 * An attack can be active or passive.[6]
 * 
 * An "active attack" attempts to alter system resources or affect their operation.
 * A "passive attack" attempts to learn or make use of information from the system but does not affect system resources (e.g., wiretapping).
 * 
 * An attack can be perpetrated by an insider or from outside the organization;[6]
 * 
 * An "inside attack" is an attack initiated by an entity inside the security perimeter (an "insider"), i.e., an entity that is authorized to access system resources but uses them in a way not approved by those who granted the authorization.
 * An "outside attack" is initiated from outside the perimeter, by an unauthorized or illegitimate user of the system (an "outsider"). In the Internet, potential outside attackers range from amateur pranksters to organized criminals, international terrorists, and hostile governments.
 * 
 * Some attacks are physical: i.e. theft or damage of computers and other equipment. Others are attempts to force changes in the logic used by computers or network protocols in order to achieve unforeseen (by the original designer) result but useful for the attacker. Software used to for logical attacks on computers is called malware.
 * 
 * The following is a partial short list of attacks:
 * 
 * Passive (Network ,Wiretapping, Port scan, Idle scan)
 * Active (Denial-of-service attack, Spoofing, Network( Man in the middle, ARP poisoning, Ping flood, Ping of death, Smurf attack), 
 * Host (Buffer overflow, Heap overflow, Stack overflow, Format string attack)
 * 
 * Design Decision
 * ----------------
 * 
 * A decis�o de se conectar risco ao ataque est� no fato 
 * 
 * 
 * 
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Attack#getType <em>Type</em>}</li>
 *   <li>{@link dmt.Attack#getInstantiatesThreat <em>Instantiates Threat</em>}</li>
 *   <li>{@link dmt.Attack#getExploitsVulnerability <em>Exploits Vulnerability</em>}</li>
 *   <li>{@link dmt.Attack#getContributesToAttack <em>Contributes To Attack</em>}</li>
 *   <li>{@link dmt.Attack#getLeadsRisk <em>Leads Risk</em>}</li>
 *   <li>{@link dmt.Attack#getOrigin <em>Origin</em>}</li>
 *   <li>{@link dmt.Attack#getRealizedByAttackVector <em>Realized By Attack Vector</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAttack()
 * @model
 * @generated
 */
public interface Attack extends MitigableElement {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"ACTIVE"</code>.
	 * The literals are from the enumeration {@link dmt.AttackTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dmt.AttackTypeEnum
	 * @see #setType(AttackTypeEnum)
	 * @see dmt.DmtPackage#getAttack_Type()
	 * @model default="ACTIVE"
	 * @generated
	 */
	AttackTypeEnum getType();

	/**
	 * Sets the value of the '{@link dmt.Attack#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dmt.AttackTypeEnum
	 * @see #getType()
	 * @generated
	 */
	void setType(AttackTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Instantiates Threat</b></em>' reference list.
	 * The list contents are of type {@link dmt.Threat}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instantiates Threat</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instantiates Threat</em>' reference list.
	 * @see dmt.DmtPackage#getAttack_InstantiatesThreat()
	 * @model
	 * @generated
	 */
	EList<Threat> getInstantiatesThreat();

	/**
	 * Returns the value of the '<em><b>Exploits Vulnerability</b></em>' reference list.
	 * The list contents are of type {@link dmt.Vulnerability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exploits Vulnerability</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exploits Vulnerability</em>' reference list.
	 * @see dmt.DmtPackage#getAttack_ExploitsVulnerability()
	 * @model
	 * @generated
	 */
	EList<Vulnerability> getExploitsVulnerability();

	/**
	 * Returns the value of the '<em><b>Contributes To Attack</b></em>' reference list.
	 * The list contents are of type {@link dmt.Attack}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contributes To Attack</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contributes To Attack</em>' reference list.
	 * @see dmt.DmtPackage#getAttack_ContributesToAttack()
	 * @model
	 * @generated
	 */
	EList<Attack> getContributesToAttack();

	/**
	 * Returns the value of the '<em><b>Leads Risk</b></em>' reference list.
	 * The list contents are of type {@link dmt.RiskComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Leads Risk</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Leads Risk</em>' reference list.
	 * @see dmt.DmtPackage#getAttack_LeadsRisk()
	 * @model
	 * @generated
	 */
	EList<RiskComponent> getLeadsRisk();

	/**
	 * Returns the value of the '<em><b>Origin</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.AttackOriginEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Origin</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Origin</em>' attribute.
	 * @see dmt.AttackOriginEnum
	 * @see #setOrigin(AttackOriginEnum)
	 * @see dmt.DmtPackage#getAttack_Origin()
	 * @model
	 * @generated
	 */
	AttackOriginEnum getOrigin();

	/**
	 * Sets the value of the '{@link dmt.Attack#getOrigin <em>Origin</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Origin</em>' attribute.
	 * @see dmt.AttackOriginEnum
	 * @see #getOrigin()
	 * @generated
	 */
	void setOrigin(AttackOriginEnum value);

	/**
	 * Returns the value of the '<em><b>Realized By Attack Vector</b></em>' reference list.
	 * The list contents are of type {@link dmt.AttackVector}.
	 * It is bidirectional and its opposite is '{@link dmt.AttackVector#getRealizesAttack <em>Realizes Attack</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Realized By Attack Vector</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Realized By Attack Vector</em>' reference list.
	 * @see dmt.DmtPackage#getAttack_RealizedByAttackVector()
	 * @see dmt.AttackVector#getRealizesAttack
	 * @model opposite="realizesAttack"
	 * @generated
	 */
	EList<AttackVector> getRealizedByAttackVector();

} // Attack
