/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Tactic Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getDeceptionTacticEvent()
 * @model
 * @generated
 */
public interface DeceptionTacticEvent extends Event {
} // DeceptionTacticEvent
