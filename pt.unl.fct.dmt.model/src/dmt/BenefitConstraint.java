/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Benefit Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getBenefitConstraint()
 * @model
 * @generated
 */
public interface BenefitConstraint extends FeatureConstraint {
} // BenefitConstraint
