/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AND Obstruction Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ANDObstructionRefinement#getANDObstructionRefine <em>AND Obstruction Refine</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getANDObstructionRefinement()
 * @model
 * @generated
 */
public interface ANDObstructionRefinement extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>AND Obstruction Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AND Obstruction Refine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AND Obstruction Refine</em>' reference.
	 * @see #setANDObstructionRefine(ObstructionElement)
	 * @see dmt.DmtPackage#getANDObstructionRefinement_ANDObstructionRefine()
	 * @model required="true"
	 * @generated
	 */
	ObstructionElement getANDObstructionRefine();

	/**
	 * Sets the value of the '{@link dmt.ANDObstructionRefinement#getANDObstructionRefine <em>AND Obstruction Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AND Obstruction Refine</em>' reference.
	 * @see #getANDObstructionRefine()
	 * @generated
	 */
	void setANDObstructionRefine(ObstructionElement value);

} // ANDObstructionRefinement
