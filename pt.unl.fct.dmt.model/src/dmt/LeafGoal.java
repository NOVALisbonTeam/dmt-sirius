/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Leaf Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getLeafGoal()
 * @model abstract="true"
 * @generated
 */
public interface LeafGoal extends Goal {
} // LeafGoal
