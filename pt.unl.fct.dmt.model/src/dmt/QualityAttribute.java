/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quality Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getQualityAttribute()
 * @model
 * @generated
 */
public interface QualityAttribute extends InfluenceNode {
} // QualityAttribute
