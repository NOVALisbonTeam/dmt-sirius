/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Strategy Tactic Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionStrategyTacticRefinement#getStrategytacticrefinementsource <em>Strategytacticrefinementsource</em>}</li>
 *   <li>{@link dmt.DeceptionStrategyTacticRefinement#getStrategytacticrefinementtarget <em>Strategytacticrefinementtarget</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionStrategyTacticRefinement()
 * @model
 * @generated
 */
public interface DeceptionStrategyTacticRefinement extends NodeRefinement {
	/**
	 * Returns the value of the '<em><b>Strategytacticrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strategytacticrefinementsource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strategytacticrefinementsource</em>' reference.
	 * @see #setStrategytacticrefinementsource(DeceptionStrategy)
	 * @see dmt.DmtPackage#getDeceptionStrategyTacticRefinement_Strategytacticrefinementsource()
	 * @model required="true"
	 * @generated
	 */
	DeceptionStrategy getStrategytacticrefinementsource();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStrategyTacticRefinement#getStrategytacticrefinementsource <em>Strategytacticrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strategytacticrefinementsource</em>' reference.
	 * @see #getStrategytacticrefinementsource()
	 * @generated
	 */
	void setStrategytacticrefinementsource(DeceptionStrategy value);

	/**
	 * Returns the value of the '<em><b>Strategytacticrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Strategytacticrefinementtarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Strategytacticrefinementtarget</em>' reference.
	 * @see #setStrategytacticrefinementtarget(DeceptionTacticStrategy)
	 * @see dmt.DmtPackage#getDeceptionStrategyTacticRefinement_Strategytacticrefinementtarget()
	 * @model required="true"
	 * @generated
	 */
	DeceptionTacticStrategy getStrategytacticrefinementtarget();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStrategyTacticRefinement#getStrategytacticrefinementtarget <em>Strategytacticrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Strategytacticrefinementtarget</em>' reference.
	 * @see #getStrategytacticrefinementtarget()
	 * @generated
	 */
	void setStrategytacticrefinementtarget(DeceptionTacticStrategy value);

} // DeceptionStrategyTacticRefinement
