/**
 */
package dmt;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see dmt.DmtPackage
 * @generated
 */
public interface DmtFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DmtFactory eINSTANCE = dmt.impl.DmtFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>DMT Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DMT Model</em>'.
	 * @generated
	 */
	DMTModel createDMTModel();

	/**
	 * Returns a new object of class '<em>Operationalization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operationalization</em>'.
	 * @generated
	 */
	Operationalization createOperationalization();

	/**
	 * Returns a new object of class '<em>Software Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Software Agent</em>'.
	 * @generated
	 */
	SoftwareAgent createSoftwareAgent();

	/**
	 * Returns a new object of class '<em>Environment Agent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Environment Agent</em>'.
	 * @generated
	 */
	EnvironmentAgent createEnvironmentAgent();

	/**
	 * Returns a new object of class '<em>Behaviour Goal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Behaviour Goal</em>'.
	 * @generated
	 */
	BehaviourGoal createBehaviourGoal();

	/**
	 * Returns a new object of class '<em>Domain Description</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Domain Description</em>'.
	 * @generated
	 */
	DomainDescription createDomainDescription();

	/**
	 * Returns a new object of class '<em>Expectation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Expectation</em>'.
	 * @generated
	 */
	Expectation createExpectation();

	/**
	 * Returns a new object of class '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requirement</em>'.
	 * @generated
	 */
	Requirement createRequirement();

	/**
	 * Returns a new object of class '<em>Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operation</em>'.
	 * @generated
	 */
	Operation createOperation();

	/**
	 * Returns a new object of class '<em>OR Goal Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OR Goal Refinement</em>'.
	 * @generated
	 */
	ORGoalRefinement createORGoalRefinement();

	/**
	 * Returns a new object of class '<em>Attack</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attack</em>'.
	 * @generated
	 */
	Attack createAttack();

	/**
	 * Returns a new object of class '<em>Threat</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Threat</em>'.
	 * @generated
	 */
	Threat createThreat();

	/**
	 * Returns a new object of class '<em>Vulnerability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vulnerability</em>'.
	 * @generated
	 */
	Vulnerability createVulnerability();

	/**
	 * Returns a new object of class '<em>Security Control</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Security Control</em>'.
	 * @generated
	 */
	SecurityControl createSecurityControl();

	/**
	 * Returns a new object of class '<em>Risk Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Risk Component</em>'.
	 * @generated
	 */
	RiskComponent createRiskComponent();

	/**
	 * Returns a new object of class '<em>Attacker</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attacker</em>'.
	 * @generated
	 */
	Attacker createAttacker();

	/**
	 * Returns a new object of class '<em>Deception Tactic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Tactic</em>'.
	 * @generated
	 */
	DeceptionTactic createDeceptionTactic();

	/**
	 * Returns a new object of class '<em>IVulnerability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>IVulnerability</em>'.
	 * @generated
	 */
	IVulnerability createIVulnerability();

	/**
	 * Returns a new object of class '<em>Deception Story</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Story</em>'.
	 * @generated
	 */
	DeceptionStory createDeceptionStory();

	/**
	 * Returns a new object of class '<em>Deception Bias</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Bias</em>'.
	 * @generated
	 */
	DeceptionBias createDeceptionBias();

	/**
	 * Returns a new object of class '<em>Artificial Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Artificial Element</em>'.
	 * @generated
	 */
	ArtificialElement createArtificialElement();

	/**
	 * Returns a new object of class '<em>Interaction Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interaction Object</em>'.
	 * @generated
	 */
	InteractionObject createInteractionObject();

	/**
	 * Returns a new object of class '<em>Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Link</em>'.
	 * @generated
	 */
	Link createLink();

	/**
	 * Returns a new object of class '<em>Mitigation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Mitigation</em>'.
	 * @generated
	 */
	Mitigation createMitigation();

	/**
	 * Returns a new object of class '<em>Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simulation</em>'.
	 * @generated
	 */
	Simulation createSimulation();

	/**
	 * Returns a new object of class '<em>Dissimulation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dissimulation</em>'.
	 * @generated
	 */
	Dissimulation createDissimulation();

	/**
	 * Returns a new object of class '<em>Agent Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agent Assignment</em>'.
	 * @generated
	 */
	AgentAssignment createAgentAssignment();

	/**
	 * Returns a new object of class '<em>AND Goal Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AND Goal Refinement</em>'.
	 * @generated
	 */
	ANDGoalRefinement createANDGoalRefinement();

	/**
	 * Returns a new object of class '<em>Conflict Goal Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Conflict Goal Refinement</em>'.
	 * @generated
	 */
	ConflictGoalRefinement createConflictGoalRefinement();

	/**
	 * Returns a new object of class '<em>Agent Monitoring</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agent Monitoring</em>'.
	 * @generated
	 */
	AgentMonitoring createAgentMonitoring();

	/**
	 * Returns a new object of class '<em>Agent Control</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Agent Control</em>'.
	 * @generated
	 */
	AgentControl createAgentControl();

	/**
	 * Returns a new object of class '<em>Deception Metric</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Metric</em>'.
	 * @generated
	 */
	DeceptionMetric createDeceptionMetric();

	/**
	 * Returns a new object of class '<em>Deception Channel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Channel</em>'.
	 * @generated
	 */
	DeceptionChannel createDeceptionChannel();

	/**
	 * Returns a new object of class '<em>Attack Vector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attack Vector</em>'.
	 * @generated
	 */
	AttackVector createAttackVector();

	/**
	 * Returns a new object of class '<em>AProperty</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AProperty</em>'.
	 * @generated
	 */
	AProperty createAProperty();

	/**
	 * Returns a new object of class '<em>Attack Step Connector</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attack Step Connector</em>'.
	 * @generated
	 */
	AttackStepConnector createAttackStepConnector();

	/**
	 * Returns a new object of class '<em>Initial Attack Step</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Initial Attack Step</em>'.
	 * @generated
	 */
	InitialAttackStep createInitialAttackStep();

	/**
	 * Returns a new object of class '<em>End Attack Step</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>End Attack Step</em>'.
	 * @generated
	 */
	EndAttackStep createEndAttackStep();

	/**
	 * Returns a new object of class '<em>Attack Tree</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Attack Tree</em>'.
	 * @generated
	 */
	AttackTree createAttackTree();

	/**
	 * Returns a new object of class '<em>Soft Goal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Soft Goal</em>'.
	 * @generated
	 */
	SoftGoal createSoftGoal();

	/**
	 * Returns a new object of class '<em>Deception Behaviour Goal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Behaviour Goal</em>'.
	 * @generated
	 */
	DeceptionBehaviourGoal createDeceptionBehaviourGoal();

	/**
	 * Returns a new object of class '<em>Deception Soft Goal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Soft Goal</em>'.
	 * @generated
	 */
	DeceptionSoftGoal createDeceptionSoftGoal();

	/**
	 * Returns a new object of class '<em>Concrete Asset</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concrete Asset</em>'.
	 * @generated
	 */
	ConcreteAsset createConcreteAsset();

	/**
	 * Returns a new object of class '<em>Goal Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Goal Container</em>'.
	 * @generated
	 */
	GoalContainer createGoalContainer();

	/**
	 * Returns a new object of class '<em>Node Selection</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Node Selection</em>'.
	 * @generated
	 */
	NodeSelection createNodeSelection();

	/**
	 * Returns a new object of class '<em>Deception Tactic Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Tactic Strategy</em>'.
	 * @generated
	 */
	DeceptionTacticStrategy createDeceptionTacticStrategy();

	/**
	 * Returns a new object of class '<em>Deception Strategy Tactic Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Strategy Tactic Refinement</em>'.
	 * @generated
	 */
	DeceptionStrategyTacticRefinement createDeceptionStrategyTacticRefinement();

	/**
	 * Returns a new object of class '<em>Deception Tactic Strategy Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Tactic Strategy Refinement</em>'.
	 * @generated
	 */
	DeceptionTacticStrategyRefinement createDeceptionTacticStrategyRefinement();

	/**
	 * Returns a new object of class '<em>Deception Strategy Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Strategy Refinement</em>'.
	 * @generated
	 */
	DeceptionStrategyRefinement createDeceptionStrategyRefinement();

	/**
	 * Returns a new object of class '<em>Simulation Has Artificial Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simulation Has Artificial Element</em>'.
	 * @generated
	 */
	SimulationHasArtificialElement createSimulationHasArtificialElement();

	/**
	 * Returns a new object of class '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Property</em>'.
	 * @generated
	 */
	Property createProperty();

	/**
	 * Returns a new object of class '<em>Annotation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Annotation</em>'.
	 * @generated
	 */
	Annotation createAnnotation();

	/**
	 * Returns a new object of class '<em>Deception Risk</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Risk</em>'.
	 * @generated
	 */
	DeceptionRisk createDeceptionRisk();

	/**
	 * Returns a new object of class '<em>Deception Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Strategy</em>'.
	 * @generated
	 */
	DeceptionStrategy createDeceptionStrategy();

	/**
	 * Returns a new object of class '<em>Deception Tactic Mechanism</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Tactic Mechanism</em>'.
	 * @generated
	 */
	DeceptionTacticMechanism createDeceptionTacticMechanism();

	/**
	 * Returns a new object of class '<em>Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature</em>'.
	 * @generated
	 */
	Feature createFeature();

	/**
	 * Returns a new object of class '<em>Required Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Required Constraint</em>'.
	 * @generated
	 */
	RequiredConstraint createRequiredConstraint();

	/**
	 * Returns a new object of class '<em>Exclude Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exclude Constraint</em>'.
	 * @generated
	 */
	ExcludeConstraint createExcludeConstraint();

	/**
	 * Returns a new object of class '<em>Benefit Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Benefit Constraint</em>'.
	 * @generated
	 */
	BenefitConstraint createBenefitConstraint();

	/**
	 * Returns a new object of class '<em>Avoid Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Avoid Constraint</em>'.
	 * @generated
	 */
	AvoidConstraint createAvoidConstraint();

	/**
	 * Returns a new object of class '<em>Tactic Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tactic Refinement</em>'.
	 * @generated
	 */
	TacticRefinement createTacticRefinement();

	/**
	 * Returns a new object of class '<em>Feature Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Feature Refinement</em>'.
	 * @generated
	 */
	FeatureRefinement createFeatureRefinement();

	/**
	 * Returns a new object of class '<em>Strategy Tactic Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Strategy Tactic Refinement</em>'.
	 * @generated
	 */
	StrategyTacticRefinement createStrategyTacticRefinement();

	/**
	 * Returns a new object of class '<em>Tactic Feature Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tactic Feature Refinement</em>'.
	 * @generated
	 */
	TacticFeatureRefinement createTacticFeatureRefinement();

	/**
	 * Returns a new object of class '<em>OR Obstruction Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>OR Obstruction Refinement</em>'.
	 * @generated
	 */
	ORObstructionRefinement createORObstructionRefinement();

	/**
	 * Returns a new object of class '<em>AND Obstruction Refinement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AND Obstruction Refinement</em>'.
	 * @generated
	 */
	ANDObstructionRefinement createANDObstructionRefinement();

	/**
	 * Returns a new object of class '<em>Obstacle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Obstacle</em>'.
	 * @generated
	 */

	Obstacle createObstacle();

	/**
	 * Returns a new object of class '<em>Influence Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Influence Node</em>'.
	 * @generated
	 */
	InfluenceNode createInfluenceNode();

	/**
	 * Returns a new object of class '<em>Quality Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quality Attribute</em>'.
	 * @generated
	 */
	QualityAttribute createQualityAttribute();

	/**
	 * Returns a new object of class '<em>Influence Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Influence Relationship</em>'.
	 * @generated
	 */
	InfluenceRelationship createInfluenceRelationship();

	/**
	 * Returns a new object of class '<em>Influence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Influence</em>'.
	 * @generated
	 */
	Influence createInfluence();

	/**
	 * Returns a new object of class '<em>Preference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Preference</em>'.
	 * @generated
	 */
	Preference createPreference();

	/**
	 * Returns a new object of class '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event</em>'.
	 * @generated
	 */
	Event createEvent();

	/**
	 * Returns a new object of class '<em>Deception Tactic Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deception Tactic Event</em>'.
	 * @generated
	 */
	DeceptionTacticEvent createDeceptionTacticEvent();

	/**
	 * Returns a new object of class '<em>Environment Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Environment Event</em>'.
	 * @generated
	 */
	EnvironmentEvent createEnvironmentEvent();

	/**
	 * Returns a new object of class '<em>Belief</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Belief</em>'.
	 * @generated
	 */
	Belief createBelief();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DmtPackage getDmtPackage();

} //DmtFactory
