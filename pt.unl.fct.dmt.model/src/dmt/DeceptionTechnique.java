/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Technique</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionTechnique#getFormalSpec <em>Formal Spec</em>}</li>
 *   <li>{@link dmt.DeceptionTechnique#getInformalSpec <em>Informal Spec</em>}</li>
 *   <li>{@link dmt.DeceptionTechnique#getExpectedOutcome <em>Expected Outcome</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionTechnique()
 * @model abstract="true"
 * @generated
 */
public interface DeceptionTechnique extends DeceptionNode {

	/**
	 * Returns the value of the '<em><b>Formal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Spec</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Spec</em>' attribute.
	 * @see #setFormalSpec(String)
	 * @see dmt.DmtPackage#getDeceptionTechnique_FormalSpec()
	 * @model
	 * @generated
	 */
	String getFormalSpec();

	/**
	 * Sets the value of the '{@link dmt.DeceptionTechnique#getFormalSpec <em>Formal Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Spec</em>' attribute.
	 * @see #getFormalSpec()
	 * @generated
	 */
	void setFormalSpec(String value);

	/**
	 * Returns the value of the '<em><b>Informal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Informal Spec</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Informal Spec</em>' attribute.
	 * @see #setInformalSpec(String)
	 * @see dmt.DmtPackage#getDeceptionTechnique_InformalSpec()
	 * @model
	 * @generated
	 */
	String getInformalSpec();

	/**
	 * Sets the value of the '{@link dmt.DeceptionTechnique#getInformalSpec <em>Informal Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Informal Spec</em>' attribute.
	 * @see #getInformalSpec()
	 * @generated
	 */
	void setInformalSpec(String value);

	/**
	 * Returns the value of the '<em><b>Expected Outcome</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expected Outcome</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expected Outcome</em>' attribute.
	 * @see #setExpectedOutcome(String)
	 * @see dmt.DmtPackage#getDeceptionTechnique_ExpectedOutcome()
	 * @model
	 * @generated
	 */
	String getExpectedOutcome();

	/**
	 * Sets the value of the '{@link dmt.DeceptionTechnique#getExpectedOutcome <em>Expected Outcome</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expected Outcome</em>' attribute.
	 * @see #getExpectedOutcome()
	 * @generated
	 */
	void setExpectedOutcome(String value);
} // DeceptionTechnique
