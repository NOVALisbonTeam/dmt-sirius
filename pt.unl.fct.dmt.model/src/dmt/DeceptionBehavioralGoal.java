/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Behavioral Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getDeceptionBehavioralGoal()
 * @model
 * @generated
 */
public interface DeceptionBehavioralGoal extends BehaviourGoal, DeceptionGoal {
} // DeceptionBehavioralGoal
