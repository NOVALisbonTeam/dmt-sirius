/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getOperation()
 * @model
 * @generated
 */
public interface Operation extends Vulnerable, Operationable {

} // Operation
