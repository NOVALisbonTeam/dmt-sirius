/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Channel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionChannel#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionChannel()
 * @model
 * @generated
 */
public interface DeceptionChannel extends DeceptionNode {

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.ChannelTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dmt.ChannelTypeEnum
	 * @see #setType(ChannelTypeEnum)
	 * @see dmt.DmtPackage#getDeceptionChannel_Type()
	 * @model
	 * @generated
	 */
	ChannelTypeEnum getType();

	/**
	 * Sets the value of the '{@link dmt.DeceptionChannel#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dmt.ChannelTypeEnum
	 * @see #getType()
	 * @generated
	 */
	void setType(ChannelTypeEnum value);
} // DeceptionChannel
