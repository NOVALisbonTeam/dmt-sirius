/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getRequiredConstraint()
 * @model
 * @generated
 */
public interface RequiredConstraint extends FeatureConstraint {
} // RequiredConstraint
