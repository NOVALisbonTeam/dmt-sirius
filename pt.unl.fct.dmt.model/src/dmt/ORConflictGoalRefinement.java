/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OR Conflict Goal Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.ORConflictGoalRefinement#getOrConflictGoalRefine <em>Or Conflict Goal Refine</em>}</li>
 *   <li>{@link dmt.ORConflictGoalRefinement#getAltName <em>Alt Name</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getORConflictGoalRefinement()
 * @model
 * @generated
 */
public interface ORConflictGoalRefinement extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>Or Conflict Goal Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Or Conflict Goal Refine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Or Conflict Goal Refine</em>' reference.
	 * @see #setOrConflictGoalRefine(Goal)
	 * @see dmt.DmtPackage#getORConflictGoalRefinement_OrConflictGoalRefine()
	 * @model required="true"
	 * @generated
	 */
	Goal getOrConflictGoalRefine();

	/**
	 * Sets the value of the '{@link dmt.ORConflictGoalRefinement#getOrConflictGoalRefine <em>Or Conflict Goal Refine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Or Conflict Goal Refine</em>' reference.
	 * @see #getOrConflictGoalRefine()
	 * @generated
	 */
	void setOrConflictGoalRefine(Goal value);

	/**
	 * Returns the value of the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alt Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alt Name</em>' attribute.
	 * @see #setAltName(String)
	 * @see dmt.DmtPackage#getORConflictGoalRefinement_AltName()
	 * @model
	 * @generated
	 */
	String getAltName();

	/**
	 * Sets the value of the '{@link dmt.ORConflictGoalRefinement#getAltName <em>Alt Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alt Name</em>' attribute.
	 * @see #getAltName()
	 * @generated
	 */
	void setAltName(String value);

} // ORConflictGoalRefinement
