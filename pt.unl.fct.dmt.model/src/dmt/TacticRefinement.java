/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tactic Refinement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.TacticRefinement#getTacticrefinementtarget <em>Tacticrefinementtarget</em>}</li>
 *   <li>{@link dmt.TacticRefinement#getTacticrefinementsource <em>Tacticrefinementsource</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getTacticRefinement()
 * @model
 * @generated
 */
public interface TacticRefinement extends NodeRefinement {
	/**
	 * Returns the value of the '<em><b>Tacticrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tacticrefinementtarget</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tacticrefinementtarget</em>' reference.
	 * @see #setTacticrefinementtarget(DeceptionTacticMechanism)
	 * @see dmt.DmtPackage#getTacticRefinement_Tacticrefinementtarget()
	 * @model required="true"
	 * @generated
	 */
	DeceptionTacticMechanism getTacticrefinementtarget();

	/**
	 * Sets the value of the '{@link dmt.TacticRefinement#getTacticrefinementtarget <em>Tacticrefinementtarget</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tacticrefinementtarget</em>' reference.
	 * @see #getTacticrefinementtarget()
	 * @generated
	 */
	void setTacticrefinementtarget(DeceptionTacticMechanism value);

	/**
	 * Returns the value of the '<em><b>Tacticrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tacticrefinementsource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tacticrefinementsource</em>' reference.
	 * @see #setTacticrefinementsource(DeceptionTacticMechanism)
	 * @see dmt.DmtPackage#getTacticRefinement_Tacticrefinementsource()
	 * @model required="true"
	 * @generated
	 */
	DeceptionTacticMechanism getTacticrefinementsource();

	/**
	 * Sets the value of the '{@link dmt.TacticRefinement#getTacticrefinementsource <em>Tacticrefinementsource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tacticrefinementsource</em>' reference.
	 * @see #getTacticrefinementsource()
	 * @generated
	 */
	void setTacticrefinementsource(DeceptionTacticMechanism value);

} // TacticRefinement
