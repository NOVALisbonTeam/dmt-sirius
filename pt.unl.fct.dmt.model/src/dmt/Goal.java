/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Goal#getGoalCategory <em>Goal Category</em>}</li>
 *   <li>{@link dmt.Goal#getPriority <em>Priority</em>}</li>
 *   <li>{@link dmt.Goal#getSource <em>Source</em>}</li>
 *   <li>{@link dmt.Goal#getStability <em>Stability</em>}</li>
 *   <li>{@link dmt.Goal#getResolution <em>Resolution</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getGoal()
 * @model abstract="true"
 * @generated
 */
public interface Goal extends ObstructedElement, Vulnerable, GoalRefinable {
	/**
	 * Returns the value of the '<em><b>Resolution</b></em>' reference list.
	 * The list contents are of type {@link dmt.ObstructionElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resolution</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resolution</em>' reference list.
	 * @see dmt.DmtPackage#getGoal_Resolution()
	 * @model
	 * @generated
	 */
	EList<ObstructionElement> getResolution();

	/**
	 * Returns the value of the '<em><b>Goal Category</b></em>' attribute.
	 * The default value is <code>"UNDEFINED"</code>.
	 * The literals are from the enumeration {@link dmt.GoalCategoryEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Goal Category</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Goal Category</em>' attribute.
	 * @see dmt.GoalCategoryEnum
	 * @see #setGoalCategory(GoalCategoryEnum)
	 * @see dmt.DmtPackage#getGoal_GoalCategory()
	 * @model default="UNDEFINED"
	 * @generated
	 */
	GoalCategoryEnum getGoalCategory();

	/**
	 * Sets the value of the '{@link dmt.Goal#getGoalCategory <em>Goal Category</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Goal Category</em>' attribute.
	 * @see dmt.GoalCategoryEnum
	 * @see #getGoalCategory()
	 * @generated
	 */
	void setGoalCategory(GoalCategoryEnum value);

	/**
	 * Returns the value of the '<em><b>Priority</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.GoalPriorityEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority</em>' attribute.
	 * @see dmt.GoalPriorityEnum
	 * @see #setPriority(GoalPriorityEnum)
	 * @see dmt.DmtPackage#getGoal_Priority()
	 * @model
	 * @generated
	 */
	GoalPriorityEnum getPriority();

	/**
	 * Sets the value of the '{@link dmt.Goal#getPriority <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority</em>' attribute.
	 * @see dmt.GoalPriorityEnum
	 * @see #getPriority()
	 * @generated
	 */
	void setPriority(GoalPriorityEnum value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' attribute.
	 * @see #setSource(String)
	 * @see dmt.DmtPackage#getGoal_Source()
	 * @model
	 * @generated
	 */
	String getSource();

	/**
	 * Sets the value of the '{@link dmt.Goal#getSource <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' attribute.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(String value);

	/**
	 * Returns the value of the '<em><b>Stability</b></em>' attribute.
	 * The default value is <code>"LOW"</code>.
	 * The literals are from the enumeration {@link dmt.GoalStabilityEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stability</em>' attribute.
	 * @see dmt.GoalStabilityEnum
	 * @see #setStability(GoalStabilityEnum)
	 * @see dmt.DmtPackage#getGoal_Stability()
	 * @model default="LOW"
	 * @generated
	 */
	GoalStabilityEnum getStability();

	/**
	 * Sets the value of the '{@link dmt.Goal#getStability <em>Stability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stability</em>' attribute.
	 * @see dmt.GoalStabilityEnum
	 * @see #getStability()
	 * @generated
	 */
	void setStability(GoalStabilityEnum value);

} // Goal
