/**
 */
package dmt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Strategy Model Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getStrategyModelRelationship()
 * @model
 * @generated
 */
public interface StrategyModelRelationship extends EObject {
} // StrategyModelRelationship
