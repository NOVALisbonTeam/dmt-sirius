/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conflict</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getConflict()
 * @model
 * @generated
 */
public interface Conflict extends GoalModelRelation {
} // Conflict
