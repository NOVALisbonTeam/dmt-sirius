/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.EnvironmentAgent#getType <em>Type</em>}</li>
 *   <li>{@link dmt.EnvironmentAgent#getResponsibilityEnvironment <em>Responsibility Environment</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getEnvironmentAgent()
 * @model
 * @generated
 */
public interface EnvironmentAgent extends Agent {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.EnvironmentAgentTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dmt.EnvironmentAgentTypeEnum
	 * @see #setType(EnvironmentAgentTypeEnum)
	 * @see dmt.DmtPackage#getEnvironmentAgent_Type()
	 * @model
	 * @generated
	 */
	EnvironmentAgentTypeEnum getType();

	/**
	 * Sets the value of the '{@link dmt.EnvironmentAgent#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dmt.EnvironmentAgentTypeEnum
	 * @see #getType()
	 * @generated
	 */
	void setType(EnvironmentAgentTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Responsibility Environment</b></em>' reference list.
	 * The list contents are of type {@link dmt.Expectation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsibility Environment</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsibility Environment</em>' reference list.
	 * @see dmt.DmtPackage#getEnvironmentAgent_ResponsibilityEnvironment()
	 * @model required="true"
	 * @generated
	 */
	EList<Expectation> getResponsibilityEnvironment();

} // EnvironmentAgent
