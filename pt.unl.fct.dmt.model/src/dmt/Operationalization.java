/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operationalization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Operationalization#getReqPre <em>Req Pre</em>}</li>
 *   <li>{@link dmt.Operationalization#getReqTrig <em>Req Trig</em>}</li>
 *   <li>{@link dmt.Operationalization#getReqPost <em>Req Post</em>}</li>
 *   <li>{@link dmt.Operationalization#getOperationalizeGoal <em>Operationalize Goal</em>}</li>
 *   <li>{@link dmt.Operationalization#getOperationalization <em>Operationalization</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getOperationalization()
 * @model
 * @generated
 */
public interface Operationalization extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>Req Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Req Pre</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Req Pre</em>' attribute.
	 * @see #setReqPre(String)
	 * @see dmt.DmtPackage#getOperationalization_ReqPre()
	 * @model
	 * @generated
	 */
	String getReqPre();

	/**
	 * Sets the value of the '{@link dmt.Operationalization#getReqPre <em>Req Pre</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Req Pre</em>' attribute.
	 * @see #getReqPre()
	 * @generated
	 */
	void setReqPre(String value);

	/**
	 * Returns the value of the '<em><b>Req Trig</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Req Trig</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Req Trig</em>' attribute.
	 * @see #setReqTrig(String)
	 * @see dmt.DmtPackage#getOperationalization_ReqTrig()
	 * @model
	 * @generated
	 */
	String getReqTrig();

	/**
	 * Sets the value of the '{@link dmt.Operationalization#getReqTrig <em>Req Trig</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Req Trig</em>' attribute.
	 * @see #getReqTrig()
	 * @generated
	 */
	void setReqTrig(String value);

	/**
	 * Returns the value of the '<em><b>Req Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Req Post</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Req Post</em>' attribute.
	 * @see #setReqPost(String)
	 * @see dmt.DmtPackage#getOperationalization_ReqPost()
	 * @model
	 * @generated
	 */
	String getReqPost();

	/**
	 * Sets the value of the '{@link dmt.Operationalization#getReqPost <em>Req Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Req Post</em>' attribute.
	 * @see #getReqPost()
	 * @generated
	 */
	void setReqPost(String value);

	/**
	 * Returns the value of the '<em><b>Operationalize Goal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operationalize Goal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operationalize Goal</em>' reference.
	 * @see #setOperationalizeGoal(LeafGoal)
	 * @see dmt.DmtPackage#getOperationalization_OperationalizeGoal()
	 * @model required="true"
	 * @generated
	 */
	LeafGoal getOperationalizeGoal();

	/**
	 * Sets the value of the '{@link dmt.Operationalization#getOperationalizeGoal <em>Operationalize Goal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operationalize Goal</em>' reference.
	 * @see #getOperationalizeGoal()
	 * @generated
	 */
	void setOperationalizeGoal(LeafGoal value);

	/**
	 * Returns the value of the '<em><b>Operationalization</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operationalization</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operationalization</em>' reference.
	 * @see #setOperationalization(Operationable)
	 * @see dmt.DmtPackage#getOperationalization_Operationalization()
	 * @model required="true"
	 * @generated
	 */
	Operationable getOperationalization();

	/**
	 * Sets the value of the '{@link dmt.Operationalization#getOperationalization <em>Operationalization</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operationalization</em>' reference.
	 * @see #getOperationalization()
	 * @generated
	 */
	void setOperationalization(Operationable value);

} // Operationalization
