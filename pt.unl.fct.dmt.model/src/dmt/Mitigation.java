/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mitigation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Mitigation#getMitigationLevel <em>Mitigation Level</em>}</li>
 *   <li>{@link dmt.Mitigation#getMitigationTarget <em>Mitigation Target</em>}</li>
 *   <li>{@link dmt.Mitigation#getMitigationSource <em>Mitigation Source</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getMitigation()
 * @model
 * @generated
 */
public interface Mitigation extends SecurityRelationship {
	/**
	 * Returns the value of the '<em><b>Mitigation Level</b></em>' attribute.
	 * The default value is <code>"M"</code>.
	 * The literals are from the enumeration {@link dmt.MitigationLevelEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mitigation Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mitigation Level</em>' attribute.
	 * @see dmt.MitigationLevelEnum
	 * @see #setMitigationLevel(MitigationLevelEnum)
	 * @see dmt.DmtPackage#getMitigation_MitigationLevel()
	 * @model default="M"
	 * @generated
	 */
	MitigationLevelEnum getMitigationLevel();

	/**
	 * Sets the value of the '{@link dmt.Mitigation#getMitigationLevel <em>Mitigation Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mitigation Level</em>' attribute.
	 * @see dmt.MitigationLevelEnum
	 * @see #getMitigationLevel()
	 * @generated
	 */
	void setMitigationLevel(MitigationLevelEnum value);

	/**
	 * Returns the value of the '<em><b>Mitigation Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mitigation Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mitigation Target</em>' reference.
	 * @see #setMitigationTarget(MitigableElement)
	 * @see dmt.DmtPackage#getMitigation_MitigationTarget()
	 * @model required="true"
	 * @generated
	 */
	MitigableElement getMitigationTarget();

	/**
	 * Sets the value of the '{@link dmt.Mitigation#getMitigationTarget <em>Mitigation Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mitigation Target</em>' reference.
	 * @see #getMitigationTarget()
	 * @generated
	 */
	void setMitigationTarget(MitigableElement value);

	/**
	 * Returns the value of the '<em><b>Mitigation Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mitigation Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mitigation Source</em>' reference.
	 * @see #setMitigationSource(Mitigator)
	 * @see dmt.DmtPackage#getMitigation_MitigationSource()
	 * @model required="true"
	 * @generated
	 */
	Mitigator getMitigationSource();

	/**
	 * Sets the value of the '{@link dmt.Mitigation#getMitigationSource <em>Mitigation Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mitigation Source</em>' reference.
	 * @see #getMitigationSource()
	 * @generated
	 */
	void setMitigationSource(Mitigator value);

} // Mitigation
