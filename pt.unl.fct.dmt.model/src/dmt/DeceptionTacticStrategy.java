/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Tactic Strategy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionTacticStrategy#getDeceptiontacticmechanism <em>Deceptiontacticmechanism</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionTacticStrategy()
 * @model
 * @generated
 */
public interface DeceptionTacticStrategy extends AbstractFeature {
	/**
	 * Returns the value of the '<em><b>Deceptiontacticmechanism</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deceptiontacticmechanism</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deceptiontacticmechanism</em>' reference.
	 * @see #setDeceptiontacticmechanism(DeceptionTacticMechanism)
	 * @see dmt.DmtPackage#getDeceptionTacticStrategy_Deceptiontacticmechanism()
	 * @model required="true"
	 * @generated
	 */
	DeceptionTacticMechanism getDeceptiontacticmechanism();

	/**
	 * Sets the value of the '{@link dmt.DeceptionTacticStrategy#getDeceptiontacticmechanism <em>Deceptiontacticmechanism</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deceptiontacticmechanism</em>' reference.
	 * @see #getDeceptiontacticmechanism()
	 * @generated
	 */
	void setDeceptiontacticmechanism(DeceptionTacticMechanism value);

} // DeceptionTacticStrategy
