/**
 */
package dmt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Security Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getSecurityRelationship()
 * @model abstract="true"
 * @generated
 */
public interface SecurityRelationship extends EObject {
} // SecurityRelationship
