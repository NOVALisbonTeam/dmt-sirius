/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initial Attack Step</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.InitialAttackStep#getHasAttackSteps <em>Has Attack Steps</em>}</li>
 *   <li>{@link dmt.InitialAttackStep#getPreCondition <em>Pre Condition</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getInitialAttackStep()
 * @model
 * @generated
 */
public interface InitialAttackStep extends AttackStep {
	/**
	 * Returns the value of the '<em><b>Has Attack Steps</b></em>' containment reference list.
	 * The list contents are of type {@link dmt.AttackStepConnector}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Attack Steps</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Attack Steps</em>' containment reference list.
	 * @see dmt.DmtPackage#getInitialAttackStep_HasAttackSteps()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttackStepConnector> getHasAttackSteps();

	/**
	 * Returns the value of the '<em><b>Pre Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pre Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Condition</em>' attribute.
	 * @see #setPreCondition(String)
	 * @see dmt.DmtPackage#getInitialAttackStep_PreCondition()
	 * @model
	 * @generated
	 */
	String getPreCondition();

	/**
	 * Sets the value of the '{@link dmt.InitialAttackStep#getPreCondition <em>Pre Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Condition</em>' attribute.
	 * @see #getPreCondition()
	 * @generated
	 */
	void setPreCondition(String value);

} // InitialAttackStep
