/**
 */
package dmt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionRelationship#getAltName <em>Alt Name</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionRelationship()
 * @model abstract="true"
 * @generated
 */
public interface DeceptionRelationship extends EObject {

	/**
	 * Returns the value of the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alt Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alt Name</em>' attribute.
	 * @see #setAltName(String)
	 * @see dmt.DmtPackage#getDeceptionRelationship_AltName()
	 * @model
	 * @generated
	 */
	String getAltName();

	/**
	 * Sets the value of the '{@link dmt.DeceptionRelationship#getAltName <em>Alt Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alt Name</em>' attribute.
	 * @see #getAltName()
	 * @generated
	 */
	void setAltName(String value);
} // DeceptionRelationship
