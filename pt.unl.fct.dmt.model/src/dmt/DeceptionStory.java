/**
 */
package dmt;

import interactions.Interaction;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Story</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionStory#getExternalIdentifier <em>External Identifier</em>}</li>
 *   <li>{@link dmt.DeceptionStory#getExploitBias <em>Exploit Bias</em>}</li>
 *   <li>{@link dmt.DeceptionStory#getStoryType <em>Story Type</em>}</li>
 *   <li>{@link dmt.DeceptionStory#getPreCondition <em>Pre Condition</em>}</li>
 *   <li>{@link dmt.DeceptionStory#getPosCondition <em>Pos Condition</em>}</li>
 *   <li>{@link dmt.DeceptionStory#getInteraction <em>Interaction</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionStory()
 * @model
 * @generated
 */
public interface DeceptionStory extends DeceptionNode {
	/**
	 * Returns the value of the '<em><b>External Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Identifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Identifier</em>' attribute.
	 * @see #setExternalIdentifier(String)
	 * @see dmt.DmtPackage#getDeceptionStory_ExternalIdentifier()
	 * @model
	 * @generated
	 */
	String getExternalIdentifier();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStory#getExternalIdentifier <em>External Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External Identifier</em>' attribute.
	 * @see #getExternalIdentifier()
	 * @generated
	 */
	void setExternalIdentifier(String value);

	/**
	 * Returns the value of the '<em><b>Exploit Bias</b></em>' reference list.
	 * The list contents are of type {@link dmt.DeceptionBias}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exploit Bias</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exploit Bias</em>' reference list.
	 * @see dmt.DmtPackage#getDeceptionStory_ExploitBias()
	 * @model
	 * @generated
	 */
	EList<DeceptionBias> getExploitBias();

	/**
	 * Returns the value of the '<em><b>Story Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.StoryTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Story Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Story Type</em>' attribute.
	 * @see dmt.StoryTypeEnum
	 * @see #setStoryType(StoryTypeEnum)
	 * @see dmt.DmtPackage#getDeceptionStory_StoryType()
	 * @model
	 * @generated
	 */
	StoryTypeEnum getStoryType();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStory#getStoryType <em>Story Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Story Type</em>' attribute.
	 * @see dmt.StoryTypeEnum
	 * @see #getStoryType()
	 * @generated
	 */
	void setStoryType(StoryTypeEnum value);

	/**
	 * Returns the value of the '<em><b>Pre Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pre Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pre Condition</em>' attribute.
	 * @see #setPreCondition(String)
	 * @see dmt.DmtPackage#getDeceptionStory_PreCondition()
	 * @model
	 * @generated
	 */
	String getPreCondition();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStory#getPreCondition <em>Pre Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pre Condition</em>' attribute.
	 * @see #getPreCondition()
	 * @generated
	 */
	void setPreCondition(String value);

	/**
	 * Returns the value of the '<em><b>Pos Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pos Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pos Condition</em>' attribute.
	 * @see #setPosCondition(String)
	 * @see dmt.DmtPackage#getDeceptionStory_PosCondition()
	 * @model
	 * @generated
	 */
	String getPosCondition();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStory#getPosCondition <em>Pos Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pos Condition</em>' attribute.
	 * @see #getPosCondition()
	 * @generated
	 */
	void setPosCondition(String value);

	/**
	 * Returns the value of the '<em><b>Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interaction</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interaction</em>' reference.
	 * @see #setInteraction(Interaction)
	 * @see dmt.DmtPackage#getDeceptionStory_Interaction()
	 * @model
	 * @generated
	 */
	Interaction getInteraction();

	/**
	 * Sets the value of the '{@link dmt.DeceptionStory#getInteraction <em>Interaction</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interaction</em>' reference.
	 * @see #getInteraction()
	 * @generated
	 */
	void setInteraction(Interaction value);

} // DeceptionStory
