/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Behaviour Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionBehaviourGoal#getFormalSpec <em>Formal Spec</em>}</li>
 *   <li>{@link dmt.DeceptionBehaviourGoal#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionBehaviourGoal()
 * @model
 * @generated
 */
public interface DeceptionBehaviourGoal extends DeceptionGoal {
	/**
	 * Returns the value of the '<em><b>Formal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formal Spec</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formal Spec</em>' attribute.
	 * @see #setFormalSpec(String)
	 * @see dmt.DmtPackage#getDeceptionBehaviourGoal_FormalSpec()
	 * @model
	 * @generated
	 */
	String getFormalSpec();

	/**
	 * Sets the value of the '{@link dmt.DeceptionBehaviourGoal#getFormalSpec <em>Formal Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formal Spec</em>' attribute.
	 * @see #getFormalSpec()
	 * @generated
	 */
	void setFormalSpec(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.GoalTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see dmt.GoalTypeEnum
	 * @see #setType(GoalTypeEnum)
	 * @see dmt.DmtPackage#getDeceptionBehaviourGoal_Type()
	 * @model
	 * @generated
	 */
	GoalTypeEnum getType();

	/**
	 * Sets the value of the '{@link dmt.DeceptionBehaviourGoal#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see dmt.GoalTypeEnum
	 * @see #getType()
	 * @generated
	 */
	void setType(GoalTypeEnum value);

} // DeceptionBehaviourGoal
