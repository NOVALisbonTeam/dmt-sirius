/**
 */
package dmt;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see dmt.DmtFactory
 * @model kind="package"
 * @generated
 */
public interface DmtPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "dmt";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "https://www.di.fct.unl.pt/dmt-metamodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "dmt";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DmtPackage eINSTANCE = dmt.impl.DmtPackageImpl.init();

	/**
	 * The meta object id for the '{@link dmt.impl.DMTModelImpl <em>DMT Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DMTModelImpl
	 * @see dmt.impl.DmtPackageImpl#getDMTModel()
	 * @generated
	 */
	int DMT_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Has Security Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_SECURITY_NODES = 0;

	/**
	 * The feature id for the '<em><b>Has Deception Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_DECEPTION_NODES = 1;

	/**
	 * The feature id for the '<em><b>Has Goal Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_GOAL_NODES = 2;

	/**
	 * The feature id for the '<em><b>Has Goal Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_GOAL_RELATIONS = 3;

	/**
	 * The feature id for the '<em><b>Has Strategy Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_STRATEGY_NODES = 4;

	/**
	 * The feature id for the '<em><b>Has Strategy Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_STRATEGY_RELATIONS = 5;

	/**
	 * The feature id for the '<em><b>Has Deception Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_DECEPTION_RELATIONS = 6;

	/**
	 * The feature id for the '<em><b>Has Security Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_SECURITY_RELATIONS = 7;

	/**
	 * The feature id for the '<em><b>Has Influence Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_INFLUENCE_NODES = 8;

	/**
	 * The feature id for the '<em><b>Has Influence Relations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_INFLUENCE_RELATIONS = 9;

	/**
	 * The feature id for the '<em><b>Has Goal Containers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_GOAL_CONTAINERS = 10;

	/**
	 * The feature id for the '<em><b>Has Misc Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL__HAS_MISC_NODES = 11;

	/**
	 * The number of structural features of the '<em>DMT Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL_FEATURE_COUNT = 12;

	/**
	 * The number of operations of the '<em>DMT Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DMT_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dmt.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.NodeImpl
	 * @see dmt.impl.DmtPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 36;

	/**
	 * The meta object id for the '{@link dmt.impl.GoalNodeImpl <em>Goal Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.GoalNodeImpl
	 * @see dmt.impl.DmtPackageImpl#getGoalNode()
	 * @generated
	 */
	int GOAL_NODE = 52;

	/**
	 * The meta object id for the '{@link dmt.impl.GoalImpl <em>Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.GoalImpl
	 * @see dmt.impl.DmtPackageImpl#getGoal()
	 * @generated
	 */
	int GOAL = 1;

	/**
	 * The meta object id for the '{@link dmt.impl.GoalRelationshipImpl <em>Goal Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.GoalRelationshipImpl
	 * @see dmt.impl.DmtPackageImpl#getGoalRelationship()
	 * @generated
	 */
	int GOAL_RELATIONSHIP = 2;

	/**
	 * The meta object id for the '{@link dmt.impl.AgentImpl <em>Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AgentImpl
	 * @see dmt.impl.DmtPackageImpl#getAgent()
	 * @generated
	 */
	int AGENT = 3;

	/**
	 * The meta object id for the '{@link dmt.impl.OperationalizationImpl <em>Operationalization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.OperationalizationImpl
	 * @see dmt.impl.DmtPackageImpl#getOperationalization()
	 * @generated
	 */
	int OPERATIONALIZATION = 4;

	/**
	 * The meta object id for the '{@link dmt.impl.SoftwareAgentImpl <em>Software Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.SoftwareAgentImpl
	 * @see dmt.impl.DmtPackageImpl#getSoftwareAgent()
	 * @generated
	 */
	int SOFTWARE_AGENT = 5;

	/**
	 * The meta object id for the '{@link dmt.impl.EnvironmentAgentImpl <em>Environment Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.EnvironmentAgentImpl
	 * @see dmt.impl.DmtPackageImpl#getEnvironmentAgent()
	 * @generated
	 */
	int ENVIRONMENT_AGENT = 6;

	/**
	 * The meta object id for the '{@link dmt.impl.BehaviourGoalImpl <em>Behaviour Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.BehaviourGoalImpl
	 * @see dmt.impl.DmtPackageImpl#getBehaviourGoal()
	 * @generated
	 */
	int BEHAVIOUR_GOAL = 7;

	/**
	 * The meta object id for the '{@link dmt.impl.DomainDescriptionImpl <em>Domain Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DomainDescriptionImpl
	 * @see dmt.impl.DmtPackageImpl#getDomainDescription()
	 * @generated
	 */
	int DOMAIN_DESCRIPTION = 8;

	/**
	 * The meta object id for the '{@link dmt.impl.ObstructedElementImpl <em>Obstructed Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ObstructedElementImpl
	 * @see dmt.impl.DmtPackageImpl#getObstructedElement()
	 * @generated
	 */
	int OBSTRUCTED_ELEMENT = 87;

	/**
	 * The number of structural features of the '<em>Obstructed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTED_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Obstructed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__VULNERABILITY = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__NAME = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__DESCRIPTION = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__UUID = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__HAS_ANNOTATIONS = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__OR_GOAL_REFINED = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__AND_GOAL_REFINED = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Goal Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__GOAL_CATEGORY = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__PRIORITY = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__SOURCE = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Stability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__STABILITY = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Resolution</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__RESOLUTION = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_FEATURE_COUNT = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The number of operations of the '<em>Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_OPERATION_COUNT = OBSTRUCTED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RELATIONSHIP__STATUS = 0;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RELATIONSHIP__TACTIC = 1;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RELATIONSHIP__SYS_REF = 2;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RELATIONSHIP__ALT_NAME = 3;

	/**
	 * The number of structural features of the '<em>Goal Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RELATIONSHIP_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Goal Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RELATIONSHIP_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__DESCRIPTION = 1;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__UUID = 2;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE__HAS_ANNOTATIONS = 3;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_NODE__NAME = NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_NODE__DESCRIPTION = NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_NODE__UUID = NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_NODE__HAS_ANNOTATIONS = NODE__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Goal Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Goal Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__NAME = GOAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__DESCRIPTION = GOAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__UUID = GOAL_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__HAS_ANNOTATIONS = GOAL_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Def</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__DEF = GOAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Load</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__LOAD = GOAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Subagent</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__SUBAGENT = GOAL_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Performs Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__PERFORMS_OPERATION = GOAL_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_FEATURE_COUNT = GOAL_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_OPERATION_COUNT = GOAL_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION__STATUS = GOAL_RELATIONSHIP__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION__TACTIC = GOAL_RELATIONSHIP__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION__SYS_REF = GOAL_RELATIONSHIP__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION__ALT_NAME = GOAL_RELATIONSHIP__ALT_NAME;

	/**
	 * The feature id for the '<em><b>Req Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION__REQ_PRE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Req Trig</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION__REQ_TRIG = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Req Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION__REQ_POST = GOAL_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Operationalize Goal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION__OPERATIONALIZE_GOAL = GOAL_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Operationalization</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION__OPERATIONALIZATION = GOAL_RELATIONSHIP_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Operationalization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Operationalization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONALIZATION_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__NAME = AGENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__DESCRIPTION = AGENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__UUID = AGENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__HAS_ANNOTATIONS = AGENT__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Def</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__DEF = AGENT__DEF;

	/**
	 * The feature id for the '<em><b>Load</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__LOAD = AGENT__LOAD;

	/**
	 * The feature id for the '<em><b>Subagent</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__SUBAGENT = AGENT__SUBAGENT;

	/**
	 * The feature id for the '<em><b>Performs Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__PERFORMS_OPERATION = AGENT__PERFORMS_OPERATION;

	/**
	 * The feature id for the '<em><b>Responsibility Software</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT__RESPONSIBILITY_SOFTWARE = AGENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Software Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT_FEATURE_COUNT = AGENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Software Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_AGENT_OPERATION_COUNT = AGENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__NAME = AGENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__DESCRIPTION = AGENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__UUID = AGENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__HAS_ANNOTATIONS = AGENT__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Def</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__DEF = AGENT__DEF;

	/**
	 * The feature id for the '<em><b>Load</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__LOAD = AGENT__LOAD;

	/**
	 * The feature id for the '<em><b>Subagent</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__SUBAGENT = AGENT__SUBAGENT;

	/**
	 * The feature id for the '<em><b>Performs Operation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__PERFORMS_OPERATION = AGENT__PERFORMS_OPERATION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__TYPE = AGENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Responsibility Environment</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT__RESPONSIBILITY_ENVIRONMENT = AGENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Environment Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT_FEATURE_COUNT = AGENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Environment Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_AGENT_OPERATION_COUNT = AGENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__VULNERABILITY = GOAL__VULNERABILITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__NAME = GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__DESCRIPTION = GOAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__UUID = GOAL__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__HAS_ANNOTATIONS = GOAL__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__OR_GOAL_REFINED = GOAL__OR_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__AND_GOAL_REFINED = GOAL__AND_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>Goal Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__GOAL_CATEGORY = GOAL__GOAL_CATEGORY;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__PRIORITY = GOAL__PRIORITY;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__SOURCE = GOAL__SOURCE;

	/**
	 * The feature id for the '<em><b>Stability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__STABILITY = GOAL__STABILITY;

	/**
	 * The feature id for the '<em><b>Resolution</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__RESOLUTION = GOAL__RESOLUTION;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__TYPE = GOAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Formal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL__FORMAL_SPEC = GOAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Behaviour Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL_FEATURE_COUNT = GOAL_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Behaviour Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_GOAL_OPERATION_COUNT = GOAL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION__VULNERABILITY = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION__NAME = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION__DESCRIPTION = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION__UUID = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION__HAS_ANNOTATIONS = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION__OR_GOAL_REFINED = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION__AND_GOAL_REFINED = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Domain Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION__DOMAIN_CATEGORY = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Domain Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION_FEATURE_COUNT = OBSTRUCTED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Domain Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOMAIN_DESCRIPTION_OPERATION_COUNT = OBSTRUCTED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.LeafGoalImpl <em>Leaf Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.LeafGoalImpl
	 * @see dmt.impl.DmtPackageImpl#getLeafGoal()
	 * @generated
	 */
	int LEAF_GOAL = 9;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__VULNERABILITY = GOAL__VULNERABILITY;

	/**
	 * The meta object id for the '{@link dmt.impl.ExpectationImpl <em>Expectation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ExpectationImpl
	 * @see dmt.impl.DmtPackageImpl#getExpectation()
	 * @generated
	 */
	int EXPECTATION = 10;

	/**
	 * The meta object id for the '{@link dmt.impl.RequirementImpl <em>Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.RequirementImpl
	 * @see dmt.impl.DmtPackageImpl#getRequirement()
	 * @generated
	 */
	int REQUIREMENT = 11;

	/**
	 * The meta object id for the '{@link dmt.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.OperationImpl
	 * @see dmt.impl.DmtPackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 12;

	/**
	 * The meta object id for the '{@link dmt.impl.ORGoalRefinementImpl <em>OR Goal Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ORGoalRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getORGoalRefinement()
	 * @generated
	 */
	int OR_GOAL_REFINEMENT = 13;

	/**
	 * The meta object id for the '{@link dmt.impl.SecurityNodeImpl <em>Security Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.SecurityNodeImpl
	 * @see dmt.impl.DmtPackageImpl#getSecurityNode()
	 * @generated
	 */
	int SECURITY_NODE = 14;

	/**
	 * The meta object id for the '{@link dmt.impl.MitigableElementImpl <em>Mitigable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.MitigableElementImpl
	 * @see dmt.impl.DmtPackageImpl#getMitigableElement()
	 * @generated
	 */
	int MITIGABLE_ELEMENT = 15;

	/**
	 * The meta object id for the '{@link dmt.impl.AttackImpl <em>Attack</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AttackImpl
	 * @see dmt.impl.DmtPackageImpl#getAttack()
	 * @generated
	 */
	int ATTACK = 16;

	/**
	 * The meta object id for the '{@link dmt.impl.ThreatImpl <em>Threat</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ThreatImpl
	 * @see dmt.impl.DmtPackageImpl#getThreat()
	 * @generated
	 */
	int THREAT = 17;

	/**
	 * The meta object id for the '{@link dmt.impl.VulnerabilityImpl <em>Vulnerability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.VulnerabilityImpl
	 * @see dmt.impl.DmtPackageImpl#getVulnerability()
	 * @generated
	 */
	int VULNERABILITY = 18;

	/**
	 * The meta object id for the '{@link dmt.impl.SecurityControlImpl <em>Security Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.SecurityControlImpl
	 * @see dmt.impl.DmtPackageImpl#getSecurityControl()
	 * @generated
	 */
	int SECURITY_CONTROL = 19;

	/**
	 * The meta object id for the '{@link dmt.impl.RiskComponentImpl <em>Risk Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.RiskComponentImpl
	 * @see dmt.impl.DmtPackageImpl#getRiskComponent()
	 * @generated
	 */
	int RISK_COMPONENT = 20;

	/**
	 * The meta object id for the '{@link dmt.impl.AttackerImpl <em>Attacker</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AttackerImpl
	 * @see dmt.impl.DmtPackageImpl#getAttacker()
	 * @generated
	 */
	int ATTACKER = 21;

	/**
	 * The meta object id for the '{@link dmt.impl.AssetImpl <em>Asset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AssetImpl
	 * @see dmt.impl.DmtPackageImpl#getAsset()
	 * @generated
	 */
	int ASSET = 22;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionNodeImpl <em>Deception Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionNodeImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionNode()
	 * @generated
	 */
	int DECEPTION_NODE = 23;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionTacticImpl <em>Deception Tactic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionTacticImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionTactic()
	 * @generated
	 */
	int DECEPTION_TACTIC = 24;

	/**
	 * The meta object id for the '{@link dmt.impl.IVulnerabilityImpl <em>IVulnerability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.IVulnerabilityImpl
	 * @see dmt.impl.DmtPackageImpl#getIVulnerability()
	 * @generated
	 */
	int IVULNERABILITY = 25;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionTechniqueImpl <em>Deception Technique</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionTechniqueImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionTechnique()
	 * @generated
	 */
	int DECEPTION_TECHNIQUE = 26;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionStoryImpl <em>Deception Story</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionStoryImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionStory()
	 * @generated
	 */
	int DECEPTION_STORY = 27;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionBiasImpl <em>Deception Bias</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionBiasImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionBias()
	 * @generated
	 */
	int DECEPTION_BIAS = 28;

	/**
	 * The meta object id for the '{@link dmt.impl.ArtificialElementImpl <em>Artificial Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ArtificialElementImpl
	 * @see dmt.impl.DmtPackageImpl#getArtificialElement()
	 * @generated
	 */
	int ARTIFICIAL_ELEMENT = 29;

	/**
	 * The meta object id for the '{@link dmt.impl.InteractionNodesImpl <em>Interaction Nodes</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.InteractionNodesImpl
	 * @see dmt.impl.DmtPackageImpl#getInteractionNodes()
	 * @generated
	 */
	int INTERACTION_NODES = 31;

	/**
	 * The meta object id for the '{@link dmt.impl.InteractionObjectImpl <em>Interaction Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.InteractionObjectImpl
	 * @see dmt.impl.DmtPackageImpl#getInteractionObject()
	 * @generated
	 */
	int INTERACTION_OBJECT = 32;

	/**
	 * The meta object id for the '{@link dmt.impl.LinkImpl <em>Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.LinkImpl
	 * @see dmt.impl.DmtPackageImpl#getLink()
	 * @generated
	 */
	int LINK = 33;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionRelationshipImpl <em>Deception Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionRelationshipImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionRelationship()
	 * @generated
	 */
	int DECEPTION_RELATIONSHIP = 72;

	/**
	 * The meta object id for the '{@link dmt.impl.MitigationImpl <em>Mitigation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.MitigationImpl
	 * @see dmt.impl.DmtPackageImpl#getMitigation()
	 * @generated
	 */
	int MITIGATION = 30;

	/**
	 * The meta object id for the '{@link dmt.impl.SimulationImpl <em>Simulation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.SimulationImpl
	 * @see dmt.impl.DmtPackageImpl#getSimulation()
	 * @generated
	 */
	int SIMULATION = 34;

	/**
	 * The meta object id for the '{@link dmt.impl.DissimulationImpl <em>Dissimulation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DissimulationImpl
	 * @see dmt.impl.DmtPackageImpl#getDissimulation()
	 * @generated
	 */
	int DISSIMULATION = 35;

	/**
	 * The meta object id for the '{@link dmt.impl.AgentRelationImpl <em>Agent Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AgentRelationImpl
	 * @see dmt.impl.DmtPackageImpl#getAgentRelation()
	 * @generated
	 */
	int AGENT_RELATION = 42;

	/**
	 * The meta object id for the '{@link dmt.impl.AgentAssignmentImpl <em>Agent Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AgentAssignmentImpl
	 * @see dmt.impl.DmtPackageImpl#getAgentAssignment()
	 * @generated
	 */
	int AGENT_ASSIGNMENT = 37;

	/**
	 * The meta object id for the '{@link dmt.impl.ANDGoalRefinementImpl <em>AND Goal Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ANDGoalRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getANDGoalRefinement()
	 * @generated
	 */
	int AND_GOAL_REFINEMENT = 38;

	/**
	 * The meta object id for the '{@link dmt.impl.ConflictGoalRefinementImpl <em>Conflict Goal Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ConflictGoalRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getConflictGoalRefinement()
	 * @generated
	 */
	int CONFLICT_GOAL_REFINEMENT = 39;

	/**
	 * The meta object id for the '{@link dmt.impl.AgentMonitoringImpl <em>Agent Monitoring</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AgentMonitoringImpl
	 * @see dmt.impl.DmtPackageImpl#getAgentMonitoring()
	 * @generated
	 */
	int AGENT_MONITORING = 40;

	/**
	 * The meta object id for the '{@link dmt.impl.AgentControlImpl <em>Agent Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AgentControlImpl
	 * @see dmt.impl.DmtPackageImpl#getAgentControl()
	 * @generated
	 */
	int AGENT_CONTROL = 41;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionMetricImpl <em>Deception Metric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionMetricImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionMetric()
	 * @generated
	 */
	int DECEPTION_METRIC = 43;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionChannelImpl <em>Deception Channel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionChannelImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionChannel()
	 * @generated
	 */
	int DECEPTION_CHANNEL = 44;

	/**
	 * The meta object id for the '{@link dmt.impl.AttackVectorImpl <em>Attack Vector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AttackVectorImpl
	 * @see dmt.impl.DmtPackageImpl#getAttackVector()
	 * @generated
	 */
	int ATTACK_VECTOR = 45;

	/**
	 * The meta object id for the '{@link dmt.impl.APropertyImpl <em>AProperty</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.APropertyImpl
	 * @see dmt.impl.DmtPackageImpl#getAProperty()
	 * @generated
	 */
	int APROPERTY = 46;

	/**
	 * The meta object id for the '{@link dmt.impl.AttackStepImpl <em>Attack Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AttackStepImpl
	 * @see dmt.impl.DmtPackageImpl#getAttackStep()
	 * @generated
	 */
	int ATTACK_STEP = 50;

	/**
	 * The meta object id for the '{@link dmt.impl.AttackStepConnectorImpl <em>Attack Step Connector</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AttackStepConnectorImpl
	 * @see dmt.impl.DmtPackageImpl#getAttackStepConnector()
	 * @generated
	 */
	int ATTACK_STEP_CONNECTOR = 47;

	/**
	 * The meta object id for the '{@link dmt.impl.InitialAttackStepImpl <em>Initial Attack Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.InitialAttackStepImpl
	 * @see dmt.impl.DmtPackageImpl#getInitialAttackStep()
	 * @generated
	 */
	int INITIAL_ATTACK_STEP = 48;

	/**
	 * The meta object id for the '{@link dmt.impl.EndAttackStepImpl <em>End Attack Step</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.EndAttackStepImpl
	 * @see dmt.impl.DmtPackageImpl#getEndAttackStep()
	 * @generated
	 */
	int END_ATTACK_STEP = 49;

	/**
	 * The meta object id for the '{@link dmt.impl.AttackTreeImpl <em>Attack Tree</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AttackTreeImpl
	 * @see dmt.impl.DmtPackageImpl#getAttackTree()
	 * @generated
	 */
	int ATTACK_TREE = 51;

	/**
	 * The meta object id for the '{@link dmt.impl.SoftGoalImpl <em>Soft Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.SoftGoalImpl
	 * @see dmt.impl.DmtPackageImpl#getSoftGoal()
	 * @generated
	 */
	int SOFT_GOAL = 53;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionSoftGoalImpl <em>Deception Soft Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionSoftGoalImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionSoftGoal()
	 * @generated
	 */
	int DECEPTION_SOFT_GOAL = 92;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionGoalImpl <em>Deception Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionGoalImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionGoal()
	 * @generated
	 */
	int DECEPTION_GOAL = 91;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionRiskImpl <em>Deception Risk</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionRiskImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionRisk()
	 * @generated
	 */
	int DECEPTION_RISK = 55;

	/**
	 * The meta object id for the '{@link dmt.impl.StrategyNodeImpl <em>Strategy Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.StrategyNodeImpl
	 * @see dmt.impl.DmtPackageImpl#getStrategyNode()
	 * @generated
	 */
	int STRATEGY_NODE = 58;

	/**
	 * The meta object id for the '{@link dmt.impl.AbstractFeatureImpl <em>Abstract Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AbstractFeatureImpl
	 * @see dmt.impl.DmtPackageImpl#getAbstractFeature()
	 * @generated
	 */
	int ABSTRACT_FEATURE = 62;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionStrategyImpl <em>Deception Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionStrategyImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionStrategy()
	 * @generated
	 */
	int DECEPTION_STRATEGY = 56;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionTacticMechanismImpl <em>Deception Tactic Mechanism</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionTacticMechanismImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionTacticMechanism()
	 * @generated
	 */
	int DECEPTION_TACTIC_MECHANISM = 57;

	/**
	 * The meta object id for the '{@link dmt.impl.FeatureImpl <em>Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.FeatureImpl
	 * @see dmt.impl.DmtPackageImpl#getFeature()
	 * @generated
	 */
	int FEATURE = 59;

	/**
	 * The meta object id for the '{@link dmt.impl.StrategyRelationshipImpl <em>Strategy Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.StrategyRelationshipImpl
	 * @see dmt.impl.DmtPackageImpl#getStrategyRelationship()
	 * @generated
	 */
	int STRATEGY_RELATIONSHIP = 60;

	/**
	 * The meta object id for the '{@link dmt.impl.FeatureConstraintImpl <em>Feature Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.FeatureConstraintImpl
	 * @see dmt.impl.DmtPackageImpl#getFeatureConstraint()
	 * @generated
	 */
	int FEATURE_CONSTRAINT = 61;

	/**
	 * The meta object id for the '{@link dmt.impl.RequiredConstraintImpl <em>Required Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.RequiredConstraintImpl
	 * @see dmt.impl.DmtPackageImpl#getRequiredConstraint()
	 * @generated
	 */
	int REQUIRED_CONSTRAINT = 63;

	/**
	 * The meta object id for the '{@link dmt.impl.ExcludeConstraintImpl <em>Exclude Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ExcludeConstraintImpl
	 * @see dmt.impl.DmtPackageImpl#getExcludeConstraint()
	 * @generated
	 */
	int EXCLUDE_CONSTRAINT = 64;

	/**
	 * The meta object id for the '{@link dmt.impl.BenefitConstraintImpl <em>Benefit Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.BenefitConstraintImpl
	 * @see dmt.impl.DmtPackageImpl#getBenefitConstraint()
	 * @generated
	 */
	int BENEFIT_CONSTRAINT = 65;

	/**
	 * The meta object id for the '{@link dmt.impl.AvoidConstraintImpl <em>Avoid Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AvoidConstraintImpl
	 * @see dmt.impl.DmtPackageImpl#getAvoidConstraint()
	 * @generated
	 */
	int AVOID_CONSTRAINT = 66;

	/**
	 * The meta object id for the '{@link dmt.impl.NodeRefinementImpl <em>Node Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.NodeRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getNodeRefinement()
	 * @generated
	 */
	int NODE_REFINEMENT = 67;

	/**
	 * The meta object id for the '{@link dmt.impl.TacticRefinementImpl <em>Tactic Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.TacticRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getTacticRefinement()
	 * @generated
	 */
	int TACTIC_REFINEMENT = 68;

	/**
	 * The meta object id for the '{@link dmt.impl.FeatureRefinementImpl <em>Feature Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.FeatureRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getFeatureRefinement()
	 * @generated
	 */
	int FEATURE_REFINEMENT = 69;

	/**
	 * The meta object id for the '{@link dmt.impl.StrategyTacticRefinementImpl <em>Strategy Tactic Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.StrategyTacticRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getStrategyTacticRefinement()
	 * @generated
	 */
	int STRATEGY_TACTIC_REFINEMENT = 70;

	/**
	 * The meta object id for the '{@link dmt.impl.TacticFeatureRefinementImpl <em>Tactic Feature Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.TacticFeatureRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getTacticFeatureRefinement()
	 * @generated
	 */
	int TACTIC_FEATURE_REFINEMENT = 71;

	/**
	 * The meta object id for the '{@link dmt.impl.SecurityRelationshipImpl <em>Security Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.SecurityRelationshipImpl
	 * @see dmt.impl.DmtPackageImpl#getSecurityRelationship()
	 * @generated
	 */
	int SECURITY_RELATIONSHIP = 74;

	/**
	 * The meta object id for the '{@link dmt.impl.MitigatorImpl <em>Mitigator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.MitigatorImpl
	 * @see dmt.impl.DmtPackageImpl#getMitigator()
	 * @generated
	 */
	int MITIGATOR = 73;

	/**
	 * The meta object id for the '{@link dmt.impl.ObstructionElementImpl <em>Obstruction Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ObstructionElementImpl
	 * @see dmt.impl.DmtPackageImpl#getObstructionElement()
	 * @generated
	 */
	int OBSTRUCTION_ELEMENT = 75;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__NAME = GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__DESCRIPTION = GOAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__UUID = GOAL__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__HAS_ANNOTATIONS = GOAL__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__OR_GOAL_REFINED = GOAL__OR_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__AND_GOAL_REFINED = GOAL__AND_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>Goal Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__GOAL_CATEGORY = GOAL__GOAL_CATEGORY;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__PRIORITY = GOAL__PRIORITY;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__SOURCE = GOAL__SOURCE;

	/**
	 * The feature id for the '<em><b>Stability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__STABILITY = GOAL__STABILITY;

	/**
	 * The feature id for the '<em><b>Resolution</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL__RESOLUTION = GOAL__RESOLUTION;

	/**
	 * The number of structural features of the '<em>Leaf Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL_FEATURE_COUNT = GOAL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Leaf Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LEAF_GOAL_OPERATION_COUNT = GOAL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__VULNERABILITY = LEAF_GOAL__VULNERABILITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__NAME = LEAF_GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__DESCRIPTION = LEAF_GOAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__UUID = LEAF_GOAL__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__HAS_ANNOTATIONS = LEAF_GOAL__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__OR_GOAL_REFINED = LEAF_GOAL__OR_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__AND_GOAL_REFINED = LEAF_GOAL__AND_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>Goal Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__GOAL_CATEGORY = LEAF_GOAL__GOAL_CATEGORY;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__PRIORITY = LEAF_GOAL__PRIORITY;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__SOURCE = LEAF_GOAL__SOURCE;

	/**
	 * The feature id for the '<em><b>Stability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__STABILITY = LEAF_GOAL__STABILITY;

	/**
	 * The feature id for the '<em><b>Resolution</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION__RESOLUTION = LEAF_GOAL__RESOLUTION;

	/**
	 * The number of structural features of the '<em>Expectation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION_FEATURE_COUNT = LEAF_GOAL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Expectation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPECTATION_OPERATION_COUNT = LEAF_GOAL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__VULNERABILITY = LEAF_GOAL__VULNERABILITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__NAME = LEAF_GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__DESCRIPTION = LEAF_GOAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__UUID = LEAF_GOAL__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__HAS_ANNOTATIONS = LEAF_GOAL__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__OR_GOAL_REFINED = LEAF_GOAL__OR_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__AND_GOAL_REFINED = LEAF_GOAL__AND_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>Goal Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__GOAL_CATEGORY = LEAF_GOAL__GOAL_CATEGORY;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__PRIORITY = LEAF_GOAL__PRIORITY;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__SOURCE = LEAF_GOAL__SOURCE;

	/**
	 * The feature id for the '<em><b>Stability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__STABILITY = LEAF_GOAL__STABILITY;

	/**
	 * The feature id for the '<em><b>Resolution</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__RESOLUTION = LEAF_GOAL__RESOLUTION;

	/**
	 * The number of structural features of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_FEATURE_COUNT = LEAF_GOAL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_OPERATION_COUNT = LEAF_GOAL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.ConcreteAssetImpl <em>Concrete Asset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ConcreteAssetImpl
	 * @see dmt.impl.DmtPackageImpl#getConcreteAsset()
	 * @generated
	 */
	int CONCRETE_ASSET = 93;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionBehaviourGoalImpl <em>Deception Behaviour Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionBehaviourGoalImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionBehaviourGoal()
	 * @generated
	 */
	int DECEPTION_BEHAVIOUR_GOAL = 54;

	/**
	 * The meta object id for the '{@link dmt.impl.ORObstructionRefinementImpl <em>OR Obstruction Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ORObstructionRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getORObstructionRefinement()
	 * @generated
	 */
	int OR_OBSTRUCTION_REFINEMENT = 76;

	/**
	 * The meta object id for the '{@link dmt.impl.ANDObstructionRefinementImpl <em>AND Obstruction Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ANDObstructionRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getANDObstructionRefinement()
	 * @generated
	 */
	int AND_OBSTRUCTION_REFINEMENT = 77;

	/**
	 * The meta object id for the '{@link dmt.impl.ObstacleImpl <em>Obstacle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.ObstacleImpl
	 * @see dmt.impl.DmtPackageImpl#getObstacle()
	 * @generated
	 */
	int OBSTACLE = 78;

	/**
	 * The meta object id for the '{@link dmt.impl.InfluenceNodeImpl <em>Influence Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.InfluenceNodeImpl
	 * @see dmt.impl.DmtPackageImpl#getInfluenceNode()
	 * @generated
	 */
	int INFLUENCE_NODE = 79;

	/**
	 * The meta object id for the '{@link dmt.impl.QualityAttributeImpl <em>Quality Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.QualityAttributeImpl
	 * @see dmt.impl.DmtPackageImpl#getQualityAttribute()
	 * @generated
	 */
	int QUALITY_ATTRIBUTE = 80;

	/**
	 * The meta object id for the '{@link dmt.impl.InfluenceRelationshipImpl <em>Influence Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.InfluenceRelationshipImpl
	 * @see dmt.impl.DmtPackageImpl#getInfluenceRelationship()
	 * @generated
	 */
	int INFLUENCE_RELATIONSHIP = 81;

	/**
	 * The meta object id for the '{@link dmt.impl.InfluenceImpl <em>Influence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.InfluenceImpl
	 * @see dmt.impl.DmtPackageImpl#getInfluence()
	 * @generated
	 */
	int INFLUENCE = 82;

	/**
	 * The meta object id for the '{@link dmt.impl.PreferenceImpl <em>Preference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.PreferenceImpl
	 * @see dmt.impl.DmtPackageImpl#getPreference()
	 * @generated
	 */
	int PREFERENCE = 83;

	/**
	 * The meta object id for the '{@link dmt.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.EventImpl
	 * @see dmt.impl.DmtPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 84;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionTacticEventImpl <em>Deception Tactic Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionTacticEventImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionTacticEvent()
	 * @generated
	 */
	int DECEPTION_TACTIC_EVENT = 85;

	/**
	 * The meta object id for the '{@link dmt.impl.EnvironmentEventImpl <em>Environment Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.EnvironmentEventImpl
	 * @see dmt.impl.DmtPackageImpl#getEnvironmentEvent()
	 * @generated
	 */
	int ENVIRONMENT_EVENT = 86;

	/**
	 * The meta object id for the '{@link dmt.impl.VulnerableImpl <em>Vulnerable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.VulnerableImpl
	 * @see dmt.impl.DmtPackageImpl#getVulnerable()
	 * @generated
	 */
	int VULNERABLE = 88;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABLE__VULNERABILITY = 0;

	/**
	 * The number of structural features of the '<em>Vulnerable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Vulnerable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABLE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__VULNERABILITY = VULNERABLE__VULNERABILITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__NAME = VULNERABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__DESCRIPTION = VULNERABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__UUID = VULNERABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__HAS_ANNOTATIONS = VULNERABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Dom Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__DOM_POST = VULNERABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Dom Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__DOM_PRE = VULNERABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Formal Dom Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__FORMAL_DOM_POST = VULNERABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Formal Dom Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__FORMAL_DOM_PRE = VULNERABLE_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = VULNERABLE_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_OPERATION_COUNT = VULNERABLE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GOAL_REFINEMENT__STATUS = GOAL_RELATIONSHIP__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GOAL_REFINEMENT__TACTIC = GOAL_RELATIONSHIP__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GOAL_REFINEMENT__SYS_REF = GOAL_RELATIONSHIP__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GOAL_REFINEMENT__ALT_NAME = GOAL_RELATIONSHIP__ALT_NAME;

	/**
	 * The feature id for the '<em><b>Or Goal Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GOAL_REFINEMENT__OR_GOAL_REFINE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>OR Goal Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GOAL_REFINEMENT_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>OR Goal Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_GOAL_REFINEMENT_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_NODE__NAME = NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_NODE__DESCRIPTION = NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_NODE__UUID = NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_NODE__HAS_ANNOTATIONS = NODE__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Security Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Security Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGABLE_ELEMENT__NAME = SECURITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGABLE_ELEMENT__DESCRIPTION = SECURITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGABLE_ELEMENT__UUID = SECURITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGABLE_ELEMENT__HAS_ANNOTATIONS = SECURITY_NODE__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Mitigable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGABLE_ELEMENT_FEATURE_COUNT = SECURITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Mitigable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGABLE_ELEMENT_OPERATION_COUNT = SECURITY_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__NAME = MITIGABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__DESCRIPTION = MITIGABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__UUID = MITIGABLE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__HAS_ANNOTATIONS = MITIGABLE_ELEMENT__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__TYPE = MITIGABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Instantiates Threat</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__INSTANTIATES_THREAT = MITIGABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Exploits Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__EXPLOITS_VULNERABILITY = MITIGABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Contributes To Attack</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__CONTRIBUTES_TO_ATTACK = MITIGABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Leads Risk</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__LEADS_RISK = MITIGABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__ORIGIN = MITIGABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Realized By Attack Vector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK__REALIZED_BY_ATTACK_VECTOR = MITIGABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Attack</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_FEATURE_COUNT = MITIGABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Attack</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_OPERATION_COUNT = MITIGABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__NAME = MITIGABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__DESCRIPTION = MITIGABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__UUID = MITIGABLE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__HAS_ANNOTATIONS = MITIGABLE_ELEMENT__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Obstruct</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__OBSTRUCT = MITIGABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>OR Obstruction Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__OR_OBSTRUCTION_REFINED = MITIGABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AND Obstruction Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__AND_OBSTRUCTION_REFINED = MITIGABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__CATEGORY = MITIGABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Scenario</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__SCENARIO = MITIGABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Contributes To Threat</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__CONTRIBUTES_TO_THREAT = MITIGABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Consequence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT__CONSEQUENCE = MITIGABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Threat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT_FEATURE_COUNT = MITIGABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Threat</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int THREAT_OPERATION_COUNT = MITIGABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABILITY__NAME = MITIGABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABILITY__DESCRIPTION = MITIGABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABILITY__UUID = MITIGABLE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABILITY__HAS_ANNOTATIONS = MITIGABLE_ELEMENT__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Window Occurrence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABILITY__WINDOW_OCCURRENCE = MITIGABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABILITY__VULNERABILITY_TYPE = MITIGABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Vulnerability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABILITY_FEATURE_COUNT = MITIGABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Vulnerability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VULNERABILITY_OPERATION_COUNT = MITIGABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__NAME = SECURITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__DESCRIPTION = SECURITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__UUID = SECURITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__HAS_ANNOTATIONS = SECURITY_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Pros</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__PROS = SECURITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cons</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__CONS = SECURITY_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__NATURE = SECURITY_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Incident</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__INCIDENT = SECURITY_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Implements</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__IMPLEMENTS = SECURITY_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Security Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL_FEATURE_COUNT = SECURITY_NODE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Security Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL_OPERATION_COUNT = SECURITY_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_COMPONENT__NAME = MITIGABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_COMPONENT__DESCRIPTION = MITIGABLE_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_COMPONENT__UUID = MITIGABLE_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_COMPONENT__HAS_ANNOTATIONS = MITIGABLE_ELEMENT__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Risk Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_COMPONENT__RISK_SEVERITY = MITIGABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Risk Likelihood</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_COMPONENT__RISK_LIKELIHOOD = MITIGABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Harms Asset</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_COMPONENT__HARMS_ASSET = MITIGABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Risk Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_COMPONENT_FEATURE_COUNT = MITIGABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Risk Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RISK_COMPONENT_OPERATION_COUNT = MITIGABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER__NAME = SECURITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER__DESCRIPTION = SECURITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER__UUID = SECURITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER__HAS_ANNOTATIONS = SECURITY_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Profile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER__PROFILE = SECURITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Launches Attack</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER__LAUNCHES_ATTACK = SECURITY_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Attacker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER_FEATURE_COUNT = SECURITY_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Attacker</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACKER_OPERATION_COUNT = SECURITY_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__NAME = SECURITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__DESCRIPTION = SECURITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__UUID = SECURITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__HAS_ANNOTATIONS = SECURITY_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Asset Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__ASSET_TYPE = SECURITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__LOCATION = SECURITY_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__HAS_PROPERTY = SECURITY_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__DOCUMENTATION = SECURITY_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>UID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET__UID = SECURITY_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_FEATURE_COUNT = SECURITY_NODE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSET_OPERATION_COUNT = SECURITY_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_NODE__NAME = NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_NODE__DESCRIPTION = NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_NODE__UUID = NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_NODE__HAS_ANNOTATIONS = NODE__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Deception Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Deception Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__NAME = DECEPTION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__DESCRIPTION = DECEPTION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__UUID = DECEPTION_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__HAS_ANNOTATIONS = DECEPTION_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Dom Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__DOM_POST = DECEPTION_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dom Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__DOM_PRE = DECEPTION_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Formal Dom Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__FORMAL_DOM_POST = DECEPTION_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Formal Dom Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__FORMAL_DOM_PRE = DECEPTION_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Sub Tactic</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__SUB_TACTIC = DECEPTION_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Apply Dissimulation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__APPLY_DISSIMULATION = DECEPTION_NODE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Described By Deception Story</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__DESCRIBED_BY_DECEPTION_STORY = DECEPTION_NODE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Measured By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__MEASURED_BY = DECEPTION_NODE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Monitored By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__MONITORED_BY = DECEPTION_NODE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Lead Deception Risk</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__LEAD_DECEPTION_RISK = DECEPTION_NODE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Apply Simulation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__APPLY_SIMULATION = DECEPTION_NODE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__IS_ABSTRACT = DECEPTION_NODE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Qualityattribute</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC__QUALITYATTRIBUTE = DECEPTION_NODE_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>Deception Tactic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_FEATURE_COUNT = DECEPTION_NODE_FEATURE_COUNT + 13;

	/**
	 * The number of operations of the '<em>Deception Tactic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_OPERATION_COUNT = DECEPTION_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVULNERABILITY__NAME = DECEPTION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVULNERABILITY__DESCRIPTION = DECEPTION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVULNERABILITY__UUID = DECEPTION_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVULNERABILITY__HAS_ANNOTATIONS = DECEPTION_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Window Occurence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVULNERABILITY__WINDOW_OCCURENCE = DECEPTION_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVULNERABILITY__VULNERABILITY_TYPE = DECEPTION_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>IVulnerability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVULNERABILITY_FEATURE_COUNT = DECEPTION_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>IVulnerability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IVULNERABILITY_OPERATION_COUNT = DECEPTION_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TECHNIQUE__NAME = DECEPTION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TECHNIQUE__DESCRIPTION = DECEPTION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TECHNIQUE__UUID = DECEPTION_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TECHNIQUE__HAS_ANNOTATIONS = DECEPTION_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Formal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TECHNIQUE__FORMAL_SPEC = DECEPTION_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Informal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TECHNIQUE__INFORMAL_SPEC = DECEPTION_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Expected Outcome</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TECHNIQUE__EXPECTED_OUTCOME = DECEPTION_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Deception Technique</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TECHNIQUE_FEATURE_COUNT = DECEPTION_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Deception Technique</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TECHNIQUE_OPERATION_COUNT = DECEPTION_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__NAME = DECEPTION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__DESCRIPTION = DECEPTION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__UUID = DECEPTION_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__HAS_ANNOTATIONS = DECEPTION_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>External Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__EXTERNAL_IDENTIFIER = DECEPTION_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Exploit Bias</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__EXPLOIT_BIAS = DECEPTION_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Story Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__STORY_TYPE = DECEPTION_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Pre Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__PRE_CONDITION = DECEPTION_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Pos Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__POS_CONDITION = DECEPTION_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Interaction</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY__INTERACTION = DECEPTION_NODE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Deception Story</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY_FEATURE_COUNT = DECEPTION_NODE_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Deception Story</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STORY_OPERATION_COUNT = DECEPTION_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BIAS__NAME = DECEPTION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BIAS__DESCRIPTION = DECEPTION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BIAS__UUID = DECEPTION_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BIAS__HAS_ANNOTATIONS = DECEPTION_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BIAS__TYPE = DECEPTION_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Deception Bias</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BIAS_FEATURE_COUNT = DECEPTION_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Deception Bias</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BIAS_OPERATION_COUNT = DECEPTION_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__NAME = DECEPTION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__DESCRIPTION = DECEPTION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__UUID = DECEPTION_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__HAS_ANNOTATIONS = DECEPTION_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Asset Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__ASSET_TYPE = DECEPTION_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__LOCATION = DECEPTION_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__HAS_PROPERTY = DECEPTION_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__DOCUMENTATION = DECEPTION_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>UID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__UID = DECEPTION_NODE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Contain Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__CONTAIN_VULNERABILITY = DECEPTION_NODE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Fake Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT__FAKE_TYPE = DECEPTION_NODE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Artificial Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT_FEATURE_COUNT = DECEPTION_NODE_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Artificial Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARTIFICIAL_ELEMENT_OPERATION_COUNT = DECEPTION_NODE_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Security Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_RELATIONSHIP_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Security Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_RELATIONSHIP_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Mitigation Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGATION__MITIGATION_LEVEL = SECURITY_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Mitigation Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGATION__MITIGATION_TARGET = SECURITY_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Mitigation Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGATION__MITIGATION_SOURCE = SECURITY_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Mitigation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGATION_FEATURE_COUNT = SECURITY_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Mitigation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGATION_OPERATION_COUNT = SECURITY_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Interaction Nodes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_NODES_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Interaction Nodes</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_NODES_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Object Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_OBJECT__OBJECT_NAME = INTERACTION_NODES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Class Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_OBJECT__CLASS_NAME = INTERACTION_NODES_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Interaction Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_OBJECT_FEATURE_COUNT = INTERACTION_NODES_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Interaction Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERACTION_OBJECT_OPERATION_COUNT = INTERACTION_NODES_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Integer Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__INTEGER_ID = INTERACTION_NODES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__MESSAGE = INTERACTION_NODES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>To Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__TO_OBJECT = INTERACTION_NODES_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>From Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK__FROM_OBJECT = INTERACTION_NODES_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_FEATURE_COUNT = INTERACTION_NODES_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_OPERATION_COUNT = INTERACTION_NODES_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__NAME = DECEPTION_TECHNIQUE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__DESCRIPTION = DECEPTION_TECHNIQUE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__UUID = DECEPTION_TECHNIQUE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__HAS_ANNOTATIONS = DECEPTION_TECHNIQUE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Formal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__FORMAL_SPEC = DECEPTION_TECHNIQUE__FORMAL_SPEC;

	/**
	 * The feature id for the '<em><b>Informal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__INFORMAL_SPEC = DECEPTION_TECHNIQUE__INFORMAL_SPEC;

	/**
	 * The feature id for the '<em><b>Expected Outcome</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__EXPECTED_OUTCOME = DECEPTION_TECHNIQUE__EXPECTED_OUTCOME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__TYPE = DECEPTION_TECHNIQUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Asset Reference</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION__ASSET_REFERENCE = DECEPTION_TECHNIQUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_FEATURE_COUNT = DECEPTION_TECHNIQUE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_OPERATION_COUNT = DECEPTION_TECHNIQUE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION__NAME = DECEPTION_TECHNIQUE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION__DESCRIPTION = DECEPTION_TECHNIQUE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION__UUID = DECEPTION_TECHNIQUE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION__HAS_ANNOTATIONS = DECEPTION_TECHNIQUE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Formal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION__FORMAL_SPEC = DECEPTION_TECHNIQUE__FORMAL_SPEC;

	/**
	 * The feature id for the '<em><b>Informal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION__INFORMAL_SPEC = DECEPTION_TECHNIQUE__INFORMAL_SPEC;

	/**
	 * The feature id for the '<em><b>Expected Outcome</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION__EXPECTED_OUTCOME = DECEPTION_TECHNIQUE__EXPECTED_OUTCOME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION__TYPE = DECEPTION_TECHNIQUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hide Real Asset</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION__HIDE_REAL_ASSET = DECEPTION_TECHNIQUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Dissimulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION_FEATURE_COUNT = DECEPTION_TECHNIQUE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Dissimulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISSIMULATION_OPERATION_COUNT = DECEPTION_TECHNIQUE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_RELATION__STATUS = GOAL_RELATIONSHIP__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_RELATION__TACTIC = GOAL_RELATIONSHIP__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_RELATION__SYS_REF = GOAL_RELATIONSHIP__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_RELATION__ALT_NAME = GOAL_RELATIONSHIP__ALT_NAME;

	/**
	 * The number of structural features of the '<em>Agent Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_RELATION_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Agent Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_RELATION_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_ASSIGNMENT__STATUS = AGENT_RELATION__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_ASSIGNMENT__TACTIC = AGENT_RELATION__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_ASSIGNMENT__SYS_REF = AGENT_RELATION__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_ASSIGNMENT__ALT_NAME = AGENT_RELATION__ALT_NAME;

	/**
	 * The feature id for the '<em><b>Agent Goal Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_ASSIGNMENT__AGENT_GOAL_ASSIGNMENT = AGENT_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Agent Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_ASSIGNMENT__AGENT_ASSIGNMENT = AGENT_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Agent Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_ASSIGNMENT_FEATURE_COUNT = AGENT_RELATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Agent Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_ASSIGNMENT_OPERATION_COUNT = AGENT_RELATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_GOAL_REFINEMENT__STATUS = GOAL_RELATIONSHIP__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_GOAL_REFINEMENT__TACTIC = GOAL_RELATIONSHIP__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_GOAL_REFINEMENT__SYS_REF = GOAL_RELATIONSHIP__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_GOAL_REFINEMENT__ALT_NAME = GOAL_RELATIONSHIP__ALT_NAME;

	/**
	 * The feature id for the '<em><b>And Goal Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_GOAL_REFINEMENT__AND_GOAL_REFINE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AND Goal Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_GOAL_REFINEMENT_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>AND Goal Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_GOAL_REFINEMENT_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_GOAL_REFINEMENT__STATUS = GOAL_RELATIONSHIP__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_GOAL_REFINEMENT__TACTIC = GOAL_RELATIONSHIP__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_GOAL_REFINEMENT__SYS_REF = GOAL_RELATIONSHIP__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_GOAL_REFINEMENT__ALT_NAME = GOAL_RELATIONSHIP__ALT_NAME;

	/**
	 * The feature id for the '<em><b>Conflict Goal Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_SOURCE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Conflict Goal Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_TARGET = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Conflict Goal Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_GOAL_REFINEMENT_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Conflict Goal Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFLICT_GOAL_REFINEMENT_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_MONITORING__STATUS = AGENT_RELATION__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_MONITORING__TACTIC = AGENT_RELATION__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_MONITORING__SYS_REF = AGENT_RELATION__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_MONITORING__ALT_NAME = AGENT_RELATION__ALT_NAME;

	/**
	 * The feature id for the '<em><b>Agent Monitor</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_MONITORING__AGENT_MONITOR = AGENT_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_MONITORING__OBJECT_ATTRIBUTE = AGENT_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Agent Monitoring</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_MONITORING_FEATURE_COUNT = AGENT_RELATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Agent Monitoring</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_MONITORING_OPERATION_COUNT = AGENT_RELATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_CONTROL__STATUS = AGENT_RELATION__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_CONTROL__TACTIC = AGENT_RELATION__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_CONTROL__SYS_REF = AGENT_RELATION__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_CONTROL__ALT_NAME = AGENT_RELATION__ALT_NAME;

	/**
	 * The feature id for the '<em><b>Object Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_CONTROL__OBJECT_ATTRIBUTE = AGENT_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Agent Control</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_CONTROL__AGENT_CONTROL = AGENT_RELATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Agent Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_CONTROL_FEATURE_COUNT = AGENT_RELATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Agent Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_CONTROL_OPERATION_COUNT = AGENT_RELATION_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_METRIC__NAME = DECEPTION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_METRIC__DESCRIPTION = DECEPTION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_METRIC__UUID = DECEPTION_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_METRIC__HAS_ANNOTATIONS = DECEPTION_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Metric Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_METRIC__METRIC_TYPE = DECEPTION_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_METRIC__UNIT = DECEPTION_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Deception Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_METRIC_FEATURE_COUNT = DECEPTION_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Deception Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_METRIC_OPERATION_COUNT = DECEPTION_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_CHANNEL__NAME = DECEPTION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_CHANNEL__DESCRIPTION = DECEPTION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_CHANNEL__UUID = DECEPTION_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_CHANNEL__HAS_ANNOTATIONS = DECEPTION_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_CHANNEL__TYPE = DECEPTION_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Deception Channel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_CHANNEL_FEATURE_COUNT = DECEPTION_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Deception Channel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_CHANNEL_OPERATION_COUNT = DECEPTION_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR__NAME = SECURITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR__DESCRIPTION = SECURITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR__UUID = SECURITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR__HAS_ANNOTATIONS = SECURITY_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Example</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR__EXAMPLE = SECURITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR__RESOURCES = SECURITY_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Realizes Attack</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR__REALIZES_ATTACK = SECURITY_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Initialattackstep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR__INITIALATTACKSTEP = SECURITY_NODE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Endattackstep</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR__ENDATTACKSTEP = SECURITY_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Attack Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR_FEATURE_COUNT = SECURITY_NODE_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Attack Vector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_VECTOR_OPERATION_COUNT = SECURITY_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__NAME = SECURITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__DESCRIPTION = SECURITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__UUID = SECURITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__HAS_ANNOTATIONS = SECURITY_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Atype</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY__ATYPE = SECURITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AProperty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY_FEATURE_COUNT = SECURITY_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>AProperty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APROPERTY_OPERATION_COUNT = SECURITY_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP__NAME = SECURITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP__DESCRIPTION = SECURITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP__UUID = SECURITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP__HAS_ANNOTATIONS = SECURITY_NODE__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Attack Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_FEATURE_COUNT = SECURITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Attack Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_OPERATION_COUNT = SECURITY_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR__NAME = ATTACK_STEP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR__DESCRIPTION = ATTACK_STEP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR__UUID = ATTACK_STEP__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR__HAS_ANNOTATIONS = ATTACK_STEP__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Subattackstep</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR__SUBATTACKSTEP = ATTACK_STEP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Step Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR__STEP_DESCRIPTION = ATTACK_STEP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Attack Step Connector Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR__ATTACK_STEP_CONNECTOR_TYPE = ATTACK_STEP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR__SEQUENCE = ATTACK_STEP_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Attack Step Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR_FEATURE_COUNT = ATTACK_STEP_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Attack Step Connector</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_STEP_CONNECTOR_OPERATION_COUNT = ATTACK_STEP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_ATTACK_STEP__NAME = ATTACK_STEP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_ATTACK_STEP__DESCRIPTION = ATTACK_STEP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_ATTACK_STEP__UUID = ATTACK_STEP__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_ATTACK_STEP__HAS_ANNOTATIONS = ATTACK_STEP__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Has Attack Steps</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_ATTACK_STEP__HAS_ATTACK_STEPS = ATTACK_STEP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pre Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_ATTACK_STEP__PRE_CONDITION = ATTACK_STEP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Initial Attack Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_ATTACK_STEP_FEATURE_COUNT = ATTACK_STEP_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Initial Attack Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INITIAL_ATTACK_STEP_OPERATION_COUNT = ATTACK_STEP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_ATTACK_STEP__NAME = ATTACK_STEP__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_ATTACK_STEP__DESCRIPTION = ATTACK_STEP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_ATTACK_STEP__UUID = ATTACK_STEP__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_ATTACK_STEP__HAS_ANNOTATIONS = ATTACK_STEP__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Expected Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_ATTACK_STEP__EXPECTED_RESULT = ATTACK_STEP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>End Attack Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_ATTACK_STEP_FEATURE_COUNT = ATTACK_STEP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>End Attack Step</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int END_ATTACK_STEP_OPERATION_COUNT = ATTACK_STEP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_TREE__NAME = SECURITY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_TREE__DESCRIPTION = SECURITY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_TREE__UUID = SECURITY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_TREE__HAS_ANNOTATIONS = SECURITY_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Has Attackvector</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_TREE__HAS_ATTACKVECTOR = SECURITY_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attack Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_TREE_FEATURE_COUNT = SECURITY_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Attack Tree</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTACK_TREE_OPERATION_COUNT = SECURITY_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__VULNERABILITY = GOAL__VULNERABILITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__NAME = GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__DESCRIPTION = GOAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__UUID = GOAL__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__HAS_ANNOTATIONS = GOAL__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__OR_GOAL_REFINED = GOAL__OR_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__AND_GOAL_REFINED = GOAL__AND_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>Goal Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__GOAL_CATEGORY = GOAL__GOAL_CATEGORY;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__PRIORITY = GOAL__PRIORITY;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__SOURCE = GOAL__SOURCE;

	/**
	 * The feature id for the '<em><b>Stability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__STABILITY = GOAL__STABILITY;

	/**
	 * The feature id for the '<em><b>Resolution</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__RESOLUTION = GOAL__RESOLUTION;

	/**
	 * The feature id for the '<em><b>Fit Criterion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__FIT_CRITERION = GOAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Soft Goal Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__SOFT_GOAL_TYPE = GOAL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Soft Goal Open Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__SOFT_GOAL_OPEN_TYPE = GOAL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Soft Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL_FEATURE_COUNT = GOAL_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Soft Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL_OPERATION_COUNT = GOAL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__VULNERABILITY = GOAL__VULNERABILITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__NAME = GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__DESCRIPTION = GOAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__UUID = GOAL__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__HAS_ANNOTATIONS = GOAL__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__OR_GOAL_REFINED = GOAL__OR_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__AND_GOAL_REFINED = GOAL__AND_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>Goal Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__GOAL_CATEGORY = GOAL__GOAL_CATEGORY;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__PRIORITY = GOAL__PRIORITY;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__SOURCE = GOAL__SOURCE;

	/**
	 * The feature id for the '<em><b>Stability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__STABILITY = GOAL__STABILITY;

	/**
	 * The feature id for the '<em><b>Resolution</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__RESOLUTION = GOAL__RESOLUTION;

	/**
	 * The feature id for the '<em><b>Deception Goal Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL__DECEPTION_GOAL_TYPE = GOAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Deception Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL_FEATURE_COUNT = GOAL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Deception Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_GOAL_OPERATION_COUNT = GOAL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__VULNERABILITY = DECEPTION_GOAL__VULNERABILITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__NAME = DECEPTION_GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__DESCRIPTION = DECEPTION_GOAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__UUID = DECEPTION_GOAL__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__HAS_ANNOTATIONS = DECEPTION_GOAL__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__OR_GOAL_REFINED = DECEPTION_GOAL__OR_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__AND_GOAL_REFINED = DECEPTION_GOAL__AND_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>Goal Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__GOAL_CATEGORY = DECEPTION_GOAL__GOAL_CATEGORY;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__PRIORITY = DECEPTION_GOAL__PRIORITY;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__SOURCE = DECEPTION_GOAL__SOURCE;

	/**
	 * The feature id for the '<em><b>Stability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__STABILITY = DECEPTION_GOAL__STABILITY;

	/**
	 * The feature id for the '<em><b>Resolution</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__RESOLUTION = DECEPTION_GOAL__RESOLUTION;

	/**
	 * The feature id for the '<em><b>Deception Goal Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__DECEPTION_GOAL_TYPE = DECEPTION_GOAL__DECEPTION_GOAL_TYPE;

	/**
	 * The feature id for the '<em><b>Formal Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__FORMAL_SPEC = DECEPTION_GOAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL__TYPE = DECEPTION_GOAL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Deception Behaviour Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL_FEATURE_COUNT = DECEPTION_GOAL_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Deception Behaviour Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_BEHAVIOUR_GOAL_OPERATION_COUNT = DECEPTION_GOAL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK__NAME = RISK_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK__DESCRIPTION = RISK_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK__UUID = RISK_COMPONENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK__HAS_ANNOTATIONS = RISK_COMPONENT__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Risk Severity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK__RISK_SEVERITY = RISK_COMPONENT__RISK_SEVERITY;

	/**
	 * The feature id for the '<em><b>Risk Likelihood</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK__RISK_LIKELIHOOD = RISK_COMPONENT__RISK_LIKELIHOOD;

	/**
	 * The feature id for the '<em><b>Harms Asset</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK__HARMS_ASSET = RISK_COMPONENT__HARMS_ASSET;

	/**
	 * The feature id for the '<em><b>Deception Risk Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK__DECEPTION_RISK_TYPE = RISK_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Deception Risk</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK_FEATURE_COUNT = RISK_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Deception Risk</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RISK_OPERATION_COUNT = RISK_COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_NODE__NAME = NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_NODE__DESCRIPTION = NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_NODE__UUID = NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_NODE__HAS_ANNOTATIONS = NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_NODE__PROPERTY = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Strategy Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Strategy Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE__NAME = STRATEGY_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE__DESCRIPTION = STRATEGY_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE__UUID = STRATEGY_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE__HAS_ANNOTATIONS = STRATEGY_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE__PROPERTY = STRATEGY_NODE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Binding State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE__BINDING_STATE = STRATEGY_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE__IS_ABSTRACT = STRATEGY_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Abstract Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE_FEATURE_COUNT = STRATEGY_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Abstract Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_FEATURE_OPERATION_COUNT = STRATEGY_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY__NAME = ABSTRACT_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY__DESCRIPTION = ABSTRACT_FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY__UUID = ABSTRACT_FEATURE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY__HAS_ANNOTATIONS = ABSTRACT_FEATURE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY__PROPERTY = ABSTRACT_FEATURE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Binding State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY__BINDING_STATE = ABSTRACT_FEATURE__BINDING_STATE;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY__IS_ABSTRACT = ABSTRACT_FEATURE__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Is Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY__IS_ROOT = ABSTRACT_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Execution Parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY__EXECUTION_PARAMETER = ABSTRACT_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Deception Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_FEATURE_COUNT = ABSTRACT_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Deception Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_OPERATION_COUNT = ABSTRACT_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM__NAME = ABSTRACT_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM__DESCRIPTION = ABSTRACT_FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM__UUID = ABSTRACT_FEATURE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM__HAS_ANNOTATIONS = ABSTRACT_FEATURE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM__PROPERTY = ABSTRACT_FEATURE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Binding State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM__BINDING_STATE = ABSTRACT_FEATURE__BINDING_STATE;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM__IS_ABSTRACT = ABSTRACT_FEATURE__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Enabling Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM__ENABLING_CONDITION = ABSTRACT_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tactic Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM__TACTIC_REFERENCE = ABSTRACT_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Deception Tactic Mechanism</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM_FEATURE_COUNT = ABSTRACT_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Deception Tactic Mechanism</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_MECHANISM_OPERATION_COUNT = ABSTRACT_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__NAME = ABSTRACT_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__DESCRIPTION = ABSTRACT_FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__UUID = ABSTRACT_FEATURE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__HAS_ANNOTATIONS = ABSTRACT_FEATURE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__PROPERTY = ABSTRACT_FEATURE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Binding State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__BINDING_STATE = ABSTRACT_FEATURE__BINDING_STATE;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE__IS_ABSTRACT = ABSTRACT_FEATURE__IS_ABSTRACT;

	/**
	 * The number of structural features of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_FEATURE_COUNT = ABSTRACT_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_OPERATION_COUNT = ABSTRACT_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_RELATIONSHIP__DESCRIPTION = 0;

	/**
	 * The number of structural features of the '<em>Strategy Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_RELATIONSHIP_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Strategy Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_RELATIONSHIP_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONSTRAINT__DESCRIPTION = STRATEGY_RELATIONSHIP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONSTRAINT__SOURCE = STRATEGY_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONSTRAINT__TARGET = STRATEGY_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONSTRAINT_FEATURE_COUNT = STRATEGY_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Feature Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_CONSTRAINT_OPERATION_COUNT = STRATEGY_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_CONSTRAINT__DESCRIPTION = FEATURE_CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_CONSTRAINT__SOURCE = FEATURE_CONSTRAINT__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_CONSTRAINT__TARGET = FEATURE_CONSTRAINT__TARGET;

	/**
	 * The number of structural features of the '<em>Required Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_CONSTRAINT_FEATURE_COUNT = FEATURE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Required Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_CONSTRAINT_OPERATION_COUNT = FEATURE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDE_CONSTRAINT__DESCRIPTION = FEATURE_CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDE_CONSTRAINT__SOURCE = FEATURE_CONSTRAINT__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDE_CONSTRAINT__TARGET = FEATURE_CONSTRAINT__TARGET;

	/**
	 * The number of structural features of the '<em>Exclude Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDE_CONSTRAINT_FEATURE_COUNT = FEATURE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Exclude Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCLUDE_CONSTRAINT_OPERATION_COUNT = FEATURE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BENEFIT_CONSTRAINT__DESCRIPTION = FEATURE_CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BENEFIT_CONSTRAINT__SOURCE = FEATURE_CONSTRAINT__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BENEFIT_CONSTRAINT__TARGET = FEATURE_CONSTRAINT__TARGET;

	/**
	 * The number of structural features of the '<em>Benefit Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BENEFIT_CONSTRAINT_FEATURE_COUNT = FEATURE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Benefit Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BENEFIT_CONSTRAINT_OPERATION_COUNT = FEATURE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVOID_CONSTRAINT__DESCRIPTION = FEATURE_CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVOID_CONSTRAINT__SOURCE = FEATURE_CONSTRAINT__SOURCE;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVOID_CONSTRAINT__TARGET = FEATURE_CONSTRAINT__TARGET;

	/**
	 * The number of structural features of the '<em>Avoid Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVOID_CONSTRAINT_FEATURE_COUNT = FEATURE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Avoid Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVOID_CONSTRAINT_OPERATION_COUNT = FEATURE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_REFINEMENT__DESCRIPTION = STRATEGY_RELATIONSHIP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enactment Choice</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_REFINEMENT__ENACTMENT_CHOICE = STRATEGY_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Node Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_REFINEMENT_FEATURE_COUNT = STRATEGY_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Node Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_REFINEMENT_OPERATION_COUNT = STRATEGY_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_REFINEMENT__DESCRIPTION = NODE_REFINEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enactment Choice</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_REFINEMENT__ENACTMENT_CHOICE = NODE_REFINEMENT__ENACTMENT_CHOICE;

	/**
	 * The feature id for the '<em><b>Tacticrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_REFINEMENT__TACTICREFINEMENTTARGET = NODE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tacticrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_REFINEMENT__TACTICREFINEMENTSOURCE = NODE_REFINEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tactic Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_REFINEMENT_FEATURE_COUNT = NODE_REFINEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tactic Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_REFINEMENT_OPERATION_COUNT = NODE_REFINEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_REFINEMENT__DESCRIPTION = NODE_REFINEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enactment Choice</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_REFINEMENT__ENACTMENT_CHOICE = NODE_REFINEMENT__ENACTMENT_CHOICE;

	/**
	 * The feature id for the '<em><b>Featurerefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_REFINEMENT__FEATUREREFINEMENTTARGET = NODE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Featurerefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_REFINEMENT__FEATUREREFINEMENTSOURCE = NODE_REFINEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Feature Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_REFINEMENT_FEATURE_COUNT = NODE_REFINEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Feature Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_REFINEMENT_OPERATION_COUNT = NODE_REFINEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_TACTIC_REFINEMENT__DESCRIPTION = NODE_REFINEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enactment Choice</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_TACTIC_REFINEMENT__ENACTMENT_CHOICE = NODE_REFINEMENT__ENACTMENT_CHOICE;

	/**
	 * The feature id for the '<em><b>Strategytacticrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET = NODE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Strategytacticrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE = NODE_REFINEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Strategy Tactic Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_TACTIC_REFINEMENT_FEATURE_COUNT = NODE_REFINEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Strategy Tactic Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRATEGY_TACTIC_REFINEMENT_OPERATION_COUNT = NODE_REFINEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_FEATURE_REFINEMENT__DESCRIPTION = NODE_REFINEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enactment Choice</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_FEATURE_REFINEMENT__ENACTMENT_CHOICE = NODE_REFINEMENT__ENACTMENT_CHOICE;

	/**
	 * The feature id for the '<em><b>Tacticfeaturerefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTTARGET = NODE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tacticfeaturerefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTSOURCE = NODE_REFINEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Tactic Feature Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_FEATURE_REFINEMENT_FEATURE_COUNT = NODE_REFINEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Tactic Feature Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TACTIC_FEATURE_REFINEMENT_OPERATION_COUNT = NODE_REFINEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RELATIONSHIP__ALT_NAME = 0;

	/**
	 * The number of structural features of the '<em>Deception Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RELATIONSHIP_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Deception Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_RELATIONSHIP_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>Mitigator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGATOR_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Mitigator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MITIGATOR_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTION_ELEMENT__NAME = GOAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTION_ELEMENT__DESCRIPTION = GOAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTION_ELEMENT__UUID = GOAL_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTION_ELEMENT__HAS_ANNOTATIONS = GOAL_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Obstruct</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTION_ELEMENT__OBSTRUCT = GOAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>OR Obstruction Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED = GOAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AND Obstruction Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED = GOAL_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Obstruction Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTION_ELEMENT_FEATURE_COUNT = GOAL_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Obstruction Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTRUCTION_ELEMENT_OPERATION_COUNT = GOAL_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_OBSTRUCTION_REFINEMENT__STATUS = GOAL_RELATIONSHIP__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_OBSTRUCTION_REFINEMENT__TACTIC = GOAL_RELATIONSHIP__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_OBSTRUCTION_REFINEMENT__SYS_REF = GOAL_RELATIONSHIP__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_OBSTRUCTION_REFINEMENT__ALT_NAME = GOAL_RELATIONSHIP__ALT_NAME;

	/**
	 * The feature id for the '<em><b>OR Obstruction Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_OBSTRUCTION_REFINEMENT__OR_OBSTRUCTION_REFINE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>OR Obstruction Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_OBSTRUCTION_REFINEMENT_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>OR Obstruction Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OR_OBSTRUCTION_REFINEMENT_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_OBSTRUCTION_REFINEMENT__STATUS = GOAL_RELATIONSHIP__STATUS;

	/**
	 * The feature id for the '<em><b>Tactic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_OBSTRUCTION_REFINEMENT__TACTIC = GOAL_RELATIONSHIP__TACTIC;

	/**
	 * The feature id for the '<em><b>Sys Ref</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_OBSTRUCTION_REFINEMENT__SYS_REF = GOAL_RELATIONSHIP__SYS_REF;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_OBSTRUCTION_REFINEMENT__ALT_NAME = GOAL_RELATIONSHIP__ALT_NAME;

	/**
	 * The feature id for the '<em><b>AND Obstruction Refine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_OBSTRUCTION_REFINEMENT__AND_OBSTRUCTION_REFINE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>AND Obstruction Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_OBSTRUCTION_REFINEMENT_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>AND Obstruction Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AND_OBSTRUCTION_REFINEMENT_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__NAME = OBSTRUCTION_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__DESCRIPTION = OBSTRUCTION_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__UUID = OBSTRUCTION_ELEMENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__HAS_ANNOTATIONS = OBSTRUCTION_ELEMENT__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Obstruct</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__OBSTRUCT = OBSTRUCTION_ELEMENT__OBSTRUCT;

	/**
	 * The feature id for the '<em><b>OR Obstruction Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__OR_OBSTRUCTION_REFINED = OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED;

	/**
	 * The feature id for the '<em><b>AND Obstruction Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__AND_OBSTRUCTION_REFINED = OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED;

	/**
	 * The feature id for the '<em><b>Obstacle Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__OBSTACLE_CATEGORY = OBSTRUCTION_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Formal Def</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__FORMAL_DEF = OBSTRUCTION_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Likelihood</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE__LIKELIHOOD = OBSTRUCTION_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Obstacle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_FEATURE_COUNT = OBSTRUCTION_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Obstacle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBSTACLE_OPERATION_COUNT = OBSTRUCTION_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_NODE__NAME = NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_NODE__DESCRIPTION = NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_NODE__UUID = NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_NODE__HAS_ANNOTATIONS = NODE__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Influence Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_NODE_FEATURE_COUNT = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Influence Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_NODE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALITY_ATTRIBUTE__NAME = INFLUENCE_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALITY_ATTRIBUTE__DESCRIPTION = INFLUENCE_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALITY_ATTRIBUTE__UUID = INFLUENCE_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALITY_ATTRIBUTE__HAS_ANNOTATIONS = INFLUENCE_NODE__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Quality Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALITY_ATTRIBUTE_FEATURE_COUNT = INFLUENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Quality Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUALITY_ATTRIBUTE_OPERATION_COUNT = INFLUENCE_NODE_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Influence Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_RELATIONSHIP_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Influence Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_RELATIONSHIP_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE__WEIGHT = INFLUENCE_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Polarity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE__POLARITY = INFLUENCE_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Influence Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE__INFLUENCE_SOURCE = INFLUENCE_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Influencetarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE__INFLUENCETARGET = INFLUENCE_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Influence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_FEATURE_COUNT = INFLUENCE_RELATIONSHIP_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Influence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INFLUENCE_OPERATION_COUNT = INFLUENCE_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Preference Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFERENCE__PREFERENCE_TARGET = INFLUENCE_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Preference Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFERENCE__PREFERENCE_SOURCE = INFLUENCE_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Weight</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFERENCE__WEIGHT = INFLUENCE_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Preference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFERENCE_FEATURE_COUNT = INFLUENCE_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Preference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFERENCE_OPERATION_COUNT = INFLUENCE_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = INFLUENCE_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__DESCRIPTION = INFLUENCE_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__UUID = INFLUENCE_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__HAS_ANNOTATIONS = INFLUENCE_NODE__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = INFLUENCE_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = INFLUENCE_NODE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_EVENT__NAME = EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_EVENT__DESCRIPTION = EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_EVENT__UUID = EVENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_EVENT__HAS_ANNOTATIONS = EVENT__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Deception Tactic Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Deception Tactic Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_EVENT__NAME = EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_EVENT__DESCRIPTION = EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_EVENT__UUID = EVENT__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_EVENT__HAS_ANNOTATIONS = EVENT__HAS_ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Environment Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Environment Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.GoalRefinableImpl <em>Goal Refinable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.GoalRefinableImpl
	 * @see dmt.impl.DmtPackageImpl#getGoalRefinable()
	 * @generated
	 */
	int GOAL_REFINABLE = 89;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_REFINABLE__NAME = GOAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_REFINABLE__DESCRIPTION = GOAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_REFINABLE__UUID = GOAL_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_REFINABLE__HAS_ANNOTATIONS = GOAL_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_REFINABLE__OR_GOAL_REFINED = GOAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_REFINABLE__AND_GOAL_REFINED = GOAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Goal Refinable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_REFINABLE_FEATURE_COUNT = GOAL_NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Goal Refinable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_REFINABLE_OPERATION_COUNT = GOAL_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.BeliefImpl <em>Belief</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.BeliefImpl
	 * @see dmt.impl.DmtPackageImpl#getBelief()
	 * @generated
	 */
	int BELIEF = 90;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF__NAME = GOAL_REFINABLE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF__DESCRIPTION = GOAL_REFINABLE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF__UUID = GOAL_REFINABLE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF__HAS_ANNOTATIONS = GOAL_REFINABLE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF__OR_GOAL_REFINED = GOAL_REFINABLE__OR_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF__AND_GOAL_REFINED = GOAL_REFINABLE__AND_GOAL_REFINED;

	/**
	 * The number of structural features of the '<em>Belief</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF_FEATURE_COUNT = GOAL_REFINABLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Belief</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF_OPERATION_COUNT = GOAL_REFINABLE_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vulnerability</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__VULNERABILITY = DECEPTION_GOAL__VULNERABILITY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__NAME = DECEPTION_GOAL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__DESCRIPTION = DECEPTION_GOAL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__UUID = DECEPTION_GOAL__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__HAS_ANNOTATIONS = DECEPTION_GOAL__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Or Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__OR_GOAL_REFINED = DECEPTION_GOAL__OR_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>And Goal Refined</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__AND_GOAL_REFINED = DECEPTION_GOAL__AND_GOAL_REFINED;

	/**
	 * The feature id for the '<em><b>Goal Category</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__GOAL_CATEGORY = DECEPTION_GOAL__GOAL_CATEGORY;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__PRIORITY = DECEPTION_GOAL__PRIORITY;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__SOURCE = DECEPTION_GOAL__SOURCE;

	/**
	 * The feature id for the '<em><b>Stability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__STABILITY = DECEPTION_GOAL__STABILITY;

	/**
	 * The feature id for the '<em><b>Resolution</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__RESOLUTION = DECEPTION_GOAL__RESOLUTION;

	/**
	 * The feature id for the '<em><b>Deception Goal Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__DECEPTION_GOAL_TYPE = DECEPTION_GOAL__DECEPTION_GOAL_TYPE;

	/**
	 * The feature id for the '<em><b>Fit Criterion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__FIT_CRITERION = DECEPTION_GOAL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Soft Goal Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__SOFT_GOAL_TYPE = DECEPTION_GOAL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Soft Goal Open Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL__SOFT_GOAL_OPEN_TYPE = DECEPTION_GOAL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Deception Soft Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL_FEATURE_COUNT = DECEPTION_GOAL_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Deception Soft Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_SOFT_GOAL_OPERATION_COUNT = DECEPTION_GOAL_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__NAME = ASSET__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__DESCRIPTION = ASSET__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__UUID = ASSET__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__HAS_ANNOTATIONS = ASSET__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Asset Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__ASSET_TYPE = ASSET__ASSET_TYPE;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__LOCATION = ASSET__LOCATION;

	/**
	 * The feature id for the '<em><b>Has Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__HAS_PROPERTY = ASSET__HAS_PROPERTY;

	/**
	 * The feature id for the '<em><b>Documentation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__DOCUMENTATION = ASSET__DOCUMENTATION;

	/**
	 * The feature id for the '<em><b>UID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__UID = ASSET__UID;

	/**
	 * The feature id for the '<em><b>Business Importance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET__BUSINESS_IMPORTANCE = ASSET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Concrete Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET_FEATURE_COUNT = ASSET_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Concrete Asset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCRETE_ASSET_OPERATION_COUNT = ASSET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.OperationableImpl <em>Operationable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.OperationableImpl
	 * @see dmt.impl.DmtPackageImpl#getOperationable()
	 * @generated
	 */
	int OPERATIONABLE = 94;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE__NAME = GOAL_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE__DESCRIPTION = GOAL_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE__UUID = GOAL_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE__HAS_ANNOTATIONS = GOAL_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Dom Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE__DOM_POST = GOAL_NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dom Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE__DOM_PRE = GOAL_NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Formal Dom Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE__FORMAL_DOM_POST = GOAL_NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Formal Dom Pre</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE__FORMAL_DOM_PRE = GOAL_NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Operationable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE_FEATURE_COUNT = GOAL_NODE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Operationable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATIONABLE_OPERATION_COUNT = GOAL_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.GoalContainerImpl <em>Goal Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.GoalContainerImpl
	 * @see dmt.impl.DmtPackageImpl#getGoalContainer()
	 * @generated
	 */
	int GOAL_CONTAINER = 95;

	/**
	 * The feature id for the '<em><b>Goal</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_CONTAINER__GOAL = 0;

	/**
	 * The number of structural features of the '<em>Goal Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Goal Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dmt.impl.NodeSelectionImpl <em>Node Selection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.NodeSelectionImpl
	 * @see dmt.impl.DmtPackageImpl#getNodeSelection()
	 * @generated
	 */
	int NODE_SELECTION = 96;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_SELECTION__DESCRIPTION = STRATEGY_RELATIONSHIP__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Selectionsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_SELECTION__SELECTIONSOURCE = STRATEGY_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selectiontarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_SELECTION__SELECTIONTARGET = STRATEGY_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_SELECTION__TYPE = STRATEGY_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Node Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_SELECTION_FEATURE_COUNT = STRATEGY_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Node Selection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_SELECTION_OPERATION_COUNT = STRATEGY_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionTacticStrategyImpl <em>Deception Tactic Strategy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionTacticStrategyImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionTacticStrategy()
	 * @generated
	 */
	int DECEPTION_TACTIC_STRATEGY = 97;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY__NAME = ABSTRACT_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY__DESCRIPTION = ABSTRACT_FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY__UUID = ABSTRACT_FEATURE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY__HAS_ANNOTATIONS = ABSTRACT_FEATURE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY__PROPERTY = ABSTRACT_FEATURE__PROPERTY;

	/**
	 * The feature id for the '<em><b>Binding State</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY__BINDING_STATE = ABSTRACT_FEATURE__BINDING_STATE;

	/**
	 * The feature id for the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY__IS_ABSTRACT = ABSTRACT_FEATURE__IS_ABSTRACT;

	/**
	 * The feature id for the '<em><b>Deceptiontacticmechanism</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY__DECEPTIONTACTICMECHANISM = ABSTRACT_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Deception Tactic Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY_FEATURE_COUNT = ABSTRACT_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Deception Tactic Strategy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY_OPERATION_COUNT = ABSTRACT_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionStrategyTacticRefinementImpl <em>Deception Strategy Tactic Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionStrategyTacticRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionStrategyTacticRefinement()
	 * @generated
	 */
	int DECEPTION_STRATEGY_TACTIC_REFINEMENT = 98;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_TACTIC_REFINEMENT__DESCRIPTION = NODE_REFINEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enactment Choice</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_TACTIC_REFINEMENT__ENACTMENT_CHOICE = NODE_REFINEMENT__ENACTMENT_CHOICE;

	/**
	 * The feature id for the '<em><b>Strategytacticrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE = NODE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Strategytacticrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET = NODE_REFINEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Deception Strategy Tactic Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_TACTIC_REFINEMENT_FEATURE_COUNT = NODE_REFINEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Deception Strategy Tactic Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_TACTIC_REFINEMENT_OPERATION_COUNT = NODE_REFINEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionTacticStrategyRefinementImpl <em>Deception Tactic Strategy Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionTacticStrategyRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionTacticStrategyRefinement()
	 * @generated
	 */
	int DECEPTION_TACTIC_STRATEGY_REFINEMENT = 99;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY_REFINEMENT__DESCRIPTION = NODE_REFINEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enactment Choice</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY_REFINEMENT__ENACTMENT_CHOICE = NODE_REFINEMENT__ENACTMENT_CHOICE;

	/**
	 * The feature id for the '<em><b>Tacticstrategytacticrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTSOURCE = NODE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tacticstrategytacticrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTTARGET = NODE_REFINEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Deception Tactic Strategy Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY_REFINEMENT_FEATURE_COUNT = NODE_REFINEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Deception Tactic Strategy Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_TACTIC_STRATEGY_REFINEMENT_OPERATION_COUNT = NODE_REFINEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.DeceptionStrategyRefinementImpl <em>Deception Strategy Refinement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.DeceptionStrategyRefinementImpl
	 * @see dmt.impl.DmtPackageImpl#getDeceptionStrategyRefinement()
	 * @generated
	 */
	int DECEPTION_STRATEGY_REFINEMENT = 100;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_REFINEMENT__DESCRIPTION = NODE_REFINEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Enactment Choice</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_REFINEMENT__ENACTMENT_CHOICE = NODE_REFINEMENT__ENACTMENT_CHOICE;

	/**
	 * The feature id for the '<em><b>Deceptionstrategyrefinementsource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTSOURCE = NODE_REFINEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Deceptionstrategyrefinementtarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTTARGET = NODE_REFINEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Deception Strategy Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_REFINEMENT_FEATURE_COUNT = NODE_REFINEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Deception Strategy Refinement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECEPTION_STRATEGY_REFINEMENT_OPERATION_COUNT = NODE_REFINEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.SimulationHasArtificialElementImpl <em>Simulation Has Artificial Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.SimulationHasArtificialElementImpl
	 * @see dmt.impl.DmtPackageImpl#getSimulationHasArtificialElement()
	 * @generated
	 */
	int SIMULATION_HAS_ARTIFICIAL_ELEMENT = 101;

	/**
	 * The feature id for the '<em><b>Alt Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_HAS_ARTIFICIAL_ELEMENT__ALT_NAME = DECEPTION_RELATIONSHIP__ALT_NAME;

	/**
	 * The feature id for the '<em><b>Show False Asset</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET = DECEPTION_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Lower Fakes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES = DECEPTION_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Upper Fakes</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES = DECEPTION_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Simulation Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE = DECEPTION_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Simulation Has Artificial Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_HAS_ARTIFICIAL_ELEMENT_FEATURE_COUNT = DECEPTION_RELATIONSHIP_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Simulation Has Artificial Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_HAS_ARTIFICIAL_ELEMENT_OPERATION_COUNT = DECEPTION_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.PropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.PropertyImpl
	 * @see dmt.impl.DmtPackageImpl#getProperty()
	 * @generated
	 */
	int PROPERTY = 102;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__NAME = DECEPTION_NODE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__DESCRIPTION = DECEPTION_NODE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__UUID = DECEPTION_NODE__UUID;

	/**
	 * The feature id for the '<em><b>Has Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__HAS_ANNOTATIONS = DECEPTION_NODE__HAS_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY__VALUE = DECEPTION_NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_FEATURE_COUNT = DECEPTION_NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROPERTY_OPERATION_COUNT = DECEPTION_NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link dmt.impl.AnnotationImpl <em>Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.impl.AnnotationImpl
	 * @see dmt.impl.DmtPackageImpl#getAnnotation()
	 * @generated
	 */
	int ANNOTATION = 103;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__TEXT = 0;

	/**
	 * The number of structural features of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link dmt.GoalCategoryEnum <em>Goal Category Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.GoalCategoryEnum
	 * @see dmt.impl.DmtPackageImpl#getGoalCategoryEnum()
	 * @generated
	 */
	int GOAL_CATEGORY_ENUM = 104;

	/**
	 * The meta object id for the '{@link dmt.DomainPropertyCategoryEnum <em>Domain Property Category Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.DomainPropertyCategoryEnum
	 * @see dmt.impl.DmtPackageImpl#getDomainPropertyCategoryEnum()
	 * @generated
	 */
	int DOMAIN_PROPERTY_CATEGORY_ENUM = 105;

	/**
	 * The meta object id for the '{@link dmt.GoalPriorityEnum <em>Goal Priority Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.GoalPriorityEnum
	 * @see dmt.impl.DmtPackageImpl#getGoalPriorityEnum()
	 * @generated
	 */
	int GOAL_PRIORITY_ENUM = 106;

	/**
	 * The meta object id for the '{@link dmt.ObstacleCategoryEnum <em>Obstacle Category Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.ObstacleCategoryEnum
	 * @see dmt.impl.DmtPackageImpl#getObstacleCategoryEnum()
	 * @generated
	 */
	int OBSTACLE_CATEGORY_ENUM = 131;

	/**
	 * The meta object id for the '{@link dmt.PreferenceWeightEnum <em>Preference Weight Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.PreferenceWeightEnum
	 * @see dmt.impl.DmtPackageImpl#getPreferenceWeightEnum()
	 * @generated
	 */
	int PREFERENCE_WEIGHT_ENUM = 132;

	/**
	 * The meta object id for the '{@link dmt.InfluenceWeightEnum <em>Influence Weight Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.InfluenceWeightEnum
	 * @see dmt.impl.DmtPackageImpl#getInfluenceWeightEnum()
	 * @generated
	 */
	int INFLUENCE_WEIGHT_ENUM = 133;

	/**
	 * The meta object id for the '{@link dmt.InfluencePolarityEnum <em>Influence Polarity Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.InfluencePolarityEnum
	 * @see dmt.impl.DmtPackageImpl#getInfluencePolarityEnum()
	 * @generated
	 */
	int INFLUENCE_POLARITY_ENUM = 134;

	/**
	 * The meta object id for the '{@link dmt.DeceptionGoalTypeEnum <em>Deception Goal Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.DeceptionGoalTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getDeceptionGoalTypeEnum()
	 * @generated
	 */
	int DECEPTION_GOAL_TYPE_ENUM = 135;

	/**
	 * The meta object id for the '{@link dmt.SoftGoalTypeEnum <em>Soft Goal Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.SoftGoalTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getSoftGoalTypeEnum()
	 * @generated
	 */
	int SOFT_GOAL_TYPE_ENUM = 136;

	/**
	 * The meta object id for the '{@link dmt.NodesSelectionType <em>Nodes Selection Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.NodesSelectionType
	 * @see dmt.impl.DmtPackageImpl#getNodesSelectionType()
	 * @generated
	 */
	int NODES_SELECTION_TYPE = 137;

	/**
	 * The meta object id for the '{@link dmt.ChannelTypeEnum <em>Channel Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.ChannelTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getChannelTypeEnum()
	 * @generated
	 */
	int CHANNEL_TYPE_ENUM = 138;

	/**
	 * The meta object id for the '{@link dmt.MitigationLevelEnum <em>Mitigation Level Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.MitigationLevelEnum
	 * @see dmt.impl.DmtPackageImpl#getMitigationLevelEnum()
	 * @generated
	 */
	int MITIGATION_LEVEL_ENUM = 107;

	/**
	 * The meta object id for the '{@link dmt.AssetTypeEnum <em>Asset Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.AssetTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getAssetTypeEnum()
	 * @generated
	 */
	int ASSET_TYPE_ENUM = 108;

	/**
	 * The meta object id for the '{@link dmt.AttackTypeEnum <em>Attack Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.AttackTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getAttackTypeEnum()
	 * @generated
	 */
	int ATTACK_TYPE_ENUM = 109;

	/**
	 * The meta object id for the '{@link dmt.AttackerProfileEnum <em>Attacker Profile Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.AttackerProfileEnum
	 * @see dmt.impl.DmtPackageImpl#getAttackerProfileEnum()
	 * @generated
	 */
	int ATTACKER_PROFILE_ENUM = 110;

	/**
	 * The meta object id for the '{@link dmt.RiskLikelihoodEnum <em>Risk Likelihood Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.RiskLikelihoodEnum
	 * @see dmt.impl.DmtPackageImpl#getRiskLikelihoodEnum()
	 * @generated
	 */
	int RISK_LIKELIHOOD_ENUM = 111;

	/**
	 * The meta object id for the '{@link dmt.RiskSeverityEnum <em>Risk Severity Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.RiskSeverityEnum
	 * @see dmt.impl.DmtPackageImpl#getRiskSeverityEnum()
	 * @generated
	 */
	int RISK_SEVERITY_ENUM = 112;

	/**
	 * The meta object id for the '{@link dmt.SecurityControlIncidentEnum <em>Security Control Incident Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.SecurityControlIncidentEnum
	 * @see dmt.impl.DmtPackageImpl#getSecurityControlIncidentEnum()
	 * @generated
	 */
	int SECURITY_CONTROL_INCIDENT_ENUM = 113;

	/**
	 * The meta object id for the '{@link dmt.ThreatCategoryEnum <em>Threat Category Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.ThreatCategoryEnum
	 * @see dmt.impl.DmtPackageImpl#getThreatCategoryEnum()
	 * @generated
	 */
	int THREAT_CATEGORY_ENUM = 114;

	/**
	 * The meta object id for the '{@link dmt.VulnerabilityTypeEnum <em>Vulnerability Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.VulnerabilityTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getVulnerabilityTypeEnum()
	 * @generated
	 */
	int VULNERABILITY_TYPE_ENUM = 115;

	/**
	 * The meta object id for the '{@link dmt.SecurityControlNatureEnum <em>Security Control Nature Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.SecurityControlNatureEnum
	 * @see dmt.impl.DmtPackageImpl#getSecurityControlNatureEnum()
	 * @generated
	 */
	int SECURITY_CONTROL_NATURE_ENUM = 116;

	/**
	 * The meta object id for the '{@link dmt.BiasTypeEnum <em>Bias Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.BiasTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getBiasTypeEnum()
	 * @generated
	 */
	int BIAS_TYPE_ENUM = 117;

	/**
	 * The meta object id for the '{@link dmt.SimulationTechniqueTypeEnum <em>Simulation Technique Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.SimulationTechniqueTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getSimulationTechniqueTypeEnum()
	 * @generated
	 */
	int SIMULATION_TECHNIQUE_TYPE_ENUM = 118;

	/**
	 * The meta object id for the '{@link dmt.MetricTypeEnum <em>Metric Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.MetricTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getMetricTypeEnum()
	 * @generated
	 */
	int METRIC_TYPE_ENUM = 119;

	/**
	 * The meta object id for the '{@link dmt.DissimulationTechniqueTypeEnum <em>Dissimulation Technique Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.DissimulationTechniqueTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getDissimulationTechniqueTypeEnum()
	 * @generated
	 */
	int DISSIMULATION_TECHNIQUE_TYPE_ENUM = 120;

	/**
	 * The meta object id for the '{@link dmt.ArtificialElementTypeEnum <em>Artificial Element Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.ArtificialElementTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getArtificialElementTypeEnum()
	 * @generated
	 */
	int ARTIFICIAL_ELEMENT_TYPE_ENUM = 130;

	/**
	 * The meta object id for the '{@link dmt.GoalTypeEnum <em>Goal Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.GoalTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getGoalTypeEnum()
	 * @generated
	 */
	int GOAL_TYPE_ENUM = 121;

	/**
	 * The meta object id for the '{@link dmt.GoalStabilityEnum <em>Goal Stability Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.GoalStabilityEnum
	 * @see dmt.impl.DmtPackageImpl#getGoalStabilityEnum()
	 * @generated
	 */
	int GOAL_STABILITY_ENUM = 122;

	/**
	 * The meta object id for the '{@link dmt.EnvironmentAgentTypeEnum <em>Environment Agent Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.EnvironmentAgentTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getEnvironmentAgentTypeEnum()
	 * @generated
	 */
	int ENVIRONMENT_AGENT_TYPE_ENUM = 123;

	/**
	 * The meta object id for the '{@link dmt.AttackOriginEnum <em>Attack Origin Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.AttackOriginEnum
	 * @see dmt.impl.DmtPackageImpl#getAttackOriginEnum()
	 * @generated
	 */
	int ATTACK_ORIGIN_ENUM = 124;

	/**
	 * The meta object id for the '{@link dmt.AttackConnectorEnum <em>Attack Connector Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.AttackConnectorEnum
	 * @see dmt.impl.DmtPackageImpl#getAttackConnectorEnum()
	 * @generated
	 */
	int ATTACK_CONNECTOR_ENUM = 125;

	/**
	 * The meta object id for the '{@link dmt.StoryTypeEnum <em>Story Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.StoryTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getStoryTypeEnum()
	 * @generated
	 */
	int STORY_TYPE_ENUM = 126;

	/**
	 * The meta object id for the '{@link dmt.DeceptionRiskTypeEnum <em>Deception Risk Type Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.DeceptionRiskTypeEnum
	 * @see dmt.impl.DmtPackageImpl#getDeceptionRiskTypeEnum()
	 * @generated
	 */
	int DECEPTION_RISK_TYPE_ENUM = 127;

	/**
	 * The meta object id for the '{@link dmt.BindingStateEnum <em>Binding State Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.BindingStateEnum
	 * @see dmt.impl.DmtPackageImpl#getBindingStateEnum()
	 * @generated
	 */
	int BINDING_STATE_ENUM = 128;

	/**
	 * The meta object id for the '{@link dmt.EnactmentEnum <em>Enactment Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see dmt.EnactmentEnum
	 * @see dmt.impl.DmtPackageImpl#getEnactmentEnum()
	 * @generated
	 */
	int ENACTMENT_ENUM = 129;


	/**
	 * Returns the meta object for class '{@link dmt.DMTModel <em>DMT Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DMT Model</em>'.
	 * @see dmt.DMTModel
	 * @generated
	 */
	EClass getDMTModel();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasSecurityNodes <em>Has Security Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Security Nodes</em>'.
	 * @see dmt.DMTModel#getHasSecurityNodes()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasSecurityNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasDeceptionNodes <em>Has Deception Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Deception Nodes</em>'.
	 * @see dmt.DMTModel#getHasDeceptionNodes()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasDeceptionNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasGoalNodes <em>Has Goal Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Goal Nodes</em>'.
	 * @see dmt.DMTModel#getHasGoalNodes()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasGoalNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasGoalRelations <em>Has Goal Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Goal Relations</em>'.
	 * @see dmt.DMTModel#getHasGoalRelations()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasGoalRelations();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasStrategyNodes <em>Has Strategy Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Strategy Nodes</em>'.
	 * @see dmt.DMTModel#getHasStrategyNodes()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasStrategyNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasStrategyRelations <em>Has Strategy Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Strategy Relations</em>'.
	 * @see dmt.DMTModel#getHasStrategyRelations()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasStrategyRelations();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasDeceptionRelations <em>Has Deception Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Deception Relations</em>'.
	 * @see dmt.DMTModel#getHasDeceptionRelations()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasDeceptionRelations();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasSecurityRelations <em>Has Security Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Security Relations</em>'.
	 * @see dmt.DMTModel#getHasSecurityRelations()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasSecurityRelations();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasInfluenceNodes <em>Has Influence Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Influence Nodes</em>'.
	 * @see dmt.DMTModel#getHasInfluenceNodes()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasInfluenceNodes();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasInfluenceRelations <em>Has Influence Relations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Influence Relations</em>'.
	 * @see dmt.DMTModel#getHasInfluenceRelations()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasInfluenceRelations();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasGoalContainers <em>Has Goal Containers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Goal Containers</em>'.
	 * @see dmt.DMTModel#getHasGoalContainers()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasGoalContainers();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.DMTModel#getHasMiscNodes <em>Has Misc Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Misc Nodes</em>'.
	 * @see dmt.DMTModel#getHasMiscNodes()
	 * @see #getDMTModel()
	 * @generated
	 */
	EReference getDMTModel_HasMiscNodes();

	/**
	 * Returns the meta object for class '{@link dmt.Goal <em>Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal</em>'.
	 * @see dmt.Goal
	 * @generated
	 */
	EClass getGoal();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Goal#getResolution <em>Resolution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Resolution</em>'.
	 * @see dmt.Goal#getResolution()
	 * @see #getGoal()
	 * @generated
	 */
	EReference getGoal_Resolution();

	/**
	 * Returns the meta object for class '{@link dmt.GoalRelationship <em>Goal Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal Relationship</em>'.
	 * @see dmt.GoalRelationship
	 * @generated
	 */
	EClass getGoalRelationship();

	/**
	 * Returns the meta object for the attribute '{@link dmt.GoalRelationship#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see dmt.GoalRelationship#getStatus()
	 * @see #getGoalRelationship()
	 * @generated
	 */
	EAttribute getGoalRelationship_Status();

	/**
	 * Returns the meta object for the attribute '{@link dmt.GoalRelationship#getTactic <em>Tactic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tactic</em>'.
	 * @see dmt.GoalRelationship#getTactic()
	 * @see #getGoalRelationship()
	 * @generated
	 */
	EAttribute getGoalRelationship_Tactic();

	/**
	 * Returns the meta object for the attribute '{@link dmt.GoalRelationship#getSysRef <em>Sys Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sys Ref</em>'.
	 * @see dmt.GoalRelationship#getSysRef()
	 * @see #getGoalRelationship()
	 * @generated
	 */
	EAttribute getGoalRelationship_SysRef();

	/**
	 * Returns the meta object for the attribute '{@link dmt.GoalRelationship#getAltName <em>Alt Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Alt Name</em>'.
	 * @see dmt.GoalRelationship#getAltName()
	 * @see #getGoalRelationship()
	 * @generated
	 */
	EAttribute getGoalRelationship_AltName();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Goal#getGoalCategory <em>Goal Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Goal Category</em>'.
	 * @see dmt.Goal#getGoalCategory()
	 * @see #getGoal()
	 * @generated
	 */
	EAttribute getGoal_GoalCategory();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Goal#getPriority <em>Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Priority</em>'.
	 * @see dmt.Goal#getPriority()
	 * @see #getGoal()
	 * @generated
	 */
	EAttribute getGoal_Priority();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Goal#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source</em>'.
	 * @see dmt.Goal#getSource()
	 * @see #getGoal()
	 * @generated
	 */
	EAttribute getGoal_Source();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Goal#getStability <em>Stability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stability</em>'.
	 * @see dmt.Goal#getStability()
	 * @see #getGoal()
	 * @generated
	 */
	EAttribute getGoal_Stability();

	/**
	 * Returns the meta object for class '{@link dmt.Agent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent</em>'.
	 * @see dmt.Agent
	 * @generated
	 */
	EClass getAgent();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Agent#getDef <em>Def</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Def</em>'.
	 * @see dmt.Agent#getDef()
	 * @see #getAgent()
	 * @generated
	 */
	EAttribute getAgent_Def();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Agent#getLoad <em>Load</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load</em>'.
	 * @see dmt.Agent#getLoad()
	 * @see #getAgent()
	 * @generated
	 */
	EAttribute getAgent_Load();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.Agent#getSubagent <em>Subagent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Subagent</em>'.
	 * @see dmt.Agent#getSubagent()
	 * @see #getAgent()
	 * @generated
	 */
	EReference getAgent_Subagent();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Agent#getPerformsOperation <em>Performs Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Performs Operation</em>'.
	 * @see dmt.Agent#getPerformsOperation()
	 * @see #getAgent()
	 * @generated
	 */
	EReference getAgent_PerformsOperation();

	/**
	 * Returns the meta object for class '{@link dmt.Operationalization <em>Operationalization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operationalization</em>'.
	 * @see dmt.Operationalization
	 * @generated
	 */
	EClass getOperationalization();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Operationalization#getReqPre <em>Req Pre</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Req Pre</em>'.
	 * @see dmt.Operationalization#getReqPre()
	 * @see #getOperationalization()
	 * @generated
	 */
	EAttribute getOperationalization_ReqPre();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Operationalization#getReqTrig <em>Req Trig</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Req Trig</em>'.
	 * @see dmt.Operationalization#getReqTrig()
	 * @see #getOperationalization()
	 * @generated
	 */
	EAttribute getOperationalization_ReqTrig();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Operationalization#getReqPost <em>Req Post</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Req Post</em>'.
	 * @see dmt.Operationalization#getReqPost()
	 * @see #getOperationalization()
	 * @generated
	 */
	EAttribute getOperationalization_ReqPost();

	/**
	 * Returns the meta object for the reference '{@link dmt.Operationalization#getOperationalizeGoal <em>Operationalize Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operationalize Goal</em>'.
	 * @see dmt.Operationalization#getOperationalizeGoal()
	 * @see #getOperationalization()
	 * @generated
	 */
	EReference getOperationalization_OperationalizeGoal();

	/**
	 * Returns the meta object for the reference '{@link dmt.Operationalization#getOperationalization <em>Operationalization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operationalization</em>'.
	 * @see dmt.Operationalization#getOperationalization()
	 * @see #getOperationalization()
	 * @generated
	 */
	EReference getOperationalization_Operationalization();

	/**
	 * Returns the meta object for class '{@link dmt.SoftwareAgent <em>Software Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Software Agent</em>'.
	 * @see dmt.SoftwareAgent
	 * @generated
	 */
	EClass getSoftwareAgent();

	/**
	 * Returns the meta object for the reference list '{@link dmt.SoftwareAgent#getResponsibilitySoftware <em>Responsibility Software</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Responsibility Software</em>'.
	 * @see dmt.SoftwareAgent#getResponsibilitySoftware()
	 * @see #getSoftwareAgent()
	 * @generated
	 */
	EReference getSoftwareAgent_ResponsibilitySoftware();

	/**
	 * Returns the meta object for class '{@link dmt.EnvironmentAgent <em>Environment Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment Agent</em>'.
	 * @see dmt.EnvironmentAgent
	 * @generated
	 */
	EClass getEnvironmentAgent();

	/**
	 * Returns the meta object for the attribute '{@link dmt.EnvironmentAgent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dmt.EnvironmentAgent#getType()
	 * @see #getEnvironmentAgent()
	 * @generated
	 */
	EAttribute getEnvironmentAgent_Type();

	/**
	 * Returns the meta object for the reference list '{@link dmt.EnvironmentAgent#getResponsibilityEnvironment <em>Responsibility Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Responsibility Environment</em>'.
	 * @see dmt.EnvironmentAgent#getResponsibilityEnvironment()
	 * @see #getEnvironmentAgent()
	 * @generated
	 */
	EReference getEnvironmentAgent_ResponsibilityEnvironment();

	/**
	 * Returns the meta object for class '{@link dmt.BehaviourGoal <em>Behaviour Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Behaviour Goal</em>'.
	 * @see dmt.BehaviourGoal
	 * @generated
	 */
	EClass getBehaviourGoal();

	/**
	 * Returns the meta object for the attribute '{@link dmt.BehaviourGoal#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dmt.BehaviourGoal#getType()
	 * @see #getBehaviourGoal()
	 * @generated
	 */
	EAttribute getBehaviourGoal_Type();

	/**
	 * Returns the meta object for the attribute '{@link dmt.BehaviourGoal#getFormalSpec <em>Formal Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formal Spec</em>'.
	 * @see dmt.BehaviourGoal#getFormalSpec()
	 * @see #getBehaviourGoal()
	 * @generated
	 */
	EAttribute getBehaviourGoal_FormalSpec();

	/**
	 * Returns the meta object for class '{@link dmt.DomainDescription <em>Domain Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Domain Description</em>'.
	 * @see dmt.DomainDescription
	 * @generated
	 */
	EClass getDomainDescription();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DomainDescription#getDomainCategory <em>Domain Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domain Category</em>'.
	 * @see dmt.DomainDescription#getDomainCategory()
	 * @see #getDomainDescription()
	 * @generated
	 */
	EAttribute getDomainDescription_DomainCategory();

	/**
	 * Returns the meta object for class '{@link dmt.LeafGoal <em>Leaf Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Leaf Goal</em>'.
	 * @see dmt.LeafGoal
	 * @generated
	 */
	EClass getLeafGoal();

	/**
	 * Returns the meta object for class '{@link dmt.Expectation <em>Expectation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expectation</em>'.
	 * @see dmt.Expectation
	 * @generated
	 */
	EClass getExpectation();

	/**
	 * Returns the meta object for class '{@link dmt.Requirement <em>Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requirement</em>'.
	 * @see dmt.Requirement
	 * @generated
	 */
	EClass getRequirement();

	/**
	 * Returns the meta object for class '{@link dmt.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see dmt.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for class '{@link dmt.ORGoalRefinement <em>OR Goal Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OR Goal Refinement</em>'.
	 * @see dmt.ORGoalRefinement
	 * @generated
	 */
	EClass getORGoalRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.ORGoalRefinement#getOrGoalRefine <em>Or Goal Refine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Or Goal Refine</em>'.
	 * @see dmt.ORGoalRefinement#getOrGoalRefine()
	 * @see #getORGoalRefinement()
	 * @generated
	 */
	EReference getORGoalRefinement_OrGoalRefine();

	/**
	 * Returns the meta object for class '{@link dmt.SecurityNode <em>Security Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Security Node</em>'.
	 * @see dmt.SecurityNode
	 * @generated
	 */
	EClass getSecurityNode();

	/**
	 * Returns the meta object for class '{@link dmt.MitigableElement <em>Mitigable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mitigable Element</em>'.
	 * @see dmt.MitigableElement
	 * @generated
	 */
	EClass getMitigableElement();

	/**
	 * Returns the meta object for class '{@link dmt.Attack <em>Attack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attack</em>'.
	 * @see dmt.Attack
	 * @generated
	 */
	EClass getAttack();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Attack#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dmt.Attack#getType()
	 * @see #getAttack()
	 * @generated
	 */
	EAttribute getAttack_Type();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Attack#getInstantiatesThreat <em>Instantiates Threat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Instantiates Threat</em>'.
	 * @see dmt.Attack#getInstantiatesThreat()
	 * @see #getAttack()
	 * @generated
	 */
	EReference getAttack_InstantiatesThreat();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Attack#getExploitsVulnerability <em>Exploits Vulnerability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Exploits Vulnerability</em>'.
	 * @see dmt.Attack#getExploitsVulnerability()
	 * @see #getAttack()
	 * @generated
	 */
	EReference getAttack_ExploitsVulnerability();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Attack#getContributesToAttack <em>Contributes To Attack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contributes To Attack</em>'.
	 * @see dmt.Attack#getContributesToAttack()
	 * @see #getAttack()
	 * @generated
	 */
	EReference getAttack_ContributesToAttack();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Attack#getLeadsRisk <em>Leads Risk</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Leads Risk</em>'.
	 * @see dmt.Attack#getLeadsRisk()
	 * @see #getAttack()
	 * @generated
	 */
	EReference getAttack_LeadsRisk();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Attack#getOrigin <em>Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Origin</em>'.
	 * @see dmt.Attack#getOrigin()
	 * @see #getAttack()
	 * @generated
	 */
	EAttribute getAttack_Origin();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Attack#getRealizedByAttackVector <em>Realized By Attack Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Realized By Attack Vector</em>'.
	 * @see dmt.Attack#getRealizedByAttackVector()
	 * @see #getAttack()
	 * @generated
	 */
	EReference getAttack_RealizedByAttackVector();

	/**
	 * Returns the meta object for class '{@link dmt.Threat <em>Threat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Threat</em>'.
	 * @see dmt.Threat
	 * @generated
	 */
	EClass getThreat();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Threat#getCategory <em>Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Category</em>'.
	 * @see dmt.Threat#getCategory()
	 * @see #getThreat()
	 * @generated
	 */
	EAttribute getThreat_Category();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Threat#getScenario <em>Scenario</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scenario</em>'.
	 * @see dmt.Threat#getScenario()
	 * @see #getThreat()
	 * @generated
	 */
	EAttribute getThreat_Scenario();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Threat#getContributesToThreat <em>Contributes To Threat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contributes To Threat</em>'.
	 * @see dmt.Threat#getContributesToThreat()
	 * @see #getThreat()
	 * @generated
	 */
	EReference getThreat_ContributesToThreat();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Threat#getConsequence <em>Consequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Consequence</em>'.
	 * @see dmt.Threat#getConsequence()
	 * @see #getThreat()
	 * @generated
	 */
	EAttribute getThreat_Consequence();

	/**
	 * Returns the meta object for class '{@link dmt.Vulnerability <em>Vulnerability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vulnerability</em>'.
	 * @see dmt.Vulnerability
	 * @generated
	 */
	EClass getVulnerability();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Vulnerability#getWindowOccurrence <em>Window Occurrence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Window Occurrence</em>'.
	 * @see dmt.Vulnerability#getWindowOccurrence()
	 * @see #getVulnerability()
	 * @generated
	 */
	EAttribute getVulnerability_WindowOccurrence();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Vulnerability#getVulnerabilityType <em>Vulnerability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vulnerability Type</em>'.
	 * @see dmt.Vulnerability#getVulnerabilityType()
	 * @see #getVulnerability()
	 * @generated
	 */
	EAttribute getVulnerability_VulnerabilityType();

	/**
	 * Returns the meta object for class '{@link dmt.SecurityControl <em>Security Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Security Control</em>'.
	 * @see dmt.SecurityControl
	 * @generated
	 */
	EClass getSecurityControl();

	/**
	 * Returns the meta object for the attribute '{@link dmt.SecurityControl#getPros <em>Pros</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pros</em>'.
	 * @see dmt.SecurityControl#getPros()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EAttribute getSecurityControl_Pros();

	/**
	 * Returns the meta object for the attribute '{@link dmt.SecurityControl#getCons <em>Cons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cons</em>'.
	 * @see dmt.SecurityControl#getCons()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EAttribute getSecurityControl_Cons();

	/**
	 * Returns the meta object for the attribute '{@link dmt.SecurityControl#getNature <em>Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nature</em>'.
	 * @see dmt.SecurityControl#getNature()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EAttribute getSecurityControl_Nature();

	/**
	 * Returns the meta object for the attribute '{@link dmt.SecurityControl#getIncident <em>Incident</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Incident</em>'.
	 * @see dmt.SecurityControl#getIncident()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EAttribute getSecurityControl_Incident();

	/**
	 * Returns the meta object for the reference '{@link dmt.SecurityControl#getImplements <em>Implements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Implements</em>'.
	 * @see dmt.SecurityControl#getImplements()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EReference getSecurityControl_Implements();

	/**
	 * Returns the meta object for class '{@link dmt.RiskComponent <em>Risk Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Risk Component</em>'.
	 * @see dmt.RiskComponent
	 * @generated
	 */
	EClass getRiskComponent();

	/**
	 * Returns the meta object for the attribute '{@link dmt.RiskComponent#getRiskSeverity <em>Risk Severity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Risk Severity</em>'.
	 * @see dmt.RiskComponent#getRiskSeverity()
	 * @see #getRiskComponent()
	 * @generated
	 */
	EAttribute getRiskComponent_RiskSeverity();

	/**
	 * Returns the meta object for the attribute '{@link dmt.RiskComponent#getRiskLikelihood <em>Risk Likelihood</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Risk Likelihood</em>'.
	 * @see dmt.RiskComponent#getRiskLikelihood()
	 * @see #getRiskComponent()
	 * @generated
	 */
	EAttribute getRiskComponent_RiskLikelihood();

	/**
	 * Returns the meta object for the reference list '{@link dmt.RiskComponent#getHarmsAsset <em>Harms Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Harms Asset</em>'.
	 * @see dmt.RiskComponent#getHarmsAsset()
	 * @see #getRiskComponent()
	 * @generated
	 */
	EReference getRiskComponent_HarmsAsset();

	/**
	 * Returns the meta object for class '{@link dmt.Attacker <em>Attacker</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attacker</em>'.
	 * @see dmt.Attacker
	 * @generated
	 */
	EClass getAttacker();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Attacker#getProfile <em>Profile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Profile</em>'.
	 * @see dmt.Attacker#getProfile()
	 * @see #getAttacker()
	 * @generated
	 */
	EAttribute getAttacker_Profile();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Attacker#getLaunchesAttack <em>Launches Attack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Launches Attack</em>'.
	 * @see dmt.Attacker#getLaunchesAttack()
	 * @see #getAttacker()
	 * @generated
	 */
	EReference getAttacker_LaunchesAttack();

	/**
	 * Returns the meta object for class '{@link dmt.Asset <em>Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Asset</em>'.
	 * @see dmt.Asset
	 * @generated
	 */
	EClass getAsset();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Asset#getAssetType <em>Asset Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Asset Type</em>'.
	 * @see dmt.Asset#getAssetType()
	 * @see #getAsset()
	 * @generated
	 */
	EAttribute getAsset_AssetType();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Asset#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see dmt.Asset#getLocation()
	 * @see #getAsset()
	 * @generated
	 */
	EAttribute getAsset_Location();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.Asset#getHasProperty <em>Has Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Property</em>'.
	 * @see dmt.Asset#getHasProperty()
	 * @see #getAsset()
	 * @generated
	 */
	EReference getAsset_HasProperty();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Asset#getDocumentation <em>Documentation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Documentation</em>'.
	 * @see dmt.Asset#getDocumentation()
	 * @see #getAsset()
	 * @generated
	 */
	EAttribute getAsset_Documentation();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Asset#getUID <em>UID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>UID</em>'.
	 * @see dmt.Asset#getUID()
	 * @see #getAsset()
	 * @generated
	 */
	EAttribute getAsset_UID();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionNode <em>Deception Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Node</em>'.
	 * @see dmt.DeceptionNode
	 * @generated
	 */
	EClass getDeceptionNode();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionTactic <em>Deception Tactic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Tactic</em>'.
	 * @see dmt.DeceptionTactic
	 * @generated
	 */
	EClass getDeceptionTactic();

	/**
	 * Returns the meta object for the reference list '{@link dmt.DeceptionTactic#getSubTactic <em>Sub Tactic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Tactic</em>'.
	 * @see dmt.DeceptionTactic#getSubTactic()
	 * @see #getDeceptionTactic()
	 * @generated
	 */
	EReference getDeceptionTactic_SubTactic();

	/**
	 * Returns the meta object for the reference list '{@link dmt.DeceptionTactic#getApplyDissimulation <em>Apply Dissimulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Apply Dissimulation</em>'.
	 * @see dmt.DeceptionTactic#getApplyDissimulation()
	 * @see #getDeceptionTactic()
	 * @generated
	 */
	EReference getDeceptionTactic_ApplyDissimulation();

	/**
	 * Returns the meta object for the reference list '{@link dmt.DeceptionTactic#getDescribedByDeceptionStory <em>Described By Deception Story</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Described By Deception Story</em>'.
	 * @see dmt.DeceptionTactic#getDescribedByDeceptionStory()
	 * @see #getDeceptionTactic()
	 * @generated
	 */
	EReference getDeceptionTactic_DescribedByDeceptionStory();

	/**
	 * Returns the meta object for the reference list '{@link dmt.DeceptionTactic#getMeasuredBy <em>Measured By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Measured By</em>'.
	 * @see dmt.DeceptionTactic#getMeasuredBy()
	 * @see #getDeceptionTactic()
	 * @generated
	 */
	EReference getDeceptionTactic_MeasuredBy();

	/**
	 * Returns the meta object for the reference list '{@link dmt.DeceptionTactic#getMonitoredBy <em>Monitored By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Monitored By</em>'.
	 * @see dmt.DeceptionTactic#getMonitoredBy()
	 * @see #getDeceptionTactic()
	 * @generated
	 */
	EReference getDeceptionTactic_MonitoredBy();

	/**
	 * Returns the meta object for the reference list '{@link dmt.DeceptionTactic#getLeadDeceptionRisk <em>Lead Deception Risk</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Lead Deception Risk</em>'.
	 * @see dmt.DeceptionTactic#getLeadDeceptionRisk()
	 * @see #getDeceptionTactic()
	 * @generated
	 */
	EReference getDeceptionTactic_LeadDeceptionRisk();

	/**
	 * Returns the meta object for the reference list '{@link dmt.DeceptionTactic#getApplySimulation <em>Apply Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Apply Simulation</em>'.
	 * @see dmt.DeceptionTactic#getApplySimulation()
	 * @see #getDeceptionTactic()
	 * @generated
	 */
	EReference getDeceptionTactic_ApplySimulation();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionTactic#isIsAbstract <em>Is Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Abstract</em>'.
	 * @see dmt.DeceptionTactic#isIsAbstract()
	 * @see #getDeceptionTactic()
	 * @generated
	 */
	EAttribute getDeceptionTactic_IsAbstract();

	/**
	 * Returns the meta object for the reference list '{@link dmt.DeceptionTactic#getQualityattribute <em>Qualityattribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Qualityattribute</em>'.
	 * @see dmt.DeceptionTactic#getQualityattribute()
	 * @see #getDeceptionTactic()
	 * @generated
	 */
	EReference getDeceptionTactic_Qualityattribute();

	/**
	 * Returns the meta object for class '{@link dmt.IVulnerability <em>IVulnerability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IVulnerability</em>'.
	 * @see dmt.IVulnerability
	 * @generated
	 */
	EClass getIVulnerability();

	/**
	 * Returns the meta object for the attribute '{@link dmt.IVulnerability#getWindowOccurence <em>Window Occurence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Window Occurence</em>'.
	 * @see dmt.IVulnerability#getWindowOccurence()
	 * @see #getIVulnerability()
	 * @generated
	 */
	EAttribute getIVulnerability_WindowOccurence();

	/**
	 * Returns the meta object for the attribute '{@link dmt.IVulnerability#getVulnerabilityType <em>Vulnerability Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vulnerability Type</em>'.
	 * @see dmt.IVulnerability#getVulnerabilityType()
	 * @see #getIVulnerability()
	 * @generated
	 */
	EAttribute getIVulnerability_VulnerabilityType();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionTechnique <em>Deception Technique</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Technique</em>'.
	 * @see dmt.DeceptionTechnique
	 * @generated
	 */
	EClass getDeceptionTechnique();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionTechnique#getFormalSpec <em>Formal Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formal Spec</em>'.
	 * @see dmt.DeceptionTechnique#getFormalSpec()
	 * @see #getDeceptionTechnique()
	 * @generated
	 */
	EAttribute getDeceptionTechnique_FormalSpec();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionTechnique#getInformalSpec <em>Informal Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Informal Spec</em>'.
	 * @see dmt.DeceptionTechnique#getInformalSpec()
	 * @see #getDeceptionTechnique()
	 * @generated
	 */
	EAttribute getDeceptionTechnique_InformalSpec();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionTechnique#getExpectedOutcome <em>Expected Outcome</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expected Outcome</em>'.
	 * @see dmt.DeceptionTechnique#getExpectedOutcome()
	 * @see #getDeceptionTechnique()
	 * @generated
	 */
	EAttribute getDeceptionTechnique_ExpectedOutcome();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionStory <em>Deception Story</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Story</em>'.
	 * @see dmt.DeceptionStory
	 * @generated
	 */
	EClass getDeceptionStory();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionStory#getExternalIdentifier <em>External Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>External Identifier</em>'.
	 * @see dmt.DeceptionStory#getExternalIdentifier()
	 * @see #getDeceptionStory()
	 * @generated
	 */
	EAttribute getDeceptionStory_ExternalIdentifier();

	/**
	 * Returns the meta object for the reference list '{@link dmt.DeceptionStory#getExploitBias <em>Exploit Bias</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Exploit Bias</em>'.
	 * @see dmt.DeceptionStory#getExploitBias()
	 * @see #getDeceptionStory()
	 * @generated
	 */
	EReference getDeceptionStory_ExploitBias();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionStory#getStoryType <em>Story Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Story Type</em>'.
	 * @see dmt.DeceptionStory#getStoryType()
	 * @see #getDeceptionStory()
	 * @generated
	 */
	EAttribute getDeceptionStory_StoryType();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionStory#getPreCondition <em>Pre Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pre Condition</em>'.
	 * @see dmt.DeceptionStory#getPreCondition()
	 * @see #getDeceptionStory()
	 * @generated
	 */
	EAttribute getDeceptionStory_PreCondition();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionStory#getPosCondition <em>Pos Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pos Condition</em>'.
	 * @see dmt.DeceptionStory#getPosCondition()
	 * @see #getDeceptionStory()
	 * @generated
	 */
	EAttribute getDeceptionStory_PosCondition();

	/**
	 * Returns the meta object for the reference '{@link dmt.DeceptionStory#getInteraction <em>Interaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interaction</em>'.
	 * @see dmt.DeceptionStory#getInteraction()
	 * @see #getDeceptionStory()
	 * @generated
	 */
	EReference getDeceptionStory_Interaction();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionBias <em>Deception Bias</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Bias</em>'.
	 * @see dmt.DeceptionBias
	 * @generated
	 */
	EClass getDeceptionBias();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionBias#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dmt.DeceptionBias#getType()
	 * @see #getDeceptionBias()
	 * @generated
	 */
	EAttribute getDeceptionBias_Type();

	/**
	 * Returns the meta object for class '{@link dmt.ArtificialElement <em>Artificial Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Artificial Element</em>'.
	 * @see dmt.ArtificialElement
	 * @generated
	 */
	EClass getArtificialElement();

	/**
	 * Returns the meta object for the reference list '{@link dmt.ArtificialElement#getContainVulnerability <em>Contain Vulnerability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contain Vulnerability</em>'.
	 * @see dmt.ArtificialElement#getContainVulnerability()
	 * @see #getArtificialElement()
	 * @generated
	 */
	EReference getArtificialElement_ContainVulnerability();

	/**
	 * Returns the meta object for the attribute '{@link dmt.ArtificialElement#getFakeType <em>Fake Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fake Type</em>'.
	 * @see dmt.ArtificialElement#getFakeType()
	 * @see #getArtificialElement()
	 * @generated
	 */
	EAttribute getArtificialElement_FakeType();

	/**
	 * Returns the meta object for class '{@link dmt.InteractionNodes <em>Interaction Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction Nodes</em>'.
	 * @see dmt.InteractionNodes
	 * @generated
	 */
	EClass getInteractionNodes();

	/**
	 * Returns the meta object for class '{@link dmt.InteractionObject <em>Interaction Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interaction Object</em>'.
	 * @see dmt.InteractionObject
	 * @generated
	 */
	EClass getInteractionObject();

	/**
	 * Returns the meta object for the attribute '{@link dmt.InteractionObject#getObjectName <em>Object Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object Name</em>'.
	 * @see dmt.InteractionObject#getObjectName()
	 * @see #getInteractionObject()
	 * @generated
	 */
	EAttribute getInteractionObject_ObjectName();

	/**
	 * Returns the meta object for the attribute '{@link dmt.InteractionObject#getClassName <em>Class Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Class Name</em>'.
	 * @see dmt.InteractionObject#getClassName()
	 * @see #getInteractionObject()
	 * @generated
	 */
	EAttribute getInteractionObject_ClassName();

	/**
	 * Returns the meta object for class '{@link dmt.Link <em>Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Link</em>'.
	 * @see dmt.Link
	 * @generated
	 */
	EClass getLink();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Link#getIntegerId <em>Integer Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Integer Id</em>'.
	 * @see dmt.Link#getIntegerId()
	 * @see #getLink()
	 * @generated
	 */
	EAttribute getLink_IntegerId();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Link#getMessage <em>Message</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message</em>'.
	 * @see dmt.Link#getMessage()
	 * @see #getLink()
	 * @generated
	 */
	EAttribute getLink_Message();

	/**
	 * Returns the meta object for the reference '{@link dmt.Link#getToObject <em>To Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To Object</em>'.
	 * @see dmt.Link#getToObject()
	 * @see #getLink()
	 * @generated
	 */
	EReference getLink_ToObject();

	/**
	 * Returns the meta object for the reference '{@link dmt.Link#getFromObject <em>From Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From Object</em>'.
	 * @see dmt.Link#getFromObject()
	 * @see #getLink()
	 * @generated
	 */
	EReference getLink_FromObject();

	/**
	 * Returns the meta object for class '{@link dmt.Mitigation <em>Mitigation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mitigation</em>'.
	 * @see dmt.Mitigation
	 * @generated
	 */
	EClass getMitigation();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Mitigation#getMitigationLevel <em>Mitigation Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mitigation Level</em>'.
	 * @see dmt.Mitigation#getMitigationLevel()
	 * @see #getMitigation()
	 * @generated
	 */
	EAttribute getMitigation_MitigationLevel();

	/**
	 * Returns the meta object for the reference '{@link dmt.Mitigation#getMitigationTarget <em>Mitigation Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mitigation Target</em>'.
	 * @see dmt.Mitigation#getMitigationTarget()
	 * @see #getMitigation()
	 * @generated
	 */
	EReference getMitigation_MitigationTarget();

	/**
	 * Returns the meta object for the reference '{@link dmt.Mitigation#getMitigationSource <em>Mitigation Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Mitigation Source</em>'.
	 * @see dmt.Mitigation#getMitigationSource()
	 * @see #getMitigation()
	 * @generated
	 */
	EReference getMitigation_MitigationSource();

	/**
	 * Returns the meta object for class '{@link dmt.Simulation <em>Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simulation</em>'.
	 * @see dmt.Simulation
	 * @generated
	 */
	EClass getSimulation();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Simulation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dmt.Simulation#getType()
	 * @see #getSimulation()
	 * @generated
	 */
	EAttribute getSimulation_Type();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Simulation#getAssetReference <em>Asset Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Asset Reference</em>'.
	 * @see dmt.Simulation#getAssetReference()
	 * @see #getSimulation()
	 * @generated
	 */
	EReference getSimulation_AssetReference();

	/**
	 * Returns the meta object for class '{@link dmt.Dissimulation <em>Dissimulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dissimulation</em>'.
	 * @see dmt.Dissimulation
	 * @generated
	 */
	EClass getDissimulation();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Dissimulation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dmt.Dissimulation#getType()
	 * @see #getDissimulation()
	 * @generated
	 */
	EAttribute getDissimulation_Type();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Dissimulation#getHideRealAsset <em>Hide Real Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Hide Real Asset</em>'.
	 * @see dmt.Dissimulation#getHideRealAsset()
	 * @see #getDissimulation()
	 * @generated
	 */
	EReference getDissimulation_HideRealAsset();

	/**
	 * Returns the meta object for class '{@link dmt.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see dmt.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Node#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see dmt.Node#getName()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_Name();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Node#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see dmt.Node#getDescription()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_Description();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Node#getUUID <em>UUID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>UUID</em>'.
	 * @see dmt.Node#getUUID()
	 * @see #getNode()
	 * @generated
	 */
	EAttribute getNode_UUID();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Node#getHasAnnotations <em>Has Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has Annotations</em>'.
	 * @see dmt.Node#getHasAnnotations()
	 * @see #getNode()
	 * @generated
	 */
	EReference getNode_HasAnnotations();

	/**
	 * Returns the meta object for class '{@link dmt.AgentAssignment <em>Agent Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent Assignment</em>'.
	 * @see dmt.AgentAssignment
	 * @generated
	 */
	EClass getAgentAssignment();

	/**
	 * Returns the meta object for the reference '{@link dmt.AgentAssignment#getAgentGoalAssignment <em>Agent Goal Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Agent Goal Assignment</em>'.
	 * @see dmt.AgentAssignment#getAgentGoalAssignment()
	 * @see #getAgentAssignment()
	 * @generated
	 */
	EReference getAgentAssignment_AgentGoalAssignment();

	/**
	 * Returns the meta object for the reference '{@link dmt.AgentAssignment#getAgentAssignment <em>Agent Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Agent Assignment</em>'.
	 * @see dmt.AgentAssignment#getAgentAssignment()
	 * @see #getAgentAssignment()
	 * @generated
	 */
	EReference getAgentAssignment_AgentAssignment();

	/**
	 * Returns the meta object for class '{@link dmt.ANDGoalRefinement <em>AND Goal Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AND Goal Refinement</em>'.
	 * @see dmt.ANDGoalRefinement
	 * @generated
	 */
	EClass getANDGoalRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.ANDGoalRefinement#getAndGoalRefine <em>And Goal Refine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>And Goal Refine</em>'.
	 * @see dmt.ANDGoalRefinement#getAndGoalRefine()
	 * @see #getANDGoalRefinement()
	 * @generated
	 */
	EReference getANDGoalRefinement_AndGoalRefine();

	/**
	 * Returns the meta object for class '{@link dmt.ConflictGoalRefinement <em>Conflict Goal Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conflict Goal Refinement</em>'.
	 * @see dmt.ConflictGoalRefinement
	 * @generated
	 */
	EClass getConflictGoalRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.ConflictGoalRefinement#getConflictGoalSource <em>Conflict Goal Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Goal Source</em>'.
	 * @see dmt.ConflictGoalRefinement#getConflictGoalSource()
	 * @see #getConflictGoalRefinement()
	 * @generated
	 */
	EReference getConflictGoalRefinement_ConflictGoalSource();

	/**
	 * Returns the meta object for the reference '{@link dmt.ConflictGoalRefinement#getConflictGoalTarget <em>Conflict Goal Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Conflict Goal Target</em>'.
	 * @see dmt.ConflictGoalRefinement#getConflictGoalTarget()
	 * @see #getConflictGoalRefinement()
	 * @generated
	 */
	EReference getConflictGoalRefinement_ConflictGoalTarget();

	/**
	 * Returns the meta object for class '{@link dmt.AgentMonitoring <em>Agent Monitoring</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent Monitoring</em>'.
	 * @see dmt.AgentMonitoring
	 * @generated
	 */
	EClass getAgentMonitoring();

	/**
	 * Returns the meta object for the reference list '{@link dmt.AgentMonitoring#getAgentMonitor <em>Agent Monitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Agent Monitor</em>'.
	 * @see dmt.AgentMonitoring#getAgentMonitor()
	 * @see #getAgentMonitoring()
	 * @generated
	 */
	EReference getAgentMonitoring_AgentMonitor();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AgentMonitoring#getObjectAttribute <em>Object Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object Attribute</em>'.
	 * @see dmt.AgentMonitoring#getObjectAttribute()
	 * @see #getAgentMonitoring()
	 * @generated
	 */
	EAttribute getAgentMonitoring_ObjectAttribute();

	/**
	 * Returns the meta object for class '{@link dmt.AgentControl <em>Agent Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent Control</em>'.
	 * @see dmt.AgentControl
	 * @generated
	 */
	EClass getAgentControl();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AgentControl#getObjectAttribute <em>Object Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object Attribute</em>'.
	 * @see dmt.AgentControl#getObjectAttribute()
	 * @see #getAgentControl()
	 * @generated
	 */
	EAttribute getAgentControl_ObjectAttribute();

	/**
	 * Returns the meta object for the reference list '{@link dmt.AgentControl#getAgentControl <em>Agent Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Agent Control</em>'.
	 * @see dmt.AgentControl#getAgentControl()
	 * @see #getAgentControl()
	 * @generated
	 */
	EReference getAgentControl_AgentControl();

	/**
	 * Returns the meta object for class '{@link dmt.AgentRelation <em>Agent Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent Relation</em>'.
	 * @see dmt.AgentRelation
	 * @generated
	 */
	EClass getAgentRelation();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionMetric <em>Deception Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Metric</em>'.
	 * @see dmt.DeceptionMetric
	 * @generated
	 */
	EClass getDeceptionMetric();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionMetric#getMetricType <em>Metric Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Metric Type</em>'.
	 * @see dmt.DeceptionMetric#getMetricType()
	 * @see #getDeceptionMetric()
	 * @generated
	 */
	EAttribute getDeceptionMetric_MetricType();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionMetric#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit</em>'.
	 * @see dmt.DeceptionMetric#getUnit()
	 * @see #getDeceptionMetric()
	 * @generated
	 */
	EAttribute getDeceptionMetric_Unit();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionChannel <em>Deception Channel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Channel</em>'.
	 * @see dmt.DeceptionChannel
	 * @generated
	 */
	EClass getDeceptionChannel();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionChannel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dmt.DeceptionChannel#getType()
	 * @see #getDeceptionChannel()
	 * @generated
	 */
	EAttribute getDeceptionChannel_Type();

	/**
	 * Returns the meta object for class '{@link dmt.AttackVector <em>Attack Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attack Vector</em>'.
	 * @see dmt.AttackVector
	 * @generated
	 */
	EClass getAttackVector();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AttackVector#getExample <em>Example</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Example</em>'.
	 * @see dmt.AttackVector#getExample()
	 * @see #getAttackVector()
	 * @generated
	 */
	EAttribute getAttackVector_Example();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AttackVector#getResources <em>Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resources</em>'.
	 * @see dmt.AttackVector#getResources()
	 * @see #getAttackVector()
	 * @generated
	 */
	EAttribute getAttackVector_Resources();

	/**
	 * Returns the meta object for the reference '{@link dmt.AttackVector#getRealizesAttack <em>Realizes Attack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Realizes Attack</em>'.
	 * @see dmt.AttackVector#getRealizesAttack()
	 * @see #getAttackVector()
	 * @generated
	 */
	EReference getAttackVector_RealizesAttack();

	/**
	 * Returns the meta object for the containment reference '{@link dmt.AttackVector#getInitialattackstep <em>Initialattackstep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Initialattackstep</em>'.
	 * @see dmt.AttackVector#getInitialattackstep()
	 * @see #getAttackVector()
	 * @generated
	 */
	EReference getAttackVector_Initialattackstep();

	/**
	 * Returns the meta object for the containment reference '{@link dmt.AttackVector#getEndattackstep <em>Endattackstep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Endattackstep</em>'.
	 * @see dmt.AttackVector#getEndattackstep()
	 * @see #getAttackVector()
	 * @generated
	 */
	EReference getAttackVector_Endattackstep();

	/**
	 * Returns the meta object for class '{@link dmt.AProperty <em>AProperty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AProperty</em>'.
	 * @see dmt.AProperty
	 * @generated
	 */
	EClass getAProperty();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AProperty#getAtype <em>Atype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Atype</em>'.
	 * @see dmt.AProperty#getAtype()
	 * @see #getAProperty()
	 * @generated
	 */
	EAttribute getAProperty_Atype();

	/**
	 * Returns the meta object for class '{@link dmt.AttackStepConnector <em>Attack Step Connector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attack Step Connector</em>'.
	 * @see dmt.AttackStepConnector
	 * @generated
	 */
	EClass getAttackStepConnector();

	/**
	 * Returns the meta object for the reference list '{@link dmt.AttackStepConnector#getSubattackstep <em>Subattackstep</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Subattackstep</em>'.
	 * @see dmt.AttackStepConnector#getSubattackstep()
	 * @see #getAttackStepConnector()
	 * @generated
	 */
	EReference getAttackStepConnector_Subattackstep();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AttackStepConnector#getStepDescription <em>Step Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Step Description</em>'.
	 * @see dmt.AttackStepConnector#getStepDescription()
	 * @see #getAttackStepConnector()
	 * @generated
	 */
	EAttribute getAttackStepConnector_StepDescription();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AttackStepConnector#getAttackStepConnectorType <em>Attack Step Connector Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attack Step Connector Type</em>'.
	 * @see dmt.AttackStepConnector#getAttackStepConnectorType()
	 * @see #getAttackStepConnector()
	 * @generated
	 */
	EAttribute getAttackStepConnector_AttackStepConnectorType();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AttackStepConnector#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sequence</em>'.
	 * @see dmt.AttackStepConnector#getSequence()
	 * @see #getAttackStepConnector()
	 * @generated
	 */
	EAttribute getAttackStepConnector_Sequence();

	/**
	 * Returns the meta object for class '{@link dmt.InitialAttackStep <em>Initial Attack Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Initial Attack Step</em>'.
	 * @see dmt.InitialAttackStep
	 * @generated
	 */
	EClass getInitialAttackStep();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.InitialAttackStep#getHasAttackSteps <em>Has Attack Steps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Has Attack Steps</em>'.
	 * @see dmt.InitialAttackStep#getHasAttackSteps()
	 * @see #getInitialAttackStep()
	 * @generated
	 */
	EReference getInitialAttackStep_HasAttackSteps();

	/**
	 * Returns the meta object for the attribute '{@link dmt.InitialAttackStep#getPreCondition <em>Pre Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pre Condition</em>'.
	 * @see dmt.InitialAttackStep#getPreCondition()
	 * @see #getInitialAttackStep()
	 * @generated
	 */
	EAttribute getInitialAttackStep_PreCondition();

	/**
	 * Returns the meta object for class '{@link dmt.EndAttackStep <em>End Attack Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>End Attack Step</em>'.
	 * @see dmt.EndAttackStep
	 * @generated
	 */
	EClass getEndAttackStep();

	/**
	 * Returns the meta object for the attribute '{@link dmt.EndAttackStep#getExpectedResult <em>Expected Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expected Result</em>'.
	 * @see dmt.EndAttackStep#getExpectedResult()
	 * @see #getEndAttackStep()
	 * @generated
	 */
	EAttribute getEndAttackStep_ExpectedResult();

	/**
	 * Returns the meta object for class '{@link dmt.AttackStep <em>Attack Step</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attack Step</em>'.
	 * @see dmt.AttackStep
	 * @generated
	 */
	EClass getAttackStep();

	/**
	 * Returns the meta object for class '{@link dmt.AttackTree <em>Attack Tree</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attack Tree</em>'.
	 * @see dmt.AttackTree
	 * @generated
	 */
	EClass getAttackTree();

	/**
	 * Returns the meta object for the reference list '{@link dmt.AttackTree#getHasAttackvector <em>Has Attackvector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Has Attackvector</em>'.
	 * @see dmt.AttackTree#getHasAttackvector()
	 * @see #getAttackTree()
	 * @generated
	 */
	EReference getAttackTree_HasAttackvector();

	/**
	 * Returns the meta object for class '{@link dmt.GoalNode <em>Goal Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal Node</em>'.
	 * @see dmt.GoalNode
	 * @generated
	 */
	EClass getGoalNode();

	/**
	 * Returns the meta object for class '{@link dmt.SoftGoal <em>Soft Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Soft Goal</em>'.
	 * @see dmt.SoftGoal
	 * @generated
	 */
	EClass getSoftGoal();

	/**
	 * Returns the meta object for the attribute '{@link dmt.SoftGoal#getFitCriterion <em>Fit Criterion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fit Criterion</em>'.
	 * @see dmt.SoftGoal#getFitCriterion()
	 * @see #getSoftGoal()
	 * @generated
	 */
	EAttribute getSoftGoal_FitCriterion();

	/**
	 * Returns the meta object for the attribute '{@link dmt.SoftGoal#getSoftGoalType <em>Soft Goal Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Soft Goal Type</em>'.
	 * @see dmt.SoftGoal#getSoftGoalType()
	 * @see #getSoftGoal()
	 * @generated
	 */
	EAttribute getSoftGoal_SoftGoalType();

	/**
	 * Returns the meta object for the attribute '{@link dmt.SoftGoal#getSoftGoalOpenType <em>Soft Goal Open Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Soft Goal Open Type</em>'.
	 * @see dmt.SoftGoal#getSoftGoalOpenType()
	 * @see #getSoftGoal()
	 * @generated
	 */
	EAttribute getSoftGoal_SoftGoalOpenType();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionBehaviourGoal <em>Deception Behaviour Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Behaviour Goal</em>'.
	 * @see dmt.DeceptionBehaviourGoal
	 * @generated
	 */
	EClass getDeceptionBehaviourGoal();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionBehaviourGoal#getFormalSpec <em>Formal Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formal Spec</em>'.
	 * @see dmt.DeceptionBehaviourGoal#getFormalSpec()
	 * @see #getDeceptionBehaviourGoal()
	 * @generated
	 */
	EAttribute getDeceptionBehaviourGoal_FormalSpec();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionBehaviourGoal#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dmt.DeceptionBehaviourGoal#getType()
	 * @see #getDeceptionBehaviourGoal()
	 * @generated
	 */
	EAttribute getDeceptionBehaviourGoal_Type();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionSoftGoal <em>Deception Soft Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Soft Goal</em>'.
	 * @see dmt.DeceptionSoftGoal
	 * @generated
	 */
	EClass getDeceptionSoftGoal();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionSoftGoal#getFitCriterion <em>Fit Criterion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fit Criterion</em>'.
	 * @see dmt.DeceptionSoftGoal#getFitCriterion()
	 * @see #getDeceptionSoftGoal()
	 * @generated
	 */
	EAttribute getDeceptionSoftGoal_FitCriterion();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionSoftGoal#getSoftGoalType <em>Soft Goal Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Soft Goal Type</em>'.
	 * @see dmt.DeceptionSoftGoal#getSoftGoalType()
	 * @see #getDeceptionSoftGoal()
	 * @generated
	 */
	EAttribute getDeceptionSoftGoal_SoftGoalType();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionSoftGoal#getSoftGoalOpenType <em>Soft Goal Open Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Soft Goal Open Type</em>'.
	 * @see dmt.DeceptionSoftGoal#getSoftGoalOpenType()
	 * @see #getDeceptionSoftGoal()
	 * @generated
	 */
	EAttribute getDeceptionSoftGoal_SoftGoalOpenType();

	/**
	 * Returns the meta object for class '{@link dmt.ConcreteAsset <em>Concrete Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concrete Asset</em>'.
	 * @see dmt.ConcreteAsset
	 * @generated
	 */
	EClass getConcreteAsset();

	/**
	 * Returns the meta object for the attribute '{@link dmt.ConcreteAsset#getBusinessImportance <em>Business Importance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Business Importance</em>'.
	 * @see dmt.ConcreteAsset#getBusinessImportance()
	 * @see #getConcreteAsset()
	 * @generated
	 */
	EAttribute getConcreteAsset_BusinessImportance();

	/**
	 * Returns the meta object for class '{@link dmt.Operationable <em>Operationable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operationable</em>'.
	 * @see dmt.Operationable
	 * @generated
	 */
	EClass getOperationable();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Operationable#getDomPost <em>Dom Post</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dom Post</em>'.
	 * @see dmt.Operationable#getDomPost()
	 * @see #getOperationable()
	 * @generated
	 */
	EAttribute getOperationable_DomPost();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Operationable#getDomPre <em>Dom Pre</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dom Pre</em>'.
	 * @see dmt.Operationable#getDomPre()
	 * @see #getOperationable()
	 * @generated
	 */
	EAttribute getOperationable_DomPre();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Operationable#getFormalDomPost <em>Formal Dom Post</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formal Dom Post</em>'.
	 * @see dmt.Operationable#getFormalDomPost()
	 * @see #getOperationable()
	 * @generated
	 */
	EAttribute getOperationable_FormalDomPost();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Operationable#getFormalDomPre <em>Formal Dom Pre</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formal Dom Pre</em>'.
	 * @see dmt.Operationable#getFormalDomPre()
	 * @see #getOperationable()
	 * @generated
	 */
	EAttribute getOperationable_FormalDomPre();

	/**
	 * Returns the meta object for class '{@link dmt.GoalContainer <em>Goal Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal Container</em>'.
	 * @see dmt.GoalContainer
	 * @generated
	 */
	EClass getGoalContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link dmt.GoalContainer#getGoal <em>Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Goal</em>'.
	 * @see dmt.GoalContainer#getGoal()
	 * @see #getGoalContainer()
	 * @generated
	 */
	EReference getGoalContainer_Goal();

	/**
	 * Returns the meta object for class '{@link dmt.NodeSelection <em>Node Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Selection</em>'.
	 * @see dmt.NodeSelection
	 * @generated
	 */
	EClass getNodeSelection();

	/**
	 * Returns the meta object for the reference '{@link dmt.NodeSelection#getSelectionsource <em>Selectionsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Selectionsource</em>'.
	 * @see dmt.NodeSelection#getSelectionsource()
	 * @see #getNodeSelection()
	 * @generated
	 */
	EReference getNodeSelection_Selectionsource();

	/**
	 * Returns the meta object for the reference '{@link dmt.NodeSelection#getSelectiontarget <em>Selectiontarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Selectiontarget</em>'.
	 * @see dmt.NodeSelection#getSelectiontarget()
	 * @see #getNodeSelection()
	 * @generated
	 */
	EReference getNodeSelection_Selectiontarget();

	/**
	 * Returns the meta object for the attribute '{@link dmt.NodeSelection#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see dmt.NodeSelection#getType()
	 * @see #getNodeSelection()
	 * @generated
	 */
	EAttribute getNodeSelection_Type();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionTacticStrategy <em>Deception Tactic Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Tactic Strategy</em>'.
	 * @see dmt.DeceptionTacticStrategy
	 * @generated
	 */
	EClass getDeceptionTacticStrategy();

	/**
	 * Returns the meta object for the reference '{@link dmt.DeceptionTacticStrategy#getDeceptiontacticmechanism <em>Deceptiontacticmechanism</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Deceptiontacticmechanism</em>'.
	 * @see dmt.DeceptionTacticStrategy#getDeceptiontacticmechanism()
	 * @see #getDeceptionTacticStrategy()
	 * @generated
	 */
	EReference getDeceptionTacticStrategy_Deceptiontacticmechanism();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionStrategyTacticRefinement <em>Deception Strategy Tactic Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Strategy Tactic Refinement</em>'.
	 * @see dmt.DeceptionStrategyTacticRefinement
	 * @generated
	 */
	EClass getDeceptionStrategyTacticRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.DeceptionStrategyTacticRefinement#getStrategytacticrefinementsource <em>Strategytacticrefinementsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Strategytacticrefinementsource</em>'.
	 * @see dmt.DeceptionStrategyTacticRefinement#getStrategytacticrefinementsource()
	 * @see #getDeceptionStrategyTacticRefinement()
	 * @generated
	 */
	EReference getDeceptionStrategyTacticRefinement_Strategytacticrefinementsource();

	/**
	 * Returns the meta object for the reference '{@link dmt.DeceptionStrategyTacticRefinement#getStrategytacticrefinementtarget <em>Strategytacticrefinementtarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Strategytacticrefinementtarget</em>'.
	 * @see dmt.DeceptionStrategyTacticRefinement#getStrategytacticrefinementtarget()
	 * @see #getDeceptionStrategyTacticRefinement()
	 * @generated
	 */
	EReference getDeceptionStrategyTacticRefinement_Strategytacticrefinementtarget();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionTacticStrategyRefinement <em>Deception Tactic Strategy Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Tactic Strategy Refinement</em>'.
	 * @see dmt.DeceptionTacticStrategyRefinement
	 * @generated
	 */
	EClass getDeceptionTacticStrategyRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.DeceptionTacticStrategyRefinement#getTacticstrategytacticrefinementsource <em>Tacticstrategytacticrefinementsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tacticstrategytacticrefinementsource</em>'.
	 * @see dmt.DeceptionTacticStrategyRefinement#getTacticstrategytacticrefinementsource()
	 * @see #getDeceptionTacticStrategyRefinement()
	 * @generated
	 */
	EReference getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementsource();

	/**
	 * Returns the meta object for the reference '{@link dmt.DeceptionTacticStrategyRefinement#getTacticstrategytacticrefinementtarget <em>Tacticstrategytacticrefinementtarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tacticstrategytacticrefinementtarget</em>'.
	 * @see dmt.DeceptionTacticStrategyRefinement#getTacticstrategytacticrefinementtarget()
	 * @see #getDeceptionTacticStrategyRefinement()
	 * @generated
	 */
	EReference getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementtarget();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionStrategyRefinement <em>Deception Strategy Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Strategy Refinement</em>'.
	 * @see dmt.DeceptionStrategyRefinement
	 * @generated
	 */
	EClass getDeceptionStrategyRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.DeceptionStrategyRefinement#getDeceptionstrategyrefinementsource <em>Deceptionstrategyrefinementsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Deceptionstrategyrefinementsource</em>'.
	 * @see dmt.DeceptionStrategyRefinement#getDeceptionstrategyrefinementsource()
	 * @see #getDeceptionStrategyRefinement()
	 * @generated
	 */
	EReference getDeceptionStrategyRefinement_Deceptionstrategyrefinementsource();

	/**
	 * Returns the meta object for the reference '{@link dmt.DeceptionStrategyRefinement#getDeceptionstrategyrefinementtarget <em>Deceptionstrategyrefinementtarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Deceptionstrategyrefinementtarget</em>'.
	 * @see dmt.DeceptionStrategyRefinement#getDeceptionstrategyrefinementtarget()
	 * @see #getDeceptionStrategyRefinement()
	 * @generated
	 */
	EReference getDeceptionStrategyRefinement_Deceptionstrategyrefinementtarget();

	/**
	 * Returns the meta object for class '{@link dmt.SimulationHasArtificialElement <em>Simulation Has Artificial Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simulation Has Artificial Element</em>'.
	 * @see dmt.SimulationHasArtificialElement
	 * @generated
	 */
	EClass getSimulationHasArtificialElement();

	/**
	 * Returns the meta object for the reference '{@link dmt.SimulationHasArtificialElement#getShowFalseAsset <em>Show False Asset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Show False Asset</em>'.
	 * @see dmt.SimulationHasArtificialElement#getShowFalseAsset()
	 * @see #getSimulationHasArtificialElement()
	 * @generated
	 */
	EReference getSimulationHasArtificialElement_ShowFalseAsset();

	/**
	 * Returns the meta object for the attribute '{@link dmt.SimulationHasArtificialElement#getLowerFakes <em>Lower Fakes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Fakes</em>'.
	 * @see dmt.SimulationHasArtificialElement#getLowerFakes()
	 * @see #getSimulationHasArtificialElement()
	 * @generated
	 */
	EAttribute getSimulationHasArtificialElement_LowerFakes();

	/**
	 * Returns the meta object for the attribute '{@link dmt.SimulationHasArtificialElement#getUpperFakes <em>Upper Fakes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Fakes</em>'.
	 * @see dmt.SimulationHasArtificialElement#getUpperFakes()
	 * @see #getSimulationHasArtificialElement()
	 * @generated
	 */
	EAttribute getSimulationHasArtificialElement_UpperFakes();

	/**
	 * Returns the meta object for the reference '{@link dmt.SimulationHasArtificialElement#getSimulationType <em>Simulation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Simulation Type</em>'.
	 * @see dmt.SimulationHasArtificialElement#getSimulationType()
	 * @see #getSimulationHasArtificialElement()
	 * @generated
	 */
	EReference getSimulationHasArtificialElement_SimulationType();

	/**
	 * Returns the meta object for class '{@link dmt.Property <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see dmt.Property
	 * @generated
	 */
	EClass getProperty();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Property#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see dmt.Property#getValue()
	 * @see #getProperty()
	 * @generated
	 */
	EAttribute getProperty_Value();

	/**
	 * Returns the meta object for class '{@link dmt.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see dmt.Annotation
	 * @generated
	 */
	EClass getAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Annotation#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see dmt.Annotation#getText()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_Text();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionGoal <em>Deception Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Goal</em>'.
	 * @see dmt.DeceptionGoal
	 * @generated
	 */
	EClass getDeceptionGoal();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionGoal#getDeceptionGoalType <em>Deception Goal Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deception Goal Type</em>'.
	 * @see dmt.DeceptionGoal#getDeceptionGoalType()
	 * @see #getDeceptionGoal()
	 * @generated
	 */
	EAttribute getDeceptionGoal_DeceptionGoalType();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionRisk <em>Deception Risk</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Risk</em>'.
	 * @see dmt.DeceptionRisk
	 * @generated
	 */
	EClass getDeceptionRisk();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionRisk#getDeceptionRiskType <em>Deception Risk Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deception Risk Type</em>'.
	 * @see dmt.DeceptionRisk#getDeceptionRiskType()
	 * @see #getDeceptionRisk()
	 * @generated
	 */
	EAttribute getDeceptionRisk_DeceptionRiskType();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionStrategy <em>Deception Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Strategy</em>'.
	 * @see dmt.DeceptionStrategy
	 * @generated
	 */
	EClass getDeceptionStrategy();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionStrategy#isIsRoot <em>Is Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Root</em>'.
	 * @see dmt.DeceptionStrategy#isIsRoot()
	 * @see #getDeceptionStrategy()
	 * @generated
	 */
	EAttribute getDeceptionStrategy_IsRoot();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionStrategy#getExecutionParameter <em>Execution Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Parameter</em>'.
	 * @see dmt.DeceptionStrategy#getExecutionParameter()
	 * @see #getDeceptionStrategy()
	 * @generated
	 */
	EAttribute getDeceptionStrategy_ExecutionParameter();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionTacticMechanism <em>Deception Tactic Mechanism</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Tactic Mechanism</em>'.
	 * @see dmt.DeceptionTacticMechanism
	 * @generated
	 */
	EClass getDeceptionTacticMechanism();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionTacticMechanism#getEnablingCondition <em>Enabling Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enabling Condition</em>'.
	 * @see dmt.DeceptionTacticMechanism#getEnablingCondition()
	 * @see #getDeceptionTacticMechanism()
	 * @generated
	 */
	EAttribute getDeceptionTacticMechanism_EnablingCondition();

	/**
	 * Returns the meta object for the reference '{@link dmt.DeceptionTacticMechanism#getTacticReference <em>Tactic Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tactic Reference</em>'.
	 * @see dmt.DeceptionTacticMechanism#getTacticReference()
	 * @see #getDeceptionTacticMechanism()
	 * @generated
	 */
	EReference getDeceptionTacticMechanism_TacticReference();

	/**
	 * Returns the meta object for class '{@link dmt.StrategyNode <em>Strategy Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Strategy Node</em>'.
	 * @see dmt.StrategyNode
	 * @generated
	 */
	EClass getStrategyNode();

	/**
	 * Returns the meta object for the reference list '{@link dmt.StrategyNode#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Property</em>'.
	 * @see dmt.StrategyNode#getProperty()
	 * @see #getStrategyNode()
	 * @generated
	 */
	EReference getStrategyNode_Property();

	/**
	 * Returns the meta object for class '{@link dmt.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature</em>'.
	 * @see dmt.Feature
	 * @generated
	 */
	EClass getFeature();

	/**
	 * Returns the meta object for class '{@link dmt.StrategyRelationship <em>Strategy Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Strategy Relationship</em>'.
	 * @see dmt.StrategyRelationship
	 * @generated
	 */
	EClass getStrategyRelationship();

	/**
	 * Returns the meta object for the attribute '{@link dmt.StrategyRelationship#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see dmt.StrategyRelationship#getDescription()
	 * @see #getStrategyRelationship()
	 * @generated
	 */
	EAttribute getStrategyRelationship_Description();

	/**
	 * Returns the meta object for class '{@link dmt.FeatureConstraint <em>Feature Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Constraint</em>'.
	 * @see dmt.FeatureConstraint
	 * @generated
	 */
	EClass getFeatureConstraint();

	/**
	 * Returns the meta object for the reference '{@link dmt.FeatureConstraint#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see dmt.FeatureConstraint#getSource()
	 * @see #getFeatureConstraint()
	 * @generated
	 */
	EReference getFeatureConstraint_Source();

	/**
	 * Returns the meta object for the reference '{@link dmt.FeatureConstraint#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see dmt.FeatureConstraint#getTarget()
	 * @see #getFeatureConstraint()
	 * @generated
	 */
	EReference getFeatureConstraint_Target();

	/**
	 * Returns the meta object for class '{@link dmt.AbstractFeature <em>Abstract Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Feature</em>'.
	 * @see dmt.AbstractFeature
	 * @generated
	 */
	EClass getAbstractFeature();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AbstractFeature#getBindingState <em>Binding State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Binding State</em>'.
	 * @see dmt.AbstractFeature#getBindingState()
	 * @see #getAbstractFeature()
	 * @generated
	 */
	EAttribute getAbstractFeature_BindingState();

	/**
	 * Returns the meta object for the attribute '{@link dmt.AbstractFeature#isIsAbstract <em>Is Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Abstract</em>'.
	 * @see dmt.AbstractFeature#isIsAbstract()
	 * @see #getAbstractFeature()
	 * @generated
	 */
	EAttribute getAbstractFeature_IsAbstract();

	/**
	 * Returns the meta object for class '{@link dmt.RequiredConstraint <em>Required Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Constraint</em>'.
	 * @see dmt.RequiredConstraint
	 * @generated
	 */
	EClass getRequiredConstraint();

	/**
	 * Returns the meta object for class '{@link dmt.ExcludeConstraint <em>Exclude Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exclude Constraint</em>'.
	 * @see dmt.ExcludeConstraint
	 * @generated
	 */
	EClass getExcludeConstraint();

	/**
	 * Returns the meta object for class '{@link dmt.BenefitConstraint <em>Benefit Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Benefit Constraint</em>'.
	 * @see dmt.BenefitConstraint
	 * @generated
	 */
	EClass getBenefitConstraint();

	/**
	 * Returns the meta object for class '{@link dmt.AvoidConstraint <em>Avoid Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Avoid Constraint</em>'.
	 * @see dmt.AvoidConstraint
	 * @generated
	 */
	EClass getAvoidConstraint();

	/**
	 * Returns the meta object for class '{@link dmt.NodeRefinement <em>Node Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Refinement</em>'.
	 * @see dmt.NodeRefinement
	 * @generated
	 */
	EClass getNodeRefinement();

	/**
	 * Returns the meta object for the attribute '{@link dmt.NodeRefinement#getEnactmentChoice <em>Enactment Choice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enactment Choice</em>'.
	 * @see dmt.NodeRefinement#getEnactmentChoice()
	 * @see #getNodeRefinement()
	 * @generated
	 */
	EAttribute getNodeRefinement_EnactmentChoice();

	/**
	 * Returns the meta object for class '{@link dmt.TacticRefinement <em>Tactic Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tactic Refinement</em>'.
	 * @see dmt.TacticRefinement
	 * @generated
	 */
	EClass getTacticRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.TacticRefinement#getTacticrefinementtarget <em>Tacticrefinementtarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tacticrefinementtarget</em>'.
	 * @see dmt.TacticRefinement#getTacticrefinementtarget()
	 * @see #getTacticRefinement()
	 * @generated
	 */
	EReference getTacticRefinement_Tacticrefinementtarget();

	/**
	 * Returns the meta object for the reference '{@link dmt.TacticRefinement#getTacticrefinementsource <em>Tacticrefinementsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tacticrefinementsource</em>'.
	 * @see dmt.TacticRefinement#getTacticrefinementsource()
	 * @see #getTacticRefinement()
	 * @generated
	 */
	EReference getTacticRefinement_Tacticrefinementsource();

	/**
	 * Returns the meta object for class '{@link dmt.FeatureRefinement <em>Feature Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Refinement</em>'.
	 * @see dmt.FeatureRefinement
	 * @generated
	 */
	EClass getFeatureRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.FeatureRefinement#getFeaturerefinementtarget <em>Featurerefinementtarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Featurerefinementtarget</em>'.
	 * @see dmt.FeatureRefinement#getFeaturerefinementtarget()
	 * @see #getFeatureRefinement()
	 * @generated
	 */
	EReference getFeatureRefinement_Featurerefinementtarget();

	/**
	 * Returns the meta object for the reference '{@link dmt.FeatureRefinement#getFeaturerefinementsource <em>Featurerefinementsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Featurerefinementsource</em>'.
	 * @see dmt.FeatureRefinement#getFeaturerefinementsource()
	 * @see #getFeatureRefinement()
	 * @generated
	 */
	EReference getFeatureRefinement_Featurerefinementsource();

	/**
	 * Returns the meta object for class '{@link dmt.StrategyTacticRefinement <em>Strategy Tactic Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Strategy Tactic Refinement</em>'.
	 * @see dmt.StrategyTacticRefinement
	 * @generated
	 */
	EClass getStrategyTacticRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.StrategyTacticRefinement#getStrategytacticrefinementtarget <em>Strategytacticrefinementtarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Strategytacticrefinementtarget</em>'.
	 * @see dmt.StrategyTacticRefinement#getStrategytacticrefinementtarget()
	 * @see #getStrategyTacticRefinement()
	 * @generated
	 */
	EReference getStrategyTacticRefinement_Strategytacticrefinementtarget();

	/**
	 * Returns the meta object for the reference '{@link dmt.StrategyTacticRefinement#getStrategytacticrefinementsource <em>Strategytacticrefinementsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Strategytacticrefinementsource</em>'.
	 * @see dmt.StrategyTacticRefinement#getStrategytacticrefinementsource()
	 * @see #getStrategyTacticRefinement()
	 * @generated
	 */
	EReference getStrategyTacticRefinement_Strategytacticrefinementsource();

	/**
	 * Returns the meta object for class '{@link dmt.TacticFeatureRefinement <em>Tactic Feature Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tactic Feature Refinement</em>'.
	 * @see dmt.TacticFeatureRefinement
	 * @generated
	 */
	EClass getTacticFeatureRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.TacticFeatureRefinement#getTacticfeaturerefinementtarget <em>Tacticfeaturerefinementtarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tacticfeaturerefinementtarget</em>'.
	 * @see dmt.TacticFeatureRefinement#getTacticfeaturerefinementtarget()
	 * @see #getTacticFeatureRefinement()
	 * @generated
	 */
	EReference getTacticFeatureRefinement_Tacticfeaturerefinementtarget();

	/**
	 * Returns the meta object for the reference '{@link dmt.TacticFeatureRefinement#getTacticfeaturerefinementsource <em>Tacticfeaturerefinementsource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Tacticfeaturerefinementsource</em>'.
	 * @see dmt.TacticFeatureRefinement#getTacticfeaturerefinementsource()
	 * @see #getTacticFeatureRefinement()
	 * @generated
	 */
	EReference getTacticFeatureRefinement_Tacticfeaturerefinementsource();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionRelationship <em>Deception Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Relationship</em>'.
	 * @see dmt.DeceptionRelationship
	 * @generated
	 */
	EClass getDeceptionRelationship();

	/**
	 * Returns the meta object for the attribute '{@link dmt.DeceptionRelationship#getAltName <em>Alt Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Alt Name</em>'.
	 * @see dmt.DeceptionRelationship#getAltName()
	 * @see #getDeceptionRelationship()
	 * @generated
	 */
	EAttribute getDeceptionRelationship_AltName();

	/**
	 * Returns the meta object for class '{@link dmt.Mitigator <em>Mitigator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Mitigator</em>'.
	 * @see dmt.Mitigator
	 * @generated
	 */
	EClass getMitigator();

	/**
	 * Returns the meta object for class '{@link dmt.SecurityRelationship <em>Security Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Security Relationship</em>'.
	 * @see dmt.SecurityRelationship
	 * @generated
	 */
	EClass getSecurityRelationship();

	/**
	 * Returns the meta object for class '{@link dmt.ObstructionElement <em>Obstruction Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Obstruction Element</em>'.
	 * @see dmt.ObstructionElement
	 * @generated
	 */
	EClass getObstructionElement();

	/**
	 * Returns the meta object for the reference list '{@link dmt.ObstructionElement#getObstruct <em>Obstruct</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Obstruct</em>'.
	 * @see dmt.ObstructionElement#getObstruct()
	 * @see #getObstructionElement()
	 * @generated
	 */
	EReference getObstructionElement_Obstruct();

	/**
	 * Returns the meta object for the reference list '{@link dmt.ObstructionElement#getORObstructionRefined <em>OR Obstruction Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>OR Obstruction Refined</em>'.
	 * @see dmt.ObstructionElement#getORObstructionRefined()
	 * @see #getObstructionElement()
	 * @generated
	 */
	EReference getObstructionElement_ORObstructionRefined();

	/**
	 * Returns the meta object for the reference list '{@link dmt.ObstructionElement#getANDObstructionRefined <em>AND Obstruction Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AND Obstruction Refined</em>'.
	 * @see dmt.ObstructionElement#getANDObstructionRefined()
	 * @see #getObstructionElement()
	 * @generated
	 */
	EReference getObstructionElement_ANDObstructionRefined();

	/**
	 * Returns the meta object for class '{@link dmt.ORObstructionRefinement <em>OR Obstruction Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OR Obstruction Refinement</em>'.
	 * @see dmt.ORObstructionRefinement
	 * @generated
	 */
	EClass getORObstructionRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.ORObstructionRefinement#getORObstructionRefine <em>OR Obstruction Refine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>OR Obstruction Refine</em>'.
	 * @see dmt.ORObstructionRefinement#getORObstructionRefine()
	 * @see #getORObstructionRefinement()
	 * @generated
	 */
	EReference getORObstructionRefinement_ORObstructionRefine();

	/**
	 * Returns the meta object for class '{@link dmt.ANDObstructionRefinement <em>AND Obstruction Refinement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AND Obstruction Refinement</em>'.
	 * @see dmt.ANDObstructionRefinement
	 * @generated
	 */
	EClass getANDObstructionRefinement();

	/**
	 * Returns the meta object for the reference '{@link dmt.ANDObstructionRefinement#getANDObstructionRefine <em>AND Obstruction Refine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AND Obstruction Refine</em>'.
	 * @see dmt.ANDObstructionRefinement#getANDObstructionRefine()
	 * @see #getANDObstructionRefinement()
	 * @generated
	 */
	EReference getANDObstructionRefinement_ANDObstructionRefine();

	/**
	 * Returns the meta object for class '{@link dmt.Obstacle <em>Obstacle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Obstacle</em>'.
	 * @see dmt.Obstacle
	 * @generated
	 */
	EClass getObstacle();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Obstacle#getObstacleCategory <em>Obstacle Category</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Obstacle Category</em>'.
	 * @see dmt.Obstacle#getObstacleCategory()
	 * @see #getObstacle()
	 * @generated
	 */
	EAttribute getObstacle_ObstacleCategory();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Obstacle#getFormalDef <em>Formal Def</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formal Def</em>'.
	 * @see dmt.Obstacle#getFormalDef()
	 * @see #getObstacle()
	 * @generated
	 */
	EAttribute getObstacle_FormalDef();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Obstacle#getLikelihood <em>Likelihood</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Likelihood</em>'.
	 * @see dmt.Obstacle#getLikelihood()
	 * @see #getObstacle()
	 * @generated
	 */
	EAttribute getObstacle_Likelihood();

	/**
	 * Returns the meta object for class '{@link dmt.InfluenceNode <em>Influence Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Influence Node</em>'.
	 * @see dmt.InfluenceNode
	 * @generated
	 */
	EClass getInfluenceNode();

	/**
	 * Returns the meta object for class '{@link dmt.QualityAttribute <em>Quality Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quality Attribute</em>'.
	 * @see dmt.QualityAttribute
	 * @generated
	 */
	EClass getQualityAttribute();

	/**
	 * Returns the meta object for class '{@link dmt.InfluenceRelationship <em>Influence Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Influence Relationship</em>'.
	 * @see dmt.InfluenceRelationship
	 * @generated
	 */
	EClass getInfluenceRelationship();

	/**
	 * Returns the meta object for class '{@link dmt.Influence <em>Influence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Influence</em>'.
	 * @see dmt.Influence
	 * @generated
	 */
	EClass getInfluence();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Influence#getWeight <em>Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Weight</em>'.
	 * @see dmt.Influence#getWeight()
	 * @see #getInfluence()
	 * @generated
	 */
	EAttribute getInfluence_Weight();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Influence#getPolarity <em>Polarity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Polarity</em>'.
	 * @see dmt.Influence#getPolarity()
	 * @see #getInfluence()
	 * @generated
	 */
	EAttribute getInfluence_Polarity();

	/**
	 * Returns the meta object for the reference '{@link dmt.Influence#getInfluenceSource <em>Influence Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Influence Source</em>'.
	 * @see dmt.Influence#getInfluenceSource()
	 * @see #getInfluence()
	 * @generated
	 */
	EReference getInfluence_InfluenceSource();

	/**
	 * Returns the meta object for the reference '{@link dmt.Influence#getInfluencetarget <em>Influencetarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Influencetarget</em>'.
	 * @see dmt.Influence#getInfluencetarget()
	 * @see #getInfluence()
	 * @generated
	 */
	EReference getInfluence_Influencetarget();

	/**
	 * Returns the meta object for class '{@link dmt.Preference <em>Preference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Preference</em>'.
	 * @see dmt.Preference
	 * @generated
	 */
	EClass getPreference();

	/**
	 * Returns the meta object for the reference '{@link dmt.Preference#getPreferenceTarget <em>Preference Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Preference Target</em>'.
	 * @see dmt.Preference#getPreferenceTarget()
	 * @see #getPreference()
	 * @generated
	 */
	EReference getPreference_PreferenceTarget();

	/**
	 * Returns the meta object for the reference '{@link dmt.Preference#getPreferenceSource <em>Preference Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Preference Source</em>'.
	 * @see dmt.Preference#getPreferenceSource()
	 * @see #getPreference()
	 * @generated
	 */
	EReference getPreference_PreferenceSource();

	/**
	 * Returns the meta object for the attribute '{@link dmt.Preference#getWeight <em>Weight</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Weight</em>'.
	 * @see dmt.Preference#getWeight()
	 * @see #getPreference()
	 * @generated
	 */
	EAttribute getPreference_Weight();

	/**
	 * Returns the meta object for class '{@link dmt.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see dmt.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for class '{@link dmt.DeceptionTacticEvent <em>Deception Tactic Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deception Tactic Event</em>'.
	 * @see dmt.DeceptionTacticEvent
	 * @generated
	 */
	EClass getDeceptionTacticEvent();

	/**
	 * Returns the meta object for class '{@link dmt.EnvironmentEvent <em>Environment Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment Event</em>'.
	 * @see dmt.EnvironmentEvent
	 * @generated
	 */
	EClass getEnvironmentEvent();

	/**
	 * Returns the meta object for class '{@link dmt.ObstructedElement <em>Obstructed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Obstructed Element</em>'.
	 * @see dmt.ObstructedElement
	 * @generated
	 */
	EClass getObstructedElement();

	/**
	 * Returns the meta object for class '{@link dmt.Vulnerable <em>Vulnerable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vulnerable</em>'.
	 * @see dmt.Vulnerable
	 * @generated
	 */
	EClass getVulnerable();

	/**
	 * Returns the meta object for the reference list '{@link dmt.Vulnerable#getVulnerability <em>Vulnerability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Vulnerability</em>'.
	 * @see dmt.Vulnerable#getVulnerability()
	 * @see #getVulnerable()
	 * @generated
	 */
	EReference getVulnerable_Vulnerability();

	/**
	 * Returns the meta object for class '{@link dmt.GoalRefinable <em>Goal Refinable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal Refinable</em>'.
	 * @see dmt.GoalRefinable
	 * @generated
	 */
	EClass getGoalRefinable();

	/**
	 * Returns the meta object for the reference list '{@link dmt.GoalRefinable#getOrGoalRefined <em>Or Goal Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Or Goal Refined</em>'.
	 * @see dmt.GoalRefinable#getOrGoalRefined()
	 * @see #getGoalRefinable()
	 * @generated
	 */
	EReference getGoalRefinable_OrGoalRefined();

	/**
	 * Returns the meta object for the reference list '{@link dmt.GoalRefinable#getAndGoalRefined <em>And Goal Refined</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>And Goal Refined</em>'.
	 * @see dmt.GoalRefinable#getAndGoalRefined()
	 * @see #getGoalRefinable()
	 * @generated
	 */
	EReference getGoalRefinable_AndGoalRefined();

	/**
	 * Returns the meta object for class '{@link dmt.Belief <em>Belief</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Belief</em>'.
	 * @see dmt.Belief
	 * @generated
	 */
	EClass getBelief();

	/**
	 * Returns the meta object for enum '{@link dmt.GoalCategoryEnum <em>Goal Category Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Goal Category Enum</em>'.
	 * @see dmt.GoalCategoryEnum
	 * @generated
	 */
	EEnum getGoalCategoryEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.DomainPropertyCategoryEnum <em>Domain Property Category Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Domain Property Category Enum</em>'.
	 * @see dmt.DomainPropertyCategoryEnum
	 * @generated
	 */
	EEnum getDomainPropertyCategoryEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.GoalPriorityEnum <em>Goal Priority Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Goal Priority Enum</em>'.
	 * @see dmt.GoalPriorityEnum
	 * @generated
	 */
	EEnum getGoalPriorityEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.ObstacleCategoryEnum <em>Obstacle Category Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Obstacle Category Enum</em>'.
	 * @see dmt.ObstacleCategoryEnum
	 * @generated
	 */
	EEnum getObstacleCategoryEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.PreferenceWeightEnum <em>Preference Weight Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Preference Weight Enum</em>'.
	 * @see dmt.PreferenceWeightEnum
	 * @generated
	 */
	EEnum getPreferenceWeightEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.InfluenceWeightEnum <em>Influence Weight Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Influence Weight Enum</em>'.
	 * @see dmt.InfluenceWeightEnum
	 * @generated
	 */
	EEnum getInfluenceWeightEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.InfluencePolarityEnum <em>Influence Polarity Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Influence Polarity Enum</em>'.
	 * @see dmt.InfluencePolarityEnum
	 * @generated
	 */
	EEnum getInfluencePolarityEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.DeceptionGoalTypeEnum <em>Deception Goal Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Deception Goal Type Enum</em>'.
	 * @see dmt.DeceptionGoalTypeEnum
	 * @generated
	 */
	EEnum getDeceptionGoalTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.SoftGoalTypeEnum <em>Soft Goal Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Soft Goal Type Enum</em>'.
	 * @see dmt.SoftGoalTypeEnum
	 * @generated
	 */
	EEnum getSoftGoalTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.NodesSelectionType <em>Nodes Selection Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Nodes Selection Type</em>'.
	 * @see dmt.NodesSelectionType
	 * @generated
	 */
	EEnum getNodesSelectionType();

	/**
	 * Returns the meta object for enum '{@link dmt.ChannelTypeEnum <em>Channel Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Channel Type Enum</em>'.
	 * @see dmt.ChannelTypeEnum
	 * @generated
	 */
	EEnum getChannelTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.MitigationLevelEnum <em>Mitigation Level Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Mitigation Level Enum</em>'.
	 * @see dmt.MitigationLevelEnum
	 * @generated
	 */
	EEnum getMitigationLevelEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.AssetTypeEnum <em>Asset Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Asset Type Enum</em>'.
	 * @see dmt.AssetTypeEnum
	 * @generated
	 */
	EEnum getAssetTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.AttackTypeEnum <em>Attack Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attack Type Enum</em>'.
	 * @see dmt.AttackTypeEnum
	 * @generated
	 */
	EEnum getAttackTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.AttackerProfileEnum <em>Attacker Profile Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attacker Profile Enum</em>'.
	 * @see dmt.AttackerProfileEnum
	 * @generated
	 */
	EEnum getAttackerProfileEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.RiskLikelihoodEnum <em>Risk Likelihood Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Risk Likelihood Enum</em>'.
	 * @see dmt.RiskLikelihoodEnum
	 * @generated
	 */
	EEnum getRiskLikelihoodEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.RiskSeverityEnum <em>Risk Severity Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Risk Severity Enum</em>'.
	 * @see dmt.RiskSeverityEnum
	 * @generated
	 */
	EEnum getRiskSeverityEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.SecurityControlIncidentEnum <em>Security Control Incident Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Security Control Incident Enum</em>'.
	 * @see dmt.SecurityControlIncidentEnum
	 * @generated
	 */
	EEnum getSecurityControlIncidentEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.ThreatCategoryEnum <em>Threat Category Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Threat Category Enum</em>'.
	 * @see dmt.ThreatCategoryEnum
	 * @generated
	 */
	EEnum getThreatCategoryEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.VulnerabilityTypeEnum <em>Vulnerability Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Vulnerability Type Enum</em>'.
	 * @see dmt.VulnerabilityTypeEnum
	 * @generated
	 */
	EEnum getVulnerabilityTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.SecurityControlNatureEnum <em>Security Control Nature Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Security Control Nature Enum</em>'.
	 * @see dmt.SecurityControlNatureEnum
	 * @generated
	 */
	EEnum getSecurityControlNatureEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.BiasTypeEnum <em>Bias Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Bias Type Enum</em>'.
	 * @see dmt.BiasTypeEnum
	 * @generated
	 */
	EEnum getBiasTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.SimulationTechniqueTypeEnum <em>Simulation Technique Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Simulation Technique Type Enum</em>'.
	 * @see dmt.SimulationTechniqueTypeEnum
	 * @generated
	 */
	EEnum getSimulationTechniqueTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.MetricTypeEnum <em>Metric Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Metric Type Enum</em>'.
	 * @see dmt.MetricTypeEnum
	 * @generated
	 */
	EEnum getMetricTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.DissimulationTechniqueTypeEnum <em>Dissimulation Technique Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Dissimulation Technique Type Enum</em>'.
	 * @see dmt.DissimulationTechniqueTypeEnum
	 * @generated
	 */
	EEnum getDissimulationTechniqueTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.ArtificialElementTypeEnum <em>Artificial Element Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Artificial Element Type Enum</em>'.
	 * @see dmt.ArtificialElementTypeEnum
	 * @generated
	 */
	EEnum getArtificialElementTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.GoalTypeEnum <em>Goal Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Goal Type Enum</em>'.
	 * @see dmt.GoalTypeEnum
	 * @generated
	 */
	EEnum getGoalTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.GoalStabilityEnum <em>Goal Stability Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Goal Stability Enum</em>'.
	 * @see dmt.GoalStabilityEnum
	 * @generated
	 */
	EEnum getGoalStabilityEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.EnvironmentAgentTypeEnum <em>Environment Agent Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Environment Agent Type Enum</em>'.
	 * @see dmt.EnvironmentAgentTypeEnum
	 * @generated
	 */
	EEnum getEnvironmentAgentTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.AttackOriginEnum <em>Attack Origin Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attack Origin Enum</em>'.
	 * @see dmt.AttackOriginEnum
	 * @generated
	 */
	EEnum getAttackOriginEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.AttackConnectorEnum <em>Attack Connector Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Attack Connector Enum</em>'.
	 * @see dmt.AttackConnectorEnum
	 * @generated
	 */
	EEnum getAttackConnectorEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.StoryTypeEnum <em>Story Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Story Type Enum</em>'.
	 * @see dmt.StoryTypeEnum
	 * @generated
	 */
	EEnum getStoryTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.DeceptionRiskTypeEnum <em>Deception Risk Type Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Deception Risk Type Enum</em>'.
	 * @see dmt.DeceptionRiskTypeEnum
	 * @generated
	 */
	EEnum getDeceptionRiskTypeEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.BindingStateEnum <em>Binding State Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Binding State Enum</em>'.
	 * @see dmt.BindingStateEnum
	 * @generated
	 */
	EEnum getBindingStateEnum();

	/**
	 * Returns the meta object for enum '{@link dmt.EnactmentEnum <em>Enactment Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Enactment Enum</em>'.
	 * @see dmt.EnactmentEnum
	 * @generated
	 */
	EEnum getEnactmentEnum();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DmtFactory getDmtFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link dmt.impl.DMTModelImpl <em>DMT Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DMTModelImpl
		 * @see dmt.impl.DmtPackageImpl#getDMTModel()
		 * @generated
		 */
		EClass DMT_MODEL = eINSTANCE.getDMTModel();

		/**
		 * The meta object literal for the '<em><b>Has Security Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_SECURITY_NODES = eINSTANCE.getDMTModel_HasSecurityNodes();

		/**
		 * The meta object literal for the '<em><b>Has Deception Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_DECEPTION_NODES = eINSTANCE.getDMTModel_HasDeceptionNodes();

		/**
		 * The meta object literal for the '<em><b>Has Goal Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_GOAL_NODES = eINSTANCE.getDMTModel_HasGoalNodes();

		/**
		 * The meta object literal for the '<em><b>Has Goal Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_GOAL_RELATIONS = eINSTANCE.getDMTModel_HasGoalRelations();

		/**
		 * The meta object literal for the '<em><b>Has Strategy Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_STRATEGY_NODES = eINSTANCE.getDMTModel_HasStrategyNodes();

		/**
		 * The meta object literal for the '<em><b>Has Strategy Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_STRATEGY_RELATIONS = eINSTANCE.getDMTModel_HasStrategyRelations();

		/**
		 * The meta object literal for the '<em><b>Has Deception Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_DECEPTION_RELATIONS = eINSTANCE.getDMTModel_HasDeceptionRelations();

		/**
		 * The meta object literal for the '<em><b>Has Security Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_SECURITY_RELATIONS = eINSTANCE.getDMTModel_HasSecurityRelations();

		/**
		 * The meta object literal for the '<em><b>Has Influence Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_INFLUENCE_NODES = eINSTANCE.getDMTModel_HasInfluenceNodes();

		/**
		 * The meta object literal for the '<em><b>Has Influence Relations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_INFLUENCE_RELATIONS = eINSTANCE.getDMTModel_HasInfluenceRelations();

		/**
		 * The meta object literal for the '<em><b>Has Goal Containers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_GOAL_CONTAINERS = eINSTANCE.getDMTModel_HasGoalContainers();

		/**
		 * The meta object literal for the '<em><b>Has Misc Nodes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DMT_MODEL__HAS_MISC_NODES = eINSTANCE.getDMTModel_HasMiscNodes();

		/**
		 * The meta object literal for the '{@link dmt.impl.GoalImpl <em>Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.GoalImpl
		 * @see dmt.impl.DmtPackageImpl#getGoal()
		 * @generated
		 */
		EClass GOAL = eINSTANCE.getGoal();

		/**
		 * The meta object literal for the '<em><b>Resolution</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL__RESOLUTION = eINSTANCE.getGoal_Resolution();

		/**
		 * The meta object literal for the '{@link dmt.impl.GoalRelationshipImpl <em>Goal Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.GoalRelationshipImpl
		 * @see dmt.impl.DmtPackageImpl#getGoalRelationship()
		 * @generated
		 */
		EClass GOAL_RELATIONSHIP = eINSTANCE.getGoalRelationship();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL_RELATIONSHIP__STATUS = eINSTANCE.getGoalRelationship_Status();

		/**
		 * The meta object literal for the '<em><b>Tactic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL_RELATIONSHIP__TACTIC = eINSTANCE.getGoalRelationship_Tactic();

		/**
		 * The meta object literal for the '<em><b>Sys Ref</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL_RELATIONSHIP__SYS_REF = eINSTANCE.getGoalRelationship_SysRef();

		/**
		 * The meta object literal for the '<em><b>Alt Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL_RELATIONSHIP__ALT_NAME = eINSTANCE.getGoalRelationship_AltName();

		/**
		 * The meta object literal for the '<em><b>Goal Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL__GOAL_CATEGORY = eINSTANCE.getGoal_GoalCategory();

		/**
		 * The meta object literal for the '<em><b>Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL__PRIORITY = eINSTANCE.getGoal_Priority();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL__SOURCE = eINSTANCE.getGoal_Source();

		/**
		 * The meta object literal for the '<em><b>Stability</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL__STABILITY = eINSTANCE.getGoal_Stability();

		/**
		 * The meta object literal for the '{@link dmt.impl.AgentImpl <em>Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AgentImpl
		 * @see dmt.impl.DmtPackageImpl#getAgent()
		 * @generated
		 */
		EClass AGENT = eINSTANCE.getAgent();

		/**
		 * The meta object literal for the '<em><b>Def</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT__DEF = eINSTANCE.getAgent_Def();

		/**
		 * The meta object literal for the '<em><b>Load</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT__LOAD = eINSTANCE.getAgent_Load();

		/**
		 * The meta object literal for the '<em><b>Subagent</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT__SUBAGENT = eINSTANCE.getAgent_Subagent();

		/**
		 * The meta object literal for the '<em><b>Performs Operation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT__PERFORMS_OPERATION = eINSTANCE.getAgent_PerformsOperation();

		/**
		 * The meta object literal for the '{@link dmt.impl.OperationalizationImpl <em>Operationalization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.OperationalizationImpl
		 * @see dmt.impl.DmtPackageImpl#getOperationalization()
		 * @generated
		 */
		EClass OPERATIONALIZATION = eINSTANCE.getOperationalization();

		/**
		 * The meta object literal for the '<em><b>Req Pre</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONALIZATION__REQ_PRE = eINSTANCE.getOperationalization_ReqPre();

		/**
		 * The meta object literal for the '<em><b>Req Trig</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONALIZATION__REQ_TRIG = eINSTANCE.getOperationalization_ReqTrig();

		/**
		 * The meta object literal for the '<em><b>Req Post</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONALIZATION__REQ_POST = eINSTANCE.getOperationalization_ReqPost();

		/**
		 * The meta object literal for the '<em><b>Operationalize Goal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONALIZATION__OPERATIONALIZE_GOAL = eINSTANCE.getOperationalization_OperationalizeGoal();

		/**
		 * The meta object literal for the '<em><b>Operationalization</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATIONALIZATION__OPERATIONALIZATION = eINSTANCE.getOperationalization_Operationalization();

		/**
		 * The meta object literal for the '{@link dmt.impl.SoftwareAgentImpl <em>Software Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.SoftwareAgentImpl
		 * @see dmt.impl.DmtPackageImpl#getSoftwareAgent()
		 * @generated
		 */
		EClass SOFTWARE_AGENT = eINSTANCE.getSoftwareAgent();

		/**
		 * The meta object literal for the '<em><b>Responsibility Software</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_AGENT__RESPONSIBILITY_SOFTWARE = eINSTANCE.getSoftwareAgent_ResponsibilitySoftware();

		/**
		 * The meta object literal for the '{@link dmt.impl.EnvironmentAgentImpl <em>Environment Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.EnvironmentAgentImpl
		 * @see dmt.impl.DmtPackageImpl#getEnvironmentAgent()
		 * @generated
		 */
		EClass ENVIRONMENT_AGENT = eINSTANCE.getEnvironmentAgent();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENVIRONMENT_AGENT__TYPE = eINSTANCE.getEnvironmentAgent_Type();

		/**
		 * The meta object literal for the '<em><b>Responsibility Environment</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT_AGENT__RESPONSIBILITY_ENVIRONMENT = eINSTANCE.getEnvironmentAgent_ResponsibilityEnvironment();

		/**
		 * The meta object literal for the '{@link dmt.impl.BehaviourGoalImpl <em>Behaviour Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.BehaviourGoalImpl
		 * @see dmt.impl.DmtPackageImpl#getBehaviourGoal()
		 * @generated
		 */
		EClass BEHAVIOUR_GOAL = eINSTANCE.getBehaviourGoal();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BEHAVIOUR_GOAL__TYPE = eINSTANCE.getBehaviourGoal_Type();

		/**
		 * The meta object literal for the '<em><b>Formal Spec</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BEHAVIOUR_GOAL__FORMAL_SPEC = eINSTANCE.getBehaviourGoal_FormalSpec();

		/**
		 * The meta object literal for the '{@link dmt.impl.DomainDescriptionImpl <em>Domain Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DomainDescriptionImpl
		 * @see dmt.impl.DmtPackageImpl#getDomainDescription()
		 * @generated
		 */
		EClass DOMAIN_DESCRIPTION = eINSTANCE.getDomainDescription();

		/**
		 * The meta object literal for the '<em><b>Domain Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOMAIN_DESCRIPTION__DOMAIN_CATEGORY = eINSTANCE.getDomainDescription_DomainCategory();

		/**
		 * The meta object literal for the '{@link dmt.impl.LeafGoalImpl <em>Leaf Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.LeafGoalImpl
		 * @see dmt.impl.DmtPackageImpl#getLeafGoal()
		 * @generated
		 */
		EClass LEAF_GOAL = eINSTANCE.getLeafGoal();

		/**
		 * The meta object literal for the '{@link dmt.impl.ExpectationImpl <em>Expectation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ExpectationImpl
		 * @see dmt.impl.DmtPackageImpl#getExpectation()
		 * @generated
		 */
		EClass EXPECTATION = eINSTANCE.getExpectation();

		/**
		 * The meta object literal for the '{@link dmt.impl.RequirementImpl <em>Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.RequirementImpl
		 * @see dmt.impl.DmtPackageImpl#getRequirement()
		 * @generated
		 */
		EClass REQUIREMENT = eINSTANCE.getRequirement();

		/**
		 * The meta object literal for the '{@link dmt.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.OperationImpl
		 * @see dmt.impl.DmtPackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '{@link dmt.impl.ORGoalRefinementImpl <em>OR Goal Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ORGoalRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getORGoalRefinement()
		 * @generated
		 */
		EClass OR_GOAL_REFINEMENT = eINSTANCE.getORGoalRefinement();

		/**
		 * The meta object literal for the '<em><b>Or Goal Refine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR_GOAL_REFINEMENT__OR_GOAL_REFINE = eINSTANCE.getORGoalRefinement_OrGoalRefine();

		/**
		 * The meta object literal for the '{@link dmt.impl.SecurityNodeImpl <em>Security Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.SecurityNodeImpl
		 * @see dmt.impl.DmtPackageImpl#getSecurityNode()
		 * @generated
		 */
		EClass SECURITY_NODE = eINSTANCE.getSecurityNode();

		/**
		 * The meta object literal for the '{@link dmt.impl.MitigableElementImpl <em>Mitigable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.MitigableElementImpl
		 * @see dmt.impl.DmtPackageImpl#getMitigableElement()
		 * @generated
		 */
		EClass MITIGABLE_ELEMENT = eINSTANCE.getMitigableElement();

		/**
		 * The meta object literal for the '{@link dmt.impl.AttackImpl <em>Attack</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AttackImpl
		 * @see dmt.impl.DmtPackageImpl#getAttack()
		 * @generated
		 */
		EClass ATTACK = eINSTANCE.getAttack();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTACK__TYPE = eINSTANCE.getAttack_Type();

		/**
		 * The meta object literal for the '<em><b>Instantiates Threat</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK__INSTANTIATES_THREAT = eINSTANCE.getAttack_InstantiatesThreat();

		/**
		 * The meta object literal for the '<em><b>Exploits Vulnerability</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK__EXPLOITS_VULNERABILITY = eINSTANCE.getAttack_ExploitsVulnerability();

		/**
		 * The meta object literal for the '<em><b>Contributes To Attack</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK__CONTRIBUTES_TO_ATTACK = eINSTANCE.getAttack_ContributesToAttack();

		/**
		 * The meta object literal for the '<em><b>Leads Risk</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK__LEADS_RISK = eINSTANCE.getAttack_LeadsRisk();

		/**
		 * The meta object literal for the '<em><b>Origin</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTACK__ORIGIN = eINSTANCE.getAttack_Origin();

		/**
		 * The meta object literal for the '<em><b>Realized By Attack Vector</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK__REALIZED_BY_ATTACK_VECTOR = eINSTANCE.getAttack_RealizedByAttackVector();

		/**
		 * The meta object literal for the '{@link dmt.impl.ThreatImpl <em>Threat</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ThreatImpl
		 * @see dmt.impl.DmtPackageImpl#getThreat()
		 * @generated
		 */
		EClass THREAT = eINSTANCE.getThreat();

		/**
		 * The meta object literal for the '<em><b>Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THREAT__CATEGORY = eINSTANCE.getThreat_Category();

		/**
		 * The meta object literal for the '<em><b>Scenario</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THREAT__SCENARIO = eINSTANCE.getThreat_Scenario();

		/**
		 * The meta object literal for the '<em><b>Contributes To Threat</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference THREAT__CONTRIBUTES_TO_THREAT = eINSTANCE.getThreat_ContributesToThreat();

		/**
		 * The meta object literal for the '<em><b>Consequence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute THREAT__CONSEQUENCE = eINSTANCE.getThreat_Consequence();

		/**
		 * The meta object literal for the '{@link dmt.impl.VulnerabilityImpl <em>Vulnerability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.VulnerabilityImpl
		 * @see dmt.impl.DmtPackageImpl#getVulnerability()
		 * @generated
		 */
		EClass VULNERABILITY = eINSTANCE.getVulnerability();

		/**
		 * The meta object literal for the '<em><b>Window Occurrence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VULNERABILITY__WINDOW_OCCURRENCE = eINSTANCE.getVulnerability_WindowOccurrence();

		/**
		 * The meta object literal for the '<em><b>Vulnerability Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VULNERABILITY__VULNERABILITY_TYPE = eINSTANCE.getVulnerability_VulnerabilityType();

		/**
		 * The meta object literal for the '{@link dmt.impl.SecurityControlImpl <em>Security Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.SecurityControlImpl
		 * @see dmt.impl.DmtPackageImpl#getSecurityControl()
		 * @generated
		 */
		EClass SECURITY_CONTROL = eINSTANCE.getSecurityControl();

		/**
		 * The meta object literal for the '<em><b>Pros</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECURITY_CONTROL__PROS = eINSTANCE.getSecurityControl_Pros();

		/**
		 * The meta object literal for the '<em><b>Cons</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECURITY_CONTROL__CONS = eINSTANCE.getSecurityControl_Cons();

		/**
		 * The meta object literal for the '<em><b>Nature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECURITY_CONTROL__NATURE = eINSTANCE.getSecurityControl_Nature();

		/**
		 * The meta object literal for the '<em><b>Incident</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECURITY_CONTROL__INCIDENT = eINSTANCE.getSecurityControl_Incident();

		/**
		 * The meta object literal for the '<em><b>Implements</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_CONTROL__IMPLEMENTS = eINSTANCE.getSecurityControl_Implements();

		/**
		 * The meta object literal for the '{@link dmt.impl.RiskComponentImpl <em>Risk Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.RiskComponentImpl
		 * @see dmt.impl.DmtPackageImpl#getRiskComponent()
		 * @generated
		 */
		EClass RISK_COMPONENT = eINSTANCE.getRiskComponent();

		/**
		 * The meta object literal for the '<em><b>Risk Severity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RISK_COMPONENT__RISK_SEVERITY = eINSTANCE.getRiskComponent_RiskSeverity();

		/**
		 * The meta object literal for the '<em><b>Risk Likelihood</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RISK_COMPONENT__RISK_LIKELIHOOD = eINSTANCE.getRiskComponent_RiskLikelihood();

		/**
		 * The meta object literal for the '<em><b>Harms Asset</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RISK_COMPONENT__HARMS_ASSET = eINSTANCE.getRiskComponent_HarmsAsset();

		/**
		 * The meta object literal for the '{@link dmt.impl.AttackerImpl <em>Attacker</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AttackerImpl
		 * @see dmt.impl.DmtPackageImpl#getAttacker()
		 * @generated
		 */
		EClass ATTACKER = eINSTANCE.getAttacker();

		/**
		 * The meta object literal for the '<em><b>Profile</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTACKER__PROFILE = eINSTANCE.getAttacker_Profile();

		/**
		 * The meta object literal for the '<em><b>Launches Attack</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACKER__LAUNCHES_ATTACK = eINSTANCE.getAttacker_LaunchesAttack();

		/**
		 * The meta object literal for the '{@link dmt.impl.AssetImpl <em>Asset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AssetImpl
		 * @see dmt.impl.DmtPackageImpl#getAsset()
		 * @generated
		 */
		EClass ASSET = eINSTANCE.getAsset();

		/**
		 * The meta object literal for the '<em><b>Asset Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET__ASSET_TYPE = eINSTANCE.getAsset_AssetType();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET__LOCATION = eINSTANCE.getAsset_Location();

		/**
		 * The meta object literal for the '<em><b>Has Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSET__HAS_PROPERTY = eINSTANCE.getAsset_HasProperty();

		/**
		 * The meta object literal for the '<em><b>Documentation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET__DOCUMENTATION = eINSTANCE.getAsset_Documentation();

		/**
		 * The meta object literal for the '<em><b>UID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSET__UID = eINSTANCE.getAsset_UID();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionNodeImpl <em>Deception Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionNodeImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionNode()
		 * @generated
		 */
		EClass DECEPTION_NODE = eINSTANCE.getDeceptionNode();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionTacticImpl <em>Deception Tactic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionTacticImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionTactic()
		 * @generated
		 */
		EClass DECEPTION_TACTIC = eINSTANCE.getDeceptionTactic();

		/**
		 * The meta object literal for the '<em><b>Sub Tactic</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC__SUB_TACTIC = eINSTANCE.getDeceptionTactic_SubTactic();

		/**
		 * The meta object literal for the '<em><b>Apply Dissimulation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC__APPLY_DISSIMULATION = eINSTANCE.getDeceptionTactic_ApplyDissimulation();

		/**
		 * The meta object literal for the '<em><b>Described By Deception Story</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC__DESCRIBED_BY_DECEPTION_STORY = eINSTANCE.getDeceptionTactic_DescribedByDeceptionStory();

		/**
		 * The meta object literal for the '<em><b>Measured By</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC__MEASURED_BY = eINSTANCE.getDeceptionTactic_MeasuredBy();

		/**
		 * The meta object literal for the '<em><b>Monitored By</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC__MONITORED_BY = eINSTANCE.getDeceptionTactic_MonitoredBy();

		/**
		 * The meta object literal for the '<em><b>Lead Deception Risk</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC__LEAD_DECEPTION_RISK = eINSTANCE.getDeceptionTactic_LeadDeceptionRisk();

		/**
		 * The meta object literal for the '<em><b>Apply Simulation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC__APPLY_SIMULATION = eINSTANCE.getDeceptionTactic_ApplySimulation();

		/**
		 * The meta object literal for the '<em><b>Is Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_TACTIC__IS_ABSTRACT = eINSTANCE.getDeceptionTactic_IsAbstract();

		/**
		 * The meta object literal for the '<em><b>Qualityattribute</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC__QUALITYATTRIBUTE = eINSTANCE.getDeceptionTactic_Qualityattribute();

		/**
		 * The meta object literal for the '{@link dmt.impl.IVulnerabilityImpl <em>IVulnerability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.IVulnerabilityImpl
		 * @see dmt.impl.DmtPackageImpl#getIVulnerability()
		 * @generated
		 */
		EClass IVULNERABILITY = eINSTANCE.getIVulnerability();

		/**
		 * The meta object literal for the '<em><b>Window Occurence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVULNERABILITY__WINDOW_OCCURENCE = eINSTANCE.getIVulnerability_WindowOccurence();

		/**
		 * The meta object literal for the '<em><b>Vulnerability Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IVULNERABILITY__VULNERABILITY_TYPE = eINSTANCE.getIVulnerability_VulnerabilityType();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionTechniqueImpl <em>Deception Technique</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionTechniqueImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionTechnique()
		 * @generated
		 */
		EClass DECEPTION_TECHNIQUE = eINSTANCE.getDeceptionTechnique();

		/**
		 * The meta object literal for the '<em><b>Formal Spec</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_TECHNIQUE__FORMAL_SPEC = eINSTANCE.getDeceptionTechnique_FormalSpec();

		/**
		 * The meta object literal for the '<em><b>Informal Spec</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_TECHNIQUE__INFORMAL_SPEC = eINSTANCE.getDeceptionTechnique_InformalSpec();

		/**
		 * The meta object literal for the '<em><b>Expected Outcome</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_TECHNIQUE__EXPECTED_OUTCOME = eINSTANCE.getDeceptionTechnique_ExpectedOutcome();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionStoryImpl <em>Deception Story</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionStoryImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionStory()
		 * @generated
		 */
		EClass DECEPTION_STORY = eINSTANCE.getDeceptionStory();

		/**
		 * The meta object literal for the '<em><b>External Identifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_STORY__EXTERNAL_IDENTIFIER = eINSTANCE.getDeceptionStory_ExternalIdentifier();

		/**
		 * The meta object literal for the '<em><b>Exploit Bias</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_STORY__EXPLOIT_BIAS = eINSTANCE.getDeceptionStory_ExploitBias();

		/**
		 * The meta object literal for the '<em><b>Story Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_STORY__STORY_TYPE = eINSTANCE.getDeceptionStory_StoryType();

		/**
		 * The meta object literal for the '<em><b>Pre Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_STORY__PRE_CONDITION = eINSTANCE.getDeceptionStory_PreCondition();

		/**
		 * The meta object literal for the '<em><b>Pos Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_STORY__POS_CONDITION = eINSTANCE.getDeceptionStory_PosCondition();

		/**
		 * The meta object literal for the '<em><b>Interaction</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_STORY__INTERACTION = eINSTANCE.getDeceptionStory_Interaction();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionBiasImpl <em>Deception Bias</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionBiasImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionBias()
		 * @generated
		 */
		EClass DECEPTION_BIAS = eINSTANCE.getDeceptionBias();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_BIAS__TYPE = eINSTANCE.getDeceptionBias_Type();

		/**
		 * The meta object literal for the '{@link dmt.impl.ArtificialElementImpl <em>Artificial Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ArtificialElementImpl
		 * @see dmt.impl.DmtPackageImpl#getArtificialElement()
		 * @generated
		 */
		EClass ARTIFICIAL_ELEMENT = eINSTANCE.getArtificialElement();

		/**
		 * The meta object literal for the '<em><b>Contain Vulnerability</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARTIFICIAL_ELEMENT__CONTAIN_VULNERABILITY = eINSTANCE.getArtificialElement_ContainVulnerability();

		/**
		 * The meta object literal for the '<em><b>Fake Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARTIFICIAL_ELEMENT__FAKE_TYPE = eINSTANCE.getArtificialElement_FakeType();

		/**
		 * The meta object literal for the '{@link dmt.impl.InteractionNodesImpl <em>Interaction Nodes</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.InteractionNodesImpl
		 * @see dmt.impl.DmtPackageImpl#getInteractionNodes()
		 * @generated
		 */
		EClass INTERACTION_NODES = eINSTANCE.getInteractionNodes();

		/**
		 * The meta object literal for the '{@link dmt.impl.InteractionObjectImpl <em>Interaction Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.InteractionObjectImpl
		 * @see dmt.impl.DmtPackageImpl#getInteractionObject()
		 * @generated
		 */
		EClass INTERACTION_OBJECT = eINSTANCE.getInteractionObject();

		/**
		 * The meta object literal for the '<em><b>Object Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERACTION_OBJECT__OBJECT_NAME = eINSTANCE.getInteractionObject_ObjectName();

		/**
		 * The meta object literal for the '<em><b>Class Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERACTION_OBJECT__CLASS_NAME = eINSTANCE.getInteractionObject_ClassName();

		/**
		 * The meta object literal for the '{@link dmt.impl.LinkImpl <em>Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.LinkImpl
		 * @see dmt.impl.DmtPackageImpl#getLink()
		 * @generated
		 */
		EClass LINK = eINSTANCE.getLink();

		/**
		 * The meta object literal for the '<em><b>Integer Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINK__INTEGER_ID = eINSTANCE.getLink_IntegerId();

		/**
		 * The meta object literal for the '<em><b>Message</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINK__MESSAGE = eINSTANCE.getLink_Message();

		/**
		 * The meta object literal for the '<em><b>To Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK__TO_OBJECT = eINSTANCE.getLink_ToObject();

		/**
		 * The meta object literal for the '<em><b>From Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK__FROM_OBJECT = eINSTANCE.getLink_FromObject();

		/**
		 * The meta object literal for the '{@link dmt.impl.MitigationImpl <em>Mitigation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.MitigationImpl
		 * @see dmt.impl.DmtPackageImpl#getMitigation()
		 * @generated
		 */
		EClass MITIGATION = eINSTANCE.getMitigation();

		/**
		 * The meta object literal for the '<em><b>Mitigation Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MITIGATION__MITIGATION_LEVEL = eINSTANCE.getMitigation_MitigationLevel();

		/**
		 * The meta object literal for the '<em><b>Mitigation Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MITIGATION__MITIGATION_TARGET = eINSTANCE.getMitigation_MitigationTarget();

		/**
		 * The meta object literal for the '<em><b>Mitigation Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MITIGATION__MITIGATION_SOURCE = eINSTANCE.getMitigation_MitigationSource();

		/**
		 * The meta object literal for the '{@link dmt.impl.SimulationImpl <em>Simulation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.SimulationImpl
		 * @see dmt.impl.DmtPackageImpl#getSimulation()
		 * @generated
		 */
		EClass SIMULATION = eINSTANCE.getSimulation();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION__TYPE = eINSTANCE.getSimulation_Type();

		/**
		 * The meta object literal for the '<em><b>Asset Reference</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION__ASSET_REFERENCE = eINSTANCE.getSimulation_AssetReference();

		/**
		 * The meta object literal for the '{@link dmt.impl.DissimulationImpl <em>Dissimulation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DissimulationImpl
		 * @see dmt.impl.DmtPackageImpl#getDissimulation()
		 * @generated
		 */
		EClass DISSIMULATION = eINSTANCE.getDissimulation();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DISSIMULATION__TYPE = eINSTANCE.getDissimulation_Type();

		/**
		 * The meta object literal for the '<em><b>Hide Real Asset</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISSIMULATION__HIDE_REAL_ASSET = eINSTANCE.getDissimulation_HideRealAsset();

		/**
		 * The meta object literal for the '{@link dmt.impl.NodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.NodeImpl
		 * @see dmt.impl.DmtPackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__NAME = eINSTANCE.getNode_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__DESCRIPTION = eINSTANCE.getNode_Description();

		/**
		 * The meta object literal for the '<em><b>UUID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE__UUID = eINSTANCE.getNode_UUID();

		/**
		 * The meta object literal for the '<em><b>Has Annotations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE__HAS_ANNOTATIONS = eINSTANCE.getNode_HasAnnotations();

		/**
		 * The meta object literal for the '{@link dmt.impl.AgentAssignmentImpl <em>Agent Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AgentAssignmentImpl
		 * @see dmt.impl.DmtPackageImpl#getAgentAssignment()
		 * @generated
		 */
		EClass AGENT_ASSIGNMENT = eINSTANCE.getAgentAssignment();

		/**
		 * The meta object literal for the '<em><b>Agent Goal Assignment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_ASSIGNMENT__AGENT_GOAL_ASSIGNMENT = eINSTANCE.getAgentAssignment_AgentGoalAssignment();

		/**
		 * The meta object literal for the '<em><b>Agent Assignment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_ASSIGNMENT__AGENT_ASSIGNMENT = eINSTANCE.getAgentAssignment_AgentAssignment();

		/**
		 * The meta object literal for the '{@link dmt.impl.ANDGoalRefinementImpl <em>AND Goal Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ANDGoalRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getANDGoalRefinement()
		 * @generated
		 */
		EClass AND_GOAL_REFINEMENT = eINSTANCE.getANDGoalRefinement();

		/**
		 * The meta object literal for the '<em><b>And Goal Refine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND_GOAL_REFINEMENT__AND_GOAL_REFINE = eINSTANCE.getANDGoalRefinement_AndGoalRefine();

		/**
		 * The meta object literal for the '{@link dmt.impl.ConflictGoalRefinementImpl <em>Conflict Goal Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ConflictGoalRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getConflictGoalRefinement()
		 * @generated
		 */
		EClass CONFLICT_GOAL_REFINEMENT = eINSTANCE.getConflictGoalRefinement();

		/**
		 * The meta object literal for the '<em><b>Conflict Goal Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_SOURCE = eINSTANCE.getConflictGoalRefinement_ConflictGoalSource();

		/**
		 * The meta object literal for the '<em><b>Conflict Goal Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONFLICT_GOAL_REFINEMENT__CONFLICT_GOAL_TARGET = eINSTANCE.getConflictGoalRefinement_ConflictGoalTarget();

		/**
		 * The meta object literal for the '{@link dmt.impl.AgentMonitoringImpl <em>Agent Monitoring</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AgentMonitoringImpl
		 * @see dmt.impl.DmtPackageImpl#getAgentMonitoring()
		 * @generated
		 */
		EClass AGENT_MONITORING = eINSTANCE.getAgentMonitoring();

		/**
		 * The meta object literal for the '<em><b>Agent Monitor</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_MONITORING__AGENT_MONITOR = eINSTANCE.getAgentMonitoring_AgentMonitor();

		/**
		 * The meta object literal for the '<em><b>Object Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT_MONITORING__OBJECT_ATTRIBUTE = eINSTANCE.getAgentMonitoring_ObjectAttribute();

		/**
		 * The meta object literal for the '{@link dmt.impl.AgentControlImpl <em>Agent Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AgentControlImpl
		 * @see dmt.impl.DmtPackageImpl#getAgentControl()
		 * @generated
		 */
		EClass AGENT_CONTROL = eINSTANCE.getAgentControl();

		/**
		 * The meta object literal for the '<em><b>Object Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AGENT_CONTROL__OBJECT_ATTRIBUTE = eINSTANCE.getAgentControl_ObjectAttribute();

		/**
		 * The meta object literal for the '<em><b>Agent Control</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_CONTROL__AGENT_CONTROL = eINSTANCE.getAgentControl_AgentControl();

		/**
		 * The meta object literal for the '{@link dmt.impl.AgentRelationImpl <em>Agent Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AgentRelationImpl
		 * @see dmt.impl.DmtPackageImpl#getAgentRelation()
		 * @generated
		 */
		EClass AGENT_RELATION = eINSTANCE.getAgentRelation();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionMetricImpl <em>Deception Metric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionMetricImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionMetric()
		 * @generated
		 */
		EClass DECEPTION_METRIC = eINSTANCE.getDeceptionMetric();

		/**
		 * The meta object literal for the '<em><b>Metric Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_METRIC__METRIC_TYPE = eINSTANCE.getDeceptionMetric_MetricType();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_METRIC__UNIT = eINSTANCE.getDeceptionMetric_Unit();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionChannelImpl <em>Deception Channel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionChannelImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionChannel()
		 * @generated
		 */
		EClass DECEPTION_CHANNEL = eINSTANCE.getDeceptionChannel();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_CHANNEL__TYPE = eINSTANCE.getDeceptionChannel_Type();

		/**
		 * The meta object literal for the '{@link dmt.impl.AttackVectorImpl <em>Attack Vector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AttackVectorImpl
		 * @see dmt.impl.DmtPackageImpl#getAttackVector()
		 * @generated
		 */
		EClass ATTACK_VECTOR = eINSTANCE.getAttackVector();

		/**
		 * The meta object literal for the '<em><b>Example</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTACK_VECTOR__EXAMPLE = eINSTANCE.getAttackVector_Example();

		/**
		 * The meta object literal for the '<em><b>Resources</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTACK_VECTOR__RESOURCES = eINSTANCE.getAttackVector_Resources();

		/**
		 * The meta object literal for the '<em><b>Realizes Attack</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK_VECTOR__REALIZES_ATTACK = eINSTANCE.getAttackVector_RealizesAttack();

		/**
		 * The meta object literal for the '<em><b>Initialattackstep</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK_VECTOR__INITIALATTACKSTEP = eINSTANCE.getAttackVector_Initialattackstep();

		/**
		 * The meta object literal for the '<em><b>Endattackstep</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK_VECTOR__ENDATTACKSTEP = eINSTANCE.getAttackVector_Endattackstep();

		/**
		 * The meta object literal for the '{@link dmt.impl.APropertyImpl <em>AProperty</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.APropertyImpl
		 * @see dmt.impl.DmtPackageImpl#getAProperty()
		 * @generated
		 */
		EClass APROPERTY = eINSTANCE.getAProperty();

		/**
		 * The meta object literal for the '<em><b>Atype</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute APROPERTY__ATYPE = eINSTANCE.getAProperty_Atype();

		/**
		 * The meta object literal for the '{@link dmt.impl.AttackStepConnectorImpl <em>Attack Step Connector</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AttackStepConnectorImpl
		 * @see dmt.impl.DmtPackageImpl#getAttackStepConnector()
		 * @generated
		 */
		EClass ATTACK_STEP_CONNECTOR = eINSTANCE.getAttackStepConnector();

		/**
		 * The meta object literal for the '<em><b>Subattackstep</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK_STEP_CONNECTOR__SUBATTACKSTEP = eINSTANCE.getAttackStepConnector_Subattackstep();

		/**
		 * The meta object literal for the '<em><b>Step Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTACK_STEP_CONNECTOR__STEP_DESCRIPTION = eINSTANCE.getAttackStepConnector_StepDescription();

		/**
		 * The meta object literal for the '<em><b>Attack Step Connector Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTACK_STEP_CONNECTOR__ATTACK_STEP_CONNECTOR_TYPE = eINSTANCE.getAttackStepConnector_AttackStepConnectorType();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTACK_STEP_CONNECTOR__SEQUENCE = eINSTANCE.getAttackStepConnector_Sequence();

		/**
		 * The meta object literal for the '{@link dmt.impl.InitialAttackStepImpl <em>Initial Attack Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.InitialAttackStepImpl
		 * @see dmt.impl.DmtPackageImpl#getInitialAttackStep()
		 * @generated
		 */
		EClass INITIAL_ATTACK_STEP = eINSTANCE.getInitialAttackStep();

		/**
		 * The meta object literal for the '<em><b>Has Attack Steps</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INITIAL_ATTACK_STEP__HAS_ATTACK_STEPS = eINSTANCE.getInitialAttackStep_HasAttackSteps();

		/**
		 * The meta object literal for the '<em><b>Pre Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INITIAL_ATTACK_STEP__PRE_CONDITION = eINSTANCE.getInitialAttackStep_PreCondition();

		/**
		 * The meta object literal for the '{@link dmt.impl.EndAttackStepImpl <em>End Attack Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.EndAttackStepImpl
		 * @see dmt.impl.DmtPackageImpl#getEndAttackStep()
		 * @generated
		 */
		EClass END_ATTACK_STEP = eINSTANCE.getEndAttackStep();

		/**
		 * The meta object literal for the '<em><b>Expected Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute END_ATTACK_STEP__EXPECTED_RESULT = eINSTANCE.getEndAttackStep_ExpectedResult();

		/**
		 * The meta object literal for the '{@link dmt.impl.AttackStepImpl <em>Attack Step</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AttackStepImpl
		 * @see dmt.impl.DmtPackageImpl#getAttackStep()
		 * @generated
		 */
		EClass ATTACK_STEP = eINSTANCE.getAttackStep();

		/**
		 * The meta object literal for the '{@link dmt.impl.AttackTreeImpl <em>Attack Tree</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AttackTreeImpl
		 * @see dmt.impl.DmtPackageImpl#getAttackTree()
		 * @generated
		 */
		EClass ATTACK_TREE = eINSTANCE.getAttackTree();

		/**
		 * The meta object literal for the '<em><b>Has Attackvector</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTACK_TREE__HAS_ATTACKVECTOR = eINSTANCE.getAttackTree_HasAttackvector();

		/**
		 * The meta object literal for the '{@link dmt.impl.GoalNodeImpl <em>Goal Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.GoalNodeImpl
		 * @see dmt.impl.DmtPackageImpl#getGoalNode()
		 * @generated
		 */
		EClass GOAL_NODE = eINSTANCE.getGoalNode();

		/**
		 * The meta object literal for the '{@link dmt.impl.SoftGoalImpl <em>Soft Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.SoftGoalImpl
		 * @see dmt.impl.DmtPackageImpl#getSoftGoal()
		 * @generated
		 */
		EClass SOFT_GOAL = eINSTANCE.getSoftGoal();

		/**
		 * The meta object literal for the '<em><b>Fit Criterion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_GOAL__FIT_CRITERION = eINSTANCE.getSoftGoal_FitCriterion();

		/**
		 * The meta object literal for the '<em><b>Soft Goal Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_GOAL__SOFT_GOAL_TYPE = eINSTANCE.getSoftGoal_SoftGoalType();

		/**
		 * The meta object literal for the '<em><b>Soft Goal Open Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_GOAL__SOFT_GOAL_OPEN_TYPE = eINSTANCE.getSoftGoal_SoftGoalOpenType();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionBehaviourGoalImpl <em>Deception Behaviour Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionBehaviourGoalImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionBehaviourGoal()
		 * @generated
		 */
		EClass DECEPTION_BEHAVIOUR_GOAL = eINSTANCE.getDeceptionBehaviourGoal();

		/**
		 * The meta object literal for the '<em><b>Formal Spec</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_BEHAVIOUR_GOAL__FORMAL_SPEC = eINSTANCE.getDeceptionBehaviourGoal_FormalSpec();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_BEHAVIOUR_GOAL__TYPE = eINSTANCE.getDeceptionBehaviourGoal_Type();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionSoftGoalImpl <em>Deception Soft Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionSoftGoalImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionSoftGoal()
		 * @generated
		 */
		EClass DECEPTION_SOFT_GOAL = eINSTANCE.getDeceptionSoftGoal();

		/**
		 * The meta object literal for the '<em><b>Fit Criterion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_SOFT_GOAL__FIT_CRITERION = eINSTANCE.getDeceptionSoftGoal_FitCriterion();

		/**
		 * The meta object literal for the '<em><b>Soft Goal Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_SOFT_GOAL__SOFT_GOAL_TYPE = eINSTANCE.getDeceptionSoftGoal_SoftGoalType();

		/**
		 * The meta object literal for the '<em><b>Soft Goal Open Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_SOFT_GOAL__SOFT_GOAL_OPEN_TYPE = eINSTANCE.getDeceptionSoftGoal_SoftGoalOpenType();

		/**
		 * The meta object literal for the '{@link dmt.impl.ConcreteAssetImpl <em>Concrete Asset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ConcreteAssetImpl
		 * @see dmt.impl.DmtPackageImpl#getConcreteAsset()
		 * @generated
		 */
		EClass CONCRETE_ASSET = eINSTANCE.getConcreteAsset();

		/**
		 * The meta object literal for the '<em><b>Business Importance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONCRETE_ASSET__BUSINESS_IMPORTANCE = eINSTANCE.getConcreteAsset_BusinessImportance();

		/**
		 * The meta object literal for the '{@link dmt.impl.OperationableImpl <em>Operationable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.OperationableImpl
		 * @see dmt.impl.DmtPackageImpl#getOperationable()
		 * @generated
		 */
		EClass OPERATIONABLE = eINSTANCE.getOperationable();

		/**
		 * The meta object literal for the '<em><b>Dom Post</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONABLE__DOM_POST = eINSTANCE.getOperationable_DomPost();

		/**
		 * The meta object literal for the '<em><b>Dom Pre</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONABLE__DOM_PRE = eINSTANCE.getOperationable_DomPre();

		/**
		 * The meta object literal for the '<em><b>Formal Dom Post</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONABLE__FORMAL_DOM_POST = eINSTANCE.getOperationable_FormalDomPost();

		/**
		 * The meta object literal for the '<em><b>Formal Dom Pre</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATIONABLE__FORMAL_DOM_PRE = eINSTANCE.getOperationable_FormalDomPre();

		/**
		 * The meta object literal for the '{@link dmt.impl.GoalContainerImpl <em>Goal Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.GoalContainerImpl
		 * @see dmt.impl.DmtPackageImpl#getGoalContainer()
		 * @generated
		 */
		EClass GOAL_CONTAINER = eINSTANCE.getGoalContainer();

		/**
		 * The meta object literal for the '<em><b>Goal</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL_CONTAINER__GOAL = eINSTANCE.getGoalContainer_Goal();

		/**
		 * The meta object literal for the '{@link dmt.impl.NodeSelectionImpl <em>Node Selection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.NodeSelectionImpl
		 * @see dmt.impl.DmtPackageImpl#getNodeSelection()
		 * @generated
		 */
		EClass NODE_SELECTION = eINSTANCE.getNodeSelection();

		/**
		 * The meta object literal for the '<em><b>Selectionsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_SELECTION__SELECTIONSOURCE = eINSTANCE.getNodeSelection_Selectionsource();

		/**
		 * The meta object literal for the '<em><b>Selectiontarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_SELECTION__SELECTIONTARGET = eINSTANCE.getNodeSelection_Selectiontarget();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE_SELECTION__TYPE = eINSTANCE.getNodeSelection_Type();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionTacticStrategyImpl <em>Deception Tactic Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionTacticStrategyImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionTacticStrategy()
		 * @generated
		 */
		EClass DECEPTION_TACTIC_STRATEGY = eINSTANCE.getDeceptionTacticStrategy();

		/**
		 * The meta object literal for the '<em><b>Deceptiontacticmechanism</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC_STRATEGY__DECEPTIONTACTICMECHANISM = eINSTANCE.getDeceptionTacticStrategy_Deceptiontacticmechanism();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionStrategyTacticRefinementImpl <em>Deception Strategy Tactic Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionStrategyTacticRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionStrategyTacticRefinement()
		 * @generated
		 */
		EClass DECEPTION_STRATEGY_TACTIC_REFINEMENT = eINSTANCE.getDeceptionStrategyTacticRefinement();

		/**
		 * The meta object literal for the '<em><b>Strategytacticrefinementsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE = eINSTANCE.getDeceptionStrategyTacticRefinement_Strategytacticrefinementsource();

		/**
		 * The meta object literal for the '<em><b>Strategytacticrefinementtarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET = eINSTANCE.getDeceptionStrategyTacticRefinement_Strategytacticrefinementtarget();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionTacticStrategyRefinementImpl <em>Deception Tactic Strategy Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionTacticStrategyRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionTacticStrategyRefinement()
		 * @generated
		 */
		EClass DECEPTION_TACTIC_STRATEGY_REFINEMENT = eINSTANCE.getDeceptionTacticStrategyRefinement();

		/**
		 * The meta object literal for the '<em><b>Tacticstrategytacticrefinementsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTSOURCE = eINSTANCE.getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementsource();

		/**
		 * The meta object literal for the '<em><b>Tacticstrategytacticrefinementtarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC_STRATEGY_REFINEMENT__TACTICSTRATEGYTACTICREFINEMENTTARGET = eINSTANCE.getDeceptionTacticStrategyRefinement_Tacticstrategytacticrefinementtarget();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionStrategyRefinementImpl <em>Deception Strategy Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionStrategyRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionStrategyRefinement()
		 * @generated
		 */
		EClass DECEPTION_STRATEGY_REFINEMENT = eINSTANCE.getDeceptionStrategyRefinement();

		/**
		 * The meta object literal for the '<em><b>Deceptionstrategyrefinementsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTSOURCE = eINSTANCE.getDeceptionStrategyRefinement_Deceptionstrategyrefinementsource();

		/**
		 * The meta object literal for the '<em><b>Deceptionstrategyrefinementtarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_STRATEGY_REFINEMENT__DECEPTIONSTRATEGYREFINEMENTTARGET = eINSTANCE.getDeceptionStrategyRefinement_Deceptionstrategyrefinementtarget();

		/**
		 * The meta object literal for the '{@link dmt.impl.SimulationHasArtificialElementImpl <em>Simulation Has Artificial Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.SimulationHasArtificialElementImpl
		 * @see dmt.impl.DmtPackageImpl#getSimulationHasArtificialElement()
		 * @generated
		 */
		EClass SIMULATION_HAS_ARTIFICIAL_ELEMENT = eINSTANCE.getSimulationHasArtificialElement();

		/**
		 * The meta object literal for the '<em><b>Show False Asset</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET = eINSTANCE.getSimulationHasArtificialElement_ShowFalseAsset();

		/**
		 * The meta object literal for the '<em><b>Lower Fakes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES = eINSTANCE.getSimulationHasArtificialElement_LowerFakes();

		/**
		 * The meta object literal for the '<em><b>Upper Fakes</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES = eINSTANCE.getSimulationHasArtificialElement_UpperFakes();

		/**
		 * The meta object literal for the '<em><b>Simulation Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE = eINSTANCE.getSimulationHasArtificialElement_SimulationType();

		/**
		 * The meta object literal for the '{@link dmt.impl.PropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.PropertyImpl
		 * @see dmt.impl.DmtPackageImpl#getProperty()
		 * @generated
		 */
		EClass PROPERTY = eINSTANCE.getProperty();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROPERTY__VALUE = eINSTANCE.getProperty_Value();

		/**
		 * The meta object literal for the '{@link dmt.impl.AnnotationImpl <em>Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AnnotationImpl
		 * @see dmt.impl.DmtPackageImpl#getAnnotation()
		 * @generated
		 */
		EClass ANNOTATION = eINSTANCE.getAnnotation();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__TEXT = eINSTANCE.getAnnotation_Text();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionGoalImpl <em>Deception Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionGoalImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionGoal()
		 * @generated
		 */
		EClass DECEPTION_GOAL = eINSTANCE.getDeceptionGoal();

		/**
		 * The meta object literal for the '<em><b>Deception Goal Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_GOAL__DECEPTION_GOAL_TYPE = eINSTANCE.getDeceptionGoal_DeceptionGoalType();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionRiskImpl <em>Deception Risk</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionRiskImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionRisk()
		 * @generated
		 */
		EClass DECEPTION_RISK = eINSTANCE.getDeceptionRisk();

		/**
		 * The meta object literal for the '<em><b>Deception Risk Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_RISK__DECEPTION_RISK_TYPE = eINSTANCE.getDeceptionRisk_DeceptionRiskType();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionStrategyImpl <em>Deception Strategy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionStrategyImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionStrategy()
		 * @generated
		 */
		EClass DECEPTION_STRATEGY = eINSTANCE.getDeceptionStrategy();

		/**
		 * The meta object literal for the '<em><b>Is Root</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_STRATEGY__IS_ROOT = eINSTANCE.getDeceptionStrategy_IsRoot();

		/**
		 * The meta object literal for the '<em><b>Execution Parameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_STRATEGY__EXECUTION_PARAMETER = eINSTANCE.getDeceptionStrategy_ExecutionParameter();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionTacticMechanismImpl <em>Deception Tactic Mechanism</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionTacticMechanismImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionTacticMechanism()
		 * @generated
		 */
		EClass DECEPTION_TACTIC_MECHANISM = eINSTANCE.getDeceptionTacticMechanism();

		/**
		 * The meta object literal for the '<em><b>Enabling Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_TACTIC_MECHANISM__ENABLING_CONDITION = eINSTANCE.getDeceptionTacticMechanism_EnablingCondition();

		/**
		 * The meta object literal for the '<em><b>Tactic Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECEPTION_TACTIC_MECHANISM__TACTIC_REFERENCE = eINSTANCE.getDeceptionTacticMechanism_TacticReference();

		/**
		 * The meta object literal for the '{@link dmt.impl.StrategyNodeImpl <em>Strategy Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.StrategyNodeImpl
		 * @see dmt.impl.DmtPackageImpl#getStrategyNode()
		 * @generated
		 */
		EClass STRATEGY_NODE = eINSTANCE.getStrategyNode();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRATEGY_NODE__PROPERTY = eINSTANCE.getStrategyNode_Property();

		/**
		 * The meta object literal for the '{@link dmt.impl.FeatureImpl <em>Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.FeatureImpl
		 * @see dmt.impl.DmtPackageImpl#getFeature()
		 * @generated
		 */
		EClass FEATURE = eINSTANCE.getFeature();

		/**
		 * The meta object literal for the '{@link dmt.impl.StrategyRelationshipImpl <em>Strategy Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.StrategyRelationshipImpl
		 * @see dmt.impl.DmtPackageImpl#getStrategyRelationship()
		 * @generated
		 */
		EClass STRATEGY_RELATIONSHIP = eINSTANCE.getStrategyRelationship();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRATEGY_RELATIONSHIP__DESCRIPTION = eINSTANCE.getStrategyRelationship_Description();

		/**
		 * The meta object literal for the '{@link dmt.impl.FeatureConstraintImpl <em>Feature Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.FeatureConstraintImpl
		 * @see dmt.impl.DmtPackageImpl#getFeatureConstraint()
		 * @generated
		 */
		EClass FEATURE_CONSTRAINT = eINSTANCE.getFeatureConstraint();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_CONSTRAINT__SOURCE = eINSTANCE.getFeatureConstraint_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_CONSTRAINT__TARGET = eINSTANCE.getFeatureConstraint_Target();

		/**
		 * The meta object literal for the '{@link dmt.impl.AbstractFeatureImpl <em>Abstract Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AbstractFeatureImpl
		 * @see dmt.impl.DmtPackageImpl#getAbstractFeature()
		 * @generated
		 */
		EClass ABSTRACT_FEATURE = eINSTANCE.getAbstractFeature();

		/**
		 * The meta object literal for the '<em><b>Binding State</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_FEATURE__BINDING_STATE = eINSTANCE.getAbstractFeature_BindingState();

		/**
		 * The meta object literal for the '<em><b>Is Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ABSTRACT_FEATURE__IS_ABSTRACT = eINSTANCE.getAbstractFeature_IsAbstract();

		/**
		 * The meta object literal for the '{@link dmt.impl.RequiredConstraintImpl <em>Required Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.RequiredConstraintImpl
		 * @see dmt.impl.DmtPackageImpl#getRequiredConstraint()
		 * @generated
		 */
		EClass REQUIRED_CONSTRAINT = eINSTANCE.getRequiredConstraint();

		/**
		 * The meta object literal for the '{@link dmt.impl.ExcludeConstraintImpl <em>Exclude Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ExcludeConstraintImpl
		 * @see dmt.impl.DmtPackageImpl#getExcludeConstraint()
		 * @generated
		 */
		EClass EXCLUDE_CONSTRAINT = eINSTANCE.getExcludeConstraint();

		/**
		 * The meta object literal for the '{@link dmt.impl.BenefitConstraintImpl <em>Benefit Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.BenefitConstraintImpl
		 * @see dmt.impl.DmtPackageImpl#getBenefitConstraint()
		 * @generated
		 */
		EClass BENEFIT_CONSTRAINT = eINSTANCE.getBenefitConstraint();

		/**
		 * The meta object literal for the '{@link dmt.impl.AvoidConstraintImpl <em>Avoid Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.AvoidConstraintImpl
		 * @see dmt.impl.DmtPackageImpl#getAvoidConstraint()
		 * @generated
		 */
		EClass AVOID_CONSTRAINT = eINSTANCE.getAvoidConstraint();

		/**
		 * The meta object literal for the '{@link dmt.impl.NodeRefinementImpl <em>Node Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.NodeRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getNodeRefinement()
		 * @generated
		 */
		EClass NODE_REFINEMENT = eINSTANCE.getNodeRefinement();

		/**
		 * The meta object literal for the '<em><b>Enactment Choice</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NODE_REFINEMENT__ENACTMENT_CHOICE = eINSTANCE.getNodeRefinement_EnactmentChoice();

		/**
		 * The meta object literal for the '{@link dmt.impl.TacticRefinementImpl <em>Tactic Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.TacticRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getTacticRefinement()
		 * @generated
		 */
		EClass TACTIC_REFINEMENT = eINSTANCE.getTacticRefinement();

		/**
		 * The meta object literal for the '<em><b>Tacticrefinementtarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TACTIC_REFINEMENT__TACTICREFINEMENTTARGET = eINSTANCE.getTacticRefinement_Tacticrefinementtarget();

		/**
		 * The meta object literal for the '<em><b>Tacticrefinementsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TACTIC_REFINEMENT__TACTICREFINEMENTSOURCE = eINSTANCE.getTacticRefinement_Tacticrefinementsource();

		/**
		 * The meta object literal for the '{@link dmt.impl.FeatureRefinementImpl <em>Feature Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.FeatureRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getFeatureRefinement()
		 * @generated
		 */
		EClass FEATURE_REFINEMENT = eINSTANCE.getFeatureRefinement();

		/**
		 * The meta object literal for the '<em><b>Featurerefinementtarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_REFINEMENT__FEATUREREFINEMENTTARGET = eINSTANCE.getFeatureRefinement_Featurerefinementtarget();

		/**
		 * The meta object literal for the '<em><b>Featurerefinementsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_REFINEMENT__FEATUREREFINEMENTSOURCE = eINSTANCE.getFeatureRefinement_Featurerefinementsource();

		/**
		 * The meta object literal for the '{@link dmt.impl.StrategyTacticRefinementImpl <em>Strategy Tactic Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.StrategyTacticRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getStrategyTacticRefinement()
		 * @generated
		 */
		EClass STRATEGY_TACTIC_REFINEMENT = eINSTANCE.getStrategyTacticRefinement();

		/**
		 * The meta object literal for the '<em><b>Strategytacticrefinementtarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTTARGET = eINSTANCE.getStrategyTacticRefinement_Strategytacticrefinementtarget();

		/**
		 * The meta object literal for the '<em><b>Strategytacticrefinementsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STRATEGY_TACTIC_REFINEMENT__STRATEGYTACTICREFINEMENTSOURCE = eINSTANCE.getStrategyTacticRefinement_Strategytacticrefinementsource();

		/**
		 * The meta object literal for the '{@link dmt.impl.TacticFeatureRefinementImpl <em>Tactic Feature Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.TacticFeatureRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getTacticFeatureRefinement()
		 * @generated
		 */
		EClass TACTIC_FEATURE_REFINEMENT = eINSTANCE.getTacticFeatureRefinement();

		/**
		 * The meta object literal for the '<em><b>Tacticfeaturerefinementtarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTTARGET = eINSTANCE.getTacticFeatureRefinement_Tacticfeaturerefinementtarget();

		/**
		 * The meta object literal for the '<em><b>Tacticfeaturerefinementsource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TACTIC_FEATURE_REFINEMENT__TACTICFEATUREREFINEMENTSOURCE = eINSTANCE.getTacticFeatureRefinement_Tacticfeaturerefinementsource();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionRelationshipImpl <em>Deception Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionRelationshipImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionRelationship()
		 * @generated
		 */
		EClass DECEPTION_RELATIONSHIP = eINSTANCE.getDeceptionRelationship();

		/**
		 * The meta object literal for the '<em><b>Alt Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DECEPTION_RELATIONSHIP__ALT_NAME = eINSTANCE.getDeceptionRelationship_AltName();

		/**
		 * The meta object literal for the '{@link dmt.impl.MitigatorImpl <em>Mitigator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.MitigatorImpl
		 * @see dmt.impl.DmtPackageImpl#getMitigator()
		 * @generated
		 */
		EClass MITIGATOR = eINSTANCE.getMitigator();

		/**
		 * The meta object literal for the '{@link dmt.impl.SecurityRelationshipImpl <em>Security Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.SecurityRelationshipImpl
		 * @see dmt.impl.DmtPackageImpl#getSecurityRelationship()
		 * @generated
		 */
		EClass SECURITY_RELATIONSHIP = eINSTANCE.getSecurityRelationship();

		/**
		 * The meta object literal for the '{@link dmt.impl.ObstructionElementImpl <em>Obstruction Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ObstructionElementImpl
		 * @see dmt.impl.DmtPackageImpl#getObstructionElement()
		 * @generated
		 */
		EClass OBSTRUCTION_ELEMENT = eINSTANCE.getObstructionElement();

		/**
		 * The meta object literal for the '<em><b>Obstruct</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSTRUCTION_ELEMENT__OBSTRUCT = eINSTANCE.getObstructionElement_Obstruct();

		/**
		 * The meta object literal for the '<em><b>OR Obstruction Refined</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSTRUCTION_ELEMENT__OR_OBSTRUCTION_REFINED = eINSTANCE.getObstructionElement_ORObstructionRefined();

		/**
		 * The meta object literal for the '<em><b>AND Obstruction Refined</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBSTRUCTION_ELEMENT__AND_OBSTRUCTION_REFINED = eINSTANCE.getObstructionElement_ANDObstructionRefined();

		/**
		 * The meta object literal for the '{@link dmt.impl.ORObstructionRefinementImpl <em>OR Obstruction Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ORObstructionRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getORObstructionRefinement()
		 * @generated
		 */
		EClass OR_OBSTRUCTION_REFINEMENT = eINSTANCE.getORObstructionRefinement();

		/**
		 * The meta object literal for the '<em><b>OR Obstruction Refine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OR_OBSTRUCTION_REFINEMENT__OR_OBSTRUCTION_REFINE = eINSTANCE.getORObstructionRefinement_ORObstructionRefine();

		/**
		 * The meta object literal for the '{@link dmt.impl.ANDObstructionRefinementImpl <em>AND Obstruction Refinement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ANDObstructionRefinementImpl
		 * @see dmt.impl.DmtPackageImpl#getANDObstructionRefinement()
		 * @generated
		 */
		EClass AND_OBSTRUCTION_REFINEMENT = eINSTANCE.getANDObstructionRefinement();

		/**
		 * The meta object literal for the '<em><b>AND Obstruction Refine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AND_OBSTRUCTION_REFINEMENT__AND_OBSTRUCTION_REFINE = eINSTANCE.getANDObstructionRefinement_ANDObstructionRefine();

		/**
		 * The meta object literal for the '{@link dmt.impl.ObstacleImpl <em>Obstacle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ObstacleImpl
		 * @see dmt.impl.DmtPackageImpl#getObstacle()
		 * @generated
		 */
		EClass OBSTACLE = eINSTANCE.getObstacle();

		/**
		 * The meta object literal for the '<em><b>Obstacle Category</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBSTACLE__OBSTACLE_CATEGORY = eINSTANCE.getObstacle_ObstacleCategory();

		/**
		 * The meta object literal for the '<em><b>Formal Def</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBSTACLE__FORMAL_DEF = eINSTANCE.getObstacle_FormalDef();

		/**
		 * The meta object literal for the '<em><b>Likelihood</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OBSTACLE__LIKELIHOOD = eINSTANCE.getObstacle_Likelihood();

		/**
		 * The meta object literal for the '{@link dmt.impl.InfluenceNodeImpl <em>Influence Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.InfluenceNodeImpl
		 * @see dmt.impl.DmtPackageImpl#getInfluenceNode()
		 * @generated
		 */
		EClass INFLUENCE_NODE = eINSTANCE.getInfluenceNode();

		/**
		 * The meta object literal for the '{@link dmt.impl.QualityAttributeImpl <em>Quality Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.QualityAttributeImpl
		 * @see dmt.impl.DmtPackageImpl#getQualityAttribute()
		 * @generated
		 */
		EClass QUALITY_ATTRIBUTE = eINSTANCE.getQualityAttribute();

		/**
		 * The meta object literal for the '{@link dmt.impl.InfluenceRelationshipImpl <em>Influence Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.InfluenceRelationshipImpl
		 * @see dmt.impl.DmtPackageImpl#getInfluenceRelationship()
		 * @generated
		 */
		EClass INFLUENCE_RELATIONSHIP = eINSTANCE.getInfluenceRelationship();

		/**
		 * The meta object literal for the '{@link dmt.impl.InfluenceImpl <em>Influence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.InfluenceImpl
		 * @see dmt.impl.DmtPackageImpl#getInfluence()
		 * @generated
		 */
		EClass INFLUENCE = eINSTANCE.getInfluence();

		/**
		 * The meta object literal for the '<em><b>Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INFLUENCE__WEIGHT = eINSTANCE.getInfluence_Weight();

		/**
		 * The meta object literal for the '<em><b>Polarity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INFLUENCE__POLARITY = eINSTANCE.getInfluence_Polarity();

		/**
		 * The meta object literal for the '<em><b>Influence Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INFLUENCE__INFLUENCE_SOURCE = eINSTANCE.getInfluence_InfluenceSource();

		/**
		 * The meta object literal for the '<em><b>Influencetarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INFLUENCE__INFLUENCETARGET = eINSTANCE.getInfluence_Influencetarget();

		/**
		 * The meta object literal for the '{@link dmt.impl.PreferenceImpl <em>Preference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.PreferenceImpl
		 * @see dmt.impl.DmtPackageImpl#getPreference()
		 * @generated
		 */
		EClass PREFERENCE = eINSTANCE.getPreference();

		/**
		 * The meta object literal for the '<em><b>Preference Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PREFERENCE__PREFERENCE_TARGET = eINSTANCE.getPreference_PreferenceTarget();

		/**
		 * The meta object literal for the '<em><b>Preference Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PREFERENCE__PREFERENCE_SOURCE = eINSTANCE.getPreference_PreferenceSource();

		/**
		 * The meta object literal for the '<em><b>Weight</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PREFERENCE__WEIGHT = eINSTANCE.getPreference_Weight();

		/**
		 * The meta object literal for the '{@link dmt.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.EventImpl
		 * @see dmt.impl.DmtPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link dmt.impl.DeceptionTacticEventImpl <em>Deception Tactic Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.DeceptionTacticEventImpl
		 * @see dmt.impl.DmtPackageImpl#getDeceptionTacticEvent()
		 * @generated
		 */
		EClass DECEPTION_TACTIC_EVENT = eINSTANCE.getDeceptionTacticEvent();

		/**
		 * The meta object literal for the '{@link dmt.impl.EnvironmentEventImpl <em>Environment Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.EnvironmentEventImpl
		 * @see dmt.impl.DmtPackageImpl#getEnvironmentEvent()
		 * @generated
		 */
		EClass ENVIRONMENT_EVENT = eINSTANCE.getEnvironmentEvent();

		/**
		 * The meta object literal for the '{@link dmt.impl.ObstructedElementImpl <em>Obstructed Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.ObstructedElementImpl
		 * @see dmt.impl.DmtPackageImpl#getObstructedElement()
		 * @generated
		 */
		EClass OBSTRUCTED_ELEMENT = eINSTANCE.getObstructedElement();

		/**
		 * The meta object literal for the '{@link dmt.impl.VulnerableImpl <em>Vulnerable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.VulnerableImpl
		 * @see dmt.impl.DmtPackageImpl#getVulnerable()
		 * @generated
		 */
		EClass VULNERABLE = eINSTANCE.getVulnerable();

		/**
		 * The meta object literal for the '<em><b>Vulnerability</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VULNERABLE__VULNERABILITY = eINSTANCE.getVulnerable_Vulnerability();

		/**
		 * The meta object literal for the '{@link dmt.impl.GoalRefinableImpl <em>Goal Refinable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.GoalRefinableImpl
		 * @see dmt.impl.DmtPackageImpl#getGoalRefinable()
		 * @generated
		 */
		EClass GOAL_REFINABLE = eINSTANCE.getGoalRefinable();

		/**
		 * The meta object literal for the '<em><b>Or Goal Refined</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL_REFINABLE__OR_GOAL_REFINED = eINSTANCE.getGoalRefinable_OrGoalRefined();

		/**
		 * The meta object literal for the '<em><b>And Goal Refined</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GOAL_REFINABLE__AND_GOAL_REFINED = eINSTANCE.getGoalRefinable_AndGoalRefined();

		/**
		 * The meta object literal for the '{@link dmt.impl.BeliefImpl <em>Belief</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.impl.BeliefImpl
		 * @see dmt.impl.DmtPackageImpl#getBelief()
		 * @generated
		 */
		EClass BELIEF = eINSTANCE.getBelief();

		/**
		 * The meta object literal for the '{@link dmt.GoalCategoryEnum <em>Goal Category Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.GoalCategoryEnum
		 * @see dmt.impl.DmtPackageImpl#getGoalCategoryEnum()
		 * @generated
		 */
		EEnum GOAL_CATEGORY_ENUM = eINSTANCE.getGoalCategoryEnum();

		/**
		 * The meta object literal for the '{@link dmt.DomainPropertyCategoryEnum <em>Domain Property Category Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.DomainPropertyCategoryEnum
		 * @see dmt.impl.DmtPackageImpl#getDomainPropertyCategoryEnum()
		 * @generated
		 */
		EEnum DOMAIN_PROPERTY_CATEGORY_ENUM = eINSTANCE.getDomainPropertyCategoryEnum();

		/**
		 * The meta object literal for the '{@link dmt.GoalPriorityEnum <em>Goal Priority Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.GoalPriorityEnum
		 * @see dmt.impl.DmtPackageImpl#getGoalPriorityEnum()
		 * @generated
		 */
		EEnum GOAL_PRIORITY_ENUM = eINSTANCE.getGoalPriorityEnum();

		/**
		 * The meta object literal for the '{@link dmt.ObstacleCategoryEnum <em>Obstacle Category Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.ObstacleCategoryEnum
		 * @see dmt.impl.DmtPackageImpl#getObstacleCategoryEnum()
		 * @generated
		 */
		EEnum OBSTACLE_CATEGORY_ENUM = eINSTANCE.getObstacleCategoryEnum();

		/**
		 * The meta object literal for the '{@link dmt.PreferenceWeightEnum <em>Preference Weight Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.PreferenceWeightEnum
		 * @see dmt.impl.DmtPackageImpl#getPreferenceWeightEnum()
		 * @generated
		 */
		EEnum PREFERENCE_WEIGHT_ENUM = eINSTANCE.getPreferenceWeightEnum();

		/**
		 * The meta object literal for the '{@link dmt.InfluenceWeightEnum <em>Influence Weight Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.InfluenceWeightEnum
		 * @see dmt.impl.DmtPackageImpl#getInfluenceWeightEnum()
		 * @generated
		 */
		EEnum INFLUENCE_WEIGHT_ENUM = eINSTANCE.getInfluenceWeightEnum();

		/**
		 * The meta object literal for the '{@link dmt.InfluencePolarityEnum <em>Influence Polarity Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.InfluencePolarityEnum
		 * @see dmt.impl.DmtPackageImpl#getInfluencePolarityEnum()
		 * @generated
		 */
		EEnum INFLUENCE_POLARITY_ENUM = eINSTANCE.getInfluencePolarityEnum();

		/**
		 * The meta object literal for the '{@link dmt.DeceptionGoalTypeEnum <em>Deception Goal Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.DeceptionGoalTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getDeceptionGoalTypeEnum()
		 * @generated
		 */
		EEnum DECEPTION_GOAL_TYPE_ENUM = eINSTANCE.getDeceptionGoalTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.SoftGoalTypeEnum <em>Soft Goal Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.SoftGoalTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getSoftGoalTypeEnum()
		 * @generated
		 */
		EEnum SOFT_GOAL_TYPE_ENUM = eINSTANCE.getSoftGoalTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.NodesSelectionType <em>Nodes Selection Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.NodesSelectionType
		 * @see dmt.impl.DmtPackageImpl#getNodesSelectionType()
		 * @generated
		 */
		EEnum NODES_SELECTION_TYPE = eINSTANCE.getNodesSelectionType();

		/**
		 * The meta object literal for the '{@link dmt.ChannelTypeEnum <em>Channel Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.ChannelTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getChannelTypeEnum()
		 * @generated
		 */
		EEnum CHANNEL_TYPE_ENUM = eINSTANCE.getChannelTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.MitigationLevelEnum <em>Mitigation Level Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.MitigationLevelEnum
		 * @see dmt.impl.DmtPackageImpl#getMitigationLevelEnum()
		 * @generated
		 */
		EEnum MITIGATION_LEVEL_ENUM = eINSTANCE.getMitigationLevelEnum();

		/**
		 * The meta object literal for the '{@link dmt.AssetTypeEnum <em>Asset Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.AssetTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getAssetTypeEnum()
		 * @generated
		 */
		EEnum ASSET_TYPE_ENUM = eINSTANCE.getAssetTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.AttackTypeEnum <em>Attack Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.AttackTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getAttackTypeEnum()
		 * @generated
		 */
		EEnum ATTACK_TYPE_ENUM = eINSTANCE.getAttackTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.AttackerProfileEnum <em>Attacker Profile Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.AttackerProfileEnum
		 * @see dmt.impl.DmtPackageImpl#getAttackerProfileEnum()
		 * @generated
		 */
		EEnum ATTACKER_PROFILE_ENUM = eINSTANCE.getAttackerProfileEnum();

		/**
		 * The meta object literal for the '{@link dmt.RiskLikelihoodEnum <em>Risk Likelihood Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.RiskLikelihoodEnum
		 * @see dmt.impl.DmtPackageImpl#getRiskLikelihoodEnum()
		 * @generated
		 */
		EEnum RISK_LIKELIHOOD_ENUM = eINSTANCE.getRiskLikelihoodEnum();

		/**
		 * The meta object literal for the '{@link dmt.RiskSeverityEnum <em>Risk Severity Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.RiskSeverityEnum
		 * @see dmt.impl.DmtPackageImpl#getRiskSeverityEnum()
		 * @generated
		 */
		EEnum RISK_SEVERITY_ENUM = eINSTANCE.getRiskSeverityEnum();

		/**
		 * The meta object literal for the '{@link dmt.SecurityControlIncidentEnum <em>Security Control Incident Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.SecurityControlIncidentEnum
		 * @see dmt.impl.DmtPackageImpl#getSecurityControlIncidentEnum()
		 * @generated
		 */
		EEnum SECURITY_CONTROL_INCIDENT_ENUM = eINSTANCE.getSecurityControlIncidentEnum();

		/**
		 * The meta object literal for the '{@link dmt.ThreatCategoryEnum <em>Threat Category Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.ThreatCategoryEnum
		 * @see dmt.impl.DmtPackageImpl#getThreatCategoryEnum()
		 * @generated
		 */
		EEnum THREAT_CATEGORY_ENUM = eINSTANCE.getThreatCategoryEnum();

		/**
		 * The meta object literal for the '{@link dmt.VulnerabilityTypeEnum <em>Vulnerability Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.VulnerabilityTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getVulnerabilityTypeEnum()
		 * @generated
		 */
		EEnum VULNERABILITY_TYPE_ENUM = eINSTANCE.getVulnerabilityTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.SecurityControlNatureEnum <em>Security Control Nature Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.SecurityControlNatureEnum
		 * @see dmt.impl.DmtPackageImpl#getSecurityControlNatureEnum()
		 * @generated
		 */
		EEnum SECURITY_CONTROL_NATURE_ENUM = eINSTANCE.getSecurityControlNatureEnum();

		/**
		 * The meta object literal for the '{@link dmt.BiasTypeEnum <em>Bias Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.BiasTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getBiasTypeEnum()
		 * @generated
		 */
		EEnum BIAS_TYPE_ENUM = eINSTANCE.getBiasTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.SimulationTechniqueTypeEnum <em>Simulation Technique Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.SimulationTechniqueTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getSimulationTechniqueTypeEnum()
		 * @generated
		 */
		EEnum SIMULATION_TECHNIQUE_TYPE_ENUM = eINSTANCE.getSimulationTechniqueTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.MetricTypeEnum <em>Metric Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.MetricTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getMetricTypeEnum()
		 * @generated
		 */
		EEnum METRIC_TYPE_ENUM = eINSTANCE.getMetricTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.DissimulationTechniqueTypeEnum <em>Dissimulation Technique Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.DissimulationTechniqueTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getDissimulationTechniqueTypeEnum()
		 * @generated
		 */
		EEnum DISSIMULATION_TECHNIQUE_TYPE_ENUM = eINSTANCE.getDissimulationTechniqueTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.ArtificialElementTypeEnum <em>Artificial Element Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.ArtificialElementTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getArtificialElementTypeEnum()
		 * @generated
		 */
		EEnum ARTIFICIAL_ELEMENT_TYPE_ENUM = eINSTANCE.getArtificialElementTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.GoalTypeEnum <em>Goal Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.GoalTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getGoalTypeEnum()
		 * @generated
		 */
		EEnum GOAL_TYPE_ENUM = eINSTANCE.getGoalTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.GoalStabilityEnum <em>Goal Stability Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.GoalStabilityEnum
		 * @see dmt.impl.DmtPackageImpl#getGoalStabilityEnum()
		 * @generated
		 */
		EEnum GOAL_STABILITY_ENUM = eINSTANCE.getGoalStabilityEnum();

		/**
		 * The meta object literal for the '{@link dmt.EnvironmentAgentTypeEnum <em>Environment Agent Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.EnvironmentAgentTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getEnvironmentAgentTypeEnum()
		 * @generated
		 */
		EEnum ENVIRONMENT_AGENT_TYPE_ENUM = eINSTANCE.getEnvironmentAgentTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.AttackOriginEnum <em>Attack Origin Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.AttackOriginEnum
		 * @see dmt.impl.DmtPackageImpl#getAttackOriginEnum()
		 * @generated
		 */
		EEnum ATTACK_ORIGIN_ENUM = eINSTANCE.getAttackOriginEnum();

		/**
		 * The meta object literal for the '{@link dmt.AttackConnectorEnum <em>Attack Connector Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.AttackConnectorEnum
		 * @see dmt.impl.DmtPackageImpl#getAttackConnectorEnum()
		 * @generated
		 */
		EEnum ATTACK_CONNECTOR_ENUM = eINSTANCE.getAttackConnectorEnum();

		/**
		 * The meta object literal for the '{@link dmt.StoryTypeEnum <em>Story Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.StoryTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getStoryTypeEnum()
		 * @generated
		 */
		EEnum STORY_TYPE_ENUM = eINSTANCE.getStoryTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.DeceptionRiskTypeEnum <em>Deception Risk Type Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.DeceptionRiskTypeEnum
		 * @see dmt.impl.DmtPackageImpl#getDeceptionRiskTypeEnum()
		 * @generated
		 */
		EEnum DECEPTION_RISK_TYPE_ENUM = eINSTANCE.getDeceptionRiskTypeEnum();

		/**
		 * The meta object literal for the '{@link dmt.BindingStateEnum <em>Binding State Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.BindingStateEnum
		 * @see dmt.impl.DmtPackageImpl#getBindingStateEnum()
		 * @generated
		 */
		EEnum BINDING_STATE_ENUM = eINSTANCE.getBindingStateEnum();

		/**
		 * The meta object literal for the '{@link dmt.EnactmentEnum <em>Enactment Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see dmt.EnactmentEnum
		 * @see dmt.impl.DmtPackageImpl#getEnactmentEnum()
		 * @generated
		 */
		EEnum ENACTMENT_ENUM = eINSTANCE.getEnactmentEnum();

	}

} //DmtPackage
