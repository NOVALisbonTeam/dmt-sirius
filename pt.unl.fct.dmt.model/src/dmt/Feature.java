/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getFeature()
 * @model
 * @generated
 */
public interface Feature extends AbstractFeature {
} // Feature
