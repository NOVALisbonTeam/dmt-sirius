/**
 */
package dmt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Security Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dmt.DmtPackage#getSecurityNode()
 * @model abstract="true"
 * @generated
 */
public interface SecurityNode extends Node {
} // SecurityNode
