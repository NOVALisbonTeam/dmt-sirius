/**
 */
package dmt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attacker</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Aka Hacker, Threat Agent
 * 
 * A security hacker is someone who seeks to breach defenses and exploit weaknesses in a computer system or network. Hackers may be motivated by a multitude of reasons, such as profit, protest, information gathering,[1] challenge, recreation,[2] or to evaluate system weaknesses to assist in formulating defenses against potential hackers. The subculture that has evolved around hackers is often referred to as the computer underground.[3]
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.Attacker#getProfile <em>Profile</em>}</li>
 *   <li>{@link dmt.Attacker#getLaunchesAttack <em>Launches Attack</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAttacker()
 * @model
 * @generated
 */
public interface Attacker extends SecurityNode {
	/**
	 * Returns the value of the '<em><b>Profile</b></em>' attribute.
	 * The default value is <code>"SCRIPTKIDDIE"</code>.
	 * The literals are from the enumeration {@link dmt.AttackerProfileEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Profile</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Profile</em>' attribute.
	 * @see dmt.AttackerProfileEnum
	 * @see #setProfile(AttackerProfileEnum)
	 * @see dmt.DmtPackage#getAttacker_Profile()
	 * @model default="SCRIPTKIDDIE"
	 * @generated
	 */
	AttackerProfileEnum getProfile();

	/**
	 * Sets the value of the '{@link dmt.Attacker#getProfile <em>Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Profile</em>' attribute.
	 * @see dmt.AttackerProfileEnum
	 * @see #getProfile()
	 * @generated
	 */
	void setProfile(AttackerProfileEnum value);

	/**
	 * Returns the value of the '<em><b>Launches Attack</b></em>' reference list.
	 * The list contents are of type {@link dmt.Attack}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Launches Attack</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Launches Attack</em>' reference list.
	 * @see dmt.DmtPackage#getAttacker_LaunchesAttack()
	 * @model
	 * @generated
	 */
	EList<Attack> getLaunchesAttack();

} // Attacker
