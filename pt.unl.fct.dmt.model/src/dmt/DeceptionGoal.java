/**
 */
package dmt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deception Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.DeceptionGoal#getDeceptionGoalType <em>Deception Goal Type</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getDeceptionGoal()
 * @model abstract="true"
 * @generated
 */
public interface DeceptionGoal extends Goal, DeceptionNode {
	/**
	 * Returns the value of the '<em><b>Deception Goal Type</b></em>' attribute.
	 * The literals are from the enumeration {@link dmt.DeceptionGoalTypeEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deception Goal Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deception Goal Type</em>' attribute.
	 * @see dmt.DeceptionGoalTypeEnum
	 * @see #setDeceptionGoalType(DeceptionGoalTypeEnum)
	 * @see dmt.DmtPackage#getDeceptionGoal_DeceptionGoalType()
	 * @model
	 * @generated
	 */
	DeceptionGoalTypeEnum getDeceptionGoalType();

	/**
	 * Sets the value of the '{@link dmt.DeceptionGoal#getDeceptionGoalType <em>Deception Goal Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deception Goal Type</em>' attribute.
	 * @see dmt.DeceptionGoalTypeEnum
	 * @see #getDeceptionGoalType()
	 * @generated
	 */
	void setDeceptionGoalType(DeceptionGoalTypeEnum value);

} // DeceptionGoal
