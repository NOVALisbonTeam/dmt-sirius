/**
 */
package dmt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link dmt.AgentAssignment#getAgentGoalAssignment <em>Agent Goal Assignment</em>}</li>
 *   <li>{@link dmt.AgentAssignment#getAgentAssignment <em>Agent Assignment</em>}</li>
 * </ul>
 *
 * @see dmt.DmtPackage#getAgentAssignment()
 * @model
 * @generated
 */
public interface AgentAssignment extends AgentRelation {
	/**
	 * Returns the value of the '<em><b>Agent Goal Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Agent Goal Assignment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Goal Assignment</em>' reference.
	 * @see #setAgentGoalAssignment(LeafGoal)
	 * @see dmt.DmtPackage#getAgentAssignment_AgentGoalAssignment()
	 * @model required="true"
	 * @generated
	 */
	LeafGoal getAgentGoalAssignment();

	/**
	 * Sets the value of the '{@link dmt.AgentAssignment#getAgentGoalAssignment <em>Agent Goal Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent Goal Assignment</em>' reference.
	 * @see #getAgentGoalAssignment()
	 * @generated
	 */
	void setAgentGoalAssignment(LeafGoal value);

	/**
	 * Returns the value of the '<em><b>Agent Assignment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Agent Assignment</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent Assignment</em>' reference.
	 * @see #setAgentAssignment(Agent)
	 * @see dmt.DmtPackage#getAgentAssignment_AgentAssignment()
	 * @model required="true"
	 * @generated
	 */
	Agent getAgentAssignment();

	/**
	 * Sets the value of the '{@link dmt.AgentAssignment#getAgentAssignment <em>Agent Assignment</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent Assignment</em>' reference.
	 * @see #getAgentAssignment()
	 * @generated
	 */
	void setAgentAssignment(Agent value);

} // AgentAssignment
