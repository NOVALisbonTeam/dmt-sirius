/**
 * Copyright (c) 2007, 2014 THALES GLOBAL SERVICES.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *    Obeo - initial API and implementation
 *    NOVALINCS - NOVA University of Lisbon, DML integration 
 * 
 */
package interactions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mix End</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see interactions.InteractionsPackage#getMixEnd()
 * @model
 * @generated
 */
public interface MixEnd extends ExecutionEnd, MessageEnd {
} // MixEnd
