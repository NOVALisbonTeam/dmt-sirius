/**
 */
package dmt.provider;


import dmt.DeceptionTactic;
import dmt.DmtPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dmt.DeceptionTactic} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DeceptionTacticItemProvider extends DeceptionNodeItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeceptionTacticItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDomPostPropertyDescriptor(object);
			addDomPrePropertyDescriptor(object);
			addFormalDomPostPropertyDescriptor(object);
			addFormalDomPrePropertyDescriptor(object);
			addSubTacticPropertyDescriptor(object);
			addApplyDissimulationPropertyDescriptor(object);
			addDescribedByDeceptionStoryPropertyDescriptor(object);
			addMeasuredByPropertyDescriptor(object);
			addMonitoredByPropertyDescriptor(object);
			addLeadDeceptionRiskPropertyDescriptor(object);
			addApplySimulationPropertyDescriptor(object);
			addIsAbstractPropertyDescriptor(object);
			addQualityattributePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Dom Post feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDomPostPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Operationable_domPost_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Operationable_domPost_feature", "_UI_Operationable_type"),
				 DmtPackage.Literals.OPERATIONABLE__DOM_POST,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Formal Dom Post feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFormalDomPostPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Operationable_formalDomPost_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Operationable_formalDomPost_feature", "_UI_Operationable_type"),
				 DmtPackage.Literals.OPERATIONABLE__FORMAL_DOM_POST,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Formal Dom Pre feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFormalDomPrePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Operationable_formalDomPre_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Operationable_formalDomPre_feature", "_UI_Operationable_type"),
				 DmtPackage.Literals.OPERATIONABLE__FORMAL_DOM_PRE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Dom Pre feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDomPrePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Operationable_domPre_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Operationable_domPre_feature", "_UI_Operationable_type"),
				 DmtPackage.Literals.OPERATIONABLE__DOM_PRE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sub Tactic feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSubTacticPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DeceptionTactic_subTactic_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DeceptionTactic_subTactic_feature", "_UI_DeceptionTactic_type"),
				 DmtPackage.Literals.DECEPTION_TACTIC__SUB_TACTIC,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Apply Dissimulation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addApplyDissimulationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DeceptionTactic_applyDissimulation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DeceptionTactic_applyDissimulation_feature", "_UI_DeceptionTactic_type"),
				 DmtPackage.Literals.DECEPTION_TACTIC__APPLY_DISSIMULATION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Described By Deception Story feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescribedByDeceptionStoryPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DeceptionTactic_describedByDeceptionStory_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DeceptionTactic_describedByDeceptionStory_feature", "_UI_DeceptionTactic_type"),
				 DmtPackage.Literals.DECEPTION_TACTIC__DESCRIBED_BY_DECEPTION_STORY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Measured By feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMeasuredByPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DeceptionTactic_measuredBy_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DeceptionTactic_measuredBy_feature", "_UI_DeceptionTactic_type"),
				 DmtPackage.Literals.DECEPTION_TACTIC__MEASURED_BY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Monitored By feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMonitoredByPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DeceptionTactic_monitoredBy_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DeceptionTactic_monitoredBy_feature", "_UI_DeceptionTactic_type"),
				 DmtPackage.Literals.DECEPTION_TACTIC__MONITORED_BY,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Lead Deception Risk feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLeadDeceptionRiskPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DeceptionTactic_leadDeceptionRisk_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DeceptionTactic_leadDeceptionRisk_feature", "_UI_DeceptionTactic_type"),
				 DmtPackage.Literals.DECEPTION_TACTIC__LEAD_DECEPTION_RISK,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Apply Simulation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addApplySimulationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DeceptionTactic_applySimulation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DeceptionTactic_applySimulation_feature", "_UI_DeceptionTactic_type"),
				 DmtPackage.Literals.DECEPTION_TACTIC__APPLY_SIMULATION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Is Abstract feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsAbstractPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DeceptionTactic_isAbstract_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DeceptionTactic_isAbstract_feature", "_UI_DeceptionTactic_type"),
				 DmtPackage.Literals.DECEPTION_TACTIC__IS_ABSTRACT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Qualityattribute feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addQualityattributePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_DeceptionTactic_qualityattribute_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_DeceptionTactic_qualityattribute_feature", "_UI_DeceptionTactic_type"),
				 DmtPackage.Literals.DECEPTION_TACTIC__QUALITYATTRIBUTE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns DeceptionTactic.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DeceptionTactic"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((DeceptionTactic)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_DeceptionTactic_type") :
			getString("_UI_DeceptionTactic_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DeceptionTactic.class)) {
			case DmtPackage.DECEPTION_TACTIC__DOM_POST:
			case DmtPackage.DECEPTION_TACTIC__DOM_PRE:
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_POST:
			case DmtPackage.DECEPTION_TACTIC__FORMAL_DOM_PRE:
			case DmtPackage.DECEPTION_TACTIC__IS_ABSTRACT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
