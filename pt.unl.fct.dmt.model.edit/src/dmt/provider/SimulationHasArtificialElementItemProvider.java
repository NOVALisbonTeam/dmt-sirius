/**
 * NOVALINCS - Nova University of Lisbon, Portugal
 */
package dmt.provider;


import dmt.DmtPackage;
import dmt.SimulationHasArtificialElement;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dmt.SimulationHasArtificialElement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SimulationHasArtificialElementItemProvider extends DeceptionRelationshipItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimulationHasArtificialElementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addShowFalseAssetPropertyDescriptor(object);
			addLowerFakesPropertyDescriptor(object);
			addUpperFakesPropertyDescriptor(object);
			addSimulationTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Show False Asset feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShowFalseAssetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SimulationHasArtificialElement_showFalseAsset_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SimulationHasArtificialElement_showFalseAsset_feature", "_UI_SimulationHasArtificialElement_type"),
				 DmtPackage.Literals.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SHOW_FALSE_ASSET,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Lower Fakes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLowerFakesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SimulationHasArtificialElement_lowerFakes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SimulationHasArtificialElement_lowerFakes_feature", "_UI_SimulationHasArtificialElement_type"),
				 DmtPackage.Literals.SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Upper Fakes feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUpperFakesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SimulationHasArtificialElement_upperFakes_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SimulationHasArtificialElement_upperFakes_feature", "_UI_SimulationHasArtificialElement_type"),
				 DmtPackage.Literals.SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Simulation Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimulationTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SimulationHasArtificialElement_simulationType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SimulationHasArtificialElement_simulationType_feature", "_UI_SimulationHasArtificialElement_type"),
				 DmtPackage.Literals.SIMULATION_HAS_ARTIFICIAL_ELEMENT__SIMULATION_TYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns SimulationHasArtificialElement.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SimulationHasArtificialElement"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SimulationHasArtificialElement)object).getAltName();
		return label == null || label.length() == 0 ?
			getString("_UI_SimulationHasArtificialElement_type") :
			getString("_UI_SimulationHasArtificialElement_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SimulationHasArtificialElement.class)) {
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__LOWER_FAKES:
			case DmtPackage.SIMULATION_HAS_ARTIFICIAL_ELEMENT__UPPER_FAKES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
