/**
 */
package dmt.provider;


import dmt.DMTModel;
import dmt.DmtFactory;
import dmt.DmtPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link dmt.DMTModel} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DMTModelItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DMTModelItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_NODES);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_RELATIONS);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_RELATIONS);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_NODES);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_RELATIONS);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_CONTAINERS);
			childrenFeatures.add(DmtPackage.Literals.DMT_MODEL__HAS_MISC_NODES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns DMTModel.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/DMTModel"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_DMTModel_type");
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(DMTModel.class)) {
			case DmtPackage.DMT_MODEL__HAS_SECURITY_NODES:
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_NODES:
			case DmtPackage.DMT_MODEL__HAS_GOAL_NODES:
			case DmtPackage.DMT_MODEL__HAS_GOAL_RELATIONS:
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_NODES:
			case DmtPackage.DMT_MODEL__HAS_STRATEGY_RELATIONS:
			case DmtPackage.DMT_MODEL__HAS_DECEPTION_RELATIONS:
			case DmtPackage.DMT_MODEL__HAS_SECURITY_RELATIONS:
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_NODES:
			case DmtPackage.DMT_MODEL__HAS_INFLUENCE_RELATIONS:
			case DmtPackage.DMT_MODEL__HAS_GOAL_CONTAINERS:
			case DmtPackage.DMT_MODEL__HAS_MISC_NODES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createAttack()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createThreat()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createVulnerability()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createSecurityControl()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createRiskComponent()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createAttacker()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createArtificialElement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createAttackVector()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createAProperty()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createAttackStepConnector()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createInitialAttackStep()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createEndAttackStep()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createAttackTree()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createDeceptionRisk()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES,
				 DmtFactory.eINSTANCE.createConcreteAsset()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createDeceptionTactic()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createIVulnerability()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createDeceptionStory()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createDeceptionBias()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createArtificialElement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createSimulation()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createDissimulation()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createDeceptionMetric()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createDeceptionChannel()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createDeceptionBehaviourGoal()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createDeceptionRisk()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createDeceptionSoftGoal()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES,
				 DmtFactory.eINSTANCE.createProperty()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createSoftwareAgent()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createEnvironmentAgent()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createBehaviourGoal()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createDomainDescription()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createExpectation()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createRequirement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createOperation()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createThreat()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createDeceptionTactic()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createSoftGoal()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createDeceptionBehaviourGoal()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createObstacle()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createBelief()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES,
				 DmtFactory.eINSTANCE.createDeceptionSoftGoal()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS,
				 DmtFactory.eINSTANCE.createOperationalization()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS,
				 DmtFactory.eINSTANCE.createORGoalRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS,
				 DmtFactory.eINSTANCE.createAgentAssignment()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS,
				 DmtFactory.eINSTANCE.createANDGoalRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS,
				 DmtFactory.eINSTANCE.createConflictGoalRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS,
				 DmtFactory.eINSTANCE.createAgentMonitoring()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS,
				 DmtFactory.eINSTANCE.createAgentControl()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS,
				 DmtFactory.eINSTANCE.createORObstructionRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_RELATIONS,
				 DmtFactory.eINSTANCE.createANDObstructionRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_NODES,
				 DmtFactory.eINSTANCE.createDeceptionStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_NODES,
				 DmtFactory.eINSTANCE.createDeceptionTacticMechanism()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_NODES,
				 DmtFactory.eINSTANCE.createFeature()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_NODES,
				 DmtFactory.eINSTANCE.createDeceptionTacticStrategy()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createRequiredConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createExcludeConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createBenefitConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createAvoidConstraint()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createTacticRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createFeatureRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createStrategyTacticRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createTacticFeatureRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createNodeSelection()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createDeceptionStrategyTacticRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createDeceptionTacticStrategyRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_STRATEGY_RELATIONS,
				 DmtFactory.eINSTANCE.createDeceptionStrategyRefinement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_RELATIONS,
				 DmtFactory.eINSTANCE.createSimulationHasArtificialElement()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_RELATIONS,
				 DmtFactory.eINSTANCE.createMitigation()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_NODES,
				 DmtFactory.eINSTANCE.createInfluenceNode()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_NODES,
				 DmtFactory.eINSTANCE.createQualityAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_NODES,
				 DmtFactory.eINSTANCE.createEvent()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_NODES,
				 DmtFactory.eINSTANCE.createDeceptionTacticEvent()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_NODES,
				 DmtFactory.eINSTANCE.createEnvironmentEvent()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_RELATIONS,
				 DmtFactory.eINSTANCE.createInfluenceRelationship()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_RELATIONS,
				 DmtFactory.eINSTANCE.createInfluence()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_INFLUENCE_RELATIONS,
				 DmtFactory.eINSTANCE.createPreference()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_GOAL_CONTAINERS,
				 DmtFactory.eINSTANCE.createGoalContainer()));

		newChildDescriptors.add
			(createChildParameter
				(DmtPackage.Literals.DMT_MODEL__HAS_MISC_NODES,
				 DmtFactory.eINSTANCE.createAnnotation()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == DmtPackage.Literals.DMT_MODEL__HAS_SECURITY_NODES ||
			childFeature == DmtPackage.Literals.DMT_MODEL__HAS_GOAL_NODES ||
			childFeature == DmtPackage.Literals.DMT_MODEL__HAS_DECEPTION_NODES;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return DmtEditPlugin.INSTANCE;
	}

}
