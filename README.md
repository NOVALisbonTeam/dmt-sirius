# Deception Modeling Tool #

Welcome to Deception Modeling Tool !

![DMT](https://bitbucket.org/NOVALisbonTeam/dmt-sirius/downloads/deception-tool-support-v2.png)

The Deception Modeling Tool (DMT) project aims at developing modeling tools for deception-based defense.
DMT is based on Deception modeling language (DML), a visual language for deception modeling. 
Deception-based defense relies on deliberated actions employed by the defender to manipulate the attackers' perception of a system. 
Despite advances in security modeling, deceptive strategies have not been considered in an appropriate level of abstraction with the separation of traditional security mechanisms from those based on deception.
DML is composed of three main components, each three different views deception: requirements model, deception strategy organizational model, and deception tactics feature model. 
DML is based on the concepts of general theory of deception and integrates goal-oriented requirements models and threat models to compose a comprehensive model considering the influences of developing deceptive mechanisms and the associated risks.

### What is this repository for? ###

* This is the repository for Deception Modeling Tool (DMT) developed with Sirius.

### How do I get set up? ###

To setup a DMT project, you should clone the repository (master branch) using your preferred git client.

* Projects Structure
	There are 6 projects in the DMT repository to clone
	*  pt.unl.fct.dmt.design: containing the description\dmt.odesign 
	*  pt.unl.fct.dmt.model: containing the ecore models 
	*  pt.unl.fct.dmt.model.edit: part of the editor 
	*  pt.unl.fct.dmt.model.editor: part of the editor
	*  pt.unl.fct.dmt.model.tests: 	tests of EMF model
	*  pt.unl.fct.dmt.test: tests using Sirius models

Following, you can find our environment development. 

* [Eclipse](https://www.eclipse.org/)
	* Eclipse Modeling Project - Version: Oxygen Release (4.7.0) Build id: (20170620-1800)
* [EGit](http://www.eclipse.org/egit/)
	* Version (4.8.0)
* [Sirius](http://www.eclipse.org/sirius/)
	* Version (5.1.0.201709140800)
* [Java](https://java.com/en/)
	* Version (1.8.0_60)
* How to run tests
	* Open pt.unl.fct.dmt.test project, within representations.aird, you will find some viewpoints based on the models created during the design. You can create and test other models according to your needs.
* Deployment instructions
	* We still do not have packaged the tool for distribution. Coming soon...



